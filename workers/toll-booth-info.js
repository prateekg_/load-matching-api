/*
    Toll plaza information scraper
*/

const request = require("request");
const cheerio = require('cheerio');
const tabletojson = require('tabletojson');
const async = require('async');
const _ = require('lodash');
const fs = require('fs');

let options = {
  method: 'POST',
  url: 'http://tis.nhai.gov.in/TollPlazaService.asmx/GetTollPlazaInfoForMapOnPC',
  headers: {
    'Content-Type': 'application/json'
  }
};

let tollBoothsFile = [];

function getTollPlazaInfo(callback) {
  let tollBooths = [];
  console.log("Fetching from nhtis....");
  request(options, function (error, response, body) {
    if (error) return console.log(error);
    let data = JSON.parse(body);

    async.eachSeries(data.d, function (tollPlaza, cb) {
      try {
        let $ = cheerio.load(tollPlaza.HtmlPopup);

        let tableData = $.html('table.tollinfotbl');
        let fee = tabletojson.convert(tableData, { useFirstRowForHeadings: true })[0];
        let state = ($('.PA15 > p > lable').first().text()).split('Of')[1].trim().toLowerCase();

        if (fee.length > 0) {
          let check = false;
          _.each(fee[0], function (value, key) {
            if (key === value) {
              check = true
            }
          });

          if (check) {
            fee.splice(0, 1)
          }
        }

        _.each(fee, function (obj) {
          _.each(obj, function (value, key) {
            obj[_.camelCase(key)] = value;
            obj[key] = undefined
          })
        });

        tollBooths.push({
          tollPlazaId: tollPlaza.TollPlazaID,
          tollName: tollPlaza.TollName,
          latitude: tollPlaza.latitude,
          longitude: tollPlaza.longitude,
          state: state,
          fee: fee
        });
        cb()
      } catch (e) {
        cb(e)
      }
    }, function (err) {
      if (!err) {
        tollBoothsFile = tollBooths;
        fs.writeFile('./toll-booth.json', JSON.stringify(tollBooths), function () {
        })
      }
      callback()
    })
  });
}

//readFile()

function readFile() {
  fs.readFile('./toll-booth.json', function (err, data) {
    console.log(err, data);
    if (err || !data) {
      getTollPlazaInfo()
    } else {
      tollBoothsFile = JSON.parse(data.toString())
    }
  })
}


exports.getTollBooths = function (state, vehicleType, cb) {
  vehicleType = vehicleType ? vehicleType.toLowerCase() : '';
  let type;
  let data = _.cloneDeep(tollBoothsFile);
  let response;
  switch (vehicleType) {
  case 'car':
  case 'jeep':
  case 'van':
  case 'lmv':
    type = 'Car/Jeep/Van';
    break;
  case 'lcv':
    type = 'lcv';
    break;
  case 'bus':
  case 'truck':
    type = 'Bus/Truck';
    break;
  case '2axle':
  case '3axle':
    type = 'Upto 3 Axle Vehicle';
    break;
  case '4axle':
  case '5axle':
  case '6axle':
    type = '4 to 6 Axle';
    break;
  case 'hcm':
  case 'eme':
    type = 'HCM/EME';
    break;
  case '7axle':
    type = '7 or more Axle';
    break
  }

  response = _.filter(data, { state: state });

  if (type) {
    _.each(response, function (tollPlaza) {
      tollPlaza.fee = _.filter(tollPlaza.fee, { typeOfVehicle: type })
    })
  }

  return cb(null, response)
};

exports.scrapeData = function (req, res) {
  getTollPlazaInfo(function () {
    let data = _.cloneDeep(tollBoothsFile);
    return res.send({
      data
    })
  })
};