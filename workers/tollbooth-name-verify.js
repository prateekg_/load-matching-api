const fs = require('fs');
const models = require('../db/models');
const async = require('async');

fs.readFile('./toll-booth.json', function (err, data) {
  data = JSON.parse(data.toString());
  let count = 0;
  async.each(data, function (tollPlaza, cb) {
    let name = tollPlaza.tollName.split('(')[0].trim();
    let query = "SELECT id, name FROM pois where name ilike '" + name + "%'";

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (result) {
      if(result.length > 1 || result.length === 0) {
        count ++;
        console.log(tollPlaza.tollName, result);
        return cb()
      }
      let id = result[0].id;
      models.poi.update({
        state: tollPlaza.state
      }, {
        where: {
          id: id
        }
      }).then(function () {
        cb()
      }).catch(function (err) {
        cb(err)
      })
    }).catch(function (err) {
      cb(err)
    })
  }, function () {
    console.log(count)
  })

});