const async = require("async");
const models = require('../db/models');
const DATA = [];

function updatePoiKeys() {
    console.log("POI update script started")

    async.forEach(DATA, function (i, cb) {
        models.poi.update({
            lat: i.lat,
            lon: i.lon
        }, {
            where: {
                id: i.id
            }
        }).then(function () {
            console.log("UPDATED : ", i.id)
            cb()
        }).catch(function (err) {
            cb(err)
        })
    }, function (err) {
        if (err) {
            console.error("POI update script stopped")
            console.error(err)
        } else {
            console.log("POI update script completed")
        }
    })
}

// setTimeout(function () {
//     updatePoiKeys();
// }, 5000)
