const models = require('../db/models/index');
let URLS = {};
const env = process.env.NODE_ENV || 'development';

/**
 * @description Get all project URLs from database
 */
function getURLsFromDB() {
  models.sequelize.query(
    "SELECT * FROM urls",
    { type: models.sequelize.QueryTypes.SELECT })
    .then(function (data) {
      data.forEach(function (i) {
        URLS[i.name] = i
      })
    }).catch(function (err) {
      console.error("getURLsFromDB() : ", err)
    })
}
getURLsFromDB();

/**
 * @description resolve server name to IP address
 * @param {string} name
 * @returns {string} IP
 */
function resolveToIP(name) {
  let ip = null;
  return URLS[name] && "local" === env ? ip = URLS[name].public_host : URLS[name] && (ip = URLS[name].host), ip
}

let server_address = {
  metrics: function () {
    return resolveToIP('Dashboard')
  },
  fleet_metrics: function () {
    return resolveToIP('Fleet - Metrics')
  },
  consign_metrics: function () {
    return resolveToIP('Consign - Metrics')
  },
  user_management: function () {
    return resolveToIP('User Management')
  },
  location: function () {
    return resolveToIP('Location Module')
  },
  ping: function () {
    return resolveToIP('Nubot')
  },
  consign_trip_planning: function () {
    return resolveToIP('Consign - trip planning')
  },
  driver_report_engine: function () {
    return resolveToIP('Driver Report server')
  },
  wad_server: function () {
    return resolveToIP('WAD')
  },
  location_api: function () {
    return resolveToIP('Location API')
  },
  map_match: function () {
    return resolveToIP('Dynamo MapMatch')
  },
  fleet_trip_planning: function () {
    return resolveToIP('Fleet - trip planning')
  },
  route_planning: function () {
    return resolveToIP('Route planning')
  },
  retrospective_api_server: function () {
    return resolveToIP('Retrospective API server')
  },
  temperature_sensor_log: function () {
    return resolveToIP('Temperature sensor log')
  },
  reports_server: function () {
    return resolveToIP('Report server')
  },
  eol_reports_server: function () {
    return resolveToIP('EOL Report server')
  },
  tollbooth_intersection: function () {
    return resolveToIP('Tollbooth intersection')
  },
  lanes_server: function () {
    return resolveToIP('lanes_api')
  }
};

module.exports = {
  local: {
    server_address: server_address,
    url: {
      domain: 'http://appdev.numadic.com'
    },
    rabbitMq: {
      userName: "numadic",
      password: "nomadic"
    },
    tokens: {
      wad_common: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiV2ViIFZpZXcgVG9rZW4gVXNlciIsImFkbWluX2lkIjoyNywiZW1haWwiOiJ3dmFjY2Vzc3Rva2VuQG51bWFkaWMuY29tIiwiYWRtaW5fZ3JvdXBzIjpbMzRdLCJpYXQiOjE1MDA4ODIzODJ9.SOqRUBMupR3thXD7xvt90xaPNj6w9JFE7LfrXb0LwX0',
      numadicAdmin: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiV2ViIFZpZXcgVG9rZW4gVXNlciIsImFkbWluX2lkIjoyNywiZW1haWwiOiJ3dmFjY2Vzc3Rva2VuQG51bWFkaWMuY29tIiwiYWRtaW5fZ3JvdXBzIjpbMzRdLCJpYXQiOjE1MDA4ODIzODJ9.SOqRUBMupR3thXD7xvt90xaPNj6w9JFE7LfrXb0LwX0'
    },
    mailer: {
      host: "email-smtp.us-west-2.amazonaws.com",
      port: 465,
      auth: {
        user: "AKIAJOFLA32HQYQXKY5Q",
        pass: "As9PUqzmF6Y7G/C0wyTsiy1dU3eUXJwr0Viq6Bvy27br"
      }
    },
    otp: {
      url: 'http://apid.numadic.com',
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJma191X2lkIjoiNjEiLCJma19jX2lkIjoiMjUiLCJwbGF0Zm9ybSI6Im1jIiwiaWF0IjoxNTA4NzYzMDE0fQ.8tgg3lNFduYlC3le-bOJjzDjbslFt_n24DgPK4lHnL4',
      twofactorurl: 'https://2factor.in/API/V1/0aa05654-5223-11e6-a8cd-00163ef91450/ADDON_SERVICES/SEND/TSMS'
    },
    googleMap: {
      key: "AIzaSyCBpdkrLwnML0daFBgs4cTz0MdAgn4Xodo",
      url: "https://maps.googleapis.com/maps/api/geocode/json?"
    },
    razorpay: {
      key: 'rzp_test_Un5pzjP0Q2ft6c',
      secret: '6TN1eQNJDCz5mZ8HB0dy2aMR'
    },
    redis: {
      url: '127.0.0.1',
      port: 6379
    }
  },
  development: {
    server_address: server_address,
    url: {
      domain: 'http://appdev.numadic.com'
    },
    rabbitMq: {
      userName: "numadic",
      password: "nomadic"
    },
    tokens: {
      wad_common: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiV2ViIFZpZXcgVG9rZW4gVXNlciIsImFkbWluX2lkIjoyNywiZW1haWwiOiJ3dmFjY2Vzc3Rva2VuQG51bWFkaWMuY29tIiwiYWRtaW5fZ3JvdXBzIjpbMzRdLCJpYXQiOjE1MDA4ODIzODJ9.SOqRUBMupR3thXD7xvt90xaPNj6w9JFE7LfrXb0LwX0',
      numadicAdmin: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiV2ViIFZpZXcgVG9rZW4gVXNlciIsImFkbWluX2lkIjoyNywiZW1haWwiOiJ3dmFjY2Vzc3Rva2VuQG51bWFkaWMuY29tIiwiYWRtaW5fZ3JvdXBzIjpbMzRdLCJpYXQiOjE1MDA4ODIzODJ9.SOqRUBMupR3thXD7xvt90xaPNj6w9JFE7LfrXb0LwX0'
    },
    mailer: {
      host: "email-smtp.us-west-2.amazonaws.com",
      port: 465,
      auth: {
        user: "AKIAJOFLA32HQYQXKY5Q",
        pass: "As9PUqzmF6Y7G/C0wyTsiy1dU3eUXJwr0Viq6Bvy27br"
      }
    },
    otp: {
      url: 'http://apid.numadic.com',
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJma191X2lkIjoiNjEiLCJma19jX2lkIjoiMjUiLCJwbGF0Zm9ybSI6Im1jIiwiaWF0IjoxNTA4NzYzMDE0fQ.8tgg3lNFduYlC3le-bOJjzDjbslFt_n24DgPK4lHnL4',
      twofactorurl: 'https://2factor.in/API/V1/0aa05654-5223-11e6-a8cd-00163ef91450/ADDON_SERVICES/SEND/TSMS'
    },
    googleMap: {
      key: "AIzaSyCBpdkrLwnML0daFBgs4cTz0MdAgn4Xodo",
      url: "https://maps.googleapis.com/maps/api/geocode/json?"
    },
    razorpay: {
      key: 'rzp_test_Un5pzjP0Q2ft6c',
      secret: '6TN1eQNJDCz5mZ8HB0dy2aMR'
    },
    redis: {
      url: 'nu-redis-dev.hzh2zq.0001.apse1.cache.amazonaws.com',
      port: 6379
    }
  },
  production: {
    server_address: server_address,
    url: {
      domain: 'http://app.numadic.com'
    },
    rabbitMq: {
      userName: "nu-user",
      password: "jAaG9d58jqJ3fOdKmNkl"
    },
    tokens: {
      wad_common: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiUGxhdGZvcm0gQ29tbW9uIE1hY2hpbmUgVG9rZW4iLCJhZG1pbl9pZCI6NzksImVtYWlsIjoicGxhdGZvcm1jb21tb25tYWNoaW5ldG9rZW5AbnVtYWRpYy5jb20iLCJhZG1pbl9ncm91cHMiOls1NF0sImlhdCI6MTUyMDUwMzI1M30.s_wN68LuGcGnlMp2NK8RJ9bYwuUUnFp5mdpromN6ckY',
      wad_fleet: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRmxlZXQgIE1hY2hpbmUgVG9rZW4iLCJhZG1pbl9pZCI6NzcsImVtYWlsIjoiZmxlZXRtYWNoaW5ldG9rZW5AbnVtYWRpYy5jb20iLCJhZG1pbl9ncm91cHMiOls1Ml0sImlhdCI6MTUyMDUwMjc5M30.31O9j6lu4akigdFB7OJxpWtx8pmZYSrkO34OcT4ANwY',
      numadicAdmin: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiV2ViIFZpZXcgVG9rZW4gVXNlciIsImFkbWluX2lkIjo1MSwiZW1haWwiOiJ3dmFjY2Vzc3Rva2VuQG51bWFkaWMuY29tIiwiYWRtaW5fZ3JvdXBzIjpbMzZdLCJpYXQiOjE1MDQ3NzI0NDR9.Kw8KEGhz1BhgiimeVZCDyNZX9wvHjMO_KviiDzEaM_o'
    },
    mailer: {
      host: "email-smtp.us-west-2.amazonaws.com",
      port: 465,
      auth: {
        user: "AKIAJOFLA32HQYQXKY5Q",
        pass: "As9PUqzmF6Y7G/C0wyTsiy1dU3eUXJwr0Viq6Bvy27br"
      }
    },
    otp: {
      url: 'http://api.numadic.com',
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJma19jX2lkIjoiMSIsImZrX3VfaWQiOiI2NTI1IiwicGxhdGZvcm0iOiJtYyIsImlhdCI6MTUxOTcyMzYwOX0.Dv7gLH9I08370k5KOgoryeyat94OKILAb1PIB0hEYhE',
      twofactorurl: 'https://2factor.in/API/V1/0aa05654-5223-11e6-a8cd-00163ef91450/ADDON_SERVICES/SEND/TSMS'
    },
    googleMap: {
      key: "AIzaSyCBpdkrLwnML0daFBgs4cTz0MdAgn4Xodo",
      url: "https://maps.googleapis.com/maps/api/geocode/json?"
    },
    razorpay: {
      key: 'rzp_live_9HLLmvlUyX3QbE',
      secret: 'wRPp4Jm0LmKUc2GOGRk6Ctc1'
    },
    redis: {
      url: 'nu-redis-prod.hzh2zq.0001.apse1.cache.amazonaws.com',
      port: 6379
    }
  }
};