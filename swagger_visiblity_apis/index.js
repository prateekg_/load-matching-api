
module.exports =
{
    "schemes": [
        "https"
    ],
    "swagger": "2.0",
    "info": {
        "description": "The purpose of this documentation is to provide vehicle visibility via APIs",
        "title": "Visibility API Documentation",
        "contact": {
            "name": "Numadic",
            "email": "help@numadic.com"
        },
        "version": "1.0.0"
    },
    "host": "api.numadic.com",
    "paths": {
        "/client/v1/fastag/locations": {
            "post": {
                "description": "View last know location or location history\n\n - Enter just vehicel number for fetch last known location\n - Enter StartTripDate and EndTripDate to view location history",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Locations"
                ],
                "summary": "View last know location or location history",
                "operationId": "getLocations",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Authorization token provided by Numadic. Append token with \"bearer \"",
                        "name": "Authorization",
                        "in": "header",
                        "required": true
                    },
                    {
                        "description": "Request location paramaters",
                        "name": "Location Request Details",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/locationRequestParams"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "$ref": "#/responses/locationsData"
                    },
                    "400": {
                        "description": "Missing parameters"
                    },
                    "402": {
                        "description": "Total credits consumed"
                    },
                    "403": {
                        "description": "Limit exhausted"
                    },
                    "404": {
                        "description": "Vehicle/Locations not found on system"
                    },
                    "default": {
                        "description": "Unexpected Error"
                    }
                }
            }
        },
        "/v3/company/credits": {
            "get": {
                "tags": [
                    "Company"
                ],
                "summary": "View available API credits",
                "description": "Total API credits available",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Authorization token provided by Numadic. Append token with \"bearer \"",
                        "name": "Authorization",
                        "in": "header",
                        "required": true
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "Success response",
                        "schema": {
                            "type": "object",
                            "properties": {
                                "credit": {
                                    "type": "number"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/client/v1/lanes/speed": {
            "get": {
                "description": "To view speed between two consicutive toll plazas. Output will be given in percentile distribution.",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Lanes"
                ],
                "summary": "To view speed between two consicutive toll plazas.",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Authorization token provided by Numadic. Append token with \"bearer \"",
                        "name": "Authorization",
                        "in": "header",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "origin plaza ID",
                        "name": "origin_toll_id",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "destination plaza ID",
                        "name": "destination_toll_id",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "time in format HH:MM:SS for which the speed percentile is required",
                        "name": "time",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "$ref": "#/responses/getLaneSpeed"
                    },
                    "400": {
                        "description": "Invalid Parameter"
                    },
                    "500": {
                        "description": "Internal server error"
                    },
                    "default": {
                        "description": "Unexpected Error"
                    }
                }
            }
        },
        "/client/v1/lanes/duration": {
            "get": {
                "description": "To view time taken(duration) between two consicutive toll plazas. Output will be given in percentile distribution.",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Lanes"
                ],
                "summary": "To view the time taken(duration) between two consicutive toll plazas.",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Authorization token provided by Numadic. Append token with \"bearer \"",
                        "name": "Authorization",
                        "in": "header",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "origin plaza ID",
                        "name": "origin_toll_id",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "destination plaza ID",
                        "name": "destination_toll_id",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "time in format HH:MM:SS for which the duration percentile is required",
                        "name": "time",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "$ref": "#/responses/getLaneDuration"
                    },
                    "400": {
                        "description": "Invalid Parameter"
                    },
                    "500": {
                        "description": "Internal server error"
                    },
                    "default": {
                        "description": "Unexpected Error"
                    }
                }
            }
        },
        "/client/v1/lanes/traffic": {
            "get": {
                "description": "To view deviation in traffic from median between two consicutive toll plazas.",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Lanes"
                ],
                "summary": "To view deviation in traffic from median between two consicutive toll plazas",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Authorization token provided by Numadic. Append token with \"bearer \"",
                        "name": "Authorization",
                        "in": "header",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "origin plaza ID",
                        "name": "origin_toll_id",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "destination plaza ID",
                        "name": "destination_toll_id",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "time in format HH:MM:SS for which the deviation in traffic is required",
                        "name": "time",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "$ref": "#/responses/getLaneTraffic"
                    },
                    "400": {
                        "description": "Invalid Parameter"
                    },
                    "500": {
                        "description": "Internal server error"
                    },
                    "default": {
                        "description": "Unexpected Error"
                    }
                }
            }
        }
    },
    "definitions": {
        "locationRequestParams": {
            "type": "object",
            "required": [
                "VehicleNumber"
            ],
            "properties": {
                "VehicleNumber": {
                    "description": "GA64HW6784",
                    "type": "string"
                },
                "StartTripDate": {
                    "description": "2020-03-21T04:27:39+05:30",
                    "type": "string"
                },
                "EndTripDate": {
                    "description": "2020-03-23T04:29:25.777+05:30",
                    "type": "string"
                }
            }
        },
        "location": {
            "type": "object",
            "properties": {
                "TransactionDateTime": {
                    "description": "2020-03-23T04:27:39+05:30",
                    "type": "string"
                },
                "LaneCode": {
                    "description": "10",
                    "type": "string"
                },
                "PlazaCode": {
                    "description": "205001",
                    "type": "string"
                },
                "PlazaName": {
                    "description": "Jaladhulagori toll plaza",
                    "type": "string"
                },
                "Lat": {
                    "description": "22.5732292379649",
                    "type": "number"
                },
                "Lng": {
                    "description": "88.1816278106244",
                    "type": "number"
                },
                "Address": {
                    "description": "Jaladhulagori (Dhulagarh) Ashoka Dhankuni Kharagpur Tollway Pvt Ltd.",
                    "type": "string"
                }
            }
        },
        "listOfLocations": {
            "type": "object",
            "properties": {
                "data": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/location"
                    }
                }
            }
        },
        "LaneSpeed": {
            "type": "object",
            "properties": {
                "origin_toll_id": {
                    "description": "Origin Toll Plaza ID",
                    "type": "string"
                },
                "destination_toll_id": {
                    "description": "Destination Toll Plaza ID",
                    "type": "string"
                },
                "percentile_25": {
                    "type": "string"
                },
                "percentile_50": {
                    "type": "string"
                },
                "percentile_75": {
                    "type": "string"
                },
                "percentile_90": {
                    "type": "string"
                },
                "hour": {
                    "type": "string"
                }
            }
        },
        "LaneDuration": {
            "type": "object",
            "properties": {
                "origin_toll_id": {
                    "description": "Origin Toll Plaza ID",
                    "type": "string"
                },
                "destination_toll_id": {
                    "description": "Destination Toll Plaza ID",
                    "type": "string"
                },
                "percentile_25": {
                    "type": "string"
                },
                "percentile_50": {
                    "type": "string"
                },
                "percentile_75": {
                    "type": "string"
                },
                "percentile_90": {
                    "type": "string"
                },
                "hour": {
                    "type": "string"
                }
            }
        },
        "LaneTraffic": {
            "type": "object",
            "properties": {
                "origin_toll_id": {
                    "description": "Origin Toll Plaza ID",
                    "type": "string"
                },
                "destination_toll_id": {
                    "description": "Destination Toll Plaza ID",
                    "type": "string"
                },
                "median_traffic": {
                    "type": "string"
                },
                "change_in_traffic": {
                    "type": "string"
                },
                "hour": {
                    "type": "string"
                }
            }
        }
    },
    "responses": {
        "locationsData": {
            "description": "Success response",
            "schema": {
                "$ref": "#/definitions/listOfLocations"
            }
        },
        "getLaneSpeed": {
            "description": "The success Response after calling /client/v1/lanes-speed API Get\nstatus Code 200",
            "schema": {
                "$ref": "#/definitions/LaneSpeed"
            }
        },
        "getLaneDuration": {
            "description": "The success Response after calling /client/v1/lanes-duration API Get\nstatus Code 200",
            "schema": {
                "$ref": "#/definitions/LaneDuration"
            }
        },
        "getLaneTraffic": {
            "description": "The success Response after calling /client/v1/lanes-traffic API Get\nstatus Code 200",
            "schema": {
                "$ref": "#/definitions/LaneTraffic"
            }
        }
    }
}