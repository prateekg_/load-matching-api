const nodemailer = require('nodemailer');
const env       = process.env.NODE_ENV || 'development';
const config    = require('../config/index')[env];
const mailerConfig = config.mailer;

exports.formatLicPlate = function (data) {
  if(data)return (data.replace(/[^a-zA-Z0-9]/g, "").toUpperCase())
};

exports.formatWithStartCase = function (data) {
  if(!data) {
    return null
  }
  return data.replace(/\b\w/g, function(l){ return l.toUpperCase() })
};

exports.sendEmail = function (user) {
  let transporter = nodemailer.createTransport(mailerConfig);
  let mailOptions = {
    from: 'noreply@numadic.com',
    to: user.email,
    bcc: 'noreply@numadic.com',
    subject: 'Welcome to the Numadic Platform',

    html: '<body style="color:#444;font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;padding:0 100px;width:800px">' +
        '<img src="cid:nu-email-header-logo"/>' +
        '<h1><b>Welcome to the Numadic Platform</b></h1>' +
        'Hello ' + user.name + ',' + '<br>' + '<br>' +
        'Thank you for trying our product! Numadic is a software platform that improves profitability with affordable, secure and easy to use Fleet Management.' +
        '<br>' +
        '<p>Your nubot is ready for action and your account is set up.</p>' + '<br>' +
        '<b>Your account details are:</b><br>' +
        'Email: ' + user.email + '<br>' +
        'Password: ' + user.password + '<br>' + '<br>' +
        '<b>Access the Numadic Platform here, if you haven\'t already:</b>' +
        '<p>https://app.numadic.com/</p>' +
        '<br>' +
        '<p>Please click this link to download the Numadic Fleet Tracking Application directly from the Playstore.</p>' +
        '<p><a href="http://nuws.co/av1">nuws.co/av1</a></p>' +
        '<br>' +
        'This is the first step towards safer, more efficient driving.' +
        '<br>' +
        '<p>Have a safe trip!</p>' +
        '</body>',
    attachments: [
      {
        filename: "nu-logo",
        path: './public/images/nu-email-header.png',
        cid: 'nu-email-header-logo'
      }]
  };
  transporter.sendMail(mailOptions, function () {
        
  });
};