const winston = require('winston');
const fs = require('fs');
const env = process.env.NODE_ENV || 'development';
const rabbit = require('../helpers/rabbitMQ');
const nodeUtil = require('util');
// lets keep the Module logs in the /var/ directory , so that we can track the logs easily
const logDir = './log';

// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir)
}

rabbit.createExchange('APP-LOGGER');

let CustomLogger = winston.transports.CustomLogger = function (options) {
  this.name = 'APP-LOGGER';
  this.level = options.level || 'info'
};

nodeUtil.inherits(CustomLogger, winston.Transport);

CustomLogger.prototype.log = function (level, msg, meta, callback) {
  rabbit.publishToAppLoggingQueue('APP-LOGGER', Buffer.from(JSON.stringify({
    level: level,
    meta: meta
  })));
  callback(null, true)
};

const tsFormat = (new Date()).toLocaleTimeString();

const logger = winston.createLogger({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      colorize: true,
      timestamp: tsFormat,
      level: 'error'
    }),
    /*
    // Output the logs to a file
    new (require('winston-daily-rotate-file'))({
      filename: logDir + '/-NU-METRICS.log',
      timestamp: tsFormat,
      datePattern: 'yyyy-MM-dd',
      json: true,
      prepend: true,
      exitOnError: false,
      level: env === 'development' ? 'debug' : 'info',
      colorize: true,
      maxDays: 10
    }),

    // Custom logger to publish to rabbitMQ
    new (winston.transports.CustomLogger)({})
    */
  ]
});

exports.formatMessage = function (req, err, statusCode) {
  req = req || {};
  return {
    app: 'nu-metrics',
    env: env,
    baseUrl: req.baseUrl,
    originalUrl: req.originalUrl,
    query: req.query,
    body: req.body,
    headers: req.headers,
    error: err,
    status_code: statusCode || 500
  }
};

exports.logger = logger;
