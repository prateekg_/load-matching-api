const moment = require('moment');
const _ = require('lodash');
const geolib = require('geolib');

let util = {};

util.timezone = { default: 'Asia/Kolkata' };

util.convertGroupIdsToTuple = function (ids) {
  let x = typeof ids === "string" ? ids : JSON.stringify(ids);
  return ids.length > 0 ? "(" + x.substring(1, x.length - 1) + ")" : null
};

util.isInt = function (value) {
  return !isNaN(value) && (function (x) { return (x | 0) === x; })(parseFloat(value))
};

util.processMapBoundParams = function (req) {
  let bounds = null;
  let ne = req.query.ne;
  let sw = req.query.sw;

  if ((ne && (ne[0] && ne[0] > 0) && (ne[1] && ne[1] > 0)) &&
    (sw && (sw[0] && sw[0] > 0) && (sw[1] && sw[1] > 0))) {
    bounds = {
      north: ne[0],
      east: ne[1],
      south: sw[0],
      west: sw[1]
    }
  }
  return bounds
};

util.durationConverter = function (dur) {
  let days = moment.duration(dur, 'minutes').days(),
    hours = moment.duration(dur, 'minutes').hours(),
    minutes = moment.duration(dur, 'minutes').minutes(),
    value

  if (days) {
    value = `${days}d ${hours}h ${minutes}min`
  } else if (hours) {
    value = `${hours}h ${minutes}min`
  } else {
    value = `${minutes}min`
  }

  return value
}
/**
 * @return {string}
 */
util.FormatMinutesDHM = function (durationMin) {

  let d, h, m, s, ms, time = "";
  ms = durationMin * 1000;
  s = Math.floor(ms / 1000);
  m = Math.floor(s / 60);
  s = s % 60;
  h = Math.floor(m / 60);
  m = m % 60;
  d = Math.floor(h / 24);
  h = h % 24;

  this.days = d;
  this.hours = h;
  this.minutes = m;

  if (d > 0) {
    time += d + "d:"
  }
  if (h > 0) {
    time += h + "h:"
  }
  time += m + "m";
  return time
};

/**
 * Returns date with Today,Yesterday or with date format
 * @param {Integer} datetimeUnix - Epoch datetime
 * @return {string}
 */
util.DatetimeNoYearFormat = function (datetimeUnix) {
  let datetimeFormat = "";
  let yesterday = 0;
  yesterday = moment().subtract(1, 'days').startOf('day');
  if (moment(datetimeUnix).isSame(moment(), 'day')) {
    datetimeFormat = moment(datetimeUnix).format('[Today], h:mma');
  } else if (moment(datetimeUnix).isSame(yesterday, 'day')) {
    datetimeFormat = moment(datetimeUnix).format('[Yesterday], h:mma');
  } else if (moment(datetimeUnix).isBefore(yesterday, 'day')) {
    datetimeFormat = moment(datetimeUnix).format('D MMM YYYY, h:mma');
  } else if (moment(datetimeUnix).isAfter(moment(), 'day')) {
    datetimeFormat = moment(datetimeUnix).format('D MMM YYYY, h:mma');
  }
  return datetimeFormat;
};

util.downloadReportDate = function () {
  return moment().format('YYYY-MM-DD-HH:mm');
};

/**
 * @return {string}
 */
util.EOLDateTimeFormat = function (tis) {
  return tis && tis > 0 ? moment.unix(tis).tz(util.timezone.default).format('Do MMM YY h:mma') : '-'
};

/**
 * @return {string}
 */
util.EOLDateFormat = function (tis) {
  return tis && tis > 0 ? moment.unix(tis).tz(util.timezone.default).format('Do-MMM-YY') : '-'
};

/**
 * @return {string}
 */
util.EOLTimeFormat = function (tis) {
  return tis && tis > 0 ? moment.unix(tis).tz(util.timezone.default).format('h:mm:ssa') : '-'
};


function utilDurationDHM(durationMin) {
  let milliseconds = 0;
  let days = 0;
  let hours = 0;
  let minutes = 0;
  let duration = 0;
  let durationItem = {};

  durationMin = ((durationMin) ? Math.abs(durationMin) : 0);
  milliseconds = (Math.floor(durationMin) * 60 * 1000);
  duration = moment.duration(milliseconds);
  days = Math.floor(duration.asDays());
  hours = Math.floor(duration.asHours() % 24);
  minutes = Math.floor(duration.asMinutes() % 60);

  durationItem = {
    days: days,
    hours: hours,
    minutes: minutes
  };

  return durationItem;
}

util.formatDuration = function (durationMin) {
  let durationItem = {};
  let durationFormat = "";

  durationItem = utilDurationDHM(durationMin);
  if (durationItem.days > 0) {
    durationFormat = durationItem.days + "d" + ((durationItem.hours > 0) ? " " + durationItem.hours + "h" : " 0h");
  } else {
    durationFormat = ((durationItem.hours > 0) ? durationItem.hours + "h" : "") +
      ((durationItem.minutes > 0) ? " " + durationItem.minutes + "m" : "");
  }
  durationFormat = durationFormat.trim();
  if (durationFormat === "") {
    durationFormat = "0m";
  }

  return durationFormat;
};

/**
 * Generic function to validate inputPayload against minimum RequiredKeys
 * the passed keys should contain minimum required payload keys
 * @param PayloadMinRequiredKeys
 * @param Payload
 * @returns {boolean}
 */
util.validateMinPayload = function (PayloadMinRequiredKeys, Payload) {
  //Fetch all keys where value is not null
  let FilteredPayload = _.omitBy(Payload, _.isNull);
  FilteredPayload = _.omitBy(FilteredPayload, _.isUndefined);

  let payloadkeys = _.keys(FilteredPayload);
  let requiredObjects = _.pick(Payload, PayloadMinRequiredKeys);
  if (util.ifEmptyObjectValues(requiredObjects)) {
    return false
  } else //find the intersection of the received payload keys and the minimum required keys, if the union is equal to the length of the minimum required keys then it means that all the keys are satisfied
    return _.isEqual(_.intersection(payloadkeys, PayloadMinRequiredKeys).length, PayloadMinRequiredKeys.length);
};

util.ifEmptyObjectValues = function (object) {
  let empty = true;
  for (let key in object) {
    if (object[key] !== null && object[key] !== "" || object[key] === 0)
      empty = false;
    else {
      return empty = true
    }
  }
  return empty;
};

/**
 * @description Processes table column search requested by client
 * @param search_cols
 * @param DB_KEYS Array of client and database table keys
 * @returns {string}
 */
util.processTableColumnSearch = function (search_cols, DB_KEYS) {
  DB_KEYS = DB_KEYS && DB_KEYS.length > 0 ? DB_KEYS : [];
  let search_col_condition = "";
  try {
    search_cols = typeof search_cols === 'string' ? [search_cols] : search_cols;
    search_cols && search_cols.forEach(function (i) {
      let col_name = i.split("|")[0];
      let value = i.split("|")[1];
      value = value.toLowerCase();
      let matched = _.find(DB_KEYS, { client: col_name });

      if (matched) {
        if (search_col_condition.length > 0) {
          search_col_condition += " AND lower(" + matched.db + "::TEXT) like '%" + value + "%'"
        } else {
          search_col_condition += " lower(" + matched.db + "::TEXT) like '%" + value + "%'";
        }
      }
    });
    search_col_condition = search_cols && search_col_condition.length > 0 ? " AND ( " + search_col_condition + " ) " : ''
  } catch (e) {
    console.warn(e)
  }
  return search_col_condition
};

/**
 * @description Processes table column sort requested by client
 * @param sort_param eg. lic_plate_no|asc
 * @param DB_SORT_KEYS Array of client and database table keys
 * @param default_condition
 * @returns {string}
 */
util.processTableColumnSort = function (sort_param, DB_SORT_KEYS, default_condition) {
  DB_SORT_KEYS = DB_SORT_KEYS && DB_SORT_KEYS.length > 0 ? DB_SORT_KEYS : [];
  default_condition = default_condition ? default_condition : "";
  let sort_condition = "";
  if (sort_param) {
    const ORDER_TYPE = ['asc', 'desc'];
    try {
      sort_param = typeof sort_param === 'string' ? [sort_param] : sort_param;
      sort_param && sort_param.forEach(function (i) {
        let key = null, order = null, db_key = null;
        key = i.split("|")[0];
        order = i.split("|")[1];
        let matched = _.find(DB_SORT_KEYS, { client: key });
        db_key = matched ? matched.db : null;
        order = order && ORDER_TYPE.indexOf(order) > -1 ? order : null;
        if (db_key && order) {
          if (sort_condition.length > 0) {
            sort_condition += `, ${db_key} ${order}`
          } else {
            sort_condition = ` ORDER BY ${db_key} ${order} NULLS LAST `
          }
        }
      })
    } catch (e) {
      console.error("processTableColumnSort() : ", e)
    }
  } else {
    sort_condition = default_condition
  }

  return sort_condition
};

/**
 * @description Apply range filters on column .. eg. date range filter
 * @param range_cols eg. trip.start_tis|1529884800-1529971199
 * @param DB_RANGE_KEYS Array of client and database table keys
 * @param limit number of days
 * @returns {{err: *, result: string}}
 */
util.processTableDateRangeFilter = function (range_cols, DB_RANGE_KEYS, limit) {
  DB_RANGE_KEYS = DB_RANGE_KEYS && DB_RANGE_KEYS.length > 0 ? DB_RANGE_KEYS : [];
  let range_filter_condition = "";
  let err = null;
  if (range_cols) {
    try {
      range_cols = typeof range_cols === 'string' ? [range_cols] : range_cols;
      range_cols && range_cols.forEach(function (i) {
        const col_name = i.split("|")[0];
        const value = i.split("|")[1];
        const from = Number(value.split("-")[0]);
        const fromTis = moment.unix(from);
        const to = Number(value.split("-")[1]);
        const toTis = moment.unix(to);
        const matched = _.find(DB_RANGE_KEYS, { client: col_name });
        if (limit && (toTis.diff(fromTis, 'days') <= limit)) {
          if (matched && from && to) {
            if (range_filter_condition.length > 0) {
              range_filter_condition += ` AND ${matched.db} BETWEEN ${from} AND ${to}`
            } else {
              range_filter_condition += ` ${matched.db} BETWEEN ${from} AND ${to}`
            }
          }
        } else {
          err = { code: 400, msg: "Time range bound exceeded" }
        }
      });
      range_filter_condition = range_cols && range_filter_condition ? ` AND ( ${range_filter_condition} )` : ""
    } catch (e) {
      console.log("ERROR : util.processTableDateRangeFilter() ", e)
    }
  }
  return { err: err, result: range_filter_condition }
};

util.utilDurationFormatMDHM = function (durationMin) {
  let units = {
    "Y": 24 * 60 * 365,
    "M": 24 * 60 * 30,
    "d": 24 * 60,
    "h": 60,
    "m": 1
  };
  let result = "";
  for (let name in units) {
    let p = Math.floor(durationMin / units[name]);
    if (p >= 1) result = result.concat(p + name + " ");
    durationMin %= units[name]
  }
  return result;
};

util.convertDistanceMetersToKm = function (meters) {
  return Number((meters / 1000).toFixed(1))
};

util.getAssetConditions = function (fk_asset_id, assetType) {
  let assetCondition = " ";
  if (Number(fk_asset_id)) {
    assetCondition = " and a.id =" + fk_asset_id.toString();
  } else if (assetType) {
    switch (assetType) {
      case "truck":
        assetCondition = " and a.fk_ast_type_id =1";
        break;
      case "van":
        assetCondition = " and a.fk_ast_type_id =8";
        break;
      default:
        assetCondition = "";
        break;
    }
  }
  return assetCondition;
};

util.validateEstimatedPolyline = function (geo_trip_route) {
  return geo_trip_route && geo_trip_route.paths ? geo_trip_route.paths[0].points : ""
};

util.getPlatformFromURL = function (req) {
  const url = req.originalUrl;
  return _.startsWith(url, '/v2/consign/') ? 'consign' : _.startsWith(url, '/v2/fleet/') ? 'fleet' : 'app';
};

/**
 * Checks if object keys are null and returns null
 * @param {object} object
 */
function processNullObjectKeys(object) {
  if (_.size(_.compact(_.values(object)))) {
    return object
  } else {
    return null;
  }
}
util.processNullObjectKeys = processNullObjectKeys;

/**
 * Find if source and destination place is same
 * @param waypoints
 */
function isRoundTrip(waypoints) {
  // Check if source and destination POI ID is same or lat, lng are within 50 meters
  let isRoundTrip = false;

  try {
    if (waypoints.length >= 2) {
      let source = waypoints[0], destination = waypoints[waypoints.length - 1];
      if (source.fk_poi_id && destination.fk_poi_id && source.fk_poi_id === destination.fk_poi_id) {
        isRoundTrip = true
      } else if (source.poiId && destination.poiId && source.poiId === destination.poiId) {
        isRoundTrip = true
      } else if (geolib.isPointInCircle({
        latitude: source.lat,
        longitude: source.lon
      }, {
        latitude: destination.lat,
        longitude: destination.lon
      }, 50)) {
        isRoundTrip = true
      }
    }
  } catch (e) {
    console.error(e)
  }
  return isRoundTrip
}
util.isRoundTrip = isRoundTrip;

util.processAssetStatus = function (asset) {
  let status = "N/A";

  if (asset) {
    if (asset.ign === 'B' && asset.spd > 0) {
      status = "moving"
    } else if (asset.ign === 'B' && asset.spd === 0) {
      status = "idling"
    } else if (asset.ign === 'A') {
      status = "stationary"
    }
  }
  return status
};

util.calculateBatteryPercentage = function (record, nubot) {
  if (typeof nubot.internal_battery_percentage === 'number') {
    return nubot.internal_battery_percentage;
  }
  if (typeof nubot.b2 === 'number' && record.max_battery_voltage && record.min_battery_voltage) {
    let diffNubotBatVoltage = record.max_battery_voltage - record.min_battery_voltage;
    return Math.min(100, Math.max(0, Math.floor(((nubot.b2 - record.min_battery_voltage) * 100) / diffNubotBatVoltage)));
  }
  return null;
};

/**
 * Clean driver object, convert object to array and remove null objects
 * @param drivers
 */
util.cleanDriverObject = function (drivers) {
  let processed_object = [];
  if (drivers) {
    if (typeof drivers === 'string') {
      try {
        let driver_obj = JSON.parse(drivers);
        Array.isArray(driver_obj) && driver_obj.forEach(function (i) {
          i = typeof i === 'string' ? JSON.parse(i) : i;
          let val = processNullObjectKeys(i);
          if (val) {
            processed_object.push(val)
          }
        })
      } catch (e) {
        console.error("cleanDriverObject(): ", e)
      }
    }
  }
  return processed_object;
};

/**
 * Function to handle empty days.
 */
util.addNullDatesToTrendsStats = function (params, data) {
  let currentDate = moment.unix(params.start_tis), end_date = moment.unix(params.end_tis);
  data = data ? data : [];
  while (currentDate.isBefore(end_date)) {
    let dayStart = currentDate.clone();
    let dayTis = dayStart.startOf('day').unix();
    if (!_.find(data, { tis: dayTis })) {
      let x = {
        date: dayStart.format('D MMM YYYY'),
        tis: dayTis
      };
      x.value = 0;
      data.push(x)
    }
    currentDate.add(1, 'day');
  }
  return data
};

/**
 * @author Haston Silva
 * @param userData
 * @returns {Array}
 * @description Merges user roles enabled features comparing with companies feature plans
 */
util.processUserRoleFeatures = function (userData) {

  if (_.includes(userData.roles, 'system-admin') || userData.is_admin) {
    return _.uniq(userData.features)
  }

  var final_feature_list = []
  userData && userData.role_features.forEach(function (i) {
    i.featureList && i.featureList.forEach(function (j) {
      if (j.enabled && userData.features.indexOf(j.feature) > -1) { final_feature_list.push(j.feature) }
    })
  })
  return _.uniq(final_feature_list)
}

module.exports = util;