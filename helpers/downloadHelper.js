let downloadHelper = {};
const Excel = require('exceljs');
const moment = require('moment');
const _ = require('lodash');
const fs = require('fs');
const CHAR_LABEL = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
const date_time_format = 'DD-MM-YY, HH:mm';
const utils = require('./util');

downloadHelper.convertTableV1ToXLS = function(status, data, file_base_path, filename, user_columns, callback) {
  try {

    let workbook = new Excel.Workbook();
    let ws = workbook.addWorksheet('List');
    ws.font = {name: 'Proxima Nova'};
    let freezeRows = 1;

    // Remove selected: false columns
    user_columns = _.filter(user_columns, 'selected');

    // Sort columns
    user_columns = _.sortBy(user_columns, ['order']);

    // Added header sections
    let sections = _.groupBy(user_columns, 'group_key');

    if(sections.length > 0){
      freezeRows = 2;
      let current_col = 0;
      _.forEach(sections, function (value, key) {
        let w = value.length - 1;
        let merge_cells = `1${CHAR_LABEL[current_col]}:1${CHAR_LABEL[w + current_col]}`;
        ws.mergeCells(merge_cells);
        let current_cell = CHAR_LABEL[current_col] + '1';
        ws.getCell(current_cell).value = key.toLocaleUpperCase();
        ws.getCell(current_cell).alignment = { horizontal: 'center' };
        current_col += w+1
      });
      ws.getRow(ws.rowCount).font = {size: 12, bold: true};
    }

    // Add column headers
    let column_headers = [];
    let column_widths = [];
    user_columns.forEach(function (i) {
      let col_header = i.name.toLocaleUpperCase();
      col_header += i.unit ? " ("+ i.unit +")" : '';

      if(i.selected){
        column_headers.push(col_header);
        column_widths.push({width: 20})
      }
    });

    ws.columns = column_widths;
    ws.addRow(column_headers);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    // Add data
    data.forEach(function (i) {
      let row = [];

      user_columns.forEach(function (col) {
        if(col.selected){
          try {
            let value;
            let nested_keys = col.key.split('.');

            if (nested_keys.length > 1) {
              if(i[nested_keys[0]] && i[nested_keys[0]][nested_keys[1]]){
                value = i[nested_keys[0]][nested_keys[1]]
              }
            } else {
              value = i[col.key]
            }

            switch (col.format) {
            case 'datetime':
              value = Number(value) ? moment.unix(value).tz(utils.timezone.default, false).format(date_time_format) : '-';
              break;
            case 'duration':
              value = utils.utilDurationFormatMDHM(value);
              break;
            case 'distance':
              value = utils.convertDistanceMetersToKm(value);
              break;
            }

            row.push(value)
          }catch(e){
            console.log("ERROR processing row data : ", e)
          }
        }
      });

      ws.addRow(row)
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: freezeRows}];

    // write to excel file
    if(!fs.existsSync(file_base_path)){
      fs.mkdirSync(file_base_path)
    }

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, data)
      }).catch(function () {
        callback({
          code: 500,
          msg: 'Internal error'
        })
      });
  }catch(e){
    console.log("XLS ERROR : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
};

downloadHelper.convertConsignmentListToXLS = function(consignment_status, data, file_base_path, filename, user_columns, callback) {
  try {

    let workbook = new Excel.Workbook();
    let ws = workbook.addWorksheet('Consignment list');
    ws.font = {name: 'Proxima Nova'};

    // Remove selected: false columns
    user_columns = _.filter(user_columns, 'selected');

    // Sort columns
    user_columns = _.sortBy(user_columns, ['order']);

    // Added header sections
    let sections = _.groupBy(user_columns, 'group_key');

    let current_col = 0;
    _.forEach(sections, function (value, key) {
      let w = value.length - 1;
      let merge_cells = `1${CHAR_LABEL[current_col]}:1${CHAR_LABEL[w + current_col]}`;
      ws.mergeCells(merge_cells);

      let current_cell = CHAR_LABEL[current_col] + '1';

      ws.getCell(current_cell).value = key.toLocaleUpperCase();
      ws.getCell(current_cell).alignment = { horizontal: 'center' };

      current_col += w+1
    });
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    // Add column headers
    let column_headers = [];
    let column_widths = [];
    user_columns.forEach(function (i) {
      let col_header = i.name.toLocaleUpperCase();
      col_header += i.unit ? " ("+ i.unit +")" : '';

      if(i.selected){
        column_headers.push(col_header);
        column_widths.push({width: 20})
      }
    });

    ws.columns = column_widths;
    ws.addRow(column_headers);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    // Add data
    data.forEach(function (i) {
      let row = [];

      user_columns.forEach(function (col) {
        if(col.selected){
          try {
            let value;
            let nested_keys = col.key.split('.');

            if (nested_keys.length > 1) {
              if(i[nested_keys[0]] && i[nested_keys[0]][nested_keys[1]]){
                value = i[nested_keys[0]][nested_keys[1]]
              }
            } else {
              value = i[col.key]
            }

            switch (col.format) {
            case 'datetime':
              value = Number(value) ? moment.unix(value).tz(utils.timezone.default, false).format(date_time_format) : '-';
              break;
            case 'duration':
              value = utils.utilDurationFormatMDHM(value);
              break;
            case 'distance':
              value = utils.convertDistanceMetersToKm(value);
              break;
            }

            row.push(value)
          }catch(e){
            console.log("ERROR processing row data : ", e)
          }
        }
      });

      ws.addRow(row)
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 2}];

    // write to excel file
    if(!fs.existsSync(file_base_path)){
      fs.mkdirSync(file_base_path)
    }

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, data)
      }).catch(function (err) {
        callback({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      });
  }catch(e){
    console.log("XLS ERROR : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
};

downloadHelper.convertAssetActivityReportToXLS = function(data, start_tis, end_tis, file_base_path, filename, callback){
  try {

    let workbook = new Excel.Workbook();
    let ws = workbook.addWorksheet('ALL-ASSETS');
    ws.font = {name: 'Proxima Nova'};

    ws.addRow(["ALL ASSETS REPORT"])
    ws.getRow(ws.rowCount).font = {size: 18, bold: true};
    
    ws.addRow([`${moment.unix(start_tis).format('MMM DD, YYYY hh:mm a')} - ${moment.unix(end_tis).format('MMM DD, YYYY hh:mm a')}`])
    ws.getRow(ws.rowCount).font = {size: 15, bold: true};

    ws.columns = [{width: 15},{width: 15},{width: 15},{width: 50},{width: 15},{width: 15},{width: 50},{width: 15},{width: 15},{width: 15},{width: 15},{width: 15},{width: 15},{width: 15},{width: 15}]

    ws.addRow([
      "License No",
      "Shift Start Date",
      "Shift Start Time",
      "Shift Start Location",
      "Shift End Date",
      "Shift End Time",
      "Shift End Location",
      "Duration",
      "Distance",
      "Engine Running Time",
      "Idling Time",
      "Stationary Time",
      "Average Speed",
      "Speed Violations",
      "Max Speed"]
    )
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};
    ws.getRow(3).fill = {
      type: 'pattern',
      pattern:'solid',
      fgColor:{ argb:'69a3e7' }
    }
    // Add data
    data.forEach(function (i) {
      ws.addRow([i.lic_plate_no,
        i.shift_start_date,
        i.shift_start_tis,
        i.shift_start_lname,
        i.shift_end_date,
        i.shift_end_tis,
        i.shift_end_lname,
        i.duration,
        i.tot_dist,
        i.tot_eng,
        i.tot_idle,
        i.tot_stat,
        i.avg_spd,
        i.tot_osf,
        i.max_spd])

      let rc = ws.rowCount
      for(let j = 1; j<=15; j++){
        let current_cell = CHAR_LABEL[j] + rc + '';
        ws.getCell(current_cell).alignment = { horizontal: 'right' };
      }
    });
    
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 3}];
    ws.autoFilter = 'A3:O3';
    ws.mergeCells('A1:O1');
    ws.mergeCells('A2:O2');

    // write to excel file
    if(!fs.existsSync(file_base_path)){
      fs.mkdirSync(file_base_path)
    }

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, data)
      }).catch(function (err) {
        callback({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      });
  }catch(e){
    console.log("XLS ERROR : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

module.exports = downloadHelper;