const models = require('../db/models');
const moment = require('moment');

function addToDB(data, callback) {
  models.group_log.create(data)
    .then(function () {
      callback()
    }).catch(function (err) {
      callback(err)
    })
}

exports.addGroup = function (fk_group_id, fk_u_id, payload, callback) {
  let data = {
    fk_group_id: fk_group_id,
    fk_by_u_id: fk_u_id,
    msg: " created group ",
    payload: payload,
    tis: moment().unix()
  };

  addToDB(data, callback)
};

exports.disableGroup = function (fk_group_id, fk_u_id, payload, callback) {
  let data = {
    fk_group_id: fk_group_id,
    fk_by_u_id: fk_u_id,
    msg: " disabled group ",
    payload: payload,
    tis: moment().unix()
  };

  addToDB(data, callback)
};

exports.addAsset = function (fk_group_id, fk_u_id, assets, payload, callback) {
  let data = {
    fk_by_u_id: fk_u_id,
    fk_asset_id: assets,
    fk_group_id: fk_group_id,
    msg: " added to ",
    payload: payload,
    tis: moment().unix()
  };

  addToDB(data, callback)
};

exports.disableAsset = function (fk_group_id, fk_u_id, fk_asset_id, payload, callback) {
  let data = {
    fk_by_u_id: fk_u_id,
    fk_asset_id: [fk_asset_id],
    fk_group_id: fk_group_id,
    msg: " removed from ",
    payload: payload,
    tis: moment().unix()
  };

  addToDB(data, callback)
};

exports.addPoi = function (fk_group_id, fk_u_id, pois, payload, callback) {
  let data = {
    fk_by_u_id: fk_u_id,
    fk_poi_id: pois,
    fk_group_id: fk_group_id,
    msg: " added to ",
    payload: payload,
    tis: moment().unix()
  };

  addToDB(data, callback)
};

exports.disablePoi = function (fk_group_id, fk_u_id, fk_poi_id, payload, callback) {
  let data = {
    fk_by_u_id: fk_u_id,
    fk_poi_id: [fk_poi_id],
    fk_group_id: fk_group_id,
    msg: " removed from ",
    payload: payload,
    tis: moment().unix()
  };

  addToDB(data, callback)
};

exports.addUser = function (fk_group_id, fk_u_id, users, payload, callback) {
  let data = {
    fk_by_u_id: fk_u_id,
    fk_u_id: users,
    fk_group_id: fk_group_id,
    msg: " added to ",
    payload: payload,
    tis: moment().unix()
  };

  addToDB(data, callback)
};

exports.disableUser = function (fk_group_id, fk_u_id, fk_delete_u_id, payload, callback) {
  let data = {
    fk_by_u_id: fk_u_id,
    fk_u_id: `{${fk_delete_u_id}}`,
    fk_group_id: fk_group_id,
    msg: " removed from ",
    payload: payload,
    tis: moment().unix()
  };

  addToDB(data, callback)
};

exports.enableUser = function (fk_group_id, fk_u_id, fk_enabled_u_id, payload, callback) {
  let data = {
    fk_by_u_id: fk_u_id,
    fk_u_id: [fk_enabled_u_id],
    fk_group_id: fk_group_id,
    msg: " enabled for ",
    payload: payload,
    tis: moment().unix()
  };

  addToDB(data, callback)
};