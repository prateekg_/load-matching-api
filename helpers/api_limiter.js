/**
 * @author Haston Silva
 * @description API limiter to limit consumption /sec /hour and /day with credit validation
 */

const models = require('../db/models');
const REDIS_DB = require('./nuRedis')

const REQ_INTERVAL_LIMIT = 6, UNITS_PER_HOUR_LIMIT = 10, UNITS_PER_DAY_LIMIT = 100, UNITS_PER_MONTH_LIMIT = 1000;

exports.VALIDATE = function (req, res, next) {
    const fk_c_id = req.headers['fk-c-id'] || req.headers.fk_c_id;
    REDIS_DB.getAPILimiterKeys(fk_c_id, function (err, data) {

        if (!data.credits || (data.credits && data.credits == 0)) {
            return res.status(401).send({ code: 402, msg: 'Total credits consumed' })
        }

        if (data.last_req) {
            return res.status(401).send({ code: 403, msg: `Try after ${REQ_INTERVAL_LIMIT} Seconds` })
        }

        if (data.consumed_current_hour && data.consumed_current_hour >= UNITS_PER_HOUR_LIMIT) {
            return res.status(401).send({ code: 403, msg: 'Limit exhausted for current hour' })
        }

        if (data.consumed_today && data.consumed_today >= UNITS_PER_DAY_LIMIT) {
            return res.status(401).send({ code: 403, msg: 'Limit exhausted for current day' })
        }

        if (data.consumed_month && data.consumed_month >= UNITS_PER_MONTH_LIMIT) {
            return res.status(401).send({ code: 403, msg: 'Limit exhausted for current month' })
        }

        next()
    })
};

exports.UPDATE = function (fk_c_id) {
    REDIS_DB.updateAPILimiterKeys(fk_c_id, function (err, result) {
        models.company_api_credit.update({ credit: result.credits }, {
            where: {
                fk_c_id: fk_c_id
            }
        }).spread(function (updated) {
            if (!updated) {
                models.company_api_credit.create({ fk_c_id: fk_c_id, credit: result.credits })
                    .then(function () {
                    }).catch(function () {
                    })
            }
        }).catch(function () {
        })
    })
}