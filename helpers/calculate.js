
const _ = require('lodash');
const geolib = require('geolib');
const moment = require('moment');
const request = require('request');

exports.getIdleTime = function (perDay, callback) {
  let idle = [];
  let idleMin = 0;
  let startTimeIdle = 0;
  let endTimeIdle = 0;

  _.forIn(perDay, function (locations) {
    let flag = true;
    _(locations).forEach(function (location) {
      //Calculate idle hours
      if(location.ign === 'B' && (location.spd === 0 || !location.spd)) {
        if(flag){
          startTimeIdle = location.tis;
          endTimeIdle = location.tis;							
        }
        let timeDiff = Math.abs(startTimeIdle - location.tis)/60;
        if(timeDiff > 5) {
          endTimeIdle = location.tis;
        }
        flag = false;
      } else {
        if(!flag) {
          let timeDiff = Math.abs(startTimeIdle - location.tis)/60;
          if(timeDiff > 5) {
            idle.push(timeDiff);
          }
        }
        flag = true;
      }					
    });
    if(!flag) idle.push(Math.abs(startTimeIdle - endTimeIdle)/60);
  });
  idleMin = _.sum(idle);
  callback(null, idleMin);
};

exports.getAverageIdleTime = function (data, callback) {
  // data = _.groupBy(data, 'asset_id');
  let startTimeIdle = 0;
  let endTimeIdle = 0;
  let totIdleTime = [];

  _.forIn(data, function (locations) {
    let idle = [];
    let flag = true;
    let sd = locations[0].tis;
    let ed = Math.round(Date.now()/1000);
    let days = Math.floor(Math.abs(sd - ed) / (3600 * 24));

    _(locations).forEach(function (location) {
      //Calculate idle hours
      if(location.ign === 'B' && (location.spd === 0 || !location.spd)) {
        if(flag){
          startTimeIdle = location.tis;
          endTimeIdle = location.tis;
        }
        let timeDiff = Math.abs(startTimeIdle - location.tis)/60;
        if(timeDiff > 5) {
          endTimeIdle = location.tis;
        }
        flag = false;
      } else {
        if(!flag) {
          let timeDiff = Math.abs(startTimeIdle - location.tis)/60;
          if(timeDiff > 5) {
            idle.push(timeDiff);
          }
        }
        flag = true;
      }
    });
    if(!flag) idle.push(Math.abs(startTimeIdle - endTimeIdle)/60);
    totIdleTime.push(_.sum(idle)/days);
  });
  callback(null, Math.round(_.sum(totIdleTime)) || 0);
};

exports.getStationaryTime = function (perDay, callback) {
  let stationaryMin = 0;
  let stationaryStartTime = 0;
  let stationaryEndTime = 0;
  let stationary = [];

  _.forIn(perDay, function (locations) {
    let flag = true;
    _(locations).forEach(function (location) {
      //Stationary time
      if(location.ign === 'A') {
        if(flag) {
          stationaryStartTime = location.tis;							
        }
        stationaryEndTime = location.tis;
				
        flag = false;
      } else {
        if(!flag) {
          stationary.push(Math.abs(stationaryStartTime - location.tis)/60);
        }
        flag = true;
      }
    });
    if(!flag) stationary.push(Math.abs(stationaryStartTime - stationaryEndTime)/60);
  });
  stationaryMin = _.sum(stationary);
  callback(null, stationaryMin)
};

exports.getAverageStationary = function (data, callback) {
  let statStartTime = 0;
  let statEndTime = 0;
  let totalStatTime = [];
  _.forIn(data, function (locations) {
    let flag = true;
    let sd = locations[0].tis;
    let ed = Math.round(Date.now()/1000);
    let days = Math.floor(Math.abs(sd - ed) / (3600 * 24));
    let stationary = [];
    locations.forEach(function (location) {
      if (location.ign === 'A') {
        if (flag) {
          statStartTime = location.tis;
        }
        statEndTime = location.tis;
        flag = false;
      } else {
        if (!flag) {
          stationary.push(Math.abs(statStartTime - location.tis) / 60);
        }
        flag = true;
      }
    });
    if (!flag) stationary.push(Math.abs(statStartTime - statEndTime) / 60);
    totalStatTime.push(_.sum(stationary) / days);
  });
  callback(null, _.sum(totalStatTime) || 0);
};

exports.getFleetwideTime = function (data, callback) {
  let fleetwideStartTime = 0;
  let fleetwideEndTime = 0;
  let fleetwide = [];
  let fleetwideMin = 0;

  _.forIn(data, function (locations) {
    let flag = true;
    _(locations).forEach(function (location) {
      if(location.ign === 'B') {
        if(flag) {
          fleetwideStartTime = location.tis;
        }
        fleetwideEndTime = location.tis;
				
        flag = false;
      } else {
        if(!flag){
          fleetwide.push(Math.abs(fleetwideStartTime - location.tis)/60);
        }
        flag = true;
      }
    });
    if(!flag) fleetwide.push(Math.abs(fleetwideStartTime - fleetwideEndTime)/60);
  });
  fleetwideMin = _.sum(fleetwide);
  callback(null, fleetwideMin);
};

exports.getAverageFleetwide = function (data, callback) {
  let fleetwideStartTime = 0;
  let fleetwideEndTime = 0;
  let totalEngTime = [];
  _.forIn(data, function (locations) {
    let flag = true;
    let sd = locations[0].tis;
    let ed = Math.round(Date.now()/1000);
    let days = Math.floor(Math.abs(sd - ed) / (3600 * 24));
    let fleetwide = [];
    locations.forEach(function (location) {
      if (location.ign === 'B') {
        if (flag) {
          fleetwideStartTime = location.tis;
        }
        fleetwideEndTime = location.tis;
        flag = false;
      } else {
        if (!flag) {
          fleetwide.push(Math.abs(fleetwideStartTime - location.tis) / 60);
        }
        flag = true;
      }
    });
    if (!flag) fleetwide.push(Math.abs(fleetwideStartTime - fleetwideEndTime) / 60);
    totalEngTime.push(_.sum(fleetwide) / days);
  });
  callback(null, _.sum(totalEngTime) || 0);
};

exports.getAssetInfo = function (data, callback) {
  let moving = 0;
  let stationary = 0;
  let totalAssets = 0;
  let fleetUtilization = 0;

  _.each(data, function (location) {
    if(location.ign === 'B') {
      moving++;
    } else {
      stationary++;
    }
  });

  totalAssets = moving + stationary;
  fleetUtilization = Math.round((moving * 100)/totalAssets) || 0;
  callback(null, {
    moving : moving,
    stationary : stationary,
    total : totalAssets,
    utilRate : fleetUtilization
  });
};

exports.getDistance = function (data, callback) {
  let dist = 0;
  let distSeg = [];

  _.forIn(data, function (locations) {
    let flag = true;
    _.each(locations, function (location) {
      if(location.ign === 'B') {
        distSeg.push({
          latitude : location.lat,
          longitude : location.lon
        });
        flag = false;
      } else {						
        if(!flag){
          distSeg.push({
            latitude : location.lat,
            longitude : location.lon
          });
          let distance = geolib.getPathLength(distSeg);
          distSeg = [];
          dist = dist + distance/1000;
        }
        flag = true;
      }
    });
    if(!flag){
      let distance = (geolib.getPathLength(distSeg));
      dist = dist + distance/1000;
      distSeg = [];
    }
  });

  // distTotal = Math.round(_.sum(dist));
  callback(null, Number(dist.toFixed(1)));
};

exports.getMapMatchedDistance = function (fk_asset_id, start_tis, end_tis, callback) {
  if(fk_asset_id && start_tis && end_tis){
    let options = {
      method: 'POST',
      url: 'http://172.31.7.251:9006/match/trip/segment',
      json: true,
      body: {
        "stis": start_tis,
        "etis": end_tis,
        "fk_asset_id": fk_asset_id
      }
    };

    request(options, function (error, response, body) {
      let distance = 0;
      if (response && response.statusCode === 200) {
        distance = (body.response.distance) / 1000
      }
      return callback(null, distance)
    })
  }else{
    return callback(null, 0)
  }
};

exports.getAverageDistance = function (data, callback) {
  let totalAssetAvg = [];
  _.forIn(data, function (locations) {
    let flag = true;
    let sd = locations[0].tis;
    let ed = Math.round(Date.now()/1000);
    let days = Math.floor(Math.abs(sd - ed) / (3600 * 24));
    let distSeg = [];
    let dist = 0;
    _.each(locations, function (location) {
      if (location.ign === 'B') {
        distSeg.push({
          latitude: location.lat,
          longitude: location.lon
        });
        flag = false;
      } else {
        if (!flag) {
          distSeg.push({
            latitude: location.lat,
            longitude: location.lon
          });
          let distance = geolib.getPathLength(distSeg);
          distSeg = [];
          dist = dist + (distance / 1000);
        }
        flag = true;
      }
    });
    if (!flag) {
      let distance = geolib.getPathLength(distSeg);
      dist = dist + (distance / 1000);
      distSeg = [];
    }
    totalAssetAvg.push((dist / days));
  });
  callback(null, _.sum(totalAssetAvg) || 0);
};

exports.getSpeedViolation = function (data, callback) {
  let totSpdVio = 0;
  _.forIn(data, function (locations) {
    _(locations).forEach(function (location) {
      if(location.osf === true) {
        totSpdVio ++;
      }
    });
  });
  callback(null, totSpdVio);
};

exports.getAverageSpeedViolation = function (data, callback) {
  let totSpdVio = [];
  let spdVio = 0;
  _.forIn(data, function (locations) {
    // count++;
    let sd = locations[0].tis;
    let ed = Math.round(Date.now()/1000);
    let days = Math.floor(Math.abs(sd - ed) / (3600 * 24));
    _(locations).forEach(function (location) {
      if(location.osf === true) {
        spdVio ++;
      }
    });
    totSpdVio.push(spdVio/days);
  });
  totSpdVio = Math.round(_.sum(totSpdVio)/totSpdVio.length) || 0;
  callback(null, totSpdVio);
};

exports.getAverageAssetsPerDay = function (data, callback) {
  let count = 0;
  if(data.length === 0) {
    return callback(null, count);
  }
  let sd = data[0].timestamp;
  let ed = Math.round(Date.now()/1000);
  let days = Math.floor(Math.abs(sd - ed) / (3600 * 24));

  data = _.groupBy(data, function (item) {
    return moment.unix(item.timestamp).format("MMM Do YY");
  });

  _.each(data, function (value) {
    count +=value.length;
  });
  count = Math.round(count/days) || 0;
  callback(null, count);

};

exports.getAssetPoiDwellTime = function (data, callback) {
  data = _.groupBy(data, 'fk_asset_id');
  let dwellTime = 0;

  let currentTimestamp = Math.floor(Date.now() / 1000);
  let dayTimestamp = currentTimestamp -(1440 * 60);

  _.forIn(data, function (asset) {
    let entryTime;
    let exitTime;

    _.each(asset, function (location, index) {
      if(location.poi_stat === 0) {
        entryTime = location.timestamp;
        exitTime = currentTimestamp;

        if(index === asset.length-1) {
          dwellTime += Math.round(Math.abs(exitTime - entryTime)/60);
        }

      } else if (location.poi_stat === 2) {
        exitTime = location.timestamp;

        if(!entryTime) {
          entryTime = dayTimestamp;
        }
        dwellTime += Math.round(Math.abs(exitTime - entryTime)/60);

        entryTime = null;
        exitTime = null;

      } else if(location.poi_stat === 1 && asset.length-1 === index) {

        if(!entryTime) {
          entryTime = dayTimestamp;
        }

        exitTime = currentTimestamp;
        dwellTime += Math.round(Math.abs(exitTime - entryTime)/60);

        entryTime = null;
        exitTime = null;
      }
    });
  });

  callback(null, dwellTime);
};

exports.getAverageAssetPoiDwellTime = function (data, callback) {
  data = _.groupBy(data, 'fk_asset_id');
  let time = [];

  let currentTimestamp = Math.floor(Date.now() / 1000);
  let dayTimestamp = currentTimestamp -(1440 * 60);

  _.forIn(data, function (asset) {
    let entryTime;
    let exitTime;
    let dwellTime = 0;
    let days = 30;

    _.each(asset, function (location, index) {
      if(location.poi_stat === 0) {
        entryTime = location.timestamp;
        exitTime = currentTimestamp;
        if(index === asset.length-1) {
          dwellTime += Math.round(Math.abs(exitTime - entryTime)/60);
        }

      } else if (location.poi_stat === 2) {
        exitTime = location.timestamp;

        if(!entryTime) {
          entryTime = dayTimestamp;
        }
        dwellTime += Math.round(Math.abs(exitTime - entryTime)/60);
        entryTime = null;
        exitTime = null;

      } else if(location.poi_stat === 1 && asset.length-1 === index) {

        if(!entryTime) {
          entryTime = dayTimestamp;
        }

        exitTime = currentTimestamp;
        dwellTime += Math.round(Math.abs(exitTime - entryTime)/60);
        entryTime = null;
        exitTime = null;
      }
    });
    time.push(dwellTime/days);
  });
  time = Math.round(_.sum(time)) || 0;
  callback(null, time);
};

exports.getFleetUtilization = function (data, callback) {
  let moving = [];
  let distSeg = [];

  _.forIn(data, function (locations, deviceId) {
    let flag = true;
    let dist = 0;
    _.each(locations, function (location) {
      if(location.ign === 'B') {
        distSeg.push({
          latitude : location.lat,
          longitude : location.lon
        });
        flag = false;
      } else {
        if(!flag){
          distSeg.push({
            latitude : location.lat,
            longitude : location.lon
          });
          let distance = geolib.getPathLength(distSeg);
          distSeg = [];
          dist = dist + distance/1000;
        }
        flag = true;
      }
    });
    if(!flag){
      let distance = (geolib.getPathLength(distSeg));
      dist = dist + distance/1000;
      distSeg = [];
    }

    if(dist >= 15 && moving.indexOf(deviceId) === -1) {
      moving.push(deviceId);
    }
  });

  callback(null, moving.length || 0);
};