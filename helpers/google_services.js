const env = process.env.NODE_ENV || 'development';
const config = require('../config/index')[env];
const request = require('request');

exports.getGoogleETA = function(source, destination, callback) {

  let data = { distance: null, duration: null };

  let options = {
    uri : `https://maps.googleapis.com/maps/api/distancematrix/json?key=${config.googleMap.key}&units=metric&origins=${source.lat},${source.lon}&destinations=${destination.lat},${destination.lon}`,
    method : "GET"
  };

  request(options, function (error, response, body) {
    if (error) {
      console.error("Error when calling Google ETA API : ", error);
    }

    try{
      let parsedBody = JSON.parse(body);
      if(parsedBody.status === "OK"){
        let routes = parsedBody.rows && parsedBody.rows.length > 0 ? parsedBody.rows[0] : null;
        let route = routes && routes.elements && routes.elements.length > 0 ? routes.elements[0] : null;

        if(route && route.distance && route.duration){
          data.distance = Number(route.distance.value) || null;
          data.duration = Math.ceil(route.duration.value / 60) || null
        }
      }
    }catch(e){
      console.error(e)
      console.error("ERROR in Google response compute ")
    }
    callback(null, data)
  });
};