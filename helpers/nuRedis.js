const env = process.env.NODE_ENV || 'development';
const config = require('../config/index')[env];
const HOST = config.redis.url;
const PORT = config.redis.port || 6379;
const redis = require("redis");
const _ = require('lodash');
const async = require('async');
const moment = require('moment');
let client = redis.createClient(PORT, HOST);
let nuRedis = {};
let LIST_COMPANIES = [];

client.on("connect", function () {
  console.log("Redis Connected ");
});

client.on("error", function (err) {
  this.connection_gone("error ", err);
});

client.on("reconnecting", function () {
  console.log("Redis Retrying");
});

// ALL assets stats getting from redis
nuRedis.getAssetsStats = function (id, type, cb) {

  if (!client.ping()) {
    return cb('Error in connecting redis');
  }

  id = 'STATS-' + id + '-' + type;
  client.hgetall(id, function (err, reply) {

    if (!reply) {
      return cb(null, {
        "totDist": 0,
        "avgDistPerAsset": 0,
        "totIdleTime": 0,
        "avgIdleTimePerAsset": 0,
        "totEngTime": 0,
        "avgEngTimePerAsset": 0,
        "totStatTime": 0,
        "avgStatTimePerAsset": 0,
        "avgSpd": 0,
        "fleetUtil": 0
      });
    }

    _.forEach(reply, function (value, key) {
      reply[key] = Number(value);
    });
    return cb(null, reply);

  });
};

//Asset stats getting from redis
nuRedis.getAssetStats = function (aid, cb) {

  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  let id = 'STATS-' + aid;

  client.hgetall(id, function (err, reply) {
    if (!reply) {
      return cb(null, {
        "totDist": 0,
        "totIdleTime": 0,
        "totEngTime": 0,
        "totStatTime": 0,
        "totSpdVio": 0,
        "avgSpd": 0
      });
    }

    _.forEach(reply, function (value, key) {
      reply[key] = Number(value);
    });
    return cb(null, reply);

  });
};

//POI stats adding to redis
nuRedis.setPoiStats = function (cid, data, days, pid, cb) {

  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  let id = 'POI-STATS-' + cid + '-' + days + '-' + pid;
  data.createdTime = Date.now();

  client.hmset(id, data, function (err, reply) {
    delete data.createdTime;
    cb(err, reply);
  });
};

//POI stats getting from redis
nuRedis.getPoiStats = function (cid, days, pid, cb) {

  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  let id = 'POI-STATS-' + cid + '-' + days + '-' + pid;

  client.hgetall(id, function (err, reply) {
    if (!reply) {
      return cb('Does not exist');
    }

    let currentTime = Date.now();
    let diff = Math.abs(currentTime - reply.createdTime);
    let minutes = Math.floor((diff / 1000) / 60);

    if (minutes > 15) {
      return cb('No recent data found');
    } else {
      delete reply.createdTime;
      _.forEach(reply, function (value, key) {
        reply[key] = Number(value);
      });
      return cb(null, reply);
    }
  });
};

// ALL assets stats for graph adding to redis
nuRedis.setAssetsGraphStats = function (id, data, timestamp, type, cb) {

  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }
  let currentTimestamp = Math.floor(Date.now() / 1000);
  let days = Math.round(Math.abs(currentTimestamp - timestamp) / (3600 * 24)) || 1;

  id = 'ALL-ASSETS-GRAPH-STATS-' + id + '-' + days + '-' + type;

  _.forEach(data, function (value, key) {
    data[key] = JSON.stringify(value);
  });

  data.createdTime = Date.now();

  client.hmset(id, data, function (err, reply) {
    if (LIST_COMPANIES.indexOf(id) !== -1) {
      LIST_COMPANIES.splice([LIST_COMPANIES.indexOf(id)], 1);
    }
    delete data.createdTime;
    _.forEach(reply, function (value, key) {
      reply[key] = JSON.stringify(value);
    });
    cb(err, reply);
  });
};

//POI's stats adding to redis
nuRedis.setPoisStats = function (cid, data, days, type, cb) {

  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  let id = 'POIS-STATS-' + cid + '-' + days + '-' + type;
  data.createdTime = Date.now();

  client.hmset(id, data, function (err, reply) {
    delete data.createdTime;
    cb(err, reply);
  });
};

//POI's stats getting from redis
nuRedis.getPoisStats = function (cid, days, type, cb) {

  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  let id = 'POIS-STATS-' + cid + '-' + days + '-' + type;

  client.hgetall(id, function (err, reply) {
    if (!reply) {
      return cb('Does not exist');
    }

    let currentTime = Date.now();
    let diff = Math.abs(currentTime - reply.createdTime);
    let minutes = Math.floor((diff / 1000) / 60);

    if (minutes > 15) {
      return cb('No recent data found');
    } else {
      delete reply.createdTime;
      _.forEach(reply, function (value, key) {
        reply[key] = Number(value);
      });
      return cb(null, reply);
    }
  });
};

// ALL assets stats for graph getting from redis
nuRedis.getAssetsGraphStats = function (id, timestamp, type, cb) {

  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  let currentTimestamp = Math.floor(Date.now() / 1000);
  let days = Math.round(Math.abs(currentTimestamp - timestamp) / (3600 * 24)) || 1;

  id = 'ALL-ASSETS-GRAPH-STATS-' + id + '-' + days + '-' + type;

  client.hgetall(id, function (err, reply) {
    if (!reply) {
      return cb('Does not exist');
    }

    let currentTime = Date.now();
    let diff = Math.abs(currentTime - reply.createdTime);
    let minutes = Math.floor((diff / 1000) / 60);

    if (minutes > 15 && LIST_COMPANIES.indexOf(id) === -1) {
      LIST_COMPANIES.push(id);
      delete reply.createdTime;
      _.forEach(reply, function (value, key) {
        reply[key] = JSON.parse(value);
      });
      return cb('No recent data found', reply);
    } else {
      delete reply.createdTime;
      _.forEach(reply, function (value, key) {
        reply[key] = JSON.parse(value);
      });
      return cb(null, reply);
    }
  });
};

// ALL assets last known location
nuRedis.getAssetsLocation = function (cb) {

  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  client.keys('STATUS*', function (err, reply) {
    if (reply && reply.length > 0) {
      client.mget(reply, function (err, nreply) {
        let data = [];
        nreply.forEach(function (i) {
          try {
            data.push(JSON.parse(i))
          } catch (e) {
            console.error(e)
            console.log("REDIS ASSET KEY error")
          }
        });
        cb(null, data)
      })
    }
  })
};

// All assets locations which groups the user belongs to
nuRedis.appendAssetsLocation = function (assets, cb) {
  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }
  let assetRedisKeys = _.map(assets, function (o) { return "STATUS-" + o.aid });
  client.mget(assetRedisKeys, function (err, nreply) {
    async.eachOf(nreply, function (lstring, i, callback) {
      let ldata = JSON.parse(lstring);
      let value = ldata ? _.find(assets, { aid: ldata.aid }) : null;
      if (value) {
        assets[i] = _.assign(assets[i], ldata)
      }
      callback()
    }, function () {
      cb(null, assets)
    })
  })
};

// ALL assets last known location
nuRedis.getAssetDetails = function (aid, cb) {

  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  client.get('STATUS-' + aid, function (err, nreply) {
    nreply = nreply || null;
    cb(null, JSON.parse(nreply))
  })
};

// IGNITION anomaly
// - Ignition wire is always on
// - Ignition wire is always off

nuRedis.getAssetHealth = function (asset_id, cb) {
  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  client.hget('ASSET-HEALTH', asset_id, function (err, reply) {
    reply = reply || null;
    cb(null, JSON.parse(reply))
  })
};

nuRedis.getLastPingTime = function (aid, cb) {
  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  client.hgetall('LAST_PING-' + aid, function (err, reply) {
    reply = reply || null;
    cb(null, reply)
  })
};

nuRedis.setLastPingTime = function (aid, cb) {
  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  let data = {
    tis: parseInt(Date.now() / 1000)
  };

  client.hmset('LAST_PING-' + aid, data, function (err) {
    cb(err)
  })
};

nuRedis.getLastData = function (assets, cb) {
  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }
  let assetRedisKeys = _.map(assets, function (o) {
    return "STATUS-" + o.aid
  });
  client.mget(assetRedisKeys, function (err, nreply) {
    async.eachOf(nreply, function (lstring, i, callback) {
      let ldata = JSON.parse(lstring);
      let value = ldata ? _.find(assets, { aid: ldata.aid }) : null;
      if (value) {
        assets[i].ign = ldata.ign;
        assets[i].lic_plate_no = ldata.lic_plate_no;
        assets[i].lname = ldata.lname;
        assets[i].spd = ldata.spd;
        assets[i].lat = ldata.lat;
        assets[i].lon = ldata.lon;
        assets[i].tis = ldata.tis
      }
      callback()
    }, function () {
      cb(null, assets)
    })
  })
};

//Entity current status details
nuRedis.getEntityDetails = function (entity_id, cb) {

  if (!client.ping()) {
    return cb('Error in connecting to redis');
  }

  client.get('STATUS_ENTITY-' + entity_id, function (err, nreply) {
    nreply = nreply || '{}';
    cb(null, JSON.parse(nreply))
  })
};


// Delete entity mappings of assets
nuRedis.deleteRedisKey = function (keys, cb) {

  client.del(keys, function (err, reply) {
    console.log('DELETING KEYS', err, reply, Date.now());
    cb()
  })
};

// Delete POI keys
nuRedis.updatePoiKeys = function (data, cb) {
  data.forEach(function (i) {
    client.del("POI-INFO-" + i.id, function (err) {
      if (err) {
        console.log("error deleting poi data from redis ", err);
      } else {
        console.log("poi data deleted from redis")
      }
    });
  });
  cb(null, 'success deleting all poi keys')
};

nuRedis.getAPILimiterKeys = function (fk_c_id, callback) {
  let KEY = `API_LIMITER-${fk_c_id}-`
  async.auto({
    last_req: function (cb) {
      client.get(`${KEY}LAST_REQ`, function (err, nreply) {
        cb(null, nreply)
      })
    },
    consumed_current_hour: function (cb) {
      client.get(`${KEY}CONSUMED_CURRENT_HOUR`, function (err, nreply) {
        cb(null, nreply)
      })
    },
    consumed_today: function (cb) {
      client.get(`${KEY}CONSUMED_TODAY`, function (err, nreply) {
        cb(null, nreply)
      })
    },
    consumed_month: function (cb) {
      client.get(`${KEY}CONSUMED_MONTH`, function (err, nreply) {
        cb(null, nreply)
      })
    },
    credits: function (cb) {
      client.get(`${KEY}CREDITS`, function (err, nreply) {
        cb(null, nreply)
      })
    }
  }, function (err, result) {
    callback(err, result)
  })
}

nuRedis.updateAPILimiterKeys = function (fk_c_id, callback) {
  let KEY = `API_LIMITER-${fk_c_id}-`
  async.auto({
    last_req: function (cb) {
      let FKEY = KEY + 'LAST_REQ';
      client.SET(FKEY, moment().unix(), function (err, nreply) {
        client.EXPIRE(FKEY, 6)
        cb(null, nreply)
      })
    },
    consumed_current_hour: function (cb) {
      let FKEY = KEY + 'CONSUMED_CURRENT_HOUR';
      client.INCR(FKEY, function (err, nreply) {
        let expiry_time = (60 - moment().minutes()) * 60
        client.EXPIRE(FKEY, expiry_time)
        cb(null, nreply)
      })
    },
    consumed_today: function (cb) {
      let FKEY = KEY + 'CONSUMED_TODAY';
      client.INCR(FKEY, function (err, nreply) {
        let expiry_time = Math.abs(moment().diff(moment().endOf('day'), 'seconds'))
        client.EXPIRE(FKEY, expiry_time)
        cb(null, nreply)
      })
    },
    consumed_month: function (cb) {
      let FKEY = KEY + 'CONSUMED_MONTH';
      client.INCR(FKEY, function (err, nreply) {
        let expiry_time = Math.abs(moment().diff(moment().endOf('month'), 'seconds'))
        client.EXPIRE(FKEY, expiry_time)
        cb(null, nreply)
      })
    },
    credits: function (cb) {
      let FKEY = KEY + 'CREDITS';
      client.DECR(FKEY, function (err, nreply) {
        cb(null, nreply)
      })
    }
  }, function (err, result) {
    callback(err, result)
  })
}

module.exports = nuRedis;