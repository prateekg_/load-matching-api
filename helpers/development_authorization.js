/**
 * @author Haston Silva
 * @description Development Authorization converts token to headers and adds feature list
 */

const jwtDecode = require('jwt-decode');
const _ = require('lodash');
const models = require('../db/models');

module.exports = function (req, res, next) {
  if(req.headers.authorization){
    let token = req.headers.authorization.split(' ')[1];
    try{
      let decoded = jwtDecode(token);
      _.each(decoded, function (value, key) {
        req.headers[key] = value;
        req.headers["feature-list"] = '[]';
      });

      let query = "SELECT DISTINCT unnest(features) feature FROM company_plans_vw WHERE fk_c_id = :fk_c_id";
      models.sequelize.query(query, {replacements: {fk_c_id: decoded.fk_c_id},type: models.sequelize.QueryTypes.SELECT})
        .then(function (features) {
          let featureList = [];
          features && features.forEach(function (item) {
            if(item && item.feature){
              featureList.push(item.feature)
            }
          });
          req.headers["feature-list"] = JSON.stringify(featureList);
          next()
        }).catch(function (err) {
          console.error(err);
          next()
        });
    }catch(e){
      console.error(e)
      res.status(400).send("Invalid Token")
    }
  }else{
    next()
  }
};