let moment = require('moment');
let _ = require('lodash');
let geolib = require('geolib');
let async = require('async');

let trip_utils = {};
const CHAR_LABEL = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];

trip_utils.processWaypoints = function (trip, mainCb) {
  try {
    async.auto({
      waypoints: function (cb) {
        let processedPlannedWaypoints = [];
        let seqWithOne = false;

        async.eachOfSeries(trip.waypoints, function (i, index, callbackWaypoints) {
          if (index === 0) {
            if (i.seq_no === 1) {
              seqWithOne = true
            }
          }

          let waypoint = {
            fk_poi_id: i.fk_poi_id,
            poi: {
              id: i.fk_poi_id > 0 ? i.fk_poi_id : null,
              type: i.type || null,
              category: null
            },
            type: "planned",
            arrivalTis: i.tis,
            departureTis: i.tis + (i.stop_dur * 60),
            dwellDuration: i.stop_dur,
            violations: 0,
            crossed: false,
            label: seqWithOne ? CHAR_LABEL[i.seq_no - 1] : CHAR_LABEL[i.seq_no] || '',
            avgSpd: 0,
            distance: 0,
            geoBounds: []
          };

          i.type = "planned";
          waypoint = _.merge(waypoint, i);
          processedPlannedWaypoints.push(waypoint);
          callbackWaypoints()
        }, function () {
          cb(null, processedPlannedWaypoints)
        })
      },
      poi_processor: function (cb) {
        if (trip.trip_log && trip.trip_log.trip) {
          let log = trip.trip_log.trip || [];
          cb(null, log)
        } else {
          cb(null, [])
        }
      },
      logProcessor: ['waypoints', 'poi_processor', function (results, cb) {
        let waypoints = results.waypoints;
        let trip_data = results.poi_processor;
        let processedLog = [];

        if (trip_data && trip_data.length > 0) {
          let log = trip_data;

          _.forEach(log, function (i, index) {
            let matchedItemIndex = _.findIndex(waypoints, function (o) {
              // Check if poi id is same or lat,lon within 50 meters
              return i.Id === o.fk_poi_id || geolib.isPointInCircle({
                latitude: o.lat,
                longitude: o.lon
              }, {
                latitude: i.Lat,
                longitude: i.Lon
              }, 50)
            });

            let labelText = "";
            let typeEvent = "unplanned";
            if (matchedItemIndex >= 0) {
              typeEvent = "planned";
              labelText = waypoints[matchedItemIndex].label;
              waypoints.splice(matchedItemIndex, 1)
            }

            processedLog.push({
              fk_poi_id: i.Id,
              poi: {
                id: i.Id > 0 ? i.Id : null,
                type: i.Type || null,
                category: i.poi_category || null
              },
              type: typeEvent,
              lname: i.LName,
              arrivalTis: i.StartTis,
              departureTis: i.EndTis,
              dwellDuration: Math.round(moment.duration(i.DwellTime, 'seconds').asMinutes()),
              violations: 0,
              crossed: true,
              label: labelText,
              lat: i.Lat,
              lon: i.Lon,
              avgSpd: 0,
              distance: 0
            });

            processedLog.push({
              fk_poi_id: null,
              poi: null,
              type: "intransit",
              lname: "",
              arrivalTis: (i.EndTis + 1),
              departureTis: log[index + 1] ? (log[index + 1].StartTis - 1) : null,
              dwellDuration: 0,
              violations: 0,
              crossed: true,
              label: "",
              lat: null,
              lon: null,
              avgSpd: i.AvgSpd,
              distance: Number((i.Distance / 1000).toFixed(1))
            })
          });

          if (trip.status === 'completed') {
            processedLog.pop()
          }

        }
        cb(null, {waypoints: waypoints, log: _.concat(processedLog, waypoints)})
      }],
      processor: ['waypoints', 'logProcessor', function (results, cb) {
        if (trip.status === 'scheduled') {
          cb(null, results.waypoints)
        } else {
          cb(null, results.logProcessor.log)
        }
      }]
    }, function (err, results) {
      let unplannedLabelIndex = 1;
      let data = _.forEach(results.processor, function (value) {
        value.fk_poi_id = undefined;
        if (value.type === 'unplanned') {
          value.label = unplannedLabelIndex.toString();
          unplannedLabelIndex++
        }
      });
      mainCb(null, _.orderBy(data, ['crossed', 'arrivalTis'], ['desc', 'asc']))
    })
  }catch(e){
    console.log("ERORR trip_utils.processWaypoints() : ", e)
  }
};

module.exports = trip_utils;