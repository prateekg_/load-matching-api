/*
 Email helper is used to write common emails sent from settings project
*/

const env       = process.env.NODE_ENV || 'development';
const config    = require('../config/index')[env];
const mailerConfig = config.mailer;
const nodemailer = require('nodemailer');

/**
 * Email sent when admin user changes email or password of other user
 * @param {String} to_emails - receiver email
 * @param {String} bcc_emails - sender email
 * @param {Object} email_data - email data
 * @param callback
 */
exports.userEmailChange = function(to_emails, bcc_emails, email_data, callback){
  const transporter = nodemailer.createTransport(mailerConfig);
  let email_text = "";
  email_data.email ? email_text = 'Email: ' + email_data.email + '<br>' :'';
  email_data.name ? email_text += 'Name: ' + email_data.name + '<br>' + '<br>' :'';
  email_data.cont_no ? email_text += 'Contact No.: ' + email_data.cont_no + '<br>' + '<br>' :'';
  const mailOptions = {
    from: 'noreply@numadic.com',
    to: to_emails,
    bcc: bcc_emails + ',noreply@numadic.com',
    subject: 'Admin Email/Password Change',

    html: '<body style="color:#444;font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;padding:0 100px;width:800px">' +
    '<img src="cid:nu-email-header-logo"/>' +
    'Hello ' + email_data.name + ',' + '<br>' + '<br>' +
    'Admin user have changed your email/password.' +
    '<br>' +
    '<b>Your new account details are:</b><br>' + email_text +
    '<b>Access the Numadic Platform here, if you haven\'t already:</b>' +
    '<p>https://app.numadic.com/</p>' +
    '<br>' +
    '<p>Have a safe trip!</p>' +
    '</body>',
    attachments: [
      {
        filename: "nu-logo",
        path: './public/images/nu-email-header.png',
        cid: 'nu-email-header-logo'
      }]
  };
  if(env === "production"){
    transporter.sendMail(mailOptions, function (error) {
      callback(error)
    });
  }
  else callback(null)
};

/**
 * Email sent when admin user changes password of other user
 * @param {String} to_emails - receiver email
 * @param {String} bcc_emails - sender email
 * @param {Object} email_data - email data
 * @param {String} req_origin
 * @param callback
 */
exports.userPasswordChange = function(to_emails, bcc_emails, email_data, req_origin, callback){
  const transporter = nodemailer.createTransport(mailerConfig);
  let email_text = "";
  email_data.password ? email_text += 'Password: ' + email_data.password + '<br>' + '<br>' :'';
  let mailOptions = {
    from: 'noreply@numadic.com',
    to: to_emails,
    bcc: bcc_emails+',noreply@numadic.com',
    subject: 'Admin Password Change',

    html: '<body style="color:#444;font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;padding:0 100px;width:800px">' +
        '<img src="cid:nu-email-header-logo"/>' +
        'Hello ' + email_data.name + ',' + '<br>' + '<br>' +
        'Admin user have changed your password.' +
        '<br>' +
        '<b>Your new password</b><br>' + email_text +
        '<b>Access the Numadic Platform here, if you haven\'t already:</b>' +
        '<p><a href="'+ req_origin +'">'+ req_origin +'</a></p>' +
        '<br>' +
        '<p>Have a safe trip!</p>' +
        '</body>',
    attachments: [
      {
        filename: "nu-logo",
        path: './public/images/nu-email-header.png',
        cid: 'nu-email-header-logo'
      }]
  };
  transporter.sendMail(mailOptions, function (error) {
    callback(error)
  });
};


/**
 * Email sent when new user is created in EOL
 * @param {Object} email_data - email data
 * @param callback
 */
exports.eolNewUserCreated = function(email_data, callback){
  const transporter = nodemailer.createTransport(mailerConfig);
  let mailOptions = {
    from: 'noreply@numadic.com',
    to: email_data.to_email,
    bcc: 'noreply@numadic.com',
    subject: 'Welcome to the Numadic Platform',

    html: '<body style="color:#444;font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;padding:0 100px;width:800px">' +
        '<img src="cid:nu-email-header-logo"/>' +
        '<h1><b>Welcome to the Numadic Platform</b></h1>' +
        'Dear ' + email_data.name + ',' + '<br>' + '<br>' +
        'Please use the link below and the login credentials to enter the Numadic Fleet Tracking and Management system on your web browser.' +
        '<br><br>' +
        'Once you log in you will be directed to a simple walkthrough that will explain the features and tools of the system.'+
        '<br><br>' +
        'To summarise you will be able to'+
        '<ol>'+
            '<li>Track vehicles </li>'+
            '<li>Monitor vehicle activity at depots</li>'+
            '<li>Monitor vehicle activity at ROs</li>'+
            '<li>Track vehicle utilisation</li>'+
            '<li>Track vehicle trips and routes which are linked to invoices</li>'+
            '<li>Export data to excel</li>'+
            '<li>Receive notifications and alerts</li>'+
        '</ol>'+
        '<div style="font-size:15px">'+
        '   <b>Access the Numadic Platform here, if you haven\'t already:</b>' + '<br>' +
        '   <p>Link: <a href="http://eol.numadic.com">eol.numadic.com</a></p>'+
        '   <br>'+
        '   <b>Your account details are:</b><br>' +
        '   User ID: ' + email_data.to_email + '<br>' +
        '   Password: ' + email_data.password + '<br><br>'+
        '   You can easily change your password from the setting page once you login to your account.'+
        '</div><br>'+
        '<div>Sincerely,<br><br>'+
        '   <b>Numadic Team</b><br>'+
        '   <span><a href="https://numadic.com/">Numadic</a></span><br>'+
        '   <span>Undisrupting logistics. ™</a>'+
        '</div><br>'+
        '<div style="font-size:xx-small">'+
        '   5th Floor<br>'+
        '   Central Towers<br>'+
        '   Panjim, Goa<br>'+
        '   Sales & Support: 777-600-5544<br>'+
        '   Office : +91 (832) 222-5544'+
        '</div><br>'+
        '<div>'+
        ' <span style="color:#cccccc;font-family:tahoma,sans-serif;font-size:xx-small">____________________________</span>'+
        '</div><br>'+
        '<div>'+
        '   <span style="color:#cccccc;font-family:tahoma,sans-serif;font-size:xx-small">This transmission is intended solely for the person or organization to whom it is addressed and it may contain privileged and confidential information. If you are not the intended recipient you must not copy, distribute or take any action in reliance on it.&nbsp;If you believe you received this transmission in error please notify the sender.</span>'+
        '</div>'+
        '</body>',
    attachments: [
      {
        filename: "nu-logo",
        path: './public/images/nu-email-header.png',
        cid: 'nu-email-header-logo'
      }]
  };
  transporter.sendMail(mailOptions, function (error) {
    callback(error)
  });
};
