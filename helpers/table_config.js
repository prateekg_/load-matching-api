const models = require('../db/models');
const _ = require('lodash');

function processTableConfigValues(data, callback) {
  try {
    _.forEach(data.default_config.tabs, function (value, key) {
      if (data.config && data.config.tabs) {
        if (!data.config.tabs[key]) {
          // tab not found in user config. add the new tab
          data.config.tabs[key] = value;
        } else {
          // check objects in tabs
          if (Array.isArray(value)) {
            value.forEach(function (i) {
              let column = _.find(data.config.tabs[key], {key: i.key});
              if(column){
                // Update default column selected with user config
                i.selected = column.selected
              }
            })
          }
        }
      } else {
        data.config = data.default_config
      }
    });
    return callback(null, data.default_config)
  }catch(e){
    console.error(e)
    return callback(null, data.default_config)
  }
}

exports.getTableConfig = function (table_name, fk_u_id, selected_tab, return_default, callback) {
  try {
    let query = "SELECT wtt.default_config, wuc.config " +
            " FROM wv_table_types wtt " +
            " LEFT JOIN wv_user_configs wuc on wuc.fk_wv_table_type_id = wtt.id and wuc.fk_user_id = $fk_u_id " +
            " WHERE wtt.table_name = $table_name;";

    let query_params = {fk_u_id: fk_u_id, table_name: table_name};
    models.sequelize.query(query, {
      bind: query_params,
      type: models.sequelize.QueryTypes.SELECT
    }).spread(function (data) {
      if (!data) {
        return callback("Invalid table name")
      } else {
        if (return_default) {
          return callback(null, data.default_config.tabs[selected_tab])
        } else {
          processTableConfigValues(data, function (err, config) {
            return callback(null, config.tabs[selected_tab])
          })
        }
      }
    }).catch(function (err) {
      callback(err)
    })
  }catch(e){
    callback(e)
  }
};