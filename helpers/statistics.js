const async = require('async');
const _ = require('lodash');
const calculate = require('../helpers/calculate');
let statistics = function () {};

statistics.prototype.getFleetData = function(perDay, perMonth, latestData, mainCallback) {
  perDay = _.groupBy(perDay, function(b) { return b.aid});
  perMonth = _.groupBy(perMonth, function(b) { return b.aid});
  async.auto({
    idleTime : calculate.getIdleTime.bind(null, perDay),
    avgIdleTime : calculate.getAverageIdleTime.bind(null, perMonth),
    statTime : calculate.getStationaryTime.bind(null, perDay),
    avgStatTime : calculate.getAverageStationary.bind(null, perMonth),
    totEngTime : calculate.getFleetwideTime.bind(null, perDay),
    avgEngTime : calculate.getAverageFleetwide.bind(null, perMonth),
    assets : calculate.getAssetInfo.bind(null, latestData),
    avgDist : calculate.getAverageDistance.bind(null, perMonth),
    totDist : calculate.getDistance.bind(null, perDay)
  },
  function (err, result) {
    result.idleTime = Math.round(result.idleTime);
    result.avgIdleTime = Math.round(result.avgIdleTime);
    result.totEngTime = Math.round(result.totEngTime);
    result.avgEngTime = Math.round(result.avgEngTime);
    result.statTime = Math.round(result.statTime);
    result.avgStatTime = Math.round(result.avgStatTime);
    result.avgDist = Number((result.avgDist).toFixed(1));
    mainCallback(result)
  });
};

statistics.prototype.getAssetData = function(perDay, count, mainCallback) {
  perDay = _.groupBy(perDay, function(b) { return b.aid});
	
  async.auto({
    totIdleTime : calculate.getIdleTime.bind(null, perDay),		
    totStatTime : calculate.getStationaryTime.bind(null, perDay),	
    totEngTime : calculate.getFleetwideTime.bind(null, perDay),
    fleetUtil : calculate.getFleetUtilization.bind(null, perDay),
    totDist : calculate.getDistance.bind(null, perDay)
  },
  function (err, result) {
    result.totIdleTime = Math.round(result.totIdleTime);
    result.avgIdleTimePerAsset = Math.round(result.totIdleTime/count) || 0;

    result.totEngTime = Math.round(result.totEngTime);
    result.avgEngTimePerAsset = Math.round(result.totEngTime/count) || 0;

    result.totStatTime = Math.round(result.totStatTime);
    result.avgStatTimePerAsset = Math.round(result.totStatTime/count) || 0;

    result.avgDistPerAsset = Number((result.totDist/count).toFixed(1))  || 0;

    result.fleetUtil = Math.round((result.fleetUtil * 100)/count) || 0;
    mainCallback(result);
  });
};

statistics.prototype.getAssetDetailsStats = function(perDay, mainCallback) {
  // perDay = _.groupBy(perDay, function(b) { return b.aid});
  perDay = {'asset': perDay};

  async.auto({
    totSpdVio : calculate.getSpeedViolation.bind(null, perDay),
    // avgSpdVio : calculate.getAverageSpeedViolation.bind(null, perMonth),
    totIdleTime : calculate.getIdleTime.bind(null, perDay),
    // avgIdleTime : calculate.getAverageIdleTime.bind(null, perMonth),
    // avgStatTime : calculate.getAverageStationary.bind(null, perMonth),
    totStatTime : calculate.getStationaryTime.bind(null, perDay),
    // avgDist : calculate.getAverageDistance.bind(null, perMonth),
    totDist : calculate.getDistance.bind(null, perDay),
    totEngTime : calculate.getFleetwideTime.bind(null, perDay),
    // avgEngTime : calculate.getAverageFleetwide.bind(null, perMonth)
  },
  function (err, result) {
    result.totEngTime = Math.round(result.totEngTime);
    result.totIdleTime = Math.round(result.totIdleTime);
    result.totStatTime = Math.round(result.totStatTime);

    mainCallback(result);
  });
};

statistics.prototype.getForklStats = function(data, mainCallback) {
  let stats = {
    tot_mov : 0,
    tot_stat : 0,
    total : 0,
    utilRate : ''
  };
  // data = _.groupBy(data, function(b) { console.log(b); return b.aid});
	
  _.each(data, function (forklift) {
    if(forklift.ign === 'B') {
      stats.tot_mov++;
    } else {
      stats.tot_stat++;
    }
    stats.total++;
  });

  stats.utilRate = Math.round((stats.tot_mov * 100)/stats.total) + '%';

  mainCallback(null, stats);
};

statistics.prototype.getPoiDetailStats = function (perDay, perMonth, totCount, avgCount, mainCallback) {
  // var idleDataPerDay = _.groupBy(perDay, 'fk_asset_id');
  // var idleDataPerMonth = _.groupBy(perMonth, 'fk_asset_id');
  async.auto({
    // avgAssetsEntered : calculate.getAverageAssetsPerDay.bind(null, perMonthAssets),
    totDwellTime : calculate.getAssetPoiDwellTime.bind(null, perDay),
    // totIdleTime : calculate.getIdleTime.bind(null, idleDataPerDay),
    // avgIdleTime : calculate.getAverageIdleTime.bind(null, idleDataPerMonth),
    avgDwellTime : calculate.getAverageAssetPoiDwellTime.bind(null, perMonth)
  }, function (err, result) {
    // result.totAssetsEntered = perDayAssets.length;
    // result.totIdleTime = Math.round(result.totIdleTime);
    let response = {};
    response.avgDwellTimePerAsset = Math.round (result.totDwellTime/totCount) || 0;
    response.avgDwellTimePerAssetDaily = Math.round(result.avgDwellTime / avgCount) || 0;
    mainCallback(null, response);
  });
};

statistics.prototype.getPoiStats = function (perDay, perMonth, totCount, avgCount, mainCallback) {
  // var idleDataPerDay = _.groupBy(perDay, 'fk_asset_id');
  // var idleDataPerMonth = _.groupBy(perMonth, 'fk_asset_id');
  async.auto({
    // avgAssetsEntered : calculate.getAverageAssetsPerDay.bind(null, perMonthAssets),
    totDwellTime : calculate.getAssetPoiDwellTime.bind(null, perDay),
    // totIdleTime : calculate.getIdleTime.bind(null, idleDataPerDay),
    // avgIdleTime : calculate.getAverageIdleTime.bind(null, idleDataPerMonth),
    avgDwellTime : calculate.getAverageAssetPoiDwellTime.bind(null, perMonth)
  }, function (err, result) {
    // result.totAssetsEntered = perDayAssets.length;
    // result.totIdleTime = Math.round(result.totIdleTime);
    let response = {};
    response.avgDwellTimePerAsset = Math.round (result.totDwellTime/totCount) || 0;
    response.avgDwellTimePerAssetDaily = Math.round(result.avgDwellTime / avgCount) || 0;
    mainCallback(null, response);
  });
};

module.exports = new statistics();