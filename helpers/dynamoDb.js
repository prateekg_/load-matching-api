const _ = require('lodash');
const request = require('request');
const nuDynamoDb = {};
const env       = process.env.NODE_ENV || 'development';
const config    = require('../config/index')[env];

nuDynamoDb.getAssetLocationFromAPI= function (params, callback) {

  let url = config.server_address.location_api() + '/data/location?report=true&fk_asset_id='+ params.fk_asset_id + '&stis=' + params.start_tis +'&etis='+params.end_tis;
  request.get(url, function (error, response, body) {
    if (error) {
      return callback(null, [])
    }
    let locationData = JSON.parse(body);
    locationData = _.sortBy(_.filter(locationData, 'lat','lon'), ['tis']);

    for(let i = 0, len = locationData.length; i < len; i++){
      let packet = locationData[i];
      if(packet.ign === 'A' && packet.spd < 5){
        let next_packet = locationData[i+1];
        if(next_packet && next_packet.ign === 'A' && next_packet.spd < 5){
          locationData[i+1].lat = packet.lat;
          locationData[i+1].lon = packet.lon
        }
      }
    }
    callback(null, locationData)
  })
};

nuDynamoDb.getAssetFuelValue= function (params, callback) {

  let url = config.server_address.location_api() + '/data/location?include=sensor_data&fk_asset_id='+ params.fk_asset_id + '&stis=' + params.start_tis +'&etis='+params.end_tis;
  request.get(url, function (error, response, body) {
    if (error) {
      return callback(null, [])
    }
    let fuelData = JSON.parse(body);
    let fuel = [];
    _.sortBy(_.map(fuelData, function(object) {
      let tmp = _.pick(object, ['fk_sensor_type_4']);
      if(!_.isEmpty(tmp)) {
        fuel.push(_.values(tmp.fk_sensor_type_4)[0])
      }
    }), ['tis']);


    callback(null, fuel)
  })
};

nuDynamoDb.getAssetLocationData= function (params, callback) {

 
  let url = config.server_address.location_api() + '/data/location?report=true&fk_asset_id='+ params.fk_asset_id + '&stis=' + params.start_tis +'&etis='+params.end_tis;
  request.get(url, function (error, response, body) {
    if (error) {
      console.log(error);
      return callback(null, [])
    }
    let locationData = JSON.parse(body);
        
    callback(null, locationData)
  })
};

/**
 * This is a common helper to fetch location data from dynamo db.
 * Use include and exists params to fetch respective fields. 
 * Include param is used to get required column from db
 * Exists param is used to fetch data if there is data for that column for a given time range.
 * Use both include and exits to fetch column from dynamo db if there is data.
 * Refer - https://docs.google.com/document/d/1e-kb8ZKQP4ejAWu3NrPJCLz8omWm0dqOW62rVOAcoPo/edit?ts=5ac1e31c#
 * @param params
 * @param {String} include - Include query params for location api url
 * @param {String} exists - Exists query params for location api url
 * @param {*} callback 
 */
nuDynamoDb.getLocationData = function(params, include, exists, callback){
  let url = config.server_address.location_api() + '/data/location?include=sensor_data&fk_asset_id='+ params.fk_asset_id + '&stis=' + params.start_tis +'&etis='+params.end_tis+include+exists;
  request.get(url, function (error, response, body) {
    if (error) {
      console.log(error);
      return callback(null, [])
    }
    let locationData = JSON.parse(body);
    locationData = _.sortBy(locationData, ['tis']).reverse();
        
    callback(null, locationData)
  })
};


nuDynamoDb.getTripPolyline = function (params, cb) {

  let options = { method: 'GET',
    url: config.server_address.map_match() + '/data/mapmatch',
    qs: { fk_trip_id: params.fk_trip_id },
    json: true,
    headers:
          { 'cache-control': 'no-cache',
            'content-type': 'application/json'
          }
  };

  request(options, function (error, response, body) {
    if (error){
      console.log("Map match trip API error : ", error);
      return cb(null, "")
    }

    if(response.statusCode === 200){
      return cb(null, body.matched_route)
    }else{
      console.log("Map match trip API error : ", error);
      return cb(null, "")
    }
  });
};


module.exports = nuDynamoDb;