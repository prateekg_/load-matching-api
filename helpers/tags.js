const async = require('async');
const models = require('../db/models');

exports.tagType = {
  trucks: 1,
  forklifts: 2,
  pois: 3,
  route_templates: 4,
  trip_plannings:	5,
  maintenance_records: 6
};

exports.processTags = function (data, rec_id, fk_company_id, tag_type,  callback) {

  let recordsToUpdate = [];
  let recordsToCreate = [];

  data.forEach(function (i) {

    i.t_id = rec_id;
    i.fk_c_id = fk_company_id;
    i.type = tag_type;

    if(i.id){
      recordsToUpdate.push(i)
    }else{
      recordsToCreate.push(i)
    }
  });


  async.parallel({
    update: function (cb) {
      async.each(recordsToUpdate,function (row, cb2) {
        models.tag.update(row, {
          where : {
            id : row.id,
            fk_c_id: fk_company_id,
            active: true
          }
        }).then(function () {
          cb2()
        }).catch(function (err) {
          cb2(err)
        })
      }, function (err, result) {
        cb(err, result)
      })
    },
    create: function (cb) {
      async.each(recordsToCreate,function (row, cb2) {
        models.tag.findOrCreate({
          where: {
            t_id: rec_id,
            fk_c_id: fk_company_id,
            tag: row.tag
          },
          defaults: row
        }).spread(function () {
          cb2();
        });
      }, function (err) {
        cb(err, null)
      })
    }
  }, function (err) {
    if(err){
      callback(err)
    }else{
      callback(null,'success')
    }
  })
};