const amqp = require('amqplib/callback_api');
const env = process.env.NODE_ENV || 'development';
const config = require('../config/index')[env];
const models = require('../db/models');
const request = require('request');
let MQUserName = config.rabbitMq.userName;
let MQPassword = config.rabbitMq.password;
let MQPort = config.rabbitMq.port;
let channel = null;
let connection = null;

let closeOnErr = function (err) {
  if (!err) return false;
  console.log('[AMQP] error', err);
  connection.close();
  return true
};

let subscribe = function () {
  connection.createChannel(function (err, ch) {
    if (closeOnErr(err)) return;
    ch.on('error', function (err) {
      console.log('[AMQP] channel error', err.message)
    });
    ch.on('close', function () {
      console.log('[AMQP] channel closed')
    });
    channel = ch
  })
};

let start = function () {
  let query = "SELECT host FROM urls WHERE name ILIKE '%RMQINT%'";

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (url) {
      let host = 'amqp://' + MQUserName + ':' + MQPassword + '@' + url.host;
      amqp.connect(host, function (err, conn) {
        if (err) {
          return setTimeout(start, 2000)
        }
        conn.on('error', function (err) {
          console.log('[AMQP] conn error', err.message)
        });
        conn.on('close', function () {
          console.log('[AMQP] reconnecting');
          return setTimeout(start, 1000)
        });
        console.log('[AMQP] connected');
        connection = conn;
        subscribe()
      })
    }).catch(function (err) {
      console.error(err);
      return setTimeout(start, 2000)
    })
};

start();

exports.createExchange = function (exchangeName) {
  if (channel) {
    try {
      channel.assertExchange(exchangeName, 'direct', { durable: true })
    } catch (e) {
      console.log('RABBITMQ FAILED', e)
    }
  } else {
    console.log('RABBIT CHANNEL NULL')
  }
};

exports.publishToAppLoggingQueue = function (queue, content) {
  try {
    channel.assertQueue(queue, { durable: true });
    channel.sendToQueue(queue, content, { persistent: true, options: { arguments: { 'x-message-ttl': 864000, 'x-expires': 864000 } } })
  } catch (e) {
    console.error('[AMQP] publish', e.message)
  }
};

exports.generateRabbitMqAccount = function (email, rmqPassword, tag, callback) {
  if(channel){
    let query = "SELECT host FROM urls WHERE name ILIKE '%RMQINT%'";

    models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
      .spread(function(url) {
        // Use it locally
        //url.host = "13.228.52.219"

        let httpHost = "http://" + MQUserName + ":" + MQPassword + "@" + url.host + ':' + MQPort;
        let rabbit_mq_body = {
          password : rmqPassword,
          tags: tag
        };
        let options = {
          uri : httpHost + '/api/users/' + email,
          method : 'PUT',
          json : rabbit_mq_body
        };

        request(options, function(error){
          if(error) {
            callback("Error creating user")
          }else{
            let permissions = {
              vhost : '/',
              username : email,
              configure : '^(' + email + '.*)|(rmq-objc-client.gen-*)|(stomp-subscription-.*)|(amq.gen-.*)$',
              read : '^(' + email + '.*)|(rmq-objc-client.gen-*)|(stomp-subscription-.*)|(amq.gen-.*)$',
              write : '^(stomp-subscription-.*)|(rmq-objc-client.gen-*)|(amq.gen-.*)|(chat.*)$'
            };

            let permission_options = {
              uri : httpHost + '/api/permissions/%2F/' + email,
              method : 'PUT',
              json : permissions
            };

            request(permission_options, function(error){
              console.log(error);
              if (error) {
                callback("Error creating permissions")
              } else {
                callback()
              }
            });
          }
        });
      }).catch(function () {
        callback('DB error')
      })
  }else{
    callback('Rabbitmq connection not established')
  }
};

exports.deleteRabbitMqAccount = function (email, rmqPassword, tag, callback) {
  if(channel){
    let query = "SELECT host FROM urls WHERE name ILIKE '%RMQINT%'";

    models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
      .spread(function(url) {
        // Use it locally
        //url.host = "13.228.52.219"
        let httpHost = "http://" + MQUserName + ":" + MQPassword + "@" + url.host + ':' + MQPort;
        let rabbit_mq_body = {
          password : rmqPassword,
          tags: tag
        };
        let options = {
          uri : httpHost + '/api/users/' + email,
          method : 'DELETE',
          json : rabbit_mq_body
        };

        request(options, function(error){
          if(error) {
            callback("Error deleting user")
          }else{
            let permissions = {
              vhost : '/',
              username : email,
              configure : '^(' + email + '.*)|(rmq-objc-client.gen-*)|(stomp-subscription-.*)|(amq.gen-.*)$',
              read : '^(' + email + '.*)|(rmq-objc-client.gen-*)|(stomp-subscription-.*)|(amq.gen-.*)$',
              write : '^(stomp-subscription-.*)|(rmq-objc-client.gen-*)|(amq.gen-.*)|(chat.*)$'
            };

            let permission_options = {
              uri : httpHost + '/api/permissions/%2F/' + email,
              method : 'DELETE',
              json : permissions
            };

            request(permission_options, function(error){
              console.log(error);
              if (error) {
                callback("Error deleting permissions")
              } else {
                callback()
              }
            });
          }
        });
      }).catch(function () {
        callback('DB error')
      })
  }else{
    callback('Rabbitmq connection not established')
  }
};
