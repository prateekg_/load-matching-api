exports.group = {
  "tracking": {
    "enabled": true,
    "trucks": {
      "speeding": {
        "level": "low",
        "trigger": 80,
        "inApp": true,
        "email": true
      },
      "continuousSpeeding": {
        "level": "low",
        "trigger": 80,
        "inApp": true,
        "email": true
      },
      "continuousIdling": {
        "level": "low",
        "trigger": 80,
        "inApp": true,
        "email": true
      },
      "cumulativeIdling": {
        "level": "low",
        "trigger": 80,
        "inApp": true,
        "email": true
      },
      "stationary": {
        "level": "low",
        "trigger": 80,
        "inApp": true,
        "email": true
      },
      "vehCompliance": {
        "level": "high",
        "inApp": true,
        "email": true
      },
      "vehComplianceReminder": {
        "level": "medium",
        "trigger": 30,
        "inApp": true,
        "email": true
      }
    },
    "vans": {
      "speeding": {
        "level": "low",
        "trigger": 80,
        "inApp": true,
        "email": true
      },
      "continuousSpeeding": {
        "level": "low",
        "trigger": 80,
        "inApp": true,
        "email": true
      },
      "continuousIdling": {
        "level": "low",
        "trigger": 80,
        "inApp": true,
        "email": true
      },
      "cumulativeIdling": {
        "level": "low",
        "trigger": 80,
        "inApp": true,
        "email": true
      },
      "stationary": {
        "level": "low",
        "trigger": 80,
        "inApp": true,
        "email": true
      },
      "vehCompliance": {
        "level": "high",
        "inApp": true,
        "email": true
      },
      "vehComplianceReminder": {
        "level": "medium",
        "trigger": 30,
        "inApp": true,
        "email": true
      }
    }
  },
  "pois": {
    "enabled": true,
    "dwelling": {
      "level": "low",
      "trigger": 80,
      "inApp": true,
      "email": true
    },
    "entry": {
      "level": "low",
      "inApp": true,
      "email": true
    },
    "exit": {
      "level": "high",
      "inApp": true,
      "email": true
    }
  },
  "trip": {
    "enabled": true,
    "start": {
      "level": "high",
      "inApp": true,
      "email": true
    },
    "nearDest": {
      "level": "high",
      "trigger": 120,
      "inApp": true,
      "email": true
    },
    "dwelling": {
      "level": "medium",
      "trigger": 180,
      "inApp": true,
      "email": true
    },
    "delay": {
      "level": "high",
      "trigger": 120,
      "inApp": true,
      "email": true
    },
    "end": {
      "level": "high",
      "inApp": true,
      "email": true
    }
  },
  "driver": {
    "enabled": true,
    "driverCompliance": {
      "level": "high",
      "inApp": true,
      "email": true
    },
    "driverComplianceReminder": {
      "level": "medium",
      "trigger": 30,
      "inApp": true,
      "email": true
    }
  },
  "maintenance": {
    "enabled": true,
    "maintenanceAlert": {
      "level": "high",
      "inApp": true,
      "email": true
    },
    "maintenanceReminder": {
      "level": "medium",
      "trigger": 30,
      "inApp": true,
      "email": true
    }
  },
  "tyre": {
    "enabled": true,
    "tyreAlert": {
      "level": "high",
      "inApp": true,
      "email": true
    },
    "tyreReminder": {
      "level": "medium",
      "trigger": 30,
      "inApp": true,
      "email": true
    }
  },
  "restrictedHours": {
    "enabled": true,
    "trucks": {
      "level": "high",
      "trigger": [],
      "inApp": true,
      "email": true
    },
    "vans": {
      "level": "high",
      "trigger": [],
      "inApp": true,
      "email": true
    }
  },
  "fuel": {
    "enabled": true,
    "trucks": {
      "rapidDrop": {
        "level": "low",
        "trigger": 0,
        "inApp": true,
        "email": true
      },
      "refuelingIncident": {
        "level": "low",
        "trigger": 0,
        "inApp": true,
        "email": true
      }
    },
    "vans": {
      "rapidDrop": {
        "level": "low",
        "trigger": 0,
        "inApp": true,
        "email": true
      },
      "refuelingIncident": {
        "level": "low",
        "trigger": 0,
        "inApp": true,
        "email": true
      }
    }
  }
};

exports.user = {
  "tracking": {
    "enabled": true,
    "speeding": {
      "inApp": true,
      "email": true
    },
    "continuousSpeeding": {
      "inApp": true,
      "email": true
    },
    "continuousIdling": {
      "inApp": true,
      "email": true
    },
    "cumulativeIdling": {
      "inApp": true,
      "email": true
    },
    "stationary": {
      "inApp": true,
      "email": true
    },
    "vehCompliance": {
      "inApp": true,
      "email": true
    },
    "vehComplianceReminder": {
      "inApp": true,
      "email": true
    }
  },
  "pois": {
    "enabled": true,
    "dwelling": {
      "inApp": true,
      "email": true
    },
    "entry": {
      "inApp": true,
      "email": true
    },
    "exit": {
      "inApp": true,
      "email": true
    }
  },
  "trip": {
    "enabled": true,
    "tripUpdates": {
      "inApp": false,
      "email": true
    },
    "start": {
      "inApp": true,
      "email": true
    },
    "nearDest": {
      "inApp": true,
      "email": true
    },
    "dwelling": {
      "inApp": true,
      "email": true
    },
    "delay": {
      "inApp": true,
      "email": true
    },
    "end": {
      "inApp": true,
      "email": true
    }
  },
  "driver": {
    "enabled": true,
    "driverCompliance": {
      "inApp": true,
      "email": true
    },
    "driverComplianceReminder": {
      "inApp": true,
      "email": true
    }
  },
  "maintenance": {
    "enabled": true,
    "maintenanceAlert": {
      "inApp": true,
      "email": true
    },
    "maintenanceReminder": {
      "inApp": true,
      "email": true
    }
  },
  "tyre": {
    "enabled": true,
    "tyreAlert": {
      "inApp": true,
      "email": true
    },
    "tyreReminder": {
      "inApp": true,
      "email": true
    }
  },
  "restrictedHours": {
    "enabled": true,
    "inApp": true,
    "email": true
  },
  "fuel": {
    "enabled": true,
    "rapidDrop": {
      "inApp": true,
      "email": true
    },
    "refuelingIncident": {
      "inApp": true,
      "email": true
    }
   
  },
  "temperature": {
    "enabled": true,
    "thresholdAlert": {
      "inApp": true,
      "email": true
    }   
  }
};

exports.userGroup = function (result) {
  return {
    "gid": result.gid,
    "fk_g_id": result.fk_group_id,
    "mute": false,
    "tracking": {
      "enabled": true,
      "trucks": {
        "speeding": {
          "inApp": false,
          "email": true
        },
        "continuousSpeeding": {
          "inApp": false,
          "email": true
        },
        "continuousIdling": {
          "inApp": false,
          "email": true
        },
        "cumulativeIdling": {
          "inApp": false,
          "email": true
        },
        "stationary": {
          "inApp": false,
          "email": true
        },
        "vehComplianceReminder": {
          "inApp": true,
          "email": true
        },
        "vehCompliance": {
          "inApp": true,
          "email": true
        }
      },
      "vans": {
        "speeding": {
          "inApp": false,
          "email": true
        },
        "continuousSpeeding": {
          "inApp": false,
          "email": true
        },
        "continuousIdling": {
          "inApp": false,
          "email": true
        },
        "cumulativeIdling": {
          "inApp": false,
          "email": true
        },
        "stationary": {
          "inApp": false,
          "email": true
        },
        "vehComplianceReminder": {
          "inApp": false,
          "email": true
        },
        "vehCompliance": {
          "inApp": false,
          "email": true
        }
      }
    },
    "pois": {
      "enabled": false,
      "dwelling": {
        "inApp": false,
        "email": true
      },
      "entry": {
        "inApp": false,
        "email": true
      },
      "exit": {
        "inApp": false,
        "email": true
      }
    },
    "trip": {
      "enabled": false,
      "tripUpdates": {
        "inApp": true,
        "email": true
      },
      "start": {
        "inApp": false,
        "email": true
      },
      "nearDest": {
        "inApp": false,
        "email": true
      },
      "dwelling": {
        "inApp": false,
        "email": true
      },
      "delay": {
        "inApp": false,
        "email": true
      },
      "end": {
        "inApp": false,
        "email": true
      }
    },
    "driver": {
      "enabled": false,
      "driverComplianceReminder": {
        "inApp": false,
        "email": true
      },
      "driverCompliance": {
        "inApp": false,
        "email": true
      }
    },
    "maintenance": {
      "enabled": false,
      "maintenanceReminder": {
        "inApp": false,
        "email": true
      },
      "maintenanceAlert": {
        "inApp": false,
        "email": true
      }
    },
    "tyre": {
      "enabled": true,
      "tyreReminder": {
        "inApp": false,
        "email": true
      },
      "tyreAlert": {
        "inApp": false,
        "email": true
      }
    },
    "restrictedHours": {
      "enabled": true,
      "trucks": {
        "inApp": true,
        "email": true
      },
      "vans": {
        "inApp": true,
        "email": true
      }
    },
    "fuel": {
      "enabled": true,
      "trucks": {
        "rapidDrop": {
          "inApp": true,
          "email": true
        },
        "refuelingIncident": {
          "inApp": true,
          "email": true
        }
      },
      "vans": {
        "rapidDrop": {
          "inApp": true,
          "email": true
        },
        "refuelingIncident": {
          "inApp": true,
          "email": true
        }
      }
    }
  }
};

exports.access = {"assets": "full", "pois": "full", "trips": "full"};

exports.sensor = {
  temperature_sensor: {
    min_temperature: 0,
    max_temperature: 0,
    continuous_notification: 0,
    instant_notification: true
  }
};

exports.defaultAdminFuelSetting = {
  "enabledAdmin": false,
  "trucks": {
    "rapidDrop": {
      "inAppAdmin": false,
      "emailAdmin": false
    },
    "refuelingIncident": {
      "inAppAdmin": false,
      "emailAdmin": false
    }
  },
  "vans": {
    "rapidDrop": {
      "inAppAdmin": false,
      "emailAdmin": false
    },
    "refuelingIncident": {
      "inAppAdmin": false,
      "emailAdmin": false
    }
  }
};

exports.defaultAdminTemperatureSetting = {
  enabled: true,
  enabledAdmin: true,
  trucks: {
    rise: {
      level: '',
      trigger: '',
      inApp: false
    },
    drop: {
      level: '',
      trigger: '',
      inApp: false
    }
  },
  vans: {
    rise: {
      level: '',
      trigger: '',
      inApp: false
    },
    drop: {
      level: '',
      trigger: '',
      inApp: false
    }
  }
};
