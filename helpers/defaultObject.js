exports.geoRoute = {

  "hints": {
    "visited_nodes.average": "",
    "visited_nodes.sum": ""
  },
  "paths": [
    {
      "instructions": [
        {
          "distance": 0,
          "heading": 0,
          "sign": 0,
          "interval": [
            0,
            0
          ],
          "text": "",
          "time": 0,
          "street_name": ""
        }
      ],
      "descend": 0,
      "ascend": 0,
      "distance": 0,
      "bbox": [0],
      "weight": 0,
      "points_encoded": true,
      "points": "",
      "transfers": 0,
      "legs": [],
      "details": {},
      "time": 0,
      "snapped_waypoints": ""
    }
  ],
  "info": {
    "took": 0,
    "copyrights": [""]
  }

};