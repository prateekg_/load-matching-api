const models = require('../db/models');
const async = require('async');
const EOL_TRIP_TABLE = 'eol-trip-list';

/**
 * @description Get user trip table column preferences if set by user or returns default config.
 * @param {boolean} return_default set true to return default column config.
 * @param {integer} fk_c_id
 * @param {integer} fk_u_id
 * @param callback
 */
exports.getUserTripTableConfig = function (return_default, fk_c_id, fk_u_id, callback) {
  async.auto({
    default_config: function (cb) {
      let query = "SELECT * FROM wv_table_types WHERE table_name = :table_name AND fk_c_id = :fk_c_id;";

      models.sequelize.query(query, {replacements:{fk_c_id: fk_c_id, table_name: EOL_TRIP_TABLE}, type: models.sequelize.QueryTypes.SELECT}).spread(function (config) {
        cb(null, config)
      }).catch(function (err) {
        cb(err)
      })
    },
    user_config: function (cb) {
      let query = "SELECT uc.config FROM wv_user_configs uc" +
                " INNER JOIN wv_table_types tt on tt.id = uc.fk_wv_table_type_id" +
                " WHERE uc.fk_user_id = :fk_u_id AND tt.table_name = :table_name";

      models.sequelize.query(query, {replacements:{fk_u_id: fk_u_id, table_name: EOL_TRIP_TABLE}, type: models.sequelize.QueryTypes.SELECT}).spread(function (user_config) {
        cb(null, user_config)
      }).catch(function (err) {
        cb(err)
      })
    }
  }, function (err, results) {
    if(err){
      callback(err)
    }
    let data = {
      config: results.user_config && results.user_config.config && results.user_config.config.tabs ? results.user_config.config : results.default_config.default_config
    };

    if(return_default){
      data.config = results.default_config.default_config
    }
    callback(null, data)
  })
};