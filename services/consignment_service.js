const async = require('async');
const models = require('../db/models');
const _ = require('lodash');
const utils = require('../helpers/util');
const nuDynamoDb = require('../helpers/dynamoDb');
const moment = require('moment');
const geolib = require('geolib');
const PolylineUtil = require('polyline-encoded');

function processWaypoints(trip, mainCb){
    
  async.auto({
    actual_log: function (cb) {
      if(trip.trip_log && trip.trip_log.trip){
        cb(null, trip.trip_log.trip)
      }else{
        cb(null, [])
      }
    },
    source: ['actual_log', function (results, cb) {
      let i = trip.consignor;
      let item = {
        dispatchAction: "pickup",
        type: "planned",
        lname: i.lname,
        arrivalTis: i.est_start_tis,
        departureTis: i.est_end_tis,
        dwellDuration: Math.round(moment.duration(i.stop_dur, 'seconds').asMinutes()),
        violations: 0,
        crossed: false,
        label: 'A',
        lat: i.lat,
        lon: i.lon,
        avgSpd: 0,
        distance: 0
      };

      let matchedIndex = _.findIndex(results.actual_log, function (o) {
        // Check if poi id is same or lat,lon within 50 meters
        return i.Id === o.fk_poi_id || geolib.isPointInCircle({
          latitude: o.lat,
          longitude: o.lon
        },{
          latitude: i.lat,
          longitude: i.lon
        }, 50)
      });

      if(matchedIndex >= 0){
        let matchedItem = results.actual_log[matchedIndex];
        item.arrivalTis = matchedItem.StartTis;
        item.departureTis = matchedItem.EndTis;
        item.dwellDuration = Math.round(moment.duration(matchedItem.DwellTime, 'seconds').asMinutes());
        item.crossed = true
      }
      cb(null, { item: item, matchedIndex: matchedIndex })
    }],
    destination: ['actual_log', function (results, cb) {
      let i = trip.consignee;
      let item = {
        dispatchAction: "dropoff",
        type: "planned",
        lname: i.lname,
        arrivalTis: i.est_start_tis,
        departureTis: i.est_end_tis,
        dwellDuration: Math.round(moment.duration(i.stop_dur, 'seconds').asMinutes()),
        violations: 0,
        crossed: false,
        label: 'B',
        lat: i.lat,
        lon: i.lon,
        avgSpd: 0,
        distance: 0
      };

      let matchedIndex = _.findLastIndex(results.actual_log, function (o) {
        // Check if poi id is same or lat,lon within 50 meters
        return i.Id === o.fk_poi_id || geolib.isPointInCircle({
          latitude: o.lat,
          longitude: o.lon
        },{
          latitude: i.lat,
          longitude: i.lon
        }, 50)
      });

      if(matchedIndex >= 0){
        let matchedItem = results.actual_log[matchedIndex];
        item.arrivalTis = matchedItem.StartTis;
        item.departureTis = matchedItem.EndTis;
        item.dwellDuration = Math.round(moment.duration(matchedItem.DwellTime, 'seconds').asMinutes());
        item.crossed = true
      }
      cb(null, { item: item, matchedIndex: matchedIndex })
    }],
    intransit: ['actual_log', 'source', 'destination', function (results, cb) {

      let startIndex = results.source.matchedIndex;
      let source = results.source.item;
      let endIndex = results.destination.matchedIndex;
      let destination = results.destination.item;
      let log = results.actual_log;
      let event = {
        type: "intransit",
        lname: '',
        arrivalTis: source.departureTis || null,
        departureTis: destination.arrivalTis || null,
        dwellDuration: Math.round(moment.duration((destination.arrivalTis - source.departureTis), 'seconds').asMinutes()) || 0,
        violations: 0,
        crossed: false,
        label: '',
        lat: null,
        lon: null,
        avgSpd: 0,
        distance: 0
      };

      let segment = log.splice(startIndex, endIndex);
      let distance = Number((_.sumBy(segment, 'Distance') /1000).toFixed(1));
      event.crossed = distance > 0;
      event.distance = distance;
      event.avgSpd = _.meanBy(segment, 'AvgSpd');

      cb(null, event)
    }]
  }, function (err, results) {
    let log = [];
    log.push(results.source.item);
    log.push(results.intransit);
    log.push(results.destination.item);

    mainCb(null, log)
  })
}

/**
 * Validates unique consignment no. in a company
 * Used in create and update consignment controller function
 * @param {Integer} consignment_no - Consignment no.
 * @param {Integer} company_id - company id
 * @param consignment_id
 * @param callback
 */
exports.validateUniqueConsignment = function(consignment_no, company_id, consignment_id, callback){
  let current_cons = consignment_id? " and id != "+consignment_id : "";
  let query = "select ext_consignment_no as consignment_no   "+
    " from consignments con "+   
    " where fk_c_id = " + company_id + " and ext_consignment_no = " + consignment_no + current_cons;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (record) {
      callback(null, record)
    
    }).catch(function (err) {
      console.log("Error in consignment validation ",err);
      callback({code: 500, msg:err.message})
    })
};

exports.getConsignmentNumber = function(company_id, servCallback){
  let uniquConsignmentNo = false;
  let count = 1;
  async.until(
    function(){
      return (uniquConsignmentNo === true)
    },
    function(callback){
      let query = "select ext_consignment_no as consignment_no   "+
            " from consignments con "+   
            " where fk_c_id = " + company_id +
            " order by id desc limit 1";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (record) {
          if(record && record.consignment_no){
            let new_consignment_no = record.consignment_no + count;
            exports.validateUniqueConsignment(new_consignment_no, company_id, null, function(err, result){
              if(result.length === 0){
                uniquConsignmentNo = true;
              }
            });
            setTimeout(function () {
              callback(null, new_consignment_no);
            }, 1000);
            count++
          }
          else{
            uniquConsignmentNo = true;
            callback(null, null);
          }
            
        }).catch(function (err) {
          return servCallback({code: 500, msg:err.message})
        })
    },
    function (err, consignment_no) {
      servCallback(null, {consignment_no: consignment_no})
    }
  )
    
};

exports.getCurrentConsignmentStatusCounts = function(req, company_id, group_ids, servCallback){

  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let gid = req.query.gid;
  let groupCondition = gid ? ` g.gid = '${gid}'` : ` g.id in ${groupIds}`;

  let query =
            `SELECT con.status, count(DISTINCT con.id)::INT
            FROM consignments con
            INNER JOIN consignment_group_mappings cgm ON cgm.fk_consignment_id = con.id and cgm.connected
            INNER JOIN groups g ON cgm.fk_group_id = g.id and cgm.connected
            WHERE con.fk_c_id = ${company_id} AND con.status != 'delivered'
            AND con.active AND g.active AND g.fk_c_id = con.fk_c_id AND ${groupCondition} 
            GROUP BY con.status;`;
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (data) {
      let record = {
        unassigned_count:0,
        assigned_count:0,
        picked_count:0,
        incomplete_count:0
      };

      data.forEach(function (i) {
        switch (i.status){
        case 'unassigned':
          record.unassigned_count = i.count;
          break;
        case 'assigned':
          record.assigned_count = i.count;
          break;
        case 'picked':
          record.picked_count = i.count;
          break;
        case 'incomplete':
          record.incomplete_count = i.count;
          break;
        }
      });

      servCallback(null, record)
    
    }).catch(function (err) {
      console.log("Error in consignment validation ",err);
      servCallback({code: 500, msg:err.message})
    })
};

/**
 * 
 * @param {Object} params 
 * @param {Object} headers 
 * @param {Array} group_ids - Array of groups ids
 * @param {*} servCallback - callback
 */
exports.getConsignmentStatusCounts = function(params, headers, group_ids, servCallback){
  let query = `select cstatus as status, delayed, count(*) from (               
        select 
            case when cs.status is null
                    then con.status
                else cs.status
            end as cstatus,
            case when cs.status = 'picked' and cs.tis > (con.consignor->>'tis')::int
                    then true
                when cs.status = 'picked' and cs.tis <= (con.consignor->>'tis')::int
                    then false
                when cs.status = 'delivered' and cs.tis > (con.consignee->>'tis')::int
                    then true
                when cs.status = 'delivered' and cs.tis <= (con.consignee->>'tis')::int
                    then false
            end as delayed
        from consignments con
        left join (
            select max(tis), cs.fk_consignment_id from consignment_statuses cs
            inner join consignments cons on cons.id = cs.fk_consignment_id
            where cons.fk_c_id =`+ headers.fk_c_id + `
            group by cs.fk_consignment_id
        ) cstat on cstat.fk_consignment_id = con.id
        left join consignment_statuses cs on cs.fk_consignment_id = cstat.fk_consignment_id and cs.tis = cstat.max
        where con.fk_c_id = `+headers.fk_c_id +` and ((cstat.max between `+params.start_tis + ` and `+params.end_tis + `) or 
            ((extract(epoch from con."createdAt"))::int >= `+params.start_tis + ` and 
            (extract(epoch from con."createdAt"))::int < `+params.end_tis + `))
        ) cdata
        group by cdata.cstatus, delayed        `;
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (record) {
      servCallback(null, record)
    
    }).catch(function (err) {
      console.log("Error in consignment validation ",err);
      servCallback({code: 500, msg:err.message})
    })
};

/**
 * Get all the consignment shared with an external user
 * @param {Object} data - email
 * @param servCallback
 */
exports.getSharedConsignments = function(data, servCallback){
  const limit = data.limit && data.limit >= 0 ? data.limit : 10;
  const page = data.page && data.page >= 0 ? data.page : 0;
  const offset = limit * page;
  const searchKeyword = data.q ? data.q.toLowerCase() : '';
  const consignmentStatus = data.status;
  const start_tis = data.start_tis ? data.start_tis : null;
  const end_tis = data.end_tis ? data.end_tis : null;
  const search_cols = data.search || null;
  let search_col_condition = "";
  const sort = data.sort;
  let sort_condition = " ORDER BY id DESC ";

  const consignmentStatusCondition = consignmentStatus ? ` AND con.status = '${consignmentStatus}'` : "";

  const tableSearchCondition = searchKeyword ? ` AND (lower(invoice_no) like '%${searchKeyword}%' OR
        " lower(material_type) like '%${searchKeyword}%' OR lower(product_code) like '%${searchKeyword}%' OR
        " lower(consignor->>'lname') like '%${searchKeyword}%' OR lower(consignee->>'lname') like '%${searchKeyword}%')` : "";

  search_col_condition = utils.processTableColumnSearch(search_cols, [{
    db: 'invoice_no',
    client: 'invoice_no'
  }, {
    db: 'material_type',
    client: 'material_type'
  }, {
    db: 'product_code',
    client: 'product_code'
  },{
    db: "consignor->>'lname'",
    client: 'consignor.lname'
  },{
    db: "consignee->>'lname'",
    client: 'consignee.lname'
  },{
    db: 'a.lic_plate_no',
    client: 'asset.licNo'
  },{
    db: 'tp.status',
    client: 'trip.status'
  }]);

  sort_condition = utils.processTableColumnSort(sort, [{
    db: 'total_packages',
    client: 'total_packages'
  }, {
    db: 'weight',
    client: 'weight'
  }],sort_condition);

  let timeFrameCondition = (start_tis && end_tis) ? ` and extract(epoch from con."updatedAt")::int between ${start_tis} and ${end_tis} ` : '';

  let query = `SELECT con.id, invoice_no, ext_consignment_no as consignment_no, consignor, consignee, material_type, product_code, total_packages, weight,
        json_build_object('id',a.id,'type',ast.type,'operator', c.cname, 'licNo', a.lic_plate_no) as asset,
        json_build_object('id',tp.id,'status',tp.status) as trip, tp.oth_details->>'drivers' as drivers,
        count(con.id) OVER()::INT AS total_count
        FROM consignment_user_mapping_externals cume
        INNER JOIN user_externals ue ON ue.id = cume.fk_user_external_id
        INNER JOIN consignments con on cume.fk_consignment_id = con.id
        LEFT JOIN trip_plannings tp on tp.id = con.fk_trip_id
        LEFT JOIN assets a on a.id = tp.fk_asset_id
        LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id
        LEFT JOIN companies c on c.id = a.fk_c_id
        WHERE ue.email = '${data.email}' and con.active ${consignmentStatusCondition + search_col_condition + 
        tableSearchCondition + timeFrameCondition + sort_condition }
        LIMIT ${limit} OFFSET ${offset}`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (consignments) {
      let total_count = 0;

      consignments.forEach(function (consignment) {
        total_count = consignment.total_count;
        consignment.drivers = utils.cleanDriverObject(consignment.drivers);
        consignment.total_count = undefined
      });

      servCallback(null, {
        count: total_count,
        data: consignments
      })
    }).catch(function (err) {
      servCallback({code: 500, msg:err.message})
    })
};

/**
 * This service is used by 3pl entity to show consignment info that shared to an external users
 * @param {Object} data - id, email
 * @param servCallback
 */
exports.get3plInfo = function (data, servCallback) {
  let query = `SELECT tc.invoice_no, tc.status, tc.consignor, tc.consignee, ext_consignment_no as consignment_no, material_type, product_code, total_packages, weight,
        json_build_object('id', tp.id) as trip,
        json_build_object('id', a.id, 'licNo', a.lic_plate_no,'type', ast.type) as asset, tp.oth_details->>'drivers' as drivers
        FROM consignments tc
        INNER JOIN consignment_user_mapping_externals cume on tc.id = cume.fk_consignment_id
        INNER JOIN user_externals ue ON ue.id = cume.fk_user_external_id AND ue.email = '${data.email}'
        LEFT JOIN trip_plannings tp on tp.id = tc.fk_trip_id
        LEFT JOIN assets a on a.id = tp.fk_asset_id
        LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id
        WHERE tc.active and tc.id = ${data.id}`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (record) {
      if(!record){
        servCallback({code:404, msg:"Record not found"})
      }else{

        record.consignor = _.omit(record.consignor, ['fk_poi_id','loading','unloading','active','type']);
        record.consignee = _.omit(record.consignee, ['fk_poi_id','loading','unloading','active','type']);
        record.drivers = utils.cleanDriverObject(record.drivers);

        if(record.consignor && !record.consignor.act_start_tis){
          record.consignor.act_start_tis = null;
          record.consignor.act_end_tis = null
        }

        if(record.consignee && !record.consignee.act_start_tis){
          record.consignee.act_start_tis = null;
          record.consignee.act_end_tis = null
        }

        servCallback(null, record)
      }
    }).catch(function (err) {
      servCallback({code:500, msg:"Internal server error", err: err})
    })
};

/**
 * This service is used by 3pl entity to show consignments that shared to an external users
 * @param {Object} data - id, email
 * @param servCallback
 */
exports.get3plMap = function (data, servCallback) {
  async.auto({
    consignment: function (cb) {
      let query = "SELECT tc.consignor, tc.consignee, a.id as fk_asset_id" +
                " FROM consignments tc" +
                " LEFT JOIN trip_plannings tp on tp.id = tc.fk_trip_id" +
                " LEFT JOIN assets a on a.id = tp.fk_asset_id" +
                " INNER JOIN consignment_user_mapping_externals cume on tc.id = cume.fk_consignment_id"+
                " INNER JOIN user_externals ue ON ue.id = cume.fk_user_external_id AND ue.email = '" + data.email + "'"+
                " WHERE tc.active and tc.id = " + data.id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (record) {
          if(!record){
            cb({
              code: 404,
              msg: "Record not found"
            })
          }else{
            cb(null, record)
          }
        }).catch(function (err) {
          cb({
            code: 500,
            msg: "Internal server error",
            err: err
          })
        })
    },
    waypoints : ['consignment', function (result, cb) {
      let consignment = result.consignment;
      cb(null, [
        _.pick(consignment.consignor,['lat','lon','seq_no','lname']),
        _.pick(consignment.consignee,['lat','lon','seq_no','lname'])
      ])
    }],
    actualPath: ['consignment', function (result, cb) {
      let consignment = result.consignment;
      console.log(" ================ consignment ", consignment);
      if(consignment.fk_asset_id && consignment.consignor && consignment.consignor.act_start_tis &&
                consignment.consignee && consignment.consignee.act_start_tis){

        let params = {
          fk_asset_id: consignment.fk_asset_id,
          actStartTis: consignment.consignor.act_start_tis,
          actEndTis: consignment.consignee.act_start_tis,
        };

        nuDynamoDb.getAssetLocationFromAPI(params, function (err, data) {
          if(data.length > 0){

            let latlngs = [];
            data.forEach(function (i) {
              latlngs.push([i.lat, i.lon])
            });
            cb(null, PolylineUtil.encode(latlngs))
          }else{
            cb(null, "")
          }
        })
      }else{
        cb(null, "")
      }
    }]
  }, function (err, results) {
    if(err){
      servCallback({code: err.code, msg: err.msg})
    }else{
      servCallback(null, _.omit(results, ['consignment']))
    }
  })
};

/**
 * This service is used by 3pl entity to show consignment log that shared to an external users
 * @param {Object} data - id, email
 * @param servCallback
 */
exports.get3plLog = function (data, servCallback) {
  let query = "SELECT tc.consignor, tc.consignee, tp.status, tp.waypoints, tp.trip_log " +
    " FROM consignments tc" +
    " LEFT JOIN trip_plannings tp on tp.id = tc.fk_trip_id" +
    " INNER JOIN consignment_user_mapping_externals cume on tc.id = cume.fk_consignment_id"+
    " INNER JOIN user_externals ue ON ue.id = cume.fk_user_external_id AND ue.email = '" + data.email + "'"+
    " WHERE tc.active and tc.id = " + data.id;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (record) {
      if(!record){
        servCallback({code: 404, msg: "Record not found"})
      }else{
        processWaypoints(record, function (err, data) {
          servCallback(null,data)
        });
      }
    }).catch(function (err) {
      console.log("*********** Error **********",err);
      servCallback({code: 500, msg: err})
    })
};

/**
 * Validates unique consignment no. in a company
 * Used in create and update consignment controller function
 * @param {Integer} invoice_no - Consignment no.
 * @param {Integer} company_id - company id
 * @param consignment_id
 * @param callback
 */
exports.validateUniqueInvoiceNo = function(invoice_no, company_id, consignment_id, callback){
  let current_cons = consignment_id? " and id != "+consignment_id : "";
  let query = "select invoice_no   "+
    " from consignments con "+   
    " where fk_c_id = " + company_id + " and invoice_no = '" + invoice_no + "' " + current_cons;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (record) {
      callback(null, record)
    
    }).catch(function (err) {
      console.log("Error in consignment validation ",err);
      callback({code: 500, msg:err.message})
    })
};

exports.get3plConsignmentStatusCounts = function(data, servCallback){
  let query = `WITH cons AS ( 
                SELECT array_agg(consg.fk_consignment_id) as ex_cons 
                FROM consignment_user_mapping_externals consg
                INNER JOIN user_externals ue ON ue.id = consg.fk_user_external_id AND ue.email = '${data.email}'
                WHERE consg.active
                 )
                SELECT (SELECT count(*)::int from consignments con where con.active  AND con.status ='unassigned' and con.id = ANY ( SELECT unnest(ex_cons) FROM cons)
                 ) as unassigned_count,
                    (SELECT count(*)::int from consignments con where con.active  AND con.status = 'assigned' and con.id = ANY ( SELECT unnest(ex_cons) FROM cons) ) as assigned_count,  
                    (SELECT count(*)::int from consignments con where con.active  AND con.status = 'picked' and con.id = ANY ( SELECT unnest(ex_cons) FROM cons) ) as picked_count,     
                    (SELECT count(*)::int from consignments con where con.active  AND con.status = 'incomplete' and con.id = ANY ( SELECT unnest(ex_cons) FROM cons) ) as incomplete_count`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (record) {
      servCallback(null, record)
    
    }).catch(function (err) {
      console.log("Error in consignment validation ",err);
      servCallback({code: 500, msg:err.message})
    })
};

/**
 * Map consignment to groups (consignment_group_mappings)
 * @param {object} consignment
 * @param callback
 */
exports.mapToGroups = function (consignment, callback) {
  /**
     * 1. get poi ids from the consignor and consignee object from consignment
     * 2. find all groups which has access to the poi
     * 3. create consignment_group_mapping record
     */

  async.auto({
    pois: function (cb) {
      let pois = [];
      if(consignment.consignor){
        if(consignment.consignor.poi_id && Number(consignment.consignor.poi_id)){
          pois.push(consignment.consignor.poi_id)
        }
      }
      if(consignment.consignee){
        if(consignment.consignee.poi_id && Number(consignment.consignee.poi_id)){
          pois.push(consignment.consignee.poi_id)
        }
      }
      cb(null, pois)
    },
    groups: ['pois', function (result, cb) {
      let pois = result.pois;
      if(pois.length === 0){
        return cb()
      }
      let query = `SELECT DISTINCT pgm.fk_group_id FROM poi_group_mappings pgm WHERE pgm.connected AND pgm.fk_poi_id IN (${pois})`;
      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function (groups) {
          cb(null, groups)
        }).catch(function (err) {
          cb(err)
        })
    }],
    create_mapping: ['groups', function (result, cb) {
      let groups = result.groups;
      async.each(groups, function (group, record_cb) {
        group.fk_consignment_id = consignment.id;
        group.tis = moment().unix();

        models.consignment_group_mapping.findOrCreate({
          where: {
            fk_consignment_id: group.fk_consignment_id,
            fk_group_id: group.fk_group_id,
            connected: true
          }, defaults: group})
          .spread(function () {
            record_cb()
          }).catch(function () {
            record_cb()
          })
      }, function (err) {
        if(err){
          cb(err)
        }else{
          cb(null, groups)
        }
      })
    }]
  }, function (err, results) {
    console.error("ERROR IN mapToGroups() : ", err);
    callback(err, results)
  })
};

/**
 * Function to share or unshare consignment from groups
 * @param consignment
 * @param groups
 * @param callback
 */
exports.mapConsignmentToGroups = function (consignment, groups, callback) {
  async.each(groups, function (group, record_cb) {

    let mapping = {
      fk_consignment_id: consignment.id,
      fk_group_id: group.id,
      connected: group.shared,
      tis: moment().unix()
    };

    async.auto({
      is_admin: function (cb) {
        let query = `SELECT * FROM groups WHERE id = ${group.id}`;
        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .spread(function (group) {
            cb(null, group.is_admin)
          }).catch(function (err) {
            cb(err)
          })
      },
      unmap: ['is_admin', function (result, cb) {

        if(result.is_admin){
          return cb()
        }
        if(mapping.connected){
          return cb()
        }else{
          models.consignment_group_mapping.update({
            connected: false
          },{
            where: {
              fk_consignment_id: mapping.fk_consignment_id,
              fk_group_id: mapping.fk_group_id,
              connected: true
            }
          }).spread(function () {
            cb()
          }).catch(function () {
            cb()
          })
        }
      }],
      map: ['unmap', function (result, cb) {
        models.consignment_group_mapping.findOrCreate({
          where: {
            fk_consignment_id: mapping.fk_consignment_id,
            fk_group_id: mapping.fk_group_id,
            connected: mapping.connected
          }, defaults: mapping})
          .spread(function (mapping, created) {
            if (!created) {
              cb()
            }else{
              // mapping created
              cb()
            }
          }).catch(function () {
            cb()
          })
      }]
    }, function (err, results) {
      record_cb(err, results)
    })
  }, function () {
    callback()
  })
};