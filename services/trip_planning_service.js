const async = require('async');
const models = require('../db/models');
const _ = require('lodash');
const moment = require('moment');
const tagsHelper = require('../helpers/tags');
const request = require('request');
const nodemailer = require('nodemailer');
const env       = process.env.NODE_ENV || 'development';
const config    = require('../config/index')[env];
const mailerConfig = config.mailer;
const MagicIncrement = require('magic-increment');
const utils = require('../helpers/util');

function checkIfAssetBelongsToCompany (newTrip, companyId, cb) {

  let whereCondition = {
    active : true
  };

  if(Number(newTrip.assetId)){
    whereCondition.id = newTrip.assetId
  }else{
    whereCondition.asset_id = newTrip.assetId
  }

  models.asset.findOne({
    attributes : ['id', 'fk_ast_type_id'],
    where : whereCondition,
    raw : true
  }).then(function (asset) {
    if(!asset) {
      return cb({
        code : 404,
        message : 'Invalid asset ID'
      })
    }
    newTrip.fk_asset_id = asset.id;
    newTrip.fk_asset_type_id = asset.fk_ast_type_id;
    delete newTrip.assetId;
    cb(null, asset);
  }).catch(function (err) {
    cb(err);
  })
}

function checkIfPoiBelongsToCompany(validateWayPoint, companyId, cb) {
  validateWayPoint = _.uniq(validateWayPoint);
  models.poi.findAll({
    attributes: ['id'],
    where: {
      id: {
        $in: validateWayPoint
      },
      $or: {
        fk_c_id: companyId,
        priv_typ: false
      }
    },
    raw : true
  }).then(function (pois) {
    if(pois.length !== validateWayPoint.length) {
      return cb({
        code : 404,
        message : 'Invalid poi ID'
      })
    }
    cb(null, pois);
  }).catch(function (err) {
    cb(err);
  })
}

function processTripPlanTemplate(trip, cb){
  if(trip.custom_fields && trip.custom_fields.length > 0){
    const fk_c_id = trip.fk_c_id;
    // find a template
    let newTemplate = {
      fk_c_id: fk_c_id,
      custom_fields: trip.custom_fields
    };

    models.trip_plan_template.findOrCreate({where: {fk_c_id: fk_c_id}, defaults: newTemplate})
      .spread(function (template, created) {
        if (!created) {
          // update the template
          newTemplate.custom_fields = _.merge(template.custom_fields, newTemplate.custom_fields);
          models.trip_plan_template.update(newTemplate, {
            where : {
              fk_c_id : fk_c_id
            }
          }).then(function () {
            cb()
          }).catch(function () {
            cb()
          })
        }else{
          // template created
          cb()
        }
      }).catch(function () {
        cb()
      })
  }else{
    cb()
  }
}

function processTripSharingWithExternalUsers(newTrip,mainCallback){
    
  if(newTrip.shared_with && newTrip.shared_with.length > 0){
    let transporter = nodemailer.createTransport(mailerConfig);
    let subject = "Trip shared access - "+ newTrip.tid;

    async.each(newTrip.shared_with, function (newExternalUser, callback) {

      async.auto({
        creatorInfo: function (cb) {
          let query = `SELECT u.name, c.cname FROM users u 
                            LEFT JOIN companies c on c.id = u.fk_c_id
                            WHERE u.id = ${newTrip.fk_u_id};`;
          models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
            .spread(function (record) {
              cb(null, record)
            }).catch(function () {
              cb()
            })
        },
        external_user: function (ext_user_cb) {
          models.user_external.findOrCreate({
            where: {
              email: newExternalUser.email
            },
            defaults: newExternalUser
          }).spread(function (user_external) {
            ext_user_cb(null, {id:user_external.id})
          }).catch(function (err) {
            ext_user_cb(err)
          })
        },
        share_trip: ['external_user', function (result, ext_user_cb) {
          let external_user = result.external_user;
          let user_trip_mapping = {
            fk_trip_id: newTrip.fk_trip_id,
            fk_u_id: newTrip.fk_u_id,
            fk_user_external_id: external_user.id,
            fk_c_id: newTrip.fk_c_id
          };

          if(newExternalUser.active && newExternalUser.active === false){
            // disable trip sharing
            models.trip_user_mapping_external.update({
              active: false
            },{
              where: {
                fk_user_external_id: external_user.id,
                fk_trip_id: user_trip_mapping.fk_trip_id
              },
              defaults: user_trip_mapping
            }).spread(function () {
              ext_user_cb(null, {created: false})
            }).catch(function () {
              ext_user_cb(null, {created: false})
            })

          }else{

            models.trip_user_mapping_external.findOrCreate({
              where: {
                fk_user_external_id: external_user.id,
                fk_trip_id: user_trip_mapping.fk_trip_id,
                active: true
              },
              defaults: user_trip_mapping
            }).spread(function (mapping, created) {
              ext_user_cb(null, {created: created})
            }).catch(function () {
              ext_user_cb(null, {created: false})
            })
          }
        }],
        send_mail: ['share_trip', 'creatorInfo', function (result, ext_user_cb) {
          if(result.share_trip.created) {
            let user = result.creatorInfo;
            let trip_url = config.url.domain +"/tracking";

            let mailOptions = {
              from: 'noreply@numadic.com',
              to: newExternalUser.email,
              bcc: 'noreply@numadic.com',
              subject: subject,
              html: `<html>
                                       <body style="width:80%; color: #444444; font-family: tahoma, sans-serif; margin: 50px;">
                                          <p>&nbsp;</p>
                                          <p>Hello,</p>
                                          <p>${user.name} at ${user.cname} has shared a trip tracking link with you.</p>
                                          <p>Please click the link below, or copy and paste it into your browser.</p>
                                          <p><a href="${trip_url}">${trip_url}</a></p>
                                          <p><br />This tracking link was generated on Numadic's Fleet & Consignment management platform. Learn more <a href="https://numadic.com/demo">here</a>.<br /><br />
                                             <br />Thank you,</span>
                                          </p>
                                          <div>&nbsp;</div>
                                          <div>Numadic  Team</div>
                                       </body>
                                    </html>`
            };

            transporter.sendMail(mailOptions, function (error) {
              if (error) {
                console.log(error);
              }
              ext_user_cb()
            });
          }else{
            ext_user_cb()
          }
        }]
      }, function () {
        callback()
      })
    }, function () {
      mainCallback()
    })
  }else{
    mainCallback()
  }
}

function processConsignmentTripMapping(data, mainCallback) {
  let consignments = data.fk_consignment_ids;
  if( consignments && consignments.length > 0){
    async.waterfall([
      //Remove all consignments for assigned to a trip.
      //This is added incase some old consignments have been unassigned and also because client sends only updated consignments ids
      function removeAllconsignments(callback){
        models.consignment.update({
          status: 'unassigned',
          fk_trip_id: null
        }, {
          where : {
            fk_trip_id: data.fk_trip_id
          }
        }).then(function () {
          callback()
        }).catch(function (err) {
          console.log(" ########### Error ###########", err);
          callback()
        })
      },
      //Add again all the updated consignments to a trip
      function addUpdatedConsignments(callback){
        models.consignment.update({
          status: 'assigned',
          fk_trip_id: data.fk_trip_id
        }, {
          where : {
            id : consignments,
            fk_c_id: data.fk_c_id,
            active: true
          }
        }).then(function () {
          callback()
        }).catch(function (err) {
          console.log(" ########### Error ###########", err);
          callback()
        })
      }
    ], function(err){
      if(err) mainCallback(err);
      else mainCallback()
    })
        
  }else{
    mainCallback()
  }
}

function getAutoTripDetails(groupCondition, fk_company_id, trip_id, callback) {

  let query = "SELECT atp.*, json_build_object('assetId',a.asset_id,'licNo',a.lic_plate_no,'assetName',a.name, "+
    " 'type', ast.type,'length', a.length,'grossWeightRegistered', a.gross_wt_registered,'bodyType',a.body_type, "+
    " 'status',null,'spd',null,'lname', null) as asset, COALESCE(json_agg(DISTINCT g.gid) FILTER (WHERE g.gid IS NOT NULL), '[]') AS gids FROM auto_trips atp "+
    " INNER JOIN assets a on a.id = atp.fk_asset_id " +
    " INNER JOIN asset_types ast on ast.id = a.fk_ast_type_id "+
    " LEFT JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected "+
    " LEFT JOIN groups g ON g.id = agm.fk_group_id AND "+ groupCondition +" AND g.fk_c_id = "+ fk_company_id +
    " WHERE atp.tid = '"+ trip_id +"'" +
    " GROUP BY atp.id, a.asset_id, a.lic_plate_no, a.name, ast.type, a.length, a.gross_wt_registered, a.body_type";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (trip) {
      if (!trip) {
        callback('Not found');
      }else{

        let lklocation = trip.trip_log.lkLocation;
        if(lklocation){
          let truck_status = 'stationary';
          if(lklocation.ign === 'B' && lklocation.spd > 0){
            truck_status = "moving"
          }else if(lklocation.ign === 'B' && lklocation.spd === 0){
            truck_status = "idling"
          }

          trip.asset.status = truck_status;
          trip.asset.spd = lklocation.spd;
          trip.asset.lname = lklocation.lname
        }

        let wp = [];

        let t = {
          auto: true,
          tripId: trip.tid,
          tripName: null,
          gids: trip.gids,
          startTis: trip.trip_log.departure,
          endTis: trip.trip_log.arrival,
          status: trip.trip_log.active ? "inprogress" : "completed",
          asset: trip.asset,
          waypoints: wp,
          tags: [],
          customer: {emailId: "", lastName: "", mobileNo: "", firstName: "", companyName: ""},
          driver: {lastName: "", mobileNo: "", firstName: ""}
        };

        let waypoints = trip.trip_log.trip;
        if(waypoints){
          let seqNO = 1;
          async.eachOf(waypoints, function (i, key, cb) {
            if(i.Id > 0 || key === 0 || i.DwellTime >= 3600 ){
              wp.push({
                id: null,
                poiId: null,
                lname: i.LName,
                lat: i.Lat,
                lon: i.Lon,
                scheduledTis: null,
                markerLabel: seqNO,
                loading: false,
                unloading: false,
                stopDur: i.DwellTime
              });
              seqNO++
            }
            cb()
          }, function () {
            t.waypoints = wp;
            callback(null, t)
          })
        }else{
          callback(null, t)
        }
      }
    }).catch(function () {
      callback('Internal server error');
    })

}

/**
 * Disables scheduled trip and removes consignment linked to the trip
 * @param {Integer} trip_id - Trip id
 * @param {Integer} companyId - Company id
 * @param servCallback
 */

exports.disableScheduledTrip = function(companyId, trip_id, servCallback){
  let whereCondition = {
    fk_c_id : companyId,
    status: "scheduled"
  };

  if(Number(trip_id)){
    whereCondition.id = trip_id
  }else{
    whereCondition.tid = trip_id
  }
  models.trip_planning.update({active : false}, {
    where : whereCondition
  }).spread(function (trip) {
    if(trip === 0) {
      servCallback({code:422, msg:'Invalid trip/ Only scheduled trip can be deleted'})
    }
    else{
      models.consignment.update({
        fk_trip_id : null,
        status: "unassigned"
      }, {
        where : {
          fk_trip_id: trip_id
        }
      }).spread(function () {
        servCallback(null,'Trip deleted successfully')
      })
    }
        
  }).catch(function () {
    servCallback({code: 500, msg:'Internal server error'})
  })
};

/**
 * Gets consignment trip details
 * @param {Integer} trip_id - Trip id
 * @param {Integer} companyId - Company id
 * @param groupIds
 * @param gid
 * @param servCallback
 */
exports.getTripDetails = function(companyId, trip_id, groupIds, gid, servCallback){
  let groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  let tripIdCondition = Number(trip_id) ? " tp.id = "+ trip_id : "tp.tid = '" + trip_id + "'";

  let query = "select tp.id, tp.new as \"isNew\", false as auto, tp.tid as \"tripId\", tp.name as \"tripName\", "+
    "   tp.start_tis as \"startTis\", tp.end_tis as \"endTis\", tp.shipment_no,"+
    "   tp.geo_trip_route::jsonb as \"geoRoute\", tp.oth_details::jsonb, tp.status,"+
    "   json_build_object('id', a.id,'assetId',a.asset_id,'operator', json_build_object('name', c.cname),'licNo',a.lic_plate_no, 'assetName', a.name,'type',ast.type,"+
    "   'status',null,'spd',null,'lname',null) as asset, COALESCE(json_agg(DISTINCT g.gid) FILTER (WHERE g.gid IS NOT NULL), '[]') AS gids,"+
    "   json_agg(json_build_object('id',wp.id,'poiId',wp.fk_poi_id,'lname', wp.lname,'lat', wp.lat,'lon', wp.lon,'scheduledTis', wp.tis,'markerLabel', wp.seq_no,'loading',"+
    "   wp.loading,'unloading', wp.unloading,'stopDur',wp.stop_dur)) as waypoints," +
    "   tp.waypoints::jsonb as new_waypoints, tp.custom_fields, tp.shared_with," +
    "   (select array(select json_build_object('id',tc.id, 'invoice_no',invoice_no, 'consignment_no',ext_consignment_no, 'consignor',consignor, 'consignee',consignee, "+
    "                                          'material_type',material_type, 'product_code',product_code, 'total_packages',total_packages, 'weight',weight"+
    "               ) from consignments tc where tc.fk_trip_id = tp.id)) as consignments"+
    " from trip_plannings tp"+
    " left join waypoints wp on wp.fk_trip_plan_id = tp.id and wp.active is true"+
    " left join assets a on a.id = tp.fk_asset_id" +
    " left join asset_types ast on ast.id = a.fk_ast_type_id"+
    " LEFT JOIN companies c on c.id = a.fk_c_id"+
    " LEFT JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected "+
    " LEFT JOIN groups g ON g.id = agm.fk_group_id AND "+ groupCondition +" AND g.fk_c_id = "+ companyId +
    " where tp.fk_c_id = "+ companyId +" and "+ tripIdCondition +" and tp.active is true group by tp.id, a.id, ast.id, c.id";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (trip) {

      if(!trip) {
        getAutoTripDetails(groupCondition, companyId, trip_id, function (err, result) {
          if(err){
            servCallback({code: 404, msg:'Invalid Trip ID'})
          }else{
            servCallback(null,result)
          }
        })
      }else{
        async.waterfall([
          function(wCb){
            trip.waypoints =  _.uniqWith(_.orderBy(trip.waypoints, ['markerLabel', 'asc']), _.isEqual);
            trip.tags = _.uniq(trip.tags) || [];
            trip.customer = trip.oth_details.customer;
            trip.drivers = trip.oth_details.drivers || trip.oth_details.driver;
            trip.transporter = trip.oth_details.transporter;
            trip.shared_with = _.filter(trip.shared_with, function (o) {
              return o.active !== false
            });

            if(!Array.isArray(trip.drivers)){
              trip.drivers = [trip.drivers]
            }

            trip =  _.omit(trip,['trip_planning', 'oth_details']);
            wCb(null)
          },
          function(wCb){
            if(trip.isNew){
              let wplist = [];
              async.eachSeries(trip.new_waypoints,function (wp, callback) {
                let waypointObj = {
                  'id': wp.id,
                  'poiId': wp.fk_poi_id || null,
                  'lname': wp.lname,
                  'lat': wp.lat,
                  'lon': wp.lon,
                  'scheduledTis': wp.tis,
                  'markerLabel': wp.seq_no,
                  'loading': wp.loading,
                  'unloading': wp.unloading,
                  'stopDur':wp.stop_dur,
                  'geoBounds': []
                };

                if(!wp.fk_poi_id){
                  wplist.push(waypointObj);
                  return callback()
                }

                let query = 'SELECT p.geo_loc as bounds' +
                                ' FROM pois p' +
                                ' WHERE id = '+ wp.fk_poi_id;
                
                models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                  .spread(function (poi) {
                    if(poi && poi.bounds){
                      let geobounds = [];
                      let coordinates = poi.bounds.coordinates[0];
                      async.each(coordinates,function (i, eachCb) {
                        geobounds.push({
                          lon: i[0],
                          lat: i[1]
                        });
                        eachCb()
                      }, function(){          
                        waypointObj.geoBounds = geobounds;
                        wplist.push(waypointObj);
                        callback()
                      })
                    }
                    else{
                      wplist.push(waypointObj);
                      callback()
                    }
                  }).catch(function (err) {
                    console.log(err)
                  });
                                
              }, function(){
                trip.waypoints = wplist;
                wCb(null)
              })
                            
            }
            else wCb(null)
          }
        ], function(){
          delete trip.isNew;
          delete trip.new_waypoints;
          servCallback(null, trip)
        })  
      }
    }).catch(function (err) {
      servCallback({code: 500, msg:'Internal server error' + err});
    })
};

exports.getShipmentNumber = function(company_id, servCallback){
  let uniquShipmentNo = false;
  let new_shipment_no = "";
  async.until(
    function(){
      return (uniquShipmentNo === true)
    },
    function(callback){
      let query = "select shipment_no   "+
            " from trip_plannings tp "+  
            " inner join consignments c on tp.id = c.fk_trip_id" +
            " where tp.fk_c_id = " + company_id +
            ' order by tp."createdAt" desc limit 1';

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (record) {
          if(record && record.shipment_no){
            let unique_shipment_no = MagicIncrement.inc(record.shipment_no);
            let find_shipment_no = new_shipment_no ? new_shipment_no : unique_shipment_no;
            exports.validateUniqueShipment(find_shipment_no, company_id, null, function(err, result){
              if(result.length === 0){
                uniquShipmentNo = true;
                callback(null, find_shipment_no);
              }else{
                new_shipment_no = MagicIncrement.inc(find_shipment_no);
                callback(null, new_shipment_no);
              }
            });
          }
          else{
            uniquShipmentNo = true;
            callback(null, null);
          }
            
        }).catch(function (err) {
          return servCallback({code: 500, msg:err.message})
        })
    },
    function (err, shipment_no) {
      servCallback(null, {shipment_no: shipment_no})
    }
  )
    
};

/**
 * Validates unique shipment no. in a company
 * Used in create and update consignment controller function
 * @param {Integer} shipment_no - Shipment no.
 * @param {Integer} company_id - company id
 * @param trip_id
 * @param callback
 */
exports.validateUniqueShipment = function(shipment_no, company_id, trip_id, callback){
  let current_cons = trip_id? " and id != "+trip_id : "";
  let query = "select shipment_no   "+
    " from trip_plannings tp "+   
    " where fk_c_id = " + company_id + " and lower(shipment_no) = lower('" + shipment_no + "')" + current_cons;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (record) {
      callback(null, record)
    
    }).catch(function (err) {
      console.log("Error in shipment validation ",err);
      callback({code: 500, msg:err.message})
    })
};

exports.addTrip = function (headers, data, host, callback) {
  const companyId = headers.fk_c_id;
  let trip = data;
  let customer = data.customer || {};
  let drivers = data.drivers || [];
  let transporter = data.transporter || {};
  let wayPoints = trip.waypoints;
  let finalWayPoints = [];
  let validateWayPoint = [];
  let matrixWayPoints = [];
  let newTrip;

    
  try {
    newTrip = {
      name: trip.tripName,
      assetId: trip.assetId,
      fk_c_id : companyId,
      fk_route_template_id: trip.fk_route_template_id,
      start_tis: trip.startTis,
      end_tis: trip.endTis,
      geo_trip_route: trip.geoRoute,
      oth_details: {
        customer : {
          companyName : customer.companyName || '',
          firstName : customer.firstName || '',
          lastName : customer.lastName || '',
          mobileNo : customer.mobileNo || '',
          emailId : customer.emailId || ''
        },
        drivers : drivers,
        transporter: transporter,
        notes : trip.notes
      },
      status : 'scheduled',
      est_distance: Math.round(trip.geoRoute.paths[0].distance) || 0,
      est_time: Math.round(moment.duration(trip.geoRoute.paths[0].time).asMinutes()), //Converting milliseconds to minutes
      way_points: trip.waypoints.length || 0,
      tags : trip.tags || [],
      new : true,
      ext_tid: trip.ext_tid,
      custom_fields: trip.custom_fields || [],
      shared_with: trip.shared_with,
      past: trip.waypoints.length > 0 && moment().diff(moment.unix(trip.waypoints[0].scheduledTis), 'minutes') >= 30,
      shipment_no: trip.shipment_no,
      round_trip: utils.isRoundTrip(trip.waypoints)
       
    };
  } catch (e) {
    console.log("Create trip error:", e);
    return callback({code: 400, error:'Improper input data', msg: 'Improper input data'})
  }

  _.forEach(wayPoints, function (wayPoint) {
    let newWayPoint = {
      fk_poi_id: wayPoint.poiId || null,
      lname: wayPoint.lname,
      lat: wayPoint.lat,
      lon: wayPoint.lon,
      tis: wayPoint.scheduledTis,
      stop_dur: wayPoint.stopDur,
      seq_no: wayPoint.markerLabel,
      loading : wayPoint.loading === true,
      unloading : wayPoint.unloading === true,
      type: wayPoint.type || '',
      dispatchAction: wayPoint.dispatchAction,
      fk_consignmentIds: wayPoint.fk_consignmentIds
    };

    newTrip.est_time += newWayPoint.stop_dur || 0;

    if(newWayPoint.fk_poi_id !== null) {
      validateWayPoint.push(newWayPoint.fk_poi_id)
    }

    matrixWayPoints.push(newWayPoint.lat + ',' +newWayPoint.lon);
    finalWayPoints.push(newWayPoint)
  });

  newTrip = _.omitBy(newTrip, _.isNil); //Remove undefined or null;

  if(trip.way_points < 2) {
    return callback({code: 400, error:'Minimum two way points are required', msg: 'Minimum two way points are required'})
  }

  async.series({
    asset : function (cb) {
      checkIfAssetBelongsToCompany(newTrip, companyId, cb)
    },
    checkWayPoints : function (cb) {
      checkIfPoiBelongsToCompany(validateWayPoint, companyId, cb)
    },
    matrixResponse : function (cb) {
      cb()
    }
  }, function (err, result) {
    if(err) {
      callback({code: err.code || 500, msg: err.msg || 'Internal error'})
    }

    newTrip.est_matrix = result.matrixResponse;
    newTrip.waypoints = _.orderBy(finalWayPoints, ['seq_no'], ['asc']);

    let response = {};

    async.auto({
      tripId :  async.retryable({times: 5, interval: 200},
        function (cb) {
          let whereCondition = {id: null};
          if (trip.ext_tid) {
            whereCondition = {
              $or: {
                ext_tid: newTrip.ext_tid
              }
            }
          }
          models.trip_planning.findOrCreate({where: whereCondition, defaults: newTrip})
            .spread(function (trip, created) {
              if (!created) {
                if (newTrip.ext_tid) {
                  return cb({
                    code: 409,
                    error: 'External Trip Reference already exists, please change the name provided'
                  })
                }
                return cb({
                  code: 409,
                  error: 'Trip id already exists'
                })
              }
              response = {
                id: trip.get({plain : true}).id,
                tid: trip.get({plain : true}).id
              };
              cb(null, trip.get({plain : true}).id)
            }).catch(function (err) {
              console.log("******************************* Error ****************************");
              console.log(err);
              cb(err)
            })
        }),
      tripDetail : ['tripId', function (result, cb) {
        cb()
      }],
      wayPoint : ['tripId', function (result, cb) {
        cb()
      }],
      tags :  ['tripId', function (result, cb) {
        tagsHelper.processTags(newTrip.tags, result.tripId, companyId, tagsHelper.tagType.trip_plannings, cb)
      }],
      processTemplate: processTripPlanTemplate.bind(null, newTrip),
      shareWithExternalUsers: ['tripId', function(result, cb){
        processTripSharingWithExternalUsers({
          fk_trip_id: response.id,
          tid: response.tid,
          fk_u_id: headers.fk_u_id,
          fk_c_id: companyId,
          shared_with: newTrip.shared_with || []
        }, cb)
      }],
      consignment_mapping: ['tripId', function (result, cb) {
        processConsignmentTripMapping({
          fk_trip_id: response.id,
          fk_c_id: companyId,
          fk_consignment_ids: trip.fk_consignment_ids || null
        }, cb)
      }],
      processRoute: function (cb) {
        if (!trip.saveAsRoute) {
          return cb()
        }
        let sourceTis = moment.unix(wayPoints[0].tis);
        let routeWayPoints = wayPoints;
        _.each(routeWayPoints, function (waypoint) {
          waypoint.arrivalDays = moment.unix(waypoint.scheduledTis).diff(sourceTis, 'd');
          waypoint.arrivalTime = moment.unix(waypoint.scheduledTis).format("HH:mm");
          delete waypoint.scheduledTis
        });
                
        delete headers['content-length'];
                
        request.post({
          uri: 'http://' + host + '/routetemplates',
          headers: headers,
          json: {
            name: trip.routeName,
            waypoints: routeWayPoints,
            geoRoute: trip.geoRoute,
          }
        }, function (error) {
          if (error) {
            return cb(err)
          }
          return cb()
        })
      }
    }, function (err) {
      if (err) {
        if (err.code) {
          return callback({code: err.code, msg:{
            error: err.error
          }})
        } else {
          return callback({code: 400, msg:{
            error: 'Error in creating trip' + err
          }})
        }
      } else {
        callback(null,response);
      }
    })
  });
};

exports.updateTrip = function (headers, data, callback) {
  const companyId = headers.fk_c_id;
  let id = data.id || data.tripId;

  if(!id) {
    return callback({code: 400, msg: 'Invalid trip ID'});
  }

  let trip = data;
  let customer = trip.customer;
  let consignment_ids = trip.fk_consignment_ids;
  let drivers = trip.drivers || [];
  let transporter = trip.transporter || {};

  let wayPoints = trip.waypoints;
  let finalWayPoints = [];
  let validateWayPoint = [];
  let matrixWayPoints = [];
  let newTrip;

  try {
    newTrip = {
      fk_c_id: companyId,
      name: trip.tripName,
      assetId: trip.assetId,
      start_tis: trip.startTis,
      end_tis: trip.endTis,
      geo_trip_route: trip.geoRoute,
      oth_details: {
        customer : {
          companyName : customer.companyName || '',
          firstName : customer.firstName || '',
          lastName : customer.lastName || '',
          mobileNo : customer.mobileNo || '',
          emailId : customer.emailId || ''
        },
        drivers : drivers,
        transporter: transporter,
        notes : trip.notes
      },
      est_distance: Math.round(trip.geoRoute.paths[0].distance) || 0,
      est_time: Math.round(moment.duration(trip.geoRoute.paths[0].time).asMinutes()), //Converting milliseconds to minutes
      way_points: trip.waypoints.length || 0,
      tags : trip.tags || [],
      shared_with: trip.shared_with,
      shipment_no: trip.shipment_no,
      round_trip: utils.isRoundTrip(trip.waypoints)
    };

    if(trip.custom_fields && trip.custom_fields.length > 0){
      newTrip.custom_fields = trip.custom_fields
    }
  } catch (e) {
    console.error(e)
    return callback({code:400, msg:'Fields are missing'})
  }


  _.forEach(wayPoints, function (wayPoint) {
    let newWayPoint = {
      id : wayPoint.id,
      fk_poi_id: wayPoint.poiId,
      lname: wayPoint.lname,
      lat: wayPoint.lat,
      lon: wayPoint.lon,
      tis: wayPoint.scheduledTis,
      stop_dur: wayPoint.stopDur,
      seq_no: wayPoint.markerLabel,
      active : wayPoint.active!==false,
      loading : wayPoint.loading === true,
      unloading : wayPoint.unloading === true,
      dispatchAction: wayPoint.dispatchAction,
      fk_consignmentIds: wayPoint.fk_consignmentIds
            
    };

    newTrip.est_time += newWayPoint.stop_dur || 0;
    if(newWayPoint.fk_poi_id !== null) {
      validateWayPoint.push(newWayPoint.fk_poi_id)
    }

    if(newWayPoint.active !== false) {
      matrixWayPoints.push(newWayPoint.lat + ',' + newWayPoint.lon)
    }

    finalWayPoints.push(newWayPoint)
  });


  trip = _.omitBy(trip, _.isNil); //Remove undefined;

  let whereCondition = {
    fk_c_id : companyId
  };

  if(Number(id)){
    whereCondition.id = id
  }else{
    whereCondition.tid = id
  }

  models.trip_planning.findOne({
    attributes : ['id'],
    where : whereCondition,
    active : true,
    started : false
  }) .then(function (trip) {
    if(!trip) {
      return callback({
        code: 404, 
        msg: "Invalid trip ID"
      })
    }
        
    async.series({
      asset : function (cb) {
        checkIfAssetBelongsToCompany(newTrip, companyId, cb)
      },
      checkWayPoints : function (cb) {
        checkIfPoiBelongsToCompany(validateWayPoint, companyId, cb)
      },
      matrixResponse : function (cb) {
        cb()
      }
    }, function (err, result) {
            
      if(err) {
        return callback({code: err.code || 500, msg: err.msg || 'Internal error'})
      }
            
      newTrip.est_matrix = result.matrixResponse;
      newTrip.waypoints = _.orderBy(_.filter(finalWayPoints, ['active', true]), ['seq_no'], ['asc']);
            
      finalWayPoints = _.map(finalWayPoints, function(wayPoint) {
        return _.extend({}, wayPoint, {fk_trip_plan_id: trip.dataValues.id});
      });
    
      trip.update(newTrip)
        .then(function () {
          let addWayPoints = _.filter(finalWayPoints, function (o) { return !o.id});
          let updateWayPoints = _.reject(finalWayPoints, ['id', null]);
                    
          async.parallel({
            bulkInsertWayPoints : function (cb) {

              models.waypoint.bulkCreate(addWayPoints)
                .then(function () {
                  cb(null)
                }).catch(function (err) {
                  cb(err)
                })
            },
            updateWayPoints : function (mainCb) {
              if(trip.dataValues.new) {
                return mainCb()
              }
              async.each(updateWayPoints, function(wayPoint, cb){
                models.waypoint.update(wayPoint, {
                  where : {
                    id : wayPoint.id,
                    fk_trip_plan_id : trip.dataValues.id
                  }
                }).then(function () {
                  cb()
                }).catch(function (err) {
                  cb(err)
                })
              }, function (err) {
                mainCb(err);
              })
            },
            processTemplate: processTripPlanTemplate.bind(null, newTrip),
            tags : tagsHelper.processTags.bind(null, newTrip.tags, trip.dataValues.id, companyId, tagsHelper.tagType.trip_plannings),
            shareWithExternalUsers: function(cb){
              processTripSharingWithExternalUsers({
                fk_trip_id: id,
                tid: id,
                fk_u_id: headers.fk_u_id,
                fk_c_id: companyId,
                shared_with: newTrip.shared_with || []
              }, cb)
            },
            consignment_mapping: function (cb) {
              console.log("==================== fk_trip_id, fk_c_id, fk_consignment_ids " , id,companyId, trip.fk_consignment_ids );
              processConsignmentTripMapping({
                fk_trip_id: id,
                fk_c_id: companyId,
                fk_consignment_ids: consignment_ids || null
              }, cb)
            },
          }, function (err) {
            if(err) {
              return callback(err)
            }
            callback(null,'Updated successfully')
          })

        }).catch(function (err) {
          console.log(" *************** Trip Update Error *************",err);
          callback({
            code: 500, 
            msg: 'Internal error'
          })
        })
    });

  }).catch(function (err) {
    callback({
      code: 500, 
      msg: 'Internal error',
      err: err
    })
  })
};