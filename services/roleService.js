const async = require('async');
const models = require('../db/models');
const _ = require('lodash');

exports.addUserRoles = function (data, headers, callback) {
 
  if(_.isArray(data.role_ids) && data.role_ids.length > 0){
    async.each(data.role_ids, function (fk_user_role_id, aCb) {
      let user_role_mapping_object = {
        fk_user_id: data.fk_user_id,
        fk_user_role_id: fk_user_role_id,
        active: true
      };
      models.user_role_mapping.create(user_role_mapping_object
      ).then(function(){
        aCb()
      }).catch(function (err) {
        aCb({code: 500, msg: 'Internal server error', err: err})
      })
    }, function (err) {
      callback(err, null)
    })
  }else{
    // Create system-admin role if no roles are found.
    async.auto({
      default_role: function (cb) {
        models.sequelize.query(
          "SELECT id FROM user_roles WHERE fk_c_id IS NULL AND name = 'system-admin' ORDER BY id LIMIT 1 ;",
          {type: models.sequelize.QueryTypes.SELECT})
          .spread(function (result) {
            cb(null, result)
          }).catch(function (err) {
            cb({code: 500, msg: 'Internal server error', err: err})
          })
      },
      mapping: ['default_role', function (results, cb) {
        let user_role_mapping_object = {
          fk_user_id: data.fk_user_id,
          fk_user_role_id: results.default_role.id,
          active: true
        };
        models.user_role_mapping.create(user_role_mapping_object
        ).then(function(){
          cb()
        }).catch(function (err) {
          cb({code: 500, msg: 'Internal server error', err: err})
        })
      }]
    }, function (err) {
      callback(err, null)
    })
  }
};