const async = require('async');
const models = require('../db/models/index');
const defaultSettings = require('../helpers/defaultSettings');
const nuRedis = require('../helpers/nuRedis');
const groupLog = require('../helpers/groupLog');

function deleteUserGroupMapping(gid, callback) {
  nuRedis.deleteRedisKey("GP-"+ gid, function () {
    callback(null, null);
  })
}

/*
* add/enable user from group
*/
exports.addUserGroup = function(groupData, headers, callback){

  const fk_c_id = headers.fk_c_id;
  const fk_u_id = headers.fk_u_id;
  let postData = groupData;

  let groups = typeof postData.group_ids === "number" ? [postData.group_ids] : postData.group_ids;
  let users = typeof postData.users === "number" ? [postData.users] : postData.users;


  async.waterfall([
    function (cb) {
      // check if admin user

      let query = " SELECT g.is_admin FROM user_group_mappings ugm "+
                " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
                " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = "+ fk_c_id +" AND fk_u_id = "+ fk_u_id ;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, {})
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, mainCb) {
      //Add new entry or update existing
      let logDataUsers = [];
      let fk_group_id;
      async.eachSeries(users, function (userId, callback) {
        async.each(groups, function(group_id, eachCallback){ 
          async.waterfall([
            function (cb) {
              let query = " SELECT u.id as fk_u_id, us.group_settings, ugm.connected, g.id as fk_group_id, g.gid FROM users u" +
                                        " INNER JOIN groups g ON g.id = '" + group_id + "' AND g.fk_c_id = " + fk_c_id +
                                        " LEFT JOIN user_group_mappings ugm ON ugm.fk_u_id = u.id AND g.id = ugm.fk_group_id" +
                                        " INNER JOIN user_settings us ON us.fk_u_id = u.id" +
                                        " WHERE u.active AND u.id = " + userId;

              models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                .spread(function (result) {
                  if(result && result.connected === false) {
                    let query = "UPDATE user_group_mappings SET connected = true WHERE fk_group_id = "+ result.fk_group_id +" and fk_u_id = "+ result.fk_u_id;

                    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                      .then(function () {
                        groupLog.enableUser(result.fk_group_id, fk_u_id, result.fk_u_id, groupData, function () {
                          cb(null, result)
                        })
                      }).catch(function (err) {
                        cb(err)
                      })
                  } else if(result && !result.connected){
                    logDataUsers.push(result.fk_u_id);
                    fk_group_id = result.fk_group_id;
                    let newMapping = {
                      fk_group_id: result.fk_group_id,
                      fk_u_id: result.fk_u_id,
                      connected: true,
                      tis: parseInt(Date.now() / 1000)
                    };

                    models.user_group_mapping.create(newMapping).then(function () {
                      cb(null, result)
                    }).catch(function (err) {
                      cb({
                        code: 500,
                        msg: err
                      })
                    })
                  } else {
                    cb(null, null)
                  }
                }).catch(function (err) {
                  cb(err)
                })
            },
            function (result, cb) {
              //update User group settings
              if(!result) {
                return cb(null, null)
              }
              result.group_settings.push(defaultSettings.userGroup(result));
              let query = "update user_settings set group_settings = '" + JSON.stringify(result.group_settings) + "' where fk_u_id = " + userId;
              models.sequelize.query(query, {type: models.sequelize.QueryTypes.UPDATE})
                .then(function () {
                  deleteUserGroupMapping(result.gid, function () {
                  });
                  cb()
                }).catch(function (err) {
                  cb({
                    code:500,
                    msg: err
                  })
                })
            }
          ], function (err) {
            eachCallback(err)
          })
        }, function (err) {
          callback(err)
        })
      }, function (err) {
        if(err) {
          return mainCb(err)
        }
        if (logDataUsers.length === 0) {
          return mainCb()
        }
        groupLog.addUser(fk_group_id, fk_u_id, logDataUsers, groupData, mainCb)
      })
    }
  ],function (err) {
    if(err){
      if(err.code){
        callback({code: err.code, message: err.msg})
      } else {
        callback({code: 500, message: 'Internal server error = '+err})
      }
    }else{
      callback(null, "User added successfully")
    }
  })
};