const models = require('../db/models');
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config')[env];

const request = require("request");
const async = require('async');
const _ = require('lodash');

exports.geocoding = function (companyId, data, callback) {
  let q = data.q || '';
  let keyword = '%' + q + '%';

  async.parallel({
    locations : function (cb) {
      let location = {};
      let options = {
        method: 'GET',
        url: config.googleMap.url + "address=" + q + "&key=" + config.googleMap.key + "&components=country:IN"
      };

      request(options, function (err,response,body) {
        body = JSON.parse(body);
        if (err || body.status !== "OK") {
          return cb(null , [])
        }

                
        try {

          location = {
            poiId : null,
            poiType : null,
            lname : body.results[0]['formatted_address'],
            lat : parseFloat(body.results[0]['geometry']['location']['lat']),
            lon : parseFloat(body.results[0]['geometry']['location']['lng'])
          }
        } catch (e) {
          console.log(e)
        }

        cb(null, location)
      });
    },
    pois : function(cb) {
      let query = 'SELECT p.id as "poiId", p.name as "lname", st_centroid(p.geo_loc::geometry) as geo_loc, p.geo_loc as bounds,'+
                '   pt.type as "poiType"' +
                ' FROM pois p' +
                ' INNER JOIN poi_types pt ON p.typ=pt.id' +
                ' LEFT JOIN json_each_text(p.company_fields) x ON TRUE' +
                ' WHERE p.active AND p.fk_c_id = $fk_c_id AND ( p.name ilike $keyword or x.value ILIKE $keyword) group by p.id, pt.id LIMIT 5';

      models.sequelize.query(query, {bind: {fk_c_id: companyId, keyword: keyword}, type: models.sequelize.QueryTypes.SELECT})
        .then(function (pois) {
          async.eachSeries(pois,function (poi, callback) {
            let geobounds = [];
            poi.lat = poi.geo_loc.coordinates[1];
            poi.lon = poi.geo_loc.coordinates[0];
            let coordinates = poi.bounds.coordinates[0];
            async.each(coordinates,function (i, eachCb) {
              geobounds.push({
                lon: i[0],
                lat: i[1]
              });
              eachCb()
            }, function(){          
              poi.geoBounds = geobounds;
              delete poi.geo_loc;
              delete poi.bounds;
              callback()
            })
          }, function(){                 
            cb(null, pois)
          });
                    
        }).catch(function (err) {
          cb(err)
        });
    }
  }, function (err, result) {
    if(err) {
      return callback({code:500, msg:'Internal error'});
    }
    let finalResult = _.concat(result.pois, result.locations);
    callback(null, finalResult)
  })
};

exports.reverseGeocoding = function(companyId, data, callback){
  let lat = Number(data.lat);
  let lon = Number(data.lon);

  if(!lat || !lon) {
    return callback({code: 400, msg: 'Invalid lat/lon'})
  }

  async.parallel({
    location : function (cb) {
      let location = {};
      let options = {
        method: 'GET',
        url: config.googleMap.url + "latlng=" + lat + ',' + lon + "&key=" + config.googleMap.key
      };

      request(options, function (err,response,body) {
        body = JSON.parse(body);
        if (err || body.status !== "OK") {
          return cb(null , [])
        }

                
        try {

          location = {
            poiId : null,
            poiType : null,
            lname : body.results[0]['formatted_address'],
            lat : parseFloat(lat),
            lon : parseFloat(lon)
          } 
        } catch (e) {
          console.log(e)
        }

        cb(null, location)
      });
    },
    poi : function(cb) {
      let query = 'SELECT p.id as "poiId", p.name as lname, pt.type as "poiType", p.geo_loc as bounds' +
                ' FROM pois p' +
                ' INNER JOIN poi_types pt ON p.typ=pt.id' +
                ' WHERE p.active AND p.fk_c_id = ' + companyId + ' AND st_intersects(ST_GeographyFromText(\'POINT('+ lon + ' ' + lat + ')\'),p. geo_loc) LIMIT 1';

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (poi) {
          if(!poi) {
            return cb(null)
          }
          poi.lat = parseFloat(lat);
          poi.lon = parseFloat(lon);
          let geobounds = [];
          let coordinates = poi.bounds.coordinates[0];
          async.each(coordinates,function (i, eachCb) {
            geobounds.push({
              lon: i[0],
              lat: i[1]
            });
            eachCb()
          }, function(){          
            poi.geoBounds = geobounds;
            delete poi.bounds;
            cb(null, poi)
          })
                    
        }).catch(function (err) {
          cb(err)
        });
    }
  }, function (err, result) {
    if(err) {
      return callback({code:500, msg:'Internal error'});
    }
    let response = result.poi ? [result.poi] : (result.location && result.location.lat) ? [result.location] : [];
    callback(null, response)
  })
};