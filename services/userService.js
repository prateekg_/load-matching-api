
const async = require('async');
const generator = require('generate-password');
const models = require('../db/models');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const defaultSettings = require('../helpers/defaultSettings');
const rabbitMq = require('../helpers/rabbitMQ');
const env       = process.env.NODE_ENV || 'development';
const config    = require('../config/index')[env];
const mailerConfig = config.mailer;
const eol_company_id  = (env === "production") ? 19 : "";
const eol_internal_sales_user = "shelton@numadic.com";
const emails = require('../helpers/emails');

exports.addUser = function(userData, headers, callback){
  const fk_c_id = headers.fk_c_id;
  const fk_u_id = headers.fk_u_id;
  userData.email = userData.email.toLowerCase();

  const uniquePassword = generator.generate({
    length: 10,
    numbers: true
  });

  const rmqPassword = generator.generate({
    length: 16,
    numbers: true
  });

  const hashPassword = bcrypt.hashSync(uniquePassword, 10);

  let query = " SELECT g.is_admin FROM user_group_mappings ugm "+
            " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
            " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = "+ fk_c_id +" AND fk_u_id = "+ fk_u_id ;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (result) {
      if(result && result.is_admin){
        models.user.findOrCreate({
          where:{
            email : userData.email
          },
          defaults:{
            name : userData.name,
            fk_c_id : fk_c_id,
            p_word : hashPassword,
            cont_no : userData.cont_no,
            rmq_p_word: rmqPassword
          }
        }).spread(function(user, created){

          if(created){
            user.password = uniquePassword;
            async.parallel([
              function (cb) {
                models.user_setting.create({
                  fk_u_id: user.get({plain: true}).id,
                  settings: defaultSettings.user,
                  group_settings: []
                }).then(function () {
                  cb()
                }).catch(function (err) {
                  cb(err)
                })
              },
              function (cb) {
                rabbitMq.generateRabbitMqAccount(user.email, rmqPassword, '', cb)
              },
              function (cb) {
                //Email template for EOL User
                if(fk_c_id === eol_company_id){
                  let email_data = {
                    to_email : user.email,
                    name: user.name,
                    password: user.password,
                    cc_emails: (fk_c_id === eol_company_id) ? eol_internal_sales_user : ""
                  };
                  emails.eolNewUserCreated(email_data, function(err){
                    if(err) cb({code:500, msg: "Error sending email"});
                    else cb(null)
                  })
                }
                else{
                  let transporter = nodemailer.createTransport(mailerConfig);
                  let mailOptions = {
                    from: 'noreply@numadic.com',
                    to: user.email,
                    bcc: 'noreply@numadic.com',
                    subject: 'Welcome to the Numadic Platform',

                    html: '<body style="color:#444;font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;padding:0 100px;width:800px">' +
                                            '<img src="cid:nu-email-header-logo"/>' +
                                            '<h1><b>Welcome to the Numadic Platform</b></h1>' +
                                            'Hello ' + user.name + ',' + '<br>' + '<br>' +
                                            'Thank you for trying our product! Numadic is a software platform that improves profitability with affordable, secure and easy to use Fleet Management.' +
                                            '<br>' +
                                            '<p>Your nubot is ready for action and your account is set up.</p>' + '<br>' +
                                            '<b>Your account details are:</b><br>' +
                                            'Email: ' + user.email + '<br>' +
                                            'Password: ' + user.password + '<br>' + '<br>' +
                                            '<b>Access the Numadic Platform here, if you haven\'t already:</b>' +
                                            '<p>https://app.numadic.com/</p>' +
                                            '<br>' +
                                            '<p>Please click this link to download the Numadic Fleet Tracking Application directly from the Playstore.</p>' +
                                            '<p><a href="http://nuws.co/av1">nuws.co/av1</a></p>' +
                                            '<br>' +
                                            'This is the first step towards safer, more efficient driving.' +
                                            '<br>' +
                                            '<p>Have a safe trip!</p>' +
                                            '</body>',
                    attachments: [
                      {
                        filename: "nu-logo",
                        path: './public/images/nu-email-header.png',
                        cid: 'nu-email-header-logo'
                      }]
                  };
                  transporter.sendMail(mailOptions, function (error, info) {
                    cb(error, info)
                  });
                }
              }
            ], function (err) {
              if (err) {
                callback({code: 500, message: err})
              } else {
                callback(null, {id: user.get({plain: true}).id})
              }
            })
          } else {
            callback({code: 400, message: 'User already exists'})
          }
        }).catch(function(err){
          callback({code: 500, message: 'Internal server error = '+err})
        });
      }else{
        callback({code: 403, message: 'User does not have permissions'})
      }
    }).catch(function (err) {
      callback({code: 500, message: 'Internal server error = '+err})
    })
};

exports.uniqueEmail = function(email, callback){
  let query = "select * from users where email = '"+ email +"'";
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (result) {
      if(result && result.email){
        callback({code:400, message:"Email already exists"})
      }
      else{
        callback(null)
      }
    });
};