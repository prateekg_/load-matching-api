/*
This service specially created to have common code used for CMA app i.e. consignment mapping app
Keep code which is different from normal consignment and used in CMA app here
*/

const async = require('async');
const models = require('../db/models');
const moment = require('moment');
const RabbitMQ = require('../helpers/rabbitMQ');

/**
 * This function is created when device issues during trip and trip is not completing we force complete the trip with a reason
 * @param {*} fk_c_id - Consignment company
 * @param {*} fk_u_id - User which force completes the trip
 * @param e_id
 * @param {*} consignment_id - consignment id to identify the trip
 * @param {*} reason - REason for marking trip complete
 * @param callback
 */
exports.overrideConsignmentTripStatus = function (fk_c_id, fk_u_id, e_id, consignment_id, reason, callback) {
  async.auto({
    trip: function (cb) {
      let query = "SELECT tp.id, tp.status FROM trip_plannings tp "+
                " INNER JOIN consignments con on con.id = tp.fk_asset_id " +
                " INNER JOIN asset_types ast on ast.id = tp.fk_asset_type_id and ast.type = 'Consignment'" +
                " INNER JOIN companies c on c.id = tp.fk_c_id" +
                " INNER JOIN company_device_inventory_vw cmvw on con.id = cmvw.fk_consignment_id and connected "+
                " INNER JOIN entities e on cmvw.fk_entity_id = e.id and e.e_id = '"+e_id + "'" +
                " WHERE tp.fk_c_id = "+ fk_c_id +" AND con.id = "+ consignment_id + " LIMIT 1";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (trip) {
          if (!trip) {
            cb({
              code: 404,
              msg: 'Not found'
            })
          }else{
            if(trip.status === "completed"){
              cb({
                code: 409,
                msg: 'old and new status is same'
              })
            }else{
              cb(null, trip)
            }
          }
        }).catch(function (err) {
          cb({
            code: 500,
            msg: 'Internal error',
            err: err
          })
        })
    },
    log_update_status: ['trip', function (results, cb) {
      let log = {
        fk_trip_id: results.trip.id,
        fk_u_id: fk_u_id,
        new_status: "completed",
        old_status: results.trip.status,
        note: reason
      };

      models.trip_user_log.create(log).then(function () {
        cb(null,null)
      }).catch(function (err) {
        console.log(err);
        cb({
          code: 500,
          msg: 'Internal error'
        })
      })
    }],
    update_trip_status: ['trip','log_update_status', function (results, cb) {
      models.trip_planning.update({
        status: "completed"
      },{
        where:{
          id: results.trip.id
        }
      }).spread(function () {
        cb(null)
      }).catch(function (err) {
        console.log(err);
        cb(null)
      })
    }],
    //Sends auto-trip engine status
    publish_to_rabbit_mq: ['trip','log_update_status', function (results, cb) {
      let msg = {id: results.trip.id, status: 'completed'};
      RabbitMQ.sendToExchange("Trip-Edit", msg);
      cb(null)
    }]
  }, function (err) {
    if(err){
      callback({
        code: err.code,
        msg: err.msg
      })
    }else{
      callback(null, "Updated trip status")
    }
  })
}; 


/**
 * This function is used to override consignment status when CMA app force completes a trip
 * 
 * @param fk_c_id
 * @param {*} fk_consignment_id - Consignment primary id
 * @param {*} fk_u_id - Company user id
 * @param {*} reason - Reason for making incomplete
 * @param {*} callback 
 * @description {STRING} - Updated via api ( This status will be overwritten by db trigger)
 */
exports.overrideConsignmentStatus = function(fk_c_id, fk_consignment_id, fk_u_id, reason, callback){
  models.consignment.update({
    status: "incomplete"
  }, {
    where: {
      id : fk_consignment_id,
      fk_c_id: fk_c_id
    }
  }).then(function (updated) {
    if(updated){
      models.consignment_status.create({
        fk_consignment_id: fk_consignment_id,
        fk_by_u_id: fk_u_id,
        tis: moment().unix(),
        status: "incomplete",
        reason: reason,
        is_current: true
      }).then(function () {
        callback(null, "updated")
      }).catch(function(err){
        console.log(err);
        callback({
          code: 500,
          msg: "Internal server error"
        })
      })
    }
    else{
      callback({
        code: 304,
        msg: "Error updating consignment"
      })
    }
  }).catch(function(err){
    console.log(err);
    callback({
      code: 500,
      msg: "Internal server error"
    })
  })
};