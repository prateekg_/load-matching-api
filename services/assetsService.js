/**
 * This service should be used to find stats of realtime or past data
 * Data specific to a asset type shouldn't be done here. Create a separate service for a asset type logic and include it in respective file
 * 
 */

const async = require('async');
const models = require('../db/models/index');
const nuRedis = require('../helpers/nuRedis');
const _ = require('lodash');

/**
 * This service gets realtime stat of all assets belonging to a group
 * @param {Integer} company_id - Client company id
 * @param {Array} group_ids - Array of group ids user belong to
 * @param {String} email - Users email id
 * @param callback
 */
exports.getAllCurrentAssetStats = function(company_id, group_ids, email, callback){
  let stat = [];
  let query = `SELECT at.type
               from groups g  
               INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
               INNER JOIN assets a on a.id = agm.fk_asset_id and a.active 
               INNER JOIN asset_types at on a.fk_ast_type_id = at.id
               inner join companies c on a.fk_c_id = c.id
               WHERE a.active and g.fk_c_id = ` + company_id+ " and  g.id in " + group_ids +
               `group by at.type`;
               
  models.sequelize.query(query, {type:models.sequelize.QueryTypes.SELECT}).then(function(data){

    async.each(data, function(asset_types, callback){
      nuRedis.getAssetsStats(email, asset_types.type, function(err, result){
                   
        stat.push(result);
        callback(err)
      })
               
    }, function(err){
      if(err){
        callback({code: 422, message: err})
      }
      else {
        let result =  _.reduce(stat, function (obj, data) {
          _.each(data, function (value, key) {
            obj[key] = obj[key] || 0; // if it undefined yet put number 0 
            obj[key] += value; // sum with the previous value
          });
          return obj; // the populated data 
        },  {});
        callback(null, result)
      }
    })
  }).catch(function(err){
    callback({code: 500, message: err})
  })
};