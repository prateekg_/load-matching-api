/**
 * This service should be used to write common logic of pois
 */

exports.allPoiEntities = function(groupIds){
  return `WITH poi_entities as (
        SELECT aps1.fk_asset_id as id, c.company_fields->>'lic_plate_no' as lic_plate_no, 
            aps1.entry_tis, aps1.exit_tis, aps1.poi_stat,
            tp.id as trip_id, --tp.status, 
            p.id as poi_id, p.typ as poi_type, p.fk_c_id, p.priv_typ, 
            at.type, null as group_id, aps1.latest_tis, 
            c.ext_consignment_no,
            tp.tid, tp.end_tis as trip_end_tis, tp.trip_log, l.lat, l.lon, l.lname, l.status
        FROM assets_pois_stats aps1 
        INNER JOIN consignments c on aps1.fk_asset_id = c.id and aps1.enum_type = 'Consignment'
        LEFT JOIN current_locations l on l.fk_asset_id = c.id
        INNER JOIN trip_plannings tp on tp.id = c.fk_trip_id
        INNER JOIN asset_types at on tp.fk_asset_type_id = at.id
        INNER JOIN pois p on p.id = aps1.fk_poi_id and p.active

        UNION ALL

        SELECT a.id, a.lic_plate_no, 
            aps.entry_tis, aps.exit_tis, aps.poi_stat,
            tp.id as trip_id, --tp.status, 
            p.id as poi_id, p.typ as poi_type, p.fk_c_id, p.priv_typ, 
            atp.type, g.id as group_id, aps.latest_tis,
            null as ext_consignment_no,
            tp.tid, tp.end_tis as trip_end_tis, tp.trip_log, l.lat, l.lon, l.lname, l.status
        FROM assets_pois_stats aps
        INNER JOIN asset_group_mappings agm on agm.fk_asset_id = aps.fk_asset_id
        INNER JOIN groups g on g.id = agm.fk_group_id and g.active
        INNER JOIN pois p on p.id = aps.fk_poi_id and p.active
        INNER JOIN assets a on a.id = aps.fk_asset_id and a.active
        LEFT JOIN current_locations l on l.fk_asset_id = a.id
        INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id
        LEFT JOIN asset_trip_maps atm on a.id = atm.fk_asset_id
        LEFT JOIN trip_plannings tp on tp.id = (
            SELECT id
            FROM trip_plannings
            where fk_asset_id =a.id
            order by "createdAt" desc limit 1
        )
        WHERE g.id in ${groupIds}
    )`;
};