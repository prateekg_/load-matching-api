const models = require('../db/models');
const async = require('async');
const _ = require('lodash');

exports.getUserTableConfig = function (data, callback) {
  let query = "SELECT uc.config, tt.default_config FROM wv_table_types tt" +
        " LEFT JOIN wv_user_configs uc on tt.id = uc.fk_wv_table_type_id AND uc.fk_user_id = :fk_u_id" +
        " WHERE tt.table_name = :table_name";

  models.sequelize.query(query, {replacements:{fk_u_id: data.fk_u_id, table_name: data.table_name}, type: models.sequelize.QueryTypes.SELECT}).spread(function (user_config) {
    let user_config_final = {};
    user_config_final.config = user_config && user_config.config && user_config.config.tabs ? user_config.config : user_config.default_config;
    callback(null, user_config_final)
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal error', err: err})
  })
};

exports.updateUserTableConfig = function (params, callback) {
  async.auto({
    table: function (cb) {
      let query = "SELECT id from wv_table_types WHERE table_name = :table_name";
      models.sequelize.query(query, {replacements: {table_name: params.table_name}, type: models.sequelize.QueryTypes.SELECT}).spread(function (data) {
        if(data){
          cb(null, data)
        }else{
          cb({code: 500, msg: 'Table configurations not found'})
        }
      }).catch(function (err) {
        cb({code: 500, msg: 'Internal server error', err: err})
      })
    },
    config: ['table', function (results, cb) {
      let table_type_id = results.table.id;

      let user_config = {
        fk_user_id: params.fk_u_id,
        fk_wv_table_type_id: table_type_id,
        config: params.data.config || {}
      };

      models.wv_user_config.findOrCreate({
        where: _.pick(user_config,['fk_user_id','fk_wv_table_type_id']),
        defaults: user_config
      }).spread(function (user_config, created) {
        if (!created) {
          // not created
          if(params.data && params.data.config){
            user_config.config = params.data.config
          }
          user_config.save().then(function () {
            cb(null, 'sucessfully updated')
          }).catch(function (err) {
            cb({code: 500, msg: 'Internal server error', err: err})
          })
        } else {
          cb(null, 'Successfully created')
        }
      }).catch(function (err) {
        cb({code: 500, msg: 'Internal server error', err: err})
      })
    }]
  }, function (err) {
    callback(err, { status: 'Configurations updated'})
  })
};