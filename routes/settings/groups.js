const express = require('express');
const router = express.Router();
const groups = require('../../controllers/settings/groups');

router.get('/', function (req, res) {
  let gid = req.query.gid;
  if (gid) {
    groups.groupDetail(req, res)
  } else {
    groups.listAll(req, res)
  }
});

router.get('/mygroups', groups.listMyGroups);

router.post('/', groups.createGroup);

router.put('/', groups.updateGroup);

router.delete('/', groups.deleteGroup);

router.get('/assets', groups.assetList);

router.post('/assets', groups.addAsset);

router.delete('/assets', groups.disableAsset);

router.delete('/assets/remove', groups.removeAsset);

router.get('/pois', groups.poiList);

router.post('/pois', groups.addPoi);

router.delete('/pois', groups.disablePoi);

router.delete('/pois/remove', groups.removePoi);

router.get('/users', groups.userList);

router.post('/users', groups.addUser);

router.delete('/users', groups.disableUser);

router.delete('/users/remove', groups.deleteGroupUser);

router.put('/access', groups.updateAccess);

router.get('/access', groups.getAccess);

router.get('/notifications', groups.getNotificationSettings);
router.put('/notifications', groups.updateNotificationSettings);

module.exports = router;
