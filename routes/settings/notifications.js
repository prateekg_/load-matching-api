const express = require('express');
const router = express.Router();

const notificationController = require('../../controllers/settings/notifications');

router.get('/', notificationController.get);
router.put('/', notificationController.update);

module.exports = router;