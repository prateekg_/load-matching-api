const express = require('express');
const router = express.Router();
const poiController = require('../../controllers/settings/poi');


// router.get('/pois', poiController.getAllList);
router.get('/pois', function (req, res) {
  if (req.query.id) {
    poiController.details(req, res)
  } else {
    poiController.getAllList(req, res)
  }
});

router.put('/pois', poiController.update);

router.post('/pois', poiController.addNewPoi);

router.post('/pois/detention', poiController.addDetentionPoi);

router.put('/pois/detention', poiController.updateDetentionSettings);

router.get('/pois/types', function (req, res) {
  if (req.query.id) {
    poiController.getPoiType(req, res)
  } else {
    poiController.getPoiTypes(req, res)
  }
});

router.post('/pois/types', poiController.createPoiType);

router.put('/pois/types', poiController.updatePoiType);

router.delete('/pois', poiController.disablePoi);

module.exports = router;
