const express = require('express');
const router = express.Router();

const tableConfigController = require('../../controllers/settings/table_config');

router.get('/config', tableConfigController.getTableConfiguration);

router.put('/config', tableConfigController.updateTableConfiguration);

module.exports = router;