const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');
const multer  = require('multer');
const multerS3 = require('multer-s3');
const path = require('path');

const assetDocumentsController = require('../../controllers/settings/assetDocuments');
const assetSensorsController = require('../../controllers/settings/assetSensors');
const assets = require('../../controllers/settings/asset');

const ASSET_DOC_CODES = ['VR','PC','VI','FC','NP','PL','R18','R19','OT'];

// File upload config START

let s3_config = require('../../config/aws/s3.json');
const S3_ASSET_DOCS_BUCKET = process.env.NODE_ENV === 'production' ? 'nu-platform-docs-prod' : 'nu-platform-docs-dev';


AWS.config.update(s3_config);
let s3 = new AWS.S3();

// File upload config END

// File upload function
let upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: S3_ASSET_DOCS_BUCKET,
    key: function (req, file, cb) {

      let record = req.body.record ? JSON.parse(req.body.record) : {};
      let asset = record.asset;

      if(!record){
        cb({status: 400, message: 'No data found'})
      }else if(!record.type_code){
        cb({status: 400, message: 'No document type code'})
      }else if(ASSET_DOC_CODES.indexOf(record.type_code) < 0){
        cb({status: 400, message: 'Invalid document type code'})
      }else if(!asset){
        cb({status: 400, message: 'No asset data'})
      }else if(!asset.id){
        cb({status: 400, message: 'No asset id'})
      }else if(!asset.licNo){
        cb({status: 400, message: 'No asset licNo'})
      }else{
        let s3_filename_prefix = "assets/"+ asset.id +"-"+ asset.licNo+"/"+record.type_code+"/";
        cb(null, s3_filename_prefix + new Date().getTime() + path.extname(file.originalname))
      }
    }
  })
});

// trucks documents

router.post('/trucks/documents',
  upload.array('files','record',20),
  assetDocumentsController.createAssetDocumentRecord);

router.put('/trucks/documents',
  upload.array('files','record',20),
  assetDocumentsController.updateAssetDocumentRecord);

router.delete('/trucks/documents', assetDocumentsController.deleteAssetDocumentRecord);

router.put('/trucks', assetSensorsController.updateAssetSensorSettings);

router.get('/trucks/sensor', assetSensorsController.getTemperatureLogs);

// new logic
router.get('/assets', function (req, res) {
  if (req.query.id) {
    assets.details(req, res)
  } else {
    assets.getAllList(req, res)
  }
});

module.exports = router;