const express = require('express');
const router = express.Router();

const drivers = require('./drivers');
const tyres = require('./tyres');
const assets = require('./assets');
const users = require('./users');
const notifications = require('./notifications');
const groups = require('./groups');
const poi = require('./poi');
const reports = require('./reports');
const scheduledReports = require('./scheduled_reports');
const tableConfigs = require('./table_config');

router.use('/settings/drivers', drivers);
router.use('/settings/tyres', tyres);
router.use('/settings', assets);
router.use('/settings', poi);
router.use('/settings', users);
router.use('/settings/notifications', notifications);
router.use('/settings/groups', groups);
router.use('/settings/table', tableConfigs);
router.use('/settings/reports', reports);
router.use('/settings/scheduledreports', scheduledReports);

module.exports = router;