const express = require('express');
const router = express.Router();
const scheduledReport = require('../../controllers/settings/scheduledReport');


//get scheduled report types
router.get('/types', scheduledReport.getReportTypes);

router.get('/', function (req, res) {
  const id = req.query.id;
  const reportTypeId = req.query.reportTypeId;
  if (id || reportTypeId) {
    // get single scheduled reports details
    scheduledReport.getScheduledReport(req, res)
  } else {
    // list all scheduled reports
    scheduledReport.listAllScheduledReports(req, res)
  }
});

// create scheduled report
router.post('/', scheduledReport.createScheduledReport);

// update scheduled report
router.put('/', scheduledReport.updateScheduledReport);

// disable scheduled report
router.delete('/', scheduledReport.disableScheduledReport);

module.exports = router;
