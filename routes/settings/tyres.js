const express = require('express');
const router = express.Router();

const tyreController = require('../../controllers/settings/tyres');

router.get('/', tyreController.getSingleTyre);
router.post('/', tyreController.createTyre);
router.put('/', tyreController.updateTyre);

router.get('/services', tyreController.getTyreService);
router.post('/services', tyreController.createTyreService);
router.put('/services', tyreController.updateTyreService);
router.delete('/services', tyreController.deleteTyreService);

module.exports = router;