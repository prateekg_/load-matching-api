const express = require('express');
const router = express.Router();

const reportsController = require('../../controllers/settings/reports');


router.get('/subscribe/global', reportsController.getUserGlobalSubscriptionStatus);

router.put('/subscribe/global', reportsController.updateUserGlobalSubscriptionStatus);

router.put('/subscribe', reportsController.updateReportSubscription);

router.get('/', reportsController.getAllUserReports);

module.exports = router;