const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');
const multer  = require('multer');

const driverController = require('../../controllers/settings/drivers');
const s3_config = require('../../config/aws/s3.json');

AWS.config.update(s3_config);

// File upload function
let upload = multer({
  storage: multer.diskStorage({
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now())
    }
  })
});

router.get('/', driverController.getDriver);
router.post('/', upload.any(), driverController.createDriver);
router.put('/', upload.any(), driverController.updateDriver);

module.exports = router;