const express = require('express');
const router = express.Router();

const userController = require('../../controllers/settings/users');

router.get('/user/preferences', userController.getUserPreferences);
router.put('/user/preferences', userController.updateUserPreferences);

router.put('/user/profile', userController.adminUpdateUserProfile);
router.put('/user/password', userController.adminUpdateUserPassword);

router.get('/', userController.getUserV1);
router.put('/', userController.updateUserV1);
router.post('/users', userController.createUserAndGroupsV1);
router.put('/company', userController.updateCompany);
router.put('/password', userController.updatePassword);
router.post('/email', userController.updateEmail);
router.get('/users', userController.getAllCompanyUsers);
router.delete('/users', userController.disableUser);
router.put('/users', userController.enableUser);
router.get('/users/email', userController.checkUniqueEmail);
router.get('/users/access', userController.getUsersFeaturesAndGroups);

module.exports = router;