const express = require('express');
const router = express.Router();
const drivers_v2 = require('../controllers/v3/drivers');

router.get('/', drivers_v2.getAll);

router.get('/info', drivers_v2.getSummary);

router.post('/mapping', drivers_v2.updateDriverAssetMapping);

router.get('/activity', drivers_v2.getDriverAssetActivity);

router.get('/stats', drivers_v2.getDriverStats);

router.get('/trip_stats', drivers_v2.getDriverCompletedTrips);

router.get('/notifications', drivers_v2.getNotifications);

module.exports = router;