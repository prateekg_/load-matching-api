const express = require('express');
const router = express.Router();
const users = require('../controllers/v3/users');
const user_hierarchy = require('./user_hierarchy');
const user_roles = require('./user_roles');

router.get('/', users.getAllUser);
router.use('/roles', user_roles);
router.use('/hierarchy', user_hierarchy);
module.exports = router;