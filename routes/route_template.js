const express = require('express');
const router = express.Router();
const routesTemplate = require('../controllers/v3/route_template');

router.get('/validatename', routesTemplate.checkRouteNameExists);

router.get('/', function (req, res) {
  if(req.query.id) {
    routesTemplate.get(req, res)
  } else {
    routesTemplate.getAll(req, res)
  }
});

router.post('/', routesTemplate.add);

router.put('/', routesTemplate.update);

router.delete('/', routesTemplate.delete);


module.exports = router;
