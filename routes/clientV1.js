const express = require('express');
const router = express.Router();
const v1_asset_controller = require('../controllers/client/v1/assets');
const v1_trip_controller = require('../controllers/client/v1/trips');
const v1_tollbooth_controller = require('../controllers/client/v1/tollbooths');
const load_matching = require('../controllers/client/v1/load_matching');
const fastag_visibilility_controller = require('../controllers/client/v1/fastag_visibility');
const icici_fastag = require('../controllers/client/v1/icici_fastag');
const worker = require('../workers/toll-booth-info.js');
const API_LIMITER = require('../helpers/api_limiter');
const env = process.env.NODE_ENV || 'development';
const config = require('../config/index')[env];
const request = require('request')

router.get('/trip/map', v1_trip_controller.getTripDetails);

router.get('/loads', load_matching.getLoads);

router.post('/trip/estimate', v1_trip_controller.getTripEstimate);

router.get('/assets', v1_asset_controller.getAssetList);

router.get('/vehicle/class-types', v1_asset_controller.getVehicleClassTypes);

router.get('/tollbooth', v1_tollbooth_controller.getTollBoothList);

router.get('/nhtis/tollbooth', worker.scrapeData);

router.post('/fastag/locations',
    // API_LIMITER.VALIDATE,
    fastag_visibilility_controller.getLocations);

router.use('/lanes',
    // API_LIMITER.VALIDATE,
    function (req, res) {
        const fk_c_id = req.headers['fk-c-id'] || req.headers.fk_c_id;
        const request_options = {
            uri: config.server_address.lanes_server() + req.originalUrl,
            method: req.method,
            headers: req.headers,
            qs: req.query,
            body: req.body,
            json: true
        };
        request(request_options, function (error, response, body) {
            if (error) {
                return res.statusCode(response.statusCode).send(body)
            } else {
                // API_LIMITER.UPDATE(fk_c_id)
                return res.json(body)
            }
        });
    })

router.post('/icici/user/kyc', icici_fastag.updateUserKYC);
router.get('/icici/user/kyc', icici_fastag.getUserKYC);

module.exports = router;