const express = require('express');
const router = express.Router();

const pois_v3 = require('../controllers/v3/pois');
// const notifications_v3 = require('../controllers/v3/notification');
const dashboard_place_v3 = require('../controllers/v3/dashboard/place');

router.get('/', function (req, res) {
  if (req.query.id) {
    pois_v3.getPoiDetail(req, res)
  } else {
    pois_v3.getAllPois(req, res)
  }
});

router.get('/activity', pois_v3.getAssetsInPoi);

router.get('/types/count', pois_v3.poiTypeCount);

router.get('/stats', function (req, res) {
  pois_v3.getPoisStats(req, res)
});

// router.get('/notifications', function (req, res) {
//   notifications_v3.getNotifications(req, res, 'Poi')
// });

router.get('/assets', pois_v3.getAllAssetsRelatedToPoi);

router.get('/detention', pois_v3.getPoiDetentionRegionsV2);

router.get('/detention/log', pois_v3.getDetentionSettingLog);
// // Dashboard API's

router.get('/dashboard/summary', dashboard_place_v3.getAPlaceRealtime);
router.get('/dashboard/map', dashboard_place_v3.getAPlaceMap);
router.get('/dashboard/activity', dashboard_place_v3.getAPlaceList);

module.exports = router;
