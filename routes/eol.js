
const express = require('express');
const router = express.Router();

const eol_assets_controller = require('../controllers/eol/assets');
const eol_poi_controller = require('../controllers/eol/poi');
const eol_tracker_diagnostics_controller = require('../controllers/eol/tracker_diagnostics');
const eol_ro_trips_controller = require('../controllers/eol/eol_ro_trip_planning');
const eol_trips_controller = require('../controllers/eol/eol_trip_planning');

router.get('/trucks', function (req, res) {
  if (req.query.id) {
    eol_assets_controller.getAssetDetail(req, res ,'Truck')
  } else {
    eol_assets_controller.getAllAssetsV2(req, res, 'Truck', 'list')
  }
});

router.get('/trucks/maps', function (req, res) {
  eol_assets_controller.getAllAssetsV2(req, res, 'Truck','map')
});

router.get('/pois/summary', eol_poi_controller.getAllPoiRealtimeStats);

router.get('/pois', function(req, res){
  if (req.query.id) {
    eol_poi_controller.getPoiDetail(req, res)
  } else {
    eol_poi_controller.getAllPois(req, res, 'list')
  }
});

router.get('/pois/maps', function(req, res){
  eol_poi_controller.getAllPois(req, res, 'map')
});

router.get('/pois/assets-inside', eol_poi_controller.getAssetsInsidePoi);

router.get('/pois/invoices', eol_poi_controller.getAllTripsOfSupplyPoint);

router.get('/trackers/summary', eol_tracker_diagnostics_controller.getTrackersSummary);

router.get('/vehicle_trackers/summary', eol_tracker_diagnostics_controller.getVehicleTrackerSummary);

router.get('/vehicle_trackers', function(req, res){
  if (req.query.id) {
    eol_tracker_diagnostics_controller.getAssetTrackerDetail(req, res)
  } else {
    eol_tracker_diagnostics_controller.getAllAssetTrackers(req, res, 'list')
  }
});

router.get('/portable_trackers', function(req, res){
  let status = req.query.trackerStatus;
  if (status === 'in_use') {
    eol_tracker_diagnostics_controller.getMappedConsignments(req, res)
  } else if (status === 'available') {
    eol_tracker_diagnostics_controller.getAvailbleConsignmentEntities(req, res)
  } else {
    res.status(400).send("Invalid trackerStatus")
  }
});

router.get('/depots', eol_poi_controller.getListOfDepots);

router.get('/depots/notifications', eol_poi_controller.getDepotNotifications);

router.get('/depots/trucks/table/config', eol_poi_controller.getDepotTruckTableConfig);

router.put('/depots/trucks/table/config', eol_poi_controller.updateDepotTruckTableConfig);

router.get('/routes', eol_trips_controller.getRoutes);

router.get('/tripplans', eol_trips_controller.getEolWvTrips);

router.get('/tripplans/realtime', eol_trips_controller.getEolRealtimeStats);

router.get('/tripplans/table/config', eol_trips_controller.getTableConfig);

router.put('/tripplans/table/config', eol_trips_controller.updateTableConfig);


// ============== Start of RO API's ===================

router.get('/mobile/tripplans', function (req, res) {
  if(req.query.id){
    eol_ro_trips_controller.getSingleTrip(req, res)
  }else{
    eol_ro_trips_controller.getAllTrips(req, res)
  }
});

router.get('/mobile/tripplans/map', eol_ro_trips_controller.getMap);

router.get('/mobile/tripplans/notifications', eol_ro_trips_controller.getNotifications);

// ============== End of RO API's ====================

module.exports = router;