const express = require('express');
const router = express.Router();
const reports_v2 = require('../controllers/v3/reports');

router.get('/', reports_v2.getAll);

router.get('/download', reports_v2.download);

router.get('/types', reports_v2.getTypes);

router.get('/asset_activity', reports_v2.getAssetActivity);

module.exports = router;