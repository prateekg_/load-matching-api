const express = require('express');
const router = express.Router();
const tyres_v2 = require('../controllers/v3/tyres');

router.get('/', tyres_v2.getAllTyres);

router.get('/info', tyres_v2.getAllTyresSummary);

router.get('/services', tyres_v2.getAllTyreServicing);

router.post('/mount', tyres_v2.mountTyre);

router.delete('/mount', tyres_v2.unmountTyre);

module.exports = router;