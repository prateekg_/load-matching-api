const express = require('express');
const router = express.Router();
const chat_controller = require('../controllers/settings/chats');

router.get('/', chat_controller.getUserChat);

module.exports = router;
