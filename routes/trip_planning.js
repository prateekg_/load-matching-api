/**
 * Created by numadic on 02/05/17.
 */
const express = require('express');
const router = express.Router();
const trip_planning_controller_v2 = require('../controllers/v3/trip_planning');

router.get('/', function (req, res) {
  if(req.query.id) {
    trip_planning_controller_v2.getTrip(req, res)
  } else {
    trip_planning_controller_v2.getAllTripsv2(req, res)
  }
});

router.get('/realtime', trip_planning_controller_v2.getRealtimeStats);

router.get('/analytics', trip_planning_controller_v2.getAllAnalytics);

// single trip plan apis

router.post('/', trip_planning_controller_v2.add);

router.put('/', trip_planning_controller_v2.update);

router.delete('/', trip_planning_controller_v2.delete);

router.get('/log', trip_planning_controller_v2.getLog);

router.get('/info', trip_planning_controller_v2.getSummary);

router.get('/notifications', trip_planning_controller_v2.getNotifications);

router.get('/template', trip_planning_controller_v2.getTemplate);

router.post('/status-override', trip_planning_controller_v2.overrideTripStatus);

router.get('/status-logs', trip_planning_controller_v2.getUserStatusTripLog);

router.get('/shared-with', trip_planning_controller_v2.getSharedTripUsers);

router.put('/shared-with', trip_planning_controller_v2.updateSharedTripUsers);

router.get('/consignments', trip_planning_controller_v2.getConsignments);

router.get('/consignments/manifest', trip_planning_controller_v2.generateConsignmentManifestPdf);

router.get('/retrospective', trip_planning_controller_v2.getTripRetrospective);

/*  Routes for PDF HTML Templates */

router.post('/tripRetrospectiveTemplate', function (req, res) {
    
  res.render('tripRetrospectiveTemplate',req.body)
});

router.post('/route/planning/estimate', trip_planning_controller_v2.getRouteEstimate);

router.get('/stats', trip_planning_controller_v2.getAllTripsStats);

module.exports = router;