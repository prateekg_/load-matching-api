const express = require('express');
const router = express.Router();
const trip = require('../controllers/v3/trip_planning');
const tpl = require('../controllers/v3/third_party_logistics');


router.get('/trips', function (req, res) {
  trip.getAllSharedTrips(req, res)
});

router.get('/trips/log', tpl.getLog);

router.get('/trips/info', tpl.tripInfo);

router.get('/consignments', tpl.getAllSharedConsignments);

router.get('/consignments/info', tpl.getConsignmentInfo);

module.exports = router;
