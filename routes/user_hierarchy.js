const express = require('express');
const router = express.Router();

const userHierarchyController = require('../controllers/settings/user_hierarchy');

router.get('/tree', function (req, res) {
  userHierarchyController.getUserHierarchyTreeV1(req, res)
});

router.post('/tree', function (req, res) {
  userHierarchyController.createOrUpdateUserHierarchyTree(req, res)
});

router.get('/unassigned', function (req, res) {
  userHierarchyController.getUserNotAssignedToHierarchy(req, res)
});


module.exports = router;