/**
 * Created by numadic on 09/04/18.
 */

const express = require('express');
const router = express.Router();

const tracker_diagnostics_controller = require('../controllers/v3/tracker_diagnostics');
const consignment_mapping_controller = require('../controllers/v3/consignment_mapping');

router.get('/info', function(req, res){
  if(Number(req.query.id)){
    tracker_diagnostics_controller.getVehicleTrackerSummary(req, res)
  }else{
    tracker_diagnostics_controller.getTrackersSummary(req, res)
  }
});

router.get('/', function(req, res){
  if (req.query.id) {
    tracker_diagnostics_controller.getAssetTrackerDetail(req, res)
  } else {
    tracker_diagnostics_controller.getAllAssetTrackers(req, res, 'list')
  }
});

router.get('/portable', function(req, res){
  req.query.status = req.query.trackerStatus;
  consignment_mapping_controller.getPortableTrackersConsignments(req, res)
});

router.post('/report-issue', tracker_diagnostics_controller.reportDeviceStatus);

module.exports = router;
