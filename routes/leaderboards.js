const express = require('express');
const router = express.Router();
const vehicles = require('../controllers/v3/vehicles');
const drivers_v2 = require('../controllers/v3/drivers');

router.get('/', function(req, res) {
  switch(req.query.type){
  case 'vehicle_distance':
    return vehicles.getVehicleDistanceLeaderboard(req, res);
  case 'drivers':
    return drivers_v2.getDriverLeaderboard(req, res);
  default:
    return res.status(400).send('Enter leaderboard type')
  }
});

module.exports = router;