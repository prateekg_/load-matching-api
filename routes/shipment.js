/**
 * Created by numadic on 02/05/17.
 */
const express = require('express');
const router = express.Router();
const shipment_controller_v2 = require('../controllers/v3/trip_planning');


router.get('/summary', shipment_controller_v2.getShipmentSummary);

router.get('/log', shipment_controller_v2.getShipmentLog);

router.get('/map', shipment_controller_v2.getShipmentMap);

router.get('/info', shipment_controller_v2.getShipmentInfo);

router.get('/notifications', shipment_controller_v2.getShipmentNotifications);

router.get('/number', shipment_controller_v2.getNewShipmentNumber);

module.exports = router;