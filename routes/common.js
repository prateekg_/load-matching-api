const express = require('express');
const router = express.Router();
const common_v3 = require('../controllers/v3/common');

router.get('/deadzones', common_v3.getDeadZones);

router.post('/wv-error', common_v3.processWVError);

router.get('/types', common_v3.getTypes);

router.get('/documents', common_v3.downloadDocument);

router.get('/sensors', function (req, res) {
  switch(req.query.type) {
  case 'door':
    common_v3.getTripDoorSensorActivity(req, res);
    break;
  case 'temperature':
    common_v3.getAssetSensorDetails(req, res);
    break;
  default:
    res.status(400).send('Please specify a valid type');
    break
  }
});

module.exports = router;