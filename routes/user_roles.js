const express = require('express');
const router = express.Router();
const userRolesController = require('../controllers/settings/user_roles');

// List all roles of a company
router.get('/', function (req, res) {
  if(req.query.id){
    // get single role
    userRolesController.getSingleUserRole(req, res)
  }else{
    // get all roles
    userRolesController.getAllUserRoles(req, res)
  }
});

// create a new user role
router.post('/', userRolesController.createUserRole);

// update a user role
router.put('/', userRolesController.updateUserRole);

// map user to a role
router.post('/mapping', userRolesController.createUserRoleMapping);

router.delete('/mapping', userRolesController.deleteUserRoleMapping);

router.put('/mapping', userRolesController.updateUserRoleMapping);

router.get('/users', userRolesController.getAllUsersOfRole);
module.exports = router;