const express = require('express');
const router = express.Router();
const vehicles = require('./vehicles');
const eol = require('./eol');
const quick_search = require('./quick_search');
const common = require('./common');
const pois = require('./pois');
const settings = require('./settings/index');
const trip_planning = require('./trip_planning.js');
const routesTemplate = require('./route_template');
const shipment = require('./shipment');
const location_controller_v2 = require('../controllers/v3/location');
const trip_planning_controller = require('../controllers/v3/trip_planning');
const consignment = require('./consignment');
const fuels = require('./fuels');
const maintenance = require('./maintenance');
const drivers = require('./drivers');
const notification_v3 = require('../controllers/v3/notification');
const trackers = require('./tracker_diagnostics');
const immobilise = require('./immobilize');
const reports = require('./reports');
const tyres = require('./tyres');
const runsheet = require('./runsheet');
const leaderboards = require('./leaderboards');
const users = require('./users');
const chats = require('./chats');
const company = require('./company');
const billing = require('./billing');
const swaggerUi = require('swagger-ui-express'), swaggerDocument = require('../swagger/swagger');

router.use('/api/docs', swaggerUi.serve, swaggerUi.setup(
  swaggerDocument,
  null,
  null,
  '.swagger-ui .topbar { display: none }',
  'numadic.com/images/numadic-favicon.ico',
  null,
  'API Documentation'
));

router.use('/status', function (req, res) {
  res.status(200).send()
});
router.use('/vehicles', vehicles);
router.use('/search', quick_search);
router.use('/', common);
router.use('/pois', pois);

router.use('/eol', eol);
router.use('/', settings);

router.use('/trips', trip_planning);
router.use('/routetemplates', routesTemplate);
router.use('/shipments', shipment);
router.get('/loccode', location_controller_v2.searchPlace);
router.get('/geopath', trip_planning_controller.getTripRoute);

router.use('/consignments', consignment);
router.use('/fuels', fuels);
router.use('/maintenances', maintenance);
router.use('/drivers', drivers);

router.get('/notifications', notification_v3.getNotifications);
router.use('/trackers', trackers);
router.use('/immobilise', immobilise);
router.use('/reports', reports);
router.use('/tyres', tyres);
router.use('/runsheet', runsheet);
router.use('/leaderboards', leaderboards);
router.use('/users', users);
router.use('/company', company);
router.use('/chats', chats);
router.use('/billing', billing);
module.exports = router;
