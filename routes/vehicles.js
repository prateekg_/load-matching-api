const express = require('express');
const router = express.Router();
const vehicles_v3 = require('../controllers/v3/vehicles');

router.get('/', function (req, res) {
  if (req.query.id) {
    vehicles_v3.getVehicleDetail(req, res)
  } else {
    vehicles_v3.getAllVehicles(req, res)
  }
});

router.get('/stats', vehicles_v3.getVehicleStats);

router.get('/activity', function(req, res){
  switch(req.query.type){
  case 'gps':
    return vehicles_v3.forwardRequestAssetGPSData(req, res);
  case 'analytics':
    return vehicles_v3.getAssetActivityStats(req, res);
  default:
    console.log("Processing vehicle activity");
    return vehicles_v3.getAssetActivity(req, res)
  }
});

router.get('/activity/trail', vehicles_v3.getAssetActivityTrail);

router.get('/class', vehicles_v3.getVehicleClass);

module.exports = router;