const express = require('express');
const router = express.Router();
const consignment_controller_v2 = require('../controllers/v3/consignment');
const trip_planning_controller_v2 = require('../controllers/v3/trip_planning');
const consignment_mapping_controller = require('../controllers/v3/consignment_mapping');


router.get('/', function (req, res) {
  if(req.query.id) {
    consignment_controller_v2.getSingleConsignment(req, res)
  } else {
    consignment_controller_v2.getAllConsignments(req, res)
  }
});

router.post('/', consignment_controller_v2.createConsignment);

router.put('/', consignment_controller_v2.updateConsignment);

router.delete('/', consignment_controller_v2.deleteConsignment);

router.get('/table/config', consignment_controller_v2.getConsignmentTableConfig);

router.put('/table/config', consignment_controller_v2.updateConsignmentTableConfig);

router.get('/map', function (req, res) {
  consignment_controller_v2.getAllConsignments(req, res, 'map_location')
});

router.get('/notifications', consignment_controller_v2.getConsignmentNotifications);

router.get('/map/auto_trip/trail', consignment_controller_v2.getMapAutoTripTrail);

router.get('/info', consignment_controller_v2.getInfo);

router.get('/log/auto_trip', consignment_controller_v2.getLogAutoTrip);

router.get('/epod', consignment_controller_v2.getEpodInfo);

router.get('/status_count', consignment_controller_v2.getStatusCount);

router.get('/number', consignment_controller_v2.getNewConsignmentNumber);

router.get('/stats', consignment_controller_v2.getRealtimeStats);

router.get('/runsheet', trip_planning_controller_v2.generateRunsheetPdf);

router.get('/material/realtime', consignment_controller_v2.getMaterialRealtimeStats);

/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::: START: Consignment Mapping aPP APis ::::::::::::::::::::::::::::::::::::::::::*/

//Api to create auto consignment trip
router.post('/trip', consignment_mapping_controller.createConsignmentTrip);

//Api to map entity to a consignment
router.post('/entity/map', consignment_mapping_controller.createConsignmentTrip);

//Api to aknowledge and unmap entity from a consignment
router.put('/entity/unmap', consignment_mapping_controller.unmapEntity);

//Api to get list of currently mapped consignments 
router.get('/entity', function(req, res){
  if(req.query.e_id){
    consignment_mapping_controller.getConsignmentEntity(req,res)
  }
  else {
    consignment_mapping_controller.getPortableTrackersConsignments(req, res)
  }
});

//Api to update consignment mapping. Allow to update consignment no, vehicle number, source and destination
router.put('/entity', consignment_mapping_controller.updateConsignmentMapping);


/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::: END: Consignment Mapping aPP APis ::::::::::::::::::::::::::::::::::::::::::*/


module.exports = router;