/*
 * Created by numadic on 27/07/17.
 */

const express = require('express');
const billingController = require('../controllers/settings/billing');

const router = express.Router();

router.get('/invoices', billingController.getInvoices);
router.post('/payment/authorize', billingController.authorizePayment);

module.exports = router;