/**
 * Created by numadic on 08/02/18.
 */

const express = require('express');
const router = express.Router();
const immobilize_v2 = require('../controllers/v3/immobilize');

router.post('/otp/generate', immobilize_v2.generateOtp);
router.post('/otp/verify', immobilize_v2.verifyOtp);


module.exports = router;
