const express = require('express');
const router = express.Router();

const companyController = require('../controllers/settings/company');

router.get('/info', companyController.getCompanyDetails);

router.get('/rules', companyController.getCompanyRules);

router.post('/rules', companyController.createOrUpdateRule);

router.get('/credits', companyController.getAPICredits);

module.exports = router;