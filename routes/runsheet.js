const express = require('express');
const router = express.Router();

const drivers_v2 = require('../controllers/v3/drivers');

router.get('/packages', function (req, res, next) {
  if (req.query.id) {
    drivers_v2.getPackage(req, res, next)
  } else {
    drivers_v2.getAllPackages(req, res, next)
  }
});

router.post('/packages/status', drivers_v2.updatePackageStatus);

router.get('/packages/map', drivers_v2.getMap);

module.exports = router;