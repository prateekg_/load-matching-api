const express = require('express');
const router = express.Router();
const fuel_v2 = require('../controllers/v3/fuels');

router.get('/', function (req, res) {
  if (req.query.id) {
    fuel_v2.getDetail(req, res)
  } else {
    fuel_v2.getAll(req, res)
  }
});

router.put('/', fuel_v2.update);

router.delete('/', fuel_v2.delete);

router.post('/', fuel_v2.add);

router.get('/realtime', fuel_v2.getRealtimeAnalytics);

router.get('/summary', fuel_v2.getFuelSummary);

router.get('/activity', fuel_v2.getTripFuelSummary);

router.get('/instances', fuel_v2.getTripFuelInstances);

router.get('/instance', function (req, res) {
  if (req.query.id) {
    fuel_v2.getFuelInstance(req, res, 'list')
  }else{
    return res.status(400).send('Enter vehicle ID') 
  }
});

router.get('/map', function (req, res) {
  if (req.query.id) {
    fuel_v2.getFuelInstance(req, res, 'map')
  }else{
    return res.status(400).send('Enter vehicle ID') 
  }
});

module.exports = router;