const express = require('express');
const router = express.Router();

const service_v2 = require('../controllers/v3/maintenance');

router.get('/', function (req, res) {
  if (req.query.id) {
    service_v2.getDetail(req, res)
  } else {
    service_v2.getAll(req, res)
  }
});

router.put('/', service_v2.update);

router.delete('/', service_v2.delete);

router.post('/', service_v2.add);

router.get('/realtime', service_v2.getRealtimeAnalytics);

module.exports = router;