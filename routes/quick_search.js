const express = require('express');
const router = express.Router();

const quick_search_v3 = require('../controllers/v3/quick_search');

router.get('/', function(req, res){
  switch(req.query.type){
  case 'vehicles':
    return quick_search_v3.getAssets(req, res);
  case 'tyres':
    return quick_search_v3.getTyres(req, res);
  default:
    return quick_search_v3.search(req, res)
  }
});

module.exports = router;