const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const APP_LOGGER = require('./helpers/logger');
const LOGGER = APP_LOGGER.logger;
const indexRouter = require('./routes/index');
const clientV1Router = require('./routes/clientV1');
const tpl_routes = require('./routes/third_party_logistics');
const developmentAuthorization = require('./helpers/development_authorization');
const path = require('path');
const moment = require('moment');
const VISIBILITY_DOC = require('./swagger_visiblity_apis')

const app = express();

moment.tz.setDefault("Asia/Kolkata");
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger({
  format: ':date[iso] :method :url - :status - :response-time ms'
}
));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

if (app.get('env') === 'local') {
  app.use(developmentAuthorization);
}

app.use(function (req, res, next) {
  req.on('end', function () {
    let statusCode = res.statusCode;
    if (statusCode >= 200 && statusCode < 300) {
      // log info
      LOGGER.info(APP_LOGGER.formatMessage(req, null, statusCode))
    } else if (statusCode >= 400 && statusCode < 500) {
      // log warn
      LOGGER.warn(APP_LOGGER.formatMessage(req, res.body, statusCode))
    }
  });
  next()
});

app.use('/v3', indexRouter);
app.use('/3pl/v3', tpl_routes);
// Client V1 APIs
app.use('/client/v1', clientV1Router);
app.get('/docs/visibility', function (req, res) {
  return res.json(VISIBILITY_DOC)
})

// catch 404 and forward to error handler
app.use(function (req, res) {
  return res.status(404).send()
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res) {
    res.status(err.status || 500).send()
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res) {
  res.status(err.status || 500).send()
});

let server = app.listen(3003, function () {
  console.log('Metrics v3 server listening on port ' + server.address().port)
});

process.on('SIGINT', function () {
  server.close(function (err) {
    process.exit(err ? 1 : 0);
  });
});

module.exports = app;
