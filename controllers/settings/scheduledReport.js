const async = require('async');
const _ = require('lodash');
const request = require('request');
const models = require('../../db/models/index');
const env = process.env.NODE_ENV || 'development';
const config = require('../../config/index')[env];

function sendToReportEngine(type, id) {
  const options = {
    method: 'POST',
    url: config.reportEngine.url,
    body: {type: type, id: id},
    json: true
  };

  request(options, function () {

  });
}

exports.getReportTypes = function (req, res) {

  const query = "SELECT id, name, frequency FROM report_types";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (result) {
      res.json(result)
    }).catch(function () {
      res.status(500).send()
    })
};

exports.listAllScheduledReports = function (req, res) {

  const keyword = req.query.q || '';
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const type = req.query.type || "general";
  const offset = limit * page;
  const searchkey = keyword ? " AND sr.name ilike '%" + keyword + "%'" : "";
  const typeCondition = type === "custom" ? " AND rt.fk_c_id is not null" : " AND rt.fk_c_id is null";

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    }
    ,
    function (cb) {
      const query = 'SELECT sr.id, sr.name, sr.groups, sr.frequency, sr.trigger_time, sr.active, rt.name as type, sr."createdAt",' +
        " COUNT(*) OVER()::INT AS count " +
        " FROM scheduled_reports sr " +
        " INNER JOIN report_types rt ON rt.id = sr.fk_report_type_id" +
        " WHERE sr.fk_c_id = " + fk_c_id + searchkey + typeCondition +
        " OFFSET " + offset + " LIMIT " + limit;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function (result) {
          let tot_rows = 0;
          result.forEach(function (i) {
            tot_rows = i.count;
            i.groups =  _.filter(i.groups, 'active'); 
            delete i.count
          });
          cb(null, {
            count: tot_rows,
            data: result
          })
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    }
  ],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json(result)
    }
  })

};

exports.getScheduledReport = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  let id = req.query.id;
  let reportTypeId = req.query.reportTypeId;

  if (!id && !reportTypeId) {
    return res.status(400).send('Enter report id')
  }

  const whereCondition = id ? " AND sr.id = " + id : " AND rt.id = " + reportTypeId;

  const query = "SELECT sr.id, rt.id as \"typeId\", rt.name as \"typeName\", sr.frequency, sr.name, sr.summary, sr.groups, sr.active, sr.trigger_on as \"triggerOn\", sr.trigger_time as \"triggerTime\"," +
    " json_agg(json_build_object('id',g.id,'gid',g.gid,'name',g.name)) as company_groups FROM scheduled_reports sr " +
    " INNER JOIN report_types rt ON rt.id = sr.fk_report_type_id " +
    " LEFT JOIN groups g on g.fk_c_id = " + fk_c_id + " AND g.active" +
    " WHERE sr.fk_c_id = " + fk_c_id + whereCondition +
    " GROUP BY sr.id, rt.id";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (result) {
      if(result){
        const g = [];
        result.company_groups.forEach(function (i) {
          const m = _.find(result.groups, {id: i.id});
          i.active = m ? m.active : false;
          g.push(i)
        });
        result.groups = g;
        delete result.company_groups;
        res.json(result)
      }else{
        res.status(404).send()
      }
    }).catch(function () {
      res.status(500).send()
    })

};

exports.createScheduledReport = function (req, res) {
  // create new group

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  //validation
  if(!postData.name || postData.name.length < 2){
    return res.status(400).send("Enter group name")
  }
  if(!postData.typeId){return res.status(400).send("Enter report type ID")}


  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // create group

      const newScheduledReport = {
        fk_c_id: fk_c_id,
        fk_report_type_id: postData.typeId,
        name: postData.name,
        summary: postData.summary || null,
        groups: postData.groups || [],
        frequency: postData.frequency,
        active: true,
        trigger_time: postData.triggerTime,
        trigger_on: postData.triggerOn
      };

      models.scheduled_report.findOrCreate({
        where: {
          fk_c_id: newScheduledReport.fk_c_id,
          fk_report_type_id: newScheduledReport.fk_report_type_id,
          $or:{
            name: newScheduledReport.name
          }
        },
        defaults: newScheduledReport
      }).spread(function (group, created) {
        if (!created) {
          // not created - name already exists
          cb({
            code: 409,
            msg: "Report with same config exists"
          })
        } else {
          cb(null, 'Successfully created');
          sendToReportEngine('insert', group.get({raw: true}).id)
        }
      }).catch(function (err) {
        cb({
          code: 500,
          msg: err
        })
      })
    }
  ],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json(result)
    }
  })
};

exports.updateScheduledReport = function (req, res) {
  // update group

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  //validation
  let id = postData.id;
  if(!id){
    return res.status(400).send("Enter report ID")
  }

  const updateReport = {
    frequency: postData.frequency
  };

  if(postData.groups){
    updateReport.groups = postData.groups
  }

  if(postData.name && postData.name.length > 2){
    updateReport.name = postData.name
  }

  if(postData.summary && postData.summary.length > 2){
    updateReport.summary = postData.summary
  }

  if(postData.active !== null && typeof postData.active === 'boolean'){
    updateReport.active = postData.active
  }

  if(postData.triggerOn !== null) {
    updateReport.trigger_on = postData.triggerOn
  }

  if(postData.triggerTime !== null) {
    updateReport.trigger_time = postData.triggerTime
  }

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {

      const query = "SELECT * FROM scheduled_reports WHERE active AND fk_c_id = " + fk_c_id + " AND id != '" + id + "' AND name ilike '" + updateReport.name + "' LIMIT 1";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (matched) {
          if(matched){
            cb({
              code:409,
              msg: "report name already exists"
            })
          }else{
            models.scheduled_report.update(updateReport , {
              where: {
                fk_c_id: fk_c_id,
                id:  id
              }
            }).spread(function (result) {
              if(result > 0){
                cb(null, "report updated");
                sendToReportEngine('update', id)
              }else{
                cb({
                  code: 404,
                  msg: "report not found"
                })
              }
            }).catch(function (err) {
              cb(err)
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    }
  ],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json(result)
    }
  })
};

exports.disableScheduledReport = function (req, res) {

  // disable scheduled report

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  //validation
  if(!postData.id){
    return res.status(400).send("Enter group ID")
  }
  const id = postData.id;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    }
    ,
    function (data, cb) {
      // delete group

      models.scheduled_report.update({
        active: false
      }, {
        where: {
          fk_c_id: fk_c_id,
          id: id
        }
      }).spread(function (result) {
        if(result > 0){
          cb(null, "Reports disabled");
          sendToReportEngine('remove', id)
        }else{
          cb({
            code: 404,
            msg: "report not found"
          })
        }
      }).catch(function (err) {
        cb(err)
      })
    }
  ],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json(result)
    }
  })
};
