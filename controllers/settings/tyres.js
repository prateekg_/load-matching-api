const moment = require('moment');
const models = require('../../db/models');

const TYRE_SERVICE_TYPES = ['repair', 'rethread', 'replace', 'rotate'];

exports.getSingleTyre = function (req, res) {

  let fk_tyre_id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;

  if(!fk_tyre_id){
    return res.status(400).send("Invalid tyre service id")
  }else{

    models.tyre.find({
      where:{
        id: fk_tyre_id,
        fk_c_id: fk_c_id
      },attributes: {
        exclude: ['createdAt','updatedAt','fk_asset_id']
      }
    }).then(function (tyre) {
      if(!tyre){
        res.status(404).send("tyre service not found")
      }else {
        res.json(tyre)
      }
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
};

exports.createTyre = function (req, res) {
  // todo : handle asset mapping and unmapping after logic is finalized.

  const fk_c_id = req.headers.fk_c_id;
  const gid = typeof req.headers.group_ids === 'string' ? JSON.parse(req.headers.group_ids) : req.headers.group_ids;
  let new_tyre = req.body;
  let fk_asset_id = null;

  if(typeof new_tyre !== 'object'){
    res.status(400).send("Invalid data")
  }else if(!new_tyre.tyre_id){
    res.status(400).send("Invalid TIN")
  }else{

    if(new_tyre.asset && new_tyre.asset.id){
      fk_asset_id = new_tyre.asset.id;
      new_tyre.fk_asset_id = fk_asset_id
    }

    new_tyre.fk_c_id = fk_c_id;

    models.tyre.findOrCreate({
      where: {
        tyre_id: new_tyre.tyre_id,
        fk_c_id: fk_c_id,
        active: true
      },
      defaults: new_tyre
    }).spread(function (tyre, created) {

      if(!created){
        res.status(400).send("TIN already exists")
      }else{

        const tyre_group = {
          fk_group_id: gid[0],
          fk_tyre_id: tyre.id,
          connected: true,
          tis: moment().unix()
        };

        models.tyre_group_mapping.findOrCreate({
          where:{
            fk_group_id: tyre_group.fk_group_id,
            fk_tyre_id: tyre_group.fk_tyre_id,
            connected: tyre_group.connected
          },
          defaults: tyre_group
        }).spread(function () {
          res.status(201).send("tyre created sucessfully")
        }).catch(function (err) {
          res.status(500).send("error mapping tyre to a group : ", err)
        })
      }
    }).catch(function (err) {
      res.status(500).send(err)
    })

  }
};

exports.updateTyre = function (req, res) {
  // todo : handle asset mapping and unmapping after logic is finalized.

  const fk_c_id = req.headers.fk_c_id;
  let update_tyre = req.body;
  let fk_asset_id = null;

  if(typeof update_tyre !== 'object'){
    res.status(400).send("Invalid data")
  }else if(!update_tyre.tyre_id){
    res.status(400).send("Invalid TIN")
  }else if(!update_tyre.id){
    res.status(400).send("Invalid tyre id")
  }else{

    if(update_tyre.asset && update_tyre.asset.id){
      fk_asset_id = update_tyre.asset.id;
      update_tyre.fk_asset_id = fk_asset_id
    }

    models.tyre.update(update_tyre, {
      where: {
        id: update_tyre.id,
        fk_c_id: fk_c_id
      }
    }).then(function (result) {
      if(result === 1){
        res.status(201).send("tyre updated sucessfully")
      }else{
        res.status(400).send("invalid tyre id")
      }
    }).catch(function (err) {
      res.status(500).send(err)

    })
  }

};

exports.getTyreService = function (req, res) {

  let service_id = req.query.id;

  if(!service_id){
    return res.status(400).send("Invalid tyre service id")
  }else{

    const query = "SELECT ts.id, ts.scheduled_tis, ts.task, ts.tyres, ts.location,ts.cost," +
      " ts.odometer, ts.note, ts.is_scheduled, ts.completed_tis,ts.est_cost, " +
      " json_build_object('id',a.id,'licNo',a.lic_plate_no,'operator', c.cname, 'type', ast.type) as asset" +
      " FROM tyre_servicings ts" +
      " LEFT JOIN assets a on a.id = ts.fk_asset_id" +
      " LEFT JOIN asset_types ast on ast.id = fk_ast_type_id" +
      " LEFT JOIN companies c on c.id = a.fk_c_id" +
      " WHERE ts.id = " + service_id +
      " LIMIT 1";

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (record) {
        if(!record){
          res.status(404).send()
        }else{
          res.json(record)
        }
      }).catch(function (err) {
        res.status(500).send(err)
      })
  }
};

exports.createTyreService = function (req, res) {
  // todo : handle asset mapping and unmapping after logic is finalized.

  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  let new_tyre_service = req.body;
  let fk_asset_id = null;

  if(typeof new_tyre_service !== 'object'){
    res.status(400).send("Invalid data")
  }else if(TYRE_SERVICE_TYPES.indexOf(new_tyre_service.task) === -1){
    res.status(400).send("Invalid tyre service type")
  }else{

    if(new_tyre_service.asset && new_tyre_service.asset.id){
      fk_asset_id = new_tyre_service.asset.id;
      new_tyre_service.fk_asset_id = fk_asset_id
    }

    new_tyre_service.fk_u_id = fk_u_id;
    new_tyre_service.fk_c_id = fk_c_id;

    models.tyre_servicing.create(new_tyre_service).then(function (result) {
      if(result){
        res.status(201).send("tyre service created sucessfully")
      }else{
        res.status(500).send("error creating tyre service")
      }
    }).catch(function (err) {
      res.status(500).send(err)

    })
  }
};

exports.updateTyreService = function (req, res) {
  // todo : handle asset mapping and unmapping after logic is finalized.

  const fk_c_id = req.headers.fk_c_id;
  let tyre_service = req.body;
  let fk_asset_id = null;

  if(typeof tyre_service !== 'object'){
    res.status(400).send("Invalid data")
  }else if(TYRE_SERVICE_TYPES.indexOf(tyre_service.task) === -1){
    res.status(400).send("Invalid tyre service type")
  }else if(!tyre_service.id){
    res.status(400).send("Invalid tyre service id")
  }else{

    if(tyre_service.asset && tyre_service.asset.id){
      fk_asset_id = tyre_service.asset.id;
      tyre_service.fk_asset_id = fk_asset_id
    }

    models.tyre_servicing.update(tyre_service, {
      where: {
        id: tyre_service.id,
        fk_c_id: fk_c_id
      }
    }).then(function (result) {
      if(result === 1){
        res.status(201).send("tyre service updated sucessfully")
      }else{
        res.status(400).send("invalid tyre service id")
      }
    }).catch(function (err) {
      res.status(500).send(err)

    })
  }

};

exports.deleteTyreService = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const tyre_service = req.body;

  if(!tyre_service.id){
    res.status(400).send("Enter tyre service id")
  }else{
    models.tyre_servicing.update({
      active: false
    },
    {
      where: {
        id: tyre_service.id,
        fk_c_id: fk_c_id
      }
    }).then(function (result) {
      if(result === 1){
        res.status(200).send("sucessfully deleted")
      }else{
        res.status(401).send("unable to delete")
      }
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
};