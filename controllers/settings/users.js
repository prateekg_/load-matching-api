const async = require('async');
const _ = require('lodash');
const models = require('../../db/models');
const bcrypt = require('bcrypt');
const env = process.env.NODE_ENV || 'development';
const config = require('../../config/index')[env];
const nuRedis = require('../../helpers/nuRedis');
const emails = require('../../helpers/emails');
const rabbitMq = require('../../helpers/rabbitMQ');
const utils = require('../../helpers/util');
const generator = require('generate-password');
const userService = require('../../services/userService');
const request = require('request');


exports.updateUserPreferences = function (req, res) {
  const fk_u_id = req.headers.fk_u_id;
  const body = req.body;

  models.user_preference.update(body, { where: { fk_u_id: fk_u_id } }).then(function () {
    res.status(200).send('Preferences updated successfully')
  }).catch(function () {
    res.status(500).send('Internal server error')
  })
};

exports.getUserPreferences = function (req, res) {
  const fk_u_id = req.headers.fk_u_id;
  const query = `SELECT walkthrough_needed, preferences FROM user_preferences WHERE fk_u_id = ${fk_u_id};`

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (result) {
      res.json(result)
    }).catch(function () {
      res.status(500).send('Internal server error')
    })
};

exports.adminUpdateUserProfile = function (req, res) {

  const companyId = req.headers.fk_c_id;
  const adminUserId = req.headers.fk_u_id;
  const adminEmail = req.headers.email;

  const name = req.body.name;
  const contactNo = req.body.cont_no;
  const newEmail = req.body.email || '';
  const userId = req.body.user_id;

  const updateObj = {
    email: newEmail,
    name: name,
    cont_no: contactNo
  };

  if (newEmail) updateObj.email = newEmail;

  const rmqPassword = generator.generate({
    length: 16,
    numbers: true
  });

  const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
    " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
    " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + companyId + " AND fk_u_id = " + adminUserId;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (result) {
      if (result && result.is_admin) {
        async.waterfall([
          function (callback) {
            if (newEmail) {
              const query = " SELECT email from users where email =  '" + newEmail + "' and id !=" + userId;

              models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
                .spread(function (result) {
                  if (result && result.email) callback({ code: 400, msg: "This email already exist" });
                  else callback(null)
                });
            }
            else callback(null)
          },
          function (callback) {
            const query = " SELECT email, name, rmq_p_word from users where id =  " + userId;

            models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
              .spread(function (result) {

                if (result.email === newEmail) {
                  callback(null, result)
                }
                else if (newEmail) {
                  async.parallel([
                    function (pcallback) {
                      nuRedis.deleteRedisKey("TOKENS-" + result.email, function () {
                        pcallback(null)
                      })
                    },
                    function (pcallback) {
                      const query = " SELECT g.gid as group_keys" +
                        " FROM user_group_mappings ugm " +
                        " INNER JOIN groups g on ugm.fk_group_id = g.id" +
                        " WHERE fk_u_id = " + userId;

                      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
                        .spread(function (result) {
                          async.each(result.group_keys, function (group_key, eachCallback) {
                            nuRedis.deleteRedisKey("GP-" + group_key, function () {
                              eachCallback()
                            })
                          }, function () {
                            pcallback(null)
                          })
                        });
                    }
                  ], function () {
                    callback(null, result)
                  })
                }
                else callback(null, result)
              });
          },
          function (userDetails, callback) {
            updateObj.rmq_p_word = rmqPassword;

            models.user.update(updateObj, {
              where: {
                id: userId
              }
            }).then(function () {
              callback(null, userDetails)
            }).catch(function (err) {
              console.log(err);
              callback({ code: 500, msg: "Internal error" })
            })
          },
          function (userDetails, callback) {
            if (userDetails.email === newEmail) {
              callback(null, userDetails)
            }
            else {
              async.waterfall([
                function (wcallback) {

                  rabbitMq.deleteRabbitMqAccount(userDetails.email, userDetails.rmq_p_word, null, function (err) {
                    wcallback(err)
                  })
                },
                function (wcallback) {

                  rabbitMq.generateRabbitMqAccount(newEmail, rmqPassword, '', function (err) {
                    wcallback(err)
                  })
                }
              ], function (err) {
                callback(err, userDetails)
              })
            }
          },
          function (userDetails, callback) {
            emails.userEmailChange(newEmail, adminEmail, updateObj, function (err) {
              if (err) callback({ code: 500, msg: "Error sending email" });
              else callback(null)
            })
          }
        ], function (err) {
          if (err) res.status(err.code).send(err.msg);
          else res.json("Updated user settings email sent to the user")
        })
      }
      else {
        res.status(403).send('User does not have permissions')
      }
    }).catch(function (err) {
      console.log(err);
      res.status(500).send('Internal server error' + err)
    })



};

exports.adminUpdateUserPassword = function (req, res) {

  const userData = req.body;
  const companyId = req.headers.fk_c_id;
  const adminUserId = req.headers.fk_u_id;
  const adminEmail = req.headers.email;
  const req_origin = req.headers.origin && req.headers.origin.includes("numadic") ? req.headers.origin : 'http://app.numadic.com';
  const newPassword = req.body.new_password;
  const userId = req.body.user_id;
  const sendEmail = userData.send_email;

  const updateObj = {};

  if (newPassword && newPassword.length < 6) return res.status(422).send("Password length should be greater than 5");

  if (newPassword) {
    updateObj.p_word = bcrypt.hashSync(newPassword, 10);
  }

  const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
    " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
    " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + companyId + " AND fk_u_id = " + adminUserId;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (result) {
      if (result && result.is_admin) {
        async.waterfall([
          function (callback) {
            console.log("======= in update ======");

            models.user.update(updateObj, {
              where: {
                id: userId
              }
            }).then(function () {
              callback(null)
            }).catch(function (err) {
              console.log(err);
              callback({ code: 500, msg: "Internal error" })
            })
          },
          function (callback) {
            if (sendEmail) {
              const query = " SELECT email, name from users where id =  " + userId;

              models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
                .spread(function (result) {
                  const email_data = {
                    name: result.name,
                    password: newPassword,
                  };
                  emails.userPasswordChange(result.email, adminEmail, email_data, req_origin, function (err) {
                    if (err) callback({ code: 500, msg: "Error sending email" });
                    else callback(null)
                  })
                });
            }
            else callback(null)
          }
        ], function (err) {
          if (err) res.status(err.code).send(err.msg);
          else res.json("Updated user settings email sent to the user")
        })
      }
      else {
        res.status(403).send('User does not have permissions')
      }
    }).catch(function (err) {
      console.log(err);
      res.status(500).send('Internal server error' + err)
    })



};

exports.updateCompany = function (req, res) {

  const userAuth = req.headers.auth || 0;
  const companyId = req.headers.fk_company_id || req.headers.fk_c_id;
  const companyInfo = req.body;

  const companyModel = ['email', 'cname', 'country', 'cont_no', 'street', 'ow_name', 'contact_type', 'state', 'city', 'zip', 'company_detail'];
  const companyDetailModel = ['w_ph', 'm_ph', 'h_ph', 'o_ph', 'w_web', 'p_web', 'o_web', 'linkedin', 'twitter', 'googleplus', 'facebook'];

  const companyObject = _.omitBy(_.pick(companyInfo, companyModel), _.isNull);
  const companyDetailObject = _.omitBy(_.pick(companyInfo, companyDetailModel), _.isNull);

  if (userAuth === 0 || userAuth === 1) {
    models.company.update(companyObject, {
      where: {
        id: companyId
      }
    }).spread(function (result) {

      if (result !== 1) {
        // could not update
        res.status(500).send("Error updating company details")
      } else {
        models.company_details.update(companyDetailObject, {
          where: {
            fk_c_id: companyId
          }
        }).spread(function (r2) {
          if (r2 !== 1) {
            // could not update
            res.status(500).send("Error updating company details")
          } else {
            res.json("Company info updated")
          }
        }).catch(function () {
          res.status(500).send("Internal error ")
        })
      }
    }).catch(function () {
      res.status(500).send("Internal error ")
    })

  } else {
    res.status(401).send("UNAUTHORIZED")
  }
};

exports.updatePassword = function (req, res) {

  const userId = req.headers.fk_u_id;
  const companyId = req.headers.fk_c_id;
  const newPassword = req.body.new_p_word || '';
  const curPassword = req.body.cur_p_word || '';

  if (newPassword.length >= 6) {

    models.user.findOne({
      where: {
        id: userId,
        fk_c_id: companyId
      }
    }).then(function (result) {

      if (result) {

        if (bcrypt.compareSync(curPassword, result.p_word)) {

          const password = bcrypt.hashSync(newPassword, 10);

          models.user.update({
            p_word: password
          }, {
            where: {
              id: userId
            }
          }).then(function () {
            res.json("password updated")
          }).catch(function () {
            res.status(500).send("Internal error")
          })

        }
        else {
          res.status(403).send("Invalid current password")
        }
      }
      else {
        res.json("invalid user")
      }

    }).catch(function (err) {
      res.status(500).send(err.message)
    })
  } else {
    res.status(422).send('Too short password')
  }

};

exports.updateEmail = function (req, res) {

  const userId = req.headers.fk_u_id;
  const companyId = req.headers.fk_c_id;


  if (req.body && req.body.email) {
    const emailObj = {
      fk_u_id: userId,
      fk_c_id: companyId,
      n_email: req.body.email,
      o_email: req.headers.email,
      approved: false
    };

    models.email_request.create(emailObj).then(function () {
      res.json("Email change request sent to admin.")
    }).catch(function () {
      res.status(500).send("Internal error")
    })
  } else {
    res.status(400).send("Bad Request")
  }


};

exports.disableUser = function (req, res) {
  const userData = req.body;
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
    " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
    " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (result) {
      if (result && result.is_admin) {
        models.user.update({
          active: false
        }, {
          where: {
            fk_c_id: fk_c_id,
            id: userData.userId
          }
        }).spread(function (updated) {
          if (updated > 0) {
            res.status(200).send("User deleted successfully")
          } else {
            res.status(404).send('Invalid user id')
          }
        }).catch(function () {
          res.status(500).send('Internal server error')
        })
      } else {
        res.status(403).send('User does not have permissions')
      }
    }).catch(function () {
      res.status(500).send('Internal server error')
    })
};

exports.enableUser = function (req, res) {
  const userData = req.body;
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
    " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
    " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (result) {
      if (result && result.is_admin) {
        models.user.update({
          active: true
        }, {
          where: {
            fk_c_id: fk_c_id,
            id: userData.userId
          }
        }).spread(function (updated) {
          if (updated > 0) {
            res.status(200).send('User enabled successfully')
          } else {
            res.status(404).send('Invalid user id')
          }
        }).catch(function () {
          res.status(500).send('Internal server error')
        })
      } else {
        res.status(403).send('User does not have permissions')
      }
    }).catch(function () {
      res.status(500).send('Internal server error')
    })
};

exports.getAllCompanyUsers = function (req, res) {

  const keyword = req.query.q || '';
  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = limit * page;
  const gid = req.query.gid;
  const active = req.query.active || true;

  async.auto({
    users: function (callback) {
      const query = "SELECT d.id, concat(d.firstname,' ',d.lastname) as name, email, d.firstname, d.lastname, d.active, d.total_groups as \"totalGroups\", d.groups, d.created_at," +
        " d.last_login as \"lastLogin\", d.roles, d.contacts, COUNT(*) OVER()::INT AS count from (" +
        " SELECT u.id, u.firstname, u.lastname, u.email, u.active, COUNT(DISTINCT g.id)::INT as  total_groups," +
        " COALESCE(json_agg(DISTINCT g.name) FILTER (WHERE g.name IS NOT NULL),'[]') as groups, EXTRACT(EPOCH FROM u.\"createdAt\")::INT AS created_at," +
        " u.last_login as last_login, COALESCE('" + gid + "' = ANY (array_agg(g.gid)::text[]), false) as gid," +
        " COALESCE(json_agg(DISTINCT ur.name) FILTER (WHERE ur.name IS NOT NULL),'[]') as roles," +
        " json_build_object('primary_email', primary_email, 'secondary_email',secondary_email,'primary_cont_num',primary_cont_num,'secondary_cont_num',secondary_cont_num) as contacts" +
        " FROM users u" +
        " LEFT JOIN user_group_mappings ugm ON ugm.fk_u_id = u.id" +
        " LEFT JOIN groups g ON ugm.fk_group_id = g.id" +
        " LEFT JOIN user_role_mappings urm on urm.fk_user_id = u.id AND urm.active" +
        " LEFT JOIN user_roles ur on ur.id = urm.fk_user_role_id " +
        " LEFT JOIN contact_relations cr ON cr.fk_party_id = u.id AND cr.related_to = 'user'" +
        " LEFT JOIN contacts ct ON cr.fk_contact_id = ct.id" +
        " WHERE u.is_internal = false AND u.fk_c_id = " + fk_c_id + " AND (u.name ilike '%" + keyword + "%' OR u.firstname ilike '%" + keyword + "%' OR u.lastname ilike '%" + keyword + "%') and u.active =" + active +
        " GROUP BY u.id, ct.id " +
        ") d WHERE d.gid = false" +
        " OFFSET " + offset + " LIMIT " + limit;

      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
        .then(function (result) {
          callback(null, result)
        }).catch(function () {
          res.status(500).send()
        })
    }
  }, function (err, result) {
    let tot_rows = 0;

    result.users.forEach(function (i) {
      tot_rows = i.count;
      delete i.count
    });
    res.json({
      count: tot_rows,
      data: result.users
    })
  })
};

exports.checkUniqueEmail = function (req, res) {
  if (!req.query.email) {
    res.status(400).send('Email id missing')
  }
  else {
    userService.uniqueEmail(req.query.email, function (err) {
      if (err) {
        res.status(err.code).send(err.message)
      }
      else {
        res.send();
      }
    })
  }
};

/**
 * API forwards request to WAD
 * @param req
 * @param res
 */
exports.getUserV1 = function (req, res) {
  const id = req.query.id || req.headers.fk_u_id;

  const request_options = {
    uri: config.server_address.wad_server() + '/users/get',
    method: 'GET',
    headers: {
      'Authorization': 'bearer ' + config.tokens.numadicAdmin
    },
    qs: {
      id: id
    },
    json: true
  };
  request(request_options).pipe(res)
};

/**
 * API forwards request to WAD
 * @param req
 * @param res
 */
exports.updateUserV1 = function (req, res) {
  const user = req.body;
  try {
    user.fk_c_id = req.headers.fk_c_id
  } catch (e) {
    console.error(e)
    return res.status(400).send("Invalid body")
  }
  const request_options = {
    uri: config.server_address.wad_server() + '/users/update',
    method: 'PUT',
    headers: {
      'Authorization': 'bearer ' + config.tokens.numadicAdmin
    },
    json: user
  };
  request(request_options).pipe(res)
};

/**
 * API forwards request to WAD
 * @param req
 * @param res
 */
exports.createUserAndGroupsV1 = function (req, res) {

  const user = req.body;
  try {
    user.fk_c_id = req.headers.fk_c_id;

    user.contacts.label = user.firstname + " " + user.lastname;
    user.contacts.cont_type = 'Personal';
    user.contacts.primary_contact = true
  } catch (e) {
    console.error(e)
    return res.status(400).send("Invalid body")
  }
  const request_options = {
    uri: config.server_address.wad_server() + '/users/create',
    method: 'POST',
    headers: {
      'Authorization': 'bearer ' + config.tokens.numadicAdmin
    },
    json: user
  };
  request(request_options).pipe(res)
};

exports.getUsersFeaturesAndGroups = function (req, res) {
  /*
    const request_options = {
      uri: config.server_address.wad_server() + '/companies/features',
      method: 'GET',
      headers: {
        'Authorization': 'bearer ' + config.tokens.numadicAdmin,
        email: req.headers.email
      },
      qs: {
        cid: req.headers.fk_c_id,
        platform: utils.getPlatformFromURL(req)
      }
    };
  
    request(request_options).pipe(res)
    */

  const fk_u_id = req.headers.fk_u_id;

  var query = ` select u.id, u.email, true = any (array_agg(g.is_admin)) as is_admin,
    json_agg(json_build_object('id', g.id, 'gid', g.gid, 'name', g.name)) as group_access,
    cp.features, cp.expiry_tis as subscription_expiry_tis, json_agg(ur.config) role_features
    from users u
    left join user_group_mappings ugm on ugm.fk_u_id = u.id and ugm.connected is true
    left join user_preferences up on up.fk_u_id = u.id
    left join groups g on g.id = ugm.fk_group_id and g.active is true
    left join companies c on c.id = u.fk_c_id
    left join company_plans_vw cp on cp.fk_c_id = c.id AND cp.platform = 'app'
    left join company_plans_vw cpp on cpp.fk_c_id = c.id
    left join user_role_mappings urm on urm.fk_user_id = u.id and urm.active
    left join user_roles ur on ur.id = urm.fk_user_role_id
    where u.active and u.id = $id
    group by u.id, c.id, up.id, cp.features, cp.expiry_tis order by c.id`

  models.sequelize.query(query, { bind: { id: fk_u_id }, type: models.sequelize.QueryTypes.SELECT })
    .spread(function (userData) {
      // featureList, subscription_expiry_tis,payment_overdue,groupAccess
      userData.payment_overdue = false
      userData.role_features = _.compact(userData.role_features)
      userData.featureList = utils.processUserRoleFeatures(userData)
      userData.groupAccess = _.uniqBy(userData.group_access, 'id')

      delete userData.group_access
      delete userData.is_admin
      delete userData.role_features
      delete userData.features
      res.json(userData)
    }).catch(function (err) {
      res.status(500).send("Internal server error");
    });
};