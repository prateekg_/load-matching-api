const async = require('async');
const _ = require('lodash');
const request = require('request');
const models = require('../../db/models/index');
const env = process.env.NODE_ENV || 'development';
const config = require('../../config/index')[env];
const Hashids = require("hashids");
const hashids = new Hashids("CD234cF2)(vgdkjs$23153%$##(", 16, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
const assetHelper = require('../../helpers/common');

exports.getAllList = function (req, res) {

  const searchKeyword = req.query.q || '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const gid = req.query.gid;
  const fk_c_id = req.headers.fk_c_id;

  const query = "select d.id, d.name, d.lic_plate_no, count(*) OVER() AS total_count from (" +
    " select a.id, a.name, a.lic_plate_no, '" + gid + "' = ANY (array_agg(g.gid)::text[]) as gid from groups g" +
    " inner join asset_group_mappings agm ON agm.fk_group_id = g.id" +
    " inner join assets a on a.id = agm.fk_asset_id and a.active" +
    " INNER JOIN (" +
    "    SELECT DISTINCT a.id" +
    "    FROM groups g " +
    "    INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected" +
    "    INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active" +
    "    WHERE g.fk_c_id = " + fk_c_id + " and g.is_admin and agm.connected and a.active" +
    " ) ca on ca.id = a.id" +
    " where g.active and g.fk_c_id = " + fk_c_id +
    " group by a.id" +
    " ) d where  d.gid = false AND (d.name ilike '%" + searchKeyword + "%' or  d.lic_plate_no ilike '%" + searchKeyword + "%')" +
    " LIMIT " + limit + " OFFSET " + offset;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (assets) {
    let totalAssets = 0;
    assets.forEach(function (asset) {
      totalAssets = Number(asset.total_count) || 0;
      delete asset.total_count
    });
    res.json({
      total_count: totalAssets,
      data: assets
    })
  }).catch(function (err) {
    console.error(err);
    res.status(500).send()
  })
};

exports.details = function(req, res){

  let assetId = req.query.id;
  const fk_company_id = req.headers.fk_company_id;

  if(!assetId){
    res.status(400).send("asset id not specified")
  }else {

    const query = 'select a.id, a.asset_id as "assetId",a.name,ast.type,a.spd_lmt as "spdLimit",a.load_lmt as "loadLimit",a.has_eng as "hasEng",' +
      ' a.eng_no as "engNo",a.length,a.width,a.height,a.tot_tyres as "totTyres",a.lic_plate_no as "licPlate",a.mfg,a.yr_of_mfg as "mfgYear",' +
      ' a.model,a.veh_id_no as "vin",a.oth_details as "notes",a.isbuiltintrailer as "hasBuiltInTrailer",a.is_refrig as "isRefrig",a.no_temp_zone as "totTempZones",' +
      ' a.zone_lmts as "tempZones",a.p_name as "inChargeName",json_build_object(\'cid\', c.cid, \'name\', c.cname) AS company , COALESCE(tas.shared,false) as "sharedAsset"' +
      ' from assets a inner join asset_types ast on ast.id = a.fk_ast_type_id  inner join companies c on c.id = a.fk_c_id' +
      ' left join asset_sharings tas ON tas.fk_asset_id = a.id AND tas.fk_shared_c_id = ' + fk_company_id + ' AND tas.shared is TRUE' +
      ' where a.active is TRUE AND ( a.fk_c_id = ' + fk_company_id + ' or tas.fk_shared_c_id = ' + fk_company_id + ') and a.asset_id =  \'' + assetId + '\' limit 1';

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (result) {
        if(result){
          res.json(result)
        }else{
          res.status(404).send('Not found');
        }
      }).catch(function (err) {
        console.error(err);
        res.status(500).send('Internal error');
      })
  }

};

exports.driverMapping = function (req, res) {

  if(req.header.auth === 1) {
    const options = {
      uri: config.url.userManagement + '/asset/assetmapping',
      method: req.method,
      json: req.body,
      headers: req.headers
    };

    request(options, function(error, response, body){
      if(error) {
        if(error.code === 'ECONNREFUSED') {
          res.status(500).send('Cannot connect to server.');
        } else if(error.code === 'ECONNRESET') {
          res.status(500).send('Failed to get response');
        } else {
          res.status(500).send('Internal error');
        }
      } else {
        res.status(response.statusCode).send(body);
      }
    });
  } else {
    res.status(401).send('Unauthorized to access');
  }

};

exports.update = function (req, res) {
  const item = req.body;
  const companyId = req.headers.fk_company_id;
  async.waterfall([
    function (callback) {
      //  check if values are not null
      if (!item.assetId) {
        callback({
          type: 100,
          code: 422,
          msg: "Invalid asset Id"
        })
      }else if (!item.type){
        callback({
          type: 100,
          code: 422,
          msg: "Invalid asset type"
        })
      } else {
        const masterObject = {
          asset_id: item.assetId,
          name: assetHelper.formatWithStartCase(item.name),
          type: item.type,
          spd_lmt: item.spdLimit,
          load_lmt: item.loadLimit,
          has_eng: item.hasEng,
          eng_no: item.engNo,
          length: item.length,
          width: item.width,
          height: item.height,
          tot_tyres: item.totTyres,
          lic_plate_no: assetHelper.formatLicPlate(item.licPlate),
          mfg: assetHelper.formatWithStartCase(item.mfg),
          yr_of_mfg: item.mfgYear,
          model: item.model,
          veh_id_no: item.vin,
          oth_details: item.notes,
          isbuiltintrailer: item.hasBuiltInTrailer,
          is_refrig: item.isRefrig,
          no_temp_zone: item.totTempZones,
          zone_lmts: item.tempZones,
          p_name: assetHelper.formatWithStartCase(item.inChargeName)
        };

        const validator = {
          Truck: ["asset_id", "name", "spd_lmt", "load_lmt", "has_eng", "eng_no", "length", "width", "height", "tot_tyres", "lic_plate_no", "mfg", "yr_of_mfg", "model", "veh_id_no", "oth_details", "isbuiltintrailer", "is_refrig", "no_temp_zone", "zone_lmts", "p_name"],
          Forklift: ["asset_id", "name", "spd_lmt", "load_lmt", "has_eng", "eng_no", "length", "width", "height", "tot_tyres", "lic_plate_no", "mfg", "yr_of_mfg", "model", "veh_id_no", "oth_details", "p_name"],
          Trailer: ["asset_id", "name", "spd_lmt", "load_lmt", "length", "width", "height", "tot_tyres", "lic_plate_no", "mfg", "yr_of_mfg", "model", "veh_id_no", "oth_details", "isbuiltintrailer", "is_refrig", "no_temp_zone", "zone_lmts", "p_name"],
          Container: ["asset_id", "name", "load_lmt", "length", "width", "height", "p_name"]
        };

        const asset = _.omitBy(_.pick(masterObject, validator[item.type]), _.isNull);

        callback(null, asset)
      }
    },
    function (asset, callback) {
      //  check if asset exists and updating fields are unique
      models.asset.findAll({
        attributes: ['id','asset_id', 'name', 'lic_plate_no'],
        where: {
          fk_c_id: companyId,
          $or: [{
            lic_plate_no: asset.lic_plate_no
          }, {
            name: asset.name
          }]
        },
        raw: true
      }).then(function (r) {

        if (!r || r.length === 0) {
          callback({type: 100, code: 422, msg: "Invalid asset Id"})
        }
        else {
          const validations = {
            name: null,
            licPlate: null
          };
          let errors = false;
          r.forEach(function (item) {
            if (item.asset_id === asset.asset_id) {
              console.log(item.asset_id)
            } else {

              if (item.name === asset.name) {
                validations.name = "Name already used";
                errors = true
              }
              if (item.lic_plate_no === asset.lic_plate_no) {
                validations.licPlate = "Licence plate already used";
                errors = true
              }
            }
          });
          if (errors) {
            callback({type: 100, code: 409, msg: validations})
          } else {
            callback(null, asset)
          }
        }
      }).catch(function (err) {
        callback(err)
      })
    },
    function (asset, callback) {
      //  update the asset
      models.asset.update(asset, {
        where: {
          asset_id: asset.asset_id
        }
      }).then(function (result) {
        callback(null, result)
      }).catch(function (err) {
        callback(err)
      })
    }
  ], function (err) {
    if (err) {
      if (err.type === 100) {
        res.status(err.code).send(err.msg)
      } else {
        res.status(500).send("Internal server error")
      }
    } else {
      res.json("Asset updated successfully")
      // res.json(result)
    }
  })
};

exports.addNewAsset = function (req, res) {

  const item = req.body;
  const companyId = req.headers.fk_company_id;
  if (!item.type) {
    res.json("Asset type not found")
  } else {
    async.waterfall([
      function (callback) {

        // Get asset type id

        models.asset_type.find({
          attributes: ['id', 'type'],
          where: {
            type: {
              $ilike: item.type
            },
          }
        }).then(function (result) {
          if (!result) {
            callback({
              code: 404,
              msg: "Asset type not found"
            })
          } else {
            callback(null, result)
          }
        }).catch(function (err) {
          callback(err)
        })
      },
      function (assetType, callback) {

        // Validate function : to check compulsory fields

        if (!assetType) {
          return callback({
            code: 400,
            msg: "Asset type not found"
          })
        }


        const newAsset = {
          "fk_c_id": companyId,
          "asset_id": hashids.encode(Date.now()),
          "fk_ast_type_id": assetType.id,
          "lic_plate_no": assetHelper.formatLicPlate(item.licPlate) || null,
          "mfg": assetHelper.formatWithStartCase(item.mfg) || null,
          "model": item.model || null,
          "yr_of_mfg": item.mfgYear || null,
          "veh_id_no": item.vin || null,
          "oth_details": item.notes || null,
          "name": assetHelper.formatWithStartCase(item.name) || null,
          "asset_id_ext": item.assetIdExt || null,
          "active": true,
          "spd_lmt": item.spdLimit || null,
          "load_lmt": item.loadLimit || null,
          "no_temp_zone": item.totTempZones || null,
          "is_refrig": item.isRefrig || false,
          "zone_lmts": item.tempZones || null,
          "eng_no": item.engNo || null,
          "has_eng": item.hasEng || false,
          "length": item.length || null,
          "width": item.width || null,
          "height": item.height || null,
          "tot_tyres": item.totTyres || null,
          "isbuiltintrailer": item.hasBuiltInTrailer || false,
          "p_name": assetHelper.formatWithStartCase(item.inChargeName) || null
        };

        const validator = {

          Truck: ["name", "lic_plate_no", "mfg", "yr_of_mfg", "model", "has_eng", "eng_no", "veh_id_no", "p_name"],
          Forklift: ["name", "mfg", "yr_of_mfg", "model", "veh_id_no", "p_name"],
          Trailer: ["name", "mfg", "yr_of_mfg", "model", "veh_id_no", "p_name"],
          Container: ["name", "load_lmt", "length", "width", "height", "p_name"]
        };

        const validateKeys = validator[assetType.type];
        const asset = _.omitBy(newAsset, _.isNull);
        const requiredKeys = _.reject(validateKeys, _.partial(_.has, asset));

        if(requiredKeys.length === 0){
          callback(null, asset)
        }else{
          callback({
            code: 400,
            msg: "Required fields are missing"
          })
        }

      },
      function (newAsset, callback) {

        // Validate function : to check if name, licence_no is unique
        models.asset.findAll({
          attributes: ['name', 'lic_plate_no'],
          where: {
            $or: [{
              lic_plate_no: newAsset.lic_plate_no
            }, {
              name: newAsset.name
            }]
          },
          raw: true
        }).then(function (result) {
          if (!result || result.length === 0) {
            callback(null, newAsset)
          }
          else {
            const validations = {
              name: null,
              licPlate: null
            };
            result.forEach(function (i) {
              if (i.name === newAsset.name) {
                validations.name = "Name already used"
              }
              if (i.lic_plate_no === newAsset.lic_plate_no) {
                validations.licPlate = "Licence plate already used"
              }
            });
            callback({
              code: 409,
              msg: validations
            })
          }
        }).catch(function (err) {
          callback(err)
        })
      }
      ,
      function (newAsset, callback) {

        // finally save the asset
        models.asset.create(newAsset).then(function (result) {
          callback(null, result)
        }).catch(function (err) {
          callback(err)
        })
      }
    ], function (err) {
      if (err) {
        if (err.code === 500) {
          res.status(500).send("Internal server error")
        } else {
          res.status(err.code).send(err.msg)
        }
      } else {
        res.status(201).send("Asset created successfully")
      }
    })
  }

};