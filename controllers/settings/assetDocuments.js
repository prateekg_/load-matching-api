const _ = require('lodash');
const models = require('../../db/models');


exports.createAssetDocumentRecord = function (req, res) {

  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  const record = JSON.parse(req.body.record);
  const asset = record.asset;

  record.fk_c_id = fk_c_id;
  record.fk_asset_id = asset.id;
  record.fk_u_id = fk_u_id;
  record.files_meta = req.files;

  models.asset_documents.update({current_record: false},{
    where: {
      fk_asset_id: record.fk_asset_id,
      type_code: record.type_code,
      current_record: true,
      active: true
    }
  }).then(function () {

    models.asset_documents.create(record).then(function () {
      res.json("sucessfully created")
    }).catch(function (err) {
      res.status(500).send(err)
    })

  }).catch(function (err) {
    res.status(500).send(err)
  })
};

exports.updateAssetDocumentRecord = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const record = JSON.parse(req.body.record);

  models.asset_documents.findOne({
    where: {
      id: record.id,
      fk_c_id: fk_c_id,
      current_record: true,
      active: true
    }
  }).then(function (document_record) {

    if(!document_record){
      res.status(404).send("not found")
    }else{

      document_record.document_no = record.document_no;
      document_record.issue_tis = record.issue_tis;
      document_record.expiry_tis = record.expiry_tis;

      // Remove files_meta to be deleted if any
      if(record.files_meta){
        document_record.files_meta = _.intersectionBy(document_record.files_meta, _.filter(record.files_meta, function (i) { return !i.deleted}) , 'key')
      }

      // Append files_meta to be added if any
      document_record.files_meta = _.concat(document_record.files_meta, req.files);

      document_record.save().then(function () {
        res.json("record updated")
      }).catch(function (err) {
        res.status(500).send(err)
      })
    }
  }).catch(function (err) {
    res.status(500).send(err)
  })
};

exports.deleteAssetDocumentRecord = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const record = req.body;

  if(!record.id){
    res.status(400).send("enter record id")
  }else {
    models.asset_documents.update({
      current_record: false,
      active: false
    }, {
      where: {
        id: record.id,
        fk_c_id: fk_c_id
      }
    }).spread(function (updated) {
      if(updated > 0){
        res.json("record deleted ")
      }else{
        res.status(404).send("record not found")
      }
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
};