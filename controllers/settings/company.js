const models = require('../../db/models');
const DEFAULT_RULES = {
  stoppage_config: {
    name: "Stoppage Configuration",
    rule: "stoppage_config",
    values: {
      stationary: {
        threshold: 0,
        enabled: true
      },
      stationary_idling: {
        threshold: 0,
        enabled: false
      }
    }
  }
};

exports.getCompanyDetails = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;

  const query = " SELECT " +
    " c.id, c.cname, ow_name, cont_no, email, o_web, linkedin, city, country, state, street, zip, expiry_tis, features" +
    " FROM companies c" +
    " LEFT JOIN company_details cd on c.id = cd.fk_c_id" +
    " LEFT JOIN company_plans_vw cp on cd.fk_c_id = cp.fk_c_id" +
    " WHERE c.id = :fk_c_id AND platform = 'app'";

  models.sequelize.query(query, {
    replacements: { fk_c_id: fk_c_id },
    type: models.sequelize.QueryTypes.SELECT
  })
    .spread(function (company) {
      if (company) {
        res.json(company)
      } else {
        res.json({})
      }
    }).catch(function (err) {
      console.log(err);
      res.status(500).send()
    })
};

exports.getCompanyRules = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const rule = req.query.rule;
  if (rule) {
    let query = "SELECT id, name, rule, values FROM company_business_rules cr WHERE cr.active AND cr.fk_c_id = " + fk_c_id + " AND cr.rule = '" + rule + "'";
    models.sequelize.query(query, {
      type: models.sequelize.QueryTypes.SELECT
    })
      .spread(function (result) {
        if (!result) {
          result = DEFAULT_RULES[rule]
        }
        res.json(result)
      }).catch(function () {
        res.status(500).send()
      });
  } else {
    let query = 'SELECT id, name, rule, values FROM company_business_rules cr WHERE cr.active AND cr.fk_c_id = ' + fk_c_id;
    models.sequelize.query(query, {
      type: models.sequelize.QueryTypes.SELECT
    })
      .then(function (result) {
        res.json({
          data: result
        })
      }).catch(function () {
        res.status(500).send()
      });
  }
};

exports.createOrUpdateRule = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const ruleObject = req.body;

  ruleObject.fk_c_id = fk_c_id;

  models.company_business_rules.update({
    values: ruleObject.values
  }, {
    where: {
      fk_c_id: fk_c_id,
      rule: ruleObject.rule
    }
  }).spread(function (updated) {
    if (updated) {
      res.json("Successfully updated");
    } else {
      models.company_business_rules.create(ruleObject)
        .then(function () {
          res.json("Successfully created");
        }).catch(function () {
          res.status(500).send()
        })
    }
  }).catch(function () {
    res.status(500).send()
  })
};

exports.getAPICredits = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;

  const query = `SELECT credit from company_api_credits WHERE fk_c_id = ${fk_c_id}`;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).spread(function (company) {
    if (company) {
      res.json(company)
    } else {
      res.json({})
    }
  }).catch(function (err) {
    console.log(err);
    res.status(500).send()
  })

}