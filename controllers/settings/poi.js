const async = require('async');
const _ = require('lodash');
const request = require('request');
const models = require('../../db/models');
const tagsHelper = require('../../helpers/tags');
const commonHelper = require('../../helpers/common');
const utils = require('../../helpers/util');
const nuRedis = require('../../helpers/nuRedis');
const geolib = require('geolib');
const moment = require('moment');

function getPoiDataFromDb(p, cb) {

  const pois = typeof p === "number" ? [p] : p;
  if (pois.length === 0) {
    cb(null, null)
  } else {
    const x = typeof pois === "string" ? pois : JSON.stringify(pois);
    const poi_ids = "(" + x.substring(1, x.length - 1) + ")";

    const query = "SELECT p.id, p.fk_c_id, p.name, pt.type, json_agg(json_build_object('id',pgm.fk_group_id)) as groups," +
      " CASE WHEN pt.fk_c_id IS NOT NULL THEN 'company' ELSE 'general' END as category FROM pois p " +
      " INNER JOIN poi_types pt ON pt.id = p.typ " +
      " INNER JOIN poi_group_mappings pgm ON pgm.fk_poi_id = p.id AND pgm.connected " +
      " WHERE p.priv_typ AND p.id in " + poi_ids +
      " GROUP BY p.id, pt.id";

    models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT }).then(function (data) {
      nuRedis.updatePoiKeys(data, cb)
    }).catch(function (err) {
      cb(err)
    })
  }
}

function getStateCode(lat, lon, cb) {
  let reqData;
  let short_name = '', long_name = '';

  if (lat && lon) {
    const url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lon + '&key=AIzaSyAccYJQ4YgjCaKpvIWwzbhdxfY04pniprM';
    request(url, function (error, response, body) {
      if (!error && response.statusCode === 200) {
        reqData = JSON.parse(body);

        if (reqData.status === 'OK') {
          _.isArray(reqData.results) && reqData.results.forEach(function (i) {
            _.isArray(i.address_components) && i.address_components.forEach(function (j) {
              if(_.includes(j.types, 'administrative_area_level_1')){
                short_name = j.short_name;
                long_name = j.long_name
              }
            })
          })
        }
      }
      return cb(null, [short_name, long_name])
    })
  }
  else{
    return cb(null, [short_name, long_name])
  }
}

exports.getAllList = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit || 10;
  const offset = (req.query.page || 0) * limit;
  const searchKeyword = req.query.q || '';
  const gid = req.query.gid;

  const query = "select d.id, d.name, d.addr as address, d.type as \"poiType\", count(*) OVER() AS total_count from (" +
    " select p.id, p.name, p.addr, '" + gid + "' = ANY (array_agg(g.gid)::text[]) as gid, pt.type from groups g" +
    " inner join poi_group_mappings pgm ON pgm.fk_group_id = g.id" +
    " inner join pois p on p.id = pgm.fk_poi_id and p.active" +
    " inner join poi_types pt on pt.id = p.typ" +
    " where g.active and g.fk_c_id = " + fk_c_id +
    " group by p.id, pt.id" +
    " ) d where  d.gid = false AND (d.name ilike '%" + searchKeyword + "%' or  d.addr ilike '%" + searchKeyword + "%')" +
    " LIMIT " + limit + " OFFSET " + offset;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .then(function (pois) {
      let totalPois = 0;
      pois.forEach(function (poi) {
        totalPois = Number(poi.total_count) || 0;
        delete poi.total_count
      });
      res.json({
        total_count: totalPois,
        data: pois
      })
    }).catch(function (err) {
      console.log(err);
      res.status(500).send()
    })
};

exports.details = function (req, res) {

  let poiId = Number(req.query.id);
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);

  if (!poiId) {
    return res.status(400).send("Invalid POI ID")
  }
  let detention_poi_id = null;

  async.waterfall([
    function(wcallback){
      const query = `select fk_primary_poi_id, fk_detention_poi_id
                        FROM poi_detention_mappings pdm 
                        WHERE fk_detention_poi_id =` + poiId + `OR fk_primary_poi_id = ` + poiId;
      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
        .spread(function (detPoi) {
          if(detPoi){
            const primary_poi_id = detPoi.fk_primary_poi_id;
            detention_poi_id = detPoi.fk_detention_poi_id;
            wcallback(null, primary_poi_id)
          }else{
            wcallback(null, poiId)
          }
        })
    }
  ], function(err, result){
    if(err){
      res.status(err.code).send(err.msg)
    }
    else{
      console.log(result);
      const primary_poi_id = result;
      async.parallel({
        poi: function(callback) {

          const query = "SELECT p.id, p.name, p.details, p.is24hours, p.addr as address, p.priv_typ as private, start_tim as \"openTime\"," +
            " end_tim as \"closeTime\", p.entry_fee as \"entryFee\", p.dwell_tim_limit as \"dwellTimeLimit\", p.geo_loc, p.company_fields as \"companyFields\"," +
            " json_build_object('id', pt.id, 'type',pt.type) as type , json_build_object('id', pc.id, 'name', pc.cont_name, 'email', pc.email, 'phone', pc.tel) as contact, json_agg(DISTINCT g.gid) as gids, " +
            " pt.company_fields" +
            " FROM pois p " +
            " INNER JOIN poi_types pt on pt.id = p.typ" +
            " LEFT JOIN poi_contacts pc on pc.fk_poi_id = p.id" +
            " INNER JOIN poi_group_mappings pgm ON pgm.fk_poi_id = p.id AND connected" +
            " INNER JOIN groups g ON g.id = pgm.fk_group_id AND g.active AND g.id in " + groupIds +
            " WHERE p.active and p.id = " + primary_poi_id +
            " GROUP BY p.id, pt.type, pc.id, pt.id";

          models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
            .spread(function (poiData) {
              if (!poiData) {
                callback({code: 404,
                  msg: 'POI Not Found'});
              } else {
                poiData.customFields = [];
                            
                _.each(poiData.companyFields, function (value, key) {
                  poiData.customFields.push({
                    key: key,
                    value: value,
                    label: poiData.company_fields? poiData.company_fields[key] : null
                  })
                });

                delete poiData.companyFields;

                const geofence = [];
                _.each(poiData.geo_loc.coordinates[0], function (poi) {
                  geofence.push({
                    lat: poi[1],
                    lon: poi[0]
                  })
                });
                poiData.geofence = geofence;
                poiData.tags = [];

                delete poiData.geo_loc;
                callback(null,poiData);
              }

            }).catch(function (err) {
              console.log(err);
              callback({code: 500, msg: "Internal Server Error"});
            })
        },
        detention: function(callback) {

          const query = "SELECT p.id, p.name, p.details, p.is24hours, p.addr as address, p.priv_typ as private, start_tim as \"openTime\"," +
            "   end_tim as \"closeTime\", p.entry_fee as \"entryFee\", p.dwell_tim_limit as \"dwellTimeLimit\", p.geo_loc, p.company_fields as \"companyFields\"," +
            "   json_build_object('id', pt.id, 'type',pt.type) as type , json_build_object('id', pc.id, 'name', pc.cont_name, 'email', pc.email, 'phone', pc.tel) as contact, json_agg(DISTINCT g.gid) as gids, " +
            "   pt.company_fields, array_agg(detention_config) as detention_config" +
            " FROM pois p " +
            " INNER JOIN poi_types pt on pt.id = p.typ" +
            " LEFT JOIN poi_contacts pc on pc.fk_poi_id = p.id" +
            " INNER JOIN poi_group_mappings pgm ON pgm.fk_poi_id = p.id AND connected" +
            " INNER JOIN groups g ON g.id = pgm.fk_group_id AND g.active AND g.id in " + groupIds +
            ` LEFT JOIN poi_detention_mappings pdm on pdm.fk_detention_poi_id = ` + detention_poi_id +
            " WHERE p.active and p.id = " + detention_poi_id +
            " GROUP BY p.id, pt.type, pc.id, pt.id";

          models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
            .spread(function (poiData) {
              if (!poiData) {
                callback(null, null)
              } else {
                poiData.customFields = [];
                poiData.detention_config = poiData.detention_config[0];
                _.each(poiData.companyFields, function (value, key) {
                  poiData.customFields.push({
                    key: key,
                    value: value,
                    label: poiData.company_fields[key]
                  })
                });

                delete poiData.companyFields;

                const geofence = [];
                _.each(poiData.geo_loc.coordinates[0], function (poi) {
                  geofence.push({
                    lat: poi[1],
                    lon: poi[0]
                  })
                });
                poiData.geofence = geofence;
                poiData.tags = [];

                delete poiData.geo_loc;
                callback(null,poiData);
              }                        
            }).catch(function (err) {
              console.log(err);
              callback({code: 500, msg: "Internal Server Error"});
            })
        }
      }, function(err, results) {
        if(err){
          res.status(err.code).send(err.msg)
        }else{
          results.poi.detention = results.detention;
          delete results.detention;
          res.json(results.poi)
        }
      });
    }
  })            
};

exports.update = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const p = req.body;
  const pc = req.body.contact || {};

  const poi = {
    id: p.id,
    name: commonHelper.formatWithStartCase(p.name),
    details: p.details,
    addr: commonHelper.formatWithStartCase(p.address),
    priv_typ: p.private,
    start_tim: p.openTime,
    end_tim: p.closeTime,
    entry_fee: p.entryFee,
    dwell_tim_limit: p.dwellTimeLimit,
    geo_loc: {
      type: 'Polygon',
      coordinates: [[]]
    },
    tags: p.tags,
    is24hours: p.is24hours || false,
    company_fields: p.companyFields
  };

  const poi_contact = {
    id: pc.id,
    cont_name: pc.name ? commonHelper.formatWithStartCase(pc.name) : null,
    email: pc.email,
    tel: pc.phone
  };

  const lastLat = p.geofence[p.geofence.length - 1].lat;
  const lastLon = p.geofence[p.geofence.length - 1].lon;

  if (p.geofence[0].lat === lastLat && p.geofence[0].lon === lastLon) {
    // do nothing
  } else {
    // push 1st element as last
    p.geofence.push({ lat: p.geofence[0].lat, lon: p.geofence[0].lon })
  }

  const tmpCoordinates = [];
  _.each(p.geofence, function (item) {
    poi.geo_loc.coordinates[0].push([item.lon, item.lat]);
    tmpCoordinates.push({ latitude: item.lat, longitude: item.lon })
  });

  const center = geolib.getCenter(tmpCoordinates);

  if (center.latitude && center.longitude) {
    poi.lat = center.latitude;
    poi.lon = center.longitude
  }

  async.waterfall([

    function (callback) {
      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
        .spread(function (result) {
          if (result && result.is_admin) {
            callback(null, result)
          } else {
            callback({
              code: 403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          callback({
            code: 500,
            msg: err
          })
        })
    },
    function (result, callback) {

      models.poi.find({
        attributes: ['id'],
        where: {
          fk_c_id: fk_c_id,
          id: poi.id
        }
      }).then(function (r) {

        if (r === null) {
          callback({ code: 404, msg: "Invalid POI ID" })
        } else {
          callback(null, r)
        }

      }).catch(function (err) {
        callback(err)
      })
    },

    function (r, callback) {

      models.poi_type.find({
        attributes: ['id'],
        where: {
          id: p.type,
          $or: [{
            fk_c_id: null
          }, {
            fk_c_id: fk_c_id
          }]
        }
      }).then(function (result) {
        if (!result) {
          return callback({
            code: 404,
            msg: 'Invalid POI type id'
          })
        }
        callback(null, result.id)
      }).catch(function (err) {
        callback(err)
      })
    },
    function (poiTypeId, callback) {

      poi.typ = poiTypeId;
      getStateCode(poi.lat, poi.lon, function (err, stateVal) {
        if (stateVal) {
          poi.state_code = stateVal[0];
          poi.state = stateVal[1]
        }
        models.poi.update(poi, {
          where: {
            id: poi.id
          }
        }).then(function (result) {
          callback(null, result)
        }).catch(function (err) {
          callback(err)
        })
      })

    },
    function (r, callback) {

      models.poi_contact.update(poi_contact, {
        where: {
          id: poi_contact.id
        }
      }).then(function (result2) {
        callback(null, result2)
      }).catch(function (err) {
        callback(err)
      })
    }, 
    function (r, callback) {
      // update or create tags
      if (poi.tags) {
        tagsHelper.processTags(poi.tags, poi.id, fk_c_id, tagsHelper.tagType.pois, function (err) {
          if (err) {
            callback(err)
          } else {
            callback(null, '')
          }
        })
      } else {
        callback(null, '')
      }
    },
    function (r, cb) {

      const poiGroups = p.gids || [];
      const gids = [];
      const added_updated = [];

      if (poiGroups.length > 0) {
        poiGroups.forEach(function (gid) {
          gids.push("'" + gid + "'");
        });

        let query = "SELECT g.id FROM groups g WHERE gid IN ( " + gids + " ) AND fk_c_id = " + fk_c_id;
        models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
          .then(function (groupIds) {

            if (groupIds.length > 0) {
              groupIds.forEach(function (g) {
                added_updated.push("'" + g.id + "'");

                models.poi_group_mapping.find({
                  attributes: ['id'],
                  where: {
                    fk_group_id: g.id,
                    fk_poi_id: p.id

                  }
                }).then(function (result) {
                  if (!result) {
                    //insert new
                    const newMapping = {
                      fk_poi_id: p.id,
                      connected: true,
                      fk_group_id: g.id,
                      tis: parseInt(Date.now() / 1000)
                    };
                    models.poi_group_mapping.create(newMapping).catch(function (err) {
                      cb(err)
                    })
                  } else {
                    //update the existing

                    models.poi_group_mapping.update({
                      connected: true,
                    }, {
                      where: {
                        id: result.id
                      }

                    }).then(function () {

                    }).catch(function (err) {
                      cb(err)
                    })
                  }

                }).catch(function (err) {
                  cb(err)
                })

              });

              query = "UPDATE poi_group_mappings SET connected = FALSE WHERE fk_group_id NOT IN ( " + added_updated + " ) AND fk_poi_id = " + p.id;

              models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
                .then(function () {
                  getPoiDataFromDb(p.id, function () {
                    cb()
                  })
                })

            } else {
              cb()
            }

          }).catch(function (err) {
            cb({
              code: 500,
              msg: err
            })
          })

      } else {
        cb()
      }
    }
  ], function (err) {
    if (err) {
      if (err.code) {
        res.status(err.code).send(err.msg)
      } else {
        res.status(500).send("Internal server error")
      }
    } else {
      res.json("Poi updated successfully")
    }
  })
};

exports.addNewPoi = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const p = req.body;
  const poiValidator = ['name', 'type', 'address'];

  if (_.every(poiValidator, _.partial(_.has, p))) {

    const poi = {
      fk_c_id: fk_c_id,
      typ: null,
      name: commonHelper.formatWithStartCase(p.name),
      details: p.details,
      addr: commonHelper.formatWithStartCase(p.address),
      priv_typ: true,
      start_tim: p.openTime,
      end_tim: p.closeTime,
      entry_fee: p.entryFee,
      dwell_tim_limit: p.dwellTimeLimit,
      geo_loc: {
        type: 'Polygon',
        coordinates: [[]]
      },
      tags: p.tags,
      is24hours: p.is24hours || false,
      company_fields: p.companyFields
    };

    let poi_contact = {
      cont_name: p.contact ? commonHelper.formatWithStartCase(p.contact.name) : null,
      email: p.contact ? p.contact.email : null,
      tel: p.contact ? p.contact.phone : null,
      fk_poi_id: null
    };

    poi_contact = _.omitBy(poi_contact, _.isNil);


    const lastLat = p.geofence[p.geofence.length - 1].lat;
    const lastLon = p.geofence[p.geofence.length - 1].lon;

    if (p.geofence[0].lat === lastLat && p.geofence[0].lon === lastLon) {
      // do nothing
    } else {
      // push 1st element as last
      p.geofence.push({ lat: p.geofence[0].lat, lon: p.geofence[0].lon })
    }

    const poiGroups = p.gids || [];

    const tmpCoordinates = [];
    _.each(p.geofence, function (item) {
      poi.geo_loc.coordinates[0].push([item.lon, item.lat]);
      tmpCoordinates.push({ latitude: item.lat, longitude: item.lon })
    });

    const center = geolib.getCenter(tmpCoordinates);

    if (center.latitude && center.longitude) {
      poi.lat = center.latitude;
      poi.lon = center.longitude
    }

    async.waterfall([

      //Authenticate user
      function (cb) {
        const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
          " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
          " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

        models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
          .spread(function (result) {
            if (result && result.is_admin) {
              cb(null, result)
            } else {
              cb({
                code: 403,
                msg: 'User does not have permissions'
              })
            }
          }).catch(function (err) {
            cb({
              code: 500,
              msg: err
            })
          })
      },
      function (result1, callback) {

        // Find poi type id

        models.poi_type.find({
          attributes: ['id'],
          where: {
            id: p.type,
            fk_c_id: fk_c_id
          }
        }).then(function (result) {
          if (!result) {
            return callback({
              code: 404,
              msg: 'Invalid POI type id'
            })
          }
          callback(null, result.id)
        }).catch(function (err) {
          callback(err)
        })
      },
      function (poiTypeId, callback) {

        poi.typ = poiTypeId;
        getStateCode(poi.lat, poi.lon, function (err, stateVal) {
          if (stateVal) {
            poi.state_code = stateVal[0];
            poi.state = stateVal[1]
          }
          models.poi.create(poi).then(function (result) {
            callback(null, result)
          }).catch(function (err) {
            callback(err)
          })
        })

      },
      function (r, callback) {

        poi_contact.fk_poi_id = r.id;
 
        async.parallel([
          function (cb) {
            models.poi_contact.create(poi_contact).then(function () {
              cb()
            }).catch(function (err) {
              cb(err)
            })
          },
          function (cb) {
            let query = " SELECT g.id FROM groups g WHERE is_admin AND g.fk_c_id = " + fk_c_id;
            const newMapping = [];

            models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
              .spread(function (result) {
                const adminMap = {
                  fk_poi_id: r.id,
                  connected: true,
                  fk_group_id: result.id,
                  tis: parseInt(Date.now() / 1000)
                };
                newMapping.push(adminMap);

                const gids = [];

                if(poiGroups.length > 0) {
                  poiGroups.forEach(function (gid) {
                    gids.push("'" + gid + "'");
                  });

                  query = "SELECT g.id FROM groups g WHERE gid IN ( " + gids +" ) AND NOT is_admin AND fk_c_id = " + fk_c_id;
                  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
                    .then(function (groupIds) {

                      if (groupIds.length > 0) {
                        groupIds.forEach(function (g) {
                          newMapping.push({
                            fk_poi_id: r.id,
                            connected: true,
                            fk_group_id: g.id,
                            tis: parseInt(Date.now() / 1000)
                          })
                        })
                      }

                      models.poi_group_mapping.bulkCreate(newMapping).then(function () {
                        getPoiDataFromDb(r.id, function () {
                          cb()
                        })
                      }).catch(function (err) {
                        cb({
                          code: 500,
                          msg: err
                        })
                      })
                                        
                    }).catch(function (err) {
                      cb({
                        code: 500,
                        msg: err
                      })
                    })
                } else {
                  models.poi_group_mapping.bulkCreate(newMapping).then(function () {
                    getPoiDataFromDb(r.id, function () {
                      cb()
                    })
                  }).catch(function (err) {
                    cb({
                      code: 500,
                      msg: err
                    })
                  })
                }

                                
              }).catch(function (err) {
                cb(err)
              })
          }
        ], function (err) {
          callback(err, null)
        })
      }
    ], function (err) {
      if (err) {
        if (err.code) {
          res.status(err.code).send(err.msg)
        } else {
          res.status(500).send("Internal server error")
        }
      } else {
        res.json("Poi Created successfully")
      }
    })
  } else {
    return res.status(400).send("Fields missing")
  }

};

/**
 * This api is used to creatention detention poi for primary poi.
 * poi_detention_mappings table will have mapping detention of poi and detention
 * @param {*} req 
 * @param {*} res 
 */
exports.addDetentionPoi = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const p = req.body;
  let poiID;

  const poiValidator = ['name', 'address', 'fk_poi_id', 'detention_config'];

  if (_.every(poiValidator, _.partial(_.has, p))) {

    const detentionConfigKeys = ['detention_mins'];
    let dataValid = utils.validateMinPayload(detentionConfigKeys, p.detention_config);
    if(!dataValid){
      return res.status(400).send("Mandatory fields : "+detentionConfigKeys)
    }
    const poi = {
      fk_c_id: fk_c_id,
      typ: null,
      name: commonHelper.formatWithStartCase(p.name),
      details: p.details,
      addr: commonHelper.formatWithStartCase(p.address),
      priv_typ: true,
      start_tim: p.openTime,
      end_tim: p.closeTime,
      entry_fee: p.entryFee,
      dwell_tim_limit: p.dwellTimeLimit,
      geo_loc: {
        type: 'Polygon',
        coordinates: [[]]
      },
      tags: p.tags,
      is24hours: p.is24hours || false,
      company_fields: p.companyFields
    };

    let poi_contact = {
      cont_name: p.contact ? commonHelper.formatWithStartCase(p.contact.name) : null,
      email: p.contact ? p.contact.email : null,
      tel: p.contact ? p.contact.phone : null,
      fk_poi_id: null
    };

    poi_contact = _.omitBy(poi_contact, _.isNil);


    const lastLat = p.geofence[p.geofence.length - 1].lat;
    const lastLon = p.geofence[p.geofence.length - 1].lon;

    if (p.geofence[0].lat === lastLat && p.geofence[0].lon === lastLon) {
      // do nothing
    } else {
      // push 1st element as last
      p.geofence.push({ lat: p.geofence[0].lat, lon: p.geofence[0].lon })
    }

    const tmpCoordinates = [];
    _.each(p.geofence, function (item) {
      poi.geo_loc.coordinates[0].push([item.lon, item.lat]);
      tmpCoordinates.push({ latitude: item.lat, longitude: item.lon })
    });

    const center = geolib.getCenter(tmpCoordinates);

    if (center.latitude && center.longitude) {
      poi.lat = center.latitude;
      poi.lon = center.longitude
    }

    async.waterfall([

      //Authenticate user
      function (cb) {
        const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
          " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
          " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

        models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
          .spread(function (result) {
            if (result && result.is_admin) {
              cb(null)
            } else {
              cb({
                code: 403,
                msg: 'User does not have permissions'
              })
            }
          }).catch(function (err) {
            console.log(err);
            cb({
              code: 500,
              msg: err
            })
          })
      },
      function (cb) {
        const query = " SELECT * FROM poi_detention_mappings pdm " +
          " WHERE pdm.fk_primary_poi_id = " + p.fk_poi_id;

        models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
          .spread(function (result) {
            if (result ) {
              cb({
                code: 403,
                msg: 'This poi already has a detention'
              })
            } else {
              cb(null)
            }
          }).catch(function (err) {
            console.log(err);
            cb({
              code: 500,
              msg: err
            })
          })
      },
      function (callback) {

        // Find poi type id

        models.poi_type.find({
          attributes: ['id'],
          where: {
            type: "DetentionPOI"
          }
        }).then(function (result) {
          if (!result) {
            return callback({
              code: 404,
              msg: 'Invalid POI type id'
            })
          }
          callback(null, result.id)
        }).catch(function (err) {
          console.log(err);
          callback(err)
        })
      },
      function (poiTypeId, callback) {

        poi.typ = poiTypeId;
        getStateCode(poi.lat, poi.lon, function (err, stateVal) {
          if (stateVal) {
            poi.state_code = stateVal[0];
            poi.state = stateVal[1]
          }
          models.poi.create(poi).then(function (result) {
            callback(null, result)
          }).catch(function (err) {
            console.log(err);
            callback(err)
          })
        })

      },
      function (r, callback) {

        poi_contact.fk_poi_id = r.id;
        poiID = r.id;

        async.parallel([
          function (cb) {
            models.poi_contact.create(poi_contact).then(function () {
              cb()
            }).catch(function (err) {
              console.log(err);
              cb(err)
            })
          },
          function (cb) {
            const newMapping = [];
            const query = "SELECT fk_group_id FROM poi_group_mappings pgm WHERE fk_poi_id = " + p.fk_poi_id;
            models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
              .then(function (groupIds) {

                if (groupIds.length > 0) {
                  async.each(groupIds, function(g, wcb) {
                    newMapping.push({
                      fk_poi_id: r.id,
                      connected: true,
                      fk_group_id: g.fk_group_id,
                      tis: parseInt(Date.now() / 1000)
                    });
                    wcb()
                  }, function(){
                                                
                    models.poi_group_mapping.bulkCreate(newMapping).then(function () {
                      getPoiDataFromDb(r.id, function () {
                        cb()
                      })
                    }).catch(function (err) {
                      console.log(err);
                      cb({
                        code: 500,
                        msg: err
                      })
                    })
                  })
                } else {
                  console.log("GRoup error");
                  cb()
                }

              }).catch(function (err) {
                cb({
                  code: 500,
                  msg: err
                })
              })
          }
        ], function (err) {
          callback(err, null)
        })
      },
      function (r, callback) {
        models.poi_detention_mapping.create({
          fk_primary_poi_id: p.fk_poi_id,
          fk_detention_poi_id: poiID,
          fk_c_id: fk_c_id,
          detention_config: p.detention_config,
          mapped: true
        }).then(function (result) {
          callback(null, result)
        }).catch(function (err) {
          console.log(err);
          callback(err)
        })
                
      }
    ], function (err) {
      if (err) {
        if (err.code) {
          res.status(err.code).send(err.msg)
        } else {
          res.status(500).send("Internal server error")
        }
      } else {
        res.json("Poi Created successfully")
      }
    })
  } else {
    return res.status(400).send("Mandatory fields : "+poiValidator)
  }

};

/**
 * Api to update detention settings like detention time
 * As you update it saves the log in detention_settings_log table
 * @param {*} req 
 * @param {*} res 
 */
exports.updateDetentionSettings = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const data = req.body;
  const p = data;

  const requiredKeys = ['id', 'name', 'address', 'geofence'];
  let dataValid = utils.validateMinPayload(requiredKeys, data);
  if(!dataValid){
    return res.status(422).send("Mandatory fields : "+requiredKeys)
  }

  const detentionConfigKeys = ['detention_mins'];
  if(!dataValid){
    return res.status(400).send("Mandatory fields : "+detentionConfigKeys)
  }

  const detention_config = p.detention_config;

  const poi = {
    id: p.id,
    name: commonHelper.formatWithStartCase(p.name),
    addr: commonHelper.formatWithStartCase(p.address),
    geo_loc: {
      type: 'Polygon',
      coordinates: [[]]
    }
  };

  const lastLat = p.geofence[p.geofence.length - 1].lat;
  const lastLon = p.geofence[p.geofence.length - 1].lon;

  if (p.geofence[0].lat === lastLat && p.geofence[0].lon === lastLon) {
    // do nothing
  } else {
    // push 1st element as last
    p.geofence.push({ lat: p.geofence[0].lat, lon: p.geofence[0].lon })
  }

  const tmpCoordinates = [];
  _.each(p.geofence, function (item) {
    poi.geo_loc.coordinates[0].push([item.lon, item.lat]);
    tmpCoordinates.push({ latitude: item.lat, longitude: item.lon })
  });

  const center = geolib.getCenter(tmpCoordinates);

  if (center.latitude && center.longitude) {
    poi.lat = center.latitude;
    poi.lon = center.longitude
  }

  async.waterfall([
    function (callback) {
      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
        .spread(function (result) {
          if (result && result.is_admin) {
            callback(null, result)
          } else {
            callback({
              code: 403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          callback({
            code: 500,
            msg: err
          })
        })
    },
    function (result, callback) {

      models.poi.find({
        attributes: ['id'],
        where: {
          fk_c_id: fk_c_id,
          id: poi.id
        }
      }).then(function (r) {

        if (r === null) {
          callback({ code: 404, msg: "Invalid POI ID" })
        } else {
          callback(null)
        }

      }).catch(function (err) {
        callback(err)
      })
    },

    function (callback) {

      // Find poi type id

      models.poi_type.find({
        attributes: ['id'],
        where: {
          type: "DetentionPOI"
        }
      }).then(function (result) {
        if (!result) {
          return callback({
            code: 404,
            msg: 'Invalid POI type id'
          })
        }
        callback(null, result.id)
      }).catch(function (err) {
        console.log(err);
        callback(err)
      })
    },
    function (poiTypeId, callback) {

      poi.typ = poiTypeId;
      getStateCode(poi.lat, poi.lon, function (err, stateVal) {
        if (stateVal) {
          poi.state_code = stateVal[0];
          poi.state = stateVal[1]
        }
        models.poi.update(poi, {
          where: {
            id: poi.id
          }
        }).then(function () {
          callback(null)
        }).catch(function (err) {
          callback(err)
        })
      })

    },
    function(callback){
      const query = `select detention_config
                        FROM poi_detention_mappings pdm 
                        WHERE fk_detention_poi_id =` + p.id;
      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
        .spread(function (detPoi) {
          if(detPoi && detPoi.detention_config){   
                                
            if((detPoi.detention_config.detention_mins === p.detention_mins) && (detPoi.detention_config.kms === p.kms))
              return callback(null); //Come out of waterfall since detention config is not changed
            else {
              callback(null) 
            }
          }else{
                                
            callback({code: 403, msg: "Invalid detention poi id"})
          }
        })
    },
    function(callback){

      const update_query = " update poi_detention_mappings " +
        " set detention_config = ('" + JSON.stringify(detention_config) + "')::json " +
        " where fk_detention_poi_id = " + p.id +
        " returning id, detention_config, (" +
        " select detention_config from poi_detention_mappings where fk_detention_poi_id = " + p.id +
        " ) as old_settings;";
      models.sequelize.query(update_query, {type: models.sequelize.QueryTypes.SELECT}).then(function (updated) {
                
        if(updated[0] && updated[0].detention_config){
          callback(null,updated[0].detention_config, updated[0].old_settings)                            
        }
        else callback({code:400, message:"Unable to save detention settings"})                
      })
    },
    function(new_settings, old_settings, callback){
            
      models.detention_settings_log.create({
        fk_u_id: fk_u_id,
        fk_poi_id: p.id,
        new_settings: new_settings,
        old_settings: old_settings,
        tis: moment().unix()
      }).then(function(){
        callback(null)
      }).catch(function(){
        callback({code: 500, msg:"Internal server error"})
      })
    }
  ], function(err){
    if(err) res.status(err.code).send(err.msg);
    else res.send("Updated")
  })


};

exports.getPoiTypes = function (req, res) {
  // get only company poi types

  const fk_c_id = req.headers.fk_c_id;
  const keyword = req.query.q || '';
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = limit * page;

  const query = "SELECT pt.id, pt.type as name, 'company' as type, count(p.id)::int as \"totalPlaces\", pt.company_fields::jsonb, COUNT(*) OVER()::INT AS count" +
    " FROM poi_types pt" +
    " LEFT JOIN pois p on p.typ = pt.id AND p.active AND p.fk_c_id = " + fk_c_id +
    " WHERE pt.fk_c_id = " + fk_c_id + " AND pt.type ilike '%" + keyword + "%'" +
    " GROUP BY pt.id OFFSET " + offset + " LIMIT " + limit;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .then(function (types) {
      let tot_rows = 0;
      types.forEach(function (i) {
        i.companyFieldsCount = _.size(i.company_fields);
        i.companyFields = _.map(i.company_fields, function (val) { return val });
        tot_rows = i.count;

        delete i.company_fields;
        delete i.count
      });

      res.json({
        count: tot_rows,
        data: types
      })
    }).catch(function () {
      res.status(500).send('Internal error');
    })
};

exports.getPoiType = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const id = req.query.id;

  const query = "SELECT pt.id, pt.type as name, pt.company_fields as fields" +
    " FROM poi_types pt" +
    " WHERE pt.fk_c_id = " + fk_c_id + " AND pt.id = " + id;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (type) {
      res.json(type)
    }).catch(function () {
      res.status(500).send('Internal error');
    })
};

exports.createPoiType = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  let data = req.body;

  if (!data || !data.name || data.name === "") {
    return res.status(400).send('Invalid POI type')
  }

  models.poi_type.count({
    where: {
      fk_c_id: fk_c_id
    }
  }).then(function () {
    models.poi_type.findOrCreate({
      where: {
        type: _.startCase(_.toLower(data.name)),
        fk_c_id: fk_c_id
      },
      defaults: {
        type: _.startCase(_.toLower(data.name)),
        fk_c_id: fk_c_id,
        company_fields: data.fields
      }
    }).spread(function (user, created) {
      if (created) {
        res.status(201).send('Poi type created successfully')
      } else {
        res.status(400).send('Poi type already exists')
      }

    }).catch(function () {
      res.status(500).send('Internal server error')
    })
  }).catch(function () {
    res.status(500).send('Internal server error')
  })


};

exports.updatePoiType = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  let data = req.body;

  if (!data || !data.name || data.name === "" || !data.id) {
    return res.status(400).send('Invalid data')
  }

  models.poi_type.update({
    type: _.startCase(_.toLower(data.name)),
    company_fields: data.fields
  }, {
    where: {
      fk_c_id: fk_c_id,
      id: data.id
    }
  }).spread(function (count) {
    if (count > 0) {
      return res.status(200).send('Poi updated successfully')
    }
    return res.status(404).send('Invalid POI type')
  }).catch(function () {
    res.status(500).send('Internal server error')
  })
};

exports.disablePoi = function (req, res) {

  const item = req.body;
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  let poiId = item.id || null;

  if (!poiId) {
    res.status(422).send("Invalid poi Id")
  } else {

    const poi = {id: poiId, active: false};

    const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
      " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
      " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

    models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
      .spread(function (result) {
        if (result && result.is_admin) {
          models.poi.update(poi, {
            where: {
              id: poiId,
              fk_c_id: fk_c_id
            }
          }).spread(function (result) {
            if (result >= 1) {
              res.json("Sucessfully deleted")
            } else {
              res.status(404).send("Not found")
            }
          }).catch(function () {
            res.status(500).send("Error deleting")
          })
        } else {
          res.status(403).send('User does not have permissions')
        }
      }).catch(function () {
        res.status(500).send("Error deleting")
      })


  }
};