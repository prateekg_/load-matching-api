const models = require('../../db/models/index');
const defaultSettings = require('../../helpers/defaultSettings');
const utils = require('../../helpers/util');
const async = require('async');
const moment = require('moment-timezone');
const _ = require('lodash');

//Update asset details
exports.updateAssetSensorSettings = function(req, res){
  const temperature_sensor = req.body.temperature_sensor;
  const asset_id = req.body.asset_id;
  const user_id = req.headers.fk_u_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);

  const required_keys = ['asset_id', 'temperature_sensor'];
  const temperature_required_keys = ['min_temperature', 'max_temperature', 'continuous_notification', 'instant_notification'];

  let dataValid = utils.validateMinPayload(required_keys, req.body);
  let temperatureDataValid = utils.validateMinPayload(temperature_required_keys, temperature_sensor);
  if(!dataValid || !temperatureDataValid){
    return res.status(400).send("Mandatory fields : "+required_keys + ", " +temperature_required_keys)
  }

  let old_temperatures = "";
  const query = "SELECT sensor_settings FROM groups g " +
    " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected " +
    " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
    " LEFT JOIN asset_attributes aa on a.id = aa.fk_asset_id" +
    " WHERE g.active AND g.id in " + groupIds + " AND a.id = " + asset_id + " LIMIT 1";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (assets) {
    if(assets && assets.length > 0){
      let sensor_settings = defaultSettings.sensor;
      old_temperatures = assets[0].sensor_settings;     
      if(assets[0].sensor_settings && assets[0].sensor_settings.temperature_sensor){
        sensor_settings = assets[0].sensor_settings
      }
      sensor_settings.temperature_sensor.min_temperature = temperature_sensor.min_temperature;
      sensor_settings.temperature_sensor.max_temperature = temperature_sensor.max_temperature;
      sensor_settings.temperature_sensor.continuous_notification = temperature_sensor.continuous_notification;
      sensor_settings.temperature_sensor.instant_notification = temperature_sensor.instant_notification;
            
      async.waterfall([
        function(callback){
          if(!old_temperatures){
            models.asset_attribute.findOrCreate({
              where: {fk_asset_id: asset_id}, 
              defaults: {fk_asset_id: asset_id}}).then(function () {
              callback(null)
            }).catch(function (err) {
              callback({code:500, message:err})
            })
          }else{
            callback(null)
          }
        },
        function(callback){
          const update_query = " update asset_attributes " +
            " set sensor_settings = ('" + JSON.stringify(sensor_settings) + "')::json " +
            " where fk_asset_id = " + asset_id +
            " returning id, sensor_settings, (" +
            " select sensor_settings from asset_attributes where fk_asset_id = " + asset_id +
            " ) as old_sensor_settings;";
          models.sequelize.query(update_query, {type: models.sequelize.QueryTypes.SELECT}).then(function (updated) {

            // Check that there is atleast one entry in the result and it is an object.
            if( updated.length > 0 && _.isObject(updated[0]) ){
              callback(null,updated[0].sensor_settings, updated[0].old_sensor_settings)
            }
            // if the fk_asset_id does not exist, nothing will get updated and it will come here. Also if updated[0] is not an object.
            else callback({code:400, message:"Unable to save sensor settings"})
                        
          })
        },
        function(new_values, old_values, callback){
          models.temperature_sensor_log.create({
            fk_u_id: user_id,
            fk_asset_id: asset_id,
            new_temperature_values: new_values.temperature_sensor,
            old_temperature_values: old_values.temperature_sensor,
            tis: moment().unix()
          }).then(function () {
            callback(null)
          }).catch(function (err) {
            callback({code:500, message:err})
          })
        }
      ], function(err){
        if(err){
          res.status(err.code).send(err.message)
        }
        else res.send("Sensor settings updated succesfully")
      })
            
    }else{
      res.status(400).send("Asset does not belong to the company")
    }
  });
};

exports.getTemperatureLogs = function(req, res){
  const asset_id = req.query.asset_id;
  let sort = req.query.sort;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;

  const keys = ['asset_id'];
  let validData = utils.validateMinPayload(keys, req.query);

  if(!validData){
    return res.status(400).send('Mandatory fields are : '+keys);
  }

  const searchCondition = searchKeyword.length > 2 ? " AND u.name ilike '%" + searchKeyword + "%' " : "";

  let sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'u.name',
      client: 'name'
    }, {
      db: "tsl.new_temperature_values->>'max_temperature'",
      client: 'max_temperature'
    }, {
      db: "tsl.new_temperature_values->>'min_temperature'",
      client: 'min_temperature'
    }, {
      db: 'tsl.tis',
      client: 'tis'
    }],
    " ORDER BY tsl.tis DESC"
  )

  const query = "select tsl.tis, tsl.new_temperature_values, u.name, count(*) OVER() AS total_count " +
    " from temperature_sensor_logs tsl" +
    " inner join users u on tsl.fk_u_id = u.id" +
    " where fk_asset_id = " + asset_id + searchCondition + sort_condition +
    " LIMIT " + limit + " OFFSET " + offset;
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (log) {
    let total_count = 0;
    async.each(log, function (log, callback) {
      total_count = log.total_count;
      delete log.total_count;
      callback()
    }, function(){
      res.json({
        data: log, 
        total_count: parseInt(total_count)
      })
    })        
  });
};