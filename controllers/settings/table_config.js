const models = require('../../db/models');
const async = require('async');
const _ = require('lodash');

/**
 * Takes default table and user config and updates if new changes are there in default config
 * @param data
 * @param callback
 * @returns {*}
 */
function processTableConfigValues(data, callback) {
  try {
    _.forEach(data.default_config.tabs, function (value, key) {
      if (data.config && data.config.tabs) {
        if (!data.config.tabs[key]) {
          // tab not found in user config. add the new tab
          data.config.tabs[key] = value;
        } else {
          // check objects in tabs
          if (Array.isArray(value)) {
            value.forEach(function (i) {
              const column = _.find(data.config.tabs[key], {key: i.key});
              if(column){
                // Update default column selected with user config
                i.selected = column.selected
                i.order = column.order
              }
            })
          }
        }
      } else {
        data.config = data.default_config
      }
    });
    return callback(null, data.default_config)
  }catch(e){
    console.log("Invalid config ", e);
    return callback(null, data.default_config)
  }
}

exports.getTableConfiguration = function (req, res) {

  const fk_u_id = req.headers.fk_u_id;
  let table_name = req.query.table_name;

  if(!table_name){
    return res.status(400).send("Enter table name")
  }

  const query = "SELECT wtt.default_config, wuc.config " +
    " FROM wv_table_types wtt " +
    " LEFT JOIN wv_user_configs wuc on wuc.fk_wv_table_type_id = wtt.id and wuc.fk_user_id = $fk_u_id " +
    " WHERE wtt.table_name = $table_name;";

  const query_params = {fk_u_id: fk_u_id, table_name: table_name};
  models.sequelize.query(query, {
    bind: query_params,
    type: models.sequelize.QueryTypes.SELECT
  }).spread(function (data) {
    if(!data){
      res.status(400).send("Invalid table name")
    }else{
      processTableConfigValues(data, function (err, config) {
        res.json({config: config})
      })
    }
  }).catch(function () {
    res.status(500).send()
  })
};

exports.updateTableConfiguration = function (req, res) {
  const fk_u_id = req.headers.fk_u_id;
  const data = req.body;

  if(!data.table_name){
    return res.status(400).send("Enter table name")
  }

  if(!data.config){
    return res.status(400).send("Enter table configurations")
  }

  async.auto({
    table: function (cb) {
      const query = "SELECT id from wv_table_types WHERE table_name = $table_name";
      models.sequelize.query(query, {
        bind: {table_name: data.table_name},
        type: models.sequelize.QueryTypes.SELECT
      }).spread(function (data) {
        if(data){
          cb(null, data)
        }else{
          cb({code: 500, msg: 'Table configurations not found'})
        }
      }).catch(function (err) {
        cb({code: 500, msg: 'Internal server error', err: err})
      })
    },
    config: ['table', function (results, cb) {
      const table_type_id = results.table.id;

      const user_config = {
        fk_user_id: fk_u_id,
        fk_wv_table_type_id: table_type_id,
        config: data.config || {}
      };

      models.wv_user_config.findOrCreate({
        where: _.pick(user_config,['fk_user_id','fk_wv_table_type_id']),
        defaults: user_config
      }).spread(function (user_config, created) {
        if (!created) {
          // not created
          if(data && data.config){
            user_config.config = data.config
          }
          user_config.save().then(function () {
            cb(null, 'sucessfully updated')
          }).catch(function (err) {
            cb({code: 500, msg: 'Internal server error', err: err})
          })
        } else {
          cb(null, 'successfully created')
        }
      }).catch(function (err) {
        cb({code: 500, msg: 'Internal server error', err: err})
      })
    }]
  }, function (err, results) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.json(results.config)
    }
  })
};