const moment = require('moment');
const _ = require('lodash');
const utils = require('../../helpers/util');
const models = require('../../db/models');

exports.getUserGlobalSubscriptionStatus = function (req, res) {

  const fk_u_id = req.headers.fk_u_id;

  const query = "SELECT settings->'reports' as reports FROM user_settings WHERE fk_u_id = " + fk_u_id;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
    .spread(function (data) {
      let subscribed = true;

      if(data.reports && data.reports.subscribed === false){
        subscribed = false
      }
      res.json({
        subscribed: subscribed
      })
    }).catch(function () {
      res.status(500).send('Internal server error')
    })
};

exports.updateUserGlobalSubscriptionStatus = function (req, res) {

  const fk_u_id = req.headers.fk_u_id;
  const subscribed = req.body.subscribed;

  if(subscribed || subscribed === false){

    const report_setting = {
      reports: {
        subscribed: subscribed
      }
    };

    models.user_setting.findOne({
      attributes: ['settings'],
      where: {
        fk_u_id: fk_u_id
      }
    }).then(function (user) {
      if (!user) {
        return res.status(404).send('User settings not available')
      }

      user.settings = _.merge(user.settings, report_setting);
      models.user_setting.update({
        settings: user.settings
      }, {
        where: {
          fk_u_id: fk_u_id
        }
      }).then(function () {
        return res.send('Updated successfully')
      }).catch(function (err) {
        console.log("PSQL ERROR UPDATE ",err);
        return res.status(500).send('Internal server error')
      });

    }).catch(function (err) {
      console.log("PSQL ERROR SELECT ",err);
      res.status(500).send('Internal server error')
    })
  }else{
    res.status(400).send("Bad request")
  }
};


exports.getAllUserReports = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  let search_cols = req.query.search;
  let search_col_condition = "";
  let table_search_condition = "";
  const category = req.query.category;

  if(['general','custom'].indexOf(category) < 0){
    return res.status(400).send("Category missing")
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'sr.name',
      client: 'name'
    }, {
      db: 'sr.summary',
      client: 'summary'
    }, {
      db: 'g.name',
      client: 'group.name'
    }]
  )

  // table search
  if(searchKeyword){
    table_search_condition = " AND ( lower(sr.name) like '%"+ searchKeyword +"%' OR lower(sr.summary) like '%"+ searchKeyword +"%' OR lower(g.name) like '%"+ searchKeyword +"%')";
  }

  // report category condition
  const category_condition = category === 'custom' ? " AND rt.fk_c_id = " + fk_c_id : " AND rt.fk_c_id IS NULL";

  const query = "SELECT sr.id, COALESCE(usr.subscribed, true) as subscribed, sr.name, sr.summary, sr.frequency," +
    " json_build_object('id', g.id, 'name', g.name) as group," +
    " json_build_object('id', rt.id, 'name', rt.name) as report_type, count(*) OVER()::INT AS total_count" +
    " FROM scheduled_reports sr " +
    " INNER JOIN report_types rt on rt.id = sr.fk_report_type_id" +
    " INNER JOIN (" +
    "   SELECT id, json_array_elements(sr.groups) as sr_groups" +
    "   FROM scheduled_reports sr " +
    "   WHERE sr.active AND sr.fk_c_id = " + fk_c_id +
    " ) gs on gs.id = sr.id and to_json(gs.sr_groups->'active')::text = 'true'" +
    " INNER JOIN user_group_mappings ugm on ugm.fk_u_id = " + fk_u_id + " and ugm.fk_group_id = to_json(gs.sr_groups->'id')::text::int" +
    " INNER JOIN groups g on g.id = ugm.fk_group_id and g.active" +
    " LEFT JOIN user_settings_reports usr on usr.fk_report_schedule_id = sr.id and usr.fk_group_id = g.id and usr.active and usr.fk_user_id = " + fk_u_id +
    " WHERE g.active and sr.fk_c_id = " + fk_c_id + table_search_condition + category_condition + search_col_condition +
    " ORDER BY sr.id" +
    " LIMIT " + limit + " OFFSET " + offset;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
    .then(function (data) {
      let totalAssets = 0;

      data.forEach(function (i) {
        totalAssets = i.total_count;
        i.total_count = undefined
      });

      res.json({
        total_count: totalAssets,
        data: data
      })
    }).catch(function () {
      res.status(500).send('Internal server error')
    })
};

exports.updateReportSubscription = function (req, res) {

  const fk_u_id = req.headers.fk_u_id;
  const data = req.body;
  const subscription = {
    fk_report_schedule_id: data.id,
    fk_group_id: data.group.id,
    fk_user_id: fk_u_id,
    subscribed: data.subscribed,
    tis: moment().unix()
  };

  if(!subscription.fk_report_schedule_id){
    return res.status(400).send("invalid report id")
  }
  if(!subscription.fk_group_id){
    return res.status(400).send("invalid group id")
  }


  models.user_settings_report.findOrCreate({
    where: {
      fk_report_schedule_id: subscription.fk_report_schedule_id,
      fk_group_id: subscription.fk_group_id,
      fk_user_id: fk_u_id,
      active: true
    },
    defaults: subscription
  }).spread(function (record, created) {

    if (!created) {
      record.subscribed = subscription.subscribed;
      record.save().then(function () {
        res.status(200).send("record updated")
      })
    } else {
      res.status(200).send("record updated")
    }
  }).catch(function () {
    res.status(500).send("Internal server error")
  })
};
