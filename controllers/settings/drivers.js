const generator = require('generate-password');
const bcrypt = require('bcrypt');
const async = require('async');
const fs = require('fs');
const AWS = require('aws-sdk');
const _ = require('lodash');
const path = require('path');

const models = require('../../db/models');
const common = require('../../helpers/common');
const rabbitMq = require('../../helpers/rabbitMQ');

const s3_config = require('../../config/aws/s3.json');
const S3_ASSET_DOCS_BUCKET = process.env.NODE_ENV === 'production' ? 'nu-platform-docs-prod' : 'nu-platform-docs-dev';


AWS.config.update(s3_config);
const params = {
  Bucket: S3_ASSET_DOCS_BUCKET
};
const s3 = new AWS.S3({params: params});

function uploadToS3(file, driver, callback){

  const s3_filename_prefix = "drivers/" + driver.id + "/";

  const s3_filename = s3_filename_prefix + new Date().getTime() + path.extname(file.originalname);

  s3.upload({
    Body: fs.createReadStream(file.path), 
    Key: s3_filename,
    ContentType: 'application/octet-stream' // force download if it's accessed as a top location
  }).send(callback);
}

function validateUserEmail(email, callback){
  const query = "SELECT email from users where email = '" + email + "'";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (result) {
      callback(null,result)
    }).catch(function (err) {
      callback(err)
    })
}

exports.getDriver = function (req, res) {
  let fk_driver_id = req.query.id;
  if(!fk_driver_id){
    return res.status(400).send("Enter driver id")
  }else{

    const query = `SELECT
    u.id, u.name, u.cont_no, u.email, u.firstname, u.lastname, u.location, u.dob, u.aadhar_no, u.sec_cont_no,
    row_to_json(dd.*) as driver_detail,
    row_to_json(ec.*) as emergency_contact,
    row_to_json(dlt.*) as driver_license_type
    FROM users u
    LEFT JOIN driver_details dd ON dd.fk_u_id = u.id
    LEFT JOIN driver_license_types dlt ON dlt.id = dd.fk_driver_license_type AND dlt.active
    LEFT JOIN emergency_contacts ec ON ec.fk_u_id = u.id
    WHERE u.id = ${fk_driver_id} AND u.active;`;

    models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT }).spread(function (driver) {
      if(driver){
        driver.driver_detail["driver_license_type"] = driver.driver_license_type;
  
        async.parallel([
          function(cb) {
            driver.driver_detail.lic_meta_files = _.map(driver.driver_detail.lic_meta_files, function (n) {
              if(!n.deleted)
                return _.pick(n, ['originalname', 'key', 'type'])
            });
            driver.driver_detail.lic_meta_files = _.filter( driver.driver_detail.lic_meta_files, _.identity);
            cb(null, driver)
          },
          function(cb) {
            driver.driver_detail.hazardous_certificate_meta_files = _.map(driver.driver_detail.hazardous_certificate_meta_files, function (n) {
              if(!n.deleted)
                return _.pick(n, ['originalname', 'key', 'type'])
            });
            driver.driver_detail.hazardous_certificate_meta_files = _.filter( driver.driver_detail.hazardous_certificate_meta_files, _.identity);
            cb(null, driver)
          }
        ], function() {
          driver.driver_detail.license_type = driver.driver_detail.driver_license_type ? driver.driver_detail.driver_license_type.type : null;
          res.json(driver)
        })                
      }else{
        res.status(404).send("No driver with this id")
      }
    }).catch(function (err) {
      console.log("driver error : ", err);
      res.status(500).send(err)
    })
  }
};

exports.createDriver = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  let userData = JSON.parse(JSON.stringify(req.body));
  userData = userData.driver && typeof userData.driver === 'string' ? JSON.parse(userData.driver) : userData;
  const driverDocuments = Object.assign([], req.files);


  const uniquePassword = generator.generate({
    length: 10,
    numbers: true
  });

  userData.rmq_p_word = generator.generate({
    length: 16,
    numbers: true
  });

  userData.p_word = bcrypt.hashSync(uniquePassword, 10);
  userData.fk_c_id = fk_c_id;
  async.auto({
    validate: function(callback){
      validateUserEmail(userData.email, function(err, result){
        if(err) callback(err);
        else if(result && result.email) callback({code:400, msg:"User already exists"});
        else callback(null)
      })
    },
    create: ['validate', function(results, callback){
      models.user_type.findOne({
        attributes: ['id'],
        where: {
          type: userData.type
        }
      }).then(function(user_type){

        userData.fk_user_type_id = user_type.id;
        models.user.create(userData, {
          include: [
            models.driver_detail,
            models.emergency_contact
          ]
        }).then(function(driver) {
          const files_meta = {};
          if(driverDocuments.length > 0){
            async.each(driverDocuments, function(file, s3cb){
              uploadToS3(file, driver, function(err, result){
                if(!err) {
                  if (file.fieldname === 'files_lc') {
                    result.originalname = file.originalname;
                    result.fieldname = file.fieldname;
                    result.mimetype = file.mimetype;
                    result.type = 'license';
                    files_meta.lic_meta_files = [result]
                  } else if (file.fieldname === 'files_hc') {
                    result.originalname = file.originalname;
                    result.fieldname = file.fieldname;
                    result.mimetype = file.mimetype;
                    result.type = 'hazardous';
                    files_meta.hazardous_certificate_meta_files = [result]
                  }

                  fs.unlink(file.path)
                } 
                s3cb()
              })
            }, function(){
              if (files_meta !== {}) {
                models.driver_detail.update(files_meta, {
                  where: {
                    fk_u_id: driver.get('id')
                  }
                }).then(function () {
                  callback(null, driver)
                }).catch(function (err) {
                  callback(err)
                })
              } else {
                callback(null, driver)
              }
            })
          }else{
            callback(null, driver)
          }
                            
        }).catch(function (err){
          console.log("Error occured creating drvier ",err);
          callback(err)
        })
      }).catch(function(err){
        console.log("Error occured due to : ", err);
        callback(err)
      })    
    }
    ]}, function(err, results){
    const result = results.create;
    console.log("************* Error ************",err);
    if(err){
      console.log("Error occured due to : ", err);
      res.status(err.code || 500).send({
        error: err,
        message: err.msg,
        code: err.code
      })
    }
    else{
      res.status(201).send({
        code: 201,
        message: 'Driver added successfully',
        id: result.get('id')
      });
      userData.password = uniquePassword;
      async.parallel([
        common.sendEmail.bind(null, userData),
        rabbitMq.generateRabbitMqAccount.bind(null, userData.email, userData.rmq_p_word, '')
      ])
    }
  })

    
};

exports.updateDriver = function (req, res) {
  let userData = JSON.parse(JSON.stringify(req.body));
  userData = userData.driver && typeof userData.driver === 'string' ? JSON.parse(userData.driver) : userData;

  const driverDocuments = Object.assign([], req.files);

  models.user.findOne({
    where: {
      email: userData.email
    }
  }).then(function(driver) {
    const userId = driver.get('id');
    async.parallel([
      function (cb) {
        models.user.update(userData, {
          where: {
            id: userId
          }
        }).then(function () {
          cb()
        }).catch(function (err) {
          cb(err)
        })
      },
      function (cb) {
        models.emergency_contact.update(userData.emergency_contact, {
          where: {
            fk_u_id: userId
          }
        }).then(function () {
          cb()
        }).catch(function (err) {
          cb(err)
        })
      },
      function (cb) {
        models.driver_detail.findOne({
          where: {
            fk_u_id: userId
          }, 
          raw: true
        }).then(function (driver_detail) {
                    
          if(driverDocuments.length > 0){
                                                
            async.each(driverDocuments, function(file, s3cb){
              uploadToS3(file, driver, function(err, result){
                if(!err) {
                  if (file.fieldname === 'files_lc') {
                    driver_detail.lic_meta_files = _(driver_detail.lic_meta_files).forEach( function (oldUrl) {
                      return oldUrl.deleted = true
                    });
                    result.originalname = file.originalname;
                    result.fieldname = file.fieldname;
                    result.mimetype = file.mimetype;
                    result.type = 'license';
                    driver_detail.lic_meta_files ? driver_detail.lic_meta_files.push(result) : driver_detail.lic_meta_files = [result]
                  } else if (file.fieldname === 'files_hc') {
                    driver_detail.hazardous_certificate_meta_files = _(driver_detail.hazardous_certificate_meta_files).forEach( function (oldUrl) {
                      return oldUrl.deleted = true
                    });
                    result.originalname = file.originalname;
                    result.fieldname = file.fieldname;
                    result.mimetype = file.mimetype;
                    result.type = 'hazardous';
                    driver_detail.hazardous_certificate_meta_files ? driver_detail.hazardous_certificate_meta_files.push(result) : driver_detail.hazardous_certificate_meta_files = [result]
                  }

                  fs.unlink(file.path)
                } 
                s3cb()
              })
            }, function(){
                            
              _.forOwn(userData.driver_detail, function(value, key) {
                driver_detail[key] = value
              } );
              models.driver_detail.update(driver_detail, {
                where: {
                  fk_u_id: userId
                }
              }).then(function () {
                cb()
              }).catch(function (err) {
                cb(err)
              })
                           
            })
          } else {

            if(userData.delete_files) {
              _(userData.delete_files).forEach(function(i) {
                if(i.type === "lc") {
                  driver_detail.lic_meta_files = _(driver_detail.lic_meta_files).forEach( function (oldUrl) {
                    if(oldUrl.key === i.key) {
                      return oldUrl.deleted = true
                    }
                    return oldUrl
                  });
                } else if(i.type === "hc") {
                  driver_detail.hazardous_certificate_meta_files = _(driver_detail.hazardous_certificate_meta_files).forEach( function (oldUrl) {
                    if(oldUrl.key === i.key) {
                      return oldUrl.deleted = true
                    }
                    return oldUrl
                  });
                }
              })
            }
            _.forOwn(userData.driver_detail, function(value, key) {
              driver_detail[key] = value
            } );
            models.driver_detail.update(driver_detail, {
              where: {
                fk_u_id: userId
              }
            }).then(function () {
              cb()
            }).catch(function (err) {
              cb(err)
            })
          }
        }).catch(function (err) {
          cb(err)
        })
      }
    ], function () {
      res.json({
        code: 200,
        message: 'Driver updated successfully'
      })
    })
      
  }).catch(function (err){
    res.status(500).send({
      error: err,
      message: 'Internal server error',
      code: 500
    })
  })
};