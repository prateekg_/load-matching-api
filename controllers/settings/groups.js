const async = require('async');
const _ = require('lodash');
const models = require('../../db/models/index');
const merge = require('deepmerge');
const Hashids = require("hashids");
const hashids = new Hashids("CD234cF2)(vgdkjs$23153%$##(", 16, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
const defaultSettings = require('../../helpers/defaultSettings');
const nuRedis = require('../../helpers/nuRedis');
const utils = require('../../helpers/util');
const groupLog = require('../../helpers/groupLog');

function deleteAssetMapping(aid, callback) {
  const eids = [];
  const query = " SELECT e.e_id, a.asset_id FROM entities e " +
    " LEFT JOIN entity_mappings em  ON em.fk_entity_id = e.id AND em.connected " +
    " LEFT JOIN assets a ON em.fk_asset_id = a.id AND a.active " +
    " WHERE a.asset_id = '" + aid + "' LIMIT 1";
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (result) {
      result.forEach(function (i) { eids.push("EM-" + i.e_id); });
      if(eids.length > 0){ nuRedis.deleteRedisKey(eids, function () { callback(null, null); })
      }else{ callback(null, null); }
    }).catch(function () {
      callback(null, null);
    })
}

function deleteUserGroupMapping(gid, callback) {
  nuRedis.deleteRedisKey("GP-"+ gid, function () {
    callback(null, null);
  })
}

function getPoiDataFromDb(p, cb){

  const pois = typeof p === "number" ? [p] : p;
  if(pois.length === 0){
    cb(null, null)
  }else{
    const x = typeof pois === "string" ? pois : JSON.stringify(pois);
    const poi_ids = "(" + x.substring(1, x.length - 1) + ")";

    const query = "SELECT p.id, p.fk_c_id, p.name, pt.type, json_agg(json_build_object('id',pgm.fk_group_id)) as groups " +
      " FROM pois p " +
      " INNER JOIN poi_types pt ON pt.id = p.typ " +
      " INNER JOIN poi_group_mappings pgm ON pgm.fk_poi_id = p.id AND pgm.connected " +
      " WHERE p.priv_typ AND p.id in " + poi_ids +
      " GROUP BY p.id, pt.id";

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
      if(data.length > 0){
        nuRedis.updatePoiKeys(data, cb)
      }else{
        nuRedis.deleteRedisKey("POI-"+ pois[0], cb)
      }
    }).catch(function (err) {
      cb(err)
    })
  }
}

/*
Gets list of all enabled and disabled groups
Each group gets count assets, users and pois
Assets count checks if assets are active and connected to a group
*/
exports.listAll = function (req, res) {

  const keyword = req.query.q || '';
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const limit = req.query.limit;
  const page = req.query.page || 0;
  const isActive = !(Boolean(req.query.active) && req.query.active === 'false');
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let limitCondition = "";
  if(limit){
    const offset = limit * page;
    limitCondition = " LIMIT " + limit + " OFFSET " + offset
  }

  const searchkey = '%' + keyword + '%';

  const query = "SELECT g.id, g.is_admin, g.gid, g.name, COALESCE(ugm.count, 0)::INT AS \"totalUsers\", COALESCE(agm.count, 0)::INT AS \"totalAssets\"," +
    " COALESCE(pgm.count, 0)::INT as \"totalPois\", EXTRACT(EPOCH FROM g.\"createdAt\")::INT AS \"createdAt\", g.active," +
    " COUNT(*) OVER()::INT AS count FROM groups g " +
    " LEFT JOIN ( " +
    " SELECT fk_group_id, count(*) " +
    " FROM asset_group_mappings agm " +
    " LEFT JOIN assets a ON a.id = agm.fk_asset_id AND a.active" +
    " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id" +
    " WHERE agm.connected " +
    " GROUP BY fk_group_id" +
    ") agm ON agm.fk_group_id = g.id" +
    " LEFT JOIN ( " +
    " SELECT fk_group_id, count (*) FROM user_group_mappings ugm  " +
    " INNER JOIN users u on u.id = ugm.fk_u_id AND u.active AND is_internal = FALSE and ugm.connected" +
    " GROUP BY fk_group_id" +
    ") ugm ON ugm.fk_group_id = g.id" +
    " LEFT JOIN ( " +
    " SELECT fk_group_id, count(*) " +
    " FROM poi_group_mappings pgm " +
    " LEFT JOIN pois p ON p.id = pgm.fk_poi_id AND p.active" +
    " INNER JOIN poi_types pt on pt.id = p.typ" +
    " WHERE pgm.connected" +
    " GROUP BY fk_group_id" +
    ") pgm ON pgm.fk_group_id = g.id" +
    " WHERE g.active is " + isActive + " AND g.name ilike '" + searchkey + "'" +
    " AND CASE WHEN ( " +
    " SELECT g.is_admin FROM user_group_mappings ugm " +
    " INNER JOIN groups g ON g.id = ugm.fk_group_id WHERE ugm.connected AND g.is_admin AND fk_u_id = " + fk_u_id +
    " ) THEN g.fk_c_id = " + fk_c_id + " ELSE g.fk_c_id = " + fk_c_id + " AND g.id in " + groupIds + " END " +
    " ORDER BY g.name " + limitCondition;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (result) {
      let tot_rows = 0;
      result.forEach(function (i) {
        tot_rows = i.count;
        delete i.count
      });
      res.json({
        count: tot_rows,
        data: result
      })
    }).catch(function () {
      res.status(500).send()
    })

};

/*
Gets list of all enabled and disabled groups
user is part of
*/

exports.listMyGroups = function (req, res) {

  const keyword = req.query.q || '';
  const fk_u_id = req.headers.fk_u_id;
  const limit = req.query.limit;
  const page = req.query.page || 0;
  let limitCondition = "";
  if(limit){
    const offset = limit * page;
    limitCondition = " LIMIT " + limit + " OFFSET " + offset
  }

  const searchkey = '%' + keyword + '%';

  const query = "SELECT g.id, g.is_admin, g.gid, g.name, EXTRACT(EPOCH FROM g.\"createdAt\")::INT AS \"createdAt\", g.active, ugm.fk_u_id, COUNT(*) OVER()::INT AS count" +
    " FROM groups g  INNER JOIN  user_group_mappings ugm  on g.id = ugm.fk_group_id" +
    " INNER JOIN users u on u.id = ugm.fk_u_id" +
    " WHERE u.active AND ugm.connected  AND ugm.fk_u_id = " + fk_u_id +
    " AND g.name ilike '" + searchkey + "'" +
    " ORDER BY g.name " + limitCondition;


  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (result) {
      let tot_rows = 0;
      result.forEach(function (i) {
        tot_rows = i.count;
        delete i.count
      });
      res.json({
        count: tot_rows,
        data: result
      })
    }).catch(function () {
      res.status(500).send()
    })

};

/*
Gets list of all assets which are active
*/
exports.assetList = function(req, res){

  let groupId = req.query.gid;
  const keyword = req.query.q || '';
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = limit * page;
  const sort = req.query.sort;
  let sortCondition = "";
  const connected = req.query.connected || true;
  const asset_type = req.query.asset_type;

  sortCondition = utils.processTableColumnSort(
    sort,
    [{
      db: 'a.name',
      client: 'assetName'
    }, {
      db: "atp.type",
      client: 'type'
    }, {
      db: "a.lic_plate_no",
      client: 'licNo'
    }],
    " ORDER BY a.name"
  )

  const typeCondition = asset_type ? ' AND atp.type = \'' + asset_type + '\'' : '';

  const searchkey = keyword ? "AND (a.lic_plate_no ilike '%" + keyword + "%' OR a.name ilike '%" + keyword + "%')" : "";

  if(!groupId){
    return res.status(400).send('Invalid id')
  }

  const query = "SELECT g.id, g.gid, g.name, agm.connected as mapped,EXTRACT(EPOCH FROM agm.\"createdAt\")::INT AS \"mappedOn\"," +
    " a.id as \"assetId\", a.lic_plate_no as \"licNo\", a.name as \"assetName\", atp.type, count(*) over()::int as count  FROM groups g " +
    " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id" +
    " LEFT JOIN assets a ON a.id = agm.fk_asset_id AND a.active" +
    " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id" +
    " WHERE g.gid = '" + groupId + "' " + searchkey + " and agm.connected = " + connected + typeCondition +
    sortCondition + " OFFSET " + offset + " LIMIT " + limit;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (result) {
      let tot_rows = 0;
      result.forEach(function (i) {
        tot_rows = i.count;
        delete i.count
      });
      res.json({
        count: tot_rows,
        data: result
      })
    }).catch(function () {
      res.status(500).send()
    })
};

exports.poiList = function(req, res){

  let groupId = req.query.gid;
  const keyword = req.query.q || '';
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = limit * page;
  const sort = req.query.sort;
  let sortCondition = "";
  const connected = req.query.connected || true;

  sortCondition = utils.processTableColumnSort(
    sort,
    [{
      db: 'p.name',
      client: 'poiName'
    }],
    " ORDER BY p.name"
  )

  const searchkey = '%' + keyword + '%';

  if(!groupId){
    return res.status(400).send('Invalid id')
  }

  const query = "SELECT g.id, g.gid, g.name, pgm.connected as mapped,EXTRACT(EPOCH FROM pgm.\"createdAt\")::INT AS \"mappedOn\"," +
    " p.id as \"poiId\", p.name as \"poiName\", pt.type  as \"poiType\", count(*) over()::int as count  FROM groups g " +
    " LEFT JOIN poi_group_mappings pgm ON pgm.fk_group_id = g.id" +
    " LEFT JOIN pois p ON p.id = pgm.fk_poi_id AND p.active" +
    " INNER JOIN poi_types pt on pt.id = p.typ" +
    " WHERE g.gid = '" + groupId + "' AND p.name ilike '" + searchkey + "' and pgm.connected = " + connected +
    sortCondition + " OFFSET " + offset + " LIMIT " + limit;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (result) {
      let tot_rows = 0;
      result.forEach(function (i) {
        tot_rows = i.count;
        delete i.count
      });
      res.json({
        count: tot_rows,
        data: result
      })
    }).catch(function () {
      res.status(500).send()
    })
};

exports.userList = function(req, res){

  let groupId = req.query.gid;
  const keyword = req.query.q || '';
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = limit * page;
  const sort = req.query.sort;
  const connected = req.query.connected || true;
  let sortCondition = "";

  sortCondition = utils.processTableColumnSort(
    sort,
    [{
      db: 'u.name',
      client: 'userName'
    }],
    " ORDER BY u.name"
  )
  
  const searchkey = keyword ? " AND u.name ilike '%" + keyword + "%'" : "";

  if(!groupId){
    return res.status(400).send('Invalid id')
  }

  const query = "SELECT g.id, g.gid, g.name, ugm.connected as mapped,EXTRACT(EPOCH FROM ugm.\"createdAt\")::INT AS \"mappedAt\"," +
    " u.id as \"userId\", u.name as \"userName\", u.email, count(*) over()::int as count  " +
    " FROM groups g " +
    " INNER JOIN user_group_mappings ugm ON ugm.fk_group_id = g.id" +
    " INNER JOIN users u ON u.id = ugm.fk_u_id AND u.active AND u.is_internal = FALSE" +
    " WHERE g.gid = '" + groupId + "' " + searchkey + " AND ugm.connected =" + connected +
    sortCondition + " OFFSET " + offset + " LIMIT " + limit;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (result) {
      let tot_rows = 0;
      result.forEach(function (i) {
        tot_rows = i.count;
        delete i.count
      });
      res.json({
        count: tot_rows,
        data: result
      })
    }).catch(function (err) {
      console.error(err);
      res.status(500).send()
    })
};

exports.addAsset = function (req, res) {
  // todo : delete entity mapping from redis
  // add/enable asset from group

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;
  const gid = postData.gid;
  const assets = postData.assets;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, mainCb) {
      const logDataAssets = [];
      let fk_group_id;
      async.each(assets, function (aid, callback) {
        async.series([
          function (cb) {
            const query = " SELECT a.id AS fk_asset_id, g.id AS fk_group_id, agm.connected from assets a" +
              " INNER JOIN groups g on g.fk_c_id = " + fk_c_id + " and g.gid = '" + gid + "'" +
              " LEFT JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id AND agm.fk_group_id = g.id " +
              " WHERE a.active AND g.gid = '" + gid + "' AND a.id = " + aid;

            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (result) {
                if (result && result.connected === false) {
                  logDataAssets.push(result.fk_asset_id);
                  fk_group_id = result.fk_group_id;
                  const query = "UPDATE asset_group_mappings SET connected = true WHERE fk_group_id = " + result.fk_group_id + " and fk_asset_id = " + result.fk_asset_id;

                  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                    .then(function () {
                      cb()
                    }).catch(function (err) {
                      cb(err)
                    })
                } else if (result && !result.connected) {
                  logDataAssets.push(result.fk_asset_id);
                  fk_group_id = result.fk_group_id;
                  const newMapping = {
                    fk_group_id: result.fk_group_id,
                    fk_asset_id: result.fk_asset_id,
                    connected: true,
                    tis: parseInt(Date.now() / 1000)
                  };
                  models.asset_group_mapping.create(newMapping).then(function () {
                    cb()
                  }).catch(function (err) {
                    cb({
                      code: 500,
                      msg: err
                    })
                  })
                } else {
                  cb(null, null)
                }
              }).catch(function (err) {
                cb({
                  code: 500,
                  msg: err
                })
              })
          }, function (cb) {
            deleteAssetMapping(aid, function () {
              cb()
            })
          }
        ], function (err) {
          callback(err)
        })
      }, function (err) {
        if(err) {
          return mainCb(err)
        }
        groupLog.addAsset(fk_group_id, fk_u_id, logDataAssets, req.body, mainCb)
      })
    }
  ],function (err) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      } else {
        res.status(500).send(err)
      }
    }else{
      res.json("Asset added sucessfully")
    }
  })
};

exports.disableAsset = function (req, res) {
  // todo : delete entity mapping from redis
  // disable asset from group
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  if(!postData.gid && !postData.assetId){
    return res.status(400).send('Invalid paramaters')
  }

  const gid = postData.gid;
  const aid = postData.assetId;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // check if asset and group is valid
      const query = " SELECT agm.*, a.asset_id, g.is_admin FROM asset_group_mappings agm " +
        "INNER JOIN groups g ON g.id = agm.fk_group_id AND g.active " +
        "INNER JOIN assets a ON a.id = agm.fk_asset_id AND a.active " +
        "WHERE g.gid = '" + gid + "' AND a.id = " + aid;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb({
              code:404,
              msg: 'asset cannot disabled from admin group'
            })
          }else if(result && result.connected){
            cb(null, result)
          }else if(result && !result.connected){
            // asset already disabled
            cb({
              code:404,
              msg: 'asset already disabled'
            })
          }else{
            // asset doest not belong in this group
            cb({
              code:404,
              msg: 'asset doest not belong in this group'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // update the asset

      const query = "UPDATE asset_group_mappings SET connected = false WHERE fk_group_id = " + data.fk_group_id + " and fk_asset_id = " + data.fk_asset_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function () {
          deleteAssetMapping(data.asset_id, function () {
            groupLog.disableAsset(data.fk_group_id, fk_u_id, data.fk_asset_id, req.body, cb)
          })
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    }
  ],function (err) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json("Asset disabled sucessfully")
    }
  })
};

/*
Admin removes asset from group
This api is used when admin selects assets to add to a groups and unselects.
Unselect assets calls this api
*/
exports.removeAsset = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  const requiredKeys = ['assets', 'gid'];
  let dataValid = utils.validateMinPayload(requiredKeys, postData);

  if(!dataValid){
    return res.status(400).send('Mandatory keys: '+ requiredKeys)
  }

  const gid = postData.gid;
  const assets = postData.assets;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (cb) {
      const query = " SELECT g.id " +
        " FROM groups g " +
        " WHERE g.fk_c_id = " + fk_c_id + " and g.gid = '" + gid + "'";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          models.asset_group_mapping.destroy({
            where:{
              fk_group_id: result.id,
              fk_asset_id: {
                $in: assets 
              }
            }
          }).then(function(){
            cb(null)
          });
        }).catch(function (err) {
          cb({
            code: 500,
            msg: err
          })
        })
    },
    function(cb){
      const query = " SELECT asset_id " +
        " FROM assets a " +
        " WHERE id in (" + assets + ")";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          console.log(result);
          async.each(result, function(asset_id, callback){ 
            console.log(" Asset id ================");
            console.log(asset_id);
                        
            deleteAssetMapping(asset_id, function () {
              callback()
            })
          }, function(){
            cb(null)
          })
        })
    }
  ],function (err) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      } else {
        res.status(500).send(err)
      }
    }else{
      res.json("Assets removed sucessfully")
    }
  })
};

exports.createGroup = function (req, res) {
  // create new group

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  //validation
  if(!postData.name || postData.name.length < 2){
    return res.status(400).send("Enter group name")
  }

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // create group

      const newGroup = {
        fk_c_id: fk_c_id,
        gid: hashids.encode(Date.now()),
        name: postData.name,
        description: postData.description || null,
        active: true,
        is_admin: false,
        in_int: true
      };

      models.group.findOrCreate({
        where: {
          name: newGroup.name,
          fk_c_id: newGroup.fk_c_id
        },
        defaults: newGroup
      }).spread(function (group, created) {
        if (!created) {
          // not created - name already exists
          cb({
            code: 409,
            msg: "Group name already exists"
          })
        } else {
          //Create default settings for group
          const groupObj = group.get({plain: true});
          const groupId = groupObj.id;
          async.parallel([
            function (callback) {
              models.access_setting.create({
                fk_group_id: groupId,
                settings: defaultSettings.access
              }).then(function () {
                callback()
              }).catch(function () {
                callback({
                  code: 500,
                  msg: 'Internal server error'
                })
              })
            }, function (callback) {
              models.group_setting.create({
                fk_g_id: groupId,
                settings: defaultSettings.group
              }).then(function () {
                callback()
              }).catch(function () {
                callback({
                  code: 500,
                  msg: 'Internal server error'
                })
              })
            },
            //Create log
            function (callback) {
              groupLog.addGroup(groupId, fk_u_id, req.body, callback)
            }
          ], function (err) {
            if(err) {
              return cb(err)
            }
            cb(null, {group_id: groupId, gid: groupObj.gid})
          })
        }
      }).catch(function (err) {
        cb({
          code: 500,
          msg: err
        })
      })
    }
  ],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json(result)
    }
  })
};

exports.groupDetail = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  let gid = req.query.gid;

  //validation
  if(!gid){
    return res.status(400).send("Enter group ID")
  }

  const query = " SELECT gid, name, description, active, is_admin as \"isAdminGroup\" FROM groups g " +
    " WHERE g.fk_c_id = " + fk_c_id + " AND g.gid = '" + gid + "'";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (result) {
      if(result){
        res.json(result)
      }else{
        res.status(404).send()
      }
    }).catch(function () {
      res.status(500).send()
    })

};

exports.updateGroup = function (req, res) {
  // update group

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  //validation
  if(!postData.gid){
    return res.status(400).send("Enter group ID")
  }
  const gid = postData.gid;
  const updateGroup = {};

  if(postData.name && postData.name.length > 2){
    updateGroup.name = postData.name
  }

  if(postData.description && postData.description.length > 2){
    updateGroup.description = postData.description
  }

  if(postData.active !== null && typeof postData.active === 'boolean'){
    updateGroup.active = postData.active
  }

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // delete group

      const query = "SELECT * FROM groups WHERE fk_c_id = " + fk_c_id + " AND gid != '" + gid + "' AND name ilike '" + updateGroup.name + "' LIMIT 1";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (matched) {
          if(matched){
            cb({
              code:409,
              msg: "Group name already exists"
            })
          }else{
            models.group.update(updateGroup, {
              where: {
                fk_c_id: fk_c_id,
                gid: gid
              }
            }).spread(function (result) {
              if(result > 0){
                cb(null, "Group updated sucessfully")
              }else{
                cb({
                  code: 404,
                  msg: "Group not found"
                })
              }
            }).catch(function (err) {
              cb(err)
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    }
  ],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json(result)
    }
  })
};

exports.deleteGroup = function (req, res) {
  // todo : delete entity mapping from redis
  // delete group

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  //validation
  if(!postData.gid){
    return res.status(400).send("Enter group ID")
  }
  const gid = postData.gid;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    }
    ,
    function (data, cb) {
      // delete group

      models.group.update({
        active: false
      }, {
        where: {
          fk_c_id: fk_c_id,
          gid: gid,
          is_admin: false
        },
        returning: true,
        raw:true
      }).then(function (result) {
        if(result[0] > 0){
          const groups = result[1];
          groupLog.disableGroup(groups[0].id, fk_u_id, req.body, cb)
        }else{
          cb({
            code: 404,
            msg: "Group not found"
          })
        }
      }).catch(function (err) {
        cb(err)
      })
    }
  ],function (err) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json("Group deleted sucessfully")
    }
  })
};

exports.addPoi = function (req, res) {
  // add/enable poi from group

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  if(!postData.gid && !postData.poiId){
    return res.status(400).send('Invalid parameters')
  }

  const gid = postData.gid;
  const pois = postData.pois;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, mainCb) {
      const logDataPois = [];
      let fk_group_id;
      async.each(pois, function (poiId, callback) {
        const query = " SELECT p.id AS fk_poi_id, g.id AS fk_group_id, pgm.connected from pois p" +
          " INNER JOIN groups g on g.fk_c_id = " + fk_c_id + " and g.gid = '" + gid + "'" +
          " LEFT JOIN poi_group_mappings pgm ON pgm.fk_poi_id = p.id AND pgm.fk_group_id = g.id " +
          " WHERE p.active AND g.gid = '" + gid + "' AND p.id = " + poiId;

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .spread(function (result) {
            if (result && result.connected === false) {
              logDataPois.push(result.fk_poi_id);
              fk_group_id = result.fk_group_id;
              const query = "UPDATE poi_group_mappings SET connected = true WHERE fk_group_id = " + result.fk_group_id + " and fk_poi_id = " + result.fk_poi_id;

              models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                .then(function () {
                  callback()
                }).catch(function (err) {
                  callback(err)
                })
            } else if (result && !result.connected) {
              logDataPois.push(result.fk_poi_id);
              fk_group_id = result.fk_group_id;
              const newMapping = {
                fk_group_id: result.fk_group_id,
                fk_poi_id: result.fk_poi_id,
                connected: true,
                tis: parseInt(Date.now() / 1000)
              };
              models.poi_group_mapping.create(newMapping).then(function () {
                callback()
              }).catch(function (err) {
                callback({
                  code: 500,
                  msg: err
                })
              })
            } else {
              callback(null)
            }
          }).catch(function (err) {
            callback({
              code: 500,
              msg: err
            })
          })

      }, function (err) {
        if(err) {
          return mainCb(err)
        }
        async.parallel([
          function (cb) {
            getPoiDataFromDb(logDataPois, cb)
          },
          function (cb) {
            groupLog.addPoi(fk_group_id, fk_u_id, logDataPois, req.body, cb)
          }
        ], function (err) {
          mainCb(err)
        })
      })
    }
  ], function (err) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json("Poi added sucessfully")
    }
  })
};

exports.disablePoi = function (req, res) {

  // disable poi from group

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  if(!postData.gid && !postData.poiId){
    return res.status(400).send('Invalid paramaters')
  }

  const gid = postData.gid;
  const poiId = postData.poiId;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // check if poi and group is valid
      const query = " SELECT pgm.* FROM poi_group_mappings pgm"
        + " INNER JOIN groups g ON g.id = pgm.fk_group_id AND g.active"
        + " INNER JOIN pois p ON p.id = pgm.fk_poi_id AND p.active"
        + " WHERE g.gid = '" + gid + "' AND p.id = " + poiId;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.connected){
            cb(null, result)
          } else if(result && !result.connected){
            // poi already disabled
            cb({
              code:404,
              msg: 'poi already disabled'
            })

          }else{
            // poi doest not belong in this group i.e add the poi
            cb({
              code:404,
              msg: 'poi does not belong in this group'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // update poi

      const query = "UPDATE poi_group_mappings SET connected = false WHERE fk_group_id = " + data.fk_group_id + " and fk_poi_id = " + data.fk_poi_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function () {
          async.parallel([
            function (cb1) {
              getPoiDataFromDb(data.fk_poi_id, cb1)
            },
            function (cb1) {
              groupLog.disablePoi(data.fk_group_id, fk_u_id, data.fk_poi_id, req.body, cb1)
            }
          ], function (err) {
            cb(err)
          })
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    }
  ],function (err) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json("Poi disabled sucessfully")
    }
  })
};

exports.removePoi = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  if(!postData.gid && !postData.poiId){
    return res.status(400).send('Invalid parameters')
  }

  const gid = postData.gid;
  const pois = postData.pois;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (cb) {
      const query = " SELECT g.id " +
        " FROM groups g " +
        " WHERE g.fk_c_id = " + fk_c_id + " and g.gid = '" + gid + "'";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          models.poi_group_mapping.destroy({
            where:{
              fk_group_id: result.id,
              fk_poi_id: {
                $in: pois 
              }
            }
          }).then(function(){
            cb(null)
          });
        }).catch(function (err) {
          cb({
            code: 500,
            msg: err
          })
        })
    },
    function(cb){
      const query = " SELECT id " +
        " FROM pois " +
        " WHERE id in (" + pois + ")";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          console.log(result);
          async.each(result, function(obj, callback){ 
            console.log(" POI id ================");
            console.log(JSON.stringify(obj));
                        
            getPoiDataFromDb(obj, callback)
          }, function(){
            cb(null)
          })
        })
            
    }
  ], function (err) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json("POI's removed sucessfully")
    }
  })
};

exports.addUser = function (req, res) {
  // add/enable user from group

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  const gids = typeof postData.gid === "string" ? [postData.gid] : postData.gid;
  const users = postData.users;

  const requiredKeys = ["users", "gid"];
  let dataValid = utils.validateMinPayload(requiredKeys, postData);

  if(!dataValid || users.length === 0 || gids.length === 0){
    return res.status(400).send('Mandatory keys: '+ requiredKeys)
  }

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, {})
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, mainCb) {
      //Add new entry or update existing
      const logDataUsers = [];
      let fk_group_id;
      async.eachSeries(users, function (userId, callback) {
        async.each(gids, function(gid, eachCallback){ 
          async.waterfall([
            function (cb) {
              const query = " SELECT u.id as fk_u_id, us.group_settings, ugm.connected, g.id as fk_group_id, g.gid FROM users u" +
                " INNER JOIN groups g ON g.gid = '" + gid + "' AND g.fk_c_id = " + fk_c_id +
                " LEFT JOIN user_group_mappings ugm ON ugm.fk_u_id = u.id AND g.id = ugm.fk_group_id" +
                " INNER JOIN user_settings us ON us.fk_u_id = u.id" +
                " WHERE u.active AND u.id = " + userId;

              models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                .spread(function (result) {
                  console.log(result);
                  if(result && result.connected === false) {
                    const query = "UPDATE user_group_mappings SET connected = true WHERE fk_group_id = " + result.fk_group_id + " and fk_u_id = " + result.fk_u_id;

                    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                      .then(function () {
                        groupLog.enableUser(result.fk_group_id, fk_u_id, result.fk_u_id, req.body, function () {
                          cb(null, result)
                        })
                      }).catch(function (err) {
                        cb(err)
                      })
                  } else if(result && !result.connected){
                    logDataUsers.push(result.fk_u_id);
                    fk_group_id = result.fk_group_id;
                    const newMapping = {
                      fk_group_id: result.fk_group_id,
                      fk_u_id: result.fk_u_id,
                      connected: true,
                      tis: parseInt(Date.now() / 1000)
                    };

                    models.user_group_mapping.create(newMapping).then(function () {
                      cb(null, result)
                    }).catch(function (err) {
                      cb({
                        code: 500,
                        msg: err
                      })
                    })
                  } else {
                    cb(null, null)
                  }
                }).catch(function (err) {
                  cb(err)
                })
            },
            function (result, cb) {
              //update User group settings
              console.log("2nd function =========== ",result);
              if(!result) {
                return cb(null, null)
              }
              result.group_settings.push(defaultSettings.userGroup(result));
              const query = "update user_settings set group_settings = '" + JSON.stringify(result.group_settings) + "' where fk_u_id = " + userId;
              models.sequelize.query(query, {type: models.sequelize.QueryTypes.UPDATE})
                .then(function (updated) {
                  console.log(" user_settings updated =============", updated);
                  deleteUserGroupMapping(gid, function () {
                    console.log(" deleteUserGroupMapping =============", updated)
                  });
                  cb()
                }).catch(function (err) {
                  cb({
                    code:500,
                    msg: err
                  })
                })
            }
          ], function (err) {
            console.log(" ========== waterfall callback =============", err);
            eachCallback(err)
          })
        }, function (err) {
          callback(err)
        })
      }, function (err) {
        if(err) {
          return mainCb(err)
        }
        if (logDataUsers.length === 0) {
          return mainCb()
        }
        groupLog.addUser(fk_group_id, fk_u_id, logDataUsers, req.body, mainCb)
      })
    }
  ],function (err) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      } else {
        res.status(500).send(err)
      }
    }else{
      res.status(200).send("User added successfully")
    }
  })
};

exports.disableUser = function (req, res) {

  // disable user from group

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const postData = req.body;

  if(!postData.gid && !postData.userId){
    return res.status(400).send('Invalid paramaters')
  }

  const gid = postData.gid;
  const userId = postData.userId;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // check if user and group is valid and get user settings
      const query = " SELECT ugm.*, us.group_settings FROM user_group_mappings ugm"
        + " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active"
        + " INNER JOIN users u ON u.id = ugm.fk_u_id AND u.active"
        + " INNER JOIN user_settings us on u.id = us.fk_u_id"
        + " WHERE g.gid = '" + gid + "' AND u.id = " + userId;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.connected){
            cb(null, result)
          } else if(result && !result.connected){
            // user already disabled
            cb({
              code:404,
              msg: 'user already disabled'
            })

          }else{
            // user doest not belong in this group i.e add the user
            cb({
              code:404,
              msg: 'user does not belong in this group'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // update user

      const query = "UPDATE user_group_mappings SET connected = false WHERE fk_group_id = " + data.fk_group_id + " and fk_u_id = " + data.fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function () {

          const settings = _.reject(data.group_settings, {fk_g_id: data.fk_group_id});

          models.user_setting.update({
            group_settings: settings
          }, {
            where: {
              fk_u_id: data.fk_u_id
            }
          }).then(function () {
            deleteUserGroupMapping(gid, function () {
              groupLog.disableUser(data.fk_group_id, fk_u_id, data.fk_u_id, req.body, cb)
            })
          }).catch(function (err) {
            cb({
              code:500,
              msg: err
            })
          })
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    }
  ],function (err) {
    if(err){
      console.error(err);
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json("User disabled sucessfully")
    }
  })
};

/**
 * Deletes user from groups
 * @param {Object} req.body - userId, group_ids
 */
exports.deleteGroupUser = function (req, res) {

  const userId = req.body.userId;
  const groupIds = req.body.groupIds;
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
    " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
    " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (result) {
      if (result && result.is_admin) {
        async.waterfall([ 
                    
          function(callback){
            const query = " SELECT us.group_settings FROM user_settings us WHERE us.fk_u_id = " + userId;

            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (result) {
                if(result && result.group_settings.length > 0){
                  let settings = result.group_settings;

                  async.each(groupIds, function(group_id, eachCallback){
                    deleteUserGroupMapping(group_id, function () {
                    });
                    settings = _.reject(settings, {fk_g_id: group_id});
                    eachCallback()
                  }, function(err){
                    if(err) {
                      console.log(" ******************** error ", err);
                      callback(err)
                    }
                    else {
                                                    
                      models.user_setting.update({
                        group_settings: settings
                      }, {
                        where: {
                          fk_u_id: userId
                        }
                      }).then(function () {
                        callback(null)
                      })
                    }
                  })
                }else{
                  callback(null)
                }
              }).catch(function (err) {
                callback({
                  code:500,
                  msg: err
                })
              })
          },
          function(callback){
            models.user_group_mapping.destroy({
              where:{
                fk_u_id: userId,
                fk_group_id: {
                  $in: groupIds 
                }
              }
            }).then(function(deleted){
              if (deleted) {
                callback(null)
              } else {
                callback({code:404, msg:'Invalid user id'})
              }
            }).catch(function (err) {
              console.log(err);
              callback({code:500, msg:'Internal server error'})
            })
          }
        ], function(err){
          if(err){
            res.status(err.code || 500).send()
          }else{
            res.status(200).send("User deleted from selected groups")                        
          }
        })
                
      } else {
        res.status(403).send('User does not have permissions')
      }
    }).catch(function (err) {
      console.log(err);
      res.status(500).send('Internal server error'+err)
    })
};

exports.updateAccess = function (req, res) {
  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  const postData = req.body;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // check if user and group is valid
      const query = "select id from groups where gid = '" + postData.gid + "' and fk_c_id = " + fk_c_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.id){
            cb(null, result)
          } else {
            // user doest not belong in this group
            cb({
              code:404,
              msg: 'group doesn\'t belong to company'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      // update access settings
      models.access_setting.update({
        settings: postData.settings
      }, {
        where: {
          fk_group_id: data.id
        }
      }).spread(function (updated) {
        if(updated > 0) {
          return cb({
            code: 200,
            msg: 'Successfully updated'
          })
        }
        return cb({
          code:404,
          msg: 'Access settings couldn\'t be updated'
        })
      }).catch(function (err) {
        cb({
          code: 500,
          msg: err
        })
      })
    }
  ],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }
    }else{
      res.json(result)
    }
  })
};

exports.getAccess = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const gid = req.query.gid;
  const query = " select acs.settings from groups g "
    + " inner join access_settings acs on g.id = acs.fk_group_id "
    + " where g.gid = '" + gid + "' and g.fk_c_id = " + fk_c_id;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (result) {
      if(result) {
        return res.json(result.settings)
      }
      res.status(404).send('Invalid group ID')
    }).catch(function (err) {
      res.status(500).send('Internal server error'+ err)
    })
};

exports.getNotificationSettings = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const gid = req.query.gid;

  const query = " select gs.settings from group_settings gs"
    + " inner join groups g on g.id = gs.fk_g_id"
    + " where g.gid = '" + gid + "' and g.fk_c_id = " + fk_c_id;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (result) {
      if(result) {
        if(result.settings){
          if(!result.settings.temperature){
            result.settings.temperature = defaultSettings.defaultAdminTemperatureSetting
          }
          if(!result.settings.fuel){
            result.settings.fuel = defaultSettings.defaultAdminFuelSetting
          }
        }
        return res.json(result.settings)
      }
      res.status(404).send('Invalid group ID')
    }).catch(function () {
      res.status(500).send('Internal server error')
    })
};

exports.updateNotificationSettings = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const gid = req.body.gid;

  const settings = req.body.settings;

  async.waterfall([
    function (cb) {
      // check if admin user

      const query = " SELECT g.is_admin FROM user_group_mappings ugm " +
        " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
        " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result && result.is_admin){
            cb(null, result)
          }else{
            cb({
              code:403,
              msg: 'User does not have permissions'
            })
          }
        }).catch(function (err) {
          cb({
            code:500,
            msg: err
          })
        })
    },
    function (data, cb) {
      const query = " select gs.settings, gs.id from group_settings gs"
        + " inner join groups g on g.id = gs.fk_g_id"
        + " where g.gid = '" + gid + "' and g.fk_c_id = " + fk_c_id;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          if(result) {
            return cb(null, result)
          }
          cb({
            err: 404,
            message: 'Group settings not found'
          })
        }).catch(function () {
          cb({
            err: 500,
            message: 'Internal server error'
          })
        })
    },
    function (groupSettings, cb) {
      groupSettings.settings = merge(groupSettings.settings, settings, {arrayMerge: function (destination, source) {
        return source
      }});
      models.group_setting.update({
        settings: groupSettings.settings
      }, {
        where: {
          id: groupSettings.id
        }
      }).spread(function (updated) {
        if (updated > 0) {
          return cb(null, 'Updated successfully')
        }
      }).catch(function () {
        cb({
          err: 500,
          message: 'Internal server error'
        })
      })
    }
  ], function (err, result) {
    if(err) {
      return res.status(err.code).send(err.message)
    }
    res.status(200).send(result)
  })
};