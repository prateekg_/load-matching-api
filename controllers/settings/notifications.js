const async = require('async');
const _ = require('lodash');

const models = require('../../db/models');
const defaultSettings = require('../../helpers/defaultSettings');


exports.get = function (req, res) {
  const fk_u_id = req.headers.fk_u_id;
  const gid = req.query.gid;

  if (gid) {
    async.auto({
      adminSettings: function (cb) {
        const query = " SELECT gs.settings FROM user_group_mappings ugm"
          + " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active"
          + " INNER JOIN group_settings gs on g.id = gs.fk_g_id"
          + " INNER JOIN users u ON u.id = ugm.fk_u_id AND u.active"
          + " WHERE g.gid = '" + gid + "' AND u.id = " + fk_u_id;

        models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
          .spread(function (data) {
            if (data) {
              return cb(null, data.settings)
            }
            cb({
              code: 404,
              message: 'Settings not available'
            })
          }).catch(function (err) {
            cb({
              code: 500,
              message: 'Internal server error'+err
            })
          })
      },
      userSettings: function (cb) {
        models.user_setting.findOne({
          attributes: ['group_settings'],
          where: {
            fk_u_id: fk_u_id
          },
          raw: true
        }).then(function (notification) {
          if (notification) {
            return cb(null, _.find(notification.group_settings, {gid: gid}))
          }
          cb({
            code: 404,
            message: 'Settings not available'
          })
        }).catch(function (err) {
          console.log(err);
          cb({
            code: 500,
            message: 'Internal server error'
          })
        })
      },
      process: ['adminSettings', 'userSettings', function (result, cb) {
        const admin = result.adminSettings;

        const newObj = {
          "tracking": {
            "enabledAdmin": admin.tracking.enabled,
            "trucks": {
              "speeding": {
                "inAppAdmin": admin.tracking.trucks.speeding.inApp,
                "emailAdmin": admin.tracking.trucks.speeding.email
              },
              "continuousSpeeding": {
                "inAppAdmin": admin.tracking.trucks.continuousSpeeding.inApp,
                "emailAdmin": admin.tracking.trucks.continuousSpeeding.email
              },
              "continuousIdling": {
                "inAppAdmin": admin.tracking.trucks.continuousIdling.inApp,
                "emailAdmin": admin.tracking.trucks.continuousIdling.email
              },
              "cumulativeIdling": {
                "inAppAdmin": admin.tracking.trucks.cumulativeIdling.inApp,
                "emailAdmin": admin.tracking.trucks.cumulativeIdling.email
              },
              "stationary": {
                "inAppAdmin": admin.tracking.trucks.stationary.inApp,
                "emailAdmin": admin.tracking.trucks.stationary.email
              },
              "vehCompliance": {
                "inAppAdmin": admin.tracking.trucks.vehCompliance.inApp,
                "emailAdmin": admin.tracking.trucks.vehCompliance.email
              },
              "vehComplianceReminder": {
                "inAppAdmin": admin.tracking.trucks.vehComplianceReminder.inApp,
                "emailAdmin": admin.tracking.trucks.vehComplianceReminder.email
              }
            },
            "vans": {
              "speeding": {
                "inAppAdmin": admin.tracking.vans.speeding.inApp,
                "emailAdmin": admin.tracking.vans.speeding.email
              },
              "continuousSpeeding": {
                "inAppAdmin": admin.tracking.vans.continuousSpeeding.inApp,
                "emailAdmin": admin.tracking.vans.continuousSpeeding.email
              },
              "continuousIdling": {
                "inAppAdmin": admin.tracking.vans.continuousIdling.inApp,
                "emailAdmin": admin.tracking.vans.continuousIdling.email
              },
              "cumulativeIdling": {
                "inAppAdmin": admin.tracking.vans.cumulativeIdling.inApp,
                "emailAdmin": admin.tracking.vans.cumulativeIdling.email
              },
              "stationary": {
                "inAppAdmin": admin.tracking.vans.stationary.inApp,
                "emailAdmin": admin.tracking.vans.stationary.email
              },
              "vehCompliance": {
                "inAppAdmin": admin.tracking.vans.vehCompliance.inApp,
                "emailAdmin": admin.tracking.vans.vehCompliance.email
              },
              "vehComplianceReminder": {
                "inAppAdmin": admin.tracking.vans.vehComplianceReminder.inApp,
                "emailAdmin": admin.tracking.vans.vehComplianceReminder.email
              }
            }
          }
        };

        if(!admin.fuel) {
          console.log("default admin fuel setting");
          //Fuel setting are not present in db for this group
          newObj['fuel'] = defaultSettings.defaultAdminFuelSetting
        } else {
          console.log("admin fuel setting from db");
          newObj['fuel'] = {
            "enabledAdmin": admin.fuel.enabled,
            "trucks": {
              "rapidDrop": {
                "inAppAdmin": admin.fuel.trucks.rapidDrop.inApp,
                "emailAdmin": admin.fuel.trucks.rapidDrop.email
              },
              "refuelingIncident": {
                "inAppAdmin": admin.fuel.trucks.refuelingIncident.inApp,
                "emailAdmin": admin.fuel.trucks.refuelingIncident.email
              }
            },
            "vans": {
              "rapidDrop": {
                "inAppAdmin": admin.fuel.vans.rapidDrop.inApp,
                "emailAdmin": admin.fuel.vans.rapidDrop.email
              },
              "refuelingIncident": {
                "inAppAdmin": admin.fuel.vans.refuelingIncident.inApp,
                "emailAdmin": admin.fuel.vans.refuelingIncident.email
              }
            }
          }
        }

        if(!admin.temperature){
          newObj.temperature = defaultSettings.defaultAdminTemperatureSetting
        }else{
          const at = admin.temperature;
          newObj.temperature = {
            "enabledAdmin": at.enabled,
            "trucks": {
              "rise": {
                "level": at.trucks.rise.level,
                "trigger": at.trucks.rise.trigger,
                "inApp": at.trucks.rise.inApp
              },
              "drop": {
                "level": at.trucks.drop.level,
                "trigger": at.trucks.drop.trigger,
                "inApp": at.trucks.drop.inApp
              }
            },
            "vans": {
              "rise": {
                "level": at.vans.rise.level,
                "trigger": at.vans.rise.trigger,
                "inApp": at.vans.rise.inApp
              },
              "drop": {
                "level": at.vans.drop.level,
                "trigger": at.vans.drop.trigger,
                "inApp": at.vans.drop.inApp
              }
            }
          }
        }

        _.each(admin, function (value, key) {
          if (key !== 'tracking' && key !== 'fuel') {
            newObj[key] = {};
            _.each(value, function (value2, key2) {
              if (key2 === 'enabled') {
                newObj[key][key2 + 'Admin'] = value2
              } else {
                newObj[key][key2] = {};
                _.each(value2, function (value3, key3) {
                  if (key3 === 'inApp' || key3 === 'email') {
                    newObj[key][key2][key3 + 'Admin'] = {};
                    newObj[key][key2][key3 + 'Admin'] = value3
                  }                                    
                })
              }
            })
          } 
        });

        result.userSettings = _.merge(result.userSettings, newObj);
        cb(null, result.userSettings)
      }]
    }, function (err, result) {
      if(err) {
        return res.status(err.code).send(err.message)
      }
      res.json(result.process)
    })

  } else {
    models.user_setting.findOne({
      attributes : ['settings'],
      where : {
        fk_u_id : fk_u_id
      }
    }).then(function (notification) {
      if(notification) {
        //Compare stored setting with default setting to find missing notification 
        //If notification is not found then send default false keys to front end
                
        _.each(defaultSettings.user, function (value, key) {
          let found = false;
          _.each(notification.settings, function(val, k) {
                       
            //Check if key matches and inner keys also matches
            if(k === key && _.isEqual(_.keys(value).sort(), _.keys(val).sort())){
              found = true;

            }

          });
          if(!found) {
            let string = JSON.stringify(value);
            string = string.replace(/true/g, "false");
            notification.settings[key] = JSON.parse(string)
          }

        });
        return res.json(notification.settings)
      }
      return res.status(400).send('Notifications not available')

    }).catch(function (err) {
      res.status(500).send('Internal server error'+err)
    })
  }
};

exports.update = function (req, res) {
  const fk_u_id = req.headers.fk_u_id;

  const data = req.body;

  models.user_setting.findOne({
    attributes: ['settings', 'group_settings'],
    where: {
      fk_u_id: fk_u_id
    }
  }).then(function (user) {
    if (!user) {
      return res.status(404).send('Notification setting not available')
    }

    if (data.gid) {
      const index = _.findIndex(user.group_settings, {gid: data.gid});
      user.group_settings[index] = _.merge(user.group_settings[index], data.settings);
      models.user_setting.update({
        group_settings: user.group_settings
      }, {
        where: {
          fk_u_id: fk_u_id
        }
      }).then(function () {
        return res.send('Notification settings updated successfully')
      }).catch(function (err) {
        return res.status(500).send('Internal server error'+err)
      });
    } else {
      user.settings = _.merge(user.settings, data);
      models.user_setting.update({
        settings: user.settings
      }, {
        where: {
          fk_u_id: fk_u_id
        }
      }).then(function () {
        return res.send('Notification settings updated successfully')
      }).catch(function (err) {
        console.error(err);
        return res.status(500).send('Internal server error' + err)
      });
    }
  }).catch(function (err) {
    res.status(500).send('Internal server error'+err)
  })
};
