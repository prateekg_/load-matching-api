/**
 * Created by numadic on 03/08/17.
 */
const request = require('request');
const async = require('async');
const RazorPay = require('razorpay');

const env = process.env.NODE_ENV || 'development';
const config = require('../../config/index')[env];

const razorpay = new RazorPay({
  key_id: config.razorpay.key,
  key_secret: config.razorpay.secret
});


function updateTransactionInWAD(params, callback) {

  const request_options = {
    uri: config.server_address.wad_server() + params.uri,
    method: params.method,
    headers: {
      'Authorization': 'bearer ' + config.tokens.numadicAdmin
    },
    qs: params.query_params,
    json: params.json
  };

  request(request_options, function(error, response, body){
    if(error) {
      if(error.code === 'ECONNREFUSED' || error.code === 'ECONNRESET') {
        callback({code: 503, msg: 'Service unavailable'})
      } else {
        callback({code: 500, msg: 'Internal error'})
      }
    }else{
      try{
        body = JSON.parse(body)
      }catch(e){
        console.log("PARSE ERROR : ", e)
      }finally {
        callback(null, body)
      }
    }
  });
}

exports.getInvoices = function (req, res) {

  req.headers['content-length'] = undefined;

  async.auto({
    invoices: function(cb){
      const request_options = {
        uri: config.server_address.wad_server() + '/invoices',
        method: req.method,
        headers: {
          'fk-c-id': req.headers.fk_c_id,
          'Authorization': 'bearer ' + config.tokens.numadicAdmin
        },
        qs: req.query,
        json: true
      };

      request(request_options, function(error, response, body){
        if(error) {
          if(error.code === 'ECONNREFUSED' || error.code === 'ECONNRESET') {
            cb({code:503, msg: 'Service unavailable'});
          } else {
            cb({code:500, msg: 'Internal error'});
          }
        }
        cb(null, body)
      });
    },
    pending_amount: function(cb){
      const request_options = {
        uri: config.server_address.wad_server() + '/invoices/pending_amount',
        method: req.method,
        headers: {
          'fk-c-id': req.headers.fk_c_id,
          'Authorization': 'bearer ' + config.tokens.numadicAdmin
        },
        qs: req.query,
        json: true
      };

      request(request_options, function(error, response, body){
        if(error) {
          if(error.code === 'ECONNREFUSED' || error.code === 'ECONNRESET') {
            cb({code:503, msg: 'Service unavailable'});
          } else {
            cb({code:500, msg: 'Internal error'});
          }
        }
        cb(null, body)
      });
    }
  }, function(err, results){
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      let data = results.invoices;
      data.pending_amount = results.pending_amount.amount;
      res.json(data)
    }
  })
};

exports.authorizePayment = function (req, res) {

  /* :
     * 1: Get transaction_id, invoice's ids from WV
     * 2: update the db via WAD apis. (complete_transaction) status=authorized
     * 3: Get total amount of all invoices from WAD
     * 4: capture the amount
     * 5: update the db via WAD apis. (complete_transaction) status=completed
     */

  const data = req.body;
  data.comments = 'NA';

  if(!data.transaction_id){
    return res.status(400).send("Enter transaction id")
  }else if(!data.invoice_ids || data.invoice_ids.length === 0){
    return res.status(400).send("Enter invoice id")
  }else if(!data.payment_date){
    return res.status(400).send("Enter payment date")
  }else if(!data.payment_amount){
    return res.status(400).send("Enter payment amount")
  }else{

    async.auto({
      init_payment: function (cb) {

        data.payment_status = 'initiated';
        const params = {
          uri: '/invoices/initiate_payment',
          method: 'POST',
          query_params: {transaction_tracker: data.transaction_id},
          json: data
        };

        updateTransactionInWAD(params, function (err, success) {
          console.log("INITIATION : ", {
            err: err,
            success: success
          });
          cb(null,"INTITATED")
        })
      },
      authorize: ['init_payment',function (results, cb) {
        console.log("INTO AUTHORIZE");
        data.payment_status = 'authorized';
        data.transaction_tracker = data.transaction_id;

        const params = {
          uri: '/invoices/complete_transaction',
          method: 'PUT',
          json: data
        };

        updateTransactionInWAD(params, function (err, success) {
          console.log("AUTHORIZE : ", {
            err: err,
            success: success
          });
          cb(null,'AUTHORIZED')
        })
      }],
      invoice_total: ['authorize', function (results, cb) {
        console.log("INTO INVOICE TOTAL");
        const params = {
          uri: '/invoices/validate',
          method: 'GET',
          query_params: {
            transaction_tracker: data.transaction_id,
            invoice_ids: data.invoice_ids
          },
          json: data
        };

        updateTransactionInWAD(params, function (err, success) {
          console.log("GET AMOUNT FROM WAD : ", success);
          if(success && success.invoice_total && success.invoice_total > 0){
            cb(null, success.invoice_total)
          }else{
            cb({
              code: 400,
              msg: 'Invalid amount'
            })
          }
        })
      }],
      capture_amount:['invoice_total', function (results, cb) {
        console.log("INTO CAPTURE");
        const amount = results.invoice_total * 100; // convert amount rupees into paise
        data.transaction_tracker = data.transaction_id;

        const params = {
          uri: '/invoices/complete_transaction',
          method: 'PUT',
          json: data
        };

        razorpay.payments.capture(data.transaction_id, amount).then(function (capture) {

          data.payment_status = "completed";
          data.comments = '';
          data.payment_mode = capture.method;
          params.json = data;

          updateTransactionInWAD(params, function () {
            cb(null, 'success')
          })
        }).catch(function (err) {

          const err_msg = err.error.description;

          data.payment_status = "failed";
          data.comments = err_msg;
          params.json = data;

          updateTransactionInWAD(params, function () {
            cb({
              code: 400,
              msg: err_msg
            })
          })
        })
      }]
    }, function (err, results) {
      if(err){
        res.status(err.code).send(err.msg)
      }else{
        res.send(results.capture_amount)
      }
    })
  }
};