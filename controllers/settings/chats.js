const models = require('../../db/models');

exports.getUserChat = function (req, res) {

  const limit = req.query.limit || 10;
  const fk_user_id = req.headers.fk_u_id;
  const fk_sender_id = req.query.fk_sender_id;
  const last_msg_id = req.query.last_msg_id;

  let last_msg_filter_condition = "";

  if(!Number(fk_sender_id)){
    return res.status(400).send("Enter sender ID")
  }

  if(Number(last_msg_id)){
    last_msg_filter_condition = ` AND id < ${last_msg_id}`;
  }

  let query = ` SELECT id, fk_sender_id, fk_reciever_id, tis, msg FROM chats
         WHERE fk_sender_id IN (${fk_sender_id}, ${fk_user_id})
         AND fk_reciever_id IN (${fk_sender_id}, ${fk_user_id}) ${last_msg_filter_condition}
         ORDER BY id DESC LIMIT ${limit}`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (result) {
      res.json(result)
    }).catch(function () {
      res.status(500).send()
    })
};
