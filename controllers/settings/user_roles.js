const async = require('async');
const _ = require('lodash');
const models = require('../../db/models');
const UTILS = require('../../helpers/util');


exports.getAllUserRoles = function (req, res) {
  let search_keyword = req.query.q;
  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = limit * page;
  let search_condition = "";

  if (search_keyword) {
    search_keyword = search_keyword.toLowerCase();
    search_condition = " AND ( lower(ur.name) like '%" + search_keyword + "%' )"
  }

  const query = "SELECT ur.id, ur.name, description, count(u.id)::INT total_users, COUNT(ur.id) OVER()::INT AS count" +
    " FROM user_roles ur" +
    " LEFT JOIN user_role_mappings urm on urm.fk_user_role_id = ur.id AND urm.active" +
    " LEFT JOIN users u on u.id = urm.fk_user_id AND u.active " +
    " WHERE ( ur.fk_c_id = :fk_c_id OR ur.name = 'system-admin' )" + search_condition +
    " GROUP BY ur.id" +
    " OFFSET :offset LIMIT :limit";

  models.sequelize.query(query, { replacements: { fk_c_id: fk_c_id, limit: limit, offset: offset }, type: models.sequelize.QueryTypes.SELECT })
    .then(function (result) {
      let tot_rows = 0;
      result.forEach(function (i) {
        tot_rows = i.count;
        delete i.count
      });
      res.json({
        count: tot_rows,
        data: result
      })
    }).catch(function () {
      res.status(500).send()
    })
};

exports.getSingleUserRole = function (req, res) {

  const id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;

  if (!Number(id)) return res.status(400).send("Invalid ID");

  async.auto({
    company_features: function (cb) {
      const query = `SELECT * FROM company_plans_vw cp WHERE cp.fk_c_id = :fk_c_id;`
      models.sequelize.query(query, { replacements: { fk_c_id: fk_c_id }, type: models.sequelize.QueryTypes.SELECT })
        .then(function (result) {
          let final_data = []
          result.forEach(function (i) {
            final_data = _.concat(final_data, i.features)
          })
          cb(null, _.uniq(_.compact(final_data)))
        }).catch(function (err) {
          cb(err)
        })
    },
    role_features: ['company_features', function (results, cb) {
      const query = `SELECT ur.id, ur.name, description, config
         FROM user_roles ur
         LEFT JOIN user_role_mappings urm on urm.id = ur.id
         LEFT JOIN users u on u.id = urm.fk_user_id
         LEFT JOIN company_plans_vw cp on cp.fk_c_id = u.fk_c_id
         WHERE ( ur.fk_c_id = :fk_c_id OR ur.name = 'system-admin' ) AND ur.id = :id
         LIMIT 1`;

      models.sequelize.query(query, { replacements: { fk_c_id: fk_c_id, id: id }, type: models.sequelize.QueryTypes.SELECT })
        .spread(function (result) {
          // return cb(null, result)
          let company_features = results.company_features
          try {
            const role_features = [];
            let role_config = !!(result.config && result.config.featureList);
            if (result) {
              result.actions = result.name === 'system-admin' ? 'full-access' : 'read-only';
              // result.company_features = _.uniq(_.flatten(result.company_features));
              company_features.forEach(function (i) {
                if (role_config) {
                  const matched = _.find(result.config.featureList, { feature: i });
                  if (matched) {
                    role_features.push(matched)
                  } else {
                    role_features.push({ feature: i, enabled: false })
                  }
                } else {
                  role_features.push({ feature: i, enabled: true })
                }
              });

              if (!role_config) {
                result.config = {}
              }
              result.config.featureList = _.uniqBy(role_features, 'feature');
            }
          } catch (e) {
            console.log("ERROR : ", e)
          }
          cb(null, result)
        }).catch(function (err) {
          cb(err)
        })
    }]
  }, function (err, results) {
    if (err) {
      return res.status(500).send("Internal server error")
    } else {
      res.json(results.role_features)
    }
  })
};

exports.createUserRole = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const data = req.body;
  let validator = { code: null, msg: null };

  if (data) {
    if (!data.name) {
      validator = { code: 400, msg: "enter name" }
    } else if (!data.config) {
      validator = { code: 400, msg: "enter config" }
    } else if (data.name && data.name.toLowerCase() === 'system-admin') {
      validator = { code: 400, msg: "Role name cannot be 'system-admin'" }
    }
  } else {
    validator = { code: 400, msg: "Invalid data" }
  }

  if (validator.code) {
    return res.status(validator.code).send(validator.msg)
  }

  models.user_roles.findOrCreate({
    where: {
      fk_c_id: fk_c_id,
      name: { $ilike: data.name }
    },
    defaults: {
      fk_c_id: fk_c_id,
      name: data.name,
      description: data.description,
      config: data.config
    }
  }).spread(function (user_role, created) {
    if (created) {
      res.json("user role created")
    } else {
      res.status(409).send("User role with same name exists")
    }
  }).catch(function () {
    res.status(500).send()
  })
};

exports.updateUserRole = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const data = req.body;
  let validator = { code: null, msg: null };

  if (!data.id) {
    validator = { code: 400, msg: "Enter role id" }
  }

  if (data) {
    if (data.name && data.name.toLowerCase() === 'system-admin') {
      validator = { code: 400, msg: "Role name cannot be 'system-admin' or Role with this name could not be updated" }
    }
  }

  if (validator.code) {
    return res.status(validator.code).send(validator.msg)
  }

  models.user_roles.update(_.pick(data, ['name', 'description', 'config']), {
    where: {
      fk_c_id: fk_c_id,
      id: data.id
    }
  }).spread(function (updated) {
    if (updated > 0) {
      res.status(200).send("User role updated successfully")
    } else {
      res.status(404).send('Invalid user role id')
    }
  }).catch(function () {
    res.status(500).send('Internal server error')
  })
};

/**
 * @description updates users roles.
 * @param req
 * @param res
 */
exports.createUserRoleMapping = function (req, res) {

  let data = req.body;

  /**
     * 1. update all user role mappings to false
     * 2. create or update user role mappings
     **/

  if (!data) {
    return res.status(400).send("Enter fk_user_id and fk_role_ids")
  }
  else if (!data.fk_user_id) {
    return res.status(400).send("Enter fk_user_id")
  } else if (data.fk_role_ids.length === 0) {
    return res.status(400).send("Enter fk_role_ids")
  }

  async.auto({
    disable: function (cb) {
      models.user_role_mapping.update({ active: false }, {
        where: {
          fk_user_id: data.fk_user_id
        }
      }).spread(function () {
        cb()
      }).catch(function () {
        cb({ code: 500, msg: 'Internal server error' })
      })
    },
    mapping: ['disable', function (results, cb) {
      async.each(data.fk_role_ids, function (fk_role_id, callback1) {
        const user_role_mapping_object = {
          fk_user_id: data.fk_user_id,
          fk_user_role_id: fk_role_id
        };

        models.user_role_mapping.findOrCreate({
          where: user_role_mapping_object,
          defaults: user_role_mapping_object
        }).spread(function (user_role, created) {
          if (created) {
            callback1()
          } else {
            user_role.active = true;
            user_role.save();
            callback1()
          }
        }).catch(function (err) {
          callback1({ code: 500, msg: 'Internal server error', err: err })
        })
      }, function (err) {
        if (err) {
          cb({ code: 500, send: 'Internal server error', err: err })
        }
        cb()
      })
    }]
  }, function (err) {
    if (err) {
      res.status(err.code).send(err.msg)
    } else {
      res.json("User successfully mapped to the Role")
    }
  })
};

exports.deleteUserRoleMapping = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const data = req.body;

  async.auto({
    validate: function (cb) {
      // Validate if role and user belongs to the company
      if (data) {
        if (data.fk_user_id && data.fk_user_role_id) {
          models.sequelize.query(
            "SELECT u.id user_id, ur.id role_id FROM " +
            "(SELECT id FROM users WHERE id = :fk_user_id AND fk_c_id = :fk_c_id) u," +
            "(SELECT id FROM user_roles WHERE id = :fk_user_role_id AND (fk_c_id = :fk_c_id OR fk_c_id IS NULL)) ur",
            {
              replacements: { fk_c_id: fk_c_id, fk_user_id: data.fk_user_id, fk_user_role_id: data.fk_user_role_id },
              type: models.sequelize.QueryTypes.SELECT
            })
            .spread(function (result) {
              if (result && result.user_id && result.role_id) {
                cb()
              } else {
                cb({ code: 400, msg: 'fk_user_id or fk_user_role_id not found for your company' })
              }
            }).catch(function (err) {
              cb({ code: 500, msg: 'Internal server error', err: err })
            })
        } else {
          cb({ code: 400, msg: 'Empty fk_user_id or fk_user_role_id' })
        }
      } else {
        cb({ code: 400, msg: 'Empty data' })
      }
    },
    mapping: ['validate', function (results, cb) {
      const user_role_mapping_object = {
        fk_user_id: data.fk_user_id,
        fk_user_role_id: data.fk_user_role_id,
        active: true
      };

      models.user_role_mapping.update({ active: false }, {
        where: user_role_mapping_object
      }).spread(function (updated) {
        if (updated) {
          cb()
        } else {
          cb({ code: 409, msg: "User and role mapping could not be deleted" })
        }
      }).catch(function (err) {
        cb({ code: 500, msg: 'Internal server error', err: err })
      })
    }]
  }, function (err) {
    if (err) {
      res.status(err.code).send(err.msg)
    } else {
      res.json("User successfully unmapped from the Role")
    }
  })
};

exports.updateUserRoleMapping = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const data = req.body;
  let fk_role_id = Number(data.fk_role_id);
  let validator = { code: null, msg: null };

  if (!fk_role_id) {
    validator = { code: 400, msg: "Enter role id" }
  }

  if (validator.code) {
    return res.status(validator.code).send(validator.msg)
  }

  async.auto({
    validate: function (cb) {
      models.sequelize.query(
        "SELECT id FROM user_roles WHERE id = :id AND (fk_c_id = :fk_c_id OR fk_c_id IS NULL);",
        { replacements: { id: fk_role_id, fk_c_id: fk_c_id }, type: models.sequelize.QueryTypes.SELECT })
        .spread(function (result) {
          if (result) {
            cb()
          } else {
            cb({ code: 400, msg: 'Invalid role ID' })
          }
        }).catch(function (err) {
          cb({ code: 500, msg: 'Internal server', err: err })
        })
    },
    create_mappings: ['validate', function (results, cb) {
      const user_ids = UTILS.processIntegerArray(data.added_users);
      async.each(user_ids, function (user_id, callback) {
        const user_role_mapping_object = {
          fk_user_id: user_id,
          fk_user_role_id: fk_role_id,
          active: true
        };
        models.user_role_mapping.findOrCreate({
          where: user_role_mapping_object,
          defaults: user_role_mapping_object
        }).spread(function () {
          callback()
        }).catch(function (err) {
          callback({ code: 500, msg: 'Internal server error', err: err })
        })
      }, function (err) {
        cb(err, null)
      })
    }],
    update_mappings: ['validate', function (results, cb) {
      const user_ids = UTILS.processIntegerArray(data.removed_users);
      async.each(user_ids, function (user_id, callback) {
        models.user_role_mapping.update({ active: false }, {
          where: {
            fk_user_id: user_id,
            fk_user_role_id: fk_role_id,
            active: true
          }
        }).spread(function () {
          callback()
        }).catch(function (err) {
          callback({ code: 500, msg: 'Internal server error', err: err })
        })
      }, function (err) {
        cb(err, null)
      })
    }]
  }, function (err) {
    if (err) {
      res.status(err.code).send(err.msg)
    } else {
      res.json("Success")
    }
  })
};

exports.getAllUsersOfRole = function (req, res) {

  const fk_role_id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = limit * page;
  let search_condition = "";
  let search_keyword = req.query.q;

  let validator = { code: null, msg: null };

  if (!Number(fk_role_id)) {
    validator = { code: 400, msg: "Enter role id" }
  }

  if (validator.code) {
    return res.status(validator.code).send(validator.msg)
  }

  if (search_keyword) {
    search_keyword = search_keyword.toLowerCase();
    search_condition = " AND ( lower(u.name) like '%" + search_keyword + "%' OR lower(u.email) like '%" + search_keyword + "%' )";
  }

  const query = "SELECT u.id, u.name, u.email, coalesce(urm.active, false) selected, COUNT(u.id) OVER()::INT AS count " +
    " FROM users u " +
    " LEFT JOIN user_role_mappings urm on urm.fk_user_id = u.id AND urm.active AND urm.fk_user_role_id = :fk_role_id" +
    " WHERE u.fk_c_id = :fk_c_id AND u.active " + search_condition +
    " ORDER BY urm.active" +
    " OFFSET :offset LIMIT :limit";

  models.sequelize.query(query, { replacements: { fk_c_id: fk_c_id, fk_role_id: fk_role_id, limit: limit, offset: offset }, type: models.sequelize.QueryTypes.SELECT })
    .then(function (result) {
      let tot_rows = 0;
      result.forEach(function (i) {
        tot_rows = i.count;
        delete i.count
      });
      res.json({
        count: tot_rows,
        data: result
      })
    }).catch(function () {
      res.status(500).send()
    })
};