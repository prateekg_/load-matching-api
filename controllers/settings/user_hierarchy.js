const async = require('async');
const _ = require('lodash');
const models = require('../../db/models');
const UTILS = require('../../helpers/util');
const jsonIterator = require('object-recursive-iterator');

exports.getUserHierarchyTreeV1 = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;

  async.auto({
    tree: function (cb) {
      let query = "SELECT uht.tree"+
                " FROM user_hierarchy_trees uht "+
                " WHERE uht.fk_c_id = :fk_c_id;";
      models.sequelize.query(query, {replacements: {fk_c_id: fk_c_id}, type: models.sequelize.QueryTypes.SELECT})
        .spread(function (result) {
          cb(null, result || {tree: {hierarchy: []}})
        }).catch(function (err) {
          cb({code: 500, msg: 'Internal error', err: err})
        })
    },
    users: function (cb) {
      let query = "SELECT u.id, u.name, u.email, COALESCE(json_agg(ur.name) FILTER (WHERE ur.id IS NOT NULL), '[]') as roles" +
                " FROM users u " +
                " LEFT JOIN user_role_mappings urm on urm.fk_user_id = u.id and urm.active" +
                " LEFT JOIN user_roles ur on ur.id = urm.fk_user_role_id" +
                " WHERE u.fk_c_id = :fk_c_id and u.active" +
                " GROUP BY u.id;";

      models.sequelize.query(query, {replacements: {fk_c_id: fk_c_id}, type: models.sequelize.QueryTypes.SELECT})
        .then(function (users) {
          cb(null, users)
        }).catch(function (err) {
          cb({code: 500, msg: 'Internal error', err: err})
        })
    },
    update_tree: ['tree', 'users', function (results, cb) {
      let users = results.users;
      let tree = results.tree;
      jsonIterator.forAll(tree, function (path, key, obj) {
        if(obj["id"] && obj["data"]){
          let user = _.find(users, {id: obj["id"]});
          if(user){
            obj["data"] = user
          }
        }
      });
      cb(null, tree)
    }]
  }, function (err, results) {
    if(err){
      return res.status(err.code).send(err.msg)
    }else{
      res.json(results.update_tree)
    }
  })
};

exports.createOrUpdateUserHierarchyTree = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  let body = req.body;
  if(!body){
    return res.status(400).send('Empty body')
  }

  if(body && !body.tree){
    return res.status(400).send('Empty tree')
  }

  models.user_hierarchy_tree.update(body, {where: { fk_c_id: fk_c_id}})
    .spread(function (result) {
      if(result === 0){
        body.fk_c_id = fk_c_id;
        models.user_hierarchy_tree.create(body).then(function () {
          res.json("Successfully Created")
        }).catch(function () {
          res.status(500).send("Internal error creating Tree")
        })
      }else{
        res.json("Successfully Updated")
      }
    }).catch(function () {
      res.status(500).send("Internal error updating Tree")
    })

};

exports.getUserNotAssignedToHierarchy = function (req, res) {
  let search_keyword = req.query.q;
  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = limit * page;
  let search_condition = "";
  const search_cols = req.query.search;

  if(search_keyword){
    search_keyword = search_keyword.toLowerCase();
    search_condition = " AND ( lower(u.name) like '%"+ search_keyword +"%' OR lower(u.email) like '%"+ search_keyword +"%')"
  }

  let search_col_condition = UTILS.processTableColumnSearch(
    search_cols,
    [{
      db: 'u.name',
      client: 'name'
    }, {
      db: 'u.email',
      client: 'email'
    }]
  );

  async.auto({
    assigned_users: function (cb) {
      // Get assigned_users from the user hierarchy tree
      let query = "SELECT uht.tree"+
                " FROM user_hierarchy_trees uht "+
                " WHERE uht.fk_c_id = :fk_c_id;";
      models.sequelize.query(query, {replacements: {fk_c_id: fk_c_id}, type: models.sequelize.QueryTypes.SELECT})
        .spread(function (tree) {
          let users = [];
          jsonIterator.forAll(tree, function (path, key, obj) {
            users.push(obj.id)
          });
          cb(null, _.uniq(_.compact(users)))
        }).catch(function (err) {
          cb({code: 500, msg: 'Internal error', err: err})
        })
    },
    unassigned_users: ['assigned_users', function (results, cb) {
      let assigned_users = results.assigned_users;
      let assigned_user_condition = assigned_users.length > 0 ? " and u.id not in ("+ assigned_users +")" : "";

      let query = "SELECT u.id, u.name, u.email, COALESCE(json_agg(ur.name) FILTER (WHERE ur.id IS NOT NULL), '[]') as roles," +
                " count(*) OVER()::INT AS count"+
                " FROM users u"+
                " LEFT JOIN user_role_mappings urm on urm.fk_user_id = u.id and urm.active"+
                " LEFT JOIN user_roles ur on ur.id = urm.fk_user_role_id"+
                " WHERE u.fk_c_id = :fk_c_id and u.active " + search_condition + search_col_condition + assigned_user_condition +
                " GROUP BY u.id" +
                " OFFSET :offset LIMIT :limit";

      models.sequelize.query(query, {replacements: {fk_c_id: fk_c_id, limit: limit, offset: offset}, type: models.sequelize.QueryTypes.SELECT})
        .then(function (result) {
          let tot_rows = 0;
          result.forEach(function (i) {
            tot_rows = i.count;
            delete i.count
          });
          cb(null, { count: tot_rows, data: result })
        }).catch(function (err) {
          cb({code:500, msg:'Internal error',err:err})
        })
    }]
  }, function (err, results) {
    if(err){
      return res.status(err.code).send(err.msg)
    }
    res.json(results.unassigned_users)
  })
};