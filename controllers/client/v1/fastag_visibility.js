const async = require('async');
const _ = require('lodash');
const models = require('../../../db/models/index');
const request = require('request');
const moment = require('moment');
const API_LIMITER = require('../../../helpers/api_limiter');

const TOLL_PLUS_SERVER = 'https://fastaglogin.icicibank.com/ISRCustVehicleAPI';
const TOLL_PLUS_LOCATION_URL = '/CustomerVehicle/GetTripDetails';

const API_HEADERS = {
    APIClient_ID: "90000004",
    API_KEY: "590C18F4EAFF63BA535C9E8F142D15629287C69CA08AB7FA82F3F00CF6388AF85BE227",
    "Content-Type": "application/json"
}

function processAndCleanData(data, callback) {
    let processedData = [];
    async.each(data, function (i, cb) {
        let a = _.pick(i, ['TransactionDateTime', 'LaneCode', 'PlazaCode', 'PlazaName']);
        a.Lat = null;
        a.Lng = null;
        a.Address = null;

        let query = `SELECT addr, lat, lon FROM pois WHERE typ = 6 
        AND ( plaza_code = '${parseInt(i.PlazaCode)}' OR plaza_code = '${i.PlazaCode}') 
        LIMIT 1;`;
        models.sequelize.query(query, {
            type: models.sequelize.QueryTypes.SELECT
        }).spread(function (result) {
            if (result) {
                a.Lat = result.lat;
                a.Lng = result.lon;
                a.Address = result.addr;
            }
            processedData.push(a)
            cb()
        }).catch(function (err) {
            processedData.push(a)
            cb()
        })
    }, function (err) {
        callback(err, _.orderBy(processedData, ['TransactionDateTime'], ['desc']))
    })
}

exports.getLocations = function (req, res) {
    const fk_c_id = req.headers['fk-c-id'] || req.headers.fk_c_id;
    let StartTripDate = req.body.StartTripDate,
        EndTripDate = req.body.EndTripDate,
        isLastKnownLocationRequest = false;

    if (!req.body.VehicleNumber) {
        return res.status(400).send({ msg: "Enter Vehicle Number" })
    }

    if (!StartTripDate || !EndTripDate) {
        //Process for last know location
        isLastKnownLocationRequest = true;
        req.body.StartTripDate = moment().subtract(38, 'days');
        req.body.EndTripDate = moment().subtract(1, 'minute');
    }

    request.post({
        uri: TOLL_PLUS_SERVER + TOLL_PLUS_LOCATION_URL,
        headers: API_HEADERS,
        json: req.body
    }, function (error, response, body) {
        let data = [];
        if (error) {
            return res.status(response.statusCode).json(body)
        } else {
            if (body.TripDetails && body.TripDetails.length > 0) {
                if (isLastKnownLocationRequest) {
                    data = [_.maxBy(body.TripDetails, 'TransactionDateTime')]
                } else {
                    data = body.TripDetails
                }
                // Process and Clean Data
                processAndCleanData(data, function (err, result) {
                    if (err) {
                        return res.status(500).send('Internal server error')
                    } else {
                        // API_LIMITER.UPDATE(fk_c_id)
                        res.set({ 'Cache-Control': 'max-age=3600' })
                        return res.json({ data: result })
                    }
                })
            } else {
                return res.status(400).send(body)
            }
        }
    })
}