const async = require('async');
const _ = require('lodash');
const models = require('../../../db/models/index');
const request = require('request');
const polyline = require('polyline');
const key = 'AIzaSyAccYJQ4YgjCaKpvIWwzbhdxfY04pniprM'; 

exports.getTollBoothList = function (req, res) {

  const slat = req.query.slat;
  const slon = req.query.slon;
  const dlat = req.query.dlat;
  const dlon = req.query.dlon;
  let name = req.query.name;
  const vehicle_type = req.query.vtype || '';
  let type;

  switch (vehicle_type.toLowerCase()) {
  case 'car':
  case 'jeep':
  case 'van':
  case 'lmv':
    type = 'Car/Jeep/Van';
    break;
  case 'lcv':
    type = 'lcv';
    break;
  case 'bus':
  case 'truck':
    type = 'Bus/Truck';
    break;
  case '2axle':
  case '3axle':
    type= 'Upto 3 Axle Vehicle';
    break;
  case '4axle':
  case '5axle':
  case '6axle':
    type = '4 to 6 Axle';
    break;
  case 'hcm':
  case 'eme': 
    type = 'HCM/EME';
    break;
  case '7axle': 
    type = '7 or more Axle';
    break
  }
    
  if (!type) {
    return res.status(400).send({
      error: "Bad request",
      message: "Please enter correct option for vehicle type",
      code: 400
    })
  } else {
    if (!name || (name && name === '')) {
      return res.status(400).send({
        error: "Bad request",
        message: "Please enter valid location for source",
        code: 400
      })
    }
  }


  if (slat && slon && dlat && dlon) {

    let reqData;

    let url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + slat + ',' + slon + '&destination=' + dlat + ',' + dlon + '&sensor=false';
    request(url, function (error, response, body) {
      if (!error && response.statusCode === 200) {
        reqData = JSON.parse(body);

        if (reqData.status === 'OVER_QUERY_LIMIT') {

          url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + slat + ',' + slon + '&destination=' + dlat + ',' + dlon + '&key=' + key;
          request(url, function (error, response, body) {

            if (!error && response.statusCode === 200) {
              reqData = JSON.parse(body);

              if (reqData.status === 'OK') {
                findTolls(reqData, name, type, vehicle_type, res)
              }
              else if (reqData.status === 'OVER_QUERY_LIMIT') {
                return res.status(404).send({
                  error: "Not found",
                  message: "Google API quota exceeded",
                  code: 404
                })
              }
              else {
                return res.status(404).send({
                  error: "Not found",
                  message: "No tollbooth found within radius for given location",
                  code: 404
                })
              }
            }
          })
                        
        }
        else if (reqData.status === 'OK') {
          findTolls(reqData, name, type, vehicle_type, res)
        }
        else {
          return res.status(404).send({
            error: "Not found",
            message: "No tollbooth found within radius for given location",
            code: 404
          })
        }
                
      }
            

            
    })

  } 
  else {
    const whereCondition = name ? " p.name ilike $name" : "ST_DWithin(geo_loc, ST_SetSRID(ST_Point($slon,$slat),4326)::geography, 1000,false )";
    const query = " SELECT p.name, p.lat, p.lon, p.fees, s.name as state FROM pois p " +
      " INNER JOIN poi_types pt ON pt.id = p.typ " +
      " INNER JOIN states s ON s.id = p.fk_state_id" +
      " WHERE pt.type = 'Toll Booth' AND " + whereCondition;
    models.sequelize.query(query, {
      bind: {
        name: '%'+name+'%',
        slon: slon,
        slat: slat
      },
      type: models.sequelize.QueryTypes.SELECT }).then(function (result) {

      if (result.length === 0) {
        return res.status(404).send({
          error: "Not found",
          message: "No tollbooth found within radius for given location",
          code: 404
        })
      }


      async.eachOf(result, function (tollPlaza, index, cb) {
        const fees = [];
        _.each(tollPlaza.fees, function (i) {
          if (i.typeOfVehicle === type)
            fees.push(_.pick(i, ['singleJourney', 'returnJourney', 'monthlyPass']))
        });
        tollPlaza.fees = fees;
        cb()
      }, function () {
        res.json({
          vehicleType: vehicle_type,
          tollbooths: result
        })
      })
    }).catch(function (err) {
      res.status(500).send({
        error: "Internal server error",
        message: err,
        code: 500
      })
    })   
  }

   
};

function findTolls(reqData, name, type, vehicle_type, res) {

  const route = reqData.routes[0].overview_polyline.points;
  const arrOfLatLon = polyline.decode(route);
  const allLat = [];
  const allLon = [];

  for (let i = 0; i < arrOfLatLon.length; i++) {
    allLat.push(arrOfLatLon[i][0]);
    allLon.push(arrOfLatLon[i][1])
  }

  const query = "SELECT p.name, p.fees, p.lat, p.lon, s.name as state FROM pois p  INNER JOIN poi_types pt ON pt.id = p.typ  INNER JOIN states s ON s.id = p.fk_state_id where p.id in ( SELECT distinct p.id FROM pois p  INNER JOIN poi_types pt ON pt.id = p.typ  INNER JOIN states s ON s.id = p.fk_state_id INNER JOIN ( SELECT UNNEST(ARRAY[" + allLat + "]) as lat, UNNEST(ARRAY[" + allLon + "]) as lon) l on ST_DWithin(geo_loc, ST_SetSRID(ST_Point(l.lon,l.lat),4326)::geography, 10000,false ) WHERE pt.type = 'Toll Booth' )";

  models.sequelize.query(query, {
    bind:{
      name: '%'+name+'%',
    },
    type: models.sequelize.QueryTypes.SELECT }).then(function (result) {

    if (result.length === 0) {
      return res.status(404).send({
        error: "Not found",
        message: "No tollbooth found within radius for given location",
        code: 404
      })
    }


    async.eachOf(result, function (tollPlaza, index, cb) {
      const fees = [];
      _.each(tollPlaza.fees, function (i) {
        if (i.typeOfVehicle === type)
          fees.push(_.pick(i, ['singleJourney', 'returnJourney', 'monthlyPass']))
      });
      tollPlaza.fees = fees;
      cb()
    }, function () {
      res.json({
        vehicleType: vehicle_type,
        tollbooths: result
      })
    })
  }).catch(function (err) {
    console.log(err);
    res.status(500).send({
      error: "Internal server error",
      message: err,
      code: 500
    })
  })
}