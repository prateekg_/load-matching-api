/**
 * Created by numadic on 21/04/17.
 */
const models = require('../../../db/models/index');

exports.getAssetList = function (req, res) {

  const fk_c_id = req.headers['fk-c-id'] || req.headers.fk_c_id;
  let startTis = Number(req.query.start_tis);
  let endTis = Number(req.query.end_tis);
  const byGroup = req.query.byGroup === 'yes';
  let timeRangeCondition = "";
  console.log("tis ", startTis, endTis);
  if(startTis && typeof startTis === 'number'){
    timeRangeCondition = " AND EXTRACT (EPOCH FROM em.\"createdAt\" at time zone 'GMT+5:30')::INT >= $start_tis";
  }else if(startTis && endTis && typeof startTis === 'number' && typeof endTis === 'number' && endTis >= startTis){
    timeRangeCondition = " AND EXTRACT (EPOCH FROM em.\"createdAt\" at time zone 'GMT+5:30')::INT BETWEEN $start_tis AND $end_tis";
  }

  let query = "SELECT * FROM (" +
    " SELECT e.e_id as nubot_id, a.lic_plate_no as vehicle_license, EXTRACT (EPOCH FROM em.\"createdAt\" at time zone 'GMT+5:30')::INT as added_tis," +
    " agm.connected as status FROM entity_mappings em" +
    " INNER JOIN entities e ON e.id = em.fk_entity_id" +
    " INNER JOIN assets a ON a.id = em.fk_asset_id" +
    " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id" +
    " INNER JOIN groups g ON g.id = agm.fk_group_id AND g.is_admin AND g.fk_c_id = $fk_c_id" +
    " WHERE g.fk_c_id = $fk_c_id AND em.connected IS TRUE" + timeRangeCondition +
    " ORDER BY em.\"createdAt\" DESC" +
    " ) d ";

  if(byGroup){
    query = "SELECT * FROM ("+
            " SELECT g.id as group_id, json_agg(json_build_object('nubot_id',e.e_id, 'vehicle_license', a.lic_plate_no, 'added_tis'," +
            " EXTRACT (EPOCH FROM em.\"createdAt\" at time zone 'GMT+5:30')::INT, 'status',agm.connected)) as assets FROM entity_mappings em"+
            " INNER JOIN entities e ON e.id = em.fk_entity_id"+
            " INNER JOIN assets a ON a.id = em.fk_asset_id"+
            " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id"+
            " INNER JOIN groups g ON g.id = agm.fk_group_id AND g.fk_c_id = $fk_c_id" +
            " WHERE g.fk_c_id = $fk_c_id AND em.connected IS TRUE" + timeRangeCondition +
            " GROUP BY g.id"+
            " ) d ";
  }

  models.sequelize.query(query, {
    bind:{
      fk_c_id: fk_c_id,
      start_tis: startTis,
      end_tis: endTis
    },
    type: models.sequelize.QueryTypes.SELECT})
    .then(function (assets) {
      res.json(assets)
    }).catch(function () {
      res.status(500).send()
    })
};

exports.getVehicleClassTypes = function (req, res) {
  res.json({
    vehicle_class: [
      {code: 'lmv', name: 'Car/Jeep/Van'},
      {code: 'lcv', name: 'Light Commercial Vehicle'},
      {code: 'truck', name: 'Bus/Truck'},
      {code: '2axle', name: 'Upto 3 Axle Vehicle'},
      {code: '6axle', name: '4 to 6 Axle'},
      {code: 'hcm', name: 'HCM/EME'},
      {code: '7axle', name: '7 or more Axle'}
    ]
  })
};