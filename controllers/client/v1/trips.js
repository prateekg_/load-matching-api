/**
 * Created by numadic on 21/04/17.
 */
const async = require('async');
const request = require('request');
const polyline = require('polyline');
const _ = require('lodash');

const models = require('../../../db/models/index');
const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/index')[env];
let TRIP_HELPER = require('../../../helpers/trip_helper');

function processVehicleClass(vehicle_type) {
  let type = "";

  switch (vehicle_type.toLowerCase()) {
  case 'car':
  case 'jeep':
  case 'van':
  case 'lmv':
    type = 'Car/Jeep/Van';
    break;
  case 'lcv':
    type = 'lcv';
    break;
  case 'bus':
  case 'truck':
    type = 'Bus/Truck';
    break;
  case '2axle':
  case '3axle':
    type= 'Upto 3 Axle Vehicle';
    break;
  case '4axle':
  case '5axle':
  case '6axle':
    type = '4 to 6 Axle';
    break;
  case 'hcm':
  case 'eme':
    type = 'HCM/EME';
    break;
  case '7axle':
    type = '7 or more Axle';
    break
  }
  return type.trim().toLowerCase()
}

exports.getTripDetails = function (req, res) {

  const tripID = Number(req.query.tripID);
  const extID = req.query.extID;
  const fk_c_id = req.headers['fk-c-id'] || req.headers.fk_c_id;
  let tripIdCondition = "";
  if(extID){
    tripIdCondition = " AND ext_tid = $fk_ext_trip_id ";
  }else if(tripID){
    tripIdCondition = " AND id = $fk_trip_id" ;
  }else{
    return res.status(417).send("Trip id missing or invalid")
  }

  async.auto({
    trip: function (callback) {
      const query = "SELECT * FROM trip_plannings where new AND fk_c_id = $fk_c_id " + tripIdCondition;
      models.sequelize.query(query, {
        bind: {
          fk_c_id: fk_c_id,
          fk_trip_id: tripID,
          fk_ext_trip_id: extID
        },
        type: models.sequelize.QueryTypes.SELECT})
        .spread(function (trip) {
          if(trip){
            const sl = trip.waypoints[0];
            const el = trip.waypoints[trip.waypoints.length - 1];
            const estpath = (trip.geo_trip_route.paths[0].points).replace(/\\/g, "\\\\");
            const data = {
              assetId: trip.fk_asset_id,
              est_path: estpath,
              act_path: [],
              eta: null,
              distance: "0 km",
              currentLocation: null,
              startLocation: {
                lat: sl.lat,
                lon: sl.lon,
                name: sl.lname
              },
              endLocation: {
                lat: el.lat,
                lon: el.lon,
                name: el.lname
              },
              tripStartTis: null,
              tripEndTis: null
            };
            if(trip.trip_log){
              const lkLocation = trip.trip_log.lkLocation;
              data.currentLocation = {
                tis: lkLocation.tis,
                lat: lkLocation.lat,
                lon: lkLocation.lon
              };
              data.distance = (trip.trip_log.distance / 1000).toFixed(1) + " km";
              data.tripStartTis = trip.trip_log.departure;
              data.tripEndTis = trip.trip_log.arrival || lkLocation.tis;
              const etaMinutes = trip.trip_log.ETA;
              data.eta = Math.floor(etaMinutes/60) + " hrs "+ etaMinutes % 60 + " mins"

            }
            TRIP_HELPER.processWaypoints(trip,  function (err, results) {
              data.trip_log = _.filter(results, function(o){ return o.type !== 'intransit'}) || [];
              return callback(null, data)
            })
          }else{
            console.log("TRIP NOT FOUND");
            return callback({
              code: 404,
              msg: 'Not Found'
            })
          }
        }).catch(function (err) {
          callback({
            code: 500,
            msg: 'Internal server error',
            err: err
          })
        });
    },
    segments: ['trip', function (results, callback) {
      const trip = results.trip;
      if(trip && trip.tripStartTis && trip.tripEndTis){
        request({
          method: 'GET',
          url: `${config.server_address.location_api()}/data/location`,
          qs: {
            report: true,
            fk_asset_id: trip.assetId,
            stis: trip.tripStartTis,
            etis: trip.tripEndTis
          },
          useQuerystring: true,
          json: true
        }, function (error, response, body) {
          if (error) {
            return callback(null, [])
          }
          const locationData = _.sortBy(_.filter(body, 'lat', 'lon'), ['tis']);
          const data = [];
          locationData.forEach(function (i) {
            data.push({ lat: i.lat, lng: i.lon})
          });
          callback(null, data)
        })
      }else{
        callback(null, [])
      }
    }]
  }, function (err, result) {
    if(err){
      if(!err.code){
        err.code = 500;
        err.msg = 'Internal server error'
      }
      res.status(err.code).send(err.msg)
    }else{
      const finalData = _.merge(result.trip, {act_path: result.segments});
      res.render('trip_map', { data: finalData});
    }
  })
};

exports.getTripEstimate = function (req, res) {
  const data = req.body;
  const vehicle_class = processVehicleClass(req.body.vehicle_class || "");
  const show_alternate_routes = !!(req.body && req.body.show_alternate_routes);

  async.auto({
    validate: function (cb) {
      if(!data.source){
        return cb({code: 400, msg: 'Invalid source'})
      }else if(!data.destination){
        return cb({code: 400, msg: 'Invalid destination'})
      }else if(!data.vehicle_class || vehicle_class === ''){
        return cb({code: 400, msg: 'Invalid vehicle class'})
      }else{
        cb()
      }
    },
    routes: ['validate', function (result, cb) {

      const url = 'https://maps.googleapis.com/maps/api/directions/json?alternatives=' + show_alternate_routes + '&origin=' +
        data.source.lat + ',' +
        data.source.lon + '&destination=' +
        data.destination.lat + ',' +
        data.destination.lon + '&key=' + config.googleMap.key;

      request(url, function (error, response, body) {

        if (error) {
          cb({code: 500, msg: 'Internal server error'})
        }
        else if (response.statusCode === 200) {
          const google_res = JSON.parse(body);
          const routes = [];

          google_res.routes && google_res.routes.forEach(function (i) {
            let coordinates = [];
            i.legs && i.legs[0].steps && i.legs[0].steps.forEach(function (l) {
              const arrOfLatLon = polyline.decode(l.polyline.points);
              coordinates = _.concat(coordinates, arrOfLatLon)
            });

            routes.push({
              polyline: i.overview_polyline.points,
              road_matched_polyline: polyline.encode(coordinates),
              distance: i.legs[0].distance,
              duration: i.legs[0].duration,
              summary: i.summary,
              tollbooths: [],
              total_toll_cost: 0
            })
          });
          cb(null, routes)
        }
      })
    }],
    tollbooths: ['routes', function (result, cb) {
      const routes = result.routes;
      async.each(routes, function (route, callback) {
        let total_toll_cost = 0;
        const tollBoothsOnRoute = [];

        const request_options = {
          url: `${config.server_address.tollbooth_intersection()}/v1/tollbooth/intersection`,
          method: 'POST',
          headers: {
            'content-type': 'application/json'
          },
          qs: req.query,
          body: {
            "polyline": "" + route.road_matched_polyline
          },
          json: true
        };

        route.road_matched_polyline = undefined;
        request(request_options, function(error, response, body){
          if(error) {
            console.log("ERROR : Numadic /v1/tollbooth/intersection : ", error)
          }else{
            const data = typeof body === 'string' ? JSON.parse(body) : body;
            _.forEach(data, function(i){
              const fee = _.find(i.fees, function (o) {
                return (o.type_of_vehicle && o.type_of_vehicle.toLowerCase() === vehicle_class) || ( o.typeOfVehicle && o.typeOfVehicle.toLowerCase() === vehicle_class)
              });
              i.fee = fee || null;
              if(fee){
                total_toll_cost += parseInt(fee.single_journey || fee.singleJourney) || 0;
              }
              i.fees = undefined;
              tollBoothsOnRoute.push(i)
            });
            route.tollbooths = tollBoothsOnRoute
          }
          route.total_toll_cost = total_toll_cost;
          callback()
        })
      }, function () {
        cb(null, routes)
      })
    }]
  }, function (err, results) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      results.tollbooths = undefined;
      res.json(results)
    }
  })
};