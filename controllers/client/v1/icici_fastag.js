const _ = require('lodash');
const models = require('../../../db/models/index');
const KYC_MODEL = {
    mobile_no: "",
    user_details: {
        name: "",
        email: "",
        mobile: "",
        mobile_alternate: "",
        verified: false
    },
    address_proof: {
        type: "",
        front_image_url: "",
        back_image_url: "",
        verified: false
    },
    id_proof: {
        type: "",
        id_proof_image_url: "",
        verified: false
    },
    selfie_proof: {
        image_url: "",
        verified: false
    },
    verified: false
}

exports.getUserKYC = function (req, res) {
    let mobile_no = req.query.mobile_no

    if (!mobile_no) {
        return res.status(400).send({ code: 400, msg: 'Enter mobile number' })
    }

    models.user_kyc.findOne({
        where: {
            mobile_no: mobile_no
        }
    }).then(function (kyc) {
        if (!kyc) {
            res.json(KYC_MODEL)
        } else {
            res.json(kyc)
        }
    }).catch(function (err) {
        res.status(500).send(err)
    })
}

exports.updateUserKYC = function (req, res) {
    let data = req.body;
    delete data.verified;

    if (!data.mobile_no) {
        return res.status(400).send({ code: 400, msg: 'Enter mobile number' })
    }

    data = _.pickBy(data, _.identity);

    models.user_kyc.findOrCreate({
        where: { mobile_no: data.mobile_no },
        defaults: data
    }).spread(function (kyc, created) {
        if (!created) {
            if (data.user_details && !_.isEmpty(data.user_details)) {
                kyc.user_details = data.user_details
            }
            if (data.address_proof && !_.isEmpty(data.address_proof)) {
                kyc.address_proof = data.address_proof
            }
            if (data.id_proof && !_.isEmpty(data.id_proof)) {
                kyc.id_proof = data.id_proof
            }
            if (data.selfie_proof && !_.isEmpty(data.selfie_proof)) {
                kyc.selfie_proof = data.selfie_proof
            }
            kyc.save().then(function () {
                return res.json({ status: 200, msg: 'sucessfully updated' })
            }).catch(function (err) {
                return res.status(500).send({ code: 500, msg: 'Internal server error', err: err })
            })
        } else {
            return res.json({ status: 200, msg: 'sucessfully created' })
        }
    }).catch(function (err) {
        return res.status(500).send({ code: 500, msg: 'Internal server error', err: err })
    })
}