const models = require('../../../db/models/index');

exports.getLoads = function (req, res) {
  const customerid = req.params.customerid

  let query = ` SELECT * FROM loads WHERE pickup_city IN (SELECT a.loc FROM
 (SELECT DISTINCT(location) AS loc, MAX(transactiondate) FROM vehicle_logs WHERE customerid =` + customerid + `GROUP BY 1 ORDER BY 2 DESC) a)`;

  models.sequelize.query(query, {
    bind:{
      customerid: customerid,
    },
    type: models.sequelize.QueryTypes.SELECT})
    .then(function (loads) {
      res.json(loads)
    }).catch(function () {
      res.status(500).send()
    })

}


