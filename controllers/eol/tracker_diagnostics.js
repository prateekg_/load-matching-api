/**
 * Created by numadic on 08/08/17.
 */
const async = require('async');
const _ = require('lodash');
const moment = require('moment-timezone');
const models = require('../../db/models/index');
const utils = require('../../helpers/util');
const fs = require('fs');
const Excel = require('exceljs');
const request = require('request');

const nuRedis = require('../../helpers/nuRedis');
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../../config/index.js')[env];
const file_base_path = __dirname + '/../../temp_reports/';

function processDeviceStatus(id, data) {
  let device_status =  _.filter(data,{'id':id});
  if(device_status.length > 0) {
    return device_status[0]
  }
}

function convertToXLS(data, type, filename, callback) {

  try {
    const workbook = new Excel.Workbook();
    let sheet_name = "";
    const width_arr = [{width: 30},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30}
    ];
    let header_arr = [];
    if(type === 'veh_trackers') {
      sheet_name = "Vehicle Trackers";
      header_arr = ['Tracker-ID', 'Asset', 'Last Location', 'Last Location Time', 'Status', 'Installation date', 'Last Serviced', 'No. of servicings'];
      width_arr.push({width: 30});
      width_arr.push({width: 30})
    } else if(type === 'portable_trackers') {
      sheet_name === "Portable Trackers";
      width_arr.push({width: 30});
      header_arr = ['Tracker-ID', 'Consignment No', 'Asset', 'Last Location', 'Status', 'Last Serviced']
    } else  if(type === 'serv_logs') {
      sheet_name === "Asset Servicing Logs";
      width_arr.push({width: 30});
      width_arr.push({width: 30});
      header_arr = ['Service-ID', 'Tracker-ID', 'Task', 'Location', 'Status', 'Servicing Date']
    }

    const ws = workbook.addWorksheet(sheet_name);
    ws.font = {name: 'Proxima Nova'};
    ws.columns = width_arr;

    ws.addRow(header_arr);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    if (type === 'veh_trackers') {
      data.forEach(function (i) {
        const time = i.service_tis ? moment.unix(i.service_tis).tz(utils.timezone.default, false).format('Do MMM YY h:mma') : '-';
        const location_tis = i.tis ? moment.unix(i.tis).tz(utils.timezone.default, false).format('Do MMM YY h:mma') : '-';
        const device_status = i.device_status ? i.device_status.status : '-';
        const installation_date = i.installation_tis ? moment.unix(i.installation_tis).tz(utils.timezone.default, false).format('Do MMM YY h:mma') : '-';
        console.log([i.e_id, i.lic_plate_no, i.lname, location_tis, device_status, time, i.no_of_servicing ],"\n");
        ws.addRow([i.e_id, i.lic_plate_no, i.lname, location_tis, device_status, installation_date, time, i.no_of_servicing ])
      })
    } else if(type === 'portable_trackers') {
      data.forEach(function (i) {
        const time = i.service_tis ? moment.unix(i.service_tis).tz(utils.timezone.default, false).format('Do MMM YY h:mma') : '-';
        const device_status = i.device_status ? i.device_status.status : '-';
        ws.addRow([i.e_id, i.ext_consignment_no, i.lic_plate_no, i.lname, device_status, time ])
      })
    } else if(type === 'serv_logs') {
      data.forEach(function (i) {
        const time = i.completed_tis ? moment.unix(i.completed_tis).tz(utils.timezone.default, false).format('Do MMM YY h:mma') : '-';
        const status = i.completed_tis ? 'Completed' : 'Pending';
        ws.addRow([i.service_id, i.e_id, i.log_type, i.lname, status, time ])
      })
    }

    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback()
      }).catch(function (err) {
        console.log("XLS ERROR : ", err);
        callback({
          code: 500,
          msg: 'Internal error'
        })
      });
  }catch(e){
    console.log(e);
        
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

function convertLocArrayToTuple(assetsIDs, allStatus) {
  let assetLocData = "";
  let count = 0;
  const obj = [];

  assetsIDs.forEach(function (value) {
    obj.push({
      asset_id: value,
      device_status: processDeviceStatus(value, allStatus)
    })
  });

    
  obj.forEach(function (value) {
        
    if (count !== 0 && count !== assetsIDs.length) {
      assetLocData += ","
    }

    assetLocData += "(" + value.asset_id + ",'" + (value.device_status ? value.device_status.statuscode : null)  + "','" + JSON.stringify(value.device_status) + "')";
       
    count++
  });
  return assetLocData
}

exports.getTrackersSummary = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  let scheduled = 0;
  const query =
    `
        SELECT distinct on (a.id) a.id, asl.servicing_logs 
        FROM groups g  
        INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
        INNER JOIN assets a on a.id = agm.fk_asset_id 
        INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = 'Truck'
        INNER JOIN entity_mappings em on em.fk_asset_id = a.id AND em.enum_type = 'Vehicle'
        INNER JOIN entities e on e.id = em.fk_entity_id
        LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
        WHERE a.active and g.fk_c_id = ` + fk_c_id +
    ` AND ` + groupCondition;


  async.parallel({
    trackers: function (cb) {
      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
        .then(function (summary) {
          summary.forEach(function (s) {
            if(s.servicing_logs && s.servicing_logs[0].completed_tis === null) {
              scheduled++
            }
        
          });
          cb(null, {
            total: summary ? summary.length : 0,
            scheduled : scheduled
          })
        }).catch(function (err) {
          console.error(err);
          res.status(500).send()
        })
    },

    portable: function (cb) {

      const portable_query = `SELECT count(*) FROM company_entity_mappings cem INNER JOIN entities e on cem.fk_entity_id = e.id AND e.active WHERE cem.fk_c_id = ` + fk_c_id;
      models.sequelize.query(portable_query, { type: models.sequelize.QueryTypes.SELECT })
        .spread(function (portable) {
          const portable_count = portable ? parseInt(portable.count) : 0;
          cb(null, portable_count)
        }).catch(function (err) {
          console.error(err);
          res.status(500).send()
        })
    }

  }, function(err, result) {
    result.total = result ? result.trackers.total : 0;
    result.scheduled = result ? result.trackers.scheduled : 0;
    result.trackers = undefined;
    res.send(result)
  })

   
};

exports.getAllAssetTrackers = function (req, res, apiType) {

  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit || 10;
  const offset = (req.query.page || 0) * limit;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  let search_col_condition = "";
  let dsfilterWhereClause = "";
  let sort = req.query.sort;
  let sort_condition = "";
  const output_format = req.query.format || 'list';

  if(searchKeyword.length > 0) {
    search_col_condition = " AND (a.lic_plate_no ilike '%" + searchKeyword + "%' OR e.e_id ilike '%" + searchKeyword + "%')"
  }

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'e_id',
      client: 'e_id'
    }, {
      db: 'lic_plate_no',
      client: 'lic_plate_no'
    }, {
      db: 'service_tis',
      client: 'service_tis'
    }, {
      db: 'installation_tis',
      client: 'installation_tis'
    }, {
      db: 'no_of_servicing',
      client: 'no_of_servicing'
    }],
    ""
  )

  const filters = {
    deviceStatus: req.query.device_status || ''
  };

  if(filters.deviceStatus.length > 0) {
    const t = [];
    for (let i = 0; i < filters.deviceStatus.length; i++) {
      t.push("'" + filters.deviceStatus[i] + "'");
    }
    dsfilterWhereClause = " AND loc.device_status_val IN (" + t +  ")" 
  }

  async.waterfall([
    function (cb) {
      const asset_ids = [];
      let query = `SELECT distinct on (a.id) a.id as asset_id
            FROM groups g
            INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
            INNER JOIN assets a on a.id = agm.fk_asset_id
            INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = 'Truck'
            INNER JOIN entity_mappings em on em.fk_asset_id = a.id AND em.enum_type = 'Vehicle' AND em.connected
            INNER JOIN entities e on e.id = em.fk_entity_id
            WHERE a.active and g.fk_c_id = ` + fk_c_id + ` AND ` + groupCondition;

      if(dsfilterWhereClause === "" && sort_condition === "" && search_col_condition === "" && output_format === "list") {
        //first page loads
        query += " LIMIT " + limit + " OFFSET " + offset;
      }

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (assets) {
        if(assets.length > 0){
          async.parallel({
            deviceStatus: function(cb1) {
                            
              _.each(assets, function (i) {
                asset_ids.push(i.asset_id)
              });
              const request_options = {
                uri: config.url.numadicAdmin + '/asset/status',
                method: 'POST',
                headers: {
                  'Authorization': 'bearer ' + config.tokens.numadicAdmin
                },
                json: {
                  "asset_ids": asset_ids
                },
                timeout: 20000
              };
              request(request_options, function (error, response, body) {
                if (error) {
                  console.log("WAD api error ", error);
                  cb1(null, [])
                } else {
                  if (response.statusCode === 200) {
                    cb1(null, body.data)
                  } else {
                    console.log("Wad api responseCode not 200", response.statusCode, body);
                    cb1(null, [])
                  }
                }
              });

            }
          }, function(err, data) {

            cb(null, convertLocArrayToTuple(asset_ids, data.deviceStatus))
          })
                   
        }else{
          cb({
            code: 404,
            msg: 'Not Found'
          })
        }
      }).catch(function (err) {
        cb(err)
      })
    },function (loc_data, cb) {

            
      if(loc_data.length === 0) {
        loc_data = '(null, null,null)'
      }

      let query = `SELECT *,  count(*) OVER() AS total_count FROM (
                SELECT distinct on (a.id) a.id as asset_id, a.asset_id as aid, a.lic_plate_no, atp.type, e.e_id, 
                COALESCE(asl.servicing_logs->0->>'completed_tis', null) service_tis, COALESCE(asl.servicing_logs->0->>'log_type', null) log_type, 
                loc.device_status, c.cname as operator, json_array_length(COALESCE(asl.servicing_logs, '[]')) no_of_servicing,
                CASE WHEN asl.servicing_logs->(json_array_length(asl.servicing_logs) - 1)->>'log_type' = 'installation' 
                THEN COALESCE(asl.servicing_logs->(json_array_length(asl.servicing_logs) - 1) ->>'completed_tis', null) 
                END installation_tis
                FROM groups g  
                INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
                INNER JOIN assets a on a.id = agm.fk_asset_id
                INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = 'Truck'
                INNER JOIN entity_mappings em on em.fk_asset_id = a.id AND em.enum_type = 'Vehicle' AND em.connected
                INNER JOIN entities e on e.id = em.fk_entity_id
                LEFT JOIN companies c on c.id = a.fk_c_id
                LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id ` +
        ` LEFT JOIN ( SELECT * FROM (VALUES ` + loc_data + `) AS t (fk_asset_id, device_status_val, device_status)) as loc on a.id = loc.fk_asset_id::INT ` +
        `WHERE a.active and g.fk_c_id = ` + fk_c_id +
        ` AND ` + groupCondition + search_col_condition + dsfilterWhereClause + `) x ` + sort_condition;


      if (apiType === 'list' && output_format === 'list') {
        query += " LIMIT " + limit + " OFFSET " + offset;
      }

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (service_list) {
        let totalAssets = 0;
        async.forEachOf(service_list, function (service, i, callback) {
          totalAssets = parseInt(service.total_count);
          service.total_count = undefined;
          if(service.device_status !== "undefined") {
            service.device_status = JSON.parse(service.device_status)
          }
          nuRedis.getAssetDetails(service.asset_id, function (err, value) {
            service.lname = null;
            service.tis = null;
            if (value) {
              service.lname = value.lname;
              service.tis = value.tis
            }
            callback()
          })
        }, function (err) {
          console.error(err);
          if (apiType === 'list') {
            cb(null, {
              total_count: totalAssets,
              data: service_list
            });
          } else {
            cb(null, service_list);
          }
        })

                
      }).catch(function (err) {
        console.log(err);
        cb(err)
      })
    }],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }else{
        res.status(500).send()
      }
    }else{
            
      if (output_format === 'xls') {
        const file_name = "VEHICLE-TRACKERS-LIST-" + fk_c_id + "-" + moment().unix() + ".xls";
        const file_path = file_base_path + file_name;

        convertToXLS(result.data, 'veh_trackers', file_name, function (err) {
          if (err) {
            res.status(err.code).send(err.msg)
          } else {
            res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      }else{
        res.json(result)
      }
    }
  })
        
};


exports.getAssetTrackerDetail = function (req, res) {
  const companyId = req.headers.fk_c_id;
  const id = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis;
  const output_format = req.query.format || 'list';

  if (isNaN(id)) {
    return res.status(400).send('Invalid asset ID');
  }

  const query = `SELECT distinct on (a.id) a.id as asset_id, asl.servicing_logs, em.connected mapped
    FROM groups g  
    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
    INNER JOIN assets a on a.id = agm.fk_asset_id 
    INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = 'Truck'
    INNER JOIN entity_mappings em on em.fk_asset_id = a.id AND em.enum_type = 'Vehicle' AND em.connected
    LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
    WHERE a.active and g.fk_c_id = ` + companyId +
    ` AND ` + groupCondition + ` AND a.id = ` + id +
    `ORDER BY a.id, em.connected desc, em."updatedAt" desc`;


  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (serviceData) {
      if(serviceData) {

        if(start_tis && end_tis) {
          serviceData.servicing_logs = _.filter(serviceData.servicing_logs, function(o) {
            if(o.completed_tis >= start_tis && o.completed_tis <= end_tis) {
              return o
            }
          })
        }

        async.forEachOf(serviceData.servicing_logs, function (service, i, callback) {
          if(service.awaiting_deployer && service.awaiting_deployer.length > 0) {
            service.awaiting_deployer = service.awaiting_deployer[service.awaiting_deployer.length - 1].notes
          }
          if(service.awaiting_client && service.awaiting_client.length > 0) {
            service.awaiting_client = service.awaiting_client[service.awaiting_client.length - 1].notes
          }
          revGeoCode(service.lat, service.lon, function (err, value) {
            service.lname = null;
            if (value) {
              service.lname = value
            }
            callback()
          })
        }, function (err) {
          console.error(err);
          if (output_format === 'xls') {
            const file_name = "ASSET-SERVICING-LOGS-" + id + "-" + companyId + "-" + moment().unix() + ".xls";
            const file_path = file_base_path + file_name;

            if (serviceData.servicing_logs && serviceData.servicing_logs.length > 0) {
              convertToXLS(serviceData.servicing_logs, 'serv_logs', file_name, function (err) {
                if (err) {
                  res.status(err.code).send(err.msg)
                } else {
                  res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
                  res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
                  res.setHeader("Content-Type", "application/vnd.ms-excel");
                  res.setHeader("Filename", file_name);

                  res.download(file_path, file_name, function (err) {
                    if (!err) {
                      fs.unlink(file_path)
                    }
                  })
                }
              })
            } else {
              res.json("No data to download")
            }
          } else {
            res.json(serviceData);
          }
        })
                
      } else {
        res.json('Not found')
      }
    }).catch(function (err) {
      console.log(err);
      res.status(500).send();
    })
};

exports.getVehicleTrackerSummary = function (req, res) {
  const companyId = req.headers.fk_c_id;
  const id = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if (isNaN(id)) {
    return res.status(400).send('Invalid asset ID');
  }

  const query = `SELECT distinct on (a.id) a.id as asset_id, a.lic_plate_no, atp.type, e.e_id, c.cname as operator, json_array_length(asl.servicing_logs) no_of_services  
    FROM groups g  
    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
    INNER JOIN assets a on a.id = agm.fk_asset_id 
    INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = 'Truck'
    LEFT JOIN companies c on c.id = a.fk_c_id
    INNER JOIN entity_mappings em on em.fk_asset_id = a.id AND em.enum_type = 'Vehicle' AND em.connected
    INNER JOIN entities e on e.id = em.fk_entity_id
    LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
    WHERE a.active and g.fk_c_id = ` + companyId +
    ` AND ` + groupCondition + ` AND a.id = ` + id;


  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (serviceData) {
      if(serviceData) {
        async.parallel({
          location: function(cb) {
            nuRedis.getAssetDetails(serviceData.asset_id, function (err, value) {
              serviceData.tis = null;
              if (value) {
                serviceData.tis = value.tis
              }
              cb(null, serviceData)
            })
          },
          device_status: function(cb) {
            const request_options = {
              uri: config.url.numadicAdmin + '/asset/status',
              method: 'POST',
              headers: {
                'Authorization': 'bearer ' + config.tokens.numadicAdmin
              },
              json: {
                "asset_ids": [serviceData.asset_id]
              },
              timeout: 2000
            };
            request(request_options, function (error, response, body) {
              if (error) {
                console.log(error);
                cb(null, {})
              } else {
                try {
                  serviceData.device_status = body.data[0];
                  cb(null, serviceData)
                } catch (e) {
                  console.log("PARSE ERROR : ", e);
                  cb(null ,{})
                }
              }
            });
          }
                    
        }, function() {
          res.json(serviceData);
        })
      } else {
        res.json('Not found')
      }
    }).catch(function (err) {
      console.log(err);
      res.status(500).send();
    })
};

let revGeoCode = function(lat, lon, cb ) {
  const options = {
    method: 'GET',
    url: config.googleMap.url + "latlng=" + lat + ',' + lon + "&key=" + config.googleMap.key  //+ "&components=country:IN"
  };
  let lname = "";
  request(options, function (err, response, body) {
    body = JSON.parse(body);
    if (err || body.status !== "OK") {
      return cb(null, lname)
    }

    try {
      lname = body.results[0]['formatted_address']
    }
    catch (e) {
      console.log(e)
    }

    cb(null, lname)
  });
};

exports.getAvailbleConsignmentEntities = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const output_format = req.query.format || 'list';
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  let search_col_condition = "";
  if (searchKeyword.length > 0) {
    search_col_condition = " AND (c.company_fields->>'lic_plate_no' ilike '%" + searchKeyword + "%' OR e.e_id ilike '%" + searchKeyword + "%' OR c.ext_consignment_no ilike '%" + searchKeyword + "%') "
  }


  let query = `SELECT *, count(e_id) OVER()::INT AS total_count FROM (
                    SELECT DISTINCT ON (cmvw.fk_entity_id) tp.status, e.e_id, u.name, cmvw.connected, cmvw.fk_consignment_id, c.ext_consignment_no , c.company_fields->>'lic_plate_no' lic_plate_no, COALESCE(asl.servicing_logs->0->>'completed_tis', null) service_tis                        
                    FROM consignment_mapping_vw cmvw
                    INNER JOIN entities e on cmvw.fk_entity_id = e.id
                    LEFT JOIN consignments c on cmvw.fk_consignment_id = c.id
                    LEFT JOIN trip_plannings tp on cmvw.fk_consignment_id = tp.fk_asset_id --and tp.status = 'completed'
                    LEFT JOIN assets a on a.lic_plate_no = c.company_fields->>'lic_plate_no' and a.active
                    LEFT JOIN asset_types at on tp.fk_asset_type_id = at.id and at.type = 'Consignment'
                    LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
                    LEFT JOIN users u on cmvw.fk_u_id = u.id
                    inner join (
                        select fk_entity_id, max(tis) as max_tis 
                        from consignment_mapping_vw 
                        group by fk_entity_id
                    ) latest_record on (latest_record.fk_entity_id = cmvw.fk_entity_id and latest_record.max_tis = cmvw.tis) OR cmvw.tis is null
                    WHERE cmvw.fk_c_id = ` + fk_c_id + ` and 
                        case when cmvw.connected = true
                            then (cmvw.fk_consignment_id is not null and tp.status = 'completed')
                            else (cmvw.connected = false )
                        end ` + search_col_condition + `
                ) ent
                order by name`;


  if (output_format !== 'xls') {
    query += " LIMIT " + limit + " OFFSET " + offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function(data){
    const total_count = data[0] ? data[0].total_count : 0;
    async.each(data, function(value, callback){
      delete value.total_count;

      if(value.connected === true)
        delete value.name;
                
      nuRedis.getEntityDetails(value.e_id, function (err, nubot) {
        value.lname = nubot && nubot.lname ? nubot.lname : null;
        value.tis = nubot && nubot.tis ? nubot.tis : null;
        callback()
      })
    }, function(){
      if (output_format === 'xls') {
        const file_name = "PORTABLE-TRACKERS-LIST-AVAILABLE-" + fk_c_id + "-" + moment().unix() + ".xls";
        const file_path = file_base_path + file_name;

        convertToXLS(data, 'portable_trackers', file_name, function (err) {
          if (err) {
            res.status(err.code).send(err.msg)
          } else {
            res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      } else {
        res.json({
          total_count: total_count,
          data: data
        })
      }
           
    })
  }).catch(function(err){
    console.log(err);
    res.status(500).send("Internal Server Error")
  })
};

/**
 * This api gets list of in-use entities mapped to consignments
 * @param {*} req 
 * @param {*} res 
 */
exports.getMappedConsignments = function(req, res){
  const fk_c_id = req.headers.fk_c_id;

  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const output_format = req.query.format || 'list';
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  let search_col_condition = "";
  if (searchKeyword.length > 0) {
    search_col_condition = " AND (c.company_fields->>'lic_plate_no' ilike '%" + searchKeyword + "%' OR e.e_id ilike '%" + searchKeyword + "%' OR c.ext_consignment_no ilike '%" + searchKeyword + "%') "
  }

  let query = `SELECT e.e_id, cmvw.fk_consignment_id, c.ext_consignment_no,  
                tp.status, u.name, c.company_fields->>'lic_plate_no' as lic_plate_no,  COALESCE(asl.servicing_logs->0->>'completed_tis', null) service_tis,
                count(e.e_id) OVER()::INT AS total_count
                FROM consignment_mapping_vw cmvw
                INNER JOIN entities e on cmvw.fk_entity_id = e.id
                INNER JOIN (
                select fk_entity_id, max(tis) as max_tis
                from consignment_mapping_vw 
                where fk_c_id = ` + fk_c_id + `
                group by fk_entity_id
                ) latest_record on (latest_record.fk_entity_id = cmvw.fk_entity_id and latest_record.max_tis = cmvw.tis)
                INNER JOIN consignments c on cmvw.fk_consignment_id = c.id
                INNER JOIN trip_plannings tp on c.id = tp.fk_asset_id
                INNER JOIN asset_types at on tp.fk_asset_type_id = at.id and type='Consignment'
                LEFT JOIN assets a on a.lic_plate_no = c.company_fields->>'lic_plate_no' and a.active
                LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
                LEFT JOIN users u on cmvw.fk_u_id = u.id
                WHERE cmvw.fk_c_id = ` + fk_c_id + ` and cmvw.connected ` + search_col_condition;

  if (output_format !== 'xls') {
    query += " LIMIT " + limit + " OFFSET " + offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function(data){
    const total_count = data[0] ? data[0].total_count : 0;
    async.each(data, function(value, callback){
      delete value.total_count;
      nuRedis.getEntityDetails(value.e_id, function (err, nubot) {
        value.lname = nubot && nubot.lname ? nubot.lname : null;
        value.tis = nubot && nubot.tis ? nubot.tis : null;
        callback()
      })
    }, function(){
      if (output_format === 'xls') {
        const file_name = "PORTABLE-TRACKERS-LIST-IN-USE-" + fk_c_id + "-" + moment().unix() + ".xls";
        const file_path = file_base_path + file_name;

        convertToXLS(data, 'portable_trackers', file_name, function (err) {
          if (err) {
            res.status(err.code).send(err.msg)
          } else {
            res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      } else {
        res.json({
          total_count: total_count,
          data: data
        })
      }
            
    })
  }).catch(function(err){
    console.error(err);
    res.status(500).send("Internal Server Error")
  })
};