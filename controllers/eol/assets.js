const async = require('async');
const _ = require('lodash');
const moment = require('moment-timezone');
const models = require('../../db/models/index');
const fs = require('fs');
const utils = require('../../helpers/util');
const auth = require(process.cwd() + '/authorization');
const file_base_path = __dirname + '/../../temp_reports/';
const Excel = require('exceljs');

function processDeviceStatus(id, data) {
   
  let device_status =  _.filter(data,{'id':id});
  if(device_status.length > 0) {
    return device_status[0]
  } else {
    return {
      "title": "No data for more than 48 hours",
      "msg": "The device has been out of coverage for more than 48 hours. This could be due to the vehicle being stationary in a bad network area.",
      "status": "No signal",
      "statuscode": "no_signal_reportable"
    }
  }
}

function processAssetStatus(asset) {
  let status = "disconnected";
  const nowTis = moment().tz(utils.timezone.default, false).unix();
  let asset_tis = asset.tis || null;

  if(!asset_tis){
    status = "disconnected"
  }else if(asset_tis && Math.abs(asset_tis - nowTis) >= 604800){
    status = "disconnected"
  }else if(asset_tis && Math.abs(asset_tis - nowTis) >= 3600 && Math.abs(asset_tis - nowTis) < 604800 ){
    status = "unknown"
  }else if(asset.status === 'idling' && Math.abs(asset_tis - asset.idling_since) < 1800){
    status = "moving"
  }else if(asset.status === 'moving'){
    status = "moving"
  }else if(asset.status === 'idling'){
    status = "idling"
  }else if(asset.status === 'stationary'){
    status = "stationary"
  }else{
    status = "disconnected"
  }
  return status
}

function eolAssetStatusLogicQuery() {
  return " CASE " +
        " WHEN loc.tis IS NOT NULL AND abs(loc.tis - "+ moment().unix() +") >= 604800 THEN 'disconnected'" +
        " WHEN loc.tis IS NOT NULL AND abs(loc.tis - "+ moment().unix() +") between 3600 AND 604800 THEN 'unknown'" +
        " WHEN loc.status = 'idling' AND abs(loc.tis - loc.idling_since) < 1800 THEN 'moving'" +
        " WHEN loc.status = 'idling' THEN 'idling'" +
        " WHEN loc.status = 'moving' THEN 'moving'" +
        " WHEN loc.status = 'stationary' THEN 'stationary'" +
        " ELSE 'disconnected'" +
        " END as status"
}

function convertToXLS(data, filename, callback) {

  try {
    const workbook = new Excel.Workbook();
    const ws = workbook.addWorksheet('Asset list');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = [
      {width: 20},
      {width: 20},
      {width: 20},
      {width: 15},
      {width: 25},
      {width: 35},
      {width: 35},
      {width: 40},
      {width: 30},
      {width: 30},
      {width: 40},
      {width: 30},
    ];

    ws.addRow(['Asset license plate','Transporter code','Transporter name','Load status','Shipment number', 'Invoice number','Last location', 'Last sent data','Speed','Nubot status','Nubot status details','Asset Status']);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    data.forEach(function (i) {
      const time = i.tis > 0 ? moment.unix(i.tis).tz(utils.timezone.default, false).format('Do MMM YY h:mma') : '-';
      let shipment_nos = [];
      let invoice_nos = [];
      let transporter_code = "";
      let load_status = "";

      if(i.trips){
        const trip = i.trips;
        transporter_code = trip.transporter.code;
        load_status = trip.tripLoadStatus;

        trip.inprogressTrips.forEach(function (a) {
          if(a.shipmentNo){
            shipment_nos.push(a.shipmentNo)
          }

          if(a.invoiceNo){
            invoice_nos.push(a.invoiceNo)
          }
        });
        shipment_nos = shipment_nos.toString().replace('[]/g','');
        invoice_nos = invoice_nos.toString().replace('[]/g','')
      }
      const device_status = i.device_status ? i.device_status.status : "";
      const device_status_details = i.device_status ? i.device_status.msg : "";

      ws.addRow([i.licNo, transporter_code, i.operator, load_status, shipment_nos, invoice_nos, i.lname, time, i.spd + ' km/h', device_status, device_status_details, i.status])
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback()
      }).catch(function (err) {
        callback({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      });
  }catch(e){
    console.error(e)
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

/*
    Version 2 to get current location and devices status from database.
 */
exports.getAllAssetsV2 = function (req, res, assetType, apiType) {

  const userFeatureList = JSON.parse(req.headers['feature-list']);
  const fk_c_id = req.headers.fk_c_id;
  let searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const sort = req.query.sort;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const output_format = req.query.format;
  const search_cols = req.query.search;
  let dsfilterWhereClause = "";
  let table_search_condition = "";
  let vehicle_status_condition = "";
  let onTrip_status_condition = "";
  let place_filter_condition = "";

  if(output_format === 'xls'){
    apiType = output_format
  }

  const search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'a.lic_plate_no',
      client: 'licNo'
    }, {
      db: 'c.transporter_code',
      client: 'trips.transporter.code'
    }, {
      db: 'loc.lname',
      client: 'lname'
    }, {
      db: 'iea.shipment_no',
      client: 'trips.shipmentNo'
    }, {
      db: 'iea.invoice_no',
      client: 'trips.invoiceNo'
    }]
  );

  const sort_condition = utils.processTableColumnSort(
    sort
    , [{
      db: 'd.last_trip_tis',
      client: 'trips.last_trip_tis'
    }, {
      db: 'd.stationary_since',
      client: 'stationary_since'
    }, {
      db: 'COALESCE(d.tis, 0)',
      client: 'tis'
    }, {
      db: 'd.device_status_val',
      client: 'device_status'
    }],
    " ORDER BY COALESCE(d.tis, 0) DESC ");

  // filters
  const filters = {
    assetStatus: req.query.status || '',
    onTrip: req.query.onTrip,
    place: req.query.place || null,
    deviceStatus: req.query.device_status || '',
  };

  if(filters.deviceStatus.length > 0) {
    const t = [];
    for (let i = 0; i < filters.deviceStatus.length; i++) {
      t.push("'" + filters.deviceStatus[i] + "'");
    }
    dsfilterWhereClause = " AND d.device_status_val IN (" + t +  ")"
  }

  if(filters.assetStatus){
    vehicle_status_condition = " AND status = '" + filters.assetStatus + "'";
  }

  if(filters.onTrip){
    onTrip_status_condition = "";
    if((filters.onTrip === 'true' || filters.onTrip === 'false')){
      onTrip_status_condition += " AND CAST(COALESCE(JSON_ARRAY_LENGTH(d.tp),0) as INTEGER) ";
      onTrip_status_condition += filters.onTrip === 'true' ? " > 0 " : " = 0 "
    }
  }

  if(filters.place){
    place_filter_condition = " AND ( lower(loc.lname) like '%"+ filters.place +"%' OR lower(iea.origin) like '%"+ filters.place +"%' OR lower(iea.destination) like '%"+ filters.place +"%' )"
  }

  async.waterfall([
    function (cb) {

      if(searchKeyword.length > 0){
        table_search_condition = " AND (lower(loc.lname) like '%" + searchKeyword + "%' or lower(a.lic_plate_no) like '%" + searchKeyword + "%' " +
                    " or lower(a.name) like '%" + searchKeyword + "%' or lower(c.cname) like '%" + searchKeyword + "%' or lower(c.transporter_code) like '%" + searchKeyword + "%' or lower(iea.*::TEXT) like '%" + searchKeyword + "%' )";
      }

      let query = "WITH vehicle_trips as (" +
        "  SELECT tp.id, tp.fk_asset_id, t.last_trip_tis  " +
        "  FROM  " +
        "    ( " +
        "      SELECT fk_asset_id, json_array_elements(trips) as trips, last_trip_tis  " +
        "      FROM asset_trip_maps " +
        "    ) t  " +
        "    inner join trip_plannings tp on tp.id = (t.trips ->> 'id'):: INT " +
        "),  " +
        "       my_assets AS ( " +
        "           SELECT DISTINCT a.id FROM GROUPS g " +
        "       inner join asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected " +
        "       inner join assets a ON a.id = agm.fk_asset_id AND a.active " +
        "       inner join " +
        "       ( " +
        "           SELECT DISTINCT a.id " +
        "       FROM            GROUPS g " +
        "       inner join      asset_group_mappings agm " +
        "       ON              agm.fk_group_id = g.id " +
        "       AND             agm.connected " +
        "       inner join      assets a " +
        "       ON              a.id = agm.fk_asset_id " +
        "       AND             a.active " +
        "       WHERE           g.fk_c_id = " + fk_c_id +
        "       AND             g.is_admin " +
        "       AND             agm.connected " +
        "       AND             a.active ) ca " +
        "       ON         ca.id = a.id " +
        "       WHERE a.active AND g.active AND g.fk_c_id = " + fk_c_id + " AND " + groupCondition +
        "   ) " +

        "SELECT  " +
        "  *,  " +
        "  count(*) OVER() AS total_count  " +
        "FROM  " +
        "  ( " +
        "    SELECT  " +
        "      DISTINCT a.id,  " +
        "      a.active,  " +
        "      a.asset_id as aid,  " +
        "      a.name,  " +
        "      atp.type,  " +
        "      a.lic_plate_no as \"licNo\",  " +
        "      c.cname as operator,  " +
        "      loc.lname,  " +
        "      loc.lat,  " +
        "      loc.lon,  " +
        "      COALESCE(loc.ign, 'A') as ign,  " +
        "      COALESCE(loc.spd, 0) as spd,  " +
        "      COALESCE(loc.osf, false) as osf,  " + eolAssetStatusLogicQuery() + "," +
        "      loc.tis,  " +
        "      COALESCE(loc.heading :: INT, 0) as heading,  " +
        "      loc.stationary_since,  " +
        "      loc.idling_since,  " +
        "      COALESCE(loc.device_status :: jsonb ->> 'statuscode', 'no_signal_reportable') as device_status_val,  " +
        "      loc.device_status :: jsonb,  " +
        "      json_agg( " +
        "        distinct json_build_object( " +
        "          'id', tp.id, 'tid', tp.tid, 'fk_asset_id',  " +
        "          tp.fk_asset_id, 'end_tis', tl.arrival,  " +
        "          'eta', tl.eta, 'delayed_by', tl.delayed_by,  " +
        "          'delayed', tl.delayed, 'shipment_no',  " +
        "          iea.shipment_no, 'invoice_no', iea.invoice_no,  " +
        "          'origin', iea.origin, 'destination',  " +
        "          iea.destination " +
        "        ):: jsonb " +
        "      ) FILTER ( " +
        "        WHERE  " +
        "          tp.id IS NOT NULL " +
        "      )::JSONB as tp,  " +
        "      json_build_object( " +
        "        'name', c.cname, 'code', c.transporter_code " +
        "      )::JSONB as transporter,  " +
        "      vt.last_trip_tis  " +
        "    FROM  " +
        "      assets a " +
        "      INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id  " +
        "      LEFT JOIN companies c on c.id = a.fk_c_id  " +
        "      LEFT JOIN ( SELECT * FROM vehicle_trips ) vt on vt.fk_asset_id = a.id  " +
        "      LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id AND tp.id in ( SELECT id FROM vehicle_trips )  " +
        "      LEFT JOIN trip_logs tl on tl.fk_trip_id = tp.id AND tl.fk_trip_id in ( SELECT id FROM vehicle_trips )  " +
        "      LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id  " +
        "      AND iea.fk_t_id in ( " +
        "        select  " +
        "          id  " +
        "        from  " +
        "          vehicle_trips " +
        "      )  " +
        "      AND tp.active  " +
        "      AND tp.fk_c_id = " + fk_c_id +
        "      AND tp.status in ('scheduled', 'inprogress')  " +
        "      LEFT JOIN current_locations loc on loc.fk_asset_id = a.id  " +
        "      INNER JOIN entity_mappings em on em.fk_entity_id = loc.fk_entity_id and em.connected" +
        "    WHERE  " +
        " a.id IN (SELECT id FROM my_assets)" +
        "      AND atp.type = 'Truck'  " +
        search_col_condition + table_search_condition + place_filter_condition +

        "    GROUP BY  " +
        "      atp.id,  " +
        "      c.id,  " +
        "      a.id,  " +
        "      loc.id,  " +
        "      vt.last_trip_tis " +
        "  ) d  " +
        "WHERE  " +
        "  d.active  " + dsfilterWhereClause + vehicle_status_condition + onTrip_status_condition +
        sort_condition;

      if(apiType === 'list'){
        query += " LIMIT " + limit + " OFFSET " + offset;
      }

      // If apiType == map and no trips filters applied get minimum data with below query.

      if(apiType === 'map' && !searchKeyword && !filters.place && !filters.deviceStatus && !_.includes(['true','false'], filters.onTrip)){
        query =

                    "WITH "+
            "       my_assets AS ( "+
            "           SELECT DISTINCT a.id FROM GROUPS g "+
            "       inner join asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected "+
            "       inner join assets a ON a.id = agm.fk_asset_id AND a.active "+
            "       inner join "+
            "       ( "+
            "           SELECT DISTINCT a.id "+
            "       FROM            GROUPS g "+
            "       inner join      asset_group_mappings agm "+
            "       ON              agm.fk_group_id = g.id "+
            "       AND             agm.connected "+
            "       inner join      assets a "+
            "       ON              a.id = agm.fk_asset_id "+
            "       AND             a.active "+
            "       WHERE           g.fk_c_id = "+ fk_c_id +
            "       AND             g.is_admin "+
            "       AND             agm.connected "+
            "       AND             a.active ) ca "+
            "       ON         ca.id = a.id "+
            "       WHERE a.active AND g.active AND g.fk_c_id = "+fk_c_id+" AND "+ groupCondition +
            "   ) " +
                "SELECT * FROM ("+

            "       SELECT a.active, "+
            "           a.id, "+
            "           atp.TYPE, "+
            "           a.lic_plate_no  AS \"licNo\" , "+
            "           loc.lname, "+
            "           loc.lat, "+
            "           loc.lon, "+
            "           Coalesce(loc.ign, 'A')   AS ign, "+
            "           Coalesce(loc.spd, 0)     AS spd, "+
            "           Coalesce(loc.osf, FALSE) AS osf, "+
                        eolAssetStatusLogicQuery() +", "+
            "           loc.tis, "+
            "           Coalesce(loc.heading :: INT, 0) AS heading "+

            "       FROM assets a "+
            "       inner join asset_types atp ON atp.id = a.fk_ast_type_id "+
            "       left join  current_locations loc ON loc.fk_asset_id = a.id "+
            "       WHERE a.active AND a.id IN (SELECT id FROM my_assets) " +
                ") d WHERE active " + vehicle_status_condition;
      }

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (assets) {
        let totalAssets = 0;
        async.each(assets, function (asset, cb) {
          totalAssets = Number(asset.total_count);
          asset.gids = _.uniq(asset.gids);
          asset.lat = asset.lat ? parseFloat(asset.lat) : null;
          asset.lon = asset.lon ? parseFloat(asset.lon) : null;
          if(auth.FEATURE_LIST.hasTrips(userFeatureList)){
            const inprogressTrips = [];
            const tp = _.uniqBy(asset.tp, 'id');
            tp.forEach(function (i) {
              if(i.id){
                inprogressTrips.push({
                  id: i.id,
                  tid: i.tid,
                  invoiceNo: i.invoice_no,
                  shipmentNo: i.shipment_no,
                  eta: {
                    tis: i.end_tis + (i.delayed_by || 0),
                    duration: Math.round(i.delayed_by / 60) || 0,
                    delayed: i.delayed || false
                  }
                })
              }
            });
            asset.trips = {
              transporter: asset.transporter,
              last_trip_tis: asset.last_trip_tis,
              tripLoadStatus: inprogressTrips.length > 0 ? 'Loaded' : 'Unloaded',
              inprogressTrips: inprogressTrips
            };
            asset.transporter = undefined;
            asset.last_trip_tis = undefined
          }

          delete asset.total_count;
          delete asset.tp;
          delete asset.device_status_val;
          process.nextTick(function(){
            return cb()
          })

        }, function (err) {
          if(err) console.error(err);

          if (apiType === 'list') {
            cb(null, {
              total_count: totalAssets,
              data: assets
            });
          } else {
            cb(null, assets);
          }
        })
      }).catch(function (err) {
        console.log(err);
        cb(err)
      })
    }],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }else{
        res.status(500).send()
      }
    }else{

      if(output_format === 'xls'){
        const filterStatus = filters.assetStatus || 'ALL';
        const file_name = "ASSET-LIST-" + filterStatus.toUpperCase() + "-" + utils.downloadReportDate() + ".xls";
        const file_path = file_base_path + file_name;

        convertToXLS(result, file_name, function (err) {
          if(err){
            res.status(err.code).send(err.msg)
          }else {
            res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      }else{
        res.json(result)
      }
    }
  })
};

exports.getAssetDetail = function(req, res, assetType) {

  const fk_c_id = req.headers.fk_c_id;
  const userFeatureList = JSON.parse(req.headers['feature-list']);
  let assetId = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  if(!assetId) {
    return res.status(400).send('Invalid asset ID');
  }
  let queryCondition = '';
  if (!utils.isInt(assetId)) {
    queryCondition = " a.asset_id = '" + assetId + "'"
  } else {
    queryCondition = ' a.id = ' + assetId
  }

  const query = `SELECT a.id, atp.type, a.lic_plate_no as "licNo", a.asset_id as "assetId", a.name, cl.status, cl.lname, cl.lat, cl.lon, cl.tis,
            cl.spd, cl.door_status, cl.ign, cl.heading, cl.device_status, json_agg(g.gid) as gids, c.cname as operator, agta.log_type
         FROM groups g
         LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected
         LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active
         INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = '${assetType}'
         INNER JOIN (
                    SELECT DISTINCT a.id
                    FROM groups g  
                    INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id   and agm.connected
                    INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active
                    WHERE g.fk_c_id = ${fk_c_id} and g.is_admin and agm.connected and a.active
                  ) ca on ca.id = a.id
         LEFT JOIN companies c on c.id = a.fk_c_id
         LEFT JOIN (
            select fk_asset_id, log_type from admin_group_task_activities where id in (
                select max(id) from admin_group_task_activities group by fk_asset_id
            )
        ) agta ON agta.fk_asset_id = a.id
         LEFT JOIN current_locations cl on cl.fk_asset_id = a.id
         WHERE g.active AND g.id in ${groupIds} AND ${queryCondition}
         GROUP BY a.id, atp.id, c.id, agta.log_type, cl.id LIMIT 1`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (asset) {
      if(asset){
        asset.lul = asset.door_sensor && asset.door_sensor.length > 0 ? asset.door_sensor[0].value : "N/A";
        asset.door_sensor = undefined;
        asset.fuel_sensor = null;
        asset.axle_load_sensor = null;
        asset.device_status = asset.device_status ? asset.device_status : processDeviceStatus(asset.id, []);

        async.parallel({
          assetLocation: function (cb) {
            return cb(null, asset)
          },
          trips: function (cb) {
            if(auth.FEATURE_LIST.hasTrips(userFeatureList)){

              const query = "SELECT json_agg(json_build_object('id', tp.id, 'tid', tp.tid, 'fk_asset_id'," +
                " tp.fk_asset_id,'end_tis',tp.end_tis,'eta',tp.trip_log->'ETA','transporter_name',iea.add1,'transporter_code',iea.add2," +
                " 'invoice_no', iea.invoice_no, 'shipment_no',iea.shipment_no,'delayed_by', tp.trip_log->'delayed_by', 'delayed', tp.trip_log->'delayed')) as tp" +
                " FROM asset_trip_maps as atm" +
                " LEFT JOIN json_array_elements_text(atm.trips) x ON TRUE" +
                " LEFT JOIN trip_plannings tp on tp.fk_asset_id = atm.fk_asset_id AND tp.id = to_json(x::json->'id')::text::int" +
                " LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
                " WHERE atm.fk_asset_id = " + asset.id;

              models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                .spread(function (data) {
                  const inprogressTrips = [];
                  const transporter = {
                    name: null,
                    code: null
                  };
                  const tp = _.uniqBy(data.tp, 'id');
                  tp.forEach(function (i) {
                    if(i.id){
                      transporter.name = i.transporter_name;
                      transporter.code = i.transporter_code;
                      inprogressTrips.push({
                        id: i.id,
                        tid: i.tid,
                        invoiceNo: i.invoice_no,
                        shipmentNo: i.shipment_no,
                        eta: {
                          tis: i.end_tis + (i.delayed_by || 0),
                          duration: Math.round(i.delayed_by / 60) || 0,
                          delayed: i.delayed || false
                        }
                      })
                    }
                  });
                  const trips = {
                    transporter: transporter,
                    tripLoadStatus: inprogressTrips.length > 0 ? 'Loaded' : 'Unloaded',
                    inprogressTrips: inprogressTrips
                  };
                  cb(null, trips)
                }).catch(function () {
                  cb(null, null)
                })
            }else{
              cb(null, undefined)
            }
          }
        }, function (err, results) {
          results = _.merge(results, results.assetLocation);
          results = _.merge(results, results.deviceStatus);
          results.assetLocation = undefined;
          results.deviceStatus = undefined;
          res.json(results)
        })

      }else{
        res.status(404).send()
      }
    }).catch(function (err) {
      console.error(err);
      res.status(500).send()
    });
};

exports.processAssetStatus = processAssetStatus;