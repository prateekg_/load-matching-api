/**
 * Created by Haston Silva on 27/06/17.
 * Essar Oil custom APIS
 */
const utils = require('../../helpers/util');
const models = require('../../db/models');
const _ = require('lodash');
const async = require('async');
const moment = require('moment');
const CHAR_LABEL = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
const PolylineUtil = require('polyline-encoded');
const nuDynamoDb = require('../../helpers/dynamoDb');

function computeAssetStatus(item) {
  let status = "stationary";
  if(item.ign === 'B' && item.spd > 0 && item.osf){
    status = 'overspeeding'
  } else if(item.ign === 'B' && item.spd > 0){
    status = 'moving'
  }else if(item.ign === 'B' && item.spd < 1){
    status = 'idling'
  }
  return status
}

function getUserShipToPartyCode(req, callback){
  // get ship to party of the company group of the user

  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;

  const query = "SELECT g.details->'eol_ship_to_party' as code FROM users u" +
    " INNER JOIN companies c on c.id = u.fk_c_id" +
    " INNER JOIN user_group_mappings ugm on ugm.fk_u_id = u.id AND ugm.connected" +
    " INNER JOIN groups g on g.id = ugm.fk_group_id AND g.fk_c_id = c.id" +
    " WHERE u.id = " + fk_u_id + " AND c.id = " + fk_c_id;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (data){

      let isValid = false;
      const codes = [];
      data.forEach(function (group) {
        if(group.code){
          isValid = true;
          codes.push(group.code)
        }
      });

      if(isValid){
        let x = JSON.stringify(codes);
        x =  x.replace(/"/g,"'");
        const finalCodes = codes.length > 0 ? "(" + x.substring(1, x.length - 1) + ")" : null;
        callback(null, finalCodes)
      }else{
        callback({
          code: 401,
          msg: 'Unauthorized'
        })
      }
    }).catch(function (err) {
      callback({
        code: 500,
        msg: err
      })
    })
}

function processWaypoints(trip, mainCb){
  const finalTrip = [];
  let seqWithOne = false;

  async.eachOfSeries(trip.waypoints, function (i, index, callback) {

    if(index === 0){
      if(i.seq_no === 1){
        seqWithOne = true
      }
    }

    let hasInstransitEvent = false;
    const matchedItem = _.find(trip.trip_log && trip.trip_log.trip, ['Id', i.fk_poi_id]);

    const item = {
      type: "planned",
      lname: i.lname,
      arrivalTis: i.tis,
      departureTis: (i.tis + (i.stop_dur * 60)),
      dwellDuration: Math.round(moment.duration(i.stop_dur, 'seconds').asMinutes()),
      violations: 0,
      crossed: false,
      label: seqWithOne ? CHAR_LABEL[i.seq_no - 1] : CHAR_LABEL[i.seq_no] || '',
      lat: i.lat,
      lon: i.lon,
      avgSpd: 0,
      distance: 0
    };

    if(index === 0){
      item.arrivalTis = trip.start_tis;
      item.departureTis = trip.start_tis
    }

    const itemIntransit = {
      type: "intransit",
      lname: '',
      arrivalTis: i.tis,
      departureTis: (i.tis + (i.stop_dur * 60)),
      dwellDuration: 0,
      violations: 0,
      crossed: true,
      label: '',
      lat: null,
      lon: null,
      avgSpd: 0,
      distance: 0
    };

    if(matchedItem){

      if(index !== 0){
        item.arrivalTis = matchedItem.StartTis;
        item.departureTis = matchedItem.EndTis;
      }
      item.dwellDuration = Math.round(moment.duration(matchedItem.DwellTime,'seconds').asMinutes());
      item.crossed = true;

      if(matchedItem.Distance && matchedItem.Distance >= 0 && matchedItem.AvgSpd){
        hasInstransitEvent = true;
        itemIntransit.arrivalTis = matchedItem.StartTis;
        itemIntransit.departureTis = matchedItem.EndTis;
        itemIntransit.avgSpd = matchedItem.AvgSpd;
        itemIntransit.distance = Number((matchedItem.Distance /1000).toFixed(1));
      }

    }

    // get poi bounds

    const query = "SELECT p.geo_loc from pois p where p.id = " + i.fk_poi_id;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (poi) {
        if(poi){
          const bounds = [];
          const coordinates = poi.geo_loc.coordinates[0];
          coordinates.forEach(function (i) {
            bounds.push({
              lon: i[0],
              lat: i[1]
            })
          });
          item.geoBounds = bounds
        }

        finalTrip.push(item);
        if(hasInstransitEvent){
          finalTrip.push(itemIntransit)
        }
        callback()
      }).catch(function (err) {
        finalTrip.push(item);
        if(hasInstransitEvent){
          finalTrip.push(itemIntransit)
        }

        console.log("err : ", err);

        callback()
      })

  }, function () {
    if(trip.trip_log){

      const tmp_trip = _.filter(trip.trip_log.trip, {Id: 0});
      let c = 1;
      tmp_trip.forEach(function (o) {

        finalTrip.push({
          type:"unplanned",
          lname: o.LName,
          arrivalTis: o.StartTis,
          departureTis: o.EndTis,
          dwellDuration: Math.round(moment.duration(o.DwellTime,'seconds').asMinutes()),
          violations: 0,
          crossed: true,
          label: c.toString(),
          lat: o.Lat,
          lon: o.Lon,
          avgSpd: 0,
          distance: 0
        });

        if(o.Distance && o.Distance >= 0 && o.AvgSpd){
          // FOUND INTRANSIT IN UNPLANNED STOP
          finalTrip.push({
            type: "intransit",
            lname: '',
            arrivalTis: o.StartTis,
            departureTis: o.EndTis,
            dwellDuration: 0,
            violations: 0,
            crossed: true,
            label: '',
            lat: null,
            lon: null,
            avgSpd: o.AvgSpd || 0,
            distance: Number((o.Distance /1000).toFixed(1))
          })
        }

        c++
      })
    }
    return mainCb(null, _.orderBy(finalTrip, ['crossed','arrivalTis'],['desc','asc']))
  });
}

exports.getAllTrips = function (req, res) {

  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const searchKeyword = req.query.q || '';
  const tripStatus = req.query.status;
  const tripStatusCondition = tripStatus ? " AND d.trip_status ilike '" + tripStatus + "'" : "";

  async.auto({
    validate_user: function (cb) {
      getUserShipToPartyCode(req, cb)
    },
    get_data: ['validate_user', function (results, cb) {
      const valid_ship_to_party_codes = results.validate_user;

      const query = "SELECT * FROM (" +
        " SELECT case when (tp.status = 'scheduled' OR tp.status = 'inprogress') then 'incoming'" +
        " when tp.status = 'completed' then 'completed'" +
        " when tp.status = 'open' then 'open'" +
        " end as trip_status," +
        " tp.id, tp.start_tis, tp.status, tp.waypoints, tp.trip_log, tp.end_tis, iea.invoice_no as \"invoiceNo\"," +
        " iea.ship_to_party as \"shipToParty\", iea.consignee as \"cmsCode\"," +
        " json_build_object('id', a.id, 'licNo',a.lic_plate_no) as asset," +
        " json_build_object('condition',tfc.condition,'description',tfc.description) as failure_condition," +
        " count(tp.id) OVER()::INT AS total_count" +
        " FROM trip_plannings tp " +
        " INNER JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
        " INNER JOIN assets a on a.id = tp.fk_asset_id" +
        " LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id" +
        " LEFT JOIN companies c on c.id = a.fk_c_id" +
        " LEFT JOIN trip_failure_conditions tfc on tfc.id = tp.fk_trip_failure_condition_id" +
        " WHERE iea.ship_to_party in " + valid_ship_to_party_codes + " AND tp.new AND to_json(tp.trip_log->'trip')::text != 'null'" +
        " AND ( a.lic_plate_no ilike '%" + searchKeyword + "%' OR tp.*::text ilike '%" + searchKeyword + "%' OR iea.*::TEXT ilike '%" + searchKeyword + "%' " +
        " OR c.cname ilike '%" + searchKeyword + "%' OR c.transporter_code ilike '%" + searchKeyword + "%' )" +
        " GROUP BY tp.id, c.id, a.id, ast.id, iea.id, tfc.id " +
        " ORDER BY tp.start_tis DESC ) d" +
        " WHERE d.id > 0" + tripStatusCondition +
        " LIMIT " + limit + " OFFSET " + offset;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function (trips) {
          let total_count = 0;
          trips.forEach(function (i) {
            try {
              total_count = i.total_count;
              i.eta = null;
              const start_wp = i.waypoints[0];
              i.source = {
                lname: start_wp.lname,
                tis: i.start_tis
              };
              const end_wp = i.waypoints[i.waypoints.length - 1];
              i.destination = {
                lname: end_wp.lname,
                tis: end_wp.tis
              };

              const tlog = i.trip_log;

              if(tlog){
                i.destination.tis = tlog.arrival && tlog.arrival > 0 ? tlog.arrival : i.destination.tis
              }

              if (tlog && i.status === 'inprogress') {
                i.eta = {
                  tis: i.end_tis + (tlog.delayed_by || 0),
                  duration: Math.round(tlog.delayed_by / 60) || 0,
                  delayed: tlog.delayed || false
                }
              }
              i.progress = tlog ? tlog.progress : 0;
              i.status = i.trip_status;

              i.trip_status = undefined;
              i.total_count = undefined;
              i.waypoints = undefined;
              i.trip_log = undefined;
              i.end_tis = undefined;
              i.groups = undefined;
              i.start_tis = undefined;

            }catch (e){
              console.log(e)
            }
          });
          cb(null,{
            count : total_count,
            data : trips
          });
        }).catch(function (err) {
          cb({
            code: 500,
            msg: err
          })
        })
    }]
  }, function (err, results) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.json(results.get_data)
    }
  })
};

exports.getSingleTrip = function (req, res){

  let id = req.query.id;

  if(!id){
    res.status(400).send('Invalid trip id')
  }else {

    async.auto({
      validate_user: function (cb) {
        getUserShipToPartyCode(req, cb)
      },
      get_data: ['validate_user', function (results, cb) {
        const valid_ship_to_party_codes = results.validate_user;

        const query = "SELECT case when (tp.status = 'scheduled' OR tp.status = 'inprogress') then 'incoming'" +
          " when tp.status = 'completed' then 'completed'" +
          " when tp.status = 'open' then 'open'" +
          " end as trip_status, " +
          " iea.invoice_no as \"invoiceNo\", tp.status, tp.est_distance as distance, tp.trip_log, tp.end_tis, tp.start_tis," +
          " iea.ship_to_party as \"shipToParty\", iea.consignee as \"cmsCode\"," +
          " json_build_object('name',COALESCE(iea.add2,c.cname),'code',COALESCE(iea.add1, c.transporter_code)) as transporter," +
          " json_build_object('name', iea.origin,'code',iea.consigner) as depot," +
          " json_build_object('lname',iea.destination) as destination, tp.oth_details," +
          " json_build_object('licNo', a.lic_plate_no) as asset, tp.waypoints," +
          " json_build_object('condition',tfc.condition,'description',tfc.description) as failure_condition" +
          " FROM trip_plannings tp " +
          " INNER JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
          " INNER JOIN assets a on a.id = tp.fk_asset_id" +
          " LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id" +
          " LEFT JOIN companies c on c.id = a.fk_c_id " +
          " LEFT JOIN trip_failure_conditions tfc on tfc.id = tp.fk_trip_failure_condition_id" +
          " WHERE iea.ship_to_party in " + valid_ship_to_party_codes + " AND tp.new " + " AND tp.id = " + id +
          " GROUP BY tp.id, c.id, a.id, ast.id, iea.id, tfc.id" +
          " ORDER BY tp.start_tis DESC LIMIT 1";

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .spread(function (trip) {
            if (!trip) {
              res.status(404).send()
            } else {

              trip.eta = null;

              const start_wp = trip.waypoints[0];
              trip.source = {
                lname: start_wp.lname,
                tis: trip.start_tis
              };
              const end_wp = trip.waypoints[trip.waypoints.length - 1];
              trip.destination = {
                lname: end_wp.lname,
                tis: end_wp.tis
              };

              const tlog = trip.trip_log;

              if(tlog){
                trip.destination.tis = tlog.arrival && tlog.arrival > 0 ? tlog.arrival : trip.destination.tis
              }

              if (tlog && trip.status === 'inprogress') {
                trip.eta = {
                  tis: trip.end_tis + (tlog.delayed_by || 0),
                  duration: Math.round(tlog.delayed_by / 60) || 0,
                  delayed: tlog.delayed || false
                }
              }
              trip.progress = tlog ? tlog.progress : 0;
              trip.distance = Number((trip.distance/1000).toFixed(1));
              trip.drivers = trip.oth_details && trip.oth_details.drivers ? trip.oth_details.drivers : [];
              trip.status = trip.trip_status;

              trip.trip_log = undefined;
              trip.end_tis = undefined;
              trip.oth_details = undefined;
              trip.trip_status = undefined;
              trip.start_tis = undefined;

              cb(null, trip)
            }
          }).catch(function (err) {
            cb({
              code: 500,
              msg: err
            })
          })
      }]
    }, function (err, results) {
      if(err){
        res.status(err.code).send(err.msg)
      }else{
        res.json(results.get_data)
      }
    })
  }
};

exports.getMap = function (req, res) {

  let id = req.query.id;

  if(!id){
    res.status(400).send('Invalid trip id');
  }else{

    async.auto({
      validate_user: function (cb) {
        getUserShipToPartyCode(req, cb)
      },
      trip: ['validate_user', function (results, cb) {
        const valid_ship_to_party_codes = results.validate_user;

        const query = "SELECT tp.id, tp.start_tis, tp.fk_asset_id, tp.status, tp.waypoints, tp.trip_log, tp.geo_trip_route," +
          " json_build_object('condition',tfc.condition,'description',tfc.description) as failure_condition" +
          " FROM trip_plannings tp " +
          " INNER JOIN assets a on a.id = tp.fk_asset_id" +
          " INNER JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
          " LEFT JOIN trip_failure_conditions tfc on tfc.id = tp.fk_trip_failure_condition_id" +
          " WHERE iea.ship_to_party in " + valid_ship_to_party_codes + " AND tp.new AND tp.id = " + id + " LIMIT 1";

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .spread(function (trip) {
            if(!trip){
              cb({
                code: 404,
                msg: 'Not found'
              })
            }else{
              let assetCurrentLocation = null;
              let actStartTis = null;
              let actEndTis = null;

              if (trip.trip_log) {

                const tl = trip.trip_log;
                const lkl = trip.trip_log.lkLocation;

                assetCurrentLocation = {
                  id: lkl.fk_asset_id,
                  licNo: lkl.lic_plate_no,
                  lat: lkl.lat,
                  lon: lkl.lon,
                  tis: lkl.tis,
                  ign: lkl.ign,
                  spd: lkl.spd
                };
                actStartTis = tl.departure;
                actEndTis = tl.arrival
              }

              processWaypoints(trip, function(err, data) {
                cb(null, {
                  fk_trip_id: trip.id,
                  estPath: utils.validateEstimatedPolyline(trip.geo_trip_route),
                  waypoints: data,
                  fk_asset_id: trip.fk_asset_id,
                  assetLocation: assetCurrentLocation,
                  actStartTis: actStartTis,
                  actEndTis: actEndTis,
                  failure_condition: trip.failure_condition,
                })
              })
            }
          }).catch(function (err) {

            cb({
              code: 500,
              msg: 'Internal server error '+ err
            })
          })
      }],
      actual: ['trip', function (results, cb) {

        const params = results.trip;
        if(!params.actStartTis && !params.actEndTis){
          cb(null, {
            path: null
          })
        }else{
          nuDynamoDb.getAssetLocationFromAPI(params, function (err, data) {
            if(data.length > 0){

              const latlngs = [];
              data.forEach(function (i) {
                latlngs.push([i.lat, i.lon]);

                i.status = computeAssetStatus(i);
                i.duration = 0;
                i.heading = 0
              });
              cb(null, {
                path: PolylineUtil.encode(latlngs),
                points: data
              })
            }else{
              cb(null, {
                path: null,
                points: []
              })
            }
          })
        }
      }],
      mapmatched_polyline: ['trip', function (result, callback) {
        const params = result.trip;
        nuDynamoDb.getTripPolyline(params, function (err, data) {
          callback(null, data)
        })
      }],
    }, function (err, results) {
      if(err){
        res.status(err.code).send(err.msg)
      }else{
        if(err){
          res.status(err.code).send(err.msg)
        }else{
          res.json({
            failure_condition: results.trip.failure_condition,
            estPathPolyline: results.trip.estPath,
            waypoints: results.trip.waypoints,
            assetLocation: results.trip.assetLocation || null,
            actualPath: results.mapmatched_polyline || results.actual.path || null,
          });
        }
      }
    })
  }
};

exports.getNotifications = function (req, res) {

  const limit = req.query.limit || 10;
  const tis = req.query.tis || moment().tz(utils.timezone.default, false).unix();

  async.auto({
    validate_user: function (cb) {
      getUserShipToPartyCode(req, cb)
    },
    get_data: ['validate_user', function (results, cb) {
      const valid_ship_to_party_codes = results.validate_user;

      const query = "SELECT n.msg, n.fk_notif_id as type, n.level as lvl, n.tis," +
        " params::jsonb as rparams, json_build_object('id', iea.fk_t_id)::jsonb as trip" +
        " FROM trip_plannings tp " +
        " INNER JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
        " INNER JOIN notifications n on n.fk_t_id = tp.id " +
        " WHERE n.tis < " + tis + " AND fk_notif_id IN (12,13,14,15,16) AND iea.ship_to_party in " + valid_ship_to_party_codes + " AND tp.new" +
        " GROUP BY tp.id, n.id, iea.id" +
        " ORDER BY n.tis DESC LIMIT " + limit;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function (notifications) {
          notifications.forEach(function (i) {
            i.aid = undefined
          });
          cb(null, notifications);
        }).catch(function (err) {
          cb({
            code: 500,
            msg: 'Internal server error',
            err: err
          })
        });
    }]
  }, function (err, results) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.json(results.get_data)
    }
  })
};