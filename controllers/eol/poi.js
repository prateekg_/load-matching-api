
const moment = require('moment-timezone');
const models = require('../../db/models/index');
const utils = require('../../helpers/util');
const _ = require('lodash');
const POI_CATEGORY = ['general', 'company'];
const file_base_path = __dirname + '/../../temp_reports/';
const Excel = require('exceljs');
const fs = require('fs');
const async = require('async');
const request = require('request');

const xlsDownload = require('../../helpers/downloadHelper');
const tableConfigsHelper = require('../../helpers/table_config');
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../../config/index.js')[env];
const table_name = 'eol-depot-places-table-master';

function convertAssetListToXLS(data, filename, callback) {

  try {
    const workbook = new Excel.Workbook();
    const ws = workbook.addWorksheet('Asset list');
    ws.font = { name: 'Proxima Nova' };
    ws.columns = [
      { width: 15 },
      { width: 15 },
      { width: 40 },
      { width: 40 },
    ];

    ws.addRow(['Asset', 'Load status', 'Shipment No', 'Invoice No']);
    ws.getRow(ws.rowCount).font = { size: 12, bold: true };

    data.forEach(function (i) {
      let shipment_nos = [];
      let invoice_nos = [];
      let load_status = "";

      if (i.trips) {
        const trip = i.trips;
        load_status = trip.tripLoadStatus;

        trip.inprogressTrips.forEach(function (a) {
          if (a.shipmentNo) {
            shipment_nos.push(a.shipmentNo)
          }

          if (a.invoiceNo) {
            invoice_nos.push(a.invoiceNo)
          }
        });
        shipment_nos = shipment_nos.toString().replace('[]/g', '');
        invoice_nos = invoice_nos.toString().replace('[]/g', '')
      }
      ws.addRow([i.licNo, load_status, shipment_nos, invoice_nos])
    });
    ws.views = [{ state: 'frozen', xSplit: 0, ySplit: 1 }];

    // write to excel file
    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback()
      }).catch(function (err) {
        console.log("XLS FILE : ", err);
        callback({
          code: 500,
          msg: 'Internal error'
        })
      });
  } catch (e) {
    console.log("ERROR IN XLS FILE : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

function convertPoiListToXLS(data, filename, callback) {

  try {
    const workbook = new Excel.Workbook();
    const ws = workbook.addWorksheet('Place list');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = [
      {width: 30},
      {width: 15},
      {width: 20},
      {width: 20},
      {width: 20},
      {width: 30},
      {width: 20},
      {width: 20},
      {width: 20},
      {width: 20},
      {width: 15},
    ];

    ws.addRow(['Place','Type', 'Ship to party code', 'CMS or consigner code','state','location', 'Numadic latitude', 'Numadic longitude', 'EOL latitude', 'EOL longitude', 'Assets inside']);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    data.forEach(function (i) {
      ws.addRow([i.name, i.type, i.ship_to_code, i.cms_or_consigner_code, i.state_name, i.addr, i.lat, i.lon, i.company_lat, i.company_lon, i.tot_assets])
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback()
      }).catch(function (err) {
        console.log("XLS FILE : ", err);
        callback({
          code: 500,
          msg: 'Internal error'
        })
      });
  }catch(e){
    console.log("ERROR IN XLS FILE : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

exports.getAllPoiRealtimeStats = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const category = req.query.category;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const last24hrsTis = moment().tz(utils.timezone.default, false).subtract(1, 'days').unix();
  let query;

  if (category === 'company') {
    // company category query
    query = 'SELECT pt.type, count(COALESCE(ps.total_assets,0))::int as tot_assets  FROM pois p' +
            ' INNER JOIN poi_group_mappings pgm on p.id = pgm.fk_poi_id and pgm.connected' +
            ' LEFT JOIN (' +
            ' SELECT aps.fk_poi_id, array_length(array_agg(distinct aps.fk_asset_id),1) as total_assets' +
            ' FROM assets_pois_stats aps ' +
            ' INNER JOIN asset_group_mappings agm on agm.fk_asset_id = aps.fk_asset_id and agm.connected' +
            ' INNER JOIN groups g on g.id = agm.fk_group_id AND ' + groupCondition +
            ' WHERE aps.exit_tis is null AND aps.latest_tis >= ' + last24hrsTis +
            ' GROUP BY aps.fk_poi_id' +
            ' ) ps on ps.fk_poi_id = p.id' +
            ' LEFT JOIN poi_types pt on pt.id = p.typ' +
            ' LEFT JOIN groups g on  g.id = pgm.fk_group_id and g.active AND ' + groupCondition +
            ' WHERE p.active AND  p.priv_typ AND pt.fk_c_id = ' + fk_c_id +
            ' GROUP BY pt.id' +
            ' ORDER BY pt.id'
  } else if (category === 'general') {
    // general category query
    query = 'SELECT pt.type, count(COALESCE(ps.total_assets,0))::int as tot_assets  FROM pois p' +
            ' LEFT JOIN (' +
            ' SELECT aps.fk_poi_id, array_length(array_agg(distinct aps.fk_asset_id),1) as total_assets' +
            ' FROM assets_pois_stats aps ' +
            ' INNER JOIN asset_group_mappings agm on agm.fk_asset_id = aps.fk_asset_id and agm.connected' +
            ' INNER JOIN groups g on g.id = agm.fk_group_id AND ' + groupCondition +
            ' WHERE aps.exit_tis is null AND aps.latest_tis >= ' + last24hrsTis +
            ' GROUP BY aps.fk_poi_id' +
            ' ) ps on ps.fk_poi_id = p.id' +
            ' LEFT JOIN poi_types pt on pt.id = p.typ' +
            ' WHERE p.active AND  p.priv_typ is FALSE AND pt.fk_c_id is null ' +
            ' GROUP BY pt.id' +
            ' ORDER BY pt.id'
  } else {
    return res.status(400).send("invalid poi category")
  }

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .then(function (list) {
      res.json(list)
    }).catch(function (err) {
      console.error(err);
      res.status(500).send()
    })

};

exports.getPoiDetail = function (req, res) {
  const companyId = req.headers.fk_c_id;
  const id = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const last24hrsTis = moment().tz(utils.timezone.default, false).subtract(1, 'days').unix();

  if (isNaN(id)) {
    return res.status(400).send('Invalid POI ID');
  }

  const querySubPart = ' p.company_fields as "companyCodes", pt.company_fields as "poiTypeFields", P.name, p.addr,' +
    ' p.start_tim as "startTime", p.end_tim as "endTime", pt.type, pc.cont_name as owner,' +
    " p.entry_fee as entryFee, P.priv_typ as private, P.is24hours, json_build_object('lat',P.lat ,'lon',P.lon) as coordinates," +
    ' COALESCE(t.tot_assets, 0) as tot_assets' +
    ' FROM pois P' +
    ' INNER JOIN poi_types pt' +
    ' ON pt.id = P.typ' +
    ' LEFT OUTER JOIN poi_contacts pc ON pc.fk_poi_id = P.id' +
    ' LEFT OUTER JOIN (' +
    ' SELECT count(distinct a.id) as tot_assets, aps.fk_poi_id FROM groups g ' +
    ' INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected ' +
    ' INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active ' +
    ' INNER JOIN assets_pois_stats aps on aps.fk_asset_id = a.id and aps.exit_tis is null ' +
    " AND aps.latest_tis >= " + last24hrsTis + " AND aps.fk_poi_id = " + id +
    ' INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id ' +
    ' WHERE g.active AND ' + groupCondition + ' AND aps.fk_poi_id = ' + id +
    ' GROUP BY aps.fk_poi_id' +
    ') t ON P.id = t.fk_poi_id';


  const query = 'SELECT null as fk_c_id, \'[]\' as gids, ' + querySubPart +
    ' WHERE P.active is TRUE AND P.priv_typ = false AND P.id = ' + id + '' +
    ' GROUP BY p.id, pt.id, pc.id, t.tot_assets' +

    ' union all ' +
  // company pois query to check if user has access to the poi

    'SELECT p.fk_c_id, COALESCE(json_agg(DISTINCT g.gid) FILTER (WHERE g.gid IS NOT NULL), \'[]\') as gids, ' + querySubPart +
    ' INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id and pgm.connected' +
    ' INNER JOIN groups g on g.id = pgm.fk_group_id AND g.active AND ' + groupCondition +
    ' WHERE P.active is TRUE AND P.priv_typ = true AND P.fk_c_id = ' + companyId + ' AND P.id = ' + id + '' +
    ' GROUP BY p.id, pt.id, pc.id, t.tot_assets' +

    ' order by fk_c_id desc limit 1';


  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (poiData) {

      if (!poiData) {
        return res.status(404).send('Invalid POI ID');
      }

      poiData.tot_assets = Number(poiData.tot_assets);
      poiData.status = poiData.tot_assets > 0 ? 'Active' : 'Inactive';
      poiData.isOpen = 'open';

      poiData.companyFields = [];
      _.each(poiData.poiTypeFields, function (value, key) {
        poiData.companyFields.push({
          field: value,
          value: poiData.companyCodes ? poiData.companyCodes[key] : "-"
        })
      });

      poiData.poiTypeFields = undefined;
      poiData.companyCodes = undefined;
      poiData.fk_c_id = undefined;

      res.json(poiData);
    }).catch(function (err) {
      console.error(err);
      res.status(500).send();
    })
};

exports.getAllPois = function (req, res, apiType) {

  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit || 2;
  const offset = (req.query.page || 0) * limit;
  const category = req.query.category;
  const poi_type = req.query.type;
  const q = req.query.q ? req.query.q.toLowerCase() : '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  let filterCondition = "";
  const mapBounds = utils.processMapBoundParams(req);
  let mapBoundsCondition = "";
  const last24hrsTis = moment().tz(utils.timezone.default, false).subtract(1, 'days').unix();
  let search_cols = req.query.search;
  let search_col_condition = "";
  let sort = req.query.sort;
  let sort_condition = " pt.id ASC, COALESCE(ps.total_assets,0) DESC";

  const output_format = req.query.format || 'list';
  if(output_format === 'xls' && category === 'company'){
    apiType = 'xls'
  }

  if (POI_CATEGORY.indexOf(category) === -1) {
    return res.status(400).send("Enter poi category")
  }

  if (mapBounds) {
    mapBoundsCondition = " AND ( ST_SetSRID(ST_Point(p.lon,p.lat),4326) && " +
            " ST_MakeEnvelope( " + mapBounds.west + "," + mapBounds.south + "," + mapBounds.east + "," + mapBounds.north + ", 4326) )";
  }

  let poiCondition = ' pt.fk_c_id = ' + fk_c_id + ' AND p.fk_c_id = ' + fk_c_id + ' and ' + groupCondition + ' and pgm.id is not null';

  if (category === 'general') {
    poiCondition = ' p.priv_typ is false'
  } 

  if (poi_type) {
    filterCondition = " AND pt.type ilike '" + poi_type + "'"
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'P.addr',
      client: 'addr'
    }, {
      db: 'P.lat',
      client: 'lat'
    }, {
      db: 'P.lon',
      client: 'lon'
    }, {
      db: 'P.name',
      client: 'name'
    }, {
      db: "COALESCE(to_json(p.company_fields->'shipToCode')::TEXT, to_json(p.company_fields->'shipToParty')::TEXT)",
      client: 'ship_to_code'
    }, {
      db: "COALESCE(to_json(p.company_fields->'consignerCode')::TEXT, to_json(p.company_fields->'cmsCode')::TEXT)",
      client: 'cms_or_consigner_code'
    }, {
      db: "p.state",
      client: 'state_name'
    }, {
      db: "p.company_lat",
      client: 'company_lat'
    }, {
      db: "p.company_lon",
      client: 'company_lon'
    }]
  )

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'COALESCE(ps.total_assets,0)',
      client: 'tot_assets'
    }, {
      db: 'P.name',
      client: 'name'
    }],
    " ORDER BY pt.id ASC, COALESCE(ps.total_assets,0) DESC"
  )

  let query = "WITH assets_in_pois as ( SELECT aps.fk_poi_id, array_length(array_agg(distinct aps.fk_asset_id),1) as total_assets" +
    " FROM assets_pois_stats aps " +
    " INNER JOIN asset_group_mappings agm on agm.fk_asset_id = aps.fk_asset_id and agm.connected" +
    " INNER JOIN groups g on g.id = agm.fk_group_id AND " + groupCondition +
    " WHERE aps.exit_tis is null AND aps.latest_tis >= " + last24hrsTis +
    " GROUP BY aps.fk_poi_id" +
    " )" +
    " SELECT P.id, P.addr, P.name, pt.type, " +
    " COALESCE(p.company_fields->'shipToCode', p.company_fields->'shipToParty') as ship_to_code," +
    " COALESCE(p.company_fields->'consignerCode', p.company_fields->'cmsCode') as cms_or_consigner_code," +
    " COALESCE(ps.total_assets,0) as tot_assets, p.lat, p.lon," +
    " COALESCE(json_agg(DISTINCT g.gid) FILTER (WHERE g.gid IS NOT NULL), '[]') as gids," +
    " geo_loc as \"geoBounds\", CASE WHEN pt.fk_c_id > 0 THEN 'company' ELSE 'general' END as category," +
    " p.state as state_name, p.company_lat, p.company_lon, p.state_code," +
    " count(*) OVER() AS total_count" +
    " FROM pois p" +
    " LEFT JOIN poi_group_mappings pgm on p.id = pgm.fk_poi_id and pgm.connected" +
    " LEFT JOIN ( SELECT * FROM assets_in_pois ) ps on ps.fk_poi_id = p.id" +
    " LEFT JOIN poi_types pt on pt.id = p.typ" +
    " LEFT JOIN poi_contacts PC ON PC.fk_poi_id = p.id" +
    " LEFT JOIN groups g on  g.id = pgm.fk_group_id and g.active" +
    " WHERE p.active AND lower(p.*::TEXT) like '%" + q + "%' AND " + poiCondition + filterCondition + mapBoundsCondition + search_col_condition +
    " GROUP BY p.id, pc.id, pt.id, ps.total_assets " +
    // " ORDER BY (count(distinct a.id))::int DESC" +
    " ORDER BY " + sort_condition;

  if (apiType === 'list') {
    query += " LIMIT " + limit + " OFFSET " + offset;
  }

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .then(function (pois) {
      let total = 0;
      pois.forEach(function (poi) {
        total = Number(poi.total_count);

        poi.geoBounds.geofence = [];
        _.each(poi.geoBounds.coordinates[0], function (coordinates) {
          poi.geoBounds.geofence.push({
            lon: coordinates[0],
            lat: coordinates[1]
          })
        });

        poi.total_count = undefined;
        // poi.geo_loc = undefined
        poi.geoBounds.coordinates = undefined
      });

      if (apiType === 'list') {
        res.json({
          total_count: total,
          data: pois
        })
      }else if(apiType === 'xls'){
        const file_name = "PLACES-" + utils.downloadReportDate() + ".xls";
        const file_path = file_base_path + file_name;
        convertPoiListToXLS(pois, file_name, function (err) {
          if(err){
            res.status(err.code).send(err.msg)
          }else {
            res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      } else {
        res.json(pois)
      }
    }).catch(function (err) {
      console.error(err);
      res.status(500).send()
    })
};

exports.getAssetsInsidePoi = function (req, res) {

  const id = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  let startTis = req.query.start_time;
  let endTis = req.query.end_time;
  const limit = req.query.limit || 10;
  const offset = (req.query.page || 0) * limit;
  const searchKeyword = req.query.q || '';
  const onTrip = req.query.onTrip;
  let trip_condition = "";
  let time_range_condition = "";
  const output_format = req.query.format || 'list';
  let search_cols = req.query.search;
  let search_col_condition = "";
  const last24hrsTis = moment().tz(utils.timezone.default, false).subtract(1, 'days').unix();

  if (isNaN(id)) {
    return res.status(400).send('Invalid POI ID');
  }

  if (startTis && endTis) {
    startTis = moment().tz(utils.timezone.default, false).startOf('day').unix();
    endTis = moment().tz(utils.timezone.default, false).endOf('day').unix();

    time_range_condition = "AND (aps.entry_tis between " + startTis + " AND " + endTis + " OR aps.latest_tis between  " + startTis + " AND " + endTis + ")";
  }

  if (onTrip === 'true' || onTrip === 'false') {
    trip_condition += " AND CAST(json_array_length(atm.trips) as INTEGER) ";
    trip_condition += onTrip === 'true' ? " > 0 " : " = 0 "
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'a.lic_plate_no',
      client: 'licNo'
    }, {
      db: 'iea.invoice_no',
      client: 'trips.invoiceNo'
    }]
  )

  let query = "SELECT * FROM (" +
    " SELECT distinct a.id, a.lic_plate_no as \"licNo\", atp.type, json_agg(json_build_object('id', tp.id, 'tid', tp.tid," +
    " 'fk_asset_id', tp.fk_asset_id,'end_tis',tp.end_tis,'eta',tp.trip_log->'ETA','invoice_no', iea.invoice_no," +
    " 'shipment_no',iea.shipment_no,'delayed_by', tp.trip_log->'delayed_by', 'delayed', tp.trip_log->'delayed'," +
    " 'origin', iea.origin, 'destination', iea.destination))::jsonb as tp, COALESCE(atm.trips::jsonb,'[]') as trips," +
    " json_array_length(COALESCE(atm.trips::json,'[]')) total_trips, count(*) OVER() AS total_count" +
    " FROM groups g" +
    " INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected" +
    " INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active" +
    " INNER JOIN assets_pois_stats aps on aps.fk_asset_id = a.id and exit_tis is null AND latest_tis >= " + last24hrsTis + " AND aps.fk_poi_id = " + id +
    " INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = aps.fk_poi_id" +
    " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id" +
    " LEFT JOIN asset_trip_maps as atm on atm.fk_asset_id = a.id" +
    " LEFT JOIN json_array_elements_text(atm.trips) x ON TRUE" +
    " LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id AND tp.id = to_json(x::json->'id')::text::int" +
    " LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id " +
    " WHERE g.active AND " + groupCondition + " AND aps.fk_poi_id = " + id + " and exit_tis is null " +
    time_range_condition + " AND ( a.lic_plate_no ilike '%" + searchKeyword + "%')" + search_col_condition + trip_condition +
    " GROUP BY a.id, atp.id, atm.id ) d" +
    " ORDER BY d.total_trips DESC";

  if (output_format === 'list') {
    query += " LIMIT " + limit + " OFFSET " + offset
  }


  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT }).then(function (list) {
    let count = 0;
    list.forEach(function (asset) {
      count = Number(asset.total_count);

      const inprogressTrips = [];
      const tp = _.uniqBy(asset.tp, 'id');
      tp.forEach(function (i) {
        if (i.id) {
          inprogressTrips.push({
            id: i.id,
            tid: i.tid,
            invoiceNo: i.invoice_no,
            shipmentNo: i.shipment_no,
            eta: {
              tis: i.end_tis + (i.delayed_by || 0),
              duration: Math.round(i.delayed_by / 60) || 0,
              delayed: i.delayed || false
            }
          })
        }
      });
      asset.trips = {
        transporter: asset.transporter,
        tripLoadStatus: inprogressTrips.length > 0 ? 'Loaded' : 'Unloaded',
        inprogressTrips: inprogressTrips
      };

      asset.transporter = undefined;
      asset.total_count = undefined;
      asset.tp = undefined
    });

    if (output_format === 'xls') {
      const file_name = "PLACE-" + id + "-ASSETS-INSIDE-" + utils.downloadReportDate() + ".xls";
      const file_path = file_base_path + file_name;

      convertAssetListToXLS(list, file_name, function (err) {
        if (err) {
          res.status(err.code).send(err.msg)
        } else {
          res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
          res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
          res.setHeader("Content-Type", "application/vnd.ms-excel");
          res.setHeader("Filename", file_name);

          res.download(file_path, file_name, function (err) {
            if (!err) {
              fs.unlink(file_path)
            }
          })
        }
      })
    } else {
      res.json({
        total_count: count,
        data: list
      })
    }
  }).catch(function (err) {
    console.error(err);
    res.status(500).send('Internal server error')
  })
};

exports.getListOfDepots = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  const query = "SELECT DISTINCT p.id, p.name " +
    " FROM pois p" +
    " INNER JOIN poi_types pt on pt.id = p.typ AND pt.fk_c_id = :fk_c_id AND pt.type = 'Supply Point'" +
    " INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id AND pgm.connected" +
    " INNER JOIN groups g on g.id = pgm.fk_group_id AND g.active " +
    " WHERE p.active AND p.priv_typ AND p.fk_c_id = :fk_c_id AND " + groupCondition;
  models.sequelize.query(query, {replacements: {fk_c_id: fk_c_id},type: models.sequelize.QueryTypes.SELECT})
    .then(function (data) {
      res.json({
        data: data
      })
    }).catch(function (err) {
      console.error(err);
      res.status(500).send()
    });
};

//Search using
exports.getAllTripsOfSupplyPoint = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const tripStatus = req.query.status;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const start_tis = moment().tz(utils.timezone.default, false).subtract(1, 'months').startOf('day').unix();
  const end_tis = moment().tz(utils.timezone.default, false).unix();
  const search_cols = req.query.search;
  let search_col_condition = "";
  const sort = req.query.sort;
  const output_format = req.query.format || 'list';
  let poiId = req.query.poiId;
  const range_cols = req.query.range_filter;
  let range_filter_condition = "";
  let sort_condition = " ORDER BY iea.date_time desc";
  const trip_status_condition = tripStatus ? " AND tp.status = '" + tripStatus + "'" : "";
  let fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';

  if (!poiId) {
    return res.status(400).send('Invalid poi id')
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [
      {
        db: 'iea.invoice_no',
        client: 'invoiceNo'
      },
      {
        db: 'iea.shipment_no',
        client: 'shipmentNo'
      },
      {
        db: 'iea.destination',
        client: 'destination.lname'
      },
      {
        db: 'iea.consignee',
        client: 'CMSCode'
      },
      {
        db: 'iea.ship_to_party',
        client: 'shipToParty'
      },
      {
        db: 'a.lic_plate_no',
        client: 'asset.licNo'
      },
      {
        db: 'COALESCE(iea.add2,c.cname)',
        client: 'transporter.name'
      },
      {
        db: 'COALESCE(iea.add1, c.transporter_code)',
        client: 'transporter.code'
      },
      {
        db: 'iea.consigner',
        client: 'consignerCode'
      },
      {
        db: 'iea.origin',
        client: 'source.lname'
      }
    ]
  );

  sort_condition = utils.processTableColumnSort(
    sort,
    [
      {
        db: 'iea.invoice_no',
        client: 'invoiceNo'
      },
      {
        db: 'iea.shipment_no',
        client: 'shipmentNo'
      },
      {
        db: 'iea.consignee',
        client: 'CMSCode'
      },
      {
        db: 'iea.ship_to_party',
        client: 'shipToParty'
      },
      {
        db: 'a.lic_plate_no',
        client: 'asset.licNo'
      },
      {
        db: 'COALESCE(iea.add2,c.cname)',
        client: 'transporter.name'
      },
      {
        db: 'COALESCE(iea.add1, c.transporter_code)',
        client: 'transporter.code'
      },
      {
        db: 'iea.consigner',
        client: 'consignerCode'
      },
      {
        db: 'tp.start_tis',
        client: 'source.tis'
      },
      {
        db: 'tp.end_tis',
        client: 'destination.tis'
      }
    ],
    sort_condition
  );

  range_filter_condition = utils.processTableDateRangeFilter(
    range_cols,
    [{
      db: 'tp.start_tis',
      client: 'source.tis'
    },
    {
      db: 'tp.end_tis',
      client: 'destination.tis'
    }],
    90 // Approx 3 months data
  );

  if(!range_filter_condition.err){
    range_filter_condition = range_filter_condition.result
  }

  const defaultTimeFrameCondition = " AND ( (tp.start_tis >= " + start_tis + "  AND tp.end_tis <= " + end_tis + ") OR (tp.start_tis >= " + start_tis + "  AND tp.start_tis <= " + end_tis + ") OR (tp.end_tis >= " + start_tis + "  AND tp.end_tis <= " + end_tis + ")) ";

  const searchCondition = searchKeyword.length > 2 ? " AND ( lower(a.lic_plate_no) like '%" + searchKeyword + "%' OR lower(tp.*::text) like '%" + searchKeyword + "%' OR lower(iea.*::TEXT) like '%" + searchKeyword + "%' " +
    " OR lower(c.cname) like '%" + searchKeyword + "%' OR lower(c.transporter_code) like '%" + searchKeyword + "%' " +
    " OR lower(iea.ship_to_party) like '%" + searchKeyword + "%' OR lower(iea.consigner) like '%" + searchKeyword + "%' OR lower(iea.consignee) like '%" + searchKeyword + "%' )" : "";

  let query = "WITH my_assets as ( SELECT DISTINCT a.id from groups g " +
    " INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected" +
    " INNER JOIN assets a on a.id = agm.fk_asset_id and a.active" +
    " WHERE a.active and g.fk_c_id = " + fk_c_id + " and " + groupCondition +
    " )" +
    " SELECT tp.id, '[]' as groups, tp.tid as \"tripId\", json_build_object('operator', c.cname, 'licNo', a.lic_plate_no," +
    " 'id', a.id, 'type', ast.type) as asset, json_build_object('name',COALESCE(iea.add2,c.cname),'code',COALESCE(iea.add1, c.transporter_code)) as transporter, tp.status, tp.waypoints, tp.trip_log, tp.end_tis," +
    " iea.invoice_no as \"invoiceNo\", iea.shipment_no as \"shipmentNo\", iea.date_time invoice_date_time," +
    " iea.ship_to_party as \"shipToParty\", iea.consigner as \"consignerCode\", iea.consignee as \"CMSCode\"," +
    " CASE WHEN count(tul.id) > 0 THEN true ELSE false END as status_updated_manually," +
    " json_build_object('condition',tfc.condition,'description',tfc.description) as failure_condition," +
    " count(tp.id) OVER()::INT AS total_count" +
    " FROM trip_plannings tp " +
    " LEFT JOIN trip_user_logs tul on tul.fk_trip_id = tp.id AND tul.active" +
    " LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
    " INNER JOIN assets a on a.id = tp.fk_asset_id" +
    " LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id" +
    " INNER JOIN pois p ON p.company_fields ->>'consignerCode' = iea.consigner AND p.id = " + poiId +
    " LEFT JOIN companies c on c.id = a.fk_c_id" +
    " LEFT JOIN trip_failure_conditions tfc on tfc.id = tp.fk_trip_failure_condition_id" +
    " WHERE tp.fk_asset_id in (select id from my_assets) AND tp.new AND tp.fk_c_id = " + fk_c_id + " AND p.fk_c_id = " + fk_c_id +
    defaultTimeFrameCondition +
    searchCondition + search_col_condition + range_filter_condition + trip_status_condition +
    " GROUP BY tp.id, c.id, a.id, ast.id, iea.id, tfc.id" +
    sort_condition;

  if(output_format === 'list'){
    query += " LIMIT "+ limit +" OFFSET "+ offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (trips) {
      let total_count = 0;
      trips.forEach(function (i) {
        try {
          total_count = i.total_count;
          i.eta = null;
          i.progress = i.trip_log ? Math.round(i.trip_log.progress) : 0;
          i.totalWaypoints = i.waypoints.length || 0;
          const start_wp = i.waypoints[0];
          i.source = {
            lname: start_wp.lname,
            tis: start_wp.tis
          };
          const end_wp = i.waypoints[i.waypoints.length - 1];
          i.destination = {
            lname: end_wp.lname,
            tis: end_wp.tis
          };

          const tlog = i.trip_log;
          if (tlog && tlog.violations) {
            i.totalViolations = tlog.violations.length
          } else {
            i.totalViolations = 0
          }

          if (tlog && i.status === 'inprogress') {
            i.eta = {
              tis: i.end_tis + (tlog.delayed_by || 0),
              duration: Math.round(tlog.delayed_by / 60) || 0,
              delayed: tlog.delayed || false
            }
          }
          i.tripLoadStatus = 'Unloaded';

          if(i.status === 'inprogress'){
            i.tripLoadStatus = 'Loaded';
            i.status = 'Exited Source Location'

          }else if(i.status === 'scheduled'){
            i.tripLoadStatus = 'Loaded';
            i.status = 'At source location'
          }

          delete i.total_count;
          delete i.waypoints;
          delete i.trip_log;
          delete i.end_tis

        }catch (e){
          console.log(e)
        }
      });

      if(output_format === 'xls'){
        const report_name = 'PLACE-INVOICES';

        const file_name = (report_name + "-" + utils.downloadReportDate()).toString().toUpperCase() + ".xls";
        const file_path = file_base_path + file_name;

        tableConfigsHelper.getTableConfig('eol-place-invoice-list', fk_u_id, 'all', default_columns, function (err, table_config) {

          xlsDownload.convertTableV1ToXLS('all', trips, file_base_path, file_name, table_config, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })
        })

      }else{
        res.json({
          count : total_count,
          data : trips
        });
      }
    }).catch(function (err) {
      console.error(err);
      res.status(500).send()
    })
};

exports.getDepotNotifications = function(req, res){
  let id = Number(req.query.poi_id);
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const lastHour = moment().subtract(1, 'hours').unix();
  const limit = req.query.limit || 10;
  const output_format = req.query.format;
  let q = req.query.q;
  let tableSearchFilter = "";
  const notification_type = req.query.type;
  let notification_type_condition = "";

  if(!id){
    res.status(400).send("Invalid ID")
  }

  // Redirect download to Reports Engine
  if(output_format === 'xls'){
    const options = {
      method: 'GET',
      url: config.server_address.eol_reports_server() + '/v1/eol/download/notifications',
      qs: _.merge(req.query, {format: 'xls', poi_id: id}),
      json: true,
      headers: {
        fk_c_id: req.headers.fk_c_id,
        group_ids: req.headers.group_ids,
        fk_u_id: req.headers.fk_u_id
      }
    };
    return request(options).pipe(res);
  }


  if(q && q.length > 1){
    q = q.toLowerCase();
    tableSearchFilter = "AND ( lower(n.msg) like '%"+ q +"%' OR lower(p.name) like '%"+ q +"%' OR lower(a.lic_plate_no) like '%"+ q +"%' OR lower(p.name) like '%"+ q +"%' OR n.fk_t_id::TEXT like '%"+ q +"%' )";
  }

  if(notification_type){
    // process notification type filter
    notification_type_condition = " AND n.fk_notif_id IN ("+ notification_type +")";
  }

  async.auto({
    valid: function (cb) {
      const query = "SELECT p.id, g.name FROM pois p " +
        " INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id" +
        " INNER JOIN groups g on g.id = pgm.fk_group_id" +
        " WHERE p.id = :id AND pgm.connected AND g.active AND " + groupCondition;

      models.sequelize.query(query, { replacements: {id: id}, type: models.sequelize.QueryTypes.SELECT })
        .spread(function (poi) {
          if(poi){
            cb()
          }else{
            cb({code: 400, msg: "Invalid poi id", err:null})
          }
        })
        .catch(function (err) {
          cb({code: 500, msg: "Internal server error", err:err})
        })
    },
    notifications: ['valid', function (results, cb) {
      const query = "SELECT n.msg, n.fk_notif_id as type, n.level as lvl, n.tis, params as rparams, is_good, anomaly, count(*) OVER()::INT AS total_count" +
        " FROM notifications n " +
        " INNER JOIN groups g ON g.id = n.fk_g_id" +
        " LEFT JOIN pois p on p.id = n.fk_poi_id " +
        " LEFT JOIN assets a on a.id = n.fk_asset_id " +
        " WHERE p.id = :fk_poi_id AND " + groupCondition + " AND fk_notif_id NOT IN (5,8) AND n.tis >= " + lastHour + tableSearchFilter + notification_type_condition +
        " ORDER BY n.tis DESC ";

      models.sequelize.query(query, { replacements: {fk_poi_id: id, limit: limit}, type: models.sequelize.QueryTypes.SELECT })
        .then(function (notifications) {
          const total_rows = notifications.length > 0 ? notifications[0].total_count : 0;
          notifications.forEach(function (item) {
            item.total_count = undefined
          });
          cb(null, {count: total_rows, data: notifications})
        }).catch(function (err) {
          cb({code: 500, msg: "Internal server error", err:err})
        });
    }]
  }, function (err, results) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.json(results.notifications);
    }
  })
};


exports.getDepotTruckTableConfig = function (req, res) {
  // API to get user column preferences

  const fk_u_id = req.headers.fk_u_id;

  const query = "SELECT uc.config FROM wv_user_configs uc" +
    " INNER JOIN wv_table_types tt on tt.id = uc.fk_wv_table_type_id" +
    " WHERE uc.fk_user_id = :fk_u_id AND tt.table_name = :table_name";


  models.sequelize.query(query, {replacements:{fk_u_id: fk_u_id, table_name: table_name}, type: models.sequelize.QueryTypes.SELECT}).spread(function (user_config) {
    const DEFAULT_COLUMNS = [{"key":"lic_plate_no","selected":true},{"key":"health","selected":true},{"key":"lname","selected":true},{"key":"tis","selected":true},{"key":"transporter_code","selected":true},{"key":"transporter_name","selected":true},{"key":"eta","selected":true},{"key":"load_status","selected":true},{"key":"origin","selected":true},{"key":"destination","selected":true},{"key":"invoice_no","selected":true},{"key":"shipment_no","selected":true},{"key":"material_code","selected":true},{"key":"last_trip_tis","selected":true},{"key":"entry_tis","selected":true},{"key":"dwell_time","selected":true}];
  
    const DEFAULT_CONFIG = {
      tabs: {
        "all": DEFAULT_COLUMNS,
        "incoming": DEFAULT_COLUMNS,
        "inside":DEFAULT_COLUMNS,
        "outgoing":DEFAULT_COLUMNS,
        "at_ro":DEFAULT_COLUMNS
      }
    };
    const user_config_final = {};
    user_config_final.config = user_config && user_config.config && user_config.config.tabs ? user_config.config : DEFAULT_CONFIG;
    res.json(user_config_final)
  }).catch(function () {
    res.status(500).send()
  })
};
  
exports.updateDepotTruckTableConfig = function (req, res) {
  // API to crete/update user column preferences
  const fk_u_id = req.headers.fk_u_id;
  const data = req.body;

  async.auto({
    table: function (cb) {
      const query = "SELECT id from wv_table_types WHERE table_name = :table_name";
      models.sequelize.query(query, {replacements: {table_name: table_name}, type: models.sequelize.QueryTypes.SELECT}).spread(function (data) {
        if(data){
          cb(null, data)
        }else{
          cb({code: 500, msg: 'Table configurations not found'})
        }
      }).catch(function (err) {
        cb({code: 500, msg: 'Internal server error', err: err})
      })
    },
    config: ['table', function (results, cb) {
      const table_type_id = results.table.id;

      const user_config = {
        fk_user_id: fk_u_id,
        fk_wv_table_type_id: table_type_id,
        config: data.config || {}
      };

      models.wv_user_config.findOrCreate({
        where: _.pick(user_config,['fk_user_id','fk_wv_table_type_id']),
        defaults: user_config
      }).spread(function (user_config, created) {
        if (!created) {
          // not created
          if(data && data.config){
            user_config.config = data.config
          }
          user_config.save().then(function () {
            cb(null, 'sucessfully updated')
          }).catch(function (err) {
            cb({code: 500, msg: 'Internal server error', err: err})
          })
        } else {
          cb(null, 'Successfully created')
        }
      }).catch(function (err) {
        cb({code: 500, msg: 'Internal server error', err: err})
      })
    }]
  }, function (err) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.json({ status: 'Configurations updated'})
    }
  })
};