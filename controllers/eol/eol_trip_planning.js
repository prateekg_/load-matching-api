const TRIP_STATUS = ['completed','inprogress','scheduled','open','all'];
const utils = require('../../helpers/util');
const models = require('../../db/models');
const _ = require('lodash');
const async = require('async');
const moment = require('moment');
const fs = require('fs');
const Excel = require('exceljs');
const file_base_path = __dirname + '/../../temp_reports/';
const EOL_TRIP_TABLE = 'eol-trip-list';
const EOLTripService = require("../../services/eol_trip_service");
const DEFAULT_DOWNLOAD_HELPER = require("../../helpers/downloadHelper");
const TABLE_CONFIG = require("../../helpers/table_config");

function convertTripListToXLS(trip_status, data, filename, user_columns, callback){
  try {
    let date_time_format = 'DD-MM-YY, HH:mm';
    user_columns = _.sortBy(user_columns, ['order']);
    let column_headers = [];
    let column_widths = [];
    user_columns.forEach(function (i) {
      if(i.selected){
        column_headers.push(i.name.toLocaleUpperCase());
        column_widths.push({width: 20})
      }
    });

    const workbook = new Excel.Workbook();
    const ws = workbook.addWorksheet('Trip list');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = column_widths;
    ws.addRow(column_headers);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    data.forEach(function (i) {
      let row = [];

      user_columns.forEach(function (col) {
        if(col.selected){
          try {
            let value;
            let nested_keys = col.key.split('.');
            if (nested_keys.length > 1) {
              if(i[nested_keys[0]] && i[nested_keys[0]][nested_keys[1]]){
                value = i[nested_keys[0]][nested_keys[1]]
              }
            } else {
              if (col.key === 'status_updated_manually') {
                value = i.status_updated_manually ? "Marked complete" : ""
              }else{
                value = i[col.key]
              }
            }

            switch (col.format) {
            case 'datetime':
              value = Number(value) ? moment.unix(value).tz(utils.timezone.default, false).format(date_time_format) : '-';
              break;
            case 'duration':
              value = utils.utilDurationFormatMDHM(value);
              break;
            }
            row.push(value)
          }catch(e){
            console.log("ERROR processing row data : ", e)
          }
        }
      });

      ws.addRow(row)
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    if(!fs.existsSync(file_base_path)){
      fs.mkdirSync(file_base_path)
    }

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, data)
      }).catch(function (err) {
        console.error(err);
        callback({
          code: 500,
          msg: 'Internal error'
        })
      });
  }catch(e){
    console.log("XLS ERROR : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

function getEolStatus(tripStatus) {
  if(tripStatus === 'scheduled'){
    tripStatus = 'At source location'
  }else if(tripStatus === 'inprogress'){
    tripStatus = 'Exited source location'
  }
  return tripStatus
}

/**
 * Note: if you remove/ add column/keys from json response object please update the wv_table_types default_config
 * @param req
 * @param res
 */
exports.getEolWvTrips = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const search_keyword = req.query.q ? req.query.q.toLowerCase() : '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const trip_status = req.query.status;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const search_cols = req.query.search;
  let search_col_condition = "";
  const sort = req.query.sort;
  const output_format = req.query.format || 'list';
  let sort_condition = " ORDER BY tp.id DESC ";
  const range_cols = req.query.range_filter;
  let range_filter_condition = "";
  const max_data_tis = moment().subtract(3, 'months').unix();
  let status_filter = req.query.status_filter;
  let status_filter_condition = "";
  let fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.licNo'
    }, {
      db: 'c.cname',
      client: 'asset.operator'
    }, {
      db: 'tp.id',
      client: 'tripId'
    },{
      db: 'c.transporter_code',
      client: 'transporter.code'
    },{
      db: 'iea.consignee',
      client: 'CMSCode'
    },{
      db: 'iea.ship_to_party',
      client: 'shipToParty'
    },{
      db: 'iea.consigner',
      client: 'consignerCode'
    },{
      db: 'iea.invoice_no',
      client: 'invoiceNo'
    },{
      db: 'iea.shipment_no',
      client: 'shipmentNo'
    },{
      db: 'tp.status',
      client: 'status'
    },{
      db: 'iea.route_id',
      client: 'route_id'
    },{
      db: 'iea.material_code',
      client: 'material_code'
    },{
      db: 'iea.origin',
      client: 'source.lname'
    },{
      db: 'iea.destination',
      client: 'destination.lname'
    }]
  );

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.licNo'
    }, {
      db: 'c.cname',
      client: 'asset.operator'
    },{
      db: 'c.transporter_code',
      client: 'transporter.code'
    },{
      db: 'iea.consignee',
      client: 'CMSCode'
    },{
      db: 'iea.ship_to_party',
      client: 'shipToParty'
    },{
      db: 'iea.consigner',
      client: 'consignerCode'
    },{
      db: 'iea.invoice_no',
      client: 'invoiceNo'
    },{
      db: 'iea.shipment_no',
      client: 'shipmentNo'
    },{
      db: 'iea.distance',
      client: 'sap_distance'
    }],
    sort_condition
  );

  range_filter_condition = utils.processTableDateRangeFilter(
    range_cols,
    [{
      db: "tl.departure",
      client: 'source.tis'
    },{
      db: "tl.arrival",
      client: 'destination.tis'
    },{
      db: 'tp.start_tis',
      client: 'invoice_tis'
    },{
      db: 'EXTRACT(EPOCH FROM tp."createdAt")::INT',
      client: 'created_tis'
    }],
    90 // Approx 3 months data
  );

  if(range_filter_condition.err){
    return res.status(range_filter_condition.err.code).send(range_filter_condition.err.msg)
  }else{
    range_filter_condition = range_filter_condition.result
  }

  if(TRIP_STATUS.indexOf(trip_status) === -1){
    return res.status(400).send("Invalid trip status")
  }
  let trip_status_condition = trip_status === 'all' ? "" : ` AND tp.status = '${trip_status}'`;


  if(status_filter && trip_status === 'all'){
    status_filter = typeof  status_filter === 'string' ? [status_filter] : status_filter;
    status_filter = _.intersection(status_filter, _.pull(_.clone(TRIP_STATUS), 'all'));

    status_filter && status_filter.forEach(function (i) {
      if(status_filter_condition.length > 0){
        status_filter_condition += ` OR tp.status = '${i}'`
      }else{
        status_filter_condition += ` tp.status = '${i}'`
      }
    });
    status_filter_condition = status_filter && status_filter_condition ? ` AND ( ${status_filter_condition} )` : ""
  }

  let timeFrameCondition = (trip_status === 'completed' || trip_status === 'open' || trip_status === 'all')  ? ` AND tp.start_tis >= ${max_data_tis}` : '';

  let searchCondition = "";
  if(search_keyword.length > 2){
    searchCondition = ` AND ( 
                            lower(a.lic_plate_no)           like $search_keyword OR 
                            lower(tp.id::text)              like $search_keyword OR 
                            lower(iea.invoice_no::text)     like $search_keyword OR 
                            lower(iea.shipment_no::text)    like $search_keyword OR 
                            lower(c.cname)                  like $search_keyword OR 
                            lower(c.transporter_code)       like $search_keyword OR 
                            lower(iea.ship_to_party)        like $search_keyword OR 
                            lower(iea.consigner)            like $search_keyword OR 
                            lower(iea.consignee)            like $search_keyword OR 
                            lower(iea.route_id)             like $search_keyword OR 
                            lower(iea.material_code)        like $search_keyword OR 
                            lower(iea.origin)               like $search_keyword OR 
                            lower(iea.destination)          like $search_keyword )`
  }

  let tripOwnersCondition = `AND (a.fk_c_id = ${fk_c_id} or tp.fk_c_id = ${fk_c_id})`;
  let limit_condition = output_format === 'list' ? ` LIMIT ${limit} OFFSET ${offset}` : "";
  let total_count_condition = " count(tp.id) OVER()::INT ";

  if((trip_status === 'completed' || trip_status === 'open' || trip_status === 'all') && !search_keyword && !search_col_condition && !range_filter_condition){
    total_count_condition = limit;
    if(output_format === 'xls'){
      limit_condition = ` LIMIT ${limit}`
    }
  }

  let query  =
        `WITH my_assets as ( 
            SELECT DISTINCT a.id FROM groups g
            INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
            INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
            WHERE a.active and g.fk_c_id = ${fk_c_id} and ${groupCondition}
         )
         
        SELECT 
        tp.id, 
        tp.tid as "tripId", 
        tp.ext_tid as external_trip_id,
        json_build_object('operator', c.cname, 'licNo', a.lic_plate_no, 'id', a.id, 'type', ast.type) as asset,
        json_build_object('name',COALESCE(iea.add2,c.cname),'code',COALESCE(iea.add1, c.transporter_code)) as transporter,
        tp.status, 
        tp.waypoints, 
        tp.end_tis,
        tp.shipment_no, 
        iea.invoice_no as "invoiceNo", 
        iea.shipment_no as "shipmentNo",
        iea.ship_to_party as "shipToParty",
        iea.consigner as "consignerCode", 
        iea.consignee as "CMSCode",
        iea.route_id,
        iea.origin,
        iea.material_code,
        iea.destination,
        (
            SELECT CASE WHEN count(tul.id) > 0 THEN true ELSE false END
            FROM trip_user_logs tul where tul.fk_trip_id = tp.id AND tul.active
        ) as status_updated_manually,
        tp.start_tis invoice_tis,
        tl.distance actual_distance,
        iea.distance sap_distance,
        tl.progress progress,
        tl.departure tp_start_tis,
        tl.arrival tp_end_tis,
        tl.delayed_by tp_delayed_by,
        tl.delayed tp_delayed,
        tl.time_taken actual_duration,
        EXTRACT(EPOCH FROM tp."createdAt")::INT created_tis,
         ${total_count_condition} AS total_count
          FROM trip_plannings tp 
          LEFT JOIN trip_logs tl on tl.fk_trip_id = tp.id
         LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id
         INNER JOIN assets a on a.id = tp.fk_asset_id
         LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id
         LEFT JOIN companies c on c.id = a.fk_c_id
         WHERE tp.new AND tp.active AND a.id in (select id from my_assets) 
         ${
  tripOwnersCondition +
         timeFrameCondition +
         searchCondition +
         search_col_condition +
         trip_status_condition +
         range_filter_condition +
         status_filter_condition
}
        GROUP BY tp.id, c.id, a.id, ast.id, iea.id, tl.fk_trip_id
        ${sort_condition + limit_condition}`;

  models.sequelize.query(query, {
    bind:{
      search_keyword: `%${search_keyword}%`
    },
    type: models.sequelize.QueryTypes.SELECT})
    .then(function (trips) {
      let total_count = 0;
      trips.forEach(function (i) {
        try {
          total_count = i.total_count;
          i.eta = null;
          i.progress = Math.round(i.progress || 0);
          i.totalWaypoints = i.waypoints.length || 0;
          const start_wp = i.waypoints[0];
          i.source = {
            lname: i.origin || start_wp.lname,
            tis: i.tp_start_tis > 0 ? i.tp_start_tis : null
          };
          const end_wp = i.waypoints[i.waypoints.length - 1];
          i.destination = {
            lname: i.destination || end_wp.lname,
            tis: i.status === 'completed' && i.tp_end_tis > 0 ? i.tp_end_tis : null
          };
          if (i.status === 'inprogress') {
            i.eta = {
              tis: i.end_tis + (i.tp_delayed_by || 0),
              duration: Math.round(i.tp_delayed_by / 60) || 0,
              delayed: i.tp_delayed || false
            }
          }
          i.tripLoadStatus = i.status === 'inprogress' || i.status === 'scheduled' ? 'Loaded' : 'Unloaded';
          i.status_eol = getEolStatus(i.status);
          i.sap_distance = Number(Number(i.sap_distance).toFixed(2));
          // Actual distance multiplied by 2 .. to make it round trip,
          // because SAp provides round trip kms
          i.actual_distance = Number(((i.actual_distance/1000)*2).toFixed(2));
          i.actual_duration = Math.round(i.actual_duration / 60)

        }catch (e){
          console.log(e)
        }
        delete i.total_count;
        delete i.waypoints;
        delete i.trip_log;
        delete i.end_tis;
        delete i.tp_start_tis;
        delete i.tp_end_tis;
        delete i.tp_delayed;
        delete i.tp_delayed_by;
        delete i.origin;
      });

      if(output_format === 'xls'){
        const report_name = getEolStatus(trip_status);
        const file_name = ("TRIPS-LIST-" + _.kebabCase(report_name) + "-" + utils.downloadReportDate()).toString().toUpperCase() + ".xls";
        const file_path = file_base_path + file_name;

        EOLTripService.getUserTripTableConfig(default_columns,fk_c_id, fk_u_id, function (err, data) {
          if(err){
            return res.status(500).send()
          }else{
            convertTripListToXLS(trip_status, trips, file_name, data.config.tabs[trip_status], function (err) {
              if(err){
                res.status(err.code).send(err.msg)
              }else {
                res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
                res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
                res.setHeader("Content-Type", "application/vnd.ms-excel");
                res.setHeader("Filename", file_name);
                res.download(file_path, file_name, function (err) {
                  if (!err) {
                    fs.unlink(file_path)
                  }
                })
              }
            })
          }
        })
      }else{
        res.json({
          count : total_count,
          data : trips
        });
      }
    }).catch(function (err) {
      res.status(500).send(err)
    })
};

exports.getEolRealtimeStats = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  async.auto({
    total_assets: function (callback) {
      const query = "SELECT COUNT(DISTINCT a.id)::INT from groups g " +
        " INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected" +
        " INNER JOIN assets a on a.id = agm.fk_asset_id and a.active" +
        " WHERE a.active and g.fk_c_id = " + fk_c_id + " and " + groupCondition;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (data) {
          callback(null, data.count)
        }).catch(function (err) {
          callback(err)
        })
    },
    assets: ['total_assets',function (results, callback) {

      const query = "SELECT a.id as fk_asset_id, json_agg(json_build_object('id', tp.id, 'start_tis', tp.start_tis," +
        " 'delayed',tp.trip_log->'delayed','status', tp.status, 'eta', tp.trip_log->'ETA')) as trips" +
        " FROM groups g  LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected" +
        " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active" +
        " LEFT JOIN asset_trip_maps as atm on atm.fk_asset_id = a.id" +
        " LEFT JOIN json_array_elements_text(atm.trips) x ON TRUE" +
        " LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id AND tp.id = to_json(x::json->'id')::text::int" +
        " WHERE g.active AND " + groupCondition + " AND a.active AND tp.new AND ( tp.status = 'scheduled' OR tp.status = 'inprogress' )" +
        " GROUP BY a.id";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function (data) {
          let assetOnTrip = 0;

          data.forEach(function (i) {
            const trips = _.uniqBy(_.filter(i.trips, 'id'), 'id');

            if(trips.length > 0){
              assetOnTrip++
            }
          });

          const total_assets = results.total_assets;

          callback(null,{
            assetsAssigned: assetOnTrip,
            assetsUnassigned: Math.abs(assetOnTrip - total_assets),
            totalAssets: total_assets
          });
        }).catch(function (err) {
          callback(err)
        })
    }],
    trips: function (callback) {
      const query = "SELECT tp.fk_asset_id, json_agg(g.id) as gids, tp.status, tp.start_tis, tp.trip_log->'ETA' as eta FROM trip_plannings tp " +
        " INNER JOIN assets a on a.id = tp.fk_asset_id" +
        " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
        " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
        " LEFT JOIN companies c on c.id = a.fk_c_id" +
        " WHERE g.active AND (a.fk_c_id = " + fk_c_id + " or tp.fk_c_id = " + fk_c_id + ") AND " + groupCondition +
        " AND tp.new AND ( tp.status = 'scheduled' OR tp.status = 'inprogress' ) AND tp.active" +
        " GROUP BY tp.id";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function (data) {
          let totalOngoing = 0;
          let totalScheduled = 0;
          const assetsDelayed = [];
          const currentTime = moment().unix();

          data.forEach(function (i) {
            if(i.status === 'inprogress'){
              totalOngoing++;
              if(i.delayed){
                assetsDelayed.push(i.fk_asset_id)
              }else if(i.eta && i.eta > 0){
                assetsDelayed.push(i.fk_asset_id)
              }
            }else if(i.status === 'scheduled'){
              totalScheduled++;
              if(i.start_tis < currentTime){
                assetsDelayed.push(i.fk_asset_id)
              }
            }
          });
          callback(null, {
            ongoing: totalOngoing,
            scheduled: totalScheduled,
            delayed: _.size(_.uniq(_.compact(assetsDelayed)))
          })
        }).catch(function (err) {
          callback(err)
        })
    }
  }, function (err, results) {
    if(err){
      res.status(500).send()
    }else{
      res.json(_.merge(results.assets, results.trips))
    }
  })
};

exports.getTableConfig = function (req, res) {
// API to get user column preferences

  let fk_c_id = req.headers.fk_c_id;
  let fk_u_id = req.headers.fk_u_id;

  EOLTripService.getUserTripTableConfig(false, fk_c_id, fk_u_id, function (err, data) {
    if(err){
      return res.status(500).send()
    }else{
      return res.json(data)
    }
  })
};

exports.updateTableConfig = function (req, res) {
// API to crete/update user column preferences
  let fk_u_id = req.headers.fk_u_id;
  let data = req.body;

  async.auto({
    table: function (cb) {
      let query = "SELECT id from wv_table_types WHERE table_name = :table_name";
      models.sequelize.query(query, {replacements: {table_name: EOL_TRIP_TABLE}, type: models.sequelize.QueryTypes.SELECT}).spread(function (data) {
        if(data){
          cb(null, data)
        }else{
          cb({code: 500, msg: 'Table configurations not found'})
        }
      }).catch(function (err) {
        cb({code: 500, msg: 'Internal server error', err: err})
      })
    },
    config: ['table', function (results, cb) {
      let table_type_id = results.table.id;

      let user_config = {
        fk_user_id: fk_u_id,
        fk_wv_table_type_id: table_type_id,
        config: data.config || {}
      };

      models.wv_user_config.findOrCreate({
        where: _.pick(user_config,['fk_user_id','fk_wv_table_type_id']),
        defaults: user_config
      }).spread(function (user_config, created) {
        if (!created) {
          if(data && data.config){
            user_config.config = data.config
          }
          user_config.save().then(function () {
            cb(null, 'sucessfully updated')
          }).catch(function (err) {
            cb({code: 500, msg: 'Internal server error', err: err})
          })
        } else {
          cb(null, 'Successfully created')
        }
      }).catch(function (err) {
        cb({code: 500, msg: 'Internal server error', err: err})
      })
    }]
  }, function (err) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.json({ status: 'Configurations updated'})
    }
  })
};

exports.getRoutes = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const search_keyword = req.query.q ? req.query.q.toLowerCase() : '';
  const start_tis = req.query.start_tis ? req.query.start_tis : moment().startOf('day').unix();
  const end_tis = req.query.end_tis ? req.query.end_tis : moment().endOf('day').unix();
  const search_cols = req.query.search;
  let search_col_condition = "";
  const sort = req.query.sort;
  const output_format = req.query.format || 'list';
  let sort_condition = " ORDER BY d.total_trips DESC NULLS LAST"; // default sort condition
  let fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';
  let limit_condition = `LIMIT ${limit} OFFSET ${offset}`;

  if(output_format === 'xls'){
    limit_condition = ""
  }

  // validate date range to be less then 1 month
  const start_time = moment.unix(start_tis), end_time = moment.unix(end_tis);

  if(Math.floor(Math.abs(moment.duration(start_time.diff(end_time)).asDays()))>32){
    return res.status(400).send("Time range exceeds more than a month")
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'route_id',
      client: 'route_id'
    }, {
      db: 'consigner',
      client: 'consigner'
    }, {
      db: 'consignee',
      client: 'consignee'
    },{
      db: 'origin',
      client: 'origin'
    },{
      db: 'destination',
      client: 'destination'
    },{
      db: 'ship_to_party',
      client: 'ship_to_party'
    }]
  );

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'total_trips',
      client: 'total_trips'
    }, {
      db: 'total_trips_scheduled',
      client: 'total_trips_scheduled'
    },{
      db: 'total_trips_inprogress',
      client: 'total_trips_inprogress'
    },{
      db: 'total_trips_completed',
      client: 'total_trips_completed'
    },{
      db: 'total_trips_open',
      client: 'total_trips_open'
    },{
      db: 'avg_speed',
      client: 'avg_speed'
    },{
      db: 'avg_distance',
      client: 'avg_distance'
    },{
      db: 'avg_stops',
      client: 'avg_stops'
    },{
      db: 'avg_violations',
      client: 'avg_violations'
    },{
      db: 'sap_distance',
      client: 'sap_distance'
    },{
      db: 'est_distance',
      client: 'est_distance'
    },{
      db: 'est_time',
      client: 'est_time'
    },{
      db: 'distance_difference',
      client: 'distance_difference'
    },{
      db: 'trip_completion_rate',
      client: 'trip_completion_rate'
    }],
    sort_condition
  );

  let tableSearchCondition = "";
  if(search_keyword.length > 2){
    tableSearchCondition = ` AND ( 
                            lower(route_id)         like '%${search_keyword}%' OR 
                            lower(consigner)        like '%${search_keyword}%' OR 
                            lower(consignee)        like '%${search_keyword}%' OR 
                            lower(origin)           like '%${search_keyword}%' OR 
                            lower(destination)      like '%${search_keyword}%' OR
                            lower(ship_to_party)    like '%${search_keyword}%' )`
  }

  let query = `
    WITH routes_stats AS (
        SELECT iea.route_id, 
        coalesce(count(tp.id),0)::INT as total_trips,
        coalesce(count(case when tp.status = 'scheduled' THEN 1 END),0)::INT as total_trips_scheduled,
        coalesce(count(case when tp.status = 'inprogress' THEN 1 END),0)::INT as total_trips_inprogress,
        coalesce(count(case when tp.status = 'completed' THEN 1 END),0)::INT as total_trips_completed,
        coalesce(count(case when tp.status = 'open' THEN 1 END),0)::INT as total_trips_open,
        coalesce(ROUND(avg(case when tl.avg_speed > 0 and tp.status = 'completed' then tl.avg_speed END)),0)::INT as avg_speed, 
        coalesce(ROUND(avg(case when tl.distance > 0 and tp.status = 'completed' then tl.distance end)/1000), 0)::INT as avg_distance, 
        coalesce(ROUND(avg(tl.stops)),0)::INT as avg_stops, 
        coalesce(ROUND(avg(json_array_length(tl.violations)-1)),0)::INT as avg_violations 
        from trip_plannings tp 
        left join trip_logs tl on tl.fk_trip_id = tp.id
        inner join input_essar_a iea on iea.fk_t_id = tp.id
        where tp.start_tis between ${start_tis} and ${end_tis}
        group by iea.route_id
    )
    
    SELECT *, count(*) over() as total_count
        FROM (
            SELECT erv.*,
            	total_trips, total_trips_scheduled, total_trips_inprogress, total_trips_completed, total_trips_open, avg_speed, avg_distance, avg_stops, avg_violations,
                avg_distance - (sap_distance/2) as distance_difference,
                coalesce(((total_trips_completed * 100) /stats.total_trips),0)::INT as trip_completion_rate
            FROM             
                eol_routes_view erv 
            LEFT JOIN 
            (
                SELECT * FROM routes_stats   
            ) stats on stats.route_id = erv.route_id
        ) d WHERE true ${tableSearchCondition} ${search_col_condition}
        ${sort_condition}
        
        ${limit_condition}`;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
    .then(function (routes) {
      try {
        let total_count = 0;

        routes.forEach(function (route) {
          total_count = Number(route.total_count);
          route.est_distance = Number(Number((route.est_distance / 1000) || 0).toFixed(1));
          route.distance_difference = route.total_trips_completed > 0 ? route.distance_difference : null;
          delete route.total_count;
        });

        if(output_format === 'xls'){
          let file_name = "ROUTE-LIST-"+ fk_c_id +"-"+ moment().unix() + ".xls";
          let file_path = file_base_path + file_name;
          let table_name = `eol-route-list`;

          TABLE_CONFIG.getTableConfig(table_name, fk_u_id, 'all', default_columns, function (err, table_config) {
            DEFAULT_DOWNLOAD_HELPER.convertTableV1ToXLS('all', routes, file_base_path, file_name, table_config, function (err) {
              if (err) {
                res.status(err.code).send(err.msg)
              } else {
                res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
                res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
                res.setHeader("Content-Type", "application/vnd.ms-excel");
                res.setHeader("Filename", file_name);

                res.download(file_path, file_name, function (err) {
                  if (!err) {
                    fs.unlink(file_path)
                  }
                })
              }
            })

          })
        } else {
          res.json({
            count: total_count,
            data: routes
          });
        }
      }catch(e){
        console.error(e);
        res.status(500).send()
      }
    })
    .catch(function (err) {
      console.error(err);
      res.status(500).send()
    })

};