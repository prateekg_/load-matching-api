const models = require('../../db/models/index');
const async = require('async');
const utils = require('../../helpers/util');
const APP_LOGGER = require('../../helpers/logger');
const LOGGER = APP_LOGGER.logger;
const nuRedis = require('../../helpers/nuRedis');
const auth = require(process.cwd() + '/authorization');

exports.getAssets = function (req, res) {

  const searchKeyword = req.query.q || '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  let query =
  ` SELECT a.id, a.lic_plate_no as "licNo", json_build_object('id', c.id, 'name', c.cname) as operator, atp.type,
    count(*) OVER()::INT AS total_count
    FROM groups g
    LEFT JOIN asset_group_mappings agm on agm.fk_group_id = g.id
    LEFT JOIN assets a on a.id = agm.fk_asset_id
    LEFT JOIN companies c on c.id = a.fk_c_id
    LEFT JOIN asset_types atp on atp.id = a.fk_ast_type_id
    WHERE ${groupCondition} AND a.lic_plate_no ilike '%${searchKeyword}%'
    LIMIT ${limit} OFFSET ${offset}`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    let total_rows = 0;

    data.forEach(function (i) {
      total_rows = i.total_count;
      i.total_count = undefined
    });

    res.json({
      total_count: total_rows,
      data: data
    })
  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send()
  })
};

exports.getTyres = function (req, res) {

  const searchKeyword = req.query.q || '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const fk_asset_id = req.query.asset_id; // asset primary key

  let query =
  ` SELECT d.*, count(*) OVER()::INT AS total_count FROM (
      SELECT distinct(t.id) as id, t.tyre_id
      FROM groups g
      LEFT JOIN tyre_group_mappings tgm on tgm.fk_group_id = g.id AND tgm.connected
      INNER JOIN tyres t on t.id = tgm.fk_tyre_id AND t.active
      LEFT JOIN (
        SELECT atm.fk_asset_id, atm.fk_tyre_id, atm.connected
        FROM asset_tyre_mappings atm
        INNER JOIN (
          SELECT max(tis), fk_tyre_id FROM asset_tyre_mappings atm GROUP BY atm.fk_tyre_id
        ) atm1 on atm1.fk_tyre_id = atm.fk_tyre_id and atm1.max = atm.tis
      ) atm on atm.fk_tyre_id = t.id
      WHERE ${groupCondition} AND COALESCE(atm.connected, false) = false AND t.tyre_id ilike '%${searchKeyword}%'
    ) d
    LIMIT ${limit} OFFSET ${offset}`;

  if(fk_asset_id){
    // get only tyres mounted on assets

    query = 
    ` SELECT d.*, count(*) OVER()::INT AS total_count FROM (
        SELECT distinct(t.id) as id, t.tyre_id
        FROM groups g
        LEFT JOIN tyre_group_mappings tgm on tgm.fk_group_id = g.id AND tgm.connected
        INNER JOIN tyres t on t.id = tgm.fk_tyre_id AND t.active
        LEFT JOIN asset_tyre_mappings atm on atm.fk_tyre_id = t.id AND atm.connected
        LEFT JOIN assets a on a.id = atm.fk_asset_id AND a.active AND atm.fk_asset_id = ${fk_asset_id}
        WHERE ${groupCondition} AND a.id = ${fk_asset_id} AND t.tyre_id ilike '%${searchKeyword}%'
      ) d
      LIMIT ${limit} OFFSET ${offset}`;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    let total_rows = 0;

    data.forEach(function (i) {
      total_rows = i.total_count;
      i.total_count = undefined
    });

    res.json({
      total_count: total_rows,
      data: data
    })
  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send()
  })
};


function findMatchedWord(pattern, data) {

  let result = '';
  pattern = pattern.toLowerCase();
  data = data.replace(/['"(){}]+/g, '');
  data = data.split(',');

  data.forEach(function (i) {
    if(i.toLowerCase().indexOf(pattern) !== -1){
      return result = i
    }
  });
  return result
}

function appendModulesToSearch(userFeatureList, fk_c_id, groupCondition, keyword) {
  let query = "";
  if(auth.FEATURE_LIST.hasTrips(userFeatureList)){
    // manual trips
    query += 
    ` UNION ALL

      SELECT tp.tid::TEXT nam , '' oth, 'trp' as type, tp.id as id  , NULL stype, NULL status, false as "autoTrip",
      false as "isPrivatePoi", null as "poiGroup", null AS tags, concat(tp.tid, ',', c.cname, ',', c.transporter_code, ',',
      iea.route_id, ',', iea.vehicle_number, ',', iea.invoice_no, ',', iea.shipment_no, ',', iea.ship_to_party, ',', iea.consignee) as row_string
      FROM assets a
      INNER JOIN asset_types at ON a.fk_ast_type_id = at.id
      INNER JOIN trip_plannings tp on tp.fk_asset_id = a.id
      LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id
      INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id AND agm.connected
      INNER JOIN groups g ON g.id = agm.fk_group_id and g.active AND ${groupCondition}
      LEFT JOIN companies c on c.id = a.fk_c_id
      WHERE tp.new AND tp.active AND tp.fk_c_id = ${fk_c_id} AND a.active is TRUE AND ( lower(tp.tid::TEXT) like ${keyword} OR tp.custom_fields::text ilike ${keyword}
      OR iea.*::TEXT ilike ${keyword} OR lower(c.cname) like ${keyword} OR lower(c.transporter_code) like ${keyword} )
      GROUP BY tp.id, iea.id, c.id`

  }

  if(auth.FEATURE_LIST.hasDriver(userFeatureList)){
    query += 
    ` UNION ALL

      SELECT u.name::TEXT nam , '' oth, 'driver' as type, u.id as id  , NULL stype, COALESCE(adm.connected, FALSE)::text status, false as "autoTrip",
      false as "isPrivatePoi", null as "poiGroup", null AS tags,
      concat(a.lic_plate_no, ',', u.lastname, ',', u.firstname) as row_string
      FROM users u
      INNER JOIN driver_details dd on dd.fk_u_id = u.id
      LEFT JOIN asset_driver_mappings adm on adm.fk_driver_id = u.id AND adm.connected
      LEFT JOIN assets a on a.id = adm.fk_asset_id AND adm.connected AND a.active
      LEFT JOIN asset_types atp on atp.id = a.fk_ast_type_id
      LEFT JOIN companies c on c.id = a.fk_c_id
      WHERE u.active AND u.fk_c_id = ${fk_c_id} AND (a.lic_plate_no ilike ${keyword} OR u.lastname ilike ${keyword}
      OR u.firstname ilike ${keyword})`
  }

  if(auth.FEATURE_LIST.hasConsignment(userFeatureList)){
    query += ` UNION ALL
            SELECT 
            DISTINCT c.ext_consignment_no nam , '' oth, 'consignment' as type, c.id as id  , NULL stype, c.status, FALSE as "autoTrip",
            FALSE as "isPrivatePoi", NULL as "poiGroup", NULL AS tags, concat(c.id, ',', c.ext_consignment_no) as row_string
            FROM consignments c
            LEFT JOIN trip_plannings tp on tp.id = c.fk_trip_id
            INNER JOIN consignment_group_mappings cgm ON cgm.fk_consignment_id = c.id and cgm.connected
            INNER JOIN groups g ON cgm.fk_group_id = g.id and cgm.connected
            WHERE c.active AND g.active AND g.fk_c_id = c.fk_c_id AND c.fk_c_id = ${fk_c_id} 
            AND (c.id::TEXT like ${keyword} OR lower(c.ext_consignment_no) like ${keyword})
         `;
  }

  return query
}

exports.search = function (req, res) {

  const userFeatureList = JSON.parse(req.headers['feature-list'] || "[]");
  let keyword = req.query.q || null;
  const q = keyword;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  if(!keyword){
    return res.status(422).send('Enter search keys')
  }

  keyword = `'%${keyword.toLowerCase()}%'`;
  const assetsTypes = ["Truck","Van","Forklift"];
  let limitCondition = req.query.limit ? " limit " + req.query.limit : "";
  
  // assets
  let query =
  `
    SELECT a.lic_plate_no nam, a.name oth , 'ast' as type, a.id as id , at.type stype,
      null status, false as "autoTrip", false as "isPrivatePoi", null as "poiGroup",
      null AS tags,
      concat(a.name, ',', a.lic_plate_no, ',', c.cname, ',', c.transporter_code) as row_string
    FROM assets a
    INNER JOIN asset_types at ON a.fk_ast_type_id = at.id
    INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id AND agm.connected
    INNER JOIN groups g ON g.id = agm.fk_group_id and g.active AND ${groupCondition}
    LEFT JOIN companies c on c.id = a.fk_c_id
    WHERE a.active is TRUE
    AND (lower(a.name) like ${keyword} OR lower(a.lic_plate_no) like ${keyword}
    OR lower(c.cname) like ${keyword} OR lower(c.transporter_code) like ${keyword})
    GROUP BY a.id, at.type, c.id`;

  // pois
  query += 
  ` UNION ALL

    SELECT  p.name nam , '' oth, 'poi', p.id as id  , pt.type stype, null status, false as "autoTrip", p.priv_typ as "isPrivatePoi",
      CASE WHEN pt.fk_c_id = ${fk_c_id} THEN 'company' ELSE 'general' END as "poiCategory",
      null AS tags,
      concat(p.name, ',', x.value) as row_string
    FROM pois p
    INNER JOIN poi_types pt ON p.typ = pt.id
    LEFT JOIN (
      SELECT p.id FROM groups g
      LEFT JOIN poi_group_mappings pgm ON pgm.fk_group_id = g.id and pgm.connected
      LEFT JOIN pois p ON p.id = pgm.fk_poi_id and p.active
      WHERE g.active AND ${groupCondition}
      GROUP BY p.id
    ) pm on pm.id = p.id
    LEFT JOIN json_each_text(p.company_fields) x ON TRUE
    WHERE p.active AND ( pm.id is not null OR p.priv_typ IS FALSE ) AND ( lower(p.name) like ${keyword} OR x.value ILIKE ${keyword} )
    GROUP BY p.name, p.id, pt.id, x.value`;

  //invoices
  query += 
  ` UNION ALL

    SELECT civ.invoice_number nam, civ.invoice_number oth , 'invoice' as type, civ.invoice_id as id , NULL stype,
    null status, false as "autoTrip", false as "isPrivatePoi", null as "poiGroup",
    null AS tags,
    concat(civ.invoice_number) as row_string
    FROM company_invoices_vw civ
    WHERE fk_c_id = ${fk_c_id} AND lower(civ.invoice_number) like ${keyword}

    ${appendModulesToSearch(userFeatureList, fk_c_id, groupCondition, keyword)}

    ORDER BY type, nam ${limitCondition}`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (list) {
      async.each(list, function (itm, cb) {
        itm.matched = findMatchedWord(q, itm.row_string);
        itm.row_string = undefined;
        itm.tags = [];
        if(assetsTypes.indexOf(itm.stype) > -1){
          nuRedis.getAssetDetails(itm.id, function (err, result) {
            if(result){
              itm.status = result.status || null;
              cb()
            }else{
              cb()
            }
          })
        }else{
          cb()
        }
      }, function () {
        res.json(list)
      })
    }).catch(function (err) {
      if (err.parent.code === '42601') {
        LOGGER.warn(APP_LOGGER.formatMessage(req, err));
        res.status(400).send('Invalid search keyword');
      } else {
        LOGGER.error(APP_LOGGER.formatMessage(req, err));
        res.status(500).send('Internal server error');
      }
    });
};

