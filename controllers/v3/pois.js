const async = require('async');
const _ = require('lodash');
const moment = require('moment-timezone');
const models = require('../../db/models/index');
const fs = require('fs');
const env       = process.env.NODE_ENV || 'development';
const nuRedis = require('../../helpers/nuRedis');
const geolib = require('geolib');
const utils = require('../../helpers/util');
const file_base_path = __dirname + '/../../temp_reports/';
const Excel = require('exceljs');
const xlsDownload = require('../../helpers/downloadHelper');
const APP_LOGGER = require('../../helpers/logger');
const tableConfigsHelper = require('../../helpers/table_config');
const LOGGER = APP_LOGGER.logger;
const POI_CATEGORY = ['general','company'];
const GOOGLE_SERVICES = require('../../helpers/google_services');
const poiService = require('../../services/poiService');


function convertDetentionListToXLS(data, filename, callback) {

  try {
    let workbook = new Excel.Workbook();
    let ws = workbook.addWorksheet('Detention Assets');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = [
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30}
    ];

    ws.addRow(['Asset','Trip Number', 'Entry Time', 'Detention Time', 'Exit Time', 'Nubot Status', 'Detention Settings']);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    data.forEach(function (i) {
      let detention_settings = i.detention_config.kms + "kms, " + utils.FormatMinutesDHM(i.detention_config.detention_mins) ;
      let exit_tis = i.exit_tis ? utils.DatetimeNoYearFormat(i.exit_tis) : null;
      ws.addRow([i.lic_plate_no, i.trip_id, utils.DatetimeNoYearFormat(i.entry_tis), utils.FormatMinutesDHM(i.detention_mins), 
        exit_tis, i.status, detention_settings])
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback()
      }).catch(function (err) {
        console.log("XLS ERROR : ", err);
        callback({
          code: 500,
          msg: 'Internal error'
        })
      });
  }catch(e){
    LOGGER.error(APP_LOGGER.formatMessage(null, e));
    console.log(e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

function convertAssetInsideListToXLS(data, filename, callback) {

  try {
    let workbook = new Excel.Workbook();
    let ws = workbook.addWorksheet('Asset list');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = [
      {width: 15},
      {width: 15},
      {width: 40},
      {width: 40},
    ];

    ws.addRow(['Asset','Load status', 'Shipment No', 'Invoice No' ]);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    data.forEach(function (i) {
      let shipment_nos = [];
      let invoice_nos = [];
      let load_status = "";

      if(i.trips){
        let trip = i.trips;
        load_status = trip.tripLoadStatus;

        trip.inprogressTrips.forEach(function (a) {
          if(a.shipmentNo){
            shipment_nos.push(a.shipmentNo)
          }

          if(a.invoiceNo){
            invoice_nos.push(a.invoiceNo)
          }
        });
        shipment_nos = shipment_nos.toString().replace('[]/g','');
        invoice_nos = invoice_nos.toString().replace('[]/g','')
      }

      ws.addRow([i.licNo, load_status, shipment_nos, invoice_nos])
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback()
      }).catch(function (err) {
        console.log("XLS FILE : ", err);
        callback({
          code: 500,
          msg: 'Internal error'
        })
      });
  }catch(e){
    LOGGER.error(APP_LOGGER.formatMessage(null, e));
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

function calcPOIStatus(start_hour, end_hour){

  let startTime = moment.utc(start_hour, "HH:mm");
  let endTime = moment.utc(end_hour, "HH:mm");
  let now = moment.utc();
  let status = "closed";

  if (startTime.isAfter(endTime)) {
    if(now.isAfter(endTime)){
      // add 1 day to endTime
      endTime = endTime.add(1, 'days')
    }else{
      // subtract 1 day to startTime
      startTime = startTime.subtract(1, 'days')
    }
  }

  if (now.isBetween(startTime, endTime)) {
    status = "open"
  }
  return status
}

/**
 * This api gives you details of general poi or company poi
 * Primary poi will give detention poi if it is mapped
 * Adding detention poi has been added in api on 13/03/18
 * @param {*} req 
 * @param {*} res 
 */
exports.getPoiDetail = function (req, res) {
  let companyId = req.headers.fk_c_id;
  let id = req.query.id;
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let gid = req.query.gid;
  let groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  if(isNaN(id)) {
    return res.status(400).send('Invalid POI ID');
  }

  let querySubPart =
  ` p.company_fields as "companyCodes", pt.company_fields as "poiTypeFields", P.name, p.addr,
    p.start_tim as "startTime", p.end_tim as "endTime", pt.type, p.lat, p.lon, 
    json_build_object('name', pc.cont_name, 'email', pc.email, 'phone', pc.tel) as owner,
    p.entry_fee as entryFee, P.priv_typ as private, P.is24hours, p.plaza_code,
    CASE WHEN pt.fk_c_id > 0 THEN 'company' ELSE 'general' END as category,
     p.geo_loc, COALESCE(t.tot_assets, 0) as tot_assets
    FROM pois P
    INNER JOIN poi_types pt ON pt.id = P.typ
    LEFT OUTER JOIN poi_contacts pc ON pc.fk_poi_id = P.id
    LEFT OUTER JOIN (
      SELECT count(distinct pe.id) as tot_assets, pe.poi_id as fk_poi_id
      FROM poi_entities pe
      WHERE pe.poi_id = ${id}
      GROUP BY pe.poi_id
  ) t ON P.id = t.fk_poi_id`;

  let query =
  ` ${poiService.allPoiEntities(groupIds)}
    SELECT null as fk_c_id, '[]' as gids, P.id as primary_poi, null as detention_poi, null as detention_config, 
    ${querySubPart}
    WHERE P.active is TRUE AND P.priv_typ = false AND P.id = ${id}
    GROUP BY p.id, pt.id, pc.id, t.tot_assets

    UNION ALL

    SELECT p.fk_c_id, COALESCE(json_agg(DISTINCT g.gid) FILTER (WHERE g.gid IS NOT NULL), '[]') as gids,
      CASE WHEN pdm.fk_primary_poi_id IS NULL THEN P.id 
      ELSE pdm.fk_primary_poi_id 
      END as primary_poi, 
      pdm.fk_detention_poi_id as detention_poi, array_agg(pdm.detention_config) as detention_config,
      ${querySubPart}
    INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id and pgm.connected
    INNER JOIN groups g on g.id = pgm.fk_group_id AND g.active AND ${groupCondition}
    LEFT JOIN poi_detention_mappings pdm on (P.id = pdm.fk_detention_poi_id OR P.id = pdm.fk_primary_poi_id)                
    WHERE P.active is TRUE AND P.priv_typ = true AND P.fk_c_id = ${companyId} AND P.id = ${id}
    GROUP BY p.id, pt.id, pc.id, t.tot_assets,pdm.fk_primary_poi_id, pdm.fk_detention_poi_id
    ORDER BY fk_c_id DESC 
    LIMIT 1`;
      
  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
    .spread(function (poiData) {
      
      if(!poiData) {
        return res.status(404).send('Invalid POI ID');
      }
      
      poiData.tot_assets = Number(poiData.tot_assets);
      poiData.status = poiData.tot_assets > 0 ? 'Active' : 'Inactive';
      poiData.isOpen = poiData.is24hours ? 'open' : calcPOIStatus(poiData.startTime, poiData.endTime);

      poiData.companyFields = [];
      _.each(poiData.poiTypeFields, function (value, key) {
        poiData.companyFields.push({
          field: value,
          value: poiData.companyCodes ? poiData.companyCodes[key] : "-"
        })
      });
      let geo_obj = [];
      if (poiData.geo_loc.type === "Polygon") {
        poiData.geo_loc.coordinates.forEach(function (points) {
          points.forEach(function (point) {
            geo_obj.push({lat:point[1], lon:point[0]});
          });
        });
      } 
      poiData.geofence = geo_obj;
      poiData.detention_config = (poiData.detention_config && poiData.detention_config[0]) ? poiData.detention_config[0] : null;

      poiData.geo_loc = undefined;
      poiData.poiTypeFields = undefined;
      poiData.companyCodes = undefined;
      poiData.fk_c_id = undefined;

      async.auto({
        assets: function(callback) {
          let query = 
          ` ${poiService.allPoiEntities(groupIds)}
            SELECT pe.id, pe.latest_tis as timestamp, pe.type, pe.ext_consignment_no, pe.lat, pe.lon, pe.lname, pe.status
            FROM poi_entities pe
            WHERE pe.exit_tis IS NULL AND pe.poi_id = ${poiData.primary_poi}`;

          models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
            return callback(null, data)
          }).catch(function (err) {
            callback(err)
          })
        },
        detention_poi : function(callback){
          let query = 
          ` SELECT p.fk_c_id, p.id, p.geo_loc, p.priv_typ, pt.type, p.name, p.addr, p.lat, p.lon
            FROM pois P
            LEFT JOIN poi_types pt ON pt.id = P.typ
            INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id and pgm.connected
            INNER JOIN groups g on g.id = pgm.fk_group_id AND g.active AND ${groupCondition}
            WHERE P.active is TRUE AND P.priv_typ = true AND P.fk_c_id = ${companyId} AND P.id = ${poiData.detention_poi}
            GROUP BY p.id, pt.id
            ORDER BY fk_c_id DESC
            LIMIT 1`;
          models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
            .spread(function (poi_data) {
              if (poi_data) {
                let poi = {};
                let coordinates = { lat: poi_data.lat, lon: poi_data.lon};
                let geo_obj = [];
                if (poi_data.geo_loc.type === "Polygon") {
                  poi_data.geo_loc.coordinates.forEach(function (points) {
                    points.forEach(function (point) {
                      let loc_obj = {};
                      loc_obj.lon = point[0];
                      loc_obj.lat = point[1];
                      geo_obj.push(loc_obj);
                    });
                  });

                } 
                poi.id = poi_data.id;
                poi.type = poi_data.type;
                poi.geofence = geo_obj;
                poi.coordinates = coordinates;
                poi.category = poi_data.fk_c_id ? 'company' : 'general';

                callback(null, poi)
              }else{
                callback(null)
              }
            }).catch(function (err) {
              callback(err)
            })
        },
        detention_assets: function(callback){
          let query =
          ` ${poiService.allPoiEntities(groupIds)}
            SELECT pe.id, pe.latest_tis as timestamp, pe.type, pe.lic_plate_no, pe.ext_consignment_no, pe.lat, pe.lon, pe.lname, pe.status
            FROM poi_entities pe
            WHERE pe.exit_tis is null and pe.poi_id = ${poiData.detention_poi} and pe.id NOT IN ( 
                SELECT pe.id
                FROM poi_entities pe
                WHERE pe.poi_id = ${poiData.primary_poi}
            )`;
          models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
            return callback(null, data)                  
          }).catch(function (err) {
            callback(err)
          })
        }
      }, function(err, results){
        res.json(_.merge(poiData, results))
      })
    }).catch(function (err) {
      console.log(err);
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send("Internal Server Error")
    })
};

exports.getAllPois = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit || 0;
  const offset = (req.query.page || 0 ) * limit;
  const category = req.query.category;
  const poi_type = req.query.type;
  const q = req.query.q ? req.query.q.toLowerCase() : '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const search_cols = req.query.search;
  let search_col_condition = "";
  const sort = req.query.sort;
  let sort_condition = "ORDER BY COALESCE(ps.total_assets,0)::INT DESC ";
  let filterCondition = "";
  const mapBounds = utils.processMapBoundParams(req);
  let mapBoundsCondition = "";
  const output_format = req.query.format;
  const fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';
  const poiStatus = 'all';
  let limit_condition = '';

  if(limit){
    limit_condition = `LIMIT ${limit} OFFSET ${offset}`
  }

  if(POI_CATEGORY.indexOf(category) === -1){
    return res.status(400).send("Enter poi category")
  }

  if(mapBounds){
    mapBoundsCondition = ` AND ( ST_SetSRID(ST_Point(p.lon,p.lat),4326) && 
      ST_MakeEnvelope( ${mapBounds.west},${mapBounds.south},${mapBounds.east},${mapBounds.north}, 4326) )`;
  }

  let poiCondition = ` pt.fk_c_id = ${fk_c_id} AND p.fk_c_id = ${fk_c_id} AND ${groupCondition} AND pgm.id IS NOT NULL`;

  if (category === 'general') {
    poiCondition = ' p.priv_typ is false'
  }

  if(poi_type){
    filterCondition = " AND pt.type ilike '"+ poi_type +"'"
  }

  search_col_condition = utils.processTableColumnSearch(search_cols, [
    {
      db: "P.addr",
      client: "addr"
    },
    {
      db: "P.name",
      client: "name"
    },
    {
      db: "p.plaza_code",
      client: "plaza_code"
    }
  ]);

  sort_condition = utils.processTableColumnSort(
    sort,
    [
      {
        db: "P.addr",
        client: "addr"
      },
      {
        db: "P.name",
        client: "name"
      },
      {
        db: "COALESCE(ps.total_assets,0)",
        client: "tot_assets"
      },
      {
        db: "p.plaza_code",
        client: "plaza_code"
      }
    ],
    sort_condition
  );

  let query =
  `WITH assets_in_pois as ( 
    SELECT aps.fk_poi_id, array_length(array_agg(distinct aps.fk_asset_id),1) as total_assets
    FROM assets_pois_stats aps
    INNER JOIN asset_group_mappings agm on agm.fk_asset_id = aps.fk_asset_id and agm.connected
    INNER JOIN groups g on g.id = agm.fk_group_id AND ${groupCondition}
    WHERE aps.exit_tis IS NULL
    GROUP BY aps.fk_poi_id
  )

  SELECT P.id, P.addr, P.name, pt.type, p.company_fields->'shipToCode' as ship_to_code,
  COALESCE(p.company_fields->'consignerCode', p.company_fields->'cmsCode') as cms_or_consigner_code,
  COALESCE(ps.total_assets,0) as tot_assets, p.lat, p.lon,
  COALESCE(json_agg(DISTINCT g.gid) FILTER (WHERE g.gid IS NOT NULL), '[]') as gids,
  p.geo_loc as "geoBounds", CASE WHEN pt.fk_c_id > 0 THEN 'company' ELSE 'general' END as category,
  p.plaza_code,
  (
    SELECT CASE WHEN COUNT(g) = 0  THEN '[]' ELSE json_agg(json_build_object('id', g2.id, 'name', g2.name)) END
    FROM poi_group_mappings pgm2
    INNER JOIN groups g2 on g2.id = pgm2.fk_group_id AND g2.active
    WHERE pgm2.fk_poi_id = p.id and pgm2.connected
  ) as groups,
	json_agg(json_build_object('id', dp.id, 'name', dp.name, 'lat', dp.lat, 'lon', dp.lon ,'detention_config', pdm.detention_config)) FILTER (WHERE pdm.id IS NOT NULL) AS detention_pois,
  COUNT(*) OVER() AS total_count
  FROM pois p
  LEFT JOIN poi_group_mappings pgm on p.id = pgm.fk_poi_id and pgm.connected
  LEFT JOIN ( SELECT * FROM assets_in_pois ) ps on ps.fk_poi_id = p.id
  LEFT JOIN poi_types pt on pt.id = p.typ
  LEFT JOIN poi_contacts PC ON PC.fk_poi_id = p.id
  LEFT JOIN groups g on  g.id = pgm.fk_group_id and g.active
  LEFT JOIN poi_detention_mappings pdm on pdm.fk_primary_poi_id = p.id and pdm.mapped
  LEFT JOIN pois dp on dp.id = pdm.fk_detention_poi_id and dp.active
  WHERE p.active AND lower(p.*::TEXT) like '%${q}%' AND ${poiCondition + filterCondition + mapBoundsCondition + search_col_condition}
  GROUP BY p.id, pc.id, pt.id, ps.total_assets
  ${sort_condition + limit_condition}`;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
    .then(function (pois) {
      let total = 0;
      pois.forEach(function (poi) {
        total = Number(poi.total_count);
        poi.status = poi.tot_assets > 0 ? 'Active' : 'Inactive';
        poi.isOpen = poi.is24hours ? 'open' : calcPOIStatus(poi.startTime, poi.endTime);
        poi.tags = [];

        poi.geoBounds.geofence = [];
        _.each(poi.geoBounds.coordinates[0], function (coordinates) {
          poi.geoBounds.geofence.push({
            lon: coordinates[0],
            lat: coordinates[1]
          })
        });

        poi.total_count = undefined;
        poi.geoBounds.coordinates = undefined
      });

      if(output_format === 'xls'){
        const file_name = "PLACE-LIST-"+ fk_c_id +"-"+ moment().unix() + ".xls";
        const file_path = file_base_path + file_name;

        tableConfigsHelper.getTableConfig(`${category}-place-list`, fk_u_id, poiStatus, default_columns, function (err, table_config) {
          xlsDownload.convertTableV1ToXLS(poiStatus, pois, file_base_path, file_name, table_config, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })
        })
      }else{
        res.json({
          total_count : total,
          data : pois
        })
      }
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send()
    })
};


exports.getPoiLocationMap = function (req, res) {
  const fk_c_id = parseInt(req.headers.fk_c_id);
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  let fees_col = 'p.cost as fees';
  if (env === 'production') {
    fees_col = 'p.fees'
  }

  let id =req.query.id;
  if (!id) {
    return res.status(400).send("invalid id in the query!");
  }

  let detention_poi_id = null;

  async.waterfall([
    function(wcallback){
      let query = `select fk_primary_poi_id, fk_detention_poi_id
                      FROM poi_detention_mappings pdm 
                      WHERE fk_detention_poi_id =`+id + `OR fk_primary_poi_id = `+id;
      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
        .spread(function (detPoi) {
          if(detPoi){
            let primary_poi_id = detPoi.fk_primary_poi_id;
            detention_poi_id = detPoi.fk_detention_poi_id;
            wcallback(null, primary_poi_id)
          }else{
            wcallback(null, id)
          }
        })
    }
  ], function(err, result){
    if(err){
      res.status(err.code).send(err.msg)
    }
    else{
      console.log(result);
      let primary_poi_id = result;
      async.parallel({
        poi: function(callback) {

          let query = 'SELECT null as fk_c_id, p.id, p.geo_loc, p.priv_typ, pt.type, p.name, p.addr, ' + fees_col +
                      ' FROM pois P INNER JOIN poi_types pt ON pt.id = P.typ' +
                      ' WHERE P.active is TRUE AND P.priv_typ = false AND P.id = ' + primary_poi_id + '' +
                      ' GROUP BY p.id, pt.id'+

                      ' union all '+
          // company pois query to check if user has access to the poi

                      ' SELECT p.fk_c_id, p.id, p.geo_loc, p.priv_typ, pt.type, p.name, p.addr, ' + fees_col +
                      ' FROM pois P' +
                      ' LEFT JOIN poi_types pt ON pt.id = P.typ' +
                      ' INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id and pgm.connected' +
                      ' INNER JOIN groups g on g.id = pgm.fk_group_id AND g.active AND ' + groupCondition +
                      ' WHERE P.active is TRUE AND P.priv_typ = true AND P.fk_c_id = ' + fk_c_id + ' AND P.id = ' + primary_poi_id + '' +
                      ' GROUP BY p.id, pt.id' +

                      ' order by fk_c_id desc limit 1';

          models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
            .spread(function (poi_data) {
              if (poi_data) {
                let poi = {};
                let coordinates = { lat: null, lon: null};
                let data = [];
                let geo_obj = [];
                if (poi_data.geo_loc.type === "Polygon") {
                  poi_data.geo_loc.coordinates.forEach(function (points) {
                    points.forEach(function (point) {
                      let loc_obj = {};
                      loc_obj.lon = point[0];
                      loc_obj.lat = point[1];
                      geo_obj.push(loc_obj);
                      data.push({
                        latitude: point[1],
                        longitude: point[0]
                      })
                    });
                  });

                  let geo_data = geolib.getCenter(data);
                  coordinates.lat = Number(geo_data.latitude) || null;
                  coordinates.lon = Number(geo_data.longitude) || null;

                } else {
                  let loc_obj = {};
                  loc_obj.lon = poi_data.geo_loc.coordinates[0];
                  loc_obj.lat = poi_data.geo_loc.coordinates[1];
                  geo_obj.push(loc_obj);

                  coordinates.lat = poi_data.geo_loc.coordinates[1];
                  coordinates.lon = poi_data.geo_loc.coordinates[0];
                }
                poi.id = poi_data.id;
                poi.type = poi_data.type;
                poi.geofence = geo_obj;
                poi.coordinates = coordinates;
                poi.category = poi_data.fk_c_id ? 'company' : 'general';

                if (env !== 'production') {
                  _.each(poi_data.fees, function (obj) {
                    _.each(obj, function (value, key) {
                      obj[_.camelCase(key)] = value;
                      obj[key] = undefined
                    })
                  })
                }
                poi.fees = poi_data.fees;

                callback(null, poi)
              }else{
                callback({
                  code: 404,
                  msg: 'Not Found'
                })
              }
            }).catch(function (err) {
              callback(err)
            })
        },
        detention_poi : function(callback){
          let query = ' SELECT p.fk_c_id, p.id, p.geo_loc, p.priv_typ, pt.type, p.name, p.addr, ' + fees_col +
                  ' FROM pois P' +
                  ' LEFT JOIN poi_types pt ON pt.id = P.typ' +
                  ' INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id and pgm.connected' +
                  ' INNER JOIN groups g on g.id = pgm.fk_group_id AND g.active AND ' + groupCondition +
                  ' WHERE P.active is TRUE AND P.priv_typ = true AND P.fk_c_id = ' + fk_c_id + ' AND P.id = ' + detention_poi_id + '' +
                  ' GROUP BY p.id, pt.id' +

                  ' order by fk_c_id desc limit 1';
          models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
            .spread(function (poi_data) {
              if (poi_data) {
                let poi = {};
                let coordinates = { lat: null, lon: null};
                let data = [];
                let geo_obj = [];
                if (poi_data.geo_loc.type === "Polygon") {
                  poi_data.geo_loc.coordinates.forEach(function (points) {
                    points.forEach(function (point) {
                      let loc_obj = {};
                      loc_obj.lon = point[0];
                      loc_obj.lat = point[1];
                      geo_obj.push(loc_obj);
                      data.push({
                        latitude: point[1],
                        longitude: point[0]
                      })
                    });
                  });

                  let geo_data = geolib.getCenter(data);
                  coordinates.lat = Number(geo_data.latitude) || null;
                  coordinates.lon = Number(geo_data.longitude) || null;

                } else {
                  let loc_obj = {};
                  loc_obj.lon = poi_data.geo_loc.coordinates[0];
                  loc_obj.lat = poi_data.geo_loc.coordinates[1];
                  geo_obj.push(loc_obj);

                  coordinates.lat = poi_data.geo_loc.coordinates[1];
                  coordinates.lon = poi_data.geo_loc.coordinates[0];
                }
                poi.id = poi_data.id;
                poi.type = poi_data.type;
                poi.geofence = geo_obj;
                poi.coordinates = coordinates;
                poi.category = poi_data.fk_c_id ? 'company' : 'general';

                if (env !== 'production') {
                  _.each(poi_data.fees, function (obj) {
                    _.each(obj, function (value, key) {
                      obj[_.camelCase(key)] = value;
                      obj[key] = undefined
                    })
                  })
                }
                poi.fees = poi_data.fees;

                callback(null, poi)
              }else{
                callback(null)
              }
            }).catch(function (err) {
              callback(err)
            })
        },
        assets: function(callback) {
          let query = poiService.allPoiEntities(groupIds) +
                              `SELECT pe.id, pe.latest_tis as timestamp, pe.type, pe.ext_consignment_no
                              FROM poi_entities pe
                              WHERE pe.exit_tis is null and pe.poi_id = `+ primary_poi_id; 
          models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
            async.forEachOf(data, function (asset, i, callback) {
              nuRedis.getAssetDetails(asset.id, function (err, value) {
                console.log(value);
                if(value){
                  asset.status = value.status;
                  asset.lat = value.lat;
                  asset.lon = value.lon;
                  asset.lname = value.lname
                }
                callback()
              })
            }, function () {
              callback(null, data)
            });
                      
          }).catch(function (err) {
            callback(err)
          })
        },
        detention_assets: function(callback){
          let query = poiService.allPoiEntities(groupIds) +
                              `SELECT pe.id, pe.latest_tis as timestamp, pe.type, pe.lic_plate_no, pe.ext_consignment_no
                              FROM poi_entities pe
                              WHERE pe.exit_tis is null and pe.poi_id = `+ detention_poi_id +` and pe.id NOT IN ( 
                                  SELECT pe.id
                                  FROM poi_entities pe
                                  WHERE pe.poi_id = `+ primary_poi_id +
                              `)`;
          models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
                      
            async.forEachOf(data, function (asset, i, callback) {
              nuRedis.getAssetDetails(asset.id, function (err, value) {
                if(value){
                  asset.status = value.status;
                  asset.lat = value.lat;
                  asset.lon = value.lon;
                  asset.lname = value.lname
                }
                callback()
              })
            }, function () {
              callback(null, data)
            });                        
          }).catch(function (err) {
            callback(err)
          })
        }
      }, function(err, results) {
        if(err){
          LOGGER.error(APP_LOGGER.formatMessage(req, err));
          if(err.code){
            res.status(err.code).send(err.msg)
          }else{
            LOGGER.error(err);
            res.status(500).send()
          }
        }else{
          res.json(results)
        }
      });
    }
  });
};

exports.getAssetsInPoi = function (req, res) {
  let apiType = "list";
  const fk_c_id = req.headers.fk_c_id;
  const id = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const startTis = req.query.start_time || moment().tz(utils.timezone.default, false).startOf('day').unix();
  const endTis = req.query.end_time || moment().tz(utils.timezone.default, false).endOf('day').unix();
  const limit = req.query.limit || 10;
  const offset = (req.query.page || 0 ) * limit;
  const searchKeyword = req.query.q || '';
  const search_cols = req.query.search;
  let search_col_condition = "";
  const status = req.query.status;
  let status_condition = "";
  const output_format = req.query.format;
  const fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';
  const poiStatus = 'all';
  let sort = req.query.sort;
  let sort_condition = " aps.latest_tis DESC";

  if(output_format === 'xls'){
    apiType = output_format
  }

  if(isNaN(id)) {
    return res.status(400).send('Invalid POI ID');
  }

  const POI_INTERACTION_STATUS = [{
    id: 0,
    status: 'entered'
  },{
    id: 1,
    status: 'dwelling'
  },{
    id: 2,
    status: 'exited'
  }];


  if(status){
    let matched = _.find(POI_INTERACTION_STATUS, {status: status.toLowerCase()});
    if (matched) {
      status_condition = " AND aps.poi_stat = " + matched.id;
    }
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'a.lic_plate_no',
      client: 'lic_plate_no'
    }]
  );

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'aps.entry_tis',
      client: 'entry_tis'
    }, {
      db: 'aps.exit_tis',
      client: 'exit_tis'
    }, {
      db: '(aps.latest_tis - aps.entry_tis)/60',
      client: 'dwell_time'
    }],
    sort_condition
  );

  let query =
  ` SELECT atp.type, a.lic_plate_no, count(*) OVER() AS total_count, aps.fk_asset_id as asset_id, (aps.latest_tis - aps.entry_tis)/60 as dwell_time, entry_tis, exit_tis,
    CASE WHEN poi_stat = 0 THEN 'Entered' WHEN poi_stat = 1 THEN 'Dwelling' WHEN poi_stat = 2 THEN 'Exited' END AS status
    FROM assets_pois_stats aps
    left join poi_group_mappings pgm on aps.fk_poi_id = pgm.fk_poi_id and pgm.connected
    left join pois p on aps.fk_poi_id = p.id
    left join poi_types pt on pt.id = p.typ
    left join groups g on  g.id = pgm.fk_group_id and g.active AND ${groupCondition}
    INNER JOIN asset_group_mappings agm on agm.fk_group_id =  g.id and agm.fk_asset_id = aps.fk_asset_id
    INNER JOIN assets a on a.id = aps.fk_asset_id
    INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id
    WHERE p.active and p.id = ${id}
      AND ((pt.fk_c_id = ${fk_c_id} AND p.fk_c_id = ${fk_c_id}
      AND ${groupCondition} AND pgm.id IS NOT NULL) OR p.priv_typ IS FALSE)
      AND (aps.entry_tis BETWEEN ${startTis} AND ${endTis} OR aps.latest_tis between ${startTis} AND ${endTis})
      AND ( a.lic_plate_no ilike '%${searchKeyword}%') ${search_col_condition + status_condition}
    GROUP BY a.id, atp.id, aps.fk_asset_id, aps.latest_tis, aps.entry_tis, aps.exit_tis, aps.poi_stat
    ORDER BY ${sort_condition}`;

  if(apiType === 'list'){
    query += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT}).then(function (list) {
    let count = 0;
    list.forEach(function (value) {
      count = Number(value.total_count);
      value.total_count = undefined
    });
    if(output_format === 'xls'){
      const file_name = "PLACE-ACTIVITY-"+ id +"-"+ utils.downloadReportDate() + ".xls";
      const file_path = file_base_path + file_name;

      tableConfigsHelper.getTableConfig(`place-activity`, fk_u_id, poiStatus, default_columns, function (err, table_config) {
        xlsDownload.convertTableV1ToXLS(poiStatus, list, file_base_path, file_name, table_config, function (err) {
          if (err) {
            res.status(err.code).send(err.msg)
          } else {
            res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })

      })
    }else{
      res.json({
        total_count: count,
        data: list
      })
    }
  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send('Internal server error')
  })
};

exports.poiTypeCount = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
    
  let query = "   SELECT priv_typ as company_pois, type, count(*) "+
                "    FROM pois p"+
                "    INNER JOIN  poi_types pt on p.typ = pt.id"+
                "    INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id"+
                "    WHERE p.fk_c_id = "+fk_c_id+" and pgm.fk_group_id in "+ groupIds+
                "    GROUP by priv_typ,type";
  models.sequelize.query(query, {
    replacements:{
      fk_c_id: fk_c_id, 
      group_ids: groupIds
    }, 
    type: models.sequelize.QueryTypes.SELECT}).then(function (pois) {
    res.json(pois)
  }).catch(function(err){
    res.status(500).send(err)
  });
};

function addNullDates(params, data) {
  let currentDate = moment.unix(params.last7DaysTis), end_date = moment.unix(params.end_tis);
  data = data ? data : [];
  while (currentDate.isBefore(end_date)) {
    let dayStart = currentDate.clone();
    let dayTis = dayStart.startOf('day').unix();
    if(!_.find(data, { tis: dayTis})){
      data.push({
        day: dayStart.format('D MMM YYYY'),
        tis: dayTis,
        value: 0
      })
    }
    currentDate.add(1, 'day');
  }
  return data
}

function processPOIAnalyticsItems(data, last7DaysTis, endTis) {
  let groupedByDay = _.groupBy(data, 'day');
  let grouped = [];

  _.forEach(groupedByDay, function (value, key) {
    grouped.push({
      day: key,
      tis: value && value.length > 0 ? value[0].tis : null,
      value: _.size(value)
    })
  });

  return _.sortBy(addNullDates({
    last7DaysTis: last7DaysTis,
    end_tis: endTis}
  , grouped), ['tis'])
}

function processPerDayAvgDwellTime(data, last7DaysTis, endTis) {
  let dates = addNullDates({ last7DaysTis: last7DaysTis, end_tis: endTis}, []);
  dates.forEach(function (i) {
    let dwell_time = [];
    let day_start_tis = i.tis;
    let day_end_tis = i.tis + (1440*60);
    data.forEach(function (j) {

      let event_start_tis, event_end_tis;
      event_start_tis = j.entry_tis < day_start_tis ? day_start_tis : j.entry_tis;
      event_end_tis = j.exit_tis > day_end_tis ? j.exit_tis : day_end_tis;

      dwell_time.push((event_end_tis - event_start_tis)/60)
    });
    i.value = Math.round(Math.abs(_.mean(dwell_time))) || 0
  });
  return dates
}

function processAssetsAtPlaces(data) {
  let assets_inside = [];
  let assets_pois = _.filter(data, function (o) {
    return !o.exit_tis
  });
  let grouped_places = _.groupBy(assets_pois, 'fk_poi_type_id');
  _.forEach(grouped_places, function (value, key) {
    let stat = {
      id: Number(key),
      name: value[0].poi_type_name,
      total_assets: _.size(value)
    };
    assets_inside.push(stat)
  });
  assets_inside = _.orderBy(assets_inside, ['total_assets'], ['desc']);
  let places = _.slice(assets_inside, 0, 4);
  let other_places = _.slice(assets_inside, 4, assets_inside.length);

  if(other_places && other_places.length > 0){
    places.push({
      id: 0,
      name: "Others",
      total_assets: _.sumBy(other_places, 'total_assets')
    })
  }

  return places
}

function processFrequentPlaces(data) {
  let frequent_visits = [];
  let grouped_by_place = _.groupBy(data, 'id');
  _.forEach(grouped_by_place, function (value, key) {
    let stat = {
      id: key,
      name: value[0].name,
      visits: _.size(value)
    };
    frequent_visits.push(stat)
  });

  return _.slice(_.orderBy(frequent_visits, ['visits'], ['desc']), 0, 10);
}

function processPlaceRealtime(data){
  console.log("data length : ", data.length);
  let inside = 0, active_pois = [];
  
  data.forEach(function(i){
    if(!i.exit_tis){
      inside++;
      active_pois.push(i.fk_poi_id)
    }
  });

  return {
    totActive: _.size(_.uniq(active_pois)),
    totInactive: null,
    totAssetsInside: inside
  }
}

exports.getPoisStats = function (req, res) {
  const poi_id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const category = req.query.category;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const startTis = req.query.start_time ? req.query.start_time : moment().tz(utils.timezone.default, false).startOf('day').unix();
  const endTis = req.query.end_time ? req.query.end_time : moment().tz(utils.timezone.default, false).endOf('day').unix();
  const last7DaysTis = moment().subtract(7, 'days').unix();
  let single_poi_condition = "";
 
  if(Number(poi_id)){
    single_poi_condition = ` AND p.id = ${poi_id}`
  }

  let poi_query = `
            SELECT DISTINCT p.id FROM pois p
            INNER JOIN poi_group_mappings pgm ON pgm.fk_poi_id = p.id AND pgm.connected
            INNER JOIN groups g ON g.id = pgm.fk_group_id
            WHERE p.active AND p.fk_c_id = ${fk_c_id} AND g.fk_c_id = ${fk_c_id} AND ${groupCondition} ${single_poi_condition}`;

  if(category === 'general'){
    poi_query = `SELECT p.id FROM pois p WHERE p.active and p.priv_typ IS FALSE ${single_poi_condition}`
  }

  async.auto({
    pois: function(cb){
      models.sequelize.query(poi_query, { type: models.sequelize.QueryTypes.SELECT})
        .then(function (pois) {
          cb(null, _.size(pois))
        }).catch(function(err){
          cb({err: err, code: 500, msg: 'Internal server error'})
        })
    },
    stats: function(cb){
      let query = `
      WITH my_pois AS (
          ${poi_query}
      ),
      my_assets as (
          SELECT DISTINCT a.id FROM assets a
          INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id AND agm.connected
          INNER JOIN groups g ON g.id = agm.fk_group_id
          WHERE a.active AND g.fk_c_id = ${fk_c_id} AND ${groupCondition}      
      )
      
      SELECT fk_asset_id, fk_poi_id, entry_tis, exit_tis, latest_tis, p.id, p.name,
      pt.id as fk_poi_type_id, pt.type as poi_type_name
      FROM assets_pois_stats aps
      LEFT JOIN pois p on p.id = aps.fk_poi_id
      LEFT JOIN poi_types pt on pt.id = p.typ
      WHERE enum_type = 'Vehicle' AND (aps.entry_tis >= ${last7DaysTis} OR aps.exit_tis >= ${last7DaysTis} OR aps.exit_tis IS NULL) 
      AND aps.fk_asset_id IN (SELECT id FROM my_assets) AND aps.fk_poi_id IN (SELECT id FROM my_pois);
    `;

      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
        .then(function (list) {
          let interaction_data = [];
          let entries = [];
          let exits = [];
          list.forEach(function (i) {
            let dwell_start = i.entry_tis < startTis ? startTis : i.entry_tis || 0;
            let dwell_end = i.exit_tis ? i.exit_tis : i.latest_tis || 0;
            i.dwell_time = (dwell_end - dwell_start) / 60;

            let entry_day = moment.unix(i.entry_tis).startOf('day');
            let exit_day = i.exit_tis ? moment.unix(i.exit_tis).startOf('day') : null;

            if(i.entry_tis >= last7DaysTis) {
              let item = {
                day: entry_day.format('D MMM YYYY'),
                tis: entry_day.unix(),
                value: 1
              };

              interaction_data.push(item);
              entries.push(item)
            }
            if(exit_day && i.exit_tis >= last7DaysTis){
              let item = {
                day: exit_day.format('D MMM YYYY'),
                tis: exit_day.unix(),
                value: 1
              };

              interaction_data.push(item);
              exits.push(item)
            }
          });

          let stats = {
            totEntered: _.size(_.filter(list, function(o){return o.entry_tis >= startTis})),
            totExited: _.size(_.filter(list, function(o){return o.exit_tis >= startTis})),
            avgDwellTime: Math.abs(Math.round(_.meanBy(_.filter(list, function (o) {
              return o.entry_tis >= startTis || o.exit_tis >= startTis || !o.exit_tis
            }), 'dwell_time')) || 0),
            interactions: [],
            trends: {
              entered: [],
              exited: [],
              avgDwellTime: []
            }
          };

          stats.interactions = processPOIAnalyticsItems(interaction_data, last7DaysTis, endTis);
          stats.trends.entered = processPOIAnalyticsItems(entries, last7DaysTis, endTis);
          stats.trends.exited = processPOIAnalyticsItems(exits, last7DaysTis, endTis);
          stats.trends.avgDwellTime = processPerDayAvgDwellTime(list, last7DaysTis, endTis);
          stats.frequent_places = processFrequentPlaces(list);
          stats.assets_at_places = processAssetsAtPlaces(list);
          stats.realtime = processPlaceRealtime(list);

          cb(null, stats)
        }).catch(function (err) {
          cb({err: err, code: 500, msg: 'Internal server error'})
        })
    }
  }, function(err, result){
    if(err){
      return res.status(err.code).send(err.msg)
    }else{
      let data = result.stats;
      data.realtime.totInactive = Math.abs(data.realtime.totActive - result.pois);
      res.json(data)
    }
  })  
};


exports.getAllPoiRealtimeStats = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const category = req.query.category;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  let poiCondition = ' pt.fk_c_id = ' + fk_c_id + ' AND p.fk_c_id = ' + fk_c_id + ' and ' + groupCondition + ' and pgm.id is not null';

  if (category === 'general') {
    poiCondition = ' p.priv_typ is false'
  }

  let query =  'SELECT p.id, p.name, COALESCE(ps.total_assets,0) as tot_assets FROM pois p' +
        ' LEFT JOIN poi_group_mappings pgm on p.id = pgm.fk_poi_id and pgm.connected' +
        ' LEFT JOIN ('+
            ' SELECT aps.fk_poi_id, array_length(array_agg(distinct aps.fk_asset_id),1) as total_assets' +
            ' FROM assets_pois_stats aps '+
            ' INNER JOIN asset_group_mappings agm on agm.fk_asset_id = aps.fk_asset_id and agm.connected' +
            ' INNER JOIN groups g on g.id = agm.fk_group_id AND '+ groupCondition +
            ' WHERE aps.exit_tis is null'+
            ' GROUP BY aps.fk_poi_id'+
        ' ) ps on ps.fk_poi_id = p.id' +
        ' LEFT JOIN poi_types pt on pt.id = p.typ' +
        ' LEFT JOIN groups g on  g.id = pgm.fk_group_id and g.active' +
        ' WHERE p.active AND ' + poiCondition +
        ' GROUP BY p.id, ps.total_assets';


  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
    .then(function (list) {
      res.json({
        totActive: _.size(_.filter(list, function(o){return o.tot_assets > 0})),
        totInactive: _.size(_.filter(list, function(o){return o.tot_assets === 0})),
        totAssetsInside: Number(_.sumBy(list, 'tot_assets'))
      })
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send()
    })

};

exports.getAllPlaceAssetDwelling = function (req, res) {

  // Dashboard places dwelling graph

  const fk_c_id = req.headers.fk_c_id;
  const category = req.query.category;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  let whereCondition = " pt.fk_c_id is NULL";
  let poiJoins =  "   INNER JOIN pois p on p.id = apst.fk_poi_id AND p.active AND p.priv_typ = FALSE"+
                    "   LEFT JOIN poi_types pt on pt.id = p.typ";

  if (category === 'company') {

    poiJoins =  "   INNER JOIN pois p on p.id = apst.fk_poi_id AND p.active AND p.priv_typ" +
                    "   INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id AND pgm.fk_group_id = g.id"+
                    "   LEFT JOIN poi_types pt on pt.id = p.typ";

    whereCondition = " pt.fk_c_id = " + fk_c_id
  }

  let query =  "SELECT pt.type as y, COALESCE(d.tot_assets, 0) as x FROM poi_types pt"+
    " LEFT JOIN("+
    "   SELECT pt.id, array_length(array_agg(DISTINCT a.id), 1) as tot_assets FROM groups g"+
    "   LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected"+
    "   LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active"+
    "   INNER JOIN assets_pois_stats apst on apst.fk_asset_id = a.id AND apst.exit_tis is NULL"+
        poiJoins +
    "   WHERE g.active AND "+ groupCondition +" GROUP BY pt.id"+
    " ) d on d.id = pt.id"+
    " WHERE " + whereCondition + " ORDER BY pt.id";

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
    .then(function (data) {
      res.json(data)
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send()
    })
};

exports.getAllPlaceAssetInteractions = function (req, res) {

  // Dashboard places interactions graph

  const fk_c_id = req.headers.fk_c_id;
  const category = req.query.category;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  let whereCondition = " pt.fk_c_id is NULL";
  let poiJoins =  "   INNER JOIN pois p on p.id = apst.fk_poi_id AND p.active AND p.priv_typ = FALSE"+
                    "   LEFT JOIN poi_types pt on pt.id = p.typ";

  if (category === 'company') {

    poiJoins =  "   INNER JOIN pois p on p.id = apst.fk_poi_id AND p.active AND p.priv_typ" +
                    "   INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id AND pgm.fk_group_id = g.id"+
                    "   LEFT JOIN poi_types pt on pt.id = p.typ";

    whereCondition = " pt.fk_c_id = " + fk_c_id
  }

  let query =  "SELECT pt.type as y, COALESCE(d.tot_assets, 0) as x FROM poi_types pt"+
    " LEFT JOIN("+
    "   SELECT pt.id, array_length(array_agg(a.id), 1) as tot_assets FROM groups g"+
    "   LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected"+
    "   LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active"+
    "   INNER JOIN assets_pois_stats apst on apst.fk_asset_id = a.id"+
        poiJoins +
    "   WHERE g.active AND "+ groupCondition +" GROUP BY pt.id"+
    " ) d on d.id = pt.id"+
    " WHERE " + whereCondition + " ORDER BY pt.id";

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
    .then(function (data) {
      res.json(data)
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send()
    })
};

exports.getAllAssetsInsidePoiList = function (req, res) {

  const id = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let startTis = req.query.start_time;
  let endTis = req.query.end_time;
  const limit = req.query.limit || 10;
  const offset = (req.query.page || 0 ) * limit;
  let searchKeyword = req.query.q || '';
  const onTrip = req.query.onTrip;
  let trip_condition = "";
  let time_range_condition = "";
  const output_format = req.query.format || 'list';
  let search_cols = req.query.search;
  let search_col_condition = "";

  if(isNaN(id)) {
    return res.status(400).send('Invalid POI ID');
  }

  if(startTis && endTis){
    startTis = moment().tz(utils.timezone.default, false).startOf('day').unix();
    endTis = moment().tz(utils.timezone.default, false).endOf('day').unix();
    time_range_condition = "AND (pe.entry_tis between " + startTis + " AND " + endTis + " OR pe.latest_tis between  "+ startTis + " AND " + endTis + ")";
  }

  if(onTrip === 'true' || onTrip === 'false'){
    trip_condition += " AND CAST(json_array_length(atm.trips) as INTEGER) ";
    trip_condition += onTrip === 'true' ? " > 0 " : " = 0 "
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'pe.lic_plate_no',
      client: 'licNo'
    }]
  )

  searchKeyword = searchKeyword ? ` AND pe.lic_plate_no ilike '%`+ searchKeyword + `%' ` : ``;

  //NEW QUERY - Find assets and consignments inside poi
  let query = poiService.allPoiEntities(groupIds) +
                `SELECT * FROM (
                    SELECT distinct pe.id, pe.lic_plate_no as "licNo", pe.type,
                        json_agg(json_build_object('id', pe.trip_id, 
                                                    'tid', pe.tid,  
                                                    'fk_asset_id', pe.id,
                                                    'end_tis',pe.trip_end_tis,
                                                    'eta',pe.trip_log->'ETA',
                                                    'delayed_by', pe.trip_log->'delayed_by', 
                                                    'delayed', pe.trip_log->'delayed')
                                                )::jsonb as tp,
                        COALESCE(atm.trips::jsonb,'[]') as trips,
                        json_array_length(COALESCE(atm.trips::json,'[]')) total_trips, count(*) OVER() AS total_count
                    FROM poi_entities pe
                    INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = pe.poi_id
                    LEFT JOIN asset_trip_maps as atm on atm.fk_asset_id = pe.id
                    LEFT JOIN json_array_elements_text(atm.trips) x ON TRUE
                    WHERE --pe.trip_id = to_json(x::json->'id')::text::int AND 
                        pe.poi_id = `+ id + ` AND exit_tis is null `+ 
                        time_range_condition + searchKeyword + search_col_condition + 
                        trip_condition +`
                    GROUP BY pe.id, pe.lic_plate_no, pe.type, atm.id
                ) d 
                ORDER BY d.total_trips DESC`;

  if(output_format === 'list'){
    query += " LIMIT " + limit + " OFFSET " + offset
  }


  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT}).then(function (list) {
    let count = 0;
    list.forEach(function (asset) {
      count = Number(asset.total_count);

      let inprogressTrips = [];
      let tp =  _.uniqBy(asset.tp, 'id');
      tp.forEach(function (i) {
        if(i.id){
          inprogressTrips.push({
            id: i.id,
            tid: i.tid,
            invoiceNo: i.invoice_no,
            shipmentNo: i.shipment_no,
            eta: {
              tis: i.end_tis + (i.delayed_by || 0),
              duration: Math.round(i.delayed_by / 60) || 0,
              delayed: i.delayed || false
            }
          })
        }
      });
      asset.trips = {
        transporter: asset.transporter,
        tripLoadStatus: inprogressTrips.length > 0 ? 'Loaded' : 'Unloaded',
        inprogressTrips: inprogressTrips
      };

      asset.transporter = undefined;
      asset.total_count = undefined;
      asset.tp = undefined
    });

    if(output_format === 'xls'){
      const file_name = "ASSET-LIST-INSIDE-POI-"+ id +"-"+ moment().unix() + ".xls";
      const file_path = file_base_path + file_name;

      convertAssetInsideListToXLS(list, file_name, function (err) {
        if(err){
          res.status(err.code).send(err.msg)
        }else {
          res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
          res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
          res.setHeader("Content-Type", "application/vnd.ms-excel");
          res.setHeader("Filename", file_name);

          res.download(file_path, file_name, function (err) {
            if (!err) {
              fs.unlink(file_path)
            }
          })
        }
      })
    }else{
      res.json({
        total_count: count,
        data: list
      })
    }
  }).catch(function (err) {
    console.log(err);
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send('Internal server error')
  })
};

exports.getAllAssetsRelatedToPoi = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const id = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  let limit = req.query.limit || null;
  const offset = (req.query.page || 0 ) * limit;
  const output_format = req.query.format || 'list';
  const search_cols = req.query.search;
  let search_col_condition = "";
  const status = req.query.status;
  let status_condition = "";
  const sort = req.query.sort;
  let sort_condition = " ORDER BY d.id ";
  const range_cols = req.query.range_filter;
  let range_filter_condition = "";
  let limit_condition = "";
  const nearyby_radius = (req.query.nearby_radius || 0.1) * 1000;
  const default_columns = req.query.columns === 'all';
  const fk_u_id = req.headers.fk_u_id;
  const device_status = req.query.device_status;
  let device_status_filter = "";

  if (isNaN(id)) {
    return res.status(400).send('Invalid POI ID');
  }

  if(status && status !== 'all' ){
    status_condition = ` WHERE poi_status = '${status}'`;
  }

  if(output_format === 'xls'){
    limit = null
  }

  if(limit){
    limit_condition = ` LIMIT ${limit} OFFSET ${offset}`
  }

  if(device_status){
    try{
      let ds = JSON.parse(JSON.stringify(device_status));
      if(typeof ds === 'object' && ds.length > 0){
        let statuses = _.map(ds, function (o) {
          return `'${o}'`
        });
        device_status_filter = ` AND device_status->>'statuscode' IN (${statuses})`
      }
    }catch (e){
      console.error(e)
      console.error("Error parsing device_status req params : ", device_status)
    }
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'lic_plate_no',
      client: 'lic_plate_no'
    }, {
      db: "(transporter->'name')",
      client: 'transporter.name'
    }, {
      db: "(trips->'id')",
      client: 'trips.id'
    }, {
      db: 'lname',
      client: 'lname'
    }, {
      db: "(source->'lname')",
      client: 'source.lname'
    }, {
      db: "(destination->'lname')",
      client: 'destination.lname'
    }]
  );

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'entry_tis',
      client: 'entry_tis'
    }, {
      db: 'dwell_minutes',
      client: 'dwell_minutes'
    }, {
      db: 'tis',
      client: 'tis'
    }, {
      db: 'exit_tis',
      client: 'exit_tis'
    }, {
      db: 'spd',
      client: 'spd'
    }],
    sort_condition
  );

  range_filter_condition = utils.processTableDateRangeFilter(
    range_cols,
    [{
      db: 'exit_tis',
      client: 'exit_tis'
    },{
      db: 'entry_tis',
      client: 'entry_tis'
    }],
    800 // Approx 2 years data
  );
  range_filter_condition = range_filter_condition.result;


  async.auto({
    poi: function (callback) {
      let query =
                `SELECT p.id, p.lat, p.lon  FROM groups g
                INNER JOIN poi_group_mappings pgm ON pgm.fk_group_id = g.id AND pgm.connected
                INNER JOIN pois p on p.id = pgm.fk_poi_id AND p.active
                WHERE g.active AND p.id = ${id} AND g.fk_c_id = ${fk_c_id} AND ${groupCondition}
                
                UNION ALL

                SELECT p.id, p.lat, p.lon FROM pois p
                WHERE p.active AND p.id = ${id} AND p.priv_typ IS FALSE;
                `;
      models.sequelize.query(query,
        {type: models.sequelize.QueryTypes.SELECT}
      ).spread(function (poi) {
        if(poi){
          callback(null, poi)
        }else{
          callback({code: 200, msg: 'No vehicles found'})
        }
      }).catch(function (err) {
        callback({code: 500, msg: 'Internal server error', err: err})
      })
    },
    assets: ['poi', function (result, callback) {
      const poi = result.poi;

      let main_query =
                `
            SELECT 
                *
            FROM 
              (
                WITH my_assets AS (
                  SELECT 
                    DISTINCT a.id 
                  FROM 
                    assets a 
                    INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id 
                    AND agm.connected 
                    INNER JOIN groups g ON g.id = agm.fk_group_id 
                  WHERE 
                    a.active 
                    AND g.fk_c_id = ${fk_c_id} 
                    AND ${groupCondition}
                ), 
                inside AS (
                  SELECT 
                    aps.fk_asset_id, 
                    'inside' AS status, 
                    aps.entry_tis, 
                    aps.latest_tis 
                  FROM 
                    assets_pois_stats aps 
                  WHERE 
                    aps.fk_poi_id = ${id} 
                    AND aps.exit_tis IS NULL 
                    AND aps.fk_asset_id in (
                      SELECT 
                        id 
                      FROM 
                        my_assets
                    )
                ), 
                on_trips AS (
                  SELECT 
                    t.id, 
                    t.fk_asset_id, 
                    case when t.source_poi_id :: INTEGER = ${id} then 'outgoing' else 'incoming' end as status, 
                    source, 
                    destination, 
                    eta,
                    exit_tis::INTEGER 
                  FROM 
                    (
                      SELECT 
                        id, 
                        status, 
                        fk_asset_id, 
                        waypoints -> 0 ->> 'fk_poi_id' as source_poi_id, 
                        waypoints ->(
                          json_array_length(waypoints)-1
                        )->> 'fk_poi_id' destination_poi_id, 
                        waypoints -> 0 as source, 
                        waypoints ->(
                          json_array_length(waypoints)-1
                        ) as destination, 
                        trip_log -> 'ETA' as eta,
                        trip_log->>'departure' as exit_tis 
                      FROM 
                        trip_plannings tp 
                      where 
                        tp.status = 'inprogress'
                    ) t 
                  WHERE 
                    (
                      t.source_poi_id :: INTEGER = ${id} 
                      or t.destination_poi_id :: INTEGER = ${id}
                    ) 
                    AND t.fk_asset_id in (
                      SELECT 
                        id 
                      FROM
                        my_assets
                    ) 
                    AND t.fk_asset_id not in (
                      SELECT 
                        id 
                      FROM 
                        inside
                    )
                ),
                nearby AS (
                
                    SELECT fk_asset_id, 'nearby' AS status

                    FROM current_locations 
                    WHERE fk_asset_id IN (SELECT id FROM my_assets) 
                    AND fk_asset_id NOT IN (SELECT fk_asset_id from inside)
                    AND fk_asset_id NOT IN (SELECT fk_asset_id from on_trips)
                    AND ST_DWithin(
                        ST_MakePoint(lon,lat)::geography,
                        ST_MakePoint(${poi.lon}, ${poi.lat})::geography,
                        ${nearyby_radius}
                    )
                ) 
                
                -- Query starts here
                
                SELECT 
                  COALESCE(
                    inside.status :: TEXT, on_trips.status :: TEXT, nearby.status::TEXT
                  ) as poi_status, 
                  a.id, 
                  atp.type, 
                  a.lic_plate_no, 
                  json_build_object(
                    'id', c.id, 'name', c.cname, 'code', 
                    c.transporter_code
                  ) as transporter, 
                  loc.lname, 
                  loc.lat, 
                  loc.lon, 
                  COALESCE(loc.ign, 'A') as ign, 
                  COALESCE(loc.spd, 0) as spd, 
                  COALESCE(loc.osf, false) as osf, 
                  COALESCE(loc.status, 'disconnected') as status, 
                  loc.tis, 
                  COALESCE(loc.heading :: INT, 0) as heading, 
                  loc.stationary_since,
                  loc.device_status, 
                  json_build_object('id', on_trips.id) as trips, 
                  on_trips.source, 
                  on_trips.destination,
                  on_trips.exit_tis,
                  inside.entry_tis, 
                  inside.latest_tis, 
                  (
                    inside.latest_tis - inside.entry_tis
                  )/ 60 as dwell_minutes 
                FROM 
                  assets a 
                  INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id 
                  LEFT JOIN companies c on c.id = a.fk_c_id 
                  LEFT JOIN current_locations loc on loc.fk_asset_id = a.id 
                  LEFT JOIN inside on inside.fk_asset_id = a.id 
                  LEFT JOIN on_trips on on_trips.fk_asset_id = a.id 
                  LEFT JOIN nearby on nearby.fk_asset_id = a.id 
                where 
                  a.id in (
                    select 
                      id 
                    from 
                      my_assets
                  )
              ) d 
            WHERE 
              d.poi_status is not null ${ search_col_condition + range_filter_condition + device_status_filter}
            ${sort_condition}
        `;

      async.auto({
        count_summary: function (callback) {
          let query =
                        `
                    SELECT 
                        count(id) over() as all
                        , count(CASE WHEN poi_status = 'incoming' THEN 1 END) over() incoming 
                        , count(CASE WHEN poi_status = 'outgoing' THEN 1 END) over() outgoing
                        , count(CASE WHEN poi_status = 'inside' THEN 1 END) over() inside
                        , count(CASE WHEN poi_status = 'nearby' THEN 1 END) over() nearby
                    FROM ( ${main_query}
                        ) d 
                    LIMIT 1
                    `;
          models.sequelize.query(query,
            {type: models.sequelize.QueryTypes.SELECT}
          ).spread(function (counts) {

            let data = {
              total_count: Number(counts && counts.all || 0),
              count_summary: {
                all: Number(counts && counts.all || 0),
                incoming: Number(counts && counts.incoming || 0),
                outgoing: Number(counts && counts.outgoing || 0),
                inside: Number(counts && counts.inside || 0),
                nearby: Number(counts && counts.nearby || 0),
              }
            };
            data.total_count = data.count_summary[status] || data.total_count;
            callback(null, data)
          }).catch(function (err) {
            callback({code: 500, msg: 'Internal server error', err: err})
          })
        },
        data : function (callback) {
          let query =
                        `
                    SELECT 
                        *
                    FROM ( ${main_query}
                        ) d 
                    ${status_condition}
                    ${limit_condition}
                 `;
          models.sequelize.query(query,
            {type: models.sequelize.QueryTypes.SELECT}
          ).then(function (assets) {
            async.each(assets, function (asset, cb) {

              asset.source = _.pick(asset.source,['fk_poi_id', 'lname','lat','lon','type']);
              asset.destination = _.pick(asset.destination,['fk_poi_id', 'lname','lat','lon','type']);
              asset.eta = { distance: null, duration: null };
              asset.trips = asset.trips && asset.trips.id ? asset.trips : null;

              if(asset.lat && asset.lon && poi.lat && poi.lon){

                let source_cordinates, destination_cordinates;

                switch (asset.poi_status){
                case 'incoming':
                case 'nearby':
                  source_cordinates = { lat: asset.lat, lon: asset.lon};
                  destination_cordinates = { lat: poi.lat, lon: poi.lon};
                  break;
                case 'outgoing':
                  source_cordinates = { lat: poi.lat, lon: poi.lon};
                  if(asset.destination && asset.destination.lat && asset.destination.lon){
                    destination_cordinates = { lat: asset.destination.lat, lon: asset.destination.lon}
                  }
                  break;
                }

                if(source_cordinates && destination_cordinates){
                  // Get google ETA
                  GOOGLE_SERVICES.getGoogleETA(source_cordinates, destination_cordinates, function (err, resp) {
                    asset.eta = resp;
                    cb()
                  })
                }else {
                  cb()
                }
              }else{
                cb()
              }
            }, function () {
              callback(null, assets || [])
            })

          }).catch(function (err) {
            callback({code: 500, msg: 'Internal server error', err: err})
          })
        }
      }, function (err, result) {
        let data = result.count_summary;
        data.data = result.data || [];
        callback(err, data)
      })
    }]
  }, function (err, results) {
    if(err){
      return res.status(err.code).send(err.msg)
    }else{
      if (output_format === 'xls') {
        let file_name = `A-PLACE-${id}-ASSETS-LIST-${moment().unix()}.xls`;
        let file_path = file_base_path + file_name;

        tableConfigsHelper.getTableConfig('one-place-assets', fk_u_id, status || 'all', default_columns, function (err, table_config) {
          xlsDownload.convertTableV1ToXLS(status, results.assets.data, file_base_path, file_name, table_config, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })
        })
      } else {
        res.json(results.assets)
      }
    }
  })
};

/**
 * Duplicate of previous code with optimization
 * @param req
 * @param res
 */
exports.getPoiDetentionRegionsV2 = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis;
  const primary_poi = req.query.primary_poi;
  const detention_poi = req.query.detention_poi;
  const limit = req.query.limit || 10;
  const offset = (req.query.page || 0 ) * limit;
  const output_format = req.query.format;
  const current_tis = moment().tz(utils.timezone.default, false).unix();

  const required_keys = ['primary_poi','detention_poi','start_tis','end_tis'];
  const dataValid = utils.validateMinPayload(required_keys, req.query);
  if(!dataValid){
    return res.status(422).send("Mandatory Fields "+required_keys)
  }

  let pre_query = `WITH poi_entities as (
        SELECT aps1.fk_asset_id as id, c.company_fields->>'lic_plate_no' as lic_plate_no, 
            aps1.entry_tis, aps1.exit_tis, aps1.poi_stat,
            tp.id as trip_id, --tp.status, 
            p.id as poi_id, p.typ as poi_type, p.fk_c_id, p.priv_typ, 
            at.type, null as group_id, aps1.latest_tis, 
            c.ext_consignment_no,
            tp.tid, tp.end_tis as trip_end_tis, tp.trip_log
        FROM assets_pois_stats aps1 
        INNER JOIN consignments c on aps1.fk_asset_id = c.id and aps1.enum_type = 'Consignment'
        INNER JOIN trip_plannings tp on tp.id = c.fk_trip_id
        INNER JOIN asset_types at on tp.fk_asset_type_id = at.id
        INNER JOIN pois p on p.id = aps1.fk_poi_id and p.active
        UNION ALL
        SELECT a.id, a.lic_plate_no, 
            aps.entry_tis, aps.exit_tis, aps.poi_stat,
            tp.id as trip_id, --tp.status, 
            p.id as poi_id, p.typ as poi_type, p.fk_c_id, p.priv_typ, 
            atp.type, g.id as group_id, aps.latest_tis,
            null as ext_consignment_no,
            tp.tid, tp.end_tis as trip_end_tis, tp.trip_log
        FROM assets_pois_stats aps
        INNER JOIN asset_group_mappings agm on agm.fk_asset_id = aps.fk_asset_id
        INNER JOIN groups g on g.id = agm.fk_group_id and g.active
        INNER JOIN pois p on p.id = aps.fk_poi_id and p.active
        INNER JOIN assets a on a.id = aps.fk_asset_id and a.active
        INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id
        LEFT JOIN asset_trip_maps atm on a.id = atm.fk_asset_id
        LEFT JOIN trip_plannings tp on tp.id = (
            SELECT id
            FROM trip_plannings
            where fk_asset_id =a.id
            order by "createdAt" desc limit 1
        )
        where g.id in  `+ groupIds +`
    )`;

  let query = `${pre_query}
        SELECT DISTINCT pe.id, pe.lic_plate_no, pe.entry_tis, pe.exit_tis,
            pe.trip_id,
            dsl.new_settings::jsonb as updated_detention_config,
            pdm.detention_config::jsonb as current_detention_config,
            pe.type,
            count(*) OVER()::INT AS total_count
        FROM poi_entities pe
        LEFT JOIN detention_settings_logs dsl on dsl.id = (
            SELECT id
            FROM detention_settings_logs
            WHERE fk_poi_id = pe.poi_id and tis <= pe.entry_tis
            ORDER BY "createdAt" desc limit 1
        )
        LEFT JOIN poi_detention_mappings pdm on pdm.fk_detention_poi_id= pe.poi_id and mapped
        WHERE pe.poi_id = ${detention_poi} 
            and (pe.entry_tis between ${start_tis} and ${end_tis} or ( pe.latest_tis between ${start_tis} and ${end_tis}))                        
        GROUP BY pe.lic_plate_no, pe.entry_tis, pe.exit_tis, pe.trip_id, pe.id, pe.type, dsl.new_settings::jsonb, pdm.detention_config::jsonb
        ORDER BY pe.entry_tis desc
        LIMIT ${limit} OFFSET ${offset}`;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    let total_count =  _.size(data) ? data[0].total_count : 0;

    async.each(data, function(asset, sCallback){
      asset.detention_config = asset.updated_detention_config || asset.current_detention_config;

      let query = `SELECT a.id, aps.*
            FROM assets_pois_stats aps
            INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = aps.fk_poi_id and pgm.connected
            INNER JOIN groups g on g.id = pgm.fk_group_id and g.active
            INNER JOIN pois p on p.id = aps.fk_poi_id
            LEFT JOIN assets a on a.id = aps.fk_asset_id and a.active and aps.enum_type = 'Vehicle'
            WHERE p.active AND p.id = ${primary_poi} AND g.id in ${groupIds}
                and (aps.entry_tis between ${asset.entry_tis} and ${asset.exit_tis || current_tis}  
                    or (aps.entry_tis >= ${asset.entry_tis} and aps.exit_tis is null)
                )
            ORDER BY aps.entry_tis asc
            LIMIT 1 `;
      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT}).spread(function (data) {
        if(data){
          asset.exit_tis = data.entry_tis;
          asset.detention_mins = parseInt((data.entry_tis - asset.entry_tis)/60)
        }
        else{
          asset.detention_mins = parseInt(((asset.exit_tis || current_tis) - asset.entry_tis)/60)
        }
        process.nextTick(sCallback)
      }).catch(function () {
        process.nextTick(sCallback)
      });

      delete asset.total_count;
      delete asset.updated_detention_config;
      delete asset.current_detention_config

    }, function(){
      if(output_format === 'xls'){
        let file_name = "DETENTION-ASSETS-"+ fk_c_id +"-"+ moment().unix() + ".xls";
        let file_path = file_base_path + file_name;

        convertDetentionListToXLS(data, file_name, function (err) {
          if(err){
            res.status(err.code).send(err.msg)
          }else {
            res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);
            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      } else {
        res.json({
          total_count: total_count,
          data: data
        })
      }
    })
  }).catch(function(err){
    console.log(err);
    res.status(500).send("Internal Server Error")
  })
};

/**
 * Api to fetch detention settings log
 * @param {*} req 
 * @param {*} res 
 */
exports.getDetentionSettingLog = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const detention_poi = req.query.detention_poi;
  const searchKeyword = req.query.q ? ` AND u.name ilike '%`+req.query.q +`%'`: '';
  const limit = req.query.limit || 10;
  const offset = (req.query.page || 0 ) * limit;

  const requiredKeys = ['detention_poi'];
  const dataValid = utils.validateMinPayload(requiredKeys, req.query);
  if(!dataValid)
    return res.status(422).send("Mandatory fields: "+requiredKeys);

  let query = `select new_settings as detention_config, tis, u.name, count(*) OVER() AS total_count 
                from detention_settings_logs dsl
                INNER JOIN users u on dsl.fk_u_id = u.id
                INNER JOIN pois p on dsl.fk_poi_id = p.id and p.fk_c_id = `+fk_c_id+`
                where fk_poi_id = `+ detention_poi + searchKeyword+`
                LIMIT ` + limit + ` OFFSET ` + offset;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT}).then(function (list) {
    let total_count = 0;
    async.each(list, function(data, callback){
      total_count = data.total_count;
      delete data.total_count;
      callback()
    }, function(){
      let output = {
        total_count : total_count,
        data : list
      };
      res.json(output)
    })
  }).catch(function(err){
    console.log(err);
    res.status(500).send("Internal Server Error")
  })
};
