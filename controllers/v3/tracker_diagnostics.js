/**
 * Created by numadic on 08/08/17.
 */

const async = require('async');
const _ = require('lodash');
const moment = require('moment-timezone');
const models = require('../../db/models/index');
const utils = require('../../helpers/util');
const fs = require('fs');
const Excel = require('exceljs');
const request = require('request');
const nuRedis = require('../../helpers/nuRedis');
const env       = process.env.NODE_ENV || 'development';
const config    = require(__dirname + '/../../config/index.js')[env];
const file_base_path = __dirname + '/../../temp_reports/';
const nodemailer = require('nodemailer');

function processDeviceStatus(id, data) {
  let device_status =  _.filter(data, {'id': id});
  if(device_status.length > 0) {
    device_status[0].msg = [device_status[0].msg];
    return device_status[0]
  }
}

function convertToXLS(data, type, filename, callback) {

  try {
        
    let workbook = new Excel.Workbook();
    let sheet_name = "";
    let width_arr = [ {width: 30},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30}
    ];
    let header_arr = [];
    if(type === 'veh_trackers') {
      sheet_name = "Vehicle Trackers";
      header_arr = ['Tracker-ID', 'Asset', 'Last Location', 'Status', 'Last Serviced', 'No. of servicings']
    } else if(type === 'portable_trackers') {
      sheet_name === "Portable Trackers";
      header_arr = ['Tracker-ID', 'Consignment No', 'Asset', 'Last Location', 'Status', 'Last Serviced']
    } else  if(type === 'serv_logs') {
      sheet_name === "Asset Servicing Logs";
      header_arr = ['Service-ID', 'Tracker-ID', 'Task', 'Location', 'Status', 'Servicing Date']
    }

    let ws = workbook.addWorksheet(sheet_name);
    ws.font = {name: 'Proxima Nova'};
    ws.columns = width_arr;

    ws.addRow(header_arr);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    if (type === 'veh_trackers') {
      data.forEach(function (i) {
        let time = i.service_tis ? moment.unix(i.service_tis).tz(utils.timezone.default, false).format('Do MMM YY h:mma') : '-';
        let device_status = i.device_status ? i.device_status.status : '-';
                
        ws.addRow([i.e_id, i.lic_plate_no, i.lname, device_status, time, i.no_of_servicing ])
      })
    } else if(type === 'portable_trackers') {
      data.forEach(function (i) {
        let time = i.service_tis ? moment.unix(i.service_tis).tz(utils.timezone.default, false).format('Do MMM YY h:mma') : '-';
        let device_status = i.device_status ? i.device_status.status : '-';
        ws.addRow([i.e_id, i.ext_consignment_no, i.lic_plate_no, i.lname, device_status, time ])
      })
    } else if(type === 'serv_logs') {
      data.forEach(function (i) {
        let time = i.completed_tis ? moment.unix(i.completed_tis).tz(utils.timezone.default, false).format('Do MMM YY h:mma') : '-';
        let status = i.completed_tis ? 'Completed' : 'Pending';
        ws.addRow([i.service_id, i.e_id, i.log_type, i.lname, status, time ])
      })
    }
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback()
      }).catch(function (err) {
        console.log("XLS ERROR : ", err);
        callback({
          code: 500,
          msg: 'Internal error'
        })
      });
  }catch(e){
    console.log(e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

function convertArrayToTuple(assetsIDs, allStatus) {
  let assetLocData = "";
  let count = 0;
  let obj = [];

  assetsIDs.forEach(function (value) {
    obj.push({
      asset_id: value,
      device_status: processDeviceStatus(value, allStatus)
    })
  });

    
  obj.forEach(function (value) {
        
    if (count !== 0 && count !== assetsIDs.length) {
      assetLocData += ","
    }

    assetLocData += "(" + value.asset_id + ",'" + value.device_status.statuscode  + "','" + JSON.stringify(value.device_status) + "')";
       
    count++
  });
  return assetLocData
}

exports.getTrackersSummary = function (req, res) {

  let fk_c_id = req.headers.fk_c_id;
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let gid = req.query.gid;
  let groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  let scheduled = 0;
  let query =
        `
        SELECT distinct on (a.id) a.id, asl.servicing_logs 
        FROM groups g  
        INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
        INNER JOIN assets a on a.id = agm.fk_asset_id 
        INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id
        INNER JOIN entity_mappings em on em.fk_asset_id = a.id AND em.enum_type = 'Vehicle' AND em.connected
        INNER JOIN entities e on e.id = em.fk_entity_id
        LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
        WHERE g.fk_c_id = ${fk_c_id} AND ${groupCondition}`;

  async.parallel({
    trackers: function (cb) {
      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
        .then(function (summary) {
          summary.forEach(function (s) {
            if(s.servicing_logs && s.servicing_logs[0].completed_tis === null) {
              scheduled++
            }
          });
          cb(null, {
            total: summary ? summary.length : 0,
            scheduled : scheduled
          })
        }).catch(function (err) {
          cb({code: 500, msg: 'Internal server error', err: err})
        })
    },
    portable: function (cb) {

      let portable_query = "select count(cdi.fk_entity_id) from company_device_inventory_vw cdi where cdi.fk_c_id = :fk_c_id";
      models.sequelize.query(portable_query, { replacements: {fk_c_id: fk_c_id}, type: models.sequelize.QueryTypes.SELECT })
        .spread(function (portable) {
          let portable_count = portable ? parseInt(portable.count): 0;
          cb(null, portable_count)
        }).catch(function (err) {
          cb({code: 500, msg: 'Internal server error', err: err})
        })
    }
  }, function(err, result) {
    if(err){
      return res.status(err.code).send(err.msg)
    }
    result.total = result ? result.trackers.total : 0;
    result.scheduled = result ? result.trackers.scheduled : 0;
    result.trackers = undefined;
    res.send(result)
  })
};

exports.getAllAssetTrackers = function (req, res, apiType) {

  let fk_c_id = req.headers.fk_c_id;
  let limit = req.query.limit || 10;
  let offset = (req.query.page || 0) * limit;
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let gid = req.query.gid;
  let groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  let search_cols = req.query.search;
  let search_col_condition = "";
  let dsfilterWhereClause = "";
  let sort = req.query.sort;
  let sort_condition = " ORDER BY service_tis ASC ";
  const range_cols = req.query.range_filter;
  let range_filter_condition = "";
  let output_format = req.query.format || 'list';

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'lic_plate_no',
      client: 'lic_plate_no'
    }, {
      db: 'e_id',
      client: 'e_id'
    }]
  );

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'e_id',
      client: 'e_id'
    }, {
      db: 'lic_plate_no',
      client: 'lic_plate_no'
    }, {
      db: 'service_tis',
      client: 'service_tis'
    }, {
      db: 'no_of_servicing',
      client: 'no_of_servicing'
    }],
    sort_condition
  );

  range_filter_condition = utils.processTableDateRangeFilter(
    range_cols,
    [{
      db: "(asl.servicing_logs->0->>'completed_tis')::TEXT::INT",
      client: 'service_tis'
    }],
    800 // Approx 2 years data
  );
  range_filter_condition = range_filter_condition.result;

  const filters = {
    deviceStatus: req.query.device_status || ''
  };

  if(filters.deviceStatus.length > 0) {
    const t = [];
    for (let i = 0; i < filters.deviceStatus.length; i++) {
      t.push("'" + filters.deviceStatus[i] + "'");
    }
    dsfilterWhereClause = " AND loc.device_status_val IN (" + t +  ")" 
  }

  async.waterfall([
    function (cb) {
      const asset_ids = [];
      const query = `SELECT distinct on (a.id) a.id as asset_id
            FROM groups g
            INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
            INNER JOIN assets a on a.id = agm.fk_asset_id
            INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = 'Truck'
            INNER JOIN entity_mappings em on em.fk_asset_id = a.id AND em.enum_type = 'Vehicle' AND em.connected
            INNER JOIN entities e on e.id = em.fk_entity_id
            WHERE g.fk_c_id = ${fk_c_id} AND ${groupCondition} LIMIT 10;`;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (assets) {
        if(assets.length > 0){
          async.parallel({
            deviceStatus: function(cb1) {
                            
              _.each(assets, function (i) {
                asset_ids.push(i.asset_id)
              });
              const request_options = {
                uri: config.server_address.wad_server() + '/asset/status',
                method: 'POST',
                headers: {
                  'Authorization': 'bearer ' + config.tokens.wad_common
                },
                json: {
                  "asset_ids": asset_ids
                }
              };
              request(request_options, function (error, response, body) {
                if (error) {
                  console.log("WAD api error ", error);
                  cb1(null, [])
                } else {
                  if (response.statusCode === 200) {
                    cb1(null, body.data)
                  } else {
                    console.log("Wad api responseCode not 200", response.statusCode, body);
                    cb1(null, [])
                  }
                }
              });

            }
          }, function(err, data) {

            cb(null, convertArrayToTuple(asset_ids, data.deviceStatus))
          })
                   
        }else{
          cb({
            code: 404,
            msg: 'Not Found'
          })
        }
      }).catch(function (err) {
        cb(err)
      })
    },function (loc_data, cb) {

            
      if(loc_data.length === 0) {
        loc_data = '(null, null,null)'
      }

      let query = `SELECT *,  count(*) OVER() AS total_count FROM (
                SELECT distinct on (a.id) a.id as asset_id, a.asset_id as aid, a.lic_plate_no, atp.type, e.e_id, COALESCE(asl.servicing_logs->0->>'completed_tis', null) service_tis, COALESCE(asl.servicing_logs->0->>'log_type', null) log_type, loc.device_status, c.cname as operator, json_array_length(COALESCE(asl.servicing_logs, '[]')) no_of_servicing  
                FROM groups g  
                INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
                INNER JOIN assets a on a.id = agm.fk_asset_id 
                INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = 'Truck'
                INNER JOIN entity_mappings em on em.fk_asset_id = a.id AND em.enum_type = 'Vehicle' AND em.connected
                INNER JOIN entities e on e.id = em.fk_entity_id
                LEFT JOIN companies c on c.id = a.fk_c_id
                LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
                LEFT JOIN ( SELECT * FROM (VALUES ${loc_data}) AS t (fk_asset_id, device_status_val, device_status)) as loc on a.id = loc.fk_asset_id
                WHERE g.fk_c_id = ${fk_c_id}
                AND ${groupCondition + search_col_condition + dsfilterWhereClause + range_filter_condition}) x ${sort_condition}`;

      if (apiType === 'list' && output_format === 'list') {
        query += ` LIMIT ${limit} OFFSET ${offset}`;
      }

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (service_list) {
        let totalAssets = 0;
        async.forEachOf(service_list, function (service, i, callback) {
          totalAssets = parseInt(service.total_count);
          service.total_count = undefined;
          if(service.device_status !== "undefined") {
            service.device_status = JSON.parse(service.device_status)
          }
          nuRedis.getAssetDetails(service.asset_id, function (err, value) {
            service.lname = null;
            service.tis = null;
            if (value) {
              service.lname = value.lname;
              service.tis = value.tis
            }
            callback()
          })
                   
        }, function () {
          if (apiType === 'list') {
            cb(null, {
              total_count: totalAssets,
              data: service_list
            });
          } else {
            cb(null, service_list);
          }
        })

                
      }).catch(function (err) {
        console.log(err);
        cb(err)
      })
    }],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }else{
        res.status(500).send()
      }
    }else{
            
      if (output_format === 'xls') {
        const file_name = "VEHICLE-TRACKERS-LIST-" + fk_c_id + "-" + moment().unix() + ".xls";
        const file_path = file_base_path + file_name;

        convertToXLS(result.data, 'veh_trackers', file_name, function (err) {
          if (err) {
            res.status(err.code).send(err.msg)
          } else {
            res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      }else{
        res.json(result)
      }
    }
  })
        
};


exports.getAssetTrackerDetail = function (req, res) {
  const companyId = req.headers.fk_c_id;
  const id = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis;
  const output_format = req.query.format || 'list';

  if (isNaN(id)) {
    return res.status(400).send('Invalid asset ID');
  }

  const query = `SELECT distinct on (a.id) a.id as asset_id, asl.servicing_logs, em.connected mapped 
    FROM groups g  
    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
    INNER JOIN assets a on a.id = agm.fk_asset_id 
    INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = 'Truck'
    INNER JOIN entity_mappings em on em.fk_asset_id = a.id AND em.enum_type = 'Vehicle' AND em.connected
    LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
    WHERE g.fk_c_id = ${companyId} AND ${groupCondition} AND a.id = ${id}
    ORDER BY a.id, em.connected desc, em."updatedAt" DESC`;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (serviceData) {
      if(serviceData) {

        if(start_tis && end_tis) {
          serviceData.servicing_logs = _.filter(serviceData.servicing_logs, function(o) {
            if(o.completed_tis >= start_tis && o.completed_tis <= end_tis) {
              return o
            }
          })
        }

        async.forEachOf(serviceData.servicing_logs, function (service, i, callback) {
          if(service.awaiting_deployer && service.awaiting_deployer.length > 0) {
            service.awaiting_deployer = service.awaiting_deployer[service.awaiting_deployer.length - 1].notes
          }
          if(service.awaiting_client && service.awaiting_client.length > 0) {
            service.awaiting_client = service.awaiting_client[service.awaiting_client.length - 1].notes
          }
          revGeoCode(service.lat, service.lon, function (err, value) {
            service.lname = null;
            if (value) {
              service.lname = value
            }
            callback()
          });

          if (service.completed_tis && service.completed_tis > 0) {
            service.status = 'Completed';
          } else {
            service.status = 'Pending';
          }

          service.notes = _.pick(service, ['awaiting_deployer', 'awaiting_client', 'assigner_notes', 'reported_cause']);
        }, function () {
          if(output_format === 'xls') {
            const file_name = "ASSET-SERVICING-LOGS-" + id + "-" + companyId + "-" + moment().unix() + ".xls";
            const file_path = file_base_path + file_name;

            if (serviceData.servicing_logs && serviceData.servicing_logs.length > 0) {
              convertToXLS(serviceData.servicing_logs, 'serv_logs', file_name, function (err) {
                if (err) {
                  res.status(err.code).send(err.msg)
                } else {
                  res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
                  res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
                  res.setHeader("Content-Type", "application/vnd.ms-excel");
                  res.setHeader("Filename", file_name);

                  res.download(file_path, file_name, function (err) {
                    if (!err) {
                      fs.unlink(file_path)
                    }
                  })
                }
              })
            } else {
              res.json("No data to download")
            }
          } else {
            res.json(serviceData);
          }
        })
                
      } else {
        res.json('Not found')
      }
    }).catch(function (err) {
      console.log(err);
      res.status(500).send();
    })
};

exports.getVehicleTrackerSummary = function (req, res) {
  const companyId = req.headers.fk_c_id;
  const id = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if (isNaN(id)) {
    return res.status(400).send('Invalid asset ID');
  }

  const query = `SELECT distinct on (a.id) a.id as asset_id, a.lic_plate_no, atp.type, e.e_id, c.cname as operator,
    json_array_length(asl.servicing_logs) no_of_services, l.tis, l.device_status 
    FROM groups g  
    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
    INNER JOIN assets a on a.id = agm.fk_asset_id 
    INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = 'Truck'
    LEFT JOIN companies c on c.id = a.fk_c_id
    INNER JOIN entity_mappings em on em.fk_asset_id = a.id AND em.enum_type = 'Vehicle' AND em.connected
    INNER JOIN entities e on e.id = em.fk_entity_id
    LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
    LEFT JOIN current_locations l on l.fk_asset_id = a.id
    WHERE g.fk_c_id = ${companyId} AND ${groupCondition} AND a.id = ${id}`;


  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (serviceData) {
      res.json(serviceData)
    }).catch(function (err) {
      console.log(err);
      res.status(500).send();
    })
};

let revGeoCode = function(lat, lon, cb ) {
  const options = {
    method: 'GET',
    url: `${config.googleMap.url}latlng=${lat},${lon}&key=${config.googleMap.key}`
  };
  let lname = "";
  request(options, function (err, response, body) {
    body = JSON.parse(body);
    if (err || body.status !== "OK") {
      return cb(null, lname)
    }
    try {
      lname = body.results[0]['formatted_address']
    }
    catch (e) {
      console.log(e)
    }
    cb(null, lname)
  });
};

exports.getAvailbleConsignmentEntities = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const output_format = req.query.format || 'list';
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  let search_col_condition = "";
  if (searchKeyword.length > 0) {
    search_col_condition = " AND (c.company_fields->>'lic_plate_no' ilike '%" + searchKeyword + "%' OR e.e_id ilike '%" + searchKeyword + "%' OR c.ext_consignment_no ilike '%" + searchKeyword + "%') "
  }


  let query = `SELECT *, count(e_id) OVER()::INT AS total_count FROM (
                    SELECT DISTINCT ON (cmvw.fk_entity_id) tp.status, e.e_id, u.name, cmvw.connected, cmvw.fk_consignment_id, c.ext_consignment_no , c.company_fields->>'lic_plate_no' lic_plate_no, COALESCE(asl.servicing_logs->0->>'completed_tis', null) service_tis                        
                    FROM consignment_mapping_vw cmvw
                    INNER JOIN entities e on cmvw.fk_entity_id = e.id
                    LEFT JOIN consignments c on cmvw.fk_consignment_id = c.id
                    LEFT JOIN trip_plannings tp on cmvw.fk_consignment_id = tp.fk_asset_id --and tp.status = 'completed'
                    LEFT JOIN assets a on a.lic_plate_no = c.company_fields->>'lic_plate_no' and a.active
                    LEFT JOIN asset_types at on tp.fk_asset_type_id = at.id and at.type = 'Consignment'
                    LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
                    LEFT JOIN users u on cmvw.fk_u_id = u.id
                    inner join (
                        select fk_entity_id, max(tis) as max_tis 
                        from consignment_mapping_vw 
                        group by fk_entity_id
                    ) latest_record on (latest_record.fk_entity_id = cmvw.fk_entity_id and latest_record.max_tis = cmvw.tis) OR cmvw.tis is null
                    WHERE cmvw.fk_c_id = ` + fk_c_id + ` and 
                        case when cmvw.connected = true
                            then (cmvw.fk_consignment_id is not null and tp.status = 'completed')
                            else (cmvw.connected = false )
                        end ` + search_col_condition + `
                ) ent
                order by name`;


  if (output_format !== 'xls') {
    query += " LIMIT " + limit + " OFFSET " + offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function(data){
    const total_count = data[0] ? data[0].total_count : 0;
    async.each(data, function(value, callback){
      delete value.total_count;

      if(value.connected === true)
        delete value.name;
                
      nuRedis.getEntityDetails(value.e_id, function (err, nubot) {
        value.lname = nubot && nubot.lname ? nubot.lname : null;
        value.tis = nubot && nubot.tis ? nubot.tis : null;
        callback()
      })
    }, function(){
      if (output_format === 'xls') {
        const file_name = "PORTABLE-TRACKERS-LIST-AVAILABLE-" + fk_c_id + "-" + moment().unix() + ".xls";
        const file_path = file_base_path + file_name;

        convertToXLS(data, 'portable_trackers', file_name, function (err) {
          if (err) {
            res.status(err.code).send(err.msg)
          } else {
            res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      } else {
        res.json({
          total_count: total_count,
          data: data
        })
      }
           
    })
  }).catch(function(err){
    console.log(err);
    res.status(500).send("Internal Server Error")
  })
};

/**
 * This api gets list of in-use entities mapped to consignments
 * @param {*} req 
 * @param {*} res 
 */
exports.getMappedConsignments = function(req, res){
  const fk_c_id = req.headers.fk_c_id;

  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const output_format = req.query.format || 'list';
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  let search_col_condition = "";
  if (searchKeyword.length > 0) {
    search_col_condition = " AND (c.company_fields->>'lic_plate_no' ilike '%" + searchKeyword + "%' OR e.e_id ilike '%" + searchKeyword + "%' OR c.ext_consignment_no ilike '%" + searchKeyword + "%') "
  }

  let query = `SELECT e.e_id, cmvw.fk_consignment_id, c.ext_consignment_no,  
                tp.status, u.name, c.company_fields->>'lic_plate_no' as lic_plate_no,  COALESCE(asl.servicing_logs->0->>'completed_tis', null) service_tis,
                count(e.e_id) OVER()::INT AS total_count
                FROM consignment_mapping_vw cmvw
                INNER JOIN entities e on cmvw.fk_entity_id = e.id
                INNER JOIN (
                select fk_entity_id, max(tis) as max_tis
                from consignment_mapping_vw 
                where fk_c_id = ` + fk_c_id + `
                group by fk_entity_id
                ) latest_record on (latest_record.fk_entity_id = cmvw.fk_entity_id and latest_record.max_tis = cmvw.tis)
                INNER JOIN consignments c on cmvw.fk_consignment_id = c.id
                INNER JOIN trip_plannings tp on c.id = tp.fk_asset_id
                INNER JOIN asset_types at on tp.fk_asset_type_id = at.id and type='Consignment'
                LEFT JOIN assets a on a.lic_plate_no = c.company_fields->>'lic_plate_no' and a.active
                LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
                LEFT JOIN users u on cmvw.fk_u_id = u.id
                WHERE cmvw.fk_c_id = ` + fk_c_id + ` and cmvw.connected and tp.status != 'completed'` + search_col_condition;

  if (output_format !== 'xls') {
    query += " LIMIT " + limit + " OFFSET " + offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function(data){
    const total_count = data[0] ? data[0].total_count : 0;
    async.each(data, function(value, callback){
      delete value.total_count;
      nuRedis.getEntityDetails(value.e_id, function (err, nubot) {
        value.lname = nubot && nubot.lname ? nubot.lname : null;
        value.tis = nubot && nubot.tis ? nubot.tis : null;
        callback()
      })
    }, function(){
      if (output_format === 'xls') {
        const file_name = "PORTABLE-TRACKERS-LIST-IN-USE-" + fk_c_id + "-" + moment().unix() + ".xls";
        const file_path = file_base_path + file_name;

        convertToXLS(data, 'portable_trackers', file_name, function (err) {
          if (err) {
            res.status(err.code).send(err.msg)
          } else {
            res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      } else {
        res.json({
          total_count: total_count,
          data: data
        })
      }
            
    })
  }).catch(function(){
    res.status(500).send("Internal Server Error")
  })
};

exports.getUnacknowledgedConsignments = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;

  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const output_format = req.query.format || 'list';
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  let search_col_condition = "";
  if (searchKeyword.length > 0) {
    search_col_condition = " AND (c.company_fields->>'lic_plate_no' ilike '%" + searchKeyword + "%' OR e.e_id ilike '%" + searchKeyword + "%' OR c.ext_consignment_no ilike '%" + searchKeyword + "%') "
  }

  let query = `SELECT e.e_id, cmvw.fk_consignment_id, c.ext_consignment_no,  
                tp.status, u.name, c.company_fields->>'lic_plate_no' as lic_plate_no,  COALESCE(asl.servicing_logs->0->>'completed_tis', null) service_tis,
                count(e.e_id) OVER()::INT AS total_count
                FROM consignment_mapping_vw cmvw
                INNER JOIN entities e on cmvw.fk_entity_id = e.id
                INNER JOIN (
                select fk_entity_id, max(tis) as max_tis
                from consignment_mapping_vw 
                where fk_c_id = ` + fk_c_id + `
                group by fk_entity_id
                ) latest_record on (latest_record.fk_entity_id = cmvw.fk_entity_id and latest_record.max_tis = cmvw.tis)
                INNER JOIN consignments c on cmvw.fk_consignment_id = c.id
                INNER JOIN trip_plannings tp on c.id = tp.fk_asset_id
                INNER JOIN asset_types at on tp.fk_asset_type_id = at.id and type='Consignment'
                LEFT JOIN assets a on a.lic_plate_no = c.company_fields->>'lic_plate_no' and a.active
                LEFT JOIN asset_servicing_logs_vw asl on asl.id = a.id
                LEFT JOIN users u on cmvw.fk_u_id = u.id
                WHERE cmvw.fk_c_id = ` + fk_c_id + ` and cmvw.connected and tp.status = 'completed' ` + search_col_condition;

  if (output_format !== 'xls') {
    query += " LIMIT " + limit + " OFFSET " + offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function(data){
    const total_count = data[0] ? data[0].total_count : 0;
    async.each(data, function(value, callback){
      delete value.total_count;
      nuRedis.getEntityDetails(value.e_id, function (err, nubot) {
        value.lname = nubot && nubot.lname ? nubot.lname : null;
        value.tis = nubot && nubot.tis ? nubot.tis : null;
        callback()
      })
    }, function(){
      if (output_format === 'xls') {
        const file_name = "PORTABLE-TRACKERS-LIST-UNACKNOWLEDGED-" + fk_c_id + "-" + moment().unix() + ".xls";
        const file_path = file_base_path + file_name;

        convertToXLS(data, 'portable_trackers', file_name, function (err) {
          if (err) {
            res.status(err.code).send(err.msg)
          } else {
            res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      } else {
        res.json({
          total_count: total_count,
          data: data
        })
      }

    })
  }).catch(function(){
    res.status(500).send("Internal Server Error")
  })
};

exports.reportDeviceStatus = function (req, res) {

  let lic_plate_no = req.body.lic_plate_no;
  let transporter_name = req.body.transporter_name || "";
  let issue = req.body.issue || "";
  let description = req.body.description || "";
  let notes = req.body.notes || "";
  let down_since = req.body.down_since || "";
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;

  if (!lic_plate_no) {
    return res.send("Lic plate missing")
  }

  const transporter = nodemailer.createTransport(config.mailer);
  let to_email = 'haston@numadic.com', from_email = 'noreply@numadic.com', cs_email = 'cs@numadic.com';

  async.auto({
    user: function (cb) {

      let query = `SELECT DISTINCT c.id, c.cname, u.email, u.name, u.cont_email FROM
              companies c
              INNER JOIN groups g on g.fk_c_id = c.id
              INNER JOIN user_group_mappings ugm on ugm.fk_group_id = g.id
              INNER JOIN users u on u.id = ugm.fk_u_id
              WHERE c.id = ${fk_c_id} AND u.id = ${fk_u_id}`;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (data) {
        if (data) {
          cb(null, data)
        } else {
          cb({
            code: 404,
            msg: 'Not Found'
          })
        }
      }).catch(function (err) {
        cb(err)
      })
    },
    support_email: ['user', function (result, cb) {
      let data = result.user;

      let reciever_email = to_email;
      let subject = `WV - Nubot status - ${data.id} - ${data.cname} - ${lic_plate_no} - ${moment().format("YYMMDD")} - INT`;

      if (env === 'production') {
        reciever_email = cs_email
      }else{
        subject = "[DEV] " + subject
      }

      let mailOptions = {
        from: from_email,
        to: reciever_email,
        subject: subject,
        bcc: from_email,

        html: `<p>
              <strong>CompanyID</strong> <br/>${data.id}
              <br/> <br/><strong>Company name<br/></strong>${data.cname}
              <br/> <br/><strong>User name<br /> </strong>${data.name}
              <br/><br/><strong>Email ID<br/> </strong>${data.email}
              <br/><br/><strong>License<br/> </strong>${lic_plate_no}
              <br/><br/><strong>Transporter<br/> </strong>${transporter_name}
              <br/><br/><strong>Issue<br/> </strong>${issue}
              <br/><br/><strong>Description<br/> </strong>${description}
              <br/><br/><strong>Down since<br/> </strong>${down_since}
              <br/> <br/><strong>Notes<br/> </strong>${notes}
              </p>`
      };

      transporter.sendMail(mailOptions, function (error) {
        if (error) {
          return cb(error);
        }
        cb(null, 'Success');
      });

    }],
    customer_email: ['user', function (data, cb) {

      let subject = `Numadic nubot report issue - ${moment().format("YYMMDD")} - ${lic_plate_no}`;
      let cc_email = '';
      let reciever_email = to_email;

      if (env === 'production') {
        reciever_email = data.user.cont_email || data.user.email;
        cc_email = cs_email;
      }else{
        subject = "[DEV] " + subject
      }

      let mailOptions = {
        from: from_email,
        to: reciever_email,
        subject: subject,
        bcc: from_email,
        cc: cc_email,

        html: `<body style="color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;width:80%">
              <p>Hi,</p>
              <p>Thank you for informing us. We have received your request for inspection/servicing.</p>
              <p>For us to look into this, we request you to share the availability of this asset in the format mentioned below.</p><br/>
              <p><b>Date available for servicing:<br/>
              Depot/Location it will be available at: <br/>
              Contact person:
              </b></p><br/>
              <p style="font-size:12px;color:#888;padding-bottom:10px;">
              Please note: Charges applicable w.r.t deployments are mentioned in your welcome mail.
              If you haven't received the mail, you can contact our team at +91-777-407-8870 for any queries/concerns.</p>
              <p>Regards,<br/>
              Customer Success Team<br/>
              <span style="color:#888">+91-777-407-8870<span><br/></p>
              </body>`
      };

      transporter.sendMail(mailOptions, function (error) {
        if (error) {
          return cb(error);
        }
        cb(null, 'Success');
      });
    }]
  }, function (err) {
    if (err) {
      res.status(err.code).send(err.message)
    } else {
      res.send("Success")
    }
  })
};