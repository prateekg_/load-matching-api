const async = require('async');
const request = require('request');
const models = require('../../db/models/index');
const utils = require('../../helpers/util');
const APP_LOGGER = require('../../helpers/logger');
const LOGGER = APP_LOGGER.logger;
const auth = require(process.cwd() + '/authorization');
const env       = process.env.NODE_ENV || 'development';
const config    = require(__dirname + '/../../config/index.js')[env];

function validateAsset(assetId, groupIds, callback) {

  let query = "SELECT a.id, em.fk_entity_id, esm.fk_sensor_id FROM groups g " +
        " INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected " +
        " INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
        " INNER JOIN entity_mappings em ON em.fk_asset_id = a.id and em.connected " +
        " INNER JOIN entity_sensor_mappings esm on esm.fk_entity_id = em.fk_entity_id and esm.fk_asset_id = a.id and esm.connected " +
        " INNER JOIN sensors s on s.id = esm.fk_sensor_id " +
        " INNER JOIN nubot_sensor_types nst on nst.id = s.fk_sensor_type and s.active " +
        " WHERE g.active AND g.id in " + groupIds + " AND a.id = " + assetId + 
        " AND nst.sensor_type = 'Immobilizer'" +
        " GROUP BY a.id, em.fk_entity_id, esm.fk_sensor_id LIMIT 1";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (asset) {
    if (!asset) {
      return callback({
        message: 'Invalid asset id or asset does not have an immobiliser sensor',
        code: 404
      })
    }
    return callback(null, asset)
  }).catch(function (err) {
    callback({
      error: err,
      message: 'Internal server error',
      code: 500
    })
  })
}

function validateUser(fk_c_id, fk_u_id, callback) {

  let query = " SELECT g.is_admin, u.cont_no, u.email FROM user_group_mappings ugm " +
                " INNER JOIN groups g ON g.id = ugm.fk_group_id AND g.active" +
                " INNER JOIN users u on u.id = ugm.fk_u_id" +
                " WHERE ugm.connected AND g.is_admin AND g.fk_c_id = " + fk_c_id + " AND fk_u_id = " + fk_u_id;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (result) {
    if (result && result.is_admin) {
      callback(null, result)
    } else {
      callback({
        code: 403,
        msg: 'User does not have permissions'
      })
    }
  }).catch(function (err) {
    callback({
      error: err,
      message: 'Internal server error',
      code: 500
    })
  })
}

exports.generateOtp = function (req, res) {
  const assetId = req.body.fk_asset_id;
  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const userFeatureList = JSON.parse(req.headers['feature-list']);
    
  if (!auth.FEATURE_LIST.hasImmobilizerSensor(userFeatureList)) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'No immobiliser feature',
      code: 400
    })
  }

  if (!fk_u_id || !fk_c_id) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Headers missing',
      code: 400
    })
  }

  if (!assetId) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'fk asset id in body missing',
      code: 400
    })
  }

  async.auto({
    validateUser: validateUser.bind(null, fk_c_id, fk_u_id),
    validateAsset: validateAsset.bind(null, assetId, groupIds),
    sendOtp: ['validateAsset', 'validateUser', function (result, cb) {
      let request_options = {
        uri: config.otp.url + '/v1/otp/generate',
        method: 'POST',
        headers: {
          'Authorization': 'bearer ' + config.otp.token
        },
        json: {
          "fk_u_id": fk_u_id,
          "cont_no":  result.validateUser.cont_no,
          "u_name": result.validateUser.email,
          "platform": 'immobilize',
          "template": 'GENERAL_OTP'
        }
      };
      request(request_options, function (error, response, body) {
        if (error) {
          cb({
            error: error,
            message: 'Internal server error',
            code: 500
          })
        } else {
          if (response.statusCode === 200) {
            return cb(null, {
              code: 200,
              message: 'OTP sent'
            })
          }
          cb({
            error: body.message,
            message: 'OTP cannot be sent',
            code: response.statusCode
          })
                    
        }
      });


    }]
  }, function (err, result) {
    if (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      if(err.code === 500){
        err.error = null
      }
      return res.status(err.code).send(err)
    }
    res.status(200).send(result.sendOtp)
  })
};

exports.verifyOtp = function (req, res) {
  const assetId = req.body.fk_asset_id;
  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const otp = req.body.otp;
  const immobilize_type = req.body.immobilize_type;
  const reason = req.body.reason;
  let statusId, api;
  const userFeatureList = JSON.parse(req.headers['feature-list']);
    
  if (!auth.FEATURE_LIST.hasImmobilizerSensor(userFeatureList)) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'No immobiliser feature',
      code: 400
    })
  }

  if (!fk_u_id || !fk_c_id) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Headers missing',
      code: 400
    })
  }

  if (!immobilize_type) {
    return res.status(400).send({message: 'Invalid immobilize_type'})
  }

  if (!otp) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'OTP in body missing'
    })
  }
  if (!assetId) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'fk asset id in body missing'
    })
  }

  async.auto({
    validateUser: validateUser.bind(null, fk_c_id, fk_u_id),
    validateAsset: validateAsset.bind(null, assetId, groupIds),
    verifyOtp: ['validateAsset', function (result, cb) {
      let request_options = {
        uri: config.otp.url + '/v1/otp/verify',
        method: 'POST',
        headers: {
          'Authorization': 'bearer ' + config.otp.token
        },
        json: {
          "otp": otp,
          "fk_u_id": fk_u_id,
          "platform": 'immobilize',
          "u_name": result.validateUser.email
        }
      };
      request(request_options, function (error, response, body) {
        if (error) {
          cb({
            error: error,
            message: 'Internal server error',
            code: 500
          })
        } else {
          if (response.statusCode === 200) {
            switch (immobilize_type) {
            case 'check_speed_disable':
              statusId = 2;
              api = "immobilize";
              break;
            case 'check_speed_enable':
              statusId = 3;
              api = "mobilize";
              break;
            default:
              break
            }
                    
            if (!statusId) {
              return cb({
                message: 'Invalid status',
                code: 400
              })
            }

            if(statusId === 2 && !reason) {
              return cb({
                error: 'Bad Request',
                message: 'reason in body missing',
                code: 400
              })
            }
                    
            let common_api = {
              uri: config.server_address.wad_server() + '/common_service/asset/' + api,
              method: 'POST',
              headers: {
                'Authorization': 'bearer ' + config.tokens.wad_common
              },
              json: {
                "fk_u_id": fk_u_id,
                "fk_asset_id": assetId,
                "user_type": "client",
                "reason": reason
              }
            };

            request(common_api).pipe(res)

          } else {
            cb({
              error: body.message,
              message: 'OTP invalid',
              code: 400
            })
          }
                     
                    
        }
      });


    }]
  }, function (err, result) {
    if (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      if(err.code === 500){
        err.error = null
      }
      return res.status(err.code).send(err)
    }
    res.status(200).send(result.verifyOtp)
  })
};