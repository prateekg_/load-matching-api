
const async = require('async');
const _ = require('lodash');
const moment = require('moment-timezone');
const models = require('../../db/models/index');
const request = require('request');
const fs = require('fs');
const Excel = require('exceljs');
const geolib = require('geolib');
const env       = process.env.NODE_ENV || 'development';
const config    = require(__dirname + '/../../config/index.js')[env];
const nuRedis = require('../../helpers/nuRedis');
const utils = require('../../helpers/util');
const file_base_path = __dirname + '/../../temp_reports/';
const APP_LOGGER = require('../../helpers/logger');
const LOGGER = APP_LOGGER.logger;
const nuDynamoDb = require('../../helpers/dynamoDb');
const statistics = require('../../helpers/statistics');

// Include services
const tableConfigsHelper = require('../../helpers/table_config');
const xlsDownload = require('../../helpers/downloadHelper');
let AWS = require('aws-sdk');
const s3_config = require('../../config/aws/s3.json');
AWS.config.update(s3_config);
const POI_FEES_COLUMN = process.env.NODE_ENV ? 'fees' : 'cost';
const POI_FEES_COLUMN_KEY = process.env.NODE_ENV ? 'typeOfVehicle' : 'type_of_vehicle';

function calculateStatsFromLocationTable(groupIds, assetId, startTime, endTime, callback) {

  nuDynamoDb.getAssetLocationFromAPI({fk_asset_id: assetId, start_tis: startTime, end_tis: endTime}, function(err, locationData) {
    statistics.getAssetDetailsStats(locationData, function (response) {
      callback(null, response)
    });
  })
}

function calculateAvg(data) {
  let sum = 0;
  for (let i =0 ; i < data.length; i++) {
    sum += data[i]
  }
  return sum / data.length;
}

function convertAssetActivityToXLS(data, filename, callback) {
  try{
    console.log('convertAssetActivityToXLS');
    let date_time_format = 'DD-MM-YY, HH:mm:ss';
    let processed_data = [];
    let lic_no = data.length > 0 ? data[0].lic_plate_no : null;
    data = _.sortBy(data, 'id');
    data.forEach(function (i) {
      let logs = i.activity || [];
      if(logs.length > 1){
        logs.pop()
      }

      logs.forEach(function (a) {
        processed_data.push(a)
      })
    });

    let final_data = [];
    let count = 0;
    let data_length = processed_data.length - 1;
    processed_data.forEach(function (i) {
    // stopped event
      final_data.push({
        start_tis: i.StartTis,
        end_tis: i.EndTis,
        start_lname: i.LName,
        end_lname: i.LName,
        distance: null,
        duration: utils.FormatMinutesDHM(i.DwellTime),
        avg_spd: null,
        event_state: 'stop'
      });

      // Intransit event
      let next_item = processed_data[count + 1];

      if(data_length !== count && next_item){

        final_data.push({
          start_tis: i.EndTis,
          end_tis: next_item.StartTis,
          start_lname: i.LName,
          end_lname: next_item.LName,
          distance: i.Distance ? Number((i.Distance / 1000).toFixed(1)) : 0,
          duration: utils.FormatMinutesDHM(Math.abs(i.EndTis - next_item.StartTis)),
          avg_spd: i.AvgSpd ? i.AvgSpd + ' km/h' : '0 km/h',
          event_state: 'intransit'
        })
      }

      count++
    });

    // SORT data with latest time first
    final_data = _.orderBy(final_data, ['start_tis'],['desc']);

    try {
      let workbook = new Excel.Workbook();
      let ws = workbook.addWorksheet('Asset list');
      ws.font = {name: 'Proxima Nova'};
      ws.columns = [
        {width: 15},
        {width: 15},
        {width: 40},
        {width: 15},
        {width: 40},
        {width: 15},
        {width: 15},
        {width: 15},
        {width: 15},
      ];

      ws.addRow(['Vehicle','Start time', 'Start location', 'End time', 'End location', 'Distance(km)', 'Duration', 'Avg speed(km/h)', 'Event state' ]);
      ws.getRow(ws.rowCount).font = {size: 12, bold: true};

      final_data.forEach(function (i) {

        ws.addRow([
          lic_no,
          moment.unix(i.start_tis).tz(utils.timezone.default, false).format(date_time_format),
          i.start_lname,
          moment.unix(i.end_tis).tz(utils.timezone.default, false).format(date_time_format),
          i.end_lname, // end lname
          i.distance || '-', // distance
          i.duration,
          i.avg_spd  || '-', // avg speed
          i.event_state // event state
        ]);

        let currentRowIndex = ws.rowCount;
        for (let j = 65; j <= 79; j++) {
          let currentCol = String.fromCharCode(j);
          if (['A', 'C', 'E', 'I'].indexOf(currentCol) === -1) {
            ws.getRow(currentRowIndex).getCell(currentCol).alignment = {horizontal: 'right'};
          }
        }
      });

      ws.addRow();
      ws.addRow(['','','','','Total', _.sumBy(final_data, 'distance')]);
      ws.getRow(ws.rowCount).font = {size: 12, bold: true};

      ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

      // write to excel file
      workbook.xlsx.writeFile(file_base_path + filename)
        .then(function () {
          callback(null, final_data)
        }).catch(function () {
          callback({
            code: 500,
            msg: 'Internal error'
          })
        });
    }catch(e){
      LOGGER.warn(APP_LOGGER.formatMessage(null, e));
      console.error(e);
      callback({
        code: 500,
        msg: 'Internal error',
        err: e
      })
    }
  }catch(e){
    console.error(e)
  }
}

exports.getAllVehicles = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const q = req.query.q ? req.query.q.toLowerCase() : '';
  const limit = parseInt(req.query.limit) || 0;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const sort = req.query.sort;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? ` g.gid = '${gid}'` : ` g.id in ${groupIds}`;
  const search_cols = req.query.search;
  let search_col_condition = "";
  let sort_condition =  " ORDER BY COALESCE(tis, 0) DESC ";    //Sort asset by latest packet recieved
  const output_format = req.query.format;
  const fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';
  let limit_condition = "";
  let vehicle_status = req.query.status;
  let vehicle_status_condition = "";
  let table_search_condition = "";
  let dsfilterWhereClause = "";
  let vehicle_type_condition = "";
  let current_tis = moment().tz(utils.timezone.default, false).unix();

  if(limit){
    limit_condition = `LIMIT ${limit} OFFSET ${offset}`
  }

  search_col_condition = utils.processTableColumnSearch(search_cols, [{
    db: 'lic_plate_no',
    client: 'lic_plate_no'
  }, {
    db: "operator->'name'",
    client: 'operator.name'
  }, {
    db: 'lname',
    client: 'lname'
  }]);

  sort_condition = utils.processTableColumnSort(sort, [{
    db: 'lic_plate_no',
    client: 'lic_plate_no'
  }, {
    db: "operator->'name'",
    client: 'operator.name'
  }, {
    db: 'lname',
    client: 'lname'
  },{
    db: 'spd',
    client: 'spd'
  }, {
    db: 'COALESCE(tis, 0)',
    client: 'tis'
  }],
  sort_condition);
  
  if(vehicle_status){
    vehicle_status_condition = ` AND status = '${vehicle_status}'`
  }

  if(q){
    table_search_condition = ` AND ( lower(lic_plate_no) like '%${q}%' OR lower(lname) like '%${q}%' OR lower((operator->'cname')::TEXT) like '%${q}%' )`
  }

  let filters = {
    onTrip: req.query.onTrip || null,
    place: req.query.place || null,
    doorStatus: req.query.doorStatus || '',
    deviceStatus: req.query.device_status || '',
    temperatureStatus: req.query.temperature_status || '',
    vehicle_type: req.query.vehicle_type
  };

  if(filters.vehicle_type){
    vehicle_type_condition = ` AND lower(atp.type) = '${filters.vehicle_type.toLowerCase()}'`
  }

  if(filters.deviceStatus.length > 0) {
    let t = [];
    let needs_attention = false;
    for (let i = 0; i < filters.deviceStatus.length; i++) {
      if(filters.deviceStatus[i] === 'needs_attention') needs_attention = true;
      t.push("'" + filters.deviceStatus[i] + "'");
    }
    if(needs_attention)
      dsfilterWhereClause = ` AND device_status->>'statuscode' IN (${t}) OR device_status is null) `;
    else
      dsfilterWhereClause = ` AND device_status->>'statuscode' IN (${t})`
  }

  let query = 
  `WITH my_vehicles as (
    SELECT DISTINCT a.id 
    FROM GROUPS g
    INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected
    INNER JOIN assets a ON a.id = agm.fk_asset_id AND a.active 
    INNER JOIN (
      SELECT DISTINCT a.id 
      FROM GROUPS g 
      INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected 
      INNER JOIN assets a ON a.id = agm.fk_asset_id AND a.active 
      WHERE g.fk_c_id = ${fk_c_id} AND g.is_admin AND agm.connected AND a.active
    ) ca ON ca.id = a.id 
    WHERE a.active AND g.active AND g.fk_c_id = ${fk_c_id} AND ${groupCondition}
  ),

  vehicle_trips as (
    SELECT tp.id, tp.fk_asset_id, t.last_trip_tis
    FROM (
      SELECT fk_asset_id, json_array_elements(trips) as trips, last_trip_tis 
      FROM asset_trip_maps atm where atm.fk_asset_id in (SELECT id FROM my_vehicles)
    ) t  
    INNER JOIN trip_plannings tp on tp.id = (t.trips ->> 'id'):: INT 
  )
      
  SELECT *,
    COUNT(*) OVER()::INT AS total_count
  FROM (
    SELECT 
      a.id, a.lic_plate_no, l.lname, l.lat, l.lon, l.ign, l.spd, l.osf, l.tis,l.heading, l.stationary_since,l.device_status,
      l.temperature_sensors, l.door_status, l.door_sensor,l.fuel_sensor,l.load_sensor,l.immobilizer_sensor,
      CASE 
        WHEN l.status IS NULL OR (l.tis > 0 AND ABS(${current_tis} - l.tis) > 604800) THEN 'disconnected'
        WHEN ABS(1552975909 - l.tis) BETWEEN 3600 AND 604800 THEN 'unknown'
        ELSE l.status
      END AS status,
      atp.type as vehicle_type,
      json_agg(json_build_object('id', g.id, 'name', g.name)) filter (where g.id is not null) as groups,
      json_build_object('id',c.id, 'name', c.cname) as operator,
      json_agg(json_build_object('id', tp.id, 'tid', tp.tid,'end_tis',tp.end_tis,'eta',tp.trip_log->'ETA',
                  'delayed_by', tp.trip_log->'delayed_by', 'delayed', tp.trip_log->'delayed')) FILTER (WHERE tp.id IS NOT NULL) as trips
    FROM assets a
    INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id ${vehicle_type_condition}
    LEFT JOIN current_locations l ON l.fk_asset_id = a.id
    LEFT JOIN companies c on c.id = a.fk_c_id
    LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id and tp.id in (SELECT id FROM vehicle_trips)
    LEFT JOIN asset_group_mappings agm on agm.fk_asset_id = a.id and agm.connected
    LEFT JOIN groups g on g.id = agm.fk_group_id and g.active and g.fk_c_id = ${fk_c_id}
    WHERE a.active and a.id in (SELECT id FROM my_vehicles)
    GROUP BY a.id, l.id, atp.id, c.id
  ) d 
  WHERE d.id > 0 
  ${search_col_condition + vehicle_status_condition + table_search_condition + dsfilterWhereClause}
  ${sort_condition + limit_condition}  `;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (vehicles) {
    let total_count = 0;
    
    vehicles.forEach(function(vehicle){
      total_count = vehicle.total_count;
      vehicle.lat = vehicle.lat ? parseFloat(vehicle.lat) : null;
      vehicle.lon = vehicle.lon ? parseFloat(vehicle.lon) : null;
      vehicle.groups = _.uniqBy(vehicle.groups, 'id');
      
      // Process trip information
      let inprogressTrips = [];
      let tp = _.uniqBy(vehicle.trips, 'id');
      tp.forEach(function (i) {
        if (i.id) {
          inprogressTrips.push({
            id: i.id,
            tid: i.tid,
            eta: {
              tis: i.end_tis + (i.delayed_by || 0),
              duration: Math.round(i.delayed_by / 60) || 0,
              delayed: i.delayed || false
            }
          })
        }
      });
      vehicle.trips = inprogressTrips;

      delete vehicle.total_count
    });

    if(output_format === 'xls'){
      const file_name = "VEHICLE-LIST-"+ fk_c_id +"-"+ moment().unix() + ".xls";
      const file_path = file_base_path + file_name;
      const table_name = `vehicle-list`;

      tableConfigsHelper.getTableConfig(table_name, fk_u_id, vehicle_status || 'all', default_columns, function (err, table_config) {
        xlsDownload.convertTableV1ToXLS(vehicle_status || 'all', vehicles, file_base_path, file_name, table_config, function (err) {
          if (err) {
            res.status(err.code).send(err.msg)
          } else {
            res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })
      })
    }else{
      return res.json({
        total_count: total_count,
        data: vehicles
      })
    }
  }).catch(function(err){
    console.error(err);
    return res.status(500).send()
  })
};

exports.getAssetActivity = function (req, res) {

  const id = req.query.id;
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const todayStartTime = moment().tz(utils.timezone.default, false).startOf('day').unix();
  const todayEndTime = moment().tz(utils.timezone.default, false).endOf('day').unix();
  let start_time = req.query.start_tis || todayStartTime;
  start_time = moment.unix(start_time).tz(utils.timezone.default, false);
  let end_time = req.query.end_tis || todayEndTime;
  end_time = moment.unix(end_time).tz(utils.timezone.default, false);
  const startOfDay = start_time.clone().startOf('day').unix();
  const endOfDay = end_time.clone().endOf('day').unix();
  const searchCondition = req.query.q ? ` AND to_json(tp.trip_log->'trip')::text ilike '%${req.query.q}%'` : "";
  const output_format = req.query.format;

  if (!id) {
    return res.status(400).send('Invalid asset ID');
  }

  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);

  let queryCondition = '';
  if (!utils.isInt(id)) {
    queryCondition = ` a.asset_id = '${id}'`
  } else {
    queryCondition = ' a.id = ' + id
  }

  let query = `SELECT a.lic_plate_no, tp.trip_log->'active' as active, tp.tid as id, tp.trip_log->'distance' as distance,
       tp.trip_log->'violations' as violations, tp.trip_log->'trip' as activity,
       json_agg(g.gid) as gids, tp.trip_log as log,
       sum((tp.trip_log->>'distance')::float) over() as total_distance,
       sum((tp.trip_log->>'time_taken')::int) over() as total_duration,
       count(*) OVER() AS total_count FROM groups g 
       LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected
       LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active 
       INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id 
       LEFT JOIN auto_trips tp on tp.fk_asset_id = a.id 
       WHERE g.active AND tp.custom = FALSE AND g.id in ${groupIds} AND ${queryCondition}
       AND ( to_json(tp.trip_log->'departure')::TEXT::INT BETWEEN ${startOfDay} AND ${endOfDay}
       OR to_json(tp.trip_log->'lkLocation'->'tis')::TEXT::INT BETWEEN ${startOfDay} AND ${endOfDay} )
         ${searchCondition}
       GROUP BY a.id, tp.id ORDER BY tp.tid DESC`;

  if(output_format !== 'xls'){
    query += ` LIMIT ${limit} OFFSET ${offset}`;
  }


  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (data) {
      if(output_format === 'xls'){
        try {

          console.log('Vehicle activity XLS');

          const lic_no = data.length > 0 ? data[0].lic_plate_no : 0;
          const file_name = "ASSET-ACTIVITY-" + lic_no + ".xls";
          const file_path = file_base_path + file_name;

          convertAssetActivityToXLS(data, file_name, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })
        }catch(e){
          console.error(e);
          LOGGER.warn(APP_LOGGER.formatMessage(req, e))
        }
      }else {

        async.auto({
          pois: function (cb) {
            let poi_ids = [];
            data.forEach(function (i) {
              poi_ids = _.concat(poi_ids, _.map(i.activity, 'Id'))
            });
            poi_ids = _.uniq(_.compact(poi_ids));
            if(_.size(poi_ids) > 0){
              let query = `SELECT p.id, pt.type, CASE WHEN p.priv_typ THEN 'company'
                                  ELSE 'general' END category
                                  FROM pois p 
                                  JOIN poi_types pt on pt.id = p.typ
                                  WHERE p.id in (`+ poi_ids +`)`;
              models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                .then(function (data) {
                  cb(null, data)
                }).catch(function () {
                  cb()
                })
            }else{
              cb()
            }
          },
          data: ['pois', function (results, cb) {
            console.log("results.pois ", results.pois);
            let totalAssets = 0;
            let totalDistance = 0;
            data.forEach(function (item) {
              totalAssets = Number(item.total_count);
              totalDistance = item.total_distance;

              item.distance = Number((item.distance / 1000).toFixed(1));
              item.violations = item.violations ? item.violations.length : 0;
              item.avgSpd = item.log.avg_speed || null;
              let s = item.activity[0];
              item.start = {
                tis: s.StartTis,
                lname: s.LName
              };
              if (!item.active) {
                // completed trip
                let e = item.activity[item.activity.length - 1];
                item.end = {
                  tis: e.StartTis,
                  lname: e.LName
                };
                item.duration = Math.round(moment.duration(item.log.time_taken, 'seconds').asMinutes());
                item.stops = item.log.stops
              } else {
                // ongoing trip
                item.end = {
                  tis: null,
                  lname: null
                };
                item.duration = Math.round(moment.duration((item.log.arrival - item.log.departure), 'seconds').asMinutes());
                item.stops = (item.log.trip.length - 1) || 0
              }

              // detail log
              let detailLog = [];
              let activityLength = item.active ? _.size(item.activity) : _.size(item.activity) - 1;
              let stopsCount = 1;
              _.forEach(item.activity, function (i, index) {

                let typeEvent = "planned";
                let labelText = "";

                if(index === 0){
                  labelText = "A"
                }else if(!item.active && index === activityLength){
                  labelText = "B"
                }else{
                  typeEvent = "unplanned";
                  labelText = stopsCount.toString();
                  stopsCount++
                }

                detailLog.push({
                  type:  typeEvent,
                  lname: i.LName,
                  arrivalTis: i.StartTis,
                  departureTis: i.EndTis,
                  dwellDuration: Math.round(moment.duration(i.DwellTime, 'seconds').asMinutes()),
                  label: labelText,
                  lat: i.Lat,
                  lon: i.Lon,
                  avgSpd: 0,
                  distance: 0,
                  poi: _.find(results.pois, {id: i.Id}) || null
                });

                if(index !== activityLength){
                  detailLog.push({
                    type: "intransit",
                    lname: "",
                    arrivalTis: (i.EndTis + 1),
                    departureTis: item.activity[index+1] ? (item.activity[index+1].StartTis - 1) : null,
                    dwellDuration: 0,
                    label: "",
                    lat: null,
                    lon: null,
                    avgSpd: i.AvgSpd,
                    distance: Number((i.Distance /1000).toFixed(1)),
                    poi: null
                  })
                }
              });

              item.log = detailLog;

              item.total_count = undefined;
              item.total_distance = undefined;
              item.total_duration = undefined;
              item.active = undefined;
              item.activity = undefined
              // item.log = undefined
            });
            cb(null, {
              summary: {
                total_distance: Number((totalDistance / 1000).toFixed(1)),
                total_duration: Math.round(moment.duration(totalDistance, 'seconds').asMinutes()),
                time_period: end_time.diff(start_time, 'minutes')
              },
              total_count: totalAssets,
              data: data
            });
          }]
        }, function (err, results) {
          res.json(results.data)
        })
      }
    }).catch(function (err) {
      LOGGER.error(req, err);
      return res.status(500).send(err);
    })
};

exports.getVehicleDetail = function(req, res) {

  let assetId = req.query.id;
  let fk_c_id = req.headers.fk_c_id;
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let groupCondition = ` g.id in ${groupIds}`;
  let current_tis = moment().tz(utils.timezone.default, false).unix();
  if(!utils.isInt(assetId)) {
    return res.status(400).send('Invalid asset ID');
  }

  let query = 
    `WITH my_vehicles as (
      SELECT DISTINCT a.id 
      FROM GROUPS g
      INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected
      INNER JOIN assets a ON a.id = agm.fk_asset_id AND a.active 
      INNER JOIN (
        SELECT DISTINCT a.id 
        FROM GROUPS g 
        INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected 
        INNER JOIN assets a ON a.id = agm.fk_asset_id AND a.active 
        WHERE g.fk_c_id = ${fk_c_id} AND g.is_admin AND agm.connected AND a.active
      ) ca ON ca.id = a.id 
      WHERE a.active AND g.active AND g.fk_c_id = ${fk_c_id} AND ${groupCondition} AND a.id = ${assetId}
    ),
  
    vehicle_trips as (
      SELECT tp.id, tp.fk_asset_id, t.last_trip_tis
      FROM (
        SELECT fk_asset_id, json_array_elements(trips) as trips, last_trip_tis 
        FROM asset_trip_maps atm where atm.fk_asset_id in (SELECT id FROM my_vehicles)
      ) t  
      INNER JOIN trip_plannings tp on tp.id = (t.trips ->> 'id'):: INT 
    )
        
    SELECT 
      a.id, a.lic_plate_no, l.lname, l.lat, l.lon, l.ign, l.spd, l.osf, l.tis,l.heading, l.stationary_since,l.device_status,
      l.temperature_sensors, l.door_status, l.door_sensor,l.fuel_sensor,l.load_sensor,l.immobilizer_sensor,
      CASE 
        WHEN l.status IS NULL OR (l.tis > 0 AND ABS(${current_tis} - l.tis) > 604800) THEN 'disconnected'
        WHEN ABS(1552975909 - l.tis) BETWEEN 3600 AND 604800 THEN 'unknown'
        ELSE l.status
      END AS status,
      atp.type as vehicle_type, t.status as cmd_status, t.fk_device_command_id, 
      array_agg(aa.sensor_settings->>'temperature_sensor') as threshold_temperature,
      json_agg(json_build_object('id', g.id, 'name', g.name)) filter (where g.id is not null) as groups,
      json_build_object('id',c.id, 'name', c.cname) as operator,
      json_agg(json_build_object('id', tp.id, 'tid', tp.tid,'end_tis',tp.end_tis,'eta',tp.trip_log->'ETA',
                  'delayed_by', tp.trip_log->'delayed_by', 'delayed', tp.trip_log->'delayed')) FILTER (WHERE tp.id IS NOT NULL) as trips
    FROM assets a
    INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id 
    LEFT JOIN current_locations l ON l.fk_asset_id = a.id
    LEFT JOIN companies c on c.id = a.fk_c_id
    LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id and tp.id in (SELECT id FROM vehicle_trips)
    LEFT JOIN asset_group_mappings agm on agm.fk_asset_id = a.id and agm.connected
    LEFT JOIN groups g on g.id = agm.fk_group_id and g.active and g.fk_c_id = ${fk_c_id}
    LEFT JOIN asset_attributes aa on a.id = aa.fk_asset_id
    LEFT JOIN (
      SELECT dcu.id, dcu.status, dcu.fk_asset_id, dcu.fk_device_command_id 
      FROM device_command_usages dcu 
      WHERE dcu.fk_asset_id = ${assetId}
      ORDER BY dcu.id DESC LIMIT 1
    ) t ON t.fk_asset_id = a.id
    WHERE a.active and a.id in (SELECT id FROM my_vehicles)
    GROUP BY a.id, l.id, atp.id, c.id, t.status, t.fk_device_command_id
    `;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (asset) {
      try{
        if(asset){
          if(asset.threshold_temperature && asset.threshold_temperature.length > 0){
            let d = asset.threshold_temperature[0];
            if(typeof d !== 'object'){
              try{
                d = JSON.parse(d)
              }catch(e){
                console.error(e)
                d = null;
                console.log("invalid temperature thresgold saved in database")
              }
            }
            asset.threshold_temperature = d
          }else{
            asset.threshold_temperature = null;
          }

          asset.groups = _.uniqBy(asset.groups, 'id');
          asset.lul = asset.door_sensor && asset.door_sensor.length > 0 ? asset.door_sensor[0].value : "N/A";

          let inprogressTrips = [];
          let tp = _.uniqBy(asset.trips, 'id');
          tp.forEach(function (i) {
            if (i.id) {
              inprogressTrips.push({
                id: i.id,
                tid: i.tid,
                eta: {
                  tis: i.end_tis + (i.delayed_by || 0),
                  duration: Math.round(i.delayed_by / 60) || 0,
                  delayed: i.delayed || false
                }
              })
            }
          });
          asset.trips = inprogressTrips;

          // Process immobiliser sensor
          if (asset.immobilizer_sensor) {
            asset.has_immobiliser_sensor = false;
            asset.immobiliser_cmd_status = null;
            asset.mobiliser_cmd_status = null;
            asset.immobilizer_enabled = false;
            asset.immobiliser_tis = null;
            asset.immobiliser_violation = false;

            if (asset.immobilizer_sensor) {
              if (asset.immobilizer_sensor.id > 0) {
                asset.has_immobiliser_sensor = true
              }
              asset.immobilizer_enabled = asset.immobilizer_sensor.immobilizer_enabled || false;
              asset.immobiliser_tis = asset.immobilizer_sensor.tis;
              asset.immobiliser_violation = asset.immobilizer_sensor.immobilizer_violation
            }
            if (asset.fk_device_command_id === 1) {
              asset.immobiliser_cmd_status = asset.cmd_status
            } else if (asset.fk_device_command_id === 2) {
              asset.mobiliser_cmd_status = asset.cmd_status
            }
          }

          // Process temperature sensor
          let temperature_sensor = asset.temperature_sensor;
          if(temperature_sensor !== "undefined" && temperature_sensor && temperature_sensor.id){
            asset.temperature_sensor = temperature_sensor.temperature_value;
            asset.temperature_sensor.status =  (asset.threshold_temperature && (asset.temperature_sensor.temperature > asset.threshold_temperature.max_temperature || asset.temperature_sensor.temperature < asset.threshold_temperature.min_temperature)) ? "Violation" : "Good";
            asset.temperature_sensor.tis = temperature_sensor.tis
          } 
          else {
            asset.temperature_sensor = null
          }

          async.parallel({
            fuel: function(cb1){
            // Process fuel sensor
              if(asset.fuel_sensor){
                let status = (asset.fuel_sensor.id > 0) ? "Installed" : "No Sensor";

                nuDynamoDb.getAssetFuelValue({
                  fk_asset_id: asset.id,
                  start_tis: moment().tz(utils.timezone.default, false).subtract(15, "minutes").unix(),
                  end_tis: moment().tz(utils.timezone.default, false).unix()
                }, function (err, data) {
                  if(data.length > 0) {
                    let avg_val = calculateAvg(data);
                    asset.fuel_sensor = {
                      level_percentage:Number((((avg_val * 0.0055) / 5 ) * 100).toFixed(2)),
                      level_liters:0,
                      status: status
                    };
                    cb1()
                  } else {
                    asset.fuel_sensor = {
                      level_percentage:0,
                      level_liters:0,
                      status: status
                    };
                    cb1()
                  }
                })
              } else{
                asset.fuel_sensor = {
                  level_percentage:0,
                  level_liters:0,
                  status: 'No Sensor'
                };
                cb1()
              }
            },
            drivers: function (cb) {
              let query =
            ` SELECT u.id, u.firstname, u.lastname FROM assets a
              INNER JOIN asset_driver_mappings adm on adm.fk_asset_id = a.id
              INNER JOIN users u on u.id = adm.fk_driver_id AND adm.connected
              WHERE a.id = ${assetId}`; 

              models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                .then(function (data) {
                  cb(null, data)
                }).catch(function (err) {
                  console.error(err);
                  cb(null, [])
                })
            }
          }, function (err, results) {
            asset.drivers = results.drivers;
            res.json(asset)
          })
        }else{
          res.status(404).send()
        }
      } catch (e){
        console.error(e)
      }
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send(err)
    });
};

exports.getVehicleDetailStats = function (req, res) {
 
  const id = req.query.id;
  const avg_start_time = moment().tz(utils.timezone.default, false).subtract(30, 'days').startOf('day').unix();

  const todayStartTime = moment().tz(utils.timezone.default, false).startOf('day').unix();
  const todayEndTime = moment().tz(utils.timezone.default, false).endOf('day').unix();
  let start_time = req.query.start_time || todayStartTime;
  start_time = moment.unix(start_time).tz(utils.timezone.default, false);
  let end_time = req.query.end_time || todayEndTime;
  end_time = moment.unix(end_time).tz(utils.timezone.default, false);
  const startOfDay = start_time.clone().startOf('day').unix();
  const endOfDay = end_time.clone().endOf('day').unix();

  const today = todayStartTime === start_time.clone().unix() && todayEndTime === end_time.clone().unix();

  if (!id) {
    return res.status(400).send('Invalid asset ID');
  }

  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);

  let queryCondition = '';
  if (!utils.isInt(id)) {
    queryCondition = " a.asset_id = '" + id + "'"
  } else {
    queryCondition = ' a.id = ' + id
  }

  let validateAssetQuery = "SELECT a.id, a.asset_id FROM groups g "+
      " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
      " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
      " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id"+
      " WHERE g.active AND g.id in "+ groupIds +" AND " + queryCondition +
      " LIMIT 1";

  models.sequelize.query(validateAssetQuery, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (asset) {
      if (!asset) {
        return res.status(404).send('Not Found')
      } else {
        let assetId = asset.asset_id;
        async.parallel({
          rangeData: function (mainCb) {
            if (today) {
              nuRedis.getAssetStats(assetId, function (err, stats) {
                return mainCb(err, stats)
              })
            } else {
              async.parallel({
                firstDayStats: function (callback) {
                  let firstDayStartTime = start_time.clone().unix();
                  let firstDayEnd = start_time.clone().endOf('day').unix();
                  //That day's stats then compare day end time and then set default(user sends) or day end timestamp
                  let firstDayEndTime = end_time.clone().unix() > firstDayEnd ? firstDayEnd : end_time.clone().unix();

                  //If requested time range is same as start of day and end of day then don't calculate as it is already calculated
                  if (startOfDay === start_time.clone().unix() && firstDayEnd === firstDayEndTime) {
                    return callback()
                  }

                  calculateStatsFromLocationTable(groupIds, asset.id, firstDayStartTime, firstDayEndTime, callback)
                },
                lastDayStats: function (callback) {
                  let lastDayEndTime = end_time.clone().unix();
                  let lastDayStart = end_time.clone().startOf('day').unix();
                  //Start is greater than day's start then set it as start time
                  let lastDayStartTime = start_time.clone().unix() < lastDayStart ? lastDayStart : start_time.clone().unix();

                  //checking if tis is in same as start day range
                  let ifSameDayRange = start_time.clone().unix() >= lastDayStart && end_time.clone().unix() <= lastDayEndTime;

                  //If requested time range is same as start of day and end of day then dont calculate as it is already calculated
                  //Or If stats are for todays range then firstDayStats will calculate
                  if ((endOfDay === end_time.clone().unix() && lastDayStart === lastDayStartTime) || ifSameDayRange) {
                    return callback()
                  }

                  calculateStatsFromLocationTable(groupIds, asset.id, lastDayStartTime, lastDayEndTime, callback)
                },
                inBetweenData: function (callback) {
                  let rangeStartTime = start_time.clone().unix();
                  let rangeEndTime = end_time.clone().unix();

                  let query = "SELECT COALESCE(SUM(tot_dist), 0) as \"totDist\", COALESCE(SUM(tot_idle),0) as \"totIdleTime\", COALESCE(CAST(AVG(CASE WHEN ast.avg_spd > 0 THEN ast.avg_spd END) as int),0) as \"avgSpd\"," +
                                          " COALESCE(SUM(tot_Eng), 0) as \"totEngTime\", COALESCE(SUM(tot_stat),0) as \"totStatTime\", COALESCE(CAST(SUM(tot_vio) as int),0) as \"totSpdVio\"" +
                                          " FROM asset_stats ast " +
                                          " WHERE ast.fk_asset_id = "+ asset.id +" AND ast.tis >= "+ rangeStartTime + " AND ast.e_tis <= " + rangeEndTime;

                  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                    .spread(function (totData) {
                      _.forIn(totData, function (value, key) {
                        if (key === 'totDist') {
                          totData[key] = Number(totData[key].toFixed(1));
                        } else {
                          totData[key] = Math.round(totData[key]);
                        }
                      });
                      callback(null, totData);
                    }).catch(function (err) {
                      callback(err);
                    })
                },
              }, function (err, result) {

                if (err) {
                  return mainCb(err)
                }

                //Sum all stats
                let defaultValue = {
                  "totDist": 0,
                  "totIdleTime": 0,
                  "totEngTime": 0,
                  "totStatTime": 0,
                  "totSpdVio": 0
                };

                let firstDay = result.firstDayStats || defaultValue;
                let lastDay = result.lastDayStats || defaultValue;
                let betweenRange = result.inBetweenData;

                mainCb(null, {
                  "totDist": Number((firstDay.totDist + lastDay.totDist + betweenRange.totDist).toFixed(1)),
                  "totIdleTime": Math.round(firstDay.totIdleTime + lastDay.totIdleTime + betweenRange.totIdleTime),
                  "totEngTime": Math.round(firstDay.totEngTime + lastDay.totEngTime + betweenRange.totEngTime),
                  "totStatTime": Math.round(firstDay.totStatTime + lastDay.totStatTime + betweenRange.totStatTime),
                  "totSpdVio": Math.round(firstDay.totSpdVio + lastDay.totSpdVio + betweenRange.totSpdVio),
                  "avgSpd": betweenRange.avgSpd
                })
              })
            }
          },
          //Avg Data
          perMonth: function (cb) {
            let query = "SELECT CAST(COUNT(*) as int) as days, COALESCE(SUM(tot_dist), 0) as \"avgDist\", COALESCE(SUM(tot_idle),0) as \"avgIdleTime\", " +
                              " COALESCE(SUM(tot_Eng), 0) as \"avgEngTime\", COALESCE(SUM(tot_stat),0) as \"avgStatTime\", COALESCE(CAST(SUM(tot_vio) as int),0) as \"avgSpdVio\"" +
                              " FROM asset_stats ast " +
                              " WHERE ast.tis >= "+ avg_start_time ;

            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (totData) {
                let days = Number(totData.days);
                let avgData = {};
                delete totData.days;

                _.forIn(totData, function (value, key) {
                  avgData[key] = (value / days) || 0;
                  if (key === 'avgDist') {
                    avgData[key] = Number(avgData[key].toFixed(1));
                  } else {
                    avgData[key] = Math.round(avgData[key]);
                  }
                });
                cb(null, avgData);
              }).catch(function (err) {
                cb(err);
              })
          }
        },
        function (err, result) {
          if (err) {
            LOGGER.error(err);
            return res.status(500).send('Internal server error');
          }
          let finalResult = _.merge(result.rangeData, result.perMonth);
          res.json(finalResult);
        });
      }
    }).catch(function (err) {
      LOGGER.error(err);
      return res.status(500).send('Internal server error')
    })
};

exports.getAllVehicleStats = function (req, res, assetType) {
  const emailId = req.headers.email;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const type = assetType;
  const todayStartTime = moment().tz(utils.timezone.default, false).startOf('day').unix();
  const todayEndTime = moment().tz(utils.timezone.default, false).endOf('day').unix();
  let start_time = req.query.start_time || todayStartTime;
  start_time = moment.unix(start_time).tz(utils.timezone.default, false);
  let end_time = req.query.end_time || todayEndTime;
  end_time = moment.unix(end_time).tz(utils.timezone.default, false);

  const today = todayStartTime === start_time.clone().unix() && todayEndTime === end_time.clone().unix();

  // stats
  async.auto({
    days: ['count', function (result, cb) {
      if(today) {
        if (gid) {
          nuRedis.getAssetsStats(gid, type, cb)
        } else {
          nuRedis.getAssetsStats(emailId, type, cb)
        }
      } else {
        let rangeStartTime = start_time.clone().unix();
        let rangeEndTime = end_time.clone().unix();
        async.parallel({
          stats : function calculateStatsForRange(callback) {

            let query = " SELECT COALESCE(SUM(tot_dist), 0) as \"Dist\", COALESCE(SUM(tot_idle),0) as \"IdleTime\",  COALESCE(SUM(tot_Eng), 0) as \"EngTime\", "+
                          " COALESCE(SUM(tot_stat),0) as \"StatTime\", COALESCE(SUM(tot_osf), 0) as \"Overspeeding\", COALESCE(SUM(tot_hbk), 0) as \"Harshbraking\" "+
                              " FROM groups g "+
                              " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
                              " INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
                              " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = '"+ assetType +"'"+
                              " LEFT JOIN asset_stats ast on ast.fk_asset_id = a.id" +
                              " WHERE g.active AND "+ groupCondition +" AND ast.tis >= "+ rangeStartTime + " AND ast.tis <= " + rangeEndTime ;

            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (totData) {
                let count = result.count;
                _.forIn(totData, function (value, key) {
                  if (key === 'Dist') {
                    totData['tot' + key] = Number(value.toFixed(1));
                    totData['avg' + key + 'PerAsset'] = Number((value / count).toFixed(1)) || 0;
                  } else {
                    totData['tot' + key] = Math.round(value);
                    totData['avg' + key + 'PerAsset'] = Math.round(value / count) || 0;
                  }
                  delete totData[key];
                });
                callback(null, totData);
              }).catch(function (err) {
                callback(err);
              })
          },
          avgSpd: function (cb) {

            let query = " SELECT COALESCE(CAST(AVG(CASE WHEN ast.avg_spd > 0 THEN ast.avg.spd END) as int),0) as spd"+
                              " FROM groups g "+
                              " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
                              " INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
                              " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = '"+ assetType +"'"+
                              " LEFT JOIN asset_stats ast on ast.fk_asset_id = a.id" +
                              " WHERE g.active AND "+ groupCondition +" AND ast.tis >= "+ rangeStartTime + " AND ast.tis <= " + rangeEndTime ;

            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (asset) {
                cb(null, Math.round(asset.spd) || 0);
              }).catch(function (err) {
                cb(err);
              })
          },
          fleetUtil: function (cb) {
            let query = " SELECT COALESCE(COUNT(DISTINCT(ast.fk_asset_id)),0) as tot_count "+
                              " FROM groups g "+
                              " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
                              " INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
                              " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = '"+ assetType +"'"+
                              " LEFT JOIN asset_stats ast on ast.fk_asset_id = a.id" +
                              " WHERE g.active AND "+ groupCondition +" AND ast.tis >= "+ rangeStartTime + " AND ast.tis <= " + rangeEndTime +" AND ast.tot_dist >= 15";

            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (assetsUtilized) {
                cb(null, Math.round((assetsUtilized.tot_count * 100) / result.count) || 0);
              }).catch(function (err) {
                cb(err);
              })
          }
        }, function(err, statsResult) {
          if(err) {
            return cb(err)
          }
          statsResult.stats.avgSpd = statsResult.avgSpd;
          statsResult.stats.fleetUtil = statsResult.fleetUtil;
          cb(null, statsResult.stats)
        })
      }
    }],
    count: function (cb) {

      let query = "SELECT COALESCE(count(DISTINCT a.id)::int, 0) as count" +
                  " FROM groups g "+
                  " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
                  " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
                  " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = '"+ assetType +"'"+
                  " WHERE g.active AND "+ groupCondition;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (assets) {
          cb(null, assets.count || 0);
        }).catch(function (err) {
          cb(err);
        })
    }
  }, function (err, result) {
    if (err) {
      LOGGER.error(err);
      return res.status(500).send('Internal server error ')
    } else {
      if(result.count > 0){
        let finalResult = result.days;
        res.json(finalResult)
      }else{
        return res.status(404).send('Not found')
      }
    }
  });
};

exports.getAssetActivityTrail = function (req, res) {

  const id = req.query.id;
  if (!Number(id)) {
    return res.status(400).send('Invalid asset ID');
  }
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);

  async.auto({
    auto_trip: function (cb) {
      let query = "SELECT tp.trip_log, json_build_object('licNo', a.lic_plate_no," +
              " 'id', a.id, 'type', atp.type) as asset FROM groups g "+
              " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
              " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
              " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id " +
              " LEFT JOIN auto_trips tp on tp.fk_asset_id = a.id "+
              " WHERE g.active AND g.id in "+ groupIds + " AND tp.tid = '"+ id +"' LIMIT 1";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (data) {
          if(data){
            let asset = data.asset;
            let trip_status = "completed";
            let tl = data.trip_log;
            let d = {
              fk_asset_id: tl.fk_asset_id,
              start_tis: tl.departure,
              end_tis: tl.arrival
            };
            let lkl = tl.lkLocation;
            let status = lkl.ign === 'B' ? "moving" : "stationary";
            let last_known_location = {
              ign: lkl.ign,
              lat: lkl.lat,
              lname: lkl.lname,
              lon: lkl.lon,
              osf: lkl.osf,
              spd: lkl.spd,
              status: status,
              tis: lkl.tis
            };
            asset = _.merge(asset, last_known_location);

            let waypoints = [];
            if(tl.active){
              trip_status = "intransit";
              last_known_location.duration = 0;
              waypoints.push(last_known_location)
            }

            tl.trip.length && tl.trip.forEach(function (i) {
              waypoints.push({
                ign: 'A',
                lat: i.Lat,
                lname: i.LName,
                lon: i.Lon,
                osf: false,
                spd: 0,
                status: "stationary",
                tis: i.StartTis,
                duration: Math.round(i.DwellTime / 60)
              })
            });

            tl.violations && tl.violations.forEach(function (i) {
              waypoints.push({
                ign: i.ign,
                lat: i.lat,
                lname: i.lname,
                lon: i.lon,
                osf: i.type === 0 ,
                spd: i.spd,
                status: i.type === 0 ? "overspeeding" : "hardbraking",
                tis: i.tis,
                duration: 0
              })
            });
            cb(null,{
              d: d,
              asset: asset,
              waypoints: _.sortBy(waypoints, 'tis'),
              status: trip_status
            })
          }else{
            cb({
              code: 404,
              msg: 'Not Found'
            })
          }
        }).catch(function (err) {
          cb({
            code: 500,
            msg: 'Internal server error',
            err: err
          })
        })
    }
    ,
    points: ['auto_trip', function (result, cb) {
      let trip = result.auto_trip.d;
      let asset = result.auto_trip.asset;

      nuDynamoDb.getAssetLocationFromAPI({
        fk_asset_id: asset.id,
        start_tis: trip.start_tis,
        end_tis: trip.end_tis
      }, function (err, data) {
        cb(null, data || [])
      })
    }]
  }, function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }else{
        res.status(500).send()
      }
    }else{
      let points = result.points;
      result.auto_trip.waypoints.forEach(function (i) {
        points.push(i)
      });

      points = _.orderBy(points, ['tis'],['asc']);

      for(let i = 0, len = points.length - 1; i <= len; i++){
        if(i > 0 && i < len){
          points[i].heading = Math.round(geolib.getBearing(points[i], points[i+1]));
        }else{
          points[i].heading = 0
        }
      }

      res.json({
        status: result.auto_trip.status,
        asset: result.auto_trip.asset,
        waypoints: result.auto_trip.waypoints,
        points: points
      })
    }
  })
};

exports.forwardRequestAssetGPSData = function (req, res) {
  // if format is json then download should be false or remove download key
  const format = req.query.format === 'xls' ? 'excel' : 'json';

  let params = _.merge({
    vehicle_no: req.query.id,
    report_type: 25,
    scheduled: false,
    download: true,
    output_type: format
  }, req.query);

  if(format === 'json'){
    params.download = undefined
  }
  delete params.id;
  delete params.format;

  let request_options = {
    uri: config.server_address.reports_server() + '/generate_report',
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      fk_c_id: req.headers.fk_c_id,
      fk_u_id: req.headers.fk_u_id,
      group_ids: req.headers.group_ids
    },
    json: true,
    qs: params
  };
  try {
    request(request_options).pipe(res)
  }catch(e){
    console.log("ERROR connecting to Reports Engine : ", e)
  }
};

exports.getAllVehiclesRealtimeStats = function (req, res, assetType) {

  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  let query = "SELECT json_agg(g.gid) as gids, a.asset_id as aid, atp.type, 'A' as ign, 0 as spd" +
      " FROM groups g "+
      " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
      " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
      " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = '"+ assetType +"'"+
      " WHERE g.active AND "+ groupCondition +
      " GROUP BY asset_id, a.name, a.lic_plate_no, atp.type";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (assets) {

    async.forEachOf(assets, function (asset, i, callback) {
      nuRedis.getAssetDetails(asset.aid, function (err, value) {
        if(value){
          asset.ign = value.ign;
          asset.spd = value.spd
        }
        callback()
      })
    }, function () {
      res.json(assets)
    });

  }).catch(function (err) {
    LOGGER.error(err);
    res.status(500).end("Internal Server Error");
  });
};

exports.getAssetActivityStats = function (req, res, assetType) {

  const id = req.query.id;
  const todayStartTime = moment().tz(utils.timezone.default, false).startOf('day').unix();
  const todayEndTime = moment().tz(utils.timezone.default, false).endOf('day').unix();
  let start_time = req.query.start_tis || todayStartTime;
  start_time = moment.unix(start_time).tz(utils.timezone.default, false);
  let end_time = req.query.end_tis || todayEndTime;
  end_time = moment.unix(end_time).tz(utils.timezone.default, false);
  const startOfDay = start_time.clone().startOf('day').unix();
  const endOfDay = end_time.clone().endOf('day').unix();
  const searchCondition = req.query.q ? " AND to_json(tp.trip_log->'trip')::text ilike '%"+ req.query.q +"%'" : "";

  if (!id) {
    return res.status(400).send('Invalid asset ID');
  }

  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);

  let queryCondition = '';
  if (!utils.isInt(id)) {
    queryCondition = " a.asset_id = '" + id + "'"
  } else {
    queryCondition = ' a.id = ' + id
  }

  let query = " SELECT COALESCE(ROUND(SUM(distance)), 0) as distance, COALESCE(ROUND(SUM(duration)), 0) duration " +
              " FROM " +
                  "(" +
                      " SELECT tp.tid as id, ((tp.trip_log->>'distance')::double precision/1000) as distance," +
                      " CASE WHEN (tp.trip_log->>'active')::BOOLEAN THEN ((tp.trip_log->>'arrival')::BIGINT - (tp.trip_log->>'departure')::BIGINT)/60" +
                      "    ELSE ((tp.trip_log->>'time_taken')::REAL/60)" +
                      " END" +
                      " as duration " +
              " FROM groups g "+
              " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
              " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
              " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = '"+ assetType +"'" +
              " LEFT JOIN auto_trips tp on tp.fk_asset_id = a.id "+
              " WHERE g.active AND tp.custom = FALSE AND g.id in "+ groupIds +" AND " + queryCondition +
              " AND to_json(tp.trip_log->'departure')::TEXT::INT >= "+ startOfDay + " AND to_json(tp.trip_log->'departure')::TEXT::INT <= " + endOfDay + " " + searchCondition +
              " GROUP BY a.id, tp.id) x ";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (data) {
      res.json(data);
    }).catch(function (err) {
      console.log(err);
      return res.status(500).send();
    })
};

exports.getAssetDocumentRecords = function (req, res) {

  const fk_asset_id = req.query.asset_id;
  const searchKeyword = req.query.q || '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  if(!fk_asset_id){
    return res.status(400).send("enter asset id")
  }else{

    let query = "SELECT *, count(*) OVER()::INT AS total_count FROM ( " +
              " SELECT ad.id, ad.type_code, ad.document_no, adt.name as type, ad.issue_tis, ad.expiry_tis, ad.files_meta,"+
              " json_build_object('id', u.id, 'fullname', u.firstname || ' ' || u.lastname, 'firstname', u.firstname, 'lastname', u.lastname," +
              " 'tis', EXTRACT( EPOCH FROM ad.\"createdAt\")::INT) as \"createdBy\""+
              " FROM groups g "+
              " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
              " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
              " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id " +
              " INNER JOIN asset_documents ad on ad.fk_asset_id = a.id AND ad.current_record AND ad.active "+
              " LEFT JOIN asset_document_types adt on adt.code = ad.type_code "+
              " LEFT JOIN users u on u.id = ad.fk_u_id"+
              " WHERE g.active AND "+ groupCondition + " AND a.id = "+ fk_asset_id + " AND adt.name ilike '%"+ searchKeyword +"%'"+
              " GROUP BY a.id, ad.id, adt.id, u.id ) d"+
          " LIMIT " + limit + " OFFSET " + offset;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (records) {

      let total_records = 0;

      records.forEach(function (record) {
        total_records = record.total_count;

        let today = moment();
        let scheduled = moment.unix(record.expiry_tis);
        let due_difference = scheduled.diff(today, 'days');
        let status_message = "valid";

        if(due_difference >= 0 && due_difference < 30){
          status_message = "due soon"
        }else if(due_difference < 0){
          status_message = "expired"
        }

        record.status = status_message;

        record.files_meta = _.map(record.files_meta, function (n) {
          return _.pick(n, ['originalname','key'])
        });

        record.total_count = undefined
      });

      res.json({
        total_count:total_records,
        data: records
      })
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send()
    })
  }

};

function my_vehicles_query(params){
  let single_vehicle_condition = parseInt(params.vehicle_id) ? ` AND a.id = ${params.vehicle_id}` : '';
  let vehicle_type_condition = params.vehicle_type ? ` AND lower(atp.type) = '${params.vehicle_type}'` : '';

  return `
  SELECT DISTINCT a.id 
  FROM GROUPS g
  INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected
  INNER JOIN assets a ON a.id = agm.fk_asset_id AND a.active 
  INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id ${vehicle_type_condition}
  INNER JOIN (
    SELECT DISTINCT a.id 
    FROM GROUPS g 
    INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected 
    INNER JOIN assets a ON a.id = agm.fk_asset_id AND a.active 
    WHERE g.fk_c_id = ${params.fk_c_id} AND g.is_admin AND agm.connected AND a.active
  ) ca ON ca.id = a.id 
  WHERE a.active AND g.active AND g.fk_c_id = ${params.fk_c_id} AND ${params.groupCondition}
  ${single_vehicle_condition + vehicle_type_condition} 
  `
}

function distanceTrends(params, callback){
  try{
    async.auto({
      previous: function(cb){
        let query =
      `
      WITH my_vehicles as (
        ${my_vehicles_query(params)}
      )
      
      SELECT tis, sum(tot_dist) distance
      FROM asset_stats ast
      WHERE ast.fk_asset_id IN (SELECT id FROM my_vehicles) AND ast.tis BETWEEN ${params.start_tis} AND ${params.end_tis}
      GROUP BY tis
      ORDER BY tis;      
      `;
        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .then(function (data) {
            cb(null, data)
          })
          .catch(function (err) {
            cb(err)
          })
      },
      today: function(cb) {
      
        if(params.today_start_tis < params.end_tis){
          let query =
          `
          WITH my_vehicles as (
            ${my_vehicles_query(params)}
          )

          SELECT start_tis as tis, sum(tot_dist) distance
          FROM assets_reports_data ard
          WHERE ard.fk_asset_id IN (SELECT id FROM my_vehicles) AND ard.start_tis BETWEEN ${params.today_start_tis} AND ${params.today_end_tis}
          GROUP BY start_tis;
          `;
          models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
            .then(function (data) {
              cb(null, data)
            })
            .catch(function (err) {
              cb(err)
            })
        }else{
          cb(null, [])
        }
      }
    }, function(err, results) {
      if(err){
        callback(err)
      }else{
        let data = _.concat(results.previous, results.today);
        data.forEach(function(i){
          i.date = moment.unix(i.tis).format('D MMM YYYY');
          i.value = Math.round(i.distance * 1000);

          delete i.distance
        });
        data = _.sortBy(utils.addNullDatesToTrendsStats({start_tis: params.start_tis, end_tis: params.end_tis},data), ['tis']);
        callback(null, {[params.type]: data})
      }
    })
  }catch(e){
    console.error(e)
  }
}

function speedTrends(params, callback){
  try{
    async.auto({
      previous: function(cb){
        let query =
      `
      WITH my_vehicles as (
        ${my_vehicles_query(params)}
      )
      
      SELECT tis, avg(CASE WHEN avg_spd > 0 THEN avg_spd END)::INT speed
      FROM asset_stats ast
      WHERE ast.fk_asset_id IN (SELECT id FROM my_vehicles) AND ast.tis BETWEEN ${params.start_tis} AND ${params.end_tis}
      GROUP BY tis
      ORDER BY tis;      
      `;
        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .then(function (data) {
            cb(null, data)
          })
          .catch(function (err) {
            cb(err)
          })
      },
      today: function(cb) {
      
        if(params.today_start_tis < params.end_tis){
          let query =
          `
          WITH my_vehicles as (
            ${my_vehicles_query(params)}
          )

          SELECT start_tis as tis, avg(CASE WHEN avg_spd > 0 THEN avg_spd END)::INT speed
          FROM assets_reports_data ard
          WHERE ard.fk_asset_id IN (SELECT id FROM my_vehicles) AND ard.start_tis BETWEEN ${params.today_start_tis} AND ${params.today_end_tis}
          GROUP BY start_tis;
          `;
          models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
            .then(function (data) {
              cb(null, data)
            })
            .catch(function (err) {
              cb(err)
            })
        }else{
          cb(null, [])
        }
      }
    }, function(err, results) {
      if(err){
        callback(err)
      }else{
        let data = _.concat(results.previous, results.today);
        data.forEach(function(i){
          i.date = moment.unix(i.tis).format('D MMM YYYY');
          i.value = (i.speed);

          delete i.speed
        });
        data = _.sortBy(utils.addNullDatesToTrendsStats({start_tis: params.start_tis, end_tis: params.end_tis},data), ['tis']);
        callback(null, {[params.type]: data})
      }
    })
  }catch(e){
    console.error(e)
  }
}

function violationCount(params, callback){
  let query =
  `
  WITH my_vehicles as (
    ${my_vehicles_query(params)}
  )

  SELECT COALESCE(sum(ard.tot_osf)::INT,0) overspeeding, COALESCE(sum(ard.tot_hbk)::INT,0) harshbraking
  FROM assets_reports_data ard
  WHERE ard.fk_asset_id IN (SELECT id FROM my_vehicles) AND ard.start_tis BETWEEN ${params.today_start_tis} AND ${params.today_end_tis}
  ;
  `;
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (data) {
      callback(null, {[params.type]: data})
    })
    .catch(function (err) {
      callback(err)
    })
}


function getVehicleUtilisation(params, callback) {
  try{
    async.auto({
      vehicle_count: function(cb){
        let query =`${my_vehicles_query(params)}`;
        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .then(function (data) {
            cb(null, data.length)
          })
          .catch(function (err) {
            cb(err)
          })
      },
      previous: function(cb){
        let query =
      `
      WITH my_vehicles as (
        ${my_vehicles_query(params)}
      )
      
      SELECT tis, COUNT(CASE WHEN tot_dist >= 50 THEN 1 END)::INT
      FROM asset_stats ast
      WHERE ast.fk_asset_id IN (SELECT id FROM my_vehicles) AND ast.tis BETWEEN ${params.start_tis} AND ${params.end_tis}
      GROUP BY tis
      ORDER BY tis;      
      `;
        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .then(function (data) {
            cb(null, data)
          })
          .catch(function (err) {
            cb(err)
          })
      },
      today: function(cb) {
      
        if(params.today_start_tis < params.end_tis){
          let query =
          `
          WITH my_vehicles as (
            ${my_vehicles_query(params)}
          )

          SELECT tis, COUNT(CASE WHEN dist >= 50 THEN 1 END)::INT FROM (
            SELECT start_tis as tis, SUM(tot_dist)::INT dist
            FROM assets_reports_data ard
            WHERE ard.fk_asset_id IN (SELECT id FROM my_vehicles) AND ard.start_tis BETWEEN ${params.today_start_tis} AND ${params.today_end_tis}
            GROUP BY start_tis
          ) d
          GROUP BY d.tis;
          `;
          models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
            .then(function (data) {
              cb(null, data)
            })
            .catch(function (err) {
              cb(err)
            })
        }else{
          cb(null, [])
        }
      }
    }, function(err, results) {
      if(err){
        callback(err)
      }else{
        let data = _.concat(results.previous, results.today);
        let total_vehicles = results.vehicle_count;
        data.forEach(function(i){
          i.date = moment.unix(i.tis).format('D MMM YYYY');
          i.value = Math.round((i.count / total_vehicles) * 100);

          delete i.count
        });
        data = _.sortBy(utils.addNullDatesToTrendsStats({start_tis: params.start_tis, end_tis: params.end_tis},data), ['tis']);
        callback(null, {[params.type]: data})
      }
    })
  }catch(e){
    console.error(e)
  }
}

function violationByTime(params, callback){
  
  let query = 
    `
    WITH my_vehicles AS (${my_vehicles_query(params)})

    SELECT (SUM(tot_osf)::INT + SUM(tot_hbk)::INT) as value, start_tis as tis
    FROM assets_reports_data
    WHERE fk_asset_id IN (SELECT id FROM my_vehicles)
    AND start_tis >= $start_tis AND end_tis <= $end_tis
    GROUP BY 2
    ORDER BY start_tis DESC
    `;
  models.sequelize.query(query, {
    bind:{
      start_tis: params.start_tis,
      end_tis: params.end_tis
    },
    type: models.sequelize.QueryTypes.SELECT}).then(function(data){

    data.forEach(function(i) {
      i.day = moment.unix(i.tis).tz(utils.timezone.default, false).format("ddd");
      i.time = moment.unix(i.tis).tz(utils.timezone.default, false).format("h a")
    });

    callback(null, {[params.type]: data})
  }).catch(function (err) {
    console.error(err);
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}


exports.getVehicleStats = function(req, res){

  let vehicle_id = req.query.id;
  let type = req.query.type;
  let vehicle_type = req.query.vehicle_type;
  let fk_c_id = req.headers.fk_c_id;
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let gid = req.query.gid;
  let groupCondition = gid ? ` g.gid = '${gid}'` : ` g.id in ${groupIds}`;
  let today_start_tis = moment().tz(utils.timezone.default, false).startOf('day').unix();
  let today_end_tis = moment().tz(utils.timezone.default, false).endOf('day').unix();
  let start_tis = req.query.start_tis || moment().tz(utils.timezone.default, false).subtract(7, 'days').startOf('day').unix();
  let end_tis = req.query.end_tis || today_end_tis;

  let params = { vehicle_id, type, vehicle_type, fk_c_id, groupCondition, start_tis, end_tis, today_start_tis, today_end_tis };

  switch(type){
  case 'distance':
    distanceTrends(params, function(err, result){
      if(err){
        return res.status(err.code || 500).send(err.msg || 'Internal server error')
      }else{
        return res.json(result)
      }
    });
    break;
  case 'speed':
    speedTrends(params, function(err, result){
      if(err){
        return res.status(err.code || 500).send(err.msg || 'Internal server error')
      }else{
        return res.json(result)
      }
    });
    break;
  case 'utilisation':
    getVehicleUtilisation(params, function(err, result){
      if(err){
        return res.status(err.code || 500).send(err.msg || 'Internal server error')
      }else{
        return res.json(result)
      }
    });
    break;
  case 'violation':
    violationCount(params, function(err, result){
      if(err){
        return res.status(err.code || 500).send(err.msg || 'Internal server error')
      }else{
        return res.json(result)
      }
    });
    break;
  case 'violation_by_time':
    violationByTime(params, function(err, result){
      if(err){
        return res.status(err.code || 500).send(err.msg || 'Internal server error')
      }else{
        return res.json(result)
      }
    });
    break;
  default:
    return res.status(400).send('Enter stats type')
  }
};


exports.getVehicleClass = function (req, res) {
  let query =
  `
    SELECT id, array_agg(each_fee ->> '${POI_FEES_COLUMN_KEY}') asset_class
    FROM pois
    CROSS JOIN json_array_elements(${POI_FEES_COLUMN}) each_fee
    WHERE ${POI_FEES_COLUMN} IS NOT NULL
    GROUP BY id LIMIT 1 
  `;
  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .spread(function (data) {
      if (!data) {
        res.status(400).send()
      } else {
        let output = [];
        data.asset_class.forEach(function(i) {
          output.push({
            class: i,
            avg_speed: 35,
            fuel_efficiency: 3.5
          })
        });
        res.status(200).json(output)
      }
    }).catch(function (err) {
      res.status(500).send(err)
    })
};


exports.getVehicleDistanceLeaderboard = function(req, res){

  let vehicle_type = req.query.vehicle_type;
  let fk_c_id = req.headers.fk_c_id;
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let gid = req.query.gid;
  let groupCondition = gid ? ` g.gid = '${gid}'` : ` g.id in ${groupIds}`;
  let start_tis = req.query.start_tis || moment().tz(utils.timezone.default, false).startOf('day').unix();
  let end_tis = req.query.end_tis || moment().tz(utils.timezone.default, false).endOf('day').unix();

  let params = { vehicle_type, fk_c_id, groupCondition, start_tis, end_tis };

  let query =
    `
    WITH my_vehicles as (
      ${my_vehicles_query(params)}
    )

    SELECT ROUND(SUM(ard.tot_dist)) distance, a.lic_plate_no, a.id, atp.type, 'km' as unit
    FROM assets_reports_data ard
    INNER JOIN assets a on a.id = ard.fk_asset_id
    INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id
    WHERE ard.fk_asset_id IN (SELECT id FROM my_vehicles)
     AND ard.start_tis BETWEEN ${params.start_tis} AND ${params.end_tis}
    GROUP BY a.id, atp.id
    HAVING ROUND(SUM(ard.tot_dist)) > 0
    ORDER BY distance DESC
    LIMIT 10
    `;
  models.sequelize.query(query, {
    bind: {
      start_tis: start_tis,
      end_tis: end_tis
    }, type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    res.send({vehicle_distance: data})
  }).catch(function (err) {
    res.status(500).send(err.message)
  })
};