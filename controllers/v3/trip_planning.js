const moment = require('moment');
const utils = require('../../helpers/util');
const models = require('../../db/models');
const _ = require('lodash');
const async = require('async');
const PolylineUtil = require('polyline-encoded');
const RabbitMQ = require('../../helpers/rabbitMQ');
const Excel = require('exceljs');
const fs = require('fs');
const auth = require(process.cwd() + '/authorization');
const geolib = require('geolib');
const nuDynamoDb = require('../../helpers/dynamoDb');
const request = require('request');
const nodemailer = require('nodemailer');
const pdfPrinter = require('pdfmake/src/printer');
const env = process.env.NODE_ENV || 'development';
const htmlToPdf = require('html-pdf');
const config = require('../../config/index')[env];
const tableConfigsHelper = require('../../helpers/table_config');

const pdfFonts = {
  Proxima_Nova: {
    normal: __dirname + '/../../public/fonts/Proxima-Nova-Alt-Regular-webfont.ttf',
    bold: __dirname + '/../../public/fonts/Proxima-Nova-Alt-Bold-webfont.ttf',
  }
};
const printer = new pdfPrinter(pdfFonts);

const CHAR_LABEL = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
const TRIP_STATUS = ['completed','inprogress','scheduled','open','all'];
const file_base_path = __dirname + '/../../temp_reports/';
const mailerConfig = config.mailer;

const tripPlanningService = require('../../services/trip_planning_service');

function computeAssetStatus(item) {
  let status = "stationary";
  if(item.ign === 'B' && item.spd > 0 && item.osf){
    status = 'overspeeding'
  } else if(item.ign === 'B' && item.spd > 0){
    status = 'moving'
  }else if(item.ign === 'B' && item.spd < 1){
    status = 'idling'
  }
  return status
}

function convertTripLogToXLS(lic_no, data, filename, callback){
  try {
    const workbook = new Excel.Workbook();
    const ws = workbook.addWorksheet('Trip report');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = [
      {width: 15},
      {width: 15},
      {width: 30},
      {width: 15},
      {width: 30},
      {width: 15},
      {width: 15},
      {width: 15},
      {width: 15},
      {width: 15},
      {width: 15},
      {width: 15}
    ];

    ws.addRow(['Asset','Event start time', 'Start location', 'Event end time', 'End location', 'Distance(km)', 'Duration(min)', 'Avg speed','Event type', 'Latitude', 'Longitude', 'Waypoint #' ]);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    let count = 0;
    data.forEach(function (i) {
      const date_time_format = 'DD-MM-YY, HH:mm';
      let start_lname = i.lname;
      let end_lname = "";
      let start_tis = i.arrivalTis;
      let end_tis = i.departureTis;
      let event_duration = utils.FormatMinutesDHM(Math.abs(i.departureTis - i.arrivalTis));

      if(i.type === 'intransit'){
        const prev_item = data[count - 1];
        const next_item = data[count + 1];

        if(prev_item){
          start_lname = prev_item.lname;
          start_tis = prev_item.departureTis
        }

        if(next_item){
          end_lname = next_item.lname;
          end_tis = next_item.arrivalTis
        }

        if(prev_item && next_item){
          event_duration = utils.FormatMinutesDHM(Math.abs(prev_item.departureTis - next_item.arrivalTis))
        }
      }else{
        end_lname = i.lname
      }

      const excel_row_item = [
        lic_no,
        moment.unix(start_tis).tz(utils.timezone.default, false).format(date_time_format),
        start_lname,
        moment.unix(end_tis).tz(utils.timezone.default, false).format(date_time_format),
        end_lname,
        i.distance,
        event_duration,
        i.avgSpd + ' km/h',
        i.type,
        i.lat,
        i.lon,
        i.label
      ];

      ws.addRow(excel_row_item);

      const currentRowIndex = ws.rowCount;
      for (let j = 65; j <= 79; j++) {
        const currentCol = String.fromCharCode(j);
        if (['A', 'C', 'E', 'I', 'L'].indexOf(currentCol) === -1) {
          ws.getRow(currentRowIndex).getCell(currentCol).alignment = {horizontal: 'right'};
        }
      }
      count++;
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    if(!fs.existsSync(file_base_path)){
      fs.mkdirSync(file_base_path)
    }

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, data)
      }).catch(function (err) {
        callback({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      });
  }catch(e){
    console.log("XLS ERROR : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

function convertTripListToXLSV1(trip_status, data, filename, user_columns, callback) {
  try {
    const date_time_format = 'DD-MM-YY, HH:mm';
    user_columns = _.sortBy(user_columns, ['order']);
    let column_headers = [];
    let column_widths = [];
    user_columns.forEach(function (i) {

      let col_header = i.name.toLocaleUpperCase();
      col_header += i.unit ? " ("+ i.unit +")" : '';

      if(i.selected){
        column_headers.push(col_header);
        column_widths.push({width: 20})
      }
    });

    const workbook = new Excel.Workbook();
    const ws = workbook.addWorksheet('Trip list');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = column_widths;
    ws.addRow(column_headers);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    data.forEach(function (i) {
      let row = [];

      user_columns.forEach(function (col) {
        if(col.selected){
          try {
            let value;
            let nested_keys = col.key.split('.');
            if (nested_keys.length > 1) {
              if(i[nested_keys[0]] && i[nested_keys[0]][nested_keys[1]]){
                value = i[nested_keys[0]][nested_keys[1]]
              }
            } else {
              if (col.key === 'status_updated_manually') {
                value = i.status_updated_manually ? "Marked complete" : ""
              }else{
                value = i[col.key]
              }
            }

            switch (col.format) {
            case 'datetime':
              value = Number(value) ? moment.unix(value).tz(utils.timezone.default, false).format(date_time_format) : '-';
              break;
            case 'duration':
              value = utils.utilDurationFormatMDHM(value);
              break;
            case 'distance':
              value = utils.convertDistanceMetersToKm(value);
              break;
            }
            row.push(value)
          }catch(e){
            console.log("ERROR processing row data : ", e)
          }
        }
      });

      ws.addRow(row)
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    if(!fs.existsSync(file_base_path)){
      fs.mkdirSync(file_base_path)
    }

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, data)
      }).catch(function (err) {
        callback({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      });
  }catch(e){
    console.log("XLS ERROR : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

function convertTripListToXLS(trip_status, data, filename, callback){
  try {
    const date_time_format = 'DD-MM-YY, HH:mm';
    let default_cols_header = ['Asset', 'Transporter name', 'Transporter code', 'Invoice no.', 'Shipment no.', 'Ship to party', 'Consigner code', 'CMS code', 'Trip start location'];

    switch(trip_status){
    case 'scheduled':
      default_cols_header = default_cols_header.concat(['Trip end location']);
      break;
    case 'inprogress':
      default_cols_header = default_cols_header.concat(['Trip start time', 'Trip end location']);
      break;
    default:
      default_cols_header = default_cols_header.concat(['Trip start time', 'Trip end location','Trip end time'])
    }

    const workbook = new Excel.Workbook();
    const ws = workbook.addWorksheet('Trip report');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = [
      {width: 15},
      {width: 15},
      {width: 30},
      {width: 15},
      {width: 30},
      {width: 15},
      {width: 15},
      {width: 15},
      {width: 15},
    ];

    ws.addRow(default_cols_header);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    data.forEach(function (i) {

      let default_cols_data = [i.asset.licNo, i.transporter.name, i.transporter.code, i.invoiceNo, i.shipmentNo, i.shipToParty, i.consignerCode, i.CMSCode, i.source.lname];

      switch(trip_status){
      case 'scheduled':
        default_cols_data = default_cols_data.concat([i.destination.lname]);
        break;
      case 'inprogress':
        default_cols_data = default_cols_data.concat([
          moment.unix(i.source.tis).tz(utils.timezone.default, false).format(date_time_format),
          i.destination.lname]);
        break;
      default:
        default_cols_data = default_cols_data.concat([
          moment.unix(i.source.tis).tz(utils.timezone.default, false).format(date_time_format),
          i.destination.lname,
          moment.unix(i.destination.tis).tz(utils.timezone.default, false).format(date_time_format)])
      }

      ws.addRow(default_cols_data)

    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    if(!fs.existsSync(file_base_path)){
      fs.mkdirSync(file_base_path)
    }

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, data)
      }).catch(function (err) {
        callback({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      });
  }catch(e){
    console.log("XLS ERROR : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

function convertTripInvoiceListToXLS(data, filename, callback){
  try {
    const date_time_format = 'DD-MM-YY, HH:mm';
    const default_cols_header = ['Asset', 'Transporter name', 'Transporter code', 'Invoice no.', 'Shipment no.', 'Ship to party', 'Consigner code', 'CMS code', 'Trip Status', 'Trip start location', 'Trip start time', 'Trip end location', 'Trip end time'];

    const workbook = new Excel.Workbook();
    const ws = workbook.addWorksheet('Invoice report');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = [
      {width: 15},
      {width: 35},
      {width: 17},
      {width: 15},
      {width: 15},
      {width: 15},
      {width: 15},
      {width: 15},
      {width: 25},
      {width: 25},
      {width: 15},
      {width: 25},
      {width: 15},
    ];

    ws.addRow(default_cols_header);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    data.forEach(function (i) {

      let default_cols_data = [i.asset.licNo, i.transporter.name, i.transporter.code, i.invoiceNo, i.shipmentNo, i.shipToParty, i.consignerCode, i.CMSCode, i.status, i.source.lname];

      default_cols_data = default_cols_data.concat([
        i.source ? moment.unix(i.source.tis).tz(utils.timezone.default, false).format(date_time_format) : '-',
        i.destination ? i.destination.lname : '-',
        i.destination ? moment.unix(i.destination.tis).tz(utils.timezone.default, false).format(date_time_format) :  '-']);


      ws.addRow(default_cols_data)
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    if(!fs.existsSync(file_base_path)){
      fs.mkdirSync(file_base_path)
    }

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, data)
      }).catch(function (err) {
        callback({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      });
  }catch(e){
    console.log("XLS ERROR : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

function processTripSharingWithExternalUsers(newTrip,mainCallback){
    
  if(newTrip.shared_with && newTrip.shared_with.length > 0){
    const transporter = nodemailer.createTransport(mailerConfig);
    const subject = "Trip shared access - " + newTrip.tid;

    async.each(newTrip.shared_with, function (newExternalUser, callback) {
    
      async.auto({
        external_user: function (ext_user_cb) {
          models.user_external.findOrCreate({
            where: {
              email: newExternalUser.email
            },
            defaults: newExternalUser
          }).spread(function (user_external) {
            ext_user_cb(null, {id:user_external.id})
          }).catch(function (err) {
            ext_user_cb(err)
          })
        },
        share_trip: ['external_user', function (result, ext_user_cb) {
          const external_user = result.external_user;
          const user_trip_mapping = {
            fk_trip_id: newTrip.fk_trip_id,
            fk_u_id: newTrip.fk_u_id,
            fk_user_external_id: external_user.id,
            fk_c_id: newTrip.fk_c_id
          };

          if(newExternalUser.active && newExternalUser.active === false){
            // disable trip sharing
            console.log("deleting new mapping");
            models.trip_user_mapping_external.update({
              active: false
            },{
              where: {
                fk_user_external_id: external_user.id,
                fk_trip_id: user_trip_mapping.fk_trip_id
              },
              defaults: user_trip_mapping
            }).spread(function () {
              ext_user_cb(null, {created: false})
            }).catch(function () {
              ext_user_cb(null, {created: false})
            })
    
          }else{
            console.log("Creating new mapping");
    
            models.trip_user_mapping_external.findOrCreate({
              where: {
                fk_user_external_id: external_user.id,
                fk_trip_id: user_trip_mapping.fk_trip_id,
                active: true
              },
              defaults: user_trip_mapping
            }).spread(function (mapping, created) {
              ext_user_cb(null, {created: created})
            }).catch(function () {
              ext_user_cb(null, {created: false})
            })
          }
        }],
        send_mail: ['share_trip', function (result, ext_user_cb) {
          if(result.share_trip.created) {

            const app_domain = env === 'production' ? 'app' : 'dev';
            const trips_url = "https://" + app_domain + ".numadic.com/tracking";

            const mailOptions = {
              from: 'noreply@numadic.com',
              to: newExternalUser.email,
              bcc: 'noreply@numadic.com',
              subject: subject,
              html: '<html><body style="width:800px;">' +
              '<p>&nbsp;</p><p>Hi,</p>' +
              '<p>A trip has been shared with you.</p>' +
              '<p>or you can log in with your email address in the link below to view all trips</p>' +
              '<p><a href="' + trips_url + '">' + trips_url + '</a></p><br />' +

              '<p><span style="color: #000000;"><br />If you face any difficulty, please contact <a href="mailto:help@numadic.com" target="_blank">help@numadic.com</a>.<br /><br />' +
              '<br />Sincerely,</span></p><div>&nbsp;</div>' +
              '<div><span style="color: #444444; font-family: tahoma, sans-serif;"><strong>Numadic  Team</strong></span></div>' +
              '</body></html>'
            };

            transporter.sendMail(mailOptions, function (error) {
              if (error) {
                console.log(error);
              }
              ext_user_cb()
            });
          }else{
            ext_user_cb()
          }
        }]
      }, function () {
        callback()
      })
    }, function () {
      mainCallback()
    })
  }else{
    mainCallback()
  }
}



function processConsignActivity(userFeatureList, trip, mainCallback){
  const activityArr = [];
  const headerArr = [
    {text: "EVENT", style: 'tableHeader'},
    {text: "LOCATION", style: 'tableHeader'},
    {text: "CONSIGNMENT NO.", style: 'tableHeader'},
    {text: "TIME", style: 'tableHeader'},
    {text: "REMARKS", style: 'tableHeader'},
    {text: "CO. SIGN/SEAL", style: 'tableHeader'}
  ];
  activityArr.push(headerArr);
  processWaypoints(userFeatureList, trip, function(err, result){
    async.each(result, function(data, callback){
      const consignmentIds = _.compact(data.fk_consignmentIds);
      const dispatch_action_time = _.upperFirst(data.dispatchAction) + "\n" + moment.unix(data.arrivalTis).tz(utils.timezone.default, false).format("h:mm a");
      async.waterfall([
        function(wCallback){
          if(consignmentIds.length > 0){
            const query = "select json_agg(ext_consignment_no) FILTER (WHERE ext_consignment_no IS NOT NULL) as consignment_nos " +
              "from consignments where id in (" + consignmentIds + ") ";
            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .then(function (consignments) {
                console.log(" ------ consignments ", _.compact(consignments[0].consignment_nos));
                wCallback(null, _.compact(consignments[0].consignment_nos))
              })
          }
          else wCallback(null,[])
        }, 
        function(consignment_nos, wCallback){
          const waypointArr = [
            {text: dispatch_action_time, style: 'tableBody'},
            {text: data.lname, style: 'tableBody'},
            {text: consignment_nos.toString('\n'), style: 'tableBody'},
            {text: "", style: 'tableBody'},
            {text: "", style: 'tableBody'},
            {text: "", style: 'tableBody'}
          ];
          activityArr.push(waypointArr);
          wCallback(null)
        }
      ], function(){
        callback()
      })
    },function(){
      return mainCallback(activityArr)
    })
  })
}

function processConsignManifest(trip_consignments, mainCallback){
  const activityArr = [];
  const headerArr = [
    {text: "SR No.", style: 'tableHeader'},
    {text: "CNOTE NO.", style: 'tableHeader'},
    {text: "ORIGIN \n" + "DESTINATION", style: 'tableHeader'},
    {text: "START DATE \n" + "END DATE", style: 'tableHeader'},
    {text: "CONSIGNOR", style: 'tableHeader'},
    {text: "CONSIGNEE", style: 'tableHeader'},
    {text: "MATERIAL", style: 'tableHeader'},
    {text: "PKG", style: 'tableHeader'},
    {text: "WT", style: 'tableHeader'}
  ];
  activityArr.push(headerArr);
  let counter = 1;
  async.each(trip_consignments, function(data, callback){
    async.waterfall([
      function(wCallback){
        const consign_dates = moment.unix(data.consignor.tis).tz(utils.timezone.default, false).format("MMM Do, YYYY") + "\n" + moment.unix(data.consignee.tis).tz(utils.timezone.default, false).format("MMM Do, YYYY");
        const weight = data.weight + " " + data.weight_unit;
        const waypointArr = [
          {text: counter, style: 'tableBody'},
          {text: data.consignment_no, style: 'tableBody'},
          {text: data.consignor.lname + '\n\n' + data.consignee.lname, style: 'tableBody'},
          {text: consign_dates, style: 'tableBody'},
          {text: data.consignor.name, style: 'tableBody'},
          {text: data.consignee.name, style: 'tableBody'},
          {text: data.material || "-", style: 'tableBody'},
          {text: data.total_packages, style: 'tableBody'},
          {text: weight, style: 'tableBody'}
        ];
        activityArr.push(waypointArr);
        counter++;
        wCallback(null)
      }
    ], function(){
      callback()
    })
  },function(){
    return mainCallback(activityArr)
  })
}

exports.delete = function (req, res) {
  let id = req.body.id;
  const companyId = req.headers.fk_c_id;

  if(!id) {
    return res.status(400).send('Invalid Trip ID')
  }
  else{
    tripPlanningService.disableScheduledTrip(companyId, id, function(err, result){
      if(err) res.status(err.code).send(err.msg);
      else res.send(result)
    })
  }
    
};

exports.add = function(req, res){
  const trip = req.body;
  const companyId = req.headers.fk_c_id;
  const host = req.get('Host');
  console.log("============== Add Trip =============");
  async.parallel([
    function(callback){
      const requiredKeys = ['assetId', 'startTis', 'endTis', 'waypoints', 'geoRoute'];
      let dataValid = utils.validateMinPayload(requiredKeys, trip);
      if (!dataValid && trip.waypoints.length === 0) {
        callback({code: 400, msg:'Mandatory Fields: '+requiredKeys})
      }
      else callback(null)
    },
    function(callback){
      const requiredKeys2 = ['paths'];
      let geoDataValid = utils.validateMinPayload(requiredKeys2, trip.geoRoute);
      if (!geoDataValid) {
        callback({code: 400, msg:'Mandatory Fields in geoRoute: '+requiredKeys2})
      }
      else callback(null)
    },
    function(callback){
      if(trip.shipment_no){
        tripPlanningService.validateUniqueShipment(trip.shipment_no, companyId, null, function(err, result){
          if(result){
            if(result.length === 0) callback(null);
            else{
              callback({
                code: 400, 
                msg: "Shipment no. already exists"
              })
            }
          }
          else callback(null)
        })
      }
      else callback(null)
    }

  ], function(err){
    if(err){
      console.log(err);
      res.status(err.code).send(err.msg)
    }else{
      tripPlanningService.addTrip(req.headers, trip, host, function(err, result){
        if(err) res.status(err.code).send(err.msg);
        else res.send(result)
      })
    }
  })
};

exports.update = function(req, res){
  const trip = req.body;
  const companyId = req.headers.fk_c_id;
  let id = req.body.id || req.body.tripId;

  console.log("============== Update Trip =============");
  async.parallel([
    function(callback){            
      if (!id) {
        callback({code: 400, msg:'Invalid/Missing trip Id'})
      }
      else callback(null)
    },
    function(callback){
      const requiredKeys = ['assetId', 'startTis', 'endTis', 'waypoints', 'geoRoute'];
      let dataValid = utils.validateMinPayload(requiredKeys, trip);
      if (!dataValid && trip.waypoints.length === 0) {
        callback({code: 400, msg:'Mandatory Fields: '+requiredKeys})
      }
      else callback(null)
    },
    function(callback){
      const requiredKeys2 = ['paths'];
      let geoDataValid = utils.validateMinPayload(requiredKeys2, trip.geoRoute);
      if (!geoDataValid) {
        callback({code: 400, msg:'Mandatory Fields in geoRoute: '+requiredKeys2})
      }
      else callback(null)
    },
    function(callback){
      if(trip.shipment_no){
        tripPlanningService.validateUniqueShipment(trip.shipment_no, companyId, id, function(err, result){
          if(result){
            if(result.length === 0) callback(null);
            else{
              callback({
                code: 400, 
                msg: "Shipment no. already exists"
              })
            }
          }
          else callback(null)
        })
      }
      else callback(null)
    }

  ], function(err){
    if(err){
      console.log(err);
      res.status(err.code).send(err.msg)
    }else{
      tripPlanningService.updateTrip(req.headers, trip, function(err, result){
        if(err) res.status(err.code).send(err.msg);
        else res.send(result)
      })
    }
  })
};
// ALL TRIP PLANS START
exports.getAllTrips = function (req, res) {

  const userFeatureList = JSON.parse(req.headers['feature-list']);
  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const tripStatus = req.query.status;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  let start_tis = req.query.start_tis ? req.query.start_tis : null;
  let end_tis = req.query.end_tis ? req.query.end_tis : null;
  let search_cols = req.query.search;
  let search_col_condition = "";
  let sort = req.query.sort;
  const output_format = req.query.format || 'list';

  let sort_condition = " ORDER BY tp.start_tis DESC ";

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.licNo'
    }, {
      db: 'c.cname',
      client: 'asset.operator'
    }, {
      db: 'tp.id',
      client: 'tripId'
    }, {
      db: 'c.transporter_code',
      client: 'transporter.code'
    }]
  )

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.licNo'
    }, {
      db: 'c.cname',
      client: 'asset.operator'
    }, {
      db: 'c.transporter_code',
      client: 'transporter.code'
    }],
    sort_condition
  )
  if(TRIP_STATUS.indexOf(tripStatus) === -1){
    return res.status(400).send("Invalid trip status")
  }

  const timeFrameCondition = (tripStatus === 'completed' || tripStatus === 'open' ) && searchKeyword.length < 4 && start_tis && end_tis ? " AND ( (tp.start_tis >= " + start_tis + "  AND tp.end_tis <= " + end_tis + ") OR (tp.start_tis >= " + start_tis + "  AND tp.start_tis <= " + end_tis + ") OR (tp.end_tis >= " + start_tis + "  AND tp.end_tis <= " + end_tis + ")) " : '';


  const searchCondition = searchKeyword.length > 2 ? " AND ( lower(a.lic_plate_no) like '%" + searchKeyword + "%' OR lower(tp.*::text) like '%" + searchKeyword + "%' " +
    " OR lower(c.cname) like '%" + searchKeyword + "%' OR lower(c.transporter_code) like '%" + searchKeyword + "%' " +
    " OR CAST(ext_consignment_no AS TEXT) like '%" + searchKeyword + "%')" : "";

  const tripOwnersCondition = "AND (a.fk_c_id = " + fk_c_id + " or tp.fk_c_id = " + fk_c_id + ")";


  //UPDATED : Removed group by from query and used select to find status_updated_manually status

  let query = " SELECT tp.id, '[]' as groups, tp.tid as \"tripId\", tp.ext_tid as external_trip_id, tp.fk_c_id," +
    "   json_build_object('operator', c.cname, 'licNo', a.lic_plate_no, 'id', a.id, 'type', ast.type) as asset, " +
    "   tp.status, tp.waypoints, tp.trip_log, tp.end_tis, tp.shipment_no, " +
    "   (select CASE WHEN count(tul.id) > 0 THEN true ELSE false END from trip_user_logs tul where tul.fk_trip_id = tp.id AND tul.active) as status_updated_manually," +
    "   json_build_object('consumed', tf.consumed, 'mileage', tf.mileage, 'refuels', tf.total_refuels,'drops', tf.total_drops) as fuel," +
    "   json_build_object('condition',tfc.condition,'description',tfc.description) as failure_condition," +
    "   (select array(select json_build_object('id',tc.id,'consignment_no',tc.ext_consignment_no) from consignments tc where tc.fk_trip_id = tp.id))as consignment," +
    "   tp.oth_details->>'drivers' as drivers," +
    ( (tripStatus === 'completed' || tripStatus === 'open' ) && !start_tis && !end_tis ? limit : " count(tp.id) OVER()::INT " ) + " AS total_count" +
    " FROM trip_plannings tp " +
    " INNER JOIN assets a on a.id = tp.fk_asset_id" +
    " LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id" +
    " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
    " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
    " LEFT JOIN companies c on c.id = a.fk_c_id" +
    " LEFT JOIN trip_fuels tf on tf.fk_trip_id = tp.id" +
    " LEFT JOIN trip_failure_conditions tfc on tfc.id = tp.fk_trip_failure_condition_id" +
    " LEFT JOIN consignments con on tp.id = con.fk_trip_id" +
    " WHERE tp.new AND" + groupCondition + tripOwnersCondition + timeFrameCondition +
    searchCondition + search_col_condition + " AND tp.status ilike '" + tripStatus + "' and tp.active" +
    " GROUP BY tp.id, c.id, a.id, ast.id, tf.id, tfc.id, con.fk_trip_id" +
    sort_condition;

  if(output_format === 'list'){
    query += " LIMIT "+ limit +" OFFSET "+ offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (trips) {
      let total_count = 0;
      trips.forEach(function (i) {
        try {
          i.shared = i.fk_c_id !== fk_c_id;
          total_count = i.total_count;
          i.eta = null;
          i.fuel = auth.FEATURE_LIST.hasFuelSensor(userFeatureList) ? i.fuel : null;
          i.progress = i.trip_log ? Math.round(i.trip_log.progress) : 0;
          i.totalWaypoints = i.waypoints.length || 0;
          const start_wp = i.waypoints[0];
          i.source = {
            lname: start_wp.lname,
            tis: start_wp.tis
          };
          const end_wp = i.waypoints[i.waypoints.length - 1];
          i.destination = {
            lname: end_wp.lname,
            tis: end_wp.tis
          };

          const tlog = i.trip_log;
          if (tlog && tlog.violations) {
            i.totalViolations = tlog.violations.length
          } else {
            i.totalViolations = 0
          }

          if (tlog && i.status === 'inprogress') {
            i.eta = {
              tis: i.end_tis + (tlog.delayed_by || 0),
              duration: Math.round(tlog.delayed_by / 60) || 0,
              delayed: tlog.delayed || false
            };
            i.source.tis = tlog.departure
          }

          if (tlog && i.status === 'completed') {
            i.source.tis = tlog.departure;
            i.destination.tis = tlog.arrival
          }

          i.tripLoadStatus = i.status === 'inprogress' || i.status === 'scheduled' ? 'Loaded' : 'Unloaded';

          delete i.total_count;
          delete i.waypoints;
          delete i.trip_log;
          delete i.end_tis

        }catch (e){
          console.log(e)
        }

        try{
          i.drivers = typeof i.drivers === "string" ? JSON.parse(i.drivers) : null;
        }catch(e){
          console.error(e)
          i.drivers = null;
        }

      });

      if(output_format === 'xls'){
        let report_name = tripStatus;
        if(tripStatus === 'scheduled'){
          report_name = 'At Source location'
        }else if(tripStatus === 'inprogress'){
          report_name = 'Exited Source location'
        }

        const file_name = ("TRIPS-LIST-" + _.kebabCase(report_name) + "-" + utils.downloadReportDate()).toString().toUpperCase() + ".xls";
        const file_path = file_base_path + file_name;

        convertTripListToXLS(tripStatus, trips, file_name, function (err) {
          if(err){
            res.status(err.code).send(err.msg)
          }else {
            res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
            })
          }
        })

      }else{
        res.json({
          count : (tripStatus === 'completed' || tripStatus === 'open' ) && !start_tis && !end_tis ? trips.length : total_count,
          data : trips
        });
      }
    }).catch(function (err) {
      console.log(err);
      res.status(500).send(err)
    })
};

exports.getTrip = function(req, res){
  let id = req.query.id;
  const companyId = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;

  if(!id) {
    return res.status(400).send('Invalid Trip ID')
  }
  else{
    tripPlanningService.getTripDetails(companyId, id, groupIds, gid, function(err, result){
      if(err) res.status(err.code).send(err.msg);
      else res.send(result)
    })
  }
};

exports.getRealtimeStats = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const tripOwnersCondition = "AND (a.fk_c_id = " + fk_c_id + " or tp.fk_c_id = " + fk_c_id + ")";

  async.auto({
    total_assets: function (callback) {
      const query = "SELECT COUNT(DISTINCT a.id)::INT from groups g " +
        " INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected" +
        " INNER JOIN assets a on a.id = agm.fk_asset_id and a.active" +
        " WHERE a.active and g.fk_c_id = " + fk_c_id + " and " + groupCondition;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (data) {
          callback(null, data.count)
        }).catch(function (err) {
          callback(err)
        })
    },
    assets: ['total_assets',function (results, callback) {

      const query = "SELECT a.id as fk_asset_id, json_agg(json_build_object('id', tp.id, 'start_tis', tp.start_tis," +
        " 'delayed',tp.trip_log->'delayed','status', tp.status, 'eta', tp.trip_log->'ETA')) as trips" +
        " FROM groups g  LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected" +
        " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active" +
        " LEFT JOIN asset_trip_maps as atm on atm.fk_asset_id = a.id" +
        " LEFT JOIN json_array_elements_text(atm.trips) x ON TRUE" +
        " LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id AND tp.id = to_json(x::json->'id')::text::int" +
        " WHERE g.active AND " + groupCondition + " AND a.active AND tp.new AND ( tp.status = 'scheduled' OR tp.status = 'inprogress' )" +
        " GROUP BY a.id";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function (data) {
          let assetOnTrip = 0;

          data.forEach(function (i) {
            const trips = _.uniqBy(_.filter(i.trips, 'id'), 'id');

            if(trips.length > 0){
              assetOnTrip++
            }
          });

          const total_assets = results.total_assets;

          callback(null,{
            assetsAssigned: assetOnTrip,
            assetsUnassigned: Math.abs(assetOnTrip - total_assets),
            totalAssets: total_assets
          });
        }).catch(function (err) {
          callback(err)
        })
    }],
    trips: function (callback) {
      const query = "SELECT tp.fk_asset_id, json_agg(g.id) as gids, tp.status, tp.start_tis, tp.trip_log->'ETA' as eta " +
        " FROM trip_plannings tp " +
        " INNER JOIN assets a on a.id = tp.fk_asset_id" +
        " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
        " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
        " LEFT JOIN companies c on c.id = a.fk_c_id" +
        " WHERE g.active " + tripOwnersCondition + " AND " + groupCondition +
        " AND tp.new AND ( tp.status = 'scheduled' OR tp.status = 'inprogress' ) AND tp.active" +
        " GROUP BY tp.id";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function (data) {
          let totalOngoing = 0;
          let totalScheduled = 0;
          const assetsDelayed = [];
          const currentTime = moment().unix();

          data.forEach(function (i) {
            if(i.status === 'inprogress'){
              totalOngoing++;
              if(i.delayed){
                assetsDelayed.push(i.fk_asset_id)
              }else if(i.eta && i.eta > 0){
                assetsDelayed.push(i.fk_asset_id)
              }
            }else if(i.status === 'scheduled'){
              totalScheduled++;
              if(i.start_tis < currentTime){
                assetsDelayed.push(i.fk_asset_id)
              }
            }
          });
          callback(null, {
            ongoing: totalOngoing,
            scheduled: totalScheduled,
            delayed: _.size(_.uniq(_.compact(assetsDelayed)))
          })
        }).catch(function (err) {
          callback(err)
        })
    }
  }, function (err, results) {
    if(err){
      res.status(500).send()
    }else{
      res.json(_.merge(results.assets, results.trips))
    }
  })
};

exports.getAllAnalytics = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const dayStartTis = moment().tz(utils.timezone.default, false).startOf('day').unix();
  const dayEndTis = moment().tz(utils.timezone.default, false).endOf('day').unix();

  const query = "SELECT tp.id, trip_log->'stops' as stops FROM trip_plannings tp " +
    " INNER JOIN assets a on a.id = tp.fk_asset_id" +
    " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
    " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
    " LEFT JOIN companies c on c.id = a.fk_c_id" +
    " WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND tp.fk_c_id = " + fk_c_id + " AND " + groupCondition + " AND tp.status = 'completed' AND tp.new" +
    "  AND ( tp.end_tis >=" + dayStartTis + " AND tp.end_tis <=" + dayEndTis + ") " +
    " GROUP BY tp.id";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (data) {
      // average stop ([number of stops] divided by [number of trips]
      const total = data.length;
      const stats = {
        totalCompleted: total,
        avgStops: Math.round(_.sumBy(data, 'stops') / total)
      };

      res.json(stats);
    }).catch(function () {
      res.status(500).send()
    })
};
// ALL TRIP PLANS END

// SINGLE TRIP PLAN START

/**
 * Gets trip details which is created based on asset in our system
 * Updated 20/03/18 - Gets trips created with or without asset attached to a trip
 * @param {*} req 
 * @param {*} res 
 */
exports.getSummary = function (req, res) {
  const userFeatureList = JSON.parse(req.headers['feature-list']);
  const id = parseInt(req.query.id);
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const tripOwnersCondition = " AND (tp_a.fk_c_id = " + fk_c_id + " or tp.fk_c_id = " + fk_c_id + ")";

  if(isNaN(id)){
    res.status(400).send('Invalid trip id')
  }else{
    async.auto({
      trip: function (callback) {

        //Latest: Get trip details without a check on asset
        const query = `SELECT tp.id, tp.tid as "tripId", tp.status, tp.waypoints, tp.trip_log, tp.geo_trip_route, tp.start_tis, tp.end_tis, tp.fk_c_id, tp.fk_asset_id, at.type as asset_type, a.lic_plate_no, tp.tid, tp.ext_tid, extract(epoch from tp."createdAt")::int as creation_tis, tp.est_distance,  
                        json_build_object('id',a.id,'type',a.type,'operator', a.cname, 'licNo', a.lic_plate_no) as asset,    
                        json_build_object('name',a.cname,'code',a.transporter_code) as transporter,   
                        json_build_object('consumed', tf.consumed, 'mileage', tf.mileage, 'refuels', tf.total_refuels,'drops', tf.total_drops) as fuel,   
                        json_build_object('condition',tfc.condition,'description',tfc.description) as failure_condition,   
                        (SELECT CASE WHEN count(*)>0 THEN true ELSE false END from consignments tc where tc.fk_trip_id = tp.id) as consignments 
                    FROM trip_plannings tp  
                    LEFT JOIN asset_types at on tp.fk_asset_type_id = at.id
                    LEFT JOIN(
                        SELECT a.id, a.fk_ast_type_id, a.lic_plate_no, ast.type, tp.fk_c_id, c.cname, c.transporter_code 
                        from assets a  
                        INNER JOIN trip_plannings tp on a.id = tp.fk_asset_id
                        LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id 
                        INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected 
                        INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected 
                        LEFT JOIN companies c on c.id = a.fk_c_id 
                        WHERE a.fk_c_id = ` + fk_c_id + ` and tp.id = ` + id + ` AND  ` + groupCondition + ` AND g.active
                    ) a on at.id = a.fk_ast_type_id
                    LEFT JOIN assets tp_a on tp.fk_asset_id = tp_a.id AND tp_a.active
                    LEFT JOIN trip_fuels tf on tf.fk_trip_id = tp.id 
                    LEFT JOIN trip_failure_conditions tfc on tfc.id = tp.fk_trip_failure_condition_id 
                    WHERE tp.id = ` + id + tripOwnersCondition;

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .spread(function (trip) {
            if(!trip){
              res.status(404).send("No data")
            }else{
              try {
                let assetCurrentLocation = null;
                let actEndTis = null;
                let departure_tis = null;
                const tl = trip.trip_log;

                if (trip.trip_log) {
                  departure_tis = trip.trip_log.departure || null;
                  const lkl = trip.trip_log.lkLocation;
                  assetCurrentLocation = {
                    id: lkl.fk_asset_id,
                    licNo: lkl.lic_plate_no,
                    lat: lkl.lat,
                    lon: lkl.lon,
                    tis: lkl.tis,
                    ign: lkl.ign,
                    spd: lkl.spd,
                    type: lkl.type
                  };
                  actEndTis = tl.arrival
                }

                const customFields = [
                  {key: "Invoice no.", value: trip.invoice_no},
                  {key: "Shipment no.", value: trip.shipment_no},
                  {key: "Consignee", value: trip.consignee},
                  {key: "Consigner", value: trip.consigner},
                  {key: "Invoice print time", value: utils.DatetimeNoYearFormat(trip.start_tis)},
                  {key: "Distance (SAP)", value: trip.distance},
                  {key: "Material code", value: trip.material_code},
                  {key: "Material quantity", value: trip.quantity},
                  {key: "Route Id", value: trip.route_id},
                  {key: "Ship to party no.", value: trip.ship_to_party},
                  {key: "Ship to party mobile no.", value: trip.mobile_no},
                  {key: "Transporter Code", value: trip.add1},
                  {key: "Transporter Name", value: trip.add2}
                ];

                const additionalFields = [{key: "add3", value: trip.add3},
                  {key: "add4", value: trip.add4},
                  {key: "add5", value: trip.add5}];

                _.forEach(additionalFields, function (o) {
                  if(o.value && o.value.length > 0){
                    customFields.push(o)
                  }
                });

                trip.distance = 0;
                trip.duration = 0;
                trip.stops = 0;
                trip.eta = {
                  tis: trip.end_tis,
                  duration: 0,
                  delayed: false
                };

                if (tl) {
                  trip.stops = tl.stops;
                  trip.distance = Number((tl.distance / 1000).toFixed(1));
                  trip.duration = Math.round( moment.duration( (tl.lkLocation.tis - tl.departure),'seconds').asMinutes());

                  if (trip.status === 'inprogress') {
                    trip.eta.tis = trip.end_tis + (tl.delayed_by || 0);
                    trip.eta.duration = Math.round(tl.delayed_by / 60 ) || 0;
                    trip.eta.delayed = tl.delayed || false
                  }
                }

                const origin = trip.waypoints[0];
                const destination = trip.waypoints[trip.waypoints.length - 1];

                trip.source = {
                  lname: origin.lname,
                  tis: origin.tis
                };

                trip.destination = {
                  lname: destination.lname,
                  tis: destination.tis
                };

                trip.fuel.consumed = Number(trip.fuel.consumed && trip.fuel.consumed.toFixed(2) || 0);
                trip.fuel.mileage = Number(trip.fuel.mileage && trip.fuel.mileage.toFixed(2) || 0);
                trip.licNo = trip.lic_plate_no,
                trip.estDistance = Number((trip.est_distance /1000).toFixed(1));
                trip.external_trip_id = trip.ext_tid;
                trip.act_start_tis = departure_tis;
                trip.act_end_tis = actEndTis;
                trip.custom = trip.fk_c_id === fk_c_id ? customFields : [];
                trip.assetLocation = assetCurrentLocation;

                processWaypoints(userFeatureList, trip, function(err, data) {
                  callback(null, _.merge(trip, {
                    waypoints: data
                  }))
                })
              }catch (e){
                console.log(e)
              }
            }
          }).catch(function (err) {
            res.status(500).send(err)
          })
      },
      actual: ['trip', function (result, callback) {

        const params = {
          fk_asset_id: result.trip.fk_asset_id,
          start_tis: result.trip.act_start_tis,
          end_tis: result.trip.act_end_tis
        };

        if(!params.start_tis && !params.end_tis){
          callback(null, {
            path: null
          })
        }else{
          nuDynamoDb.getAssetLocationFromAPI(params, function (err, data) {
            if(data.length > 0){

              const latlngs = [];
              data.forEach(function (i) {
                latlngs.push([i.lat, i.lon]);

                i.status = computeAssetStatus(i);
                i.duration = 0;
                i.heading = 0
              });
              callback(null, {
                path: PolylineUtil.encode(latlngs),
                points: data
              })
            }else{
              callback(null, {
                path: null,
                points: []
              })
            }
          })
        }
      }],
      mapmatched_polyline: ['trip', function (result, callback) {
        const params = { fk_trip_id: result.trip.id };
        nuDynamoDb.getTripPolyline(params, function (err, data) {
          callback(null, data)
        })
      }],
      fuel: ['trip', function (result, callback) {
        const params = result.trip;
        if(!params.act_start_tis && !params.act_end_tis){
          callback(null, [])
        }else{
          const query = "SELECT is_refuel, tis, lat, lon, lname, volume_before, volume_after" +
            " FROM fuel_sensor_instances tf " +
            " WHERE tf.active AND tf.fk_asset_id = " + params.fk_asset_id + " " +
            " AND tf.tis BETWEEN " + params.act_start_tis + " AND " + params.act_end_tis +
            " ORDER BY tf.tis";

          models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
            .then(function (fuel_instances) {
              callback(null, fuel_instances)
            }).catch(function (err) {
              callback({
                code: 500,
                msg: 'Internal server error 1',
                err: err
              })
            })
        }
      }]
    }, function (err, result) {
      if(err){
        res.status(err.code).send(err.msg)
      }else{
        const data = result.trip;
        data.actualPath = result.mapmatched_polyline || result.actual.path || null,
        data.actualPoints = result.actual.points || [],
        data.fuel = auth.FEATURE_LIST.hasFuelSensor(userFeatureList) ? result.fuel : [],

        delete data.asset;
        delete data.trip_log;
        delete data.fk_asset_id;
        delete data.licNo;
        delete data.lic_plate_no;
        delete data.asset_type;
        delete data.tid;
        delete data.ext_tid;

        res.json(data);
      }
    })
  }
};

exports.getLog = function (req, res) {
  const userFeatureList = JSON.parse(req.headers['feature-list']);
  let id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const output_format = req.query.format;

  if(!id){
    res.status(400).send('Invalid trip id')
  }else{

    //New query to get trip details
    const query = `SELECT a.lic_plate_no, tp.tid, tp.status, tp.waypoints, tp.trip_log, 
                        (SELECT CASE WHEN count(*)>0 THEN true ELSE false END from consignments tc where tc.fk_trip_id = tp.id) as consignments
                    FROM trip_plannings tp  
                    LEFT JOIN asset_types at on tp.fk_asset_type_id = at.id
                    LEFT JOIN(
                        SELECT a.id, a.fk_ast_type_id, a.lic_plate_no, ast.type, tp.fk_c_id, c.cname, c.transporter_code 
                        from assets a  
                        INNER JOIN trip_plannings tp on a.id = tp.fk_asset_id
                        LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id 
                        INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected 
                        INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected 
                        LEFT JOIN companies c on c.id = a.fk_c_id 
                        WHERE a.fk_c_id = ` + fk_c_id + ` and tp.id = ` + id + ` AND  ` + groupCondition + ` AND g.active
                    ) a on at.id = a.fk_ast_type_id
                    WHERE  tp.id = ` + id;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (trip) {
        if(!trip){
          res.status(404).send()
        }else{
          const asset_lic = trip.lic_plate_no;
          if(output_format === 'xls'){

            const file_name = ("TRIP-LOG-" + trip.tid + "-" + utils.downloadReportDate()).toString().toUpperCase() + ".xls";
            const file_path = file_base_path + file_name;
            processWaypoints(userFeatureList, trip, function (err, data) {
              convertTripLogToXLS(asset_lic, data, file_name, function (err) {
                if(err){
                  res.status(err.code).send(err.msg)
                }else {
                  res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
                  res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
                  res.setHeader("Content-Type", "application/vnd.ms-excel");
                  res.setHeader("Filename", file_name);

                  res.download(file_path, file_name, function (err) {
                    if (!err) {
                      fs.unlink(file_path)
                    }
                  })
                }
              })
            })
          }else{
            processWaypoints(userFeatureList, trip,function (err, data) {
              res.json(data)
            })
          }
        }
      }).catch(function () {
        res.status(500).send()
      })
  }
};

exports.getNotifications = function (req, res) {
  let id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if(!id){
    res.status(400).send('Invalid trip id')
  }else{

    const query = "SELECT tp.fk_asset_id, tp.trip_log FROM trip_plannings tp " +
      " INNER JOIN assets a on a.id = tp.fk_asset_id" +
      " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
      " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
      " LEFT JOIN companies c on c.id = a.fk_c_id" +
      " WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND tp.id = " + id + " AND " + groupCondition + " LIMIT 1";

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (trip) {
                
        if(trip && trip.trip_log){

          const start_tis = trip.trip_log.departure;
          const end_tis = trip.trip_log.arrival;
          const fk_asset_id = trip.fk_asset_id;

          const query = 'SELECT n.msg, n.fk_notif_id as type, n.level as lvl, n.tis, params as rparams' +
            ' FROM notifications n ' +
            ' INNER JOIN groups g ON g.id = n.fk_g_id AND g.active' +
            ' LEFT JOIN assets a on a.id = n.fk_asset_id AND a.active AND a.id = ' + fk_asset_id +
            ' LEFT JOIN asset_types atp on atp.id = a.fk_ast_type_id ' +
            ' LEFT JOIN pois p on p.id = n.fk_poi_id AND p.active' +
            ' LEFT JOIN poi_types pt on pt.id = p.typ' +
            ' WHERE ' + groupCondition + ' AND fk_notif_id NOT IN (5,8) AND a.id = ' + fk_asset_id + ' AND n.fk_t_id = ' + id +
            ' AND n.tis >= ' + start_tis + ' AND n.tis <= ' + end_tis +
            ' ORDER BY n.tis DESC ';

          models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
            .then(function (notifications) {
              res.json([{
                count: notifications.length,
                data: notifications
              }])
            }).catch(function () {
              res.status(500).send()
            });
        }else{
                    
          res.json([])
        }
      }).catch(function (err) {
        console.log(err);
        res.status(500).send()
      })
  }
};

exports.getTemplate = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;

  const query = "SELECT tp.custom_fields FROM trip_plan_templates tp " +
    " WHERE tp.active AND tp.fk_c_id = " + fk_c_id + " LIMIT 1";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (template) {
      if(!template){
        res.status(404).send()
      }else{
        res.json(template)
      }
    }).catch(function () {
      res.status(500).send()
    })
};

exports.overrideTripStatus = function (req, res) {
  const data = req.body;
  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if(!data.id){
    return res.status(400).send("Enter trip id")
  }else if(!data.new_status){
    return res.status(400).send("Enter status")
  }

  async.auto({
    trip: function (cb) {
      const query = "SELECT tp.id, tp.status FROM trip_plannings tp " +
        " INNER JOIN assets a on a.id = tp.fk_asset_id" +
        " LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id" +
        " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
        " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
        " LEFT JOIN companies c on c.id = a.fk_c_id" +
        " WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND tp.id = " + data.id + " AND tp.fk_c_id = " + fk_c_id + " AND " + groupCondition + " LIMIT 1";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (trip) {
          if (!trip) {
            cb({
              code: 404,
              msg: 'Not found'
            })
          }else{
            if(trip.status === data.new_status){
              cb({
                code: 409,
                msg: 'old and new status is same'
              })
            }else{
              cb(null, trip)
            }
          }
        }).catch(function (err) {
          cb({
            code: 500,
            msg: 'Internal error',
            err: err
          })
        })
    },
    log_update_status: ['trip', function (results, cb) {
      const log = {
        fk_trip_id: results.trip.id,
        fk_u_id: fk_u_id,
        new_status: data.new_status,
        old_status: results.trip.status,
        note: data.note
      };

      models.trip_user_log.create(log).then(function () {
        cb(null,null)
      }).catch(function (err) {
        cb({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      })
    }],
    update_trip_status: ['trip','log_update_status', function (results, cb) {
      models.trip_planning.update({
        status: data.new_status
      },{
        where:{
          id: data.id
        }
      }).spread(function () {
        cb(null,null)
      }).catch(function () {
        cb(null,null)
      })
    }],
    publish_to_rabbit_mq: ['trip','log_update_status', function (results, cb) {
      if(data.new_status === 'open'){
        const msg = {id: data.id, status: 'open'};
        RabbitMQ.sendToExchange("Trip-Edit", msg)

      }
      cb(null,null)
    }]
  }, function (err) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.status(201).send("Updated trip status")
    }
  })
};

exports.getUserStatusTripLog = function (req, res) {

  let id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const searchKeyword = req.query.q || '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if(!id){
    return res.status(400).send("Enter trip id")
  }else{

    const query = "SELECT tul.note, tul.new_status, tul.old_status, EXTRACT(epoch FROM tul.\"createdAt\")::INT as tis," +
      " json_build_object('firstname', u.firstname, 'lastname', u.lastname) as user, count(tp.id) OVER()::INT AS total_count FROM trip_plannings tp " +
      " INNER JOIN trip_user_logs tul on tul.fk_trip_id = tp.id AND tul.active" +
      " LEFT JOIN users u on u.id = tul.fk_u_id" +
      " INNER JOIN assets a on a.id = tp.fk_asset_id" +
      " LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id" +
      " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
      " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
      " LEFT JOIN companies c on c.id = a.fk_c_id" +
      " WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND tp.id = " + id + " AND " + groupCondition +
      " AND (tul.note ilike '%" + searchKeyword + "%' OR tul.new_status ilike '%" + searchKeyword + "%' OR tul.old_status ilike '%" + searchKeyword + "%')" +
      " LIMIT " + limit + " OFFSET " + offset;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .then(function (logs) {
        let total_count = 0;
        logs.forEach(function (i) {
          total_count = i.total_count;


          delete i.total_count;
        });
        res.json({
          count : total_count,
          data : logs
        });
      }).catch(function () {
        res.status(500).send('Internal error')
      })
  }
};

//--------------- trip door sensor code starts here ----------------------------

function processWaypoints(userFeatureList, trip, mainCb){

  async.auto({
    waypoints: function (cb) {
      const processedPlannedWaypoints = [];
      let seqWithOne = false;

      async.eachOfSeries(trip.waypoints, function (i, index, callbackWaypoints) {

        if(index === 0){
          if(i.seq_no === 1){ seqWithOne = true }
        }

        let waypoint = {
          poi: {
            id: i.fk_poi_id > 0 ? i.fk_poi_id : null,
            type: i.type || null,
            category: null
          },
          type: "planned",
          arrivalTis: i.tis,
          departureTis: i.tis + (i.stop_dur * 60),
          dwellDuration: i.stop_dur,
          violations: 0,
          crossed: false,
          label: seqWithOne ? CHAR_LABEL[i.seq_no - 1] : CHAR_LABEL[i.seq_no || index] || '',
          avgSpd: 0,
          distance: 0,
          geoBounds: []

        };

        i.type = "planned";
        waypoint = _.merge(waypoint, i);

        if(i.fk_poi_id){
          const query = "SELECT p.geo_loc from pois p where p.id = " + i.fk_poi_id;

          models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
            .spread(function (poi) {
              if(poi){
                const bounds = [];
                const coordinates = poi.geo_loc.coordinates[0];
                coordinates.forEach(function (p) {
                  bounds.push({lon: p[0], lat: p[1]})
                });
                waypoint.geoBounds = bounds
              }
              processedPlannedWaypoints.push(waypoint);
              callbackWaypoints()
            }).catch(function (err) {
              console.log("Error occured ",err);
              processedPlannedWaypoints.push(waypoint);
              callbackWaypoints()
            })
        }else{
          processedPlannedWaypoints.push(waypoint);
          callbackWaypoints()
        }
      }, function () {
        cb(null, processedPlannedWaypoints)
      })
    },
    poi_processor: function (cb) {
      if(trip.trip_log && trip.trip_log.trip) {
        const log = trip.trip_log.trip || [];

        async.eachOfSeries(log, function (i, index, callbackWaypoints) {
          if(i.Id && i.Id > 0){
            const query = "SELECT CASE WHEN (pt.fk_c_id > 0 OR p.priv_typ) THEN 'company' ELSE 'general' END as category" +
              " FROM pois p" +
              " LEFT JOIN poi_types pt on pt.id = p.typ" +
              " WHERE p.id = " + i.Id;
            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (poi) {
                log[index].poi_category = poi ? poi.category : null;
                callbackWaypoints()
              }).catch(function (err) {
                console.log("Error occured ",err);
                callbackWaypoints()
              })
          }else{
            callbackWaypoints()
          }
        }, function () {
          cb(null, log)
        })

      }else{
        cb(null, [])
      }
    },
    logProcessor: ['waypoints', 'poi_processor', function (results, cb) {
      const waypoints = results.waypoints;
      const trip_data = results.poi_processor;
      const processedLog = [];
      if(trip_data && trip_data.length > 0){
        const log = trip_data;

        _.forEach(log, function (i, index) {

          const matchedItemIndex = _.findIndex(waypoints, function (o) {
            // Check if poi id is same or lat,lon within 50 meters
            return i.Id === o.fk_poi_id || geolib.isPointInCircle({
              latitude: o.lat,
              longitude: o.lon
            }, {
              latitude: i.Lat,
              longitude: i.Lon
            }, 50)
          });

          let labelText = "";
          let typeEvent = "unplanned";
          if(matchedItemIndex >= 0){
            typeEvent = "planned";
            labelText = waypoints[matchedItemIndex].label;
            waypoints.splice(matchedItemIndex,1)
          }

          processedLog.push({
            fk_poi_id: i.Id,
            poi: {
              id: i.Id > 0 ? i.Id : null,
              type: i.Type || null,
              category: i.poi_category || null
            },
            type:  typeEvent,
            lname: i.LName,
            arrivalTis: i.StartTis,
            departureTis: i.EndTis,
            dwellDuration: Math.round(moment.duration(i.DwellTime, 'seconds').asMinutes()),
            violations: 0,
            crossed: true,
            label: labelText,
            lat: i.Lat,
            lon: i.Lon,
            avgSpd: 0,
            distance: 0
          });

          processedLog.push({
            fk_poi_id: null,
            poi: null,
            type: "intransit",
            lname: "",
            arrivalTis: (i.EndTis + 1),
            departureTis: log[index+1] ? (log[index+1].StartTis - 1) : null,
            dwellDuration: 0,
            violations: 0,
            crossed: true,
            label: "",
            lat: null,
            lon: null,
            avgSpd: i.AvgSpd,
            distance: Number((i.Distance /1000).toFixed(1))
          })
        });

        if(trip.status === 'completed'){
          processedLog.pop()
        }

      }
      cb(null, {waypoints: waypoints, log: _.concat(processedLog, waypoints)})
    }],
    processor: ['waypoints','logProcessor',function (results, cb) {
      if(trip.status === 'scheduled'){
        cb(null, results.waypoints)
      } else {
        cb(null, results.logProcessor.log)
      }
    }]
  }, function (err, results) {
    let unplannedLabelIndex = 1;
    const data = _.forEach(results.processor, function (value) {
      value.fk_poi_id = undefined;
      if (value.type === 'unplanned') {
        value.label = unplannedLabelIndex.toString();
        unplannedLabelIndex++
      }

    });
    mainCb(null,  _.orderBy(data, ['crossed','arrivalTis'],['desc','asc']))
  })
}

//Search using
exports.getAllTripsOfSupplyPoint = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const start_tis = req.query.start_tis ? req.query.start_tis : moment().tz(utils.timezone.default, false).startOf('day').unix();
  const end_tis = req.query.end_tis ? req.query.end_tis : moment().tz(utils.timezone.default, false).unix();
  let search_cols = req.query.search;
  let search_col_condition = "";
  let sort = req.query.sort;
  const output_format = req.query.format || 'list';
  let poiId = req.query.poiId;

  let sort_condition = " ORDER BY iea.date_time desc";

  if (!poiId) {
    return res.status(400).send('Invalid poi id')
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.licNo'
    }, {
      db: 'c.cname',
      client: 'asset.operator'
    }, {
      db: 'tp.id',
      client: 'tripId'
    }, {
      db: 'COALESCE(iea.add2,c.cname)',
      client: 'transporter.name'
    }, {
      db: 'COALESCE(iea.add1, c.transporter_code)',
      client: 'transporter.code'
    }, {
      db: 'iea.consignee',
      client: 'CMSCode'
    }, {
      db: 'iea.ship_to_party',
      client: 'shipToParty'
    }, {
      db: 'iea.consigner',
      client: 'consignerCode'
    }, {
      db: 'iea.invoice_no',
      client: 'invoiceNo'
    }, {
      db: 'iea.shipment_no',
      client: 'shipmentNo'
    }]
  )

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.licNo'
    }, {
      db: 'c.cname',
      client: 'asset.operator'
    }, {
      db: 'c.transporter_code',
      client: 'transporter.code'
    }, {
      db: 'iea.consignee',
      client: 'CMSCode'
    }, {
      db: 'iea.ship_to_party',
      client: 'shipToParty'
    }, {
      db: 'iea.consigner',
      client: 'consignerCode'
    }, {
      db: 'iea.invoice_no',
      client: 'invoiceNo'
    }, {
      db: 'iea.shipment_no',
      client: 'shipmentNo'
    }, {
      db: 'iea.date_time',
      client: 'dateTime'
    }],
    sort_condition
  )

  const timeFrameCondition = " AND ( tp.start_tis >= " + start_tis + "  AND tp.start_tis <= " + end_tis + ") ";

  const searchCondition = searchKeyword.length > 2 ? " AND ( lower(a.lic_plate_no) like '%" + searchKeyword + "%' OR lower(tp.*::text) like '%" + searchKeyword + "%' OR lower(iea.*::TEXT) like '%" + searchKeyword + "%' " +
    " OR lower(c.cname) like '%" + searchKeyword + "%' OR lower(c.transporter_code) like '%" + searchKeyword + "%' " +
    " OR lower(iea.ship_to_party) like '%" + searchKeyword + "%' OR lower(iea.consigner) like '%" + searchKeyword + "%' OR lower(iea.consignee) like '%" + searchKeyword + "%' )" : "";

  let query = "WITH my_assets as ( SELECT DISTINCT a.id from groups g " +
    " INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected" +
    " INNER JOIN assets a on a.id = agm.fk_asset_id and a.active" +
    " WHERE a.active and g.fk_c_id = " + fk_c_id + " and " + groupCondition +
    " )" +
    " SELECT tp.id, '[]' as groups, tp.tid as \"tripId\", json_build_object('operator', c.cname, 'licNo', a.lic_plate_no," +
    " 'id', a.id, 'type', ast.type) as asset, json_build_object('name',COALESCE(iea.add2,c.cname),'code',COALESCE(iea.add1, c.transporter_code)) as transporter, tp.status, tp.waypoints, tp.trip_log, tp.end_tis," +
    " iea.invoice_no as \"invoiceNo\", iea.shipment_no as \"shipmentNo\", iea.date_time invoice_date_time," +
    " iea.ship_to_party as \"shipToParty\", iea.consigner as \"consignerCode\", iea.consignee as \"CMSCode\"," +
    " CASE WHEN count(tul.id) > 0 THEN true ELSE false END as status_updated_manually," +
    " json_build_object('condition',tfc.condition,'description',tfc.description) as failure_condition," +
    " count(tp.id) OVER()::INT AS total_count" +
    " FROM trip_plannings tp " +
    " LEFT JOIN trip_user_logs tul on tul.fk_trip_id = tp.id AND tul.active" +
    " LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
    " INNER JOIN assets a on a.id = tp.fk_asset_id" +
    " LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id" +
    " INNER JOIN pois p ON p.company_fields ->>'consignerCode' = iea.consigner AND p.id = " + poiId +
    " LEFT JOIN companies c on c.id = a.fk_c_id" +
    " LEFT JOIN trip_failure_conditions tfc on tfc.id = tp.fk_trip_failure_condition_id" +
    " WHERE tp.fk_asset_id in (select id from my_assets) AND tp.new AND tp.fk_c_id = " + fk_c_id + " AND p.fk_c_id = " + fk_c_id +
    timeFrameCondition + searchCondition + search_col_condition +
    " GROUP BY tp.id, c.id, a.id, ast.id, iea.id, tfc.id" +
    sort_condition;

  if(output_format === 'list'){
    query += " LIMIT "+ limit +" OFFSET "+ offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (trips) {
      let total_count = 0;
      trips.forEach(function (i) {
        try {
          total_count = i.total_count;
          i.eta = null;
          i.progress = i.trip_log ? Math.round(i.trip_log.progress) : 0;
          i.totalWaypoints = i.waypoints.length || 0;
          const start_wp = i.waypoints[0];
          i.source = {
            lname: start_wp.lname,
            tis: start_wp.tis
          };
          const end_wp = i.waypoints[i.waypoints.length - 1];
          i.destination = {
            lname: end_wp.lname,
            tis: end_wp.tis
          };

          const tlog = i.trip_log;
          if (tlog && tlog.violations) {
            i.totalViolations = tlog.violations.length
          } else {
            i.totalViolations = 0
          }

          if (tlog && i.status === 'inprogress') {
            i.eta = {
              tis: i.end_tis + (tlog.delayed_by || 0),
              duration: Math.round(tlog.delayed_by / 60) || 0,
              delayed: tlog.delayed || false
            }
          }
          i.tripLoadStatus = 'Unloaded';

          if(i.status === 'inprogress'){
            i.tripLoadStatus = 'Loaded';
            i.status = 'Exited Source Location'

          }else if(i.status === 'scheduled'){
            i.tripLoadStatus = 'Loaded';
            i.status = 'At source location'
          }

          delete i.total_count;
          delete i.waypoints;
          delete i.trip_log;
          delete i.end_tis

        }catch (e){
          console.log(e)
        }
      });

      if(output_format === 'xls'){
        const report_name = 'PLACE-INVOICES';

        const file_name = (report_name + "-" + utils.downloadReportDate()).toString().toUpperCase() + ".xls";
        const file_path = file_base_path + file_name;

        convertTripInvoiceListToXLS(trips, file_name, function (err) {
          if(err){
            res.status(err.code).send(err.msg)
          }else {
            res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
            res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
            res.setHeader("Content-Type", "application/vnd.ms-excel");
            res.setHeader("Filename", file_name);

            res.download(file_path, file_name, function (err) {
              if (!err) {
                fs.unlink(file_path, function () {
                                    
                })
              }
            })
          }
        })

      }else{
        res.json({
          count : total_count,
          data : trips
        });
      }
    }).catch(function () {
      res.status(500).send()
    })
};


//--------------- eol shipment code starts here ----------------------------

function preProcessShipmentWaypoints(trips){
  let main_trip; 
  try {
    trips = _.uniqBy(trips, 'tid');

    main_trip = _.find(trips, _.maxBy(trips, function (o) {
      let time = 0;
      if(o.trip_log){
        time = o.trip_log.time_taken
      }else{
        time = Math.abs(o.start_tis - o.end_tis)
      }
      return time
    }));

    trips.forEach(function (trip) {
      if (trip.tid !== main_trip.tid) {
        // add other waypoints
        if(trip.waypoints){
          main_trip.waypoints.push(_.last(trip.waypoints))
        }else{
          main_trip.waypoints = []
        }
        
        if(main_trip && main_trip.trip_log) {
          const last_wp = _.last(trip.trip_log.trip);

          if (last_wp && last_wp.Id > 0) {

            const matched_wp_index = _.findIndex(main_trip.trip_log.trip, function (o) {
              return geolib.isPointInCircle(
                {latitude: last_wp.Lat, longitude: last_wp.Lon},
                {latitude: o.Lat, longitude: o.Lon},
                50
              )
            });

            if (matched_wp_index > 0) {
              main_trip.trip_log.trip[matched_wp_index].Id = last_wp.Id;
              main_trip.trip_log.trip[matched_wp_index].LName = last_wp.LName;
              main_trip.trip_log.trip[matched_wp_index].Type = last_wp.Type
            }
          }
        }
      }
    });

    const wp = _.orderBy(_.uniqBy(main_trip.waypoints, 'fk_poi_id'), ['tis'], ['asc']);

    wp && _.forEach(wp, function (value, key) {
      value.seq_no = key
    });
    main_trip.waypoints = wp;
    return main_trip
  }catch(e){
    console.log("ERROR in preProcessShipmentWaypoints : " + e);
    return main_trip
  }
}

function processShipmentWaypoints(userFeatureList, trip, mainCb){
  const finalTrip = [];
  let seqWithOne = false;
  const planned_poi_ids = [];

  async.eachOfSeries(trip.waypoints, function (i, index, callback) {

    planned_poi_ids.push(i.fk_poi_id);
    if(index === 0){
      if(i.seq_no === 1){
        seqWithOne = true
      }
    }

    let hasInstransitEvent = false;
    const matchedItem = _.find(trip.trip_log && trip.trip_log.trip, ['Id', i.fk_poi_id]);

    const item = {
      type: "planned",
      lname: i.lname,
      arrivalTis: i.tis,
      departureTis: (i.tis + (i.stop_dur * 60)),
      dwellDuration: Math.round(moment.duration(i.stop_dur, 'seconds').asMinutes()),
      violations: 0,
      crossed: false,
      label: seqWithOne ? CHAR_LABEL[i.seq_no - 1] : CHAR_LABEL[i.seq_no] || '',
      lat: i.lat,
      lon: i.lon,
      avgSpd: 0,
      distance: 0
    };

    const itemIntransit = {
      type: "intransit",
      lname: '',
      arrivalTis: i.tis,
      departureTis: (i.tis + (i.stop_dur * 60)),
      dwellDuration: 0,
      violations: 0,
      crossed: true,
      label: '',
      lat: null,
      lon: null,
      avgSpd: 0,
      distance: 0,
      fuel: null
    };

    if(auth.FEATURE_LIST.hasFuelSensor(userFeatureList)){
      itemIntransit.fuel = {
        consumed: 0,
        mileage: 0,
      }
    }


    if(matchedItem){
      item.arrivalTis = matchedItem.StartTis;
      item.departureTis = matchedItem.EndTis;
      item.dwellDuration = Math.round(moment.duration(matchedItem.DwellTime,'seconds').asMinutes());
      item.crossed = true;

      if(matchedItem.Distance && matchedItem.Distance >= 0 && matchedItem.AvgSpd){
        hasInstransitEvent = true;
        itemIntransit.arrivalTis = matchedItem.StartTis;
        itemIntransit.departureTis = matchedItem.EndTis;
        itemIntransit.avgSpd = matchedItem.AvgSpd;
        itemIntransit.distance = Number((matchedItem.Distance /1000).toFixed(1));
      }

    }

    // get poi bounds

    const query = "SELECT p.geo_loc from pois p where p.id = " + i.fk_poi_id;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (poi) {
        if(poi){
          const bounds = [];
          const coordinates = poi.geo_loc.coordinates[0];
          coordinates.forEach(function (i) {
            bounds.push({
              lon: i[0],
              lat: i[1]
            })
          });
          item.geoBounds = bounds
        }

        finalTrip.push(item);
        if(hasInstransitEvent){
          finalTrip.push(itemIntransit)
        }
        callback()
      }).catch(function () {
        finalTrip.push(item);
        if(hasInstransitEvent){
          finalTrip.push(itemIntransit)
        }
        callback()
      })

  }, function () {
    if(trip.trip_log){

      const tmp_trip = _.filter(trip.trip_log.trip, function (o) {
        return planned_poi_ids.indexOf(o.Id) === -1
      });
      let c = 1;
      tmp_trip.forEach(function (o) {

        finalTrip.push({
          type:"unplanned",
          lname: o.LName,
          arrivalTis: o.StartTis,
          departureTis: o.EndTis,
          dwellDuration: Math.round(moment.duration(o.DwellTime,'seconds').asMinutes()),
          violations: 0,
          crossed: true,
          label: c.toString(),
          lat: o.Lat,
          lon: o.Lon,
          avgSpd: 0,
          distance: 0
        });

        if(o.Distance && o.Distance >= 0 && o.AvgSpd){
          // FOUND INTRANSIT IN UNPLANNED STOP

          const itemIntransit = {
            type: "intransit",
            lname: '',
            arrivalTis: o.StartTis,
            departureTis: o.EndTis,
            dwellDuration: 0,
            violations: 0,
            crossed: true,
            label: '',
            lat: null,
            lon: null,
            avgSpd: o.AvgSpd || 0,
            distance: Number((o.Distance / 1000).toFixed(1))
          };

          if(auth.FEATURE_LIST.hasFuelSensor(userFeatureList)){
            itemIntransit.fuel = {
              consumed: 0,
              mileage: 0,
            }
          }

          finalTrip.push(itemIntransit)
        }

        c++
      })
    }
    return mainCb(null, _.orderBy(finalTrip, ['crossed','arrivalTis'],['desc','asc']))
  });
}

exports.getShipmentSummary = function (req, res) {
  const id = parseInt(req.query.id);
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if(isNaN(id)){
    res.status(400).send('Invalid shipment id')
  }else{

    const query = "SELECT iea.shipment_no as id," +
      " json_build_object('id',a.id,'type',ast.type,'operator', c.cname, 'licNo', a.lic_plate_no) as asset, " +
      " json_build_object('name',c.cname,'code',c.transporter_code) as transporter" +
      " FROM trip_plannings tp " +
      " INNER JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
      " INNER JOIN assets a on a.id = tp.fk_asset_id" +
      " LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id" +
      " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
      " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
      " LEFT JOIN companies c on c.id = a.fk_c_id" +
      " WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND iea.shipment_no = '" + id + "' AND " + groupCondition + " LIMIT 1";

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (shipment) {
        if(!shipment){
          res.status(404).send()
        }else{
          res.json(shipment)
        }
      }).catch(function (err) {
        res.status(500).send(err)
      })
  }
};

exports.getShipmentLog = function (req, res) {
  const userFeatureList = JSON.parse(req.headers['feature-list']);
  let id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if(!id){
    res.status(400).send('Invalid trip id')
  }else{

    const query = "SELECT a.lic_plate_no, tp.tid, tp.start_tis, tp.end_tis,  tp.status, tp.waypoints, tp.trip_log" +
      " FROM trip_plannings tp " +
      " INNER JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
      " INNER JOIN assets a on a.id = tp.fk_asset_id" +
      " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
      " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
      " LEFT JOIN companies c on c.id = a.fk_c_id" +
      " WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND iea.shipment_no = '" + id + "' AND " + groupCondition;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .then(function (trips) {
        processShipmentWaypoints(userFeatureList, preProcessShipmentWaypoints(trips), function (err, data) {
          if(err){
            res.status(500).send()
          }else{
            res.json(data)
          }
        })
      }).catch(function () {
        res.status(500).send()
      })
  }
};

exports.getShipmentMap = function (req, res) {
  const userFeatureList = JSON.parse(req.headers['feature-list']);
  let id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if(!id){
    res.status(400).send('Invalid trip id');
  }else{

    async.auto({
      trip: function (callback) {

        const query = "SELECT tp.id as tid, tp.fk_asset_id, tp.start_tis, tp.end_tis, tp.status," +
          " tp.waypoints, tp.trip_log, tp.geo_trip_route" +
          " FROM trip_plannings tp " +
          " INNER JOIN assets a on a.id = tp.fk_asset_id" +
          " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
          " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
          " LEFT JOIN companies c on c.id = a.fk_c_id" +
          " INNER JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
          " WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND iea.shipment_no = '" + id + "' AND " + groupCondition;

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .then(function (trips) {

            let fk_asset_id = null;
            const longest_trip = _.find(trips, _.maxBy(trips, function (o) {
              let longest_time = 0;
              if (o.trip_log) {
                longest_time = o.trip_log.time_taken
              } else {
                longest_time = Math.abs(o.start_tis - o.end_tis)
              }
              return longest_time
            }));

            const assetCurrentLocations = [];
            const tripActualStartTis = [];
            const tripActualEndTis = [];

            trips.forEach(function (trip){

              if(trip.trip_log){
                const lkl = trip.trip_log.lkLocation;
                fk_asset_id = lkl.fk_asset_id;
                assetCurrentLocations.push({
                  id: lkl.fk_asset_id,
                  licNo: lkl.lic_plate_no,
                  lat: lkl.lat,
                  lon: lkl.lon,
                  tis: lkl.tis,
                  ign: lkl.ign,
                  spd: lkl.spd
                });

                tripActualStartTis.push(trip.trip_log.departure);
                tripActualEndTis.push(trip.trip_log.arrival)
              }
            });

            processShipmentWaypoints(userFeatureList, preProcessShipmentWaypoints(trips), function (err, data) {
              callback(null, {
                estPath: utils.validateEstimatedPolyline(longest_trip.geo_trip_route),
                waypoints: data,
                fk_asset_id: fk_asset_id,
                assetLocation: _.find(assetCurrentLocations, _.maxBy(assetCurrentLocations, 'tis')),
                actStartTis: _.min(tripActualStartTis) || null,
                actEndTis: _.max(tripActualEndTis) || null
              })
            })

          }).catch(function (err) {
            callback({
              code: 500,
              msg: 'Internal server error 1',
              err: err
            })
          })
      },
      actual: ['trip', function (result, callback) {

        const params = result.trip;
        if(!params.actStartTis && !params.actEndTis){
          callback(null, {
            path: null
          })
        }else{

          nuDynamoDb.getAssetLocationFromAPI(params, function (err, data) {
            if(data.length > 0){

              const latlngs = [];
              data.forEach(function (i) {
                latlngs.push([i.lat, i.lon]);

                i.status = computeAssetStatus(i);
                i.duration = 0;
                i.heading = 0
              });
              callback(null, {
                path: PolylineUtil.encode(latlngs),
                points: data
              })
            }else{
              callback(null, {
                path: null,
                points: []
              })
            }
          })
        }
      }]

    }, function (err, result) {
      if(err){
        res.status(err.code).send(err.msg)
      }else{

        const data = {
          estPathPolyline: result.trip.estPath,
          waypoints: result.trip.waypoints,
          assetLocation: result.trip.assetLocation || null,
          actualPath: result.actual.path || null,
          actualPoints: result.actual.points || [],
          fuel: null
        };

        res.json(data);
      }
    })
  }
};

exports.getShipmentInfo = function (req, res) {
  let id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if(!id){
    res.status(400).send('Invalid trip id')
  }else{

    const query = "SELECT a.lic_plate_no, tp.start_tis, tp.tid, tp.trip_log, tp.est_distance, iea.* FROM trip_plannings tp " +
      " INNER JOIN assets a on a.id = tp.fk_asset_id" +
      " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
      " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
      " LEFT JOIN companies c on c.id = a.fk_c_id" +
      " INNER JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
      " WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND iea.shipment_no = '" + id + "' AND " + groupCondition;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .then(function (trips) {
        const shipments = [];
        trips = _.uniqBy(trips, 'tid');

        trips.forEach(function(trip){
          try {
            let departure_tis = null;
            if (trip.trip_log) {
              departure_tis = trip.trip_log.departure || null
            }

            const customFields = [
              {key: "Invoice no.", value: trip.invoice_no},
              {key: "Shipment no.", value: trip.shipment_no},
              {key: "Consignee", value: trip.consignee},
              {key: "Consigner", value: trip.consigner},
              {key: "Dispatch time", value: utils.DatetimeNoYearFormat(trip.start_tis)},
              {key: "Distance (SAP)", value: trip.distance},
              {key: "Material code", value: trip.material_code},
              {key: "Material quantity", value: trip.quantity},
              {key: "Route Id", value: trip.route_id},
              {key: "Ship to party no.", value: trip.ship_to_party},
              {key: "Ship to party mobile no.", value: trip.mobile_no},
              {key: "Transporter Code", value: trip.add1},
              {key: "Transporter Name", value: trip.add2}
            ];

            const additionalFields = [{key: "add3", value: trip.add3},
              {key: "add4", value: trip.add4},
              {key: "add5", value: trip.add5}];

            _.forEach(additionalFields, function (o) {
              if(o.value && o.value.length > 0){
                customFields.push(o)
              }
            });

            const data = {
              tripId: trip.tid,
              departureTis: departure_tis || trip.start_tis,
              licNo: trip.lic_plate_no,
              origin: trip.origin,
              destination: trip.destination,
              estDistance: Number((trip.est_distance / 1000).toFixed(1)),
              custom: customFields
            };

            shipments.push(data)
          }catch (e){
            console.log(" errro : " ,e)
          }
        });

        res.json(shipments)

      }).catch(function (err) {
        res.status(500).send(err)
      })
  }
};

exports.getShipmentNotifications = function (req, res) {
  let id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if(!id){
    res.status(400).send('Invalid trip id')
  }else{

    const query = "SELECT tp.fk_asset_id," +
      " min(COALESCE(to_json(tp.trip_log->'departure')::TEXT::INT,tp.start_tis)) start_tis," +
      " max(COALESCE(to_json(tp.trip_log->'arrival')::TEXT::INT,tp.end_tis)) end_tis" +
      " FROM trip_plannings tp " +
      " INNER JOIN input_essar_a iea on iea.fk_t_id = tp.id" +
      " INNER JOIN assets a on a.id = tp.fk_asset_id" +
      " LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id" +
      " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
      " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
      " LEFT JOIN companies c on c.id = a.fk_c_id" +
      " WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND iea.shipment_no = '" + id + "' AND " + groupCondition + "" +
      " GROUP BY tp.fk_asset_id";

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (trip) {
        if(trip){
          const start_tis = trip.start_tis;
          const end_tis = trip.end_tis;
          const fk_asset_id = trip.fk_asset_id;

          const query = 'SELECT n.msg, n.fk_notif_id as type, n.level as lvl, n.tis, params as rparams' +
            ' FROM notifications n ' +
            ' INNER JOIN groups g ON g.id = n.fk_g_id AND g.active' +
            ' LEFT JOIN assets a on a.id = n.fk_asset_id AND a.active AND a.id = ' + fk_asset_id +
            ' LEFT JOIN asset_types atp on atp.id = a.fk_ast_type_id ' +
            ' LEFT JOIN pois p on p.id = n.fk_poi_id AND p.active' +
            ' LEFT JOIN poi_types pt on pt.id = p.typ' +
            ' WHERE ' + groupCondition + ' AND fk_notif_id NOT IN (5,8) AND a.id = ' + fk_asset_id +
            ' AND n.tis >= ' + start_tis + ' AND n.tis <= ' + end_tis +
            ' ORDER BY n.tis DESC ';

          models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
            .then(function (notifications) {
              res.json([{
                count: notifications.length,
                data: notifications
              }])
            }).catch(function () {
              res.status(500).send()
            });
        }else{
          res.json([])
        }
      }).catch(function () {
        res.status(500).send()
      })
  }
};

//--------------- eol shipment code ends here ----------------------------

exports.getConsignments = function (req, res) {
  let id = req.query.id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  let search_cols = req.query.search;
  let search_col_condition = "";

  if(!id){
    res.status(400).send('Invalid trip id');
  }else{

    search_col_condition = utils.processTableColumnSearch(
      search_cols,
      [{
        db: 'con.invoice_no',
        client: 'invoice_no'
      }, {
        db: "con.consignor->>'name'",
        client: 'consignor.name'
      }, {
        db: "con.consignor->>'lname'",
        client: 'consignor.lname'
      }, {
        db: "con.consignee->>'name'",
        client: 'consignee.name'
      }, {
        db: "con.consignee->>'lname'",
        client: 'consignee.lname'
      }, {
        db: 'con.material_type',
        client: 'material_type'
      }, {
        db: 'con.product_code',
        client: 'product_code'
      }]
    )

    const searchCondition = searchKeyword.length > 2 ? " AND ( " +
      " lower(con.invoice_no) like '%" + searchKeyword + "%'" +
      " OR lower(con.consignor->>'name') like '%" + searchKeyword + "%'" +
      " OR lower(con.consignor->>'lname') like '%" + searchKeyword + "%' " +
      " OR lower(con.consignee->>'name') like '%" + searchKeyword + "%'" +
      " OR lower(con.consignee->>'lname') like '%" + searchKeyword + "%' " +
      " OR lower(con.material_type) like '%" + searchKeyword + "%'" +
      " OR lower(con.product_code) like '%" + searchKeyword + "%' )" : "";


    const query = "SELECT con.id, invoice_no, consignor, consignee, material_type, product_code, total_packages, weight, weight_unit, con.status, " +
      " cs.note, json_build_object('id', u.id, 'firstname', u.firstname, 'lastname',u.lastname) as user," +
      " count(con.id) OVER()::INT as total_count" +
      " FROM consignments con" +
      " LEFT JOIN consignment_statuses cs on cs.fk_consignment_id = con.id and cs.is_current and cs.active" +
      " LEFT JOIN users u on u.id = cs.fk_by_u_id" +
      " WHERE fk_trip_id = " + id + " and con.active " + search_col_condition + searchCondition +
      " ORDER BY con.id ASC" +
      " LIMIT " + limit + " OFFSET " + offset;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .then(function (consignments) {
        let total_count = 0;
        consignments.forEach(function (i) {
          total_count = i.total_count;

          i.total_count = undefined
        });
        res.json({
          count : total_count,
          data : consignments
        })
      }).catch(function () {
        res.status(500).send("Internal server error")
      })
  }
};

exports.getNewShipmentNumber = function(req, res){
  let fk_c_id = req.headers.fk_c_id;
  if(!fk_c_id ){
    res.status(400).send("Company id ")
  }
  else{
    tripPlanningService.getShipmentNumber(fk_c_id, function(err, result){
      if(err) res.status(err.code).send(err.msg);
      else res.send(result)
    })
  }
};

exports.generateRunsheetPdf = function(req, res){
  const userFeatureList = JSON.parse(req.headers['feature-list']);
  const userId = req.headers.fk_u_id;
  let id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;

  if(!id){
    res.status(400).send('Invalid trip id')
  }else{

    const query = "SELECT a.lic_plate_no, tp.id,  tp.tid, tp.status, tp.waypoints, tp.trip_log, tp.oth_details, " +
      "   tripc.cname," +
      "   (SELECT CASE WHEN count(*)>0 THEN true ELSE false END from consignments tc where tc.fk_trip_id = tp.id) as consignments" +
      " FROM trip_plannings tp " +
      " INNER JOIN assets a on a.id = tp.fk_asset_id" +
      " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected" +
      " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected" +
      " LEFT JOIN companies astc on astc.id = a.fk_c_id" +
      " INNER JOIN companies tripc on tp.fk_c_id = tripc.id" +
      " WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND tp.id = " + id + " AND " + groupCondition + " LIMIT 1";

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (trip) {
        if(!trip){
          res.status(404).send()
        }else{
          const asset_lic = trip.lic_plate_no;
          let runsheet_by = "Runsheet generated on " + moment().format('MMM Do, YYYY h:mm a') + " by ";
          const driver_name = trip.oth_details.drivers[0].firstName + " " + trip.oth_details.drivers[0].lastName;

          const nu_logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGIAAABiCAYAAACrpQYOAAAAAXNSR0IArs4c6QAAFO1JREFUeAHtXQt0VdWZPnufc/MkIA+BpbQyFMk7NxFR6xRNLXZaWlqcKaX4KHVYVrQ64jgd5SURArQd0BaXLWqX2oraKTpV15LRGR9EqxRtJje5CQkxuMCGTsGS8IpJ7j1n7/n+c+85uffmnNsGzr1xrTkbLuec/fj33t+//73//e8HrKwqKBXfjToCfNRL4BfAREBLxIExptDPd5lHQEqp0M9yNiOIAVLIHQh4ywr0n5lDAGyYyzi73mJGEiOQ7Wtt4dATmcvep2whUF5ZHUHjtxmROkbkWBH9Z8YRSMI6lREZz93PwBkBnxHOuGTd12dE1iF3ztBnhDMuWff1GZF1yJ0z9BnhjEvWfX1GZB1y5wx9RjjjknVfnxFZh9w5Q9vE4Rycfd+amppzB3QlqCj68Mw1TVGj0eOtra2/p8Dq6upzBqVcwAXLE0wMWdAQxvFHMrlnX3NzG8WdWVHxmdxA4BLFMOgz2XHOmM46W1ubTLrl5eUTmKZdLYVQYQVNoqtIzoRiHG0Ph98AEQdiyaT/2q9PHCMihrxP0/gthMEwB6ylqg2WB4Pz2pqbfztgKAsDAe1xCWhU/El0ZMQ0DONl+H0ZwI4RXPsPQFol2fBOgMPP4EYn4lW2tbVFJFcfZFy9VlGGW6JhqFOY4IOIOwtxP0zM82zeh5fqbKh5kFagFQshTBOxZSpOfKKF5sJ6vLm2tlZj+uAbhq4foXBKk/oD256hIglVvUXlrCotXUU+R0woraycxzlfLCA5ifla7/Eq/ldeXt7/elBdm8QnhhEzZ84ci1KxKePHP4NKvw4w7EImvqC7UBjnnzva23tDe3v7IYC9OXUNhdIC9IaK4uKn0HI/zRX2fQLSyVFaxP2AGcaW6dOn5zHGNyJmsnglJASdfqbytY2NjdEE77N+da7tWZMdGYGSysrKvMIxTaWVwdW7d+/WAe5qVHjQlQqBKpV1JSUlE0/1FD4CIEOJzEDaCGis3LlzpwFpWA/GnevGCEgYSMnNkIae/LFjbwYTL3GLazYOKZ5oa2pqdi3bGQZ8EhgBKPhmhfMZePnXqqqq4vaWlt9hkeoXrlIBRiDsApaTc3d3955+Kdlq1F8QBjGw5JMd4fAejCWfQ/9/LXVJTo6YBwnbyw3jl7OCwfPR+690Y0Jcco5EGdvoROts/UadEWWV1V+D0jKf+mSAWKQztoEqpTG5ASAdpRbr5AhcgHxraVVVRXs4tEsI+YKqquhm5DHF0NbTGAJ+bUL6gFP6uB+0HrmaxoaAlKuR/5R0jCDJ6WxuPpyG3hkHjSoj0PoLoWIS8CbaZsuVyj+UBINfbGlp6Uaf8UMolq6VQysthDRRC2UY5FcKKd7DsL22ra3xwyM9Pdeiv59LY4qTI8kRUj67r6XltYqKioulwm5MJzmg3VwQCDzqRMsLv1FVX9H6l6uMVyYCAHChqIuNGLwb8nK07f26/m34BZ1aKqVD2ILyqqoFbS0tLwKQS/GTGKAngDfraCBxc6B3AiK1FuHQRtV6MCYvsRwp6SRa7BoM0B+n+Hv2OWoSgVb4Kej1d6cCTN/oYi4OFBQso4qjPa9Bbd0RNaWJ1c+89FLSusx4QtPuxAA9I5W2hZopDUJuaw+F3kfX+A0w8+/cmEBxpRQvtjY3v2Slz8Rz1BgBbWaNmzZDANLACWVqfAcAwMD9AgHi5My4nFfm9g9eQ+EzMTNnUmJC6Mw7gE7jyIFcld1fXFxchImzOSY50SY/dEl96MJIcpwJuiUcob9z7UZIZKTRy6qqLgXQS91aYZzeRwAtgneoSMoaxD3tlg+Y0YexpoXC9d7eUxhVWl2HFgRgHFkfCoWOq7n5t6MxFKeTHNhJfgYNLOyWt1f+o8EITJagAmKGnKYSaIRiDQbsPopD9iLJlJ86SYXZdQj5MOI0UdyDBw8OQP0idXbYhAugQ1013pwyYcLTJdXV08Hju5ARJRvmYpIjuiMq+9GwwAx4ZJ0RJVVV30Qlv5BOm0F/8AKMartIqyoLBmuo3oam/RtAO0QAWY7eDSEOQ9X9IQb3XMwb7kR3VtIWCr2N7uzJYYyTMio4X0mTRigEdQifgAHAIpf0NPORSn1XU9NHSQEZ+sgqI2bPnj0O5oa6dHUBg04rGqcBmpr0jzhT34U6+9XOxsY/owWvT5xXEFj4Ww/JOYrBfbmqavdDE9uuLFqkMqHdB8YhTczFJWdHRyj0DrrGuX9pooeucG+kv+8JK32mn1llxEBE/ycAMit9n6w8BBPCPnMcYWwZJlGaKtlGMLEgX9OeBKPejjHAtBG9G+nre7y4uOY8sOQeWFtpkndlWcf736K5BKRiiykV4BZN9KTG76vFRC/eNaad6GEkWdXV1eVuZvGYM1ljRHn5RTOhCv1z2j5ZikPRQW3LIrRogLUJoOZSF4aZRVX/oH5LzNAmV1oYYBxZTWDxXINmxVNtBnNZZ0qf0B8SwmjXMOOG/rO1vanp0J96eq7DWPEXJnrKrzEved3KJxvPrDFCcqMOAJzj1ifTdABAbujsbPzzvo6OxWj1V1laFQGMJQOyQ03DTPgttO61+G3AOPIqDIazIQ3/mBgXDJzZH42ugOnitJTqct3Qt0IMtpE6jJWede5lIHVVnoQGVpcN8BPzyMrMuqSiuhZALrbASiwAvZM2A1vTnoKcwC+pJWM2fd/QkIyRAYxAi5+sG4LGjuXt4eb6OA2YqfhGhCXNiikfTNBXlFZXP90eanoTcemnoLsjyfkbt3IgTDGk+AnmLp1x+ll7ZFwiAGyAqZLsQe5Ml1KXkq+irqc/atxJLdruZuJQxMBVvgOmXm6hU1YRdJ0VQ6LOwcRunRWXjIPg+PfcmEDjDk30NCEesNJk8+kOjkel+Dga/Tbn6uXp1FVhiF91tDbvRgu+EGaPFa5gcZ4LhfU2FO0ds3hMWUIApjKNwmKMY4vBuJ93tIZ2Q8KuQosfS1ZeVydkXbg13BsPZ5hrXKByrinxIVsEhC2oBap62EvbU0YlYtbs2ZNgHb03XZ8MBvVGpFZnVl7I9QB2XByIYQ/S+AH6G0MB7GknJtjhjGmcy3pTKo3A82DCkUT114pHXRIY1zB50vhfWX4wJC7WJGuVuhGWXDd/2J8Qph+XSrg/oj9kxfXimVFGqLr+fXTin3YDiwCAveGBrtbGA6WVNfNg8l7kKg0YZEh1VYX4hVXxfeHQc9Ccdpl0LM+EZ0zj4n8L0K4ndRbS5mhWR/miXOXm6qCdnLFuqM40+88D82I/RaHvXDSWXPgdsuN68JIxRpSX15Sh4re6A2vOAzoHC/p+QrNifG1CfUjPdHE0johVsQX+6vlY5L8CEQUzOC2rupunaebMlHvJNH5iTMF2Wleg7sxyxESahdNsXFEWqTTZmwGFgXaJIO7jqUymtKhT50BA9XQsyRgjFFVsQCXGWBV2eqJ3X9e1t+tkTmHhjWiRc9JJDkB5Curqm6SuArsXoRX9hkzpbUAQez4eTgXMyo9oImy6VNW7uvdgWZUNLatSNxWf6K2n+GXBzpswO38zL6LfT99YQq2HVH2U2p2h3HUfNDaeoDheuYwwAq11Pgq/0FUaqBVK8VpZcfFOgDkFkrPGjQnxFtgjOa9DpTG3w4ocwz4K2IkEU01TCJY5f4C8/pDY0hMBMsvB+G3FtB4eCr0Exr1IjKPVP5jBt9BEr7x8zlQqB8VFJjeQZED6PoSx0e7OTKNhrNy/TqTvxbvnjJj22c/mo7WSnu9OW4pBVMrcZQGA78H7+ekYgYFkK2xEB61FHOr7Y4ApS6GWXka2JkjMBjdGEFAAfSxMJRvoHenvRfoBaGv79IEBc9AV6uBQOcx1braxFuYQmFV+hvhhU+KkoAOIq2h3CNHx0rmDdYa5FPX1fQeFrnEDliokhfIoepT3YFktR/O7iUB1cgSsIWQL9hxto0UchcHol+hgAkGbpjmKSoO4YYh33JhBeUBx+PvSioqrY+sLspZL42v79+8/RRZeSNp3rXIQo0Fn7kdY9zZXCQVbie9+zLp/jrHj3cQiePXuKSNggpgMYNJuSUElDwMAkhiGnZLUigvTVAZrOAoN0Kd5bj4ZDEsSGRwH7CoyrdMgjsirQEt3o4dhG0vkav10bCSDqWQv9tAeUJQ62kVQj3LkD0/H1tEg394aeknRoxedU1j4L8PjeOPjKSN0ye7GevOnqGiomOMP/fMmAHCktKJ6Pnr6awhYp7gchjpaIiUQzPVtRd7jGley9ZgrFKClN2CF9GnaVuNEk8qFsEsKisYtoXdypcHffJ2p6vyYNJA2FfuZeanqDKkGTPCxSNexB4O9mSgD/3g6s0YVjgO8jsRWa5UZ4wbsSaKl/9TJx8iPMUOTinYA4j6sMZAHgDkJyaEWTi6Cnn0vNpKdN2waHeN3y7GiIrN/40Z0rVACF6D1Tx4WF4SwazwKOkdNqviHwyqIx59QogD1mUkOGgUGc8/Hg6Q84h/Mup3G7LsNeTNuHnjEKeJf60ezWLe4MTP2UOh5aMWTotEhpT4ehHUF8otQdzMUW1HcaKfSpTRp4hLaSeDS1k1ISgBSBP7FHBoTBEUVUARohc72t8LP9ombB77LVPawNS55KhFUOCdQ3Ar9R2yX+aNboIP/SGiPJG5HR8cxh+yy6jWsW8hq7n5mNgI+I2woRvfFZ8To4m/n7jPChmJ0X3xGjC7+du4+I2woRvfFZ8To4m/n7jPChmJ0X1IZ4fkMcnSr94nOPQnrpJk17DAa2eBPnaKd7b7LFAJFRUUSR8u0xEP6tq2JMgX6h2GI6cHTZ0SmuAC6EAWydE7A83wrmySJgEn0fESwA61I/tNbBOxWThsb4i6JEbA40qIKWSbtuFZE/+kpAsQBMr/b+NsvZAbHjoZ6mGZ3epqlT8wRAWnIRVi6rXM0g2PpsRtLiPscU/qeniJQWlHVrfChbVxJ6isW0IdCPM3WJ5aKQCrWSYxIjex/Zw8BnxHZwzptTj4j0sKTvUCfEdnDOm1OPiPSwpO9QJ8R2cM6bU4+I9LCk71AnxHZwzptTj4j0sKTvUDb1uRFluVVNd/QRfTg/vhNxaDJsLVwCWwqU3Vp7Malif9TXlNThh3eX8IO7B5s73xSqa1lZcd6b8AZifFc8F3hcGMHNihfLbkY29HS8lxquWgr5UDUWIoTEr8DvdbEcLoQJaqoczvCFz6mKMlnGHA4cQmsy1MFTrPTflYc/X3/RE9hA13OaNGgsxYaUy/HUYA/4HyebXOjPHGA/gpsySxF4l4Wjb5Fh1isdF48PZUIbIu/Q9MCL9PtkvHCMRjeV3NN3coln2f6GfJy7Cndijv46rHlXZ02OBjALc/1KuII1biM4gCoJZqqPYtTO9YmZDMp3VYDQJ5A3Edx3seMawbE/zFwQCag8UdKK7s+n+hP79hNvArHjO/HRbwP4LkN5zL+c9zE/gbsNK+y4sLkvIDKinMY9vZ7uroCjP9vbKJ+FekexKUuOxQtECoNBm+w0nnx9JQRKFAfNmdPxGVTz+Os25X4xi58ORCzMEpcNgMnFZ2+sd3Xbon0Tn5xMzwYwQbwgSffiE3SWygZXd+gK3wnwIhdHwo65G+54higX6Gd6LAjr4B/sik/Xg7kc0ToRhfFQz5zcJ/fzlmzZk+K04nGyor84dBQcJW1/gyOD1xJ8Q1KJ8Rx2LD7Ub7342k8eXjNCAKTIJgIwJ6FZHwejDnjG15MsDi/q6wyuMNg7Hl0cV+OATW87jDj34Zfnpk/Z19EN0MXLSY5MvXj2MaP83O0IBrLN0HrNM5hzArkGouTIsY/cDfgVyC9F+N8NhoGu95MZ+hzuKHPo7tpndKcqZ+nY4RVCFr1QGubhOMG6GcBDTHnjJwkychDi7yOkgM42lJPkpBD35aDtMxAt7QYrfVjZNWG7mUOLtW4A+HDwEJZdOwUp+PAO8FgXEmKQ5dczrZoJT5xoqmaaaZgNWLMeCoe1pUYx6t3zyUCFSPsX8GBlQNowRNRUEvszTIDCDtPOv/QPW0aXR9t+pmyFK8ZneBEK3wZdNabUNBZaqbchOAPUnsd9P/Lccx0LOg0gAG3UwvG4cWvm/dvxOlZDxyMIWbGHFPGoqzUVOxu0goynwwNIebG1Zr3PCWFevphg+IV1RgjlL0YuBeigofN70TijOEsggn5NLTIm8s6O28F0OapUiyGHEmMiqOg0X3h5nXQYu7EaZ4luLfvMTAjSYrRj09Fb780JizsMpyS2wHqdOgkH/rR7Un08AEGTwODLsI95D9AnCspHUB4JTVe/Pt1hENRYhVHj/VCCai8gg6YIP0tLvHP2NtTRqBxmY0XI3EBqZbM0BeiZEnMyFGVBlSujfpznGfejgF5G/rpHNS3Eceo3qaaoI3GhcA8VqW0t4R+TBfsTschRDTfWB7xKgs1sAy0JoPp5ukiCOS5iHCCAEZplkAr+kw8Kq7xM5WEFRi3fq+q/G4wBQf15L+fO2HCLjOOTTuWB1Yrfwu6D5N04kD+jYjeQKd8oNH9lBpRnK4nD08ZgYoTGBFUztRo6H8+waVVC9F3H4a/CSBd84lrGzBQyldRydMIO4Wzdbu4wb9l3WoJTpCGRbRimhZeEpyZB1p9lNRZ3AKwFO+DYN6tEZUXD6r8Qi6MMrDzFQy0+fgPPJbF08bKFqOJc3TyEPLdDE4so8sWKQ7o0NOKZ3qhMd2BeKupq0WaQZS3B98vYDryHkXwyiWJ+dkShTbxPaHyMRpj9qWGNLkrLS2dYxQU2KC2tTXRuvjVdOUnxF7Syf/EvJnQNwnJt+N04clEf1wlOogWvkCqMjdSkNsdOXZMx/UR1zA9KtpbWzsR1z4bh3Nx17F8dYoqofHAQT6uUw1IIXCOci5wKv9IqDl0PJF+VNO24xT4c1Ll5jWnFBY/x7cJ57wf5Pn5U6G99e1vahrJibPELFzf7Q1mpNphZ8FZH2Z0zckPSEIg9TCjt11TUlb+x0gQ8BkxErQyGNdnRAbBHQlpnxEjQSuDcX1GZBDckZD2GTEStDIY12dEBsEdCWmfESNBK4NxfUZkENyRkE5lBNlZfJcdBJKwtm1NMGiR1fMLmHonLbpkp0z//3IB1nPJymg529ZEHjDAmT8r0H9mDgGz4aPxW86WCPJIDbQi+c/MI5A6RmQ+Rz8HRwT+D40An1MVO5YeAAAAAElFTkSuQmCC";
          async.waterfall([

            function(callback){
              models.sequelize.query("select name from users where id = "+userId, {type: models.sequelize.QueryTypes.SELECT})
                .spread(function (user) {
                  runsheet_by = runsheet_by + user.name + "\n\n";
                  callback(null)
                });
            },
            function(callback){
              processConsignActivity(userFeatureList, trip, function(result){
                callback(null, result)
              })
            },
            function(tableContent, callback){

              if(!fs.existsSync(file_base_path)){
                fs.mkdirSync(file_base_path)
              }

              const content_data = [
                {
                  table: {
                    widths: ['*', 'auto'],
                    body: [
                      [
                        {
                          "text": "DRIVERS RUN SHEET",
                          "style": 'docTitle',
                        },
                        {
                          "image": nu_logo,
                          "style": "logo",
                          "height": 50,
                          "width": 49
                        }
                      ]
                    ]
                  },
                  layout: 'noBorders'
                },
                {
                  "text": trip.cname,
                  "style": "heading-1"

                }, {
                  "text": runsheet_by

                },
                {
                  table: {
                    widths: [60, 90, 120, '*'],
                    body: [
                      ["Trip:", "Asset:", "Driver:", "Date:"],
                      [
                        {
                          "text": trip.id,
                          "style": "summary-value"
                        },
                        {
                          "text": asset_lic,
                          "style": "summary-value"
                        },
                        {
                          "text": driver_name,
                          "style": "summary-value"
                        },
                        {
                          "text": moment().format('MMM Do, YYYY'),
                          "style": "summary-value"
                        }
                      ]
                    ]
                  },
                  layout: 'noBorders'
                }, {
                  "text": '\n',
                }, {
                  style: 'docHeadTable',
                  table: {
                    widths: [50, '*', 'auto', 45, 'auto', 50],
                    body: tableContent
                  }
                }

              ];
              callback(null, content_data)
            }
          ], function(err, result){
            const docDefinition = {
              content: result,
              styles: {
                "docTitle": {
                  "border": false,
                  "fontSize": 18,
                  "bold": true,
                  "fontWeight": "bold",
                  "margin": [150, 10, 0, 0]
                },
                "logo": {"alignment": "right"},
                "docHeadTable": {"color": "#4A4A4A"},
                "summary-value": {"bold": true, "fontSize": 11},
                "heading-1": {"bold": true, "fontSize": 16, "fontWeight": 600},
                "heading-2": {"bold": true, "fontSize": 16, "fontWeight": 600},
                "tableHeader": {"fontSize": 9, "bold": true, "color": "#000000", "margin": [5, 10, 0, 0]},
                "tableBody": {"fontSize": 10, "color": "#000000", "margin": [5, 10, 10, 10]}
              },
              defaultStyle: {
                font: 'Proxima_Nova'
              }
            };
            const file_name = ("RS" + "-" + utils.downloadReportDate()).toString().toUpperCase() + ".pdf";
            const file_path = file_base_path + file_name;
            res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
            res.setHeader("Content-Disposition","attachment; filename=" + file_name);
            res.setHeader("Content-Type","application/pdf");
            res.setHeader("Filename", file_name);
            const pdfKitDoc = printer.createPdfKitDocument(docDefinition);
            const fileStream = fs.createWriteStream(file_path);

            pdfKitDoc.pipe(fileStream);
            pdfKitDoc.end();
           
            fileStream.on('close', function(){
              res.download(file_path, file_name,function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
                else {
                  console.log("Error downloading pdf === ",err)
                }
              })   
            })
                        
          })
                    
        }
      }).catch(function (err) {
        console.log(err);
        res.status(500).send()
      })
  }
};



exports.getTripRetrospective = function(req,res){
  const userId = req.headers.fk_u_id;
  const companyId = req.headers.fk_c_id;

  let trip_id = req.query.id;
  const output_format = req.query.format;

  if(!trip_id) res.status(400).send("Invalid trip id");

  const url = config.server_address.retrospective_api_server() + "/trip_retrospective?trip_id=" + trip_id;
  const options = {
    url: url,
    headers: {
      'User-Agent': 'request',
      'Authorization': req.headers.authorization
    }
  };
  request(options, function (error, response, body) {
    if(body.status === "Error"){
      res.status(400).send(body.msg)
    }
    else{
      const trip_data = JSON.parse(body);
      trip_data.googleMapKey = config.googleMap.key;
      trip_data.url = config.server_address.consign_trip_planning();

      if(output_format === 'pdf'){
        async.waterfall([
          function(callback){
            models.sequelize.query("select name from users where id = "+userId, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (user) {
                trip_data.userName = user.name ;
                trip_data.reportGeneratedDate = moment().format('MMM Do, YYYY');
                callback(null)
              });
          },
          function(callback){
            const query = " select cname, a.lic_plate_no, " +
              "   tp.waypoints->0->'lname' as source, tp.waypoints->(json_array_length(tp.waypoints)-1)->'lname' as destination," +
              "   tp.waypoints->0->'lat' as source_lat, tp.waypoints->0->'lon' as source_lng," +
              "   tp.waypoints->(json_array_length(tp.waypoints)-1)->'lat' as destination_lat, " +
              "   tp.waypoints->(json_array_length(tp.waypoints)-1)->'lon' as destination_lng" +
              " from companies c " +
              " inner join trip_plannings tp on c.id = tp.fk_c_id" +
              " inner join assets a on tp.fk_asset_id = a.id" +
              " where c.id = " + companyId + " and tp.id = " + trip_data.trip_id;
            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (trip) {
                trip_data.trip = trip;
                callback(null)
              });
          }
        ], function(err){
          if(err){
            res.status(400).send(err)
          }
          else{
            const pdfOptions = {
              format: "A4"
            };
            const fileName = ("TRIP-RETROSPECTIVE" + "-" + utils.downloadReportDate()).toString().toUpperCase() + ".pdf";

            const options = {
              uri: config.server_address.consign_trip_planning() + '/v2/consign/tripplans/tripRetrospectiveTemplate',
              method: 'POST',
              json: trip_data
            };

            request(options, function (err, respdf, body) {
              if (err) {
                return res.status(500).send(err)
              } else {
        
                htmlToPdf.create(body, pdfOptions).toFile(file_base_path + fileName, function (err, result) {
        
                  if (err) {
                    return res.status(500).send(err)
                  } else {
                    res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
                    res.setHeader("Content-Disposition","attachment; filename=" + fileName);
                    res.setHeader("Content-Type","application/pdf");
                    res.setHeader("Filename", fileName);

                    res.download(result.filename, fileName ,function (err) {
                      if(!err){
                        fs.unlink(result.filename)
                      }
                    })
                  }
                });
        
              }
            })
          }
        })
                
      } else {
        trip_data.googleMapKey = undefined;
        trip_data.url = undefined;
        res.json(trip_data)
      }
    }
  });
};

exports.generateConsignmentManifestPdf = function(req, res){

  const userId = req.headers.fk_u_id;
  let trip_id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  if(!trip_id) return res.status(400).send('Invalid trip id');

  const requiredKeys = ['fk_u_id', 'fk_c_id', 'group_ids'];
  let dataValid = utils.validateMinPayload(requiredKeys, req.headers);
  if (!dataValid && !trip_id) {
    return res.status(400).send('Mandatory Fields: trip_id, '+requiredKeys)
  }

  const query = "SELECT tp.id, a.lic_plate_no, tp.start_tis, cmp.cname, " +
    "tp.waypoints->0->'lname' as source, tp.waypoints->(json_array_length(tp.waypoints)-1)->'lname' as destination, " +
    "tp.oth_details->'drivers'->0->'firstName' as driver_firstname, tp.oth_details->'drivers'->0->'lastName'as driver_lastname, " +
    "(select count(*) from consignments where fk_trip_id = tp.id) as consignments_count, " +
    "(select coalesce(sum(weight),0) from consignments where fk_trip_id = tp.id) as total_weight, " +
    "(select coalesce(sum(total_packages),0) from consignments where fk_trip_id = tp.id) as total_packages, " +
    "(select array(select json_build_object('consignment_no',ext_consignment_no, " +
    "                                       'consignor',consignor, " +
    "                                       'consignee',consignee, " +
    "                                       'material',material_type, " +
    "                                       'total_packages',coalesce(total_packages,0), " +
    "                                       'weight',coalesce(weight,0), " +
    "                                       'weight_unit', weight_unit) " +
    "   from consignments where fk_trip_id = tp.id) ) as consignments " +
    "FROM trip_plannings tp  " +
    "INNER JOIN assets a on a.id = tp.fk_asset_id " +
    "INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected " +
    "INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected " +
    "INNER JOIN companies cmp on tp.fk_c_id = cmp.id " +
    "WHERE g.active AND g.fk_c_id = " + fk_c_id + " AND tp.id = " + trip_id + " AND  g.id in " + groupIds;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (trip) {
      if(trip){
        const companyName = trip.cname || "-";
        let runsheet_by = "Runsheet generated on " + moment().format('MMM Do, YYYY h:mm a') + " by ";
        const nu_logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGIAAABiCAYAAACrpQYOAAAAAXNSR0IArs4c6QAAFO1JREFUeAHtXQt0VdWZPnufc/MkIA+BpbQyFMk7NxFR6xRNLXZaWlqcKaX4KHVYVrQ64jgd5SURArQd0BaXLWqX2oraKTpV15LRGR9EqxRtJje5CQkxuMCGTsGS8IpJ7j1n7/n+c+85uffmnNsGzr1xrTkbLuec/fj33t+//73//e8HrKwqKBXfjToCfNRL4BfAREBLxIExptDPd5lHQEqp0M9yNiOIAVLIHQh4ywr0n5lDAGyYyzi73mJGEiOQ7Wtt4dATmcvep2whUF5ZHUHjtxmROkbkWBH9Z8YRSMI6lREZz93PwBkBnxHOuGTd12dE1iF3ztBnhDMuWff1GZF1yJ0z9BnhjEvWfX1GZB1y5wx9RjjjknVfnxFZh9w5Q9vE4Rycfd+amppzB3QlqCj68Mw1TVGj0eOtra2/p8Dq6upzBqVcwAXLE0wMWdAQxvFHMrlnX3NzG8WdWVHxmdxA4BLFMOgz2XHOmM46W1ubTLrl5eUTmKZdLYVQYQVNoqtIzoRiHG0Ph98AEQdiyaT/2q9PHCMihrxP0/gthMEwB6ylqg2WB4Pz2pqbfztgKAsDAe1xCWhU/El0ZMQ0DONl+H0ZwI4RXPsPQFol2fBOgMPP4EYn4lW2tbVFJFcfZFy9VlGGW6JhqFOY4IOIOwtxP0zM82zeh5fqbKh5kFagFQshTBOxZSpOfKKF5sJ6vLm2tlZj+uAbhq4foXBKk/oD256hIglVvUXlrCotXUU+R0woraycxzlfLCA5ifla7/Eq/ldeXt7/elBdm8QnhhEzZ84ci1KxKePHP4NKvw4w7EImvqC7UBjnnzva23tDe3v7IYC9OXUNhdIC9IaK4uKn0HI/zRX2fQLSyVFaxP2AGcaW6dOn5zHGNyJmsnglJASdfqbytY2NjdEE77N+da7tWZMdGYGSysrKvMIxTaWVwdW7d+/WAe5qVHjQlQqBKpV1JSUlE0/1FD4CIEOJzEDaCGis3LlzpwFpWA/GnevGCEgYSMnNkIae/LFjbwYTL3GLazYOKZ5oa2pqdi3bGQZ8EhgBKPhmhfMZePnXqqqq4vaWlt9hkeoXrlIBRiDsApaTc3d3955+Kdlq1F8QBjGw5JMd4fAejCWfQ/9/LXVJTo6YBwnbyw3jl7OCwfPR+690Y0Jcco5EGdvoROts/UadEWWV1V+D0jKf+mSAWKQztoEqpTG5ASAdpRbr5AhcgHxraVVVRXs4tEsI+YKqquhm5DHF0NbTGAJ+bUL6gFP6uB+0HrmaxoaAlKuR/5R0jCDJ6WxuPpyG3hkHjSoj0PoLoWIS8CbaZsuVyj+UBINfbGlp6Uaf8UMolq6VQysthDRRC2UY5FcKKd7DsL22ra3xwyM9Pdeiv59LY4qTI8kRUj67r6XltYqKioulwm5MJzmg3VwQCDzqRMsLv1FVX9H6l6uMVyYCAHChqIuNGLwb8nK07f26/m34BZ1aKqVD2ILyqqoFbS0tLwKQS/GTGKAngDfraCBxc6B3AiK1FuHQRtV6MCYvsRwp6SRa7BoM0B+n+Hv2OWoSgVb4Kej1d6cCTN/oYi4OFBQso4qjPa9Bbd0RNaWJ1c+89FLSusx4QtPuxAA9I5W2hZopDUJuaw+F3kfX+A0w8+/cmEBxpRQvtjY3v2Slz8Rz1BgBbWaNmzZDANLACWVqfAcAwMD9AgHi5My4nFfm9g9eQ+EzMTNnUmJC6Mw7gE7jyIFcld1fXFxchImzOSY50SY/dEl96MJIcpwJuiUcob9z7UZIZKTRy6qqLgXQS91aYZzeRwAtgneoSMoaxD3tlg+Y0YexpoXC9d7eUxhVWl2HFgRgHFkfCoWOq7n5t6MxFKeTHNhJfgYNLOyWt1f+o8EITJagAmKGnKYSaIRiDQbsPopD9iLJlJ86SYXZdQj5MOI0UdyDBw8OQP0idXbYhAugQ1013pwyYcLTJdXV08Hju5ARJRvmYpIjuiMq+9GwwAx4ZJ0RJVVV30Qlv5BOm0F/8AKMartIqyoLBmuo3oam/RtAO0QAWY7eDSEOQ9X9IQb3XMwb7kR3VtIWCr2N7uzJYYyTMio4X0mTRigEdQifgAHAIpf0NPORSn1XU9NHSQEZ+sgqI2bPnj0O5oa6dHUBg04rGqcBmpr0jzhT34U6+9XOxsY/owWvT5xXEFj4Ww/JOYrBfbmqavdDE9uuLFqkMqHdB8YhTczFJWdHRyj0DrrGuX9pooeucG+kv+8JK32mn1llxEBE/ycAMit9n6w8BBPCPnMcYWwZJlGaKtlGMLEgX9OeBKPejjHAtBG9G+nre7y4uOY8sOQeWFtpkndlWcf736K5BKRiiykV4BZN9KTG76vFRC/eNaad6GEkWdXV1eVuZvGYM1ljRHn5RTOhCv1z2j5ZikPRQW3LIrRogLUJoOZSF4aZRVX/oH5LzNAmV1oYYBxZTWDxXINmxVNtBnNZZ0qf0B8SwmjXMOOG/rO1vanp0J96eq7DWPEXJnrKrzEved3KJxvPrDFCcqMOAJzj1ifTdABAbujsbPzzvo6OxWj1V1laFQGMJQOyQ03DTPgttO61+G3AOPIqDIazIQ3/mBgXDJzZH42ugOnitJTqct3Qt0IMtpE6jJWede5lIHVVnoQGVpcN8BPzyMrMuqSiuhZALrbASiwAvZM2A1vTnoKcwC+pJWM2fd/QkIyRAYxAi5+sG4LGjuXt4eb6OA2YqfhGhCXNiikfTNBXlFZXP90eanoTcemnoLsjyfkbt3IgTDGk+AnmLp1x+ll7ZFwiAGyAqZLsQe5Ml1KXkq+irqc/atxJLdruZuJQxMBVvgOmXm6hU1YRdJ0VQ6LOwcRunRWXjIPg+PfcmEDjDk30NCEesNJk8+kOjkel+Dga/Tbn6uXp1FVhiF91tDbvRgu+EGaPFa5gcZ4LhfU2FO0ds3hMWUIApjKNwmKMY4vBuJ93tIZ2Q8KuQosfS1ZeVydkXbg13BsPZ5hrXKByrinxIVsEhC2oBap62EvbU0YlYtbs2ZNgHb03XZ8MBvVGpFZnVl7I9QB2XByIYQ/S+AH6G0MB7GknJtjhjGmcy3pTKo3A82DCkUT114pHXRIY1zB50vhfWX4wJC7WJGuVuhGWXDd/2J8Qph+XSrg/oj9kxfXimVFGqLr+fXTin3YDiwCAveGBrtbGA6WVNfNg8l7kKg0YZEh1VYX4hVXxfeHQc9Ccdpl0LM+EZ0zj4n8L0K4ndRbS5mhWR/miXOXm6qCdnLFuqM40+88D82I/RaHvXDSWXPgdsuN68JIxRpSX15Sh4re6A2vOAzoHC/p+QrNifG1CfUjPdHE0johVsQX+6vlY5L8CEQUzOC2rupunaebMlHvJNH5iTMF2Wleg7sxyxESahdNsXFEWqTTZmwGFgXaJIO7jqUymtKhT50BA9XQsyRgjFFVsQCXGWBV2eqJ3X9e1t+tkTmHhjWiRc9JJDkB5Curqm6SuArsXoRX9hkzpbUAQez4eTgXMyo9oImy6VNW7uvdgWZUNLatSNxWf6K2n+GXBzpswO38zL6LfT99YQq2HVH2U2p2h3HUfNDaeoDheuYwwAq11Pgq/0FUaqBVK8VpZcfFOgDkFkrPGjQnxFtgjOa9DpTG3w4ocwz4K2IkEU01TCJY5f4C8/pDY0hMBMsvB+G3FtB4eCr0Exr1IjKPVP5jBt9BEr7x8zlQqB8VFJjeQZED6PoSx0e7OTKNhrNy/TqTvxbvnjJj22c/mo7WSnu9OW4pBVMrcZQGA78H7+ekYgYFkK2xEB61FHOr7Y4ApS6GWXka2JkjMBjdGEFAAfSxMJRvoHenvRfoBaGv79IEBc9AV6uBQOcx1braxFuYQmFV+hvhhU+KkoAOIq2h3CNHx0rmDdYa5FPX1fQeFrnEDliokhfIoepT3YFktR/O7iUB1cgSsIWQL9hxto0UchcHol+hgAkGbpjmKSoO4YYh33JhBeUBx+PvSioqrY+sLspZL42v79+8/RRZeSNp3rXIQo0Fn7kdY9zZXCQVbie9+zLp/jrHj3cQiePXuKSNggpgMYNJuSUElDwMAkhiGnZLUigvTVAZrOAoN0Kd5bj4ZDEsSGRwH7CoyrdMgjsirQEt3o4dhG0vkav10bCSDqWQv9tAeUJQ62kVQj3LkD0/H1tEg394aeknRoxedU1j4L8PjeOPjKSN0ye7GevOnqGiomOMP/fMmAHCktKJ6Pnr6awhYp7gchjpaIiUQzPVtRd7jGley9ZgrFKClN2CF9GnaVuNEk8qFsEsKisYtoXdypcHffJ2p6vyYNJA2FfuZeanqDKkGTPCxSNexB4O9mSgD/3g6s0YVjgO8jsRWa5UZ4wbsSaKl/9TJx8iPMUOTinYA4j6sMZAHgDkJyaEWTi6Cnn0vNpKdN2waHeN3y7GiIrN/40Z0rVACF6D1Tx4WF4SwazwKOkdNqviHwyqIx59QogD1mUkOGgUGc8/Hg6Q84h/Mup3G7LsNeTNuHnjEKeJf60ezWLe4MTP2UOh5aMWTotEhpT4ehHUF8otQdzMUW1HcaKfSpTRp4hLaSeDS1k1ISgBSBP7FHBoTBEUVUARohc72t8LP9ombB77LVPawNS55KhFUOCdQ3Ar9R2yX+aNboIP/SGiPJG5HR8cxh+yy6jWsW8hq7n5mNgI+I2woRvfFZ8To4m/n7jPChmJ0X3xGjC7+du4+I2woRvfFZ8To4m/n7jPChmJ0X1IZ4fkMcnSr94nOPQnrpJk17DAa2eBPnaKd7b7LFAJFRUUSR8u0xEP6tq2JMgX6h2GI6cHTZ0SmuAC6EAWydE7A83wrmySJgEn0fESwA61I/tNbBOxWThsb4i6JEbA40qIKWSbtuFZE/+kpAsQBMr/b+NsvZAbHjoZ6mGZ3epqlT8wRAWnIRVi6rXM0g2PpsRtLiPscU/qeniJQWlHVrfChbVxJ6isW0IdCPM3WJ5aKQCrWSYxIjex/Zw8BnxHZwzptTj4j0sKTvUCfEdnDOm1OPiPSwpO9QJ8R2cM6bU4+I9LCk71AnxHZwzptTj4j0sKTvUDb1uRFluVVNd/QRfTg/vhNxaDJsLVwCWwqU3Vp7Malif9TXlNThh3eX8IO7B5s73xSqa1lZcd6b8AZifFc8F3hcGMHNihfLbkY29HS8lxquWgr5UDUWIoTEr8DvdbEcLoQJaqoczvCFz6mKMlnGHA4cQmsy1MFTrPTflYc/X3/RE9hA13OaNGgsxYaUy/HUYA/4HyebXOjPHGA/gpsySxF4l4Wjb5Fh1isdF48PZUIbIu/Q9MCL9PtkvHCMRjeV3NN3coln2f6GfJy7Cndijv46rHlXZ02OBjALc/1KuII1biM4gCoJZqqPYtTO9YmZDMp3VYDQJ5A3Edx3seMawbE/zFwQCag8UdKK7s+n+hP79hNvArHjO/HRbwP4LkN5zL+c9zE/gbsNK+y4sLkvIDKinMY9vZ7uroCjP9vbKJ+FekexKUuOxQtECoNBm+w0nnx9JQRKFAfNmdPxGVTz+Os25X4xi58ORCzMEpcNgMnFZ2+sd3Xbon0Tn5xMzwYwQbwgSffiE3SWygZXd+gK3wnwIhdHwo65G+54higX6Gd6LAjr4B/sik/Xg7kc0ToRhfFQz5zcJ/fzlmzZk+K04nGyor84dBQcJW1/gyOD1xJ8Q1KJ8Rx2LD7Ub7342k8eXjNCAKTIJgIwJ6FZHwejDnjG15MsDi/q6wyuMNg7Hl0cV+OATW87jDj34Zfnpk/Z19EN0MXLSY5MvXj2MaP83O0IBrLN0HrNM5hzArkGouTIsY/cDfgVyC9F+N8NhoGu95MZ+hzuKHPo7tpndKcqZ+nY4RVCFr1QGubhOMG6GcBDTHnjJwkychDi7yOkgM42lJPkpBD35aDtMxAt7QYrfVjZNWG7mUOLtW4A+HDwEJZdOwUp+PAO8FgXEmKQ5dczrZoJT5xoqmaaaZgNWLMeCoe1pUYx6t3zyUCFSPsX8GBlQNowRNRUEvszTIDCDtPOv/QPW0aXR9t+pmyFK8ZneBEK3wZdNabUNBZaqbchOAPUnsd9P/Lccx0LOg0gAG3UwvG4cWvm/dvxOlZDxyMIWbGHFPGoqzUVOxu0goynwwNIebG1Zr3PCWFevphg+IV1RgjlL0YuBeigofN70TijOEsggn5NLTIm8s6O28F0OapUiyGHEmMiqOg0X3h5nXQYu7EaZ4luLfvMTAjSYrRj09Fb780JizsMpyS2wHqdOgkH/rR7Un08AEGTwODLsI95D9AnCspHUB4JTVe/Pt1hENRYhVHj/VCCai8gg6YIP0tLvHP2NtTRqBxmY0XI3EBqZbM0BeiZEnMyFGVBlSujfpznGfejgF5G/rpHNS3Eceo3qaaoI3GhcA8VqW0t4R+TBfsTschRDTfWB7xKgs1sAy0JoPp5ukiCOS5iHCCAEZplkAr+kw8Kq7xM5WEFRi3fq+q/G4wBQf15L+fO2HCLjOOTTuWB1Yrfwu6D5N04kD+jYjeQKd8oNH9lBpRnK4nD08ZgYoTGBFUztRo6H8+waVVC9F3H4a/CSBd84lrGzBQyldRydMIO4Wzdbu4wb9l3WoJTpCGRbRimhZeEpyZB1p9lNRZ3AKwFO+DYN6tEZUXD6r8Qi6MMrDzFQy0+fgPPJbF08bKFqOJc3TyEPLdDE4so8sWKQ7o0NOKZ3qhMd2BeKupq0WaQZS3B98vYDryHkXwyiWJ+dkShTbxPaHyMRpj9qWGNLkrLS2dYxQU2KC2tTXRuvjVdOUnxF7Syf/EvJnQNwnJt+N04clEf1wlOogWvkCqMjdSkNsdOXZMx/UR1zA9KtpbWzsR1z4bh3Nx17F8dYoqofHAQT6uUw1IIXCOci5wKv9IqDl0PJF+VNO24xT4c1Ll5jWnFBY/x7cJ57wf5Pn5U6G99e1vahrJibPELFzf7Q1mpNphZ8FZH2Z0zckPSEIg9TCjt11TUlb+x0gQ8BkxErQyGNdnRAbBHQlpnxEjQSuDcX1GZBDckZD2GTEStDIY12dEBsEdCWmfESNBK4NxfUZkENyRkE5lBNlZfJcdBJKwtm1NMGiR1fMLmHonLbpkp0z//3IB1nPJymg529ZEHjDAmT8r0H9mDgGz4aPxW86WCPJIDbQi+c/MI5A6RmQ+Rz8HRwT+D40An1MVO5YeAAAAAElFTkSuQmCC";
        async.waterfall([
          function(callback){
            models.sequelize.query("select name from users where id = "+userId, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (user) {
                runsheet_by = runsheet_by + user.name + "\n\n";
                callback(null)
              });
          },
          function(callback){
            processConsignManifest(trip.consignments, function(result){
              callback(null, result)
            })
          },
          function(tableContent, callback){
            console.log(" ManifestArr === ", JSON.stringify(tableContent));

            if(!fs.existsSync(file_base_path)){
              fs.mkdirSync(file_base_path)
            }
            const content_data = [
              {
                table: {
                  widths: ['*', 'auto'],
                  body: [
                    [
                      {
                        "text": "CONSIGNMENT MANIFEST",
                        "style": 'docTitle',
                      },
                      {
                        "image": nu_logo,
                        "style": "logo",
                        "height": 50,
                        "width": 49
                      }
                    ]
                  ]
                },
                layout: 'noBorders'
              },
              {
                "text": companyName,
                "style": "heading-1"
              }, {
                "text": runsheet_by
              },
              {
                "columns": [
                  {
                    "table": {
                      "widths": [33, 100],
                      "fontSize": 10,
                      "body": [
                        [{"text": "Trip:", "style": "summary-title"}, {"text": trip.id, "style": "summary-value"}],
                        [{"text": "From:", "style": "summary-title"}, {"text": trip.source, "style": "summary-value"}],
                        [{"text": "To:", "style": "summary-title"}, {
                          "text": trip.destination,
                          "style": "summary-value"
                        }],
                        [{"text": "Asset:", "style": "summary-title"}, {
                          "text": trip.lic_plate_no,
                          "style": "summary-value"
                        }],
                        [{
                          "text": "Driver:",
                          "style": "summary-title"
                        }, {"text": trip.driver_firstname + " " + trip.driver_lastname, "style": "summary-value"}]

                      ]
                    },
                    layout: 'noBorders'
                  },
                  {
                    "table": {
                      "widths": [90, 70],
                      "fontSize": 10,
                      "body": [
                        [{"text": "Total CN note:", "style": "summary-title"}, {
                          "text": trip.consignments_count,
                          "style": "summary-value"
                        }],
                        [{"text": "Total weight:", "style": "summary-title"}, {
                          "text": trip.total_weight,
                          "style": "summary-value"
                        }],
                        [{"text": "Total package:", "style": "summary-title"}, {
                          "text": trip.total_packages,
                          "style": "summary-value"
                        }]
                      ]
                    },
                    layout: 'noBorders'
                  },
                  {
                    "table": {
                      "widths": [60, 100],
                      "fontSize": 10,
                      "body": [
                        [{"text": "Date:", "style": "summary-title"}, {
                          "text": moment().format('D MMM YYYY'),
                          "style": "summary-value"
                        }],
                      ]
                    },
                    layout: 'noBorders'
                  }
                ]
              },
              {
                "text": '\n',
              },
              {
                style: 'docHeadTable',
                table: {
                  widths: [30, 45, '*', 70, 'auto', 'auto', 'auto', 'auto', 40],
                  body: tableContent
                }
              }

            ];
            callback(null, content_data)
          }
        ], function(err, result){
          const docDefinition = {
            content: result,
            styles: {
              "docTitle": {
                "border": false,
                "fontSize": 18,
                "bold": true,
                "fontWeight": "bold",
                "margin": [150, 10, 0, 0]
              },
              "logo": {"alignment": "right"},
              "docHeadTable": {"color": "#4A4A4A"},
              "summary-title": {"bold": true, "fontSize": 10},
              "summary-value": {"fontSize": 10},
              "heading-1": {"bold": true, "fontSize": 16, "fontWeight": 600},
              "heading-2": {"bold": true, "fontSize": 16, "fontWeight": 600},
              "tableHeader": {"fontSize": 9, "bold": true, "color": "#000000", "margin": [5, 10, 0, 0]},
              "tableBody": {"fontSize": 10, "color": "#000000", "margin": [5, 10, 10, 10]}
            },
            defaultStyle: {
              font: 'Proxima_Nova'
            }
          };

          const file_name = ("CONSIGNMENT-MANIFEST" + "-" + utils.downloadReportDate()).toString().toUpperCase() + ".pdf";
          const file_path = file_base_path + file_name;

          res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
          res.setHeader("Content-Disposition","attachment; filename=" + file_name);
          res.setHeader("Content-Type","application/pdf");
          res.setHeader("Filename", file_name);
          const pdfKitDoc = printer.createPdfKitDocument(docDefinition);
          const fileStream = fs.createWriteStream(file_path);

          pdfKitDoc.pipe(fileStream);
          pdfKitDoc.end();
          
          fileStream.on('close', function(){
            res.download(file_path, file_name,function (err) {
              if (!err) {
                fs.unlink(file_path)
              }
              else {
                console.log("Error downloading pdf === ",err)
              }
            })   
          })
                        
        })
      }else{
        res.status(404).send()
      }
    }).catch(function (err) {
      console.log(err);
      res.status(500).send()
    })
};

/**
 * Updated to get trips created by owner or asset owner 
 * 20/3/18 - Updated to show trips which has assets or consignment linked to a trip
 * @param {*} req 
 * @param {*} res 
 */
exports.getAllTripsv2 = function (req, res) {

  const userFeatureList = JSON.parse(req.headers['feature-list']);
  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const search_keyword = req.query.q ? req.query.q.toLowerCase() : '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const trip_status = req.query.status;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const search_cols = req.query.search;
  let search_col_condition = "";
  const sort = req.query.sort;
  const output_format = req.query.format || 'list';
  let sort_condition = " ORDER BY tp.start_tis DESC ";
  const range_cols = req.query.range_filter;
  let range_filter_condition = "";
  let status_filter = req.query.status_filter;
  let status_filter_condition = "";
  const fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.licNo'
    }, {
      db: 'c.cname',
      client: 'asset.operator'
    }, {
      db: 'tp.id',
      client: 'tripId'
    }, {
      db: 'c.transporter_code',
      client: 'transporter.code'
    }]
  );

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.licNo'
    }, {
      db: 'c.cname',
      client: 'asset.operator'
    }, {
      db: 'c.transporter_code',
      client: 'transporter.code'
    }],
    sort_condition
  );

  range_filter_condition = utils.processTableDateRangeFilter(
    range_cols,
    [{
      db: "(tp.trip_log->>'departure')::INT",
      client: 'source.tis'
    },{
      db: "(tp.trip_log->>'arrival')::INT",
      client: 'destination.tis'
    },{
      db: "tp.start_tis",
      client: 'source.est_tis'
    },{
      db: "tp.end_tis",
      client: 'destination.est_tis'
    },{
      db: 'EXTRACT(EPOCH FROM tp."createdAt")::INT',
      client: 'created_tis'
    }],
    800 // Approx 2 years data
  );
  range_filter_condition = range_filter_condition.result;

  if (TRIP_STATUS.indexOf(trip_status) === -1) {
    return res.status(400).send("Invalid trip status")
  }

  let trip_status_condition = trip_status === 'all' ? "" : ` AND tp.status = '${trip_status}'`;

  if(status_filter && trip_status === 'all'){
    status_filter = typeof  status_filter === 'string' ? [status_filter] : status_filter;
    status_filter = _.intersection(status_filter, _.pull(_.clone(TRIP_STATUS), 'all'));

    status_filter && status_filter.forEach(function (i) {
      if(status_filter_condition.length > 0){
        status_filter_condition += ` OR tp.status = '${i}'`
      }else{
        status_filter_condition += ` tp.status = '${i}'`
      }
    });
    status_filter_condition = status_filter && status_filter_condition ? ` AND ( ${status_filter_condition} )` : ""
  }

  let searchCondition = "";
  if(search_keyword.length > 2 ){
    searchCondition = ` AND (
                            lower(a.lic_plate_no)               like $search_keyword OR 
                            lower(tp.*::text)                   like $search_keyword OR 
                            lower(c.cname)                      like $search_keyword OR 
                            lower(c.transporter_code)           like $search_keyword OR
                            CAST(ext_consignment_no AS TEXT)    like $search_keyword
                          )`
  }

  let tripOwnersCondition = `AND (a.fk_c_id = ${fk_c_id} or tp.fk_c_id = ${fk_c_id})`;
  let limit_condition = output_format === 'list' ? ` LIMIT ${limit} OFFSET ${offset}` : "";
  let total_count_condition = " count(tp.id) OVER()::INT ";

  if((trip_status === 'completed' || trip_status === 'open' || trip_status === 'all') && !search_keyword && !search_col_condition && !range_filter_condition){
    total_count_condition = limit;
    if(output_format === 'xls'){
      limit_condition = ` LIMIT ${limit}`
    }
  }

  let query =
        `WITH my_assets as ( 
        SELECT DISTINCT a.id from groups g
        INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
        INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
        WHERE a.active and g.fk_c_id = ${fk_c_id} and ${groupCondition}
    )
    
    SELECT 
        tp.id,
        tp.tid as "tripId", 
        tp.ext_tid as external_trip_id, 
        tp.fk_c_id,
        json_build_object('operator', json_build_object('name', c.cname), 'licNo', a.lic_plate_no, 'id', a.id, 'type', ast.type) as asset,
        tp.status, 
        tp.waypoints, 
        tp.trip_log, 
        tp.start_tis, 
        tp.end_tis, 
        tp.shipment_no, 
        tp.est_distance, 
        tp.est_time,
        tp.trip_log->'distance' actual_distance,
        tp.trip_log->'departure' tp_start_tis,
        tp.trip_log->'arrival' tp_end_tis,
        tp.trip_log->'time_taken' actual_duration,
        (
            SELECT CASE WHEN count(tul.id) > 0 THEN true ELSE false END
            FROM trip_user_logs tul where tul.fk_trip_id = tp.id AND tul.active
        ) as status_updated_manually,
        json_build_object('consumed', tf.consumed, 'mileage', tf.mileage, 'refuels', tf.total_refuels,'drops', tf.total_drops) as fuel,
        json_build_object('condition',tfc.condition,'description',tfc.description) as failure_condition,
        (
            SELECT array(
                SELECT json_build_object('id',tc.id,'consignment_no',tc.ext_consignment_no) 
                FROM consignments tc where tc.fk_trip_id = tp.id
            )
        )as consignment,
        tp.oth_details->>'drivers' as drivers,
        EXTRACT(EPOCH FROM tp."createdAt")::INT created_tis,
        ${total_count_condition} AS total_count
    FROM trip_plannings tp
    INNER JOIN assets a on a.id = tp.fk_asset_id
    LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id
    INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected
    INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected
    LEFT JOIN companies c on c.id = a.fk_c_id
    LEFT JOIN trip_fuels tf on tf.fk_trip_id = tp.id
    LEFT JOIN trip_failure_conditions tfc on tfc.id = tp.fk_trip_failure_condition_id
    LEFT JOIN consignments con on tp.id = con.fk_trip_id
    WHERE tp.new AND tp.active AND ${groupCondition}
    ${
  tripOwnersCondition +
        searchCondition +
        search_col_condition +
        trip_status_condition +
        range_filter_condition +
        status_filter_condition
}
    GROUP BY tp.id, c.id, a.id, ast.id, tf.id, tfc.id, con.fk_trip_id
    ${sort_condition + limit_condition}`;

  models.sequelize.query(query, {
    bind:{
      search_keyword: `%${search_keyword}%`
    },
    type: models.sequelize.QueryTypes.SELECT})
    .then(function (trips) {
      let total_count = 0;
      trips.forEach(function (i) {
        try {
          i.shared = i.fk_c_id !== fk_c_id;
          total_count = i.total_count;
          i.eta = null;
          i.fuel = auth.FEATURE_LIST.hasFuelSensor(userFeatureList) ? utils.processNullObjectKeys(i.fuel) : null;
          i.progress = i.trip_log ? Math.round(i.trip_log.progress) : 0;
          i.totalWaypoints = i.waypoints.length || 0;
          let start_wp = i.waypoints[0];
          i.source = {
            lname: start_wp.lname,
            est_tis: i.start_tis,
            tis: null
          };
          let end_wp = i.waypoints[i.waypoints.length - 1];
          i.destination = {
            lname: end_wp.lname,
            est_tis: i.end_tis,
            tis: null
          };

          let tlog = i.trip_log;
          if (tlog && tlog.violations) {
            i.totalViolations = tlog.violations.length
          } else {
            i.totalViolations = 0
          }

          i.avg_spd = 0;
          i.stops = 0;
          i.distance_planned = i.est_distance;
          i.distance_actual = 0;
          i.duration_planned = Number(i.est_time)/60;
          i.duration_actual = 0;
          if (tlog) {
            i.avg_spd = tlog.AvgSpeed;
            i.stops = tlog.stops;
            i.distance_actual = Number(tlog.distance); // in metres
            i.duration_actual = moment.unix(tlog.arrival).diff(moment.unix(tlog.departure), 'minutes')
          }

          if (tlog && i.status === 'inprogress') {
            i.eta = {
              tis: i.end_tis + (tlog.delayed_by || 0),
              duration: Math.round(tlog.delayed_by / 60) || 0,
              delayed: tlog.delayed || false
            };
            i.source.tis = tlog.departure
          }

          if (tlog && i.status === 'completed') {
            i.source.tis = tlog.departure;
            i.destination.tis = tlog.arrival
          }

          i.tripLoadStatus = i.status === 'inprogress' || i.status === 'scheduled' ? 'Loaded' : 'Unloaded';
          i.actual_distance = Number(i.actual_distance);
          i.actual_duration = Math.round(i.actual_duration / 60);
          i.failure_condition = utils.processNullObjectKeys(i.failure_condition);
          i.drivers = utils.cleanDriverObject(i.drivers);

          i.total_count = undefined;
          i.waypoints = undefined;
          i.trip_log = undefined;
          i.start_tis = undefined;
          i.end_tis = undefined;
          i.est_distance = undefined;
          i.est_time = undefined;
          delete i.tp_start_tis;
          delete i.tp_end_tis;
          delete i.fk_c_id;

        } catch (e) {
          console.log(e)
        }
      });

      if (output_format === 'xls') {
        const file_name = ("TRIPS-LIST-" + _.kebabCase(trip_status.toUpperCase()) + "-" + utils.downloadReportDate()).toString().toUpperCase() + ".xls";
        const file_path = file_base_path + file_name;

        tableConfigsHelper.getTableConfig('trip-list', fk_u_id, trip_status, default_columns, function (err, table_config) {

          convertTripListToXLSV1(trip_status, trips, file_name, table_config, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })
        })

      } else {
        res.json({
          count: total_count,
          data: trips
        });
      }
    }).catch(function () {
      res.status(500).send()
    })
};

/* START: code from old trip controllers */
exports.getSharedTripUsers = function (req, res) {
  let trip_id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;

  if(!trip_id){
    res.status(400).send("Enter trip id")
  }

  const query = "SELECT shared_with FROM trip_plannings WHERE id = " + trip_id + " AND fk_c_id = " + fk_c_id + " LIMIT 1";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (trip) {
      if(!trip){
        return res.status(404).send('Trip not found with ID : '+ trip_id)
      }else{
        res.json(_.filter(trip.shared_with, function (o) {
          return o.active !== false
        }))
      }
    }).catch(function () {
      res.status(500).send()
    })
};

exports.updateSharedTripUsers = function (req, res) {
  let trip_id = req.body.id;
  const fk_c_id = req.headers.fk_c_id;
  const shared_with = req.body.shared_with;

  if(!trip_id){
    res.status(400).send("Enter trip id")
  }

  async.auto({
    trip: function (cb) {
      const query = "SELECT id, tid FROM trip_plannings WHERE id = " + trip_id + " AND fk_c_id = " + fk_c_id + " LIMIT 1";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (trip) {
          if(!trip){
            return cb({code: 404, msg: 'Trip not found with ID : '+ trip_id})
          }else{
            processTripSharingWithExternalUsers({
              fk_trip_id: trip.id,
              tid: trip.tid,
              fk_u_id: req.headers.fk_u_id,
              fk_c_id: fk_c_id,
              shared_with: shared_with
            }, function () {
              cb(null, trip)
            })
          }
        }).catch(function (err) {
          return cb({code: 500, msg: 'Internal error', err: err})
        })
    },
    update_trip: ['trip', function (results, cb) {

      models.trip_planning.update({
        shared_with: shared_with
      },{
        where: {
          id: trip_id,
          fk_c_id: fk_c_id
        }
      }).spread(function () {
        cb(null, null)
      }).catch(function (err) {
        cb({code: 500, msg: 'Internal error', err: err})
      })
    }],
    shareWithExternalUsers: ['trip', function(results, cb){
      const trip = results.trip;
      processTripSharingWithExternalUsers({
        fk_trip_id: trip.id,
        tid: trip.tid,
        fk_u_id: req.headers.fk_u_id,
        fk_c_id: fk_c_id,
        shared_with: shared_with || []
      }, cb)
    }]
  }, function(err){
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.status(201).send("Sucessfully updated")
    }
  })
};

exports.getTripRoute = function (req, res) {

  let points = req.query.point;
  let parameters = '';
  let waypoints = '';

  _.each(points, function (point, index){
    if (index === 0) {
      parameters += ('&origin=' + point)
    } else if (index === points.length - 1) {
      parameters += ('&destination=' + point)
    } else {
      if (waypoints !== '') {
        waypoints += ('|' + point)
      } else {
        waypoints = '&waypoints=' + point
      }
    }
  });

  let options = {
    uri : 'https://maps.googleapis.com/maps/api/directions/json?key=' + config.googleMap.key + '&units=metric' + parameters + waypoints,
    method : "GET"
  };

  request(options, function(error, response, body){

    if (error || response.statusCode !== 200) {
      return res.status(response.statusCode).send(JSON.parse(body))
    }

    let googleResponse = JSON.parse(body);

    let finalResponse = {
      hints: {
        'visited_nodes.average': '',
        'visited_nodes.sum': ''
      },
      paths: [],
      info: {
        took: 'trip-planning',
        copyrights: ''
      }
    };

    if (googleResponse.routes.length > 0) {
      let route = googleResponse.routes[0];
      let distance = 0, time = 0;

      route.legs && route.legs.forEach(function (i) {
        distance += Number(i.distance.value);
        time += Number(i.duration.value)
      });

      finalResponse.paths = [{
        distance: distance,
        time: time * 1000,
        points: route.overview_polyline.points,
        weight: time
      }]
    }
    res.json(finalResponse)
  })
};

// Public APIS

exports.getAllSharedTrips = function (req, res) {

  const TRIP_STATUS = ['inprogress','completed'];
  let email = req.query.email;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  let tripStatus = req.query.status;
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  let timeFrameCondition = "";
  const offset = limit * page;

  if(!email){
    return res.status(400).send("Enter email")
  }

  if(!tripStatus){
    return res.status(400).send("Enter status")
  }

  if(TRIP_STATUS.indexOf(tripStatus) === -1){
    return res.status(400).send("Invalid status")
  }


  if(tripStatus === 'completed'){
    timeFrameCondition = ` AND to_json(trip_log->'arrival')::TEXT::INT >= ${moment().tz(utils.timezone.default, false).subtract(1, 'days').unix()}`;
  }

  const searchCondition = searchKeyword.length > 2 ? ` AND lower(a.lic_plate_no) like '%${searchKeyword}%'` : "";

  const query =
    ` SELECT tp.id, tp.waypoints,tp.tid as "tripId",
      json_build_object('operator', c.cname, 'licNo', a.lic_plate_no,'id', a.id) as asset,
      json_build_object('name',c.cname,'code',c.transporter_code) as transporter,
      tp.status, tp.waypoints, tp.trip_log, tp.end_tis,
      count(tp.id) OVER()::INT AS total_count
    FROM trip_user_mapping_externals tump
    INNER JOIN user_externals ue ON ue.id = tump.fk_user_external_id
    INNER JOIN trip_plannings tp ON tp.id = tump.fk_trip_id
    LEFT JOIN assets a on a.id = tp.fk_asset_id
    LEFT JOIN companies c on c.id = a.fk_c_id
    WHERE ue.email = '${email}' AND tp.status = '${tripStatus}' ${searchCondition + timeFrameCondition}
    ORDER BY tp.start_tis DESC
    LIMIT ${limit} OFFSET ${offset}`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (trips) {

      let total_count = 0;
      trips.forEach(function (i) {
        try {
          total_count = i.total_count;
          i.eta = null;
          i.progress = i.trip_log ? Math.round(i.trip_log.progress) : 0;
          const start_wp = i.waypoints[0];
          i.source = {
            lname: start_wp.lname,
            tis: start_wp.tis
          };
          const end_wp = i.waypoints[i.waypoints.length - 1];
          i.destination = {
            lname: end_wp.lname,
            tis: end_wp.tis
          };

          const tlog = i.trip_log;
          if (tlog && i.status === 'inprogress') {
            i.eta = {
              tis: i.end_tis + (tlog.delayed_by || 0),
              duration: Math.round(tlog.delayed_by / 60) || 0,
              delayed: tlog.delayed || false
            }
          }

          delete i.total_count;
          delete i.waypoints;
          delete i.trip_log;
          delete i.end_tis

        }catch (e){
          console.log(e)
        }
      });

      res.json({
        count : total_count,
        data : trips
      });
    }).catch(function (err) {
      console.error(err);
      res.status(500).send()
    })
};

exports.getRouteEstimate = function (req, res) {
  const params = req.body;

  if (( !_.isArray(params.waypoints) || params.waypoints.length <= 0)) {
    return res.status(400).send({
      msg: "Invalid or missing  waypoints"
    })
  }

  const options = {
    uri: config.server_address.route_planning() + '/v1/trip/route_planning',
    json: {
      waypoints: params.waypoints,
      type_of_vehicle: params.type_of_vehicle,
      avoid_highways: params.avoid_highways,
      avoid_tollbooths: params.avoid_tollbooths,
      avg_speed: params.avg_speed,
      fuel_efficiency: params.fuel_efficiency || 3.5
    }
  };

  request.post(options, function (error, response, body) {
    if (error) {
      return res.status(500).send(error)
    }

    if(body.routes && body.routes.length > 0) {
      body.routes.forEach(function (i) {
        i.fuel_cost = Number((i.fuel_cost).toFixed(2));
        i.toll_cost = Number((i.toll_cost).toFixed(2));
        i.tolls = i.tollbooths.length + " tolls";
        i.duration_format = secondsToString(i.duration);
        i.distance_format = formatNumber(i.distance / 1000) + " km ";
        i.fastag_savings = params.fastag_savings ? Number((i.toll_cost - ( i.toll_cost * (0.75))).toFixed(2)) : 0;
        i.total_cost = (Number(i.fuel_cost + i.toll_cost) - Number(i.fastag_savings)).toFixed(2);

        i.fuel_cost = "₹" + formatNumber(i.fuel_cost);
        i.toll_cost = "₹" + formatNumber(i.toll_cost);
        i.fastag_savings = "₹" + formatNumber(i.fastag_savings);
        i.total_cost = "₹" + formatNumber(i.total_cost)
      });
      body.template = {
        duration_format: "Est time",
        distance_format: "Distance",
        tolls: "Tolls",
        toll_cost: "Toll cost",
        fastag_savings: "FASTag savings",
        fuel_cost: "Fuel cost",
        total_cost: "Total cost"
      };
      return res.status(200).send(body)
    } else {
      return res.status(200).send({routes: [], msg: 'No routes found'})
    }

        
  })
};

function secondsToString(seconds) {

  const numdays = Math.floor(seconds / 86400);
  const numhours = Math.floor((seconds % 86400) / 3600);
  const numminutes = Math.floor(((seconds % 86400) % 3600) / 60);
  let result = "";
  result = numdays > 0 ? numdays + " days, " : result;
  result += numhours > 0 ? numhours + " hours, " : result;
  result += numminutes > 0 ? numminutes + " minutes, " : result;

  result = result.slice(0, -2);

  return result;

}

function formatNumber(num) {
  let n1, n2;
  num = num + '' || '';
  // works for integer and floating as well
  n1 = num.split('.');
  n2 = n1[1] || null;
  n1 = n1[0].replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
  num = n2 ? n1 + '.' + n2 : n1;
  return num;
}
/* END: code from old trip controllers */

/**
 * This api gets count of unplanned stops in a trip for a given time
 * It is used in dashboard apis
 */
function getUnplannedTripStops(params, callback) {
  let processedLog = [];
  const outputObj = {};

  const requiredKeys = ['start_tis', 'end_tis'];
  let dataValid = utils.validateMinPayload(requiredKeys, params);
  if(!dataValid){
    return callback({code: 400, msg: `Mandatory fields : ${requiredKeys}`})
  }

  let query = "WITH my_assets as ( SELECT DISTINCT a.id from groups g " +
    " INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected" +
    " INNER JOIN assets a on a.id = agm.fk_asset_id and a.active" +
    " WHERE a.active and g.fk_c_id = " + params.fk_c_id + " and " + params.groupCondition +
    " )";
  query = query + 
    `
    SELECT waypoints, trip_log->'trip' stops from trip_plannings tp
    WHERE tp.fk_asset_id IN (SELECT id FROM my_assets) AND status IN ('inprogress','completed') AND CASE WHEN status = 'inprogress' THEN tp.start_tis >= $start_tis ELSE tp.start_tis >= $start_tis AND tp.end_tis <= $end_tis  END
    `;
  models.sequelize.query(query, {
    bind:{
      start_tis: params.start_tis,
      end_tis: params.end_tis
    },type: models.sequelize.QueryTypes.SELECT}).then(function(data){

    async.forEachOf(data, function (j, idx, callback) {
      async.forEachOf(j.stops, function (i, index, cb) {

        const matchedItemIndex = _.findIndex(j.waypoints, function (o) {
          // Check if poi id is same or lat,lon within 50 meters
          return i.Id === o.fk_poi_id || geolib.isPointInCircle({
            latitude: o.lat,
            longitude: o.lon
          }, {
            latitude: i.Lat,
            longitude: i.Lon
          }, 50)
        });

        if(matchedItemIndex > 0) {
          return cb()
        }


        let tag = "";
        const stop_time = Math.round(moment.duration(i.DwellTime, 'seconds').asMinutes());
        if (stop_time >= 10 && stop_time <= 30) {
          tag = ">10 mins"
        } else if (stop_time >= 30 && stop_time <= 60) {
          tag = ">30 mins"
        } else if (stop_time >= 60) {
          tag = ">1 hour"
        }
        if (tag === "") {
          return cb()
        }

        getStateName(i.Lat, i.Lon, function (err, value) {
          if (err || value === "undefined") {
            value = ""
          }
          processedLog.push({
            type: "unplanned",
            dwellDuration: Math.round(moment.duration(i.DwellTime, 'seconds').asMinutes()),
            tag: tag,
            region: value
          });

          cb()
        })
      }, function() {
        callback()
      })
    }, function () {
                
      processedLog = _.countBy(processedLog, 'region');
      _.forOwn(processedLog, function(value, key) {
        if(key !== "") {
          outputObj[key] = value
        }
      });
      callback(null, outputObj)
    })
  }).catch(function (err) {
    console.log(err);
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}

let getStateName = function(lat, lon, cb) {
  const options = {
    method: 'GET',
    url: config.googleMap.url + "latlng=" + lat + ',' + lon + "&key=" + config.googleMap.key
  };
  let state_name = "";
  request(options, function (err, response, body) {
    try {
      body = JSON.parse(body);
      if (err || body.status !== "OK") {
        return cb(null, state_name)
      }

      body.results && body.results.forEach(function (i) {
        i.address_components && i.address_components.forEach(function (j) {
          if(_.includes(j.types,'administrative_area_level_1')){
            state_name = j.long_name;
            return 0
          }
        })
      })
    } catch (e) {
      console.log(e)
    }
    cb(null, state_name)
  });
};

function getViolations(params, callback) {

  let query =
        `WITH my_assets as ( 
            SELECT DISTINCT a.id from groups g 
            INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
            INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
            WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
        )
        
        SELECT trip_log->'violations' as violations 
        FROM trip_plannings tp
        WHERE tp.status NOT IN ('scheduled') 
        AND tp.fk_asset_id IN (SELECT id FROM my_assets)
        AND tp.start_tis BETWEEN ${params.start_tis} AND ${params.end_tis};
        `;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    let violations = [];

    async.each(data, function (i, callback) {
      async.each(i.violations, function (j, cb) {
        if(!j.lat || !j.lon){
          return cb()
        }

        getStateName(j.lat, j.lon, function (err, value) {
          if (err || value === "undefined") {
            value = ""
          }
          violations.push({
            region: value,
            lat: j.lat,
            lon: j.lon
          });
          cb()
        })
      }, function () {
        callback()
      })
    }, function () {
      let regions = _.groupBy(violations, 'region');
      let stats = [];
      _.forEach(regions, function (value, key) {
        console.log(key);
        stats.push({
          region: key,
          count: _.size(value)
        })
      });
      callback(null, stats)
    })
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}

function getTripStatusCounts(params, callback) {
  async.auto({
    completed: function (cb) {
      let query =
                `WITH my_assets as ( 
                SELECT DISTINCT a.id from groups g 
                    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
                    INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
                    WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
                )
                
                SELECT count(tp.id)::INT
                FROM trip_plannings tp
                LEFT JOIN trip_logs tl on tl.fk_trip_id = tp.id
                WHERE tp.status = 'completed' 
                AND tp.fk_asset_id IN (SELECT id FROM my_assets)
                AND ((tl.arrival BETWEEN ${params.start_tis} AND ${params.end_tis})
                OR (tl.departure BETWEEN ${params.start_tis} AND ${params.end_tis}));
                `;
      models.sequelize.query(query, {
        type: models.sequelize.QueryTypes.SELECT
      }).spread(function (data) {
        cb(null, data.count || 0)
      }).catch(function (err) {
        cb(err)
      })
    },
    others: function (cb) {
      let query =
                `WITH my_assets as ( 
                SELECT DISTINCT a.id from groups g 
                    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
                    INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
                    WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
                )
                
                SELECT tp.status, trip_log->'delayed' as delayed
                FROM trip_plannings tp
                WHERE tp.status IN ('scheduled', 'inprogress') 
                AND tp.fk_asset_id IN (SELECT id FROM my_assets);
                `;
      models.sequelize.query(query, {
        type: models.sequelize.QueryTypes.SELECT
      }).then(function (data) {
        let ongoing = 0, on_time = 0, delayed = 0, scheduled = 0;

        data.forEach(function (i) {
          switch(i.status){
          case 'inprogress':
            ongoing++;
            break;
          case 'scheduled':
            scheduled++;
            break;
          }

          if(i.status === 'inprogress' && typeof i.delayed === 'boolean'){
            if(i.delayed){
              delayed++
            }else if(!i.delayed){
              on_time++
            }
          }
        });

        cb(null, {
          ongoing: ongoing,
          on_time: on_time,
          delayed: delayed,
          scheduled: scheduled
        })
      }).catch(function (err) {
        cb(err)
      })
    }
  }, function (err, results) {
    if(err){
      callback({code: 500, msg: 'Internal server error', err: err})
    }{
      let counts = results.others;
      counts.completed = results.completed;
      counts.total = counts.completed + counts.ongoing + counts.scheduled;
      callback(null, counts)
    }
  })
}

function getTripsCreated(params, callback) {
  let query =
        `WITH my_assets as ( 
                SELECT DISTINCT a.id from groups g 
                    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
                    INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
                    WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
                )
                
        SELECT tp.id, extract(epoch from tp."createdAt")::int as created_tis
        FROM trip_plannings tp
        WHERE extract(epoch from tp."createdAt")::int >= ${params.last_7_days_tis}
        AND tp.fk_asset_id IN (SELECT id FROM my_assets);
        `;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    let stats = [];
    data.forEach(function (i) {
      let day = moment.unix(i.created_tis).startOf('day');

      i.day = day.format('D MMM YYYY');
      i.tis = day.unix()
    });
    let days_data = _.groupBy(data, 'day');

    _.forEach(days_data, function (value, key) {
      stats.push({
        day: key,
        tis: value && value.length > 0 ? value[0].tis : null,
        value: _.size(value)
      })
    });

    callback(null, _.sortBy(addNullDates(params, stats), ['tis']))
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}

function getTripsStarted(params, callback) {
  let query =
        `WITH my_assets as ( 
                SELECT DISTINCT a.id from groups g 
                    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
                    INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
                    WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
                )
                
        SELECT tp.id, extract(epoch from tp."createdAt")::int as created_tis
        FROM trip_plannings tp
        LEFT JOIN trip_logs tl on tl.fk_trip_id = tp.id
        WHERE tl.departure >= ${params.last_7_days_tis}
        AND tp.fk_asset_id IN (SELECT id FROM my_assets);
        `;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    let stats = [];
    data.forEach(function (i) {
      let day = moment.unix(i.created_tis).startOf('day');

      i.day = day.format('D MMM YYYY');
      i.tis = day.unix()
    });
    let days_data = _.groupBy(data, 'day');

    _.forEach(days_data, function (value, key) {
      let val = value[0] || null;
      if(val && val.tis >= params.last_7_days_tis && val.tis <= params.end_tis){
        stats.push({
          day: key,
          tis: value && value.length > 0 ? value[0].tis : null,
          value: _.size(value)
        })
      }
    });

    callback(null, _.sortBy(addNullDates(params, stats), ['tis']))
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}

function getOpenTripsCount(params, callback) {
  let query =
        `WITH my_assets as ( 
                SELECT DISTINCT a.id from groups g 
                    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
                    INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
                    WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
                )
                
        SELECT count(tp.id)::INT
        FROM trip_plannings tp
        WHERE tp.status = 'open'
        AND tp.fk_asset_id IN (SELECT id FROM my_assets);
        `;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).spread(function (data) {
    callback(null, data.count || 0)
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}

function getCompletedTripsCount(params, callback) {
  let query =
        `WITH my_assets as ( 
                SELECT DISTINCT a.id from groups g 
                    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
                    INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
                    WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
                )
                
        SELECT count(tp.id)::INT
        FROM trip_plannings tp
        LEFT JOIN trip_logs tl on tl.fk_trip_id = tp.id
        WHERE tp.status = 'completed' 
        AND tp.fk_asset_id IN (SELECT id FROM my_assets)
        AND tl.arrival BETWEEN ${params.start_tis} AND ${params.end_tis};
        `;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).spread(function (data) {
    callback(null, data.count || 0)
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}

function getUnplannedStopsTrends(params, callback) {
  let query =
        `WITH my_assets as ( 
                SELECT DISTINCT a.id from groups g 
                    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
                    INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
                    WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
                )
                
        SELECT tp.waypoints, tp.trip_log->'trip' as log
        FROM trip_plannings tp
        LEFT JOIN trip_logs tl on tl.fk_trip_id = tp.id
        WHERE tp.status in ('inprogress','completed')
        AND tp.fk_asset_id IN (SELECT id FROM my_assets)
        AND tl.arrival BETWEEN ${params.last_7_days_tis} AND ${params.end_tis}
        `;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    let stats = [];
    let items = [];

    data.forEach(function (i) {
      i.log && i.log.forEach(function (log_i) {
        let matchedItemIndex = _.findIndex(i.waypoints, function (o) {
          // Check if poi id is not same or lat,lon within 5 meters
          return log_i.Id !== o.fk_poi_id || !geolib.isPointInCircle({
            latitude: o.lat,
            longitude: o.lon
          }, {
            latitude: log_i.Lat,
            longitude: log_i.Lon
          }, 10)
        });

        if(matchedItemIndex > 0){
          let day = moment.unix(log_i.StartTis).startOf('day');
          let item = {
            day: day.format('D MMM YYYY'),
            tis: day.unix(),
            value: 1
          };
          items.push(item)
        }
      })
    });

    let days_data = _.groupBy(items, 'day');

    _.forEach(days_data, function (value, key) {
      let val = value[0] || null;
      if(val && val.tis >= params.last_7_days_tis && val.tis <= params.end_tis){
        stats.push({
          day: key,
          tis: value && value.length > 0 ? value[0].tis : null,
          value: _.size(value)
        })
      }
    });

    callback(null, _.sortBy(addNullDates(params, stats), ['tis']))
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}

function getTripDistanceTrends(params, callback) {
  let query =
        `WITH my_assets as ( 
                SELECT DISTINCT a.id from groups g 
                    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
                    INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
                    WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
                )
                
        SELECT tl.arrival, tl.distance
        FROM trip_plannings tp
        LEFT JOIN trip_logs tl on tl.fk_trip_id = tp.id
        WHERE tp.status in ('inprogress','completed')
        AND tp.fk_asset_id IN (SELECT id FROM my_assets)
        AND tl.arrival BETWEEN ${params.last_7_days_tis} AND ${params.end_tis}
        `;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    let stats = [];
    let items = [];

    data.forEach(function (i) {
      let day = moment.unix(i.arrival).startOf('day');
      let item = {
        day: day.format('D MMM YYYY'),
        tis: day.unix(),
        distance: Number(i.distance)
      };
      items.push(item)
    });

    let days_data = _.groupBy(items, 'day');

    _.forEach(days_data, function (value, key) {
      let val = value[0] || null;
      if(val && val.tis >= params.last_7_days_tis && val.tis <= params.end_tis){
        stats.push({
          day: key,
          tis: value && value.length > 0 ? value[0].tis : null,
          value: Number((_.sumBy(value, 'distance') / 1000).toFixed(1))
        })
      }
    });

    callback(null, _.sortBy(addNullDates(params, stats), ['tis']))
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}

function getTripCompletedTrends(params, callback) {
  let query =
        `WITH my_assets as ( 
                SELECT DISTINCT a.id from groups g 
                    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
                    INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
                    WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
                )
                
        SELECT tp.id, tl.arrival
        FROM trip_plannings tp
        LEFT JOIN trip_logs tl on tl.fk_trip_id = tp.id
        WHERE tp.status in ('completed')
        AND tp.fk_asset_id IN (SELECT id FROM my_assets)
        AND tl.arrival BETWEEN ${params.last_7_days_tis} AND ${params.end_tis}
        `;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    let stats = [];
    let items = [];

    data.forEach(function (i) {
      let day = moment.unix(i.arrival).startOf('day');
      let item = {
        day: day.format('D MMM YYYY'),
        tis: day.unix(),
        fk_trip_id: i.id
      };
      items.push(item)
    });

    let days_data = _.groupBy(items, 'day');

    _.forEach(days_data, function (value, key) {
      let val = value[0] || null;
      if(val && val.tis >= params.last_7_days_tis && val.tis <= params.end_tis){
        stats.push({
          day: key,
          tis: value && value.length > 0 ? value[0].tis : null,
          value: _.size(value)
        })
      }
    });

    callback(null, _.sortBy(addNullDates(params, stats), ['tis']))
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}


function getDelayedTripsTrends(params, callback) {

  let query =
    `WITH my_assets as ( 
      SELECT DISTINCT a.id from groups g 
        INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
        INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
        WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
      )
                
    SELECT to_char(to_timestamp(start_tis), 'YYYY-MM-DD') AS day, count(id)::INT as value
    FROM trip_plannings tp 
    WHERE tp.trip_log->>'delayed' = 'true' 
    AND status = 'completed'
    AND start_tis BETWEEN ${params.start_tis} AND ${params.end_tis} 
    AND tp.fk_asset_id IN (SELECT id FROM my_assets)
    GROUP BY day
    ORDER BY day ASC
    `;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    callback(null, addNullDates(params, data))
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}

function getEmptyKmsTrends(params, callback) {

  let query =
    `WITH my_assets as ( 
      SELECT DISTINCT a.id from groups g 
        INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
        INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
        WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
      )`
                
  async.parallel({
    trips: function (callback) {
      let q = ` ${query}
      SELECT (tp.trip_log->>'distance')::float * 0.001 distance, tp.start_tis, tp.end_tis  
      FROM trip_plannings tp 
      WHERE tp.status = 'completed' 
      AND tp.start_tis >= $start_tis and tp.end_tis <= $end_tis
      AND tp.fk_asset_id IN (SELECT id FROM my_assets) `

      models.sequelize.query(q, {
        bind: {
          start_tis: params.start_tis,
          end_tis: params.end_tis
        }, type: models.sequelize.QueryTypes.SELECT
      }).then(function (data) {
        callback(null, data)
      }).catch(function (err) {
        callback(err)
      })
    },
    asset_stats: function (callback) {
      let q = `${query}
      SELECT SUM(tot_dist), tis, e_tis
      FROM asset_stats
      WHERE tis >= $start_tis and e_tis <= $end_tis
      AND fk_asset_id IN (SELECT id FROM my_assets) 
      group by tis, e_tis
      order by tis ASC`
      models.sequelize.query(q, {
        bind: {
          start_tis: params.start_tis,
          end_tis: params.end_tis
        }, type: models.sequelize.QueryTypes.SELECT
      }).then(function (data) {
        callback(null, data)
      }).catch(function (err) {
        callback(err)
      })
    },
    current_day_stats: function (callback) {

      if (params.end_tis < moment().tz('Asia/Kolkata').startOf('day').unix()) {
        return callback(null, null)
      }

      let q = `${query}
      SELECT COALESCE(SUM(tot_dist),0) sum
      FROM assets_reports_data
      WHERE start_tis >= $start_tis and end_tis <= $end_tis
      AND fk_asset_id IN (SELECT id FROM my_assets) 
      `
      models.sequelize.query(q, {
        bind: {
          start_tis: moment().tz('Asia/Kolkata').startOf('day').unix(),
          end_tis: moment().tz('Asia/Kolkata').unix()
        }, type: models.sequelize.QueryTypes.SELECT
      }).spread(function (data) {
        data.tis = moment().tz('Asia/Kolkata').startOf('day').unix(),
        data.e_tis = moment().tz('Asia/Kolkata').unix()
        callback(null, data)
      }).catch(function (err) {
        callback(err)
      })
    }
  }, function (err, results) {
    if (err) {
      console.error(err)
      return callback({code: 500, msg: 'Internal error'})
    }

    let data = []
    _.forEach(results.asset_stats, function (i) {
      let tmp = _.filter(results.trips, function (v) {
        return v.start_tis >= i.tis
      })
      if (tmp && tmp.length > 0) {
        let sum = 0
        tmp.forEach(function (e) {
          sum += e.distance ? parseFloat(e.distance) : 0
        })
        data.push({
          value: Math.abs(Number((sum - i.sum).toFixed(2))),
          day: moment.unix(i.tis).tz('Asia/Kolkata').format("D MMM YYYY"),
          tis: i.tis
        })
      } else {
        data.push({
          empty_kms: Math.abs(Number((i.sum).toFixed(2))),
          days: moment.unix(i.tis).tz('Asia/Kolkata').format("D MMM YYYY"),
          tis: i.tis
        })
      }
    })
    callback(null, addNullDates(params, data))
  })
}

function getTripsCost(params, callback) {
  let query =
        `WITH my_assets as ( 
                SELECT DISTINCT a.id from groups g 
                    INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected
                    INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
                    WHERE a.active and g.fk_c_id = ${params.fk_c_id} and ${params.groupCondition}
                )
                
        SELECT tp.id, tp.end_tis, COALESCE(tl.cost,0)::INT as cost
        FROM trip_plannings tp
        LEFT JOIN trip_logs tl on tl.fk_trip_id = tp.id
        WHERE tp.status = 'completed' 
        AND tp.fk_asset_id IN (SELECT id FROM my_assets)
        AND tl.arrival BETWEEN ${params.last_7_days_tis} AND ${params.end_tis};
        `;
  models.sequelize.query(query, {
    type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    let stats = [];
    data.forEach(function (i) {
      let day = moment.unix(i.end_tis).startOf('day');

      i.day = day.format('D MMM YYYY');
      i.tis = day.unix()
    });
    let days_data = _.groupBy(data, 'day');

    _.forEach(days_data, function (value, key) {
      let val = value[0] || null;
      if(val && val.tis >= params.last_7_days_tis && val.tis <= params.end_tis){
        stats.push({
          day: key,
          tis: value && value.length > 0 ? value[0].tis : null,
          value: _.sumBy(value, 'cost')
        })
      }
    });

    callback(null, _.sortBy(addNullDates(params, stats), ['tis']))
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}

function addNullDates(params, data) {
  let currentDate = moment.unix(params.last_7_days_tis), end_date = moment.unix(params.end_tis);
  data = data ? data : [];
  while (currentDate.isBefore(end_date)) {
    let dayStart = currentDate.clone();
    let dayTis = dayStart.startOf('day').unix();
    if(!_.find(data, { tis: dayTis})){
      data.push({
        day: dayStart.format('D MMM YYYY'),
        tis: dayTis,
        value: 0
      })
    }
    currentDate.add(1, 'day');
  }
  return data
}

exports.getAllTripsStats = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const groupCondition = " g.id in " + groupIds;
  const start_tis = req.query.start_tis || moment().tz(utils.timezone.default, false).startOf('day').unix();
  const end_tis = req.query.end_tis || moment().tz(utils.timezone.default, false).endOf('day').unix();
  const last7DaysTis = moment().subtract(7, 'days').startOf('day').unix();
  const fk_asset_id = req.query.fk_asset_id;
  const asset_type = req.query.type;

  async.auto({
    status_counts: function (cb) {
      getTripStatusCounts({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis,
      }, cb)
    },
    violations: function (cb) {
      getViolations({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis,
      }, cb)
    },
    trips_created: function (cb) {
      getTripsCreated({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        last_7_days_tis: last7DaysTis,
        end_tis: end_tis
      }, cb)
    },
    trips_completed: function (cb) {
      getCompletedTripsCount({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis,
      }, cb)
    },
    trips_open: function (cb) {
      getOpenTripsCount({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition
      }, cb)
    },
    trips_started: function (cb) {
      getTripsStarted({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis,
        last_7_days_tis: last7DaysTis
      }, cb)
    },
    trips_cost: function (cb) {
      getTripsCost({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis,
        last_7_days_tis: last7DaysTis
      }, cb)
    },
    unplanned_stops_trends: function (cb) {
      getUnplannedStopsTrends({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis,
        last_7_days_tis: last7DaysTis
      }, cb)
    },
    trip_distance: function (cb) {
      getTripDistanceTrends({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis,
        last_7_days_tis: last7DaysTis
      }, cb)
    },
    trips_completed_trends: function (cb) {
      getTripCompletedTrends({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis,
        last_7_days_tis: last7DaysTis
      }, cb)
    },
    trips_today: function (cb) {
      getTodaysTripStatus({
        fk_c_id: fk_c_id,
        groupCondition: groupCondition,
        start_tis: start_tis,
        fk_asset_id: fk_asset_id,
        type: asset_type,
      }, cb)
    },
    unplanned_stops: function (cb) {
      getUnplannedTripStops({
        fk_c_id: fk_c_id,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis
      }, cb)
    },
    trips_delayed_trends: function (cb) {
      getDelayedTripsTrends({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis,
        last_7_days_tis: last7DaysTis
      }, cb)
    },
    empty_kms_trends: function (cb) {
      getEmptyKmsTrends({
        fk_c_id: fk_c_id,
        groupIds: groupIds,
        groupCondition: groupCondition,
        start_tis: start_tis,
        end_tis: end_tis,
        last_7_days_tis: last7DaysTis
      }, cb)
    }
  }, function (err, results) {
    if(err){
      console.error(err);
      return res.status(err.code).send(err.msg)
    }
    res.json(results)
  })
};

/**
 * Get todays trip stats
 * @param {Integer} req.query.start_tis - Start of todays time
 * @param {Integer} req.query.end_tis - Start of todays time
 */
function getTodaysTripStatus(params, callback) {
  const currentTime = moment().unix();

  const requiredKeys = ['start_tis'];
  let dataValid = utils.validateMinPayload(requiredKeys, params);
  if (!dataValid) {
    return callback({code: 400, msg: `Mandatory fields : ${requiredKeys}`})
  }
  const fk_asset_id = parseInt(params.fk_asset_id) || null;
  const assetCondition = utils.getAssetConditions(fk_asset_id, params.type);

  let query = "WITH my_assets as ( SELECT DISTINCT a.id from groups g " +
    " INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected" +
    " INNER JOIN assets a on a.id = agm.fk_asset_id and a.active" +
    " WHERE a.active and g.fk_c_id = " + params.fk_c_id + " and " + params.groupCondition + assetCondition +
    " )";
  query = query + ` select tp.status, (case when (tp.status = 'completed' or tp.status = 'inprogress') then trip_log->>'delayed'
                            when (tp.status = 'scheduled' and tp.start_tis <= $current_tis) then 'true'
                        end)::boolean as delayed,  count(*)
                        from trip_plannings tp
                        where tp.fk_asset_id in (select id from my_assets) 
                        AND tp.active
                        AND (case when (tp.status = 'completed' or tp.status = 'inprogress' or tp.status = 'open') 
                                        then cast(tp.trip_log->>'arrival' as int) >= $start_tis 
                                  when tp.status = 'scheduled' 
                                        then (tp.start_tis >= $start_tis and tp.start_tis <= $current_tis)
                            end
                            ) 
                        group by tp.status, delayed
            `;
  models.sequelize.query(query, {
    bind: {
      start_tis: params.start_tis,
      current_tis: currentTime
    }, type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    callback(null,data)
  }).catch(function (err) {
    callback({code: 500, msg: 'Internal server error', err: err})
  })
}