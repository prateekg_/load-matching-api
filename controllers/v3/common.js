const models = require('../../db/models/index');
const utils = require('../../helpers/util');
const APP_LOGGER = require('../../helpers/logger');
const LOGGER = APP_LOGGER.logger;
const async = require('async');
const nuRedis = require('../../helpers/nuRedis');
const nuDynamoDb = require('../../helpers/dynamoDb');
const nodemailer = require('nodemailer');
const auth = require(process.cwd() + '/authorization');
const env       = process.env.NODE_ENV || 'development';
const config    = require('../../config/index')[env];
const mailerConfig = config.mailer;
const moment = require('moment');
const AWS = require('aws-sdk');
const s3_config = require('../../config/aws/s3.json');
AWS.config.update(s3_config);
let s3 = new AWS.S3();
const request = require('request');
const _ = require('lodash');
const fs = require('fs');
const file_base_path = __dirname + '/../../temp_reports/';
const S3_ASSET_DOCS_BUCKET = process.env.NODE_ENV === 'production' ? 'nu-platform-docs-prod' : 'nu-platform-docs-dev';
const BASE64_MIME_TYPES = ['image/jpeg','image/png'];
const Excel = require('exceljs');

function convertDoorSensorActivityToXLS(data, filename, callback){
  try {
    let default_cols_header = ['Location','Door open time', 'Door close time', 'Door open duration'];

    let workbook = new Excel.Workbook();
    let ws = workbook.addWorksheet('Door Sensor Activity');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = [
      {width: 30},
      {width: 20},
      {width: 20},
      {width: 15}
    ];

    ws.addRow(default_cols_header);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    data.forEach(function (i) {
      let default_cols_data = [
        i.lname,
        utils.DatetimeNoYearFormat(i.door_open_tis),
        i.door_close_tis ? utils.DatetimeNoYearFormat(i.door_close_tis) : '-',
        i.door_open_duration? utils.FormatMinutesDHM(i.door_open_duration * 60) : '-'
      ];
      ws.addRow(default_cols_data)
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    if(!fs.existsSync(file_base_path)){
      fs.mkdirSync(file_base_path)
    }

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, data)
      }).catch(function (err) {
        callback({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      });
  }catch(e){
    console.log("XLS ERROR : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

exports.searchAssets = function (req, res) {
  const searchKeyword = req.query.q || '';
  const limit = parseInt(req.query.limit) || 50;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const fk_c_id = req.headers.fk_c_id;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  let query = "SELECT a.id, a.name, a.lic_plate_no, json_agg(g.gid) as gids, c.cname as operator, null as lname, aa.axle_count, aa.vehicle_length_cm as vehicle_length" +
        " FROM groups g "+
        " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
        " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
        " LEFT JOIN asset_attributes aa on aa.fk_asset_id = a.id "+
        " LEFT JOIN companies c on c.id = a.fk_c_id "+
        " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id " +
        " WHERE g.active AND "+ groupCondition + " AND g.fk_c_id = " + fk_c_id +
        " AND lower(a.lic_plate_no) ilike '%" + searchKeyword + "%'" +
        " GROUP BY  a.id, c.cname, aa.axle_count, aa.vehicle_length_cm" +
        " LIMIT " + limit + " OFFSET " + offset;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (assets) {
    async.forEachOf(assets, function (asset, i, callback) {
      asset.axle_count = asset.axle_count ? asset.axle_count + " axle" : null;
      asset.vehicle_length = asset.vehicle_length ? parseInt(asset.vehicle_length * 0.0328) + " ft" : null;
      nuRedis.getAssetDetails(asset.id, function (err, value) {
        if(value){
          asset.lname = value.lname
        }
        callback()
      })
    }, function (err) {
      console.error(err);
      res.json(assets)
    });
  }).catch(function (err) {
    console.log(err);
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send('Internal server error')
  })
};

exports.getDeadZones = function (req, res) {

  const type = req.query.type || 'GPRS';
  const tis = req.query.tis || Date.now();

  let query = "SELECT sz.file_meta FROM nu_smart_zones sz " +
                "INNER JOIN nu_smart_zone_types szt ON szt.id = sz.fk_smart_zone_type_id " +
                "WHERE sz.active AND sz.start_tis <= "+ tis + " AND sz.end_tis >= " + tis + " AND szt.type = '"+ type +"' LIMIT 1";
    
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (record) {
    if(!record){
      res.status(404).send("no record found")
    }else{
      let key = record.file_meta && record.file_meta.key;
      if(!key){
        return res.status(404).send('invalid key')
      }else{
        let params = {
          Bucket: record.file_meta.bucket,
          Key: key
        };
            
        s3.getObject(params, function (err, data) {
          if (err) {
            return res.status(500).send('Internal server erorr')
          }

          res.status(200).send({data: data.Body.toString()})
        })
      }
    }
  }).catch(function(err) {
    console.error(err);
    res.status(500).send('Internal error')
  })
};

exports.processWVError = function(req, res){
  let body = req.body;
  if(!body){
    return res.status(400).send('No data in body')
  }

  let transporter = nodemailer.createTransport(mailerConfig);
  let mailOptions = {
    from: 'noreply@numadic.com',
    to: ['sedrick@numadic.com','haston@numadic.com'],
    bcc: 'noreply@numadic.com',
    subject: `[${env.toUpperCase()}] NU-WV-V4 Error - ${moment().format('DDMMYY')} - INT`,
    text: JSON.stringify(body)
  };
  transporter.sendMail(mailOptions, function (error) {
    if(error){
      console.error(error);
      return res.status(500).send("Mail not send")
    }else{
      return res.json("success")
    }
  });
};

exports.getTypes = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const category = req.query.category;
  const type = req.query.type;
  let query = "SELECT id, type";
  let poiCondition = '';

  switch(type) {
  case 'poi':
    poiCondition = "fk_c_id is null";
    if (category === 'company') {
      poiCondition = "fk_c_id = " + fk_c_id
    }
    if (!category) {
      poiCondition = "fk_c_id is null OR fk_c_id = " + fk_c_id
    }
    query += ", CASE WHEN fk_c_id IS NULL THEN 'general' ELSE 'company' END as category FROM poi_types where " + poiCondition;
    break;
  case 'notification':
    query += " FROM notification_types WHERE type not ilike 'RO%'";
    break;
  case 'user':
    query += " FROM user_types";
    break;
  case 'licence':
    query += " FROM driver_license_types WHERE active";
    break;
  case 'smart_notification':
    query += " FROM smart_notification_types";
    break;
  case 'maintenance':
    query += " FROM maintenance_types where fk_c_id is null or fk_c_id = " + fk_c_id;
    break;
  default:
    return res.status(400).send('Please specify a valid type');
  }

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
    .then(function (types) {
      res.json(types);
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send('Internal server error');
    })
};

exports.getAssetSensorDetails = function(req, res){
  const asset_id = req.query.id;
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis;
  const type = req.query.type;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const limit = req.query.limit || 10;
  const offset = (req.query.page || 0 ) * limit;
  const output_format = req.query.format;

  let required_keys = ['id','start_tis', 'end_tis'];
  let data_valid = utils.validateMinPayload(required_keys, req.query);
  if(!data_valid){
    res.status(400).send("Mandatory fields : "+required_keys)
  }

  let query = `SELECT a.id, aa.sensor_settings->>'temperature_sensor' as  temperature_sensor, st.id as sensor_type_id, 
                  st.sensor_type
              FROM groups g `+
              " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
              " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
              " LEFT JOIN asset_attributes aa on a.id = aa.fk_asset_id"+
              ` INNER JOIN entity_mappings em on a.id = em.fk_asset_id and em.connected
              LEFT JOIN entity_sensor_mappings esm on em.fk_entity_id = esm.fk_entity_id
              INNER JOIN sensors s on esm.fk_sensor_id = s.id
              INNER JOIN nubot_sensor_types st on s.fk_sensor_type = st.id `+
              " WHERE g.active AND g.id in "+ groupIds + " AND a.id = "+ asset_id +" LIMIT 1";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (assetData) {
      if(assetData[0] && assetData[0].id && output_format === 'xls'){
        let request_options = {
          url: config.server_address.temperature_sensor_log() + '/v1/templog',
          method: 'POST',
          json: true,
          body:{
            fk_asset_id: Number(asset_id),
            stis: Number(start_tis),
            etis: Number(end_tis)
          }
        };
        return request(request_options).pipe(res);
      }

      if(assetData[0] && assetData[0].id){
        let current_threshold = JSON.parse(assetData[0].temperature_sensor);
        let null_threshold = _.clone(current_threshold);
        for (let key in null_threshold) {
          null_threshold[key] = null;
        }
        let sensor_type_ids = _.map(assetData, 'sensor_type_id');
              
        //Dev environment gets data from db whereas production directly fetches from dynamo db
        //nubot_sensor_data in postgres has only one month backup data
        async.auto({
          getSensorData: function(callback){
            if(env === "development"){
              let temperature_data = [];
              async.parallel({
                minTemperature: function(pcallback){
                  let query = `Select min((((nsd.data->'data')::json->0->'value')->>'temperature')::numeric) AS minv
                                                  from nubot_sensor_data nsd 
                                                  where fk_nubot_sensor_type IN (`+ sensor_type_ids +`) and fk_asset_id = ` + asset_id +`   
                                                      and tis between ` + start_tis +` and ` + end_tis;

                  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                    .spread(function (sensorData) {
                      pcallback(null, sensorData.minv)
                    });

                },
                maxTemperature: function(pcallback){
                  let query = `Select max((((nsd.data->'data')::json->0->'value')->>'temperature')::numeric) AS maxv
                                                  from nubot_sensor_data nsd 
                                                  where fk_nubot_sensor_type IN (`+ sensor_type_ids +`) and fk_asset_id = ` + asset_id +`   
                                                      and tis between ` + start_tis +` and ` + end_tis;

                  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                    .spread(function (sensorData) {
                      pcallback(null, sensorData.maxv)
                    });

                }, 
                temperatureData: function(pcallback){
                  let query = "select * from nubot_sensor_data nsd "+
                                      `where fk_nubot_sensor_type IN (`+ sensor_type_ids +`) and fk_asset_id = `+ asset_id +
                                      "   and tis between "+ start_tis + " and "+ end_tis + 
                                      " order by tis desc "+
                                      " limit "+ limit + " offset "+offset; //by default it will take only 10

                  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                    .then(function (sensorData) {
                                      
                      async.each(sensorData, function(sensor, callback){                                    
                        let temperature_sensor_values = sensor.data.data;

                        let query = `select new_temperature_values 
                                                      from temperature_sensor_logs 
                                                      where tis <= `+ sensor.tis +
                                                      ` and fk_asset_id = `+ asset_id + ` order by "createdAt" desc limit 1`;
                        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                          .spread(function (thresholdData) {
                            // If there was no threshold set before this temperature value was generated, set the threshold to the null threshold
                            let threshold = thresholdData ? thresholdData.new_temperature_values : null_threshold;
                            let temperature = temperature_sensor_values[0].value.temperature;
                            let temperature_obj = {
                              temperature: temperature, 
                              tis: sensor.tis,
                              location: null,
                              threshold: threshold
                            };

                            temperature_data.push(temperature_obj);
                            callback()

                          });
                      }, function(){
                        let output = {
                          data: temperature_data,
                          threshold: current_threshold
                        };
                        pcallback(null, output)
                      })
                    });
                }
              }, function(err, results){
                console.error(err);
                results.temperatureData.min_temperature = parseFloat(results.minTemperature);
                results.temperatureData.max_temperature = parseFloat(results.maxTemperature);
                callback(null, results.temperatureData)
              })

            }else{
              let dynamo_url_include = "";
              let dynamo_url_exists = "";

              async.each(assetData, function(sensors, callback){
                dynamo_url_include = dynamo_url_include ? dynamo_url_include +"," +"&include=fk_sensor_type_"+sensors.sensor_type_id : "&include=fk_sensor_type_"+sensors.sensor_type_id;
                dynamo_url_exists = dynamo_url_include ? dynamo_url_exists +"," +"&exists=fk_sensor_type_"+sensors.sensor_type_id : "&include=fk_sensor_type_"+sensors.sensor_type_id;
                callback()
              }, function(){

                nuDynamoDb.getLocationData({
                  fk_asset_id: asset_id,
                  start_tis: start_tis,
                  end_tis: end_tis
                }, dynamo_url_include , dynamo_url_exists, function (err, data) {
                                  
                  if(err){
                    LOGGER.error(APP_LOGGER.formatMessage(req, err))
                  }
                  let temperature_data = [];
                  async.each(data, function(value, callback){
                    let temperature_sensor_values = value.fk_sensor_type_7 || value.fk_sensor_type_12;
                    let sensor_id = Object.keys(temperature_sensor_values)[0];
              
                    let query = "select new_temperature_values from temperature_sensor_logs where tis <= "+ value.tis +
                                                  " and fk_asset_id = "+ asset_id + " limit 1";
                    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                      .spread(function (thresholdData) {
                        // If there was no threshold set before this temperature value was generated, set the threshold to the null threshold
                        let threshold = thresholdData ? thresholdData.new_temperature_values : null_threshold;
                        let temperature = parseFloat(temperature_sensor_values[sensor_id].temperature);
                        let temperature_obj = {
                          temperature: temperature, 
                          tis: value.tis,
                          location: value.lname,
                          threshold: threshold
                        };
                        // show only violated info
                        if(type === 'violation'){
                          if(temperature < threshold.min_temperature || temperature > threshold.max_temperature)
                            temperature_data.push(temperature_obj)
                        }
                        else{
                          temperature_data.push(temperature_obj)
                        }
                        callback()
                      });
                  }, function(){
                    let output = {
                      data: temperature_data,
                      min_temperature: temperature_data.length > 0 ? _.min(temperature_data, _.property('temperature')).temperature : null,
                      max_temperature: temperature_data.length > 0 ? _.maxBy(temperature_data, _.property('temperature')).temperature : null,
                      threshold: current_threshold
                    };
                    callback(null, output)
                  })
                                  
                })
              })
            }
          }
        }, function(err, results){
          res.json(results.getSensorData)
        })
      }
      else{
        res.json([])
      } 
    });
};

exports.getTripDoorSensorActivity = function (req, res) {

  const userFeatureList = JSON.parse(req.headers['feature-list']);
  const id = parseInt(req.query.id);
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const output_format = req.query.format;

  if(!auth.FEATURE_LIST.hasDoorSensor(userFeatureList)){
    return res.status(404).send("No access to door sensor")
  }else if(isNaN(id)){
    res.status(400).send('Invalid trip id')
  }else{

    async.auto({
      trip: function (callback) {
        let query = "SELECT tp.fk_asset_id, tp.start_tis, tp.end_tis" +
                    " FROM trip_plannings tp " +
                    " LEFT JOIN assets a on a.id = tp.fk_asset_id" +
                    " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected"+
                    " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected"+
                    " WHERE g.active AND g.fk_c_id = "+ fk_c_id +" AND tp.id = "+ id +" AND " + groupCondition +" LIMIT 1";

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .spread(function (trip) {
            if(!trip){
              callback({ code: 404, msg: 'Record not found'})
            }else{
              callback(null, trip)
            }
          }).catch(function () {
            callback({
              code: 500,
              msg: 'Internal server error'
            })
          })
      },
      door_data: ['trip', function (results, callback) {
        let trip = results.trip;

        let query = "SELECT data->'data' as data, tis" +
                    " FROM nubot_sensor_data" +
                    " WHERE fk_nubot_sensor_type = 1 and fk_asset_id = "+ trip.fk_asset_id + "" +
                    " AND tis BETWEEN "+ trip.start_tis + " AND "+ trip.end_tis;

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .then(function (data) {
            let d = [];
            let z = [];
            data.forEach(function (item){
              d.push({
                tis: item.tis,
                lat: item.data[0].lat,
                lon: item.data[0].lon,
                value: item.data[0].value
              })
            });

            async.eachOfSeries(d, function (item, index, a_callback) {

              if(item.value && item.value.toLowerCase() === 'open'){
                let record = {
                  door_open_tis: item.tis,
                  door_close_tis: null,
                  door_open_duration: null,
                  lat: parseFloat(item.lat),
                  lon: parseFloat(item.lon),
                  lname: null
                };

                let close_event = _.find(d, function (o) {
                  return o.tis >= item.tis && o.value.toLowerCase() === 'closed'
                });

                if(close_event){
                  record.door_close_tis = close_event.tis;
                  record.door_open_duration = Math.round(moment.duration(Math.abs(close_event.tis - item.tis),'seconds').asMinutes())
                }

                z.push(record)
              }

              a_callback()
            }, function (err) {
              if(err){
                callback({
                  code: 500,
                  msg: 'Internal server error'
                })
              }else{
                callback(null, z)
              }
            })

          }).catch(function () {
            callback({
              code: 500,
              msg: 'Internal server error'
            })
          })
      }]
    }, function (err, results) {
      if(err){
        res.status(err.code).send(err.msg)
      }else{
        if(output_format === 'xls'){

          let file_name = ("TRIP-DOOR-ACTIVITY-REPORT-" + id +"-"+ utils.downloadReportDate()).toString().toUpperCase() +".xls";
          let file_path = file_base_path + file_name;
          convertDoorSensorActivityToXLS(results.door_data, file_name, function (err) {
            if(err){
              res.status(err.code).send(err.msg)
            }else {
              res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })
        }else{
          res.json(results.door_data)
        }
      }
    })
  }
};

exports.downloadDocument = function (req, res) {

  const record_id = req.query.id;
  const key = req.query.key;
  const type = req.query.type;

  if(!record_id){
    return res.status(400).send("Enter record id")
  }else if(!key){
    return res.status(400).send("Enter document key")
  }

  let query = "";
  let doc_type = req.query.doc_type;
  let col = (doc_type === 'licence') ? 'dd.lic_meta_files' : (doc_type === 'hazardous') ? 'dd.hazardous_certificate_meta_files' : "";

  switch(type) {
  case 'driver':
    if(!doc_type || !col) {
      return res.status(400).send("Invalid document type")
    }
    query = "SELECT " + col + " FROM driver_details dd WHERE dd.fk_u_id = "+ record_id +" LIMIT 1";
    break;
  case 'vehicle':
    query = "SELECT ad.files_meta FROM asset_documents ad WHERE ad.id = "+ record_id +" LIMIT 1";
    break;
  default:
    return res.status(400).send("Please specify a valid type")
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (record) {
    if(!record){
      res.status(404).send("No record found")
    }else{
      let match = _.find(record.files_meta, {key: key});
      if(!match){
        return res.status(404).send('Invalid key')
      }else{
        let params = {
          Bucket: S3_ASSET_DOCS_BUCKET,
          Key: key
        };

        async.auto({
          temporary_url: function (cb) {
            s3.getSignedUrl('getObject', params, function (err, url) {
              if(err){
                cb(err)
              }else{
                cb(null, url)
              }
            })
          },
          base64: function (cb) {

            if(BASE64_MIME_TYPES.indexOf(match.mimetype) >= 0){
              s3.getObject(params, function (err, data) {
                if(err){
                  cb(err)
                }else{
                  cb(null, data.Body.toString('base64'))
                }
              })
            }else{
              cb(null, null)
            }
          },
          mime_type: function (cb) {
            cb(null, match.mimetype)
          }
        }, function (err, data) {
          if(err){
            LOGGER.error(APP_LOGGER.formatMessage(req, err));
            res.status(500).send()
          }else{
            res.json(data)
          }
        })
      }
    }
  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send()
  })
};