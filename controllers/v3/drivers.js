const moment = require('moment-timezone');
const request = require('request');
const async = require('async');
const _ = require('lodash');
const Excel = require('exceljs');
const geolib = require('geolib');
const AWS = require('aws-sdk');

const models = require('../../db/models/index');
const utils = require('../../helpers/util');
const nuRedis = require('../../helpers/nuRedis');
const nuDynamoDb = require('../../helpers/dynamoDb');
const s3_config = require('../../config/aws/s3.json');
const env       = process.env.NODE_ENV || 'development';
const config    = require(__dirname + '/../../config/index.js')[env];
const tableConfigsHelper = require('../../helpers/table_config');
const xlsDownload = require('../../helpers/downloadHelper');
const file_base_path = __dirname + '/../../temp_reports/';
const fs = require('fs');

const APP_LOGGER = require('../../helpers/logger');
const LOGGER = APP_LOGGER.logger;
const CONSIGNMENT_STATUSES = ['assigned','picked','delivered','incomplete'];

AWS.config.update(s3_config);

function processConsigneeContactNo(data){
  return _.uniq(_.pull(data, 'null', null))
}

exports.getAll = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const driver_asset_status = req.query.isAssigned;
  let searchCondition = "";
  let search_cols = req.query.search;
  let search_col_condition = "";
  let sort = req.query.sort;
  let sort_condition = " ORDER BY u.id DESC ";
  let driver_asset_condition = "";
  const fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';
  let driverMappedStatus = 'all';
  const output_format = req.query.format || 'list';

  if(driver_asset_status === 'true'){
    driver_asset_condition = " AND COALESCE(adm.connected, FALSE) = true";
    driverMappedStatus = 'assigned'
  }else if(driver_asset_status === 'false'){
    driver_asset_condition = " AND COALESCE(adm.connected, FALSE) = false";
    driverMappedStatus = 'unassigned'
  }

  if(searchKeyword.length > 0){
    searchCondition = " AND (lower(a.lic_plate_no) like '%"+ searchKeyword +"%' OR lower(u.lastname) like '%"+ searchKeyword +"%' OR lower(u.firstname) like '%"+ searchKeyword +"%')"
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.lic_plate_no'
    }, {
      db: 'c.cname',
      client: 'asset.operator'
    }, {
      db: 'u.firstname',
      client: 'firstname'
    }, {
      db: 'u.lastname',
      client: 'lastname'
    }, {
      db: 'u.cont_no',
      client: 'phone'
    }]
  )

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'dd.lic_expiry_tis',
      client: 'licExpiryTis'
    }, {
      db: 'dd.lic_issue_tis',
      client: 'licIssueTis'
    }],
    sort_condition
  )

  let query = "SELECT json_build_object('id',a.id,'licNo',a.lic_plate_no,'operator', c.cname, 'type', atp.type) as asset," +
        " u.id, u.firstname, u.lastname, u.firstname || ' ' || u.lastname as fullname, u.cont_no as phone, dd.lic_expiry_tis as \"licExpiryTis\", dd.lic_issue_tis as \"licIssueTis\"," +
        " COALESCE(adm.connected, FALSE) as \"isAssigned\", count(*) OVER()::INT AS total_count FROM users u"+
        " INNER JOIN driver_details dd on dd.fk_u_id = u.id "+
        " LEFT JOIN asset_driver_mappings adm on adm.fk_driver_id = u.id AND adm.connected" +
        " LEFT JOIN assets a on a.id = adm.fk_asset_id AND adm.connected AND a.active" +
        " LEFT JOIN asset_types atp on atp.id = a.fk_ast_type_id"+
        " LEFT JOIN companies c on c.id = a.fk_c_id"+
        " WHERE u.active AND u.fk_c_id = "+ fk_c_id + driver_asset_condition + searchCondition + search_col_condition +
            sort_condition;

  if(output_format === 'list'){
    query += " LIMIT " + limit + " OFFSET " + offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    let total_rows = 0;

    data.forEach(function (i) {
      total_rows = i.total_count;
      i.total_count = undefined
    });

    if (output_format === 'xls') {
      try {
        let file_name = "DRIVER-LIST-" + fk_c_id + "-" + moment().unix() + ".xls";
        let file_path = file_base_path + file_name;

        tableConfigsHelper.getTableConfig('driver-list', fk_u_id, driverMappedStatus, default_columns, function (err, table_config) {
          xlsDownload.convertTableV1ToXLS(driverMappedStatus, data, file_base_path, file_name, table_config, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })

        })
      }catch (e){
        console.log("e: ", e)
      }
    } else {
      res.json({
        total_count: total_rows,
        data: data
      })
    }

  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send()
  })
};

exports.getSummary = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const today_tis = moment().startOf('day').unix();
  const thirty_days_tis = moment().add(30, 'days').unix();

  if(!req.query.id) {
    let query = "SELECT dd.lic_expiry_tis, dd.fitness_certificate_expiry_tis, COALESCE(adm.connected, FALSE) as connected FROM users u" +
            " INNER JOIN user_types ut on ut.id = u.fk_user_type_id AND ut.type = 'driver'" +
            " LEFT JOIN driver_details dd on dd.fk_u_id = u.id " +
            " LEFT JOIN asset_driver_mappings adm on adm.fk_driver_id = u.id AND adm.connected" +
            " WHERE u.active AND u.fk_c_id = " + fk_c_id;

    models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT }).then(function (data) {
      let total = data.length;
      let totalUnassigned = _.size(_.filter(data, { connected: false }));
      res.json({
        totalDrivers: total,
        totalAssigned: Math.abs(total - totalUnassigned),
        totalUnassigned: totalUnassigned,
        driversAssigned: Math.round((total - totalUnassigned) / total * 100) || 0,
        totalLicenseExpiring: _.size(_.filter(data, function (o) {
          return (o.lic_expiry_tis >= today_tis && o.lic_expiry_tis <= thirty_days_tis)
        })),
        totalMedicalDue: _.size(_.filter(data, function (o) {
          return (o.fitness_certificate_expiry_tis >= today_tis && o.fitness_certificate_expiry_tis <= thirty_days_tis)
        }))

      })
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send()
    })
  } else {
    //Get summary for a driver
    let user_id = req.query.id;
    let query = `SELECT u.name, u.cont_no, u.sec_cont_no, json_build_object('id', adm.fk_asset_id, 'licNo', a.lic_plate_no, 'operator', c.cname, 'vehicle_type', at.type, 'axle_count', aa.axle_count, 'vehicle_length', aa.vehicle_length_cm ) asset, tp.id trip_id, COALESCE(adm.connected, false) assigned, lat, lon, lname  FROM users u 
        INNER JOIN user_types ut on ut.id = u.fk_user_type_id AND ut.type = 'driver'
        LEFT JOIN asset_driver_mappings adm on adm.fk_driver_id = u.id
        LEFT JOIN assets a on a.id = adm.fk_asset_id
        LEFT JOIN asset_types at on at.id = a.fk_ast_type_id
        LEFT JOIN asset_attributes aa on aa.fk_asset_id = a.id
        LEFT JOIN companies c on c.id = a.fk_c_id
        LEFT JOIN trip_plannings tp on tp.fk_asset_id = adm.fk_asset_id and tp.status = 'inprogress'
        WHERE u.id = $user_id and u.active
        ORDER BY adm.id DESC
        LIMIT 1`;

    models.sequelize.query(query, { bind:{ user_id: user_id},  type: models.sequelize.QueryTypes.SELECT }).spread(function (data) {
      if(!data) {
        res.send('No driver found')
      }
      data.asset.axle_count = data.asset.axle_count ? data.asset.axle_count + " axle" : null;
      data.asset.vehicle_length = data.asset.vehicle_length ? parseInt(data.asset.vehicle_length * 0.0328) + " ft" : null;

      if(!data.assigned) {
        return res.send(data)
      }
      nuRedis.getAssetDetails(data.asset.id, function (err, value) {
        if(!value) {
          return res.send(data)
        }
        data.lat = value.lat;
        data.lon = value.lon;
        data.lname = value.lname;

        return res.send(data)

      })
            
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send()
    })
  }
   
};

exports.updateDriverAssetMapping = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const driver = req.body.driver || null;
  const asset = req.body.asset || null;
  const map_State = req.body.connected;

  if(!driver || !driver.id){
    return res.json("invalid driver id")
  }

  if(!asset || !asset.id){
    return res.json("invalid asset id")
  }

  async.auto({
    driver: function (cb) {

      let query = "SELECT u.id, adm.connected FROM users u "+
            " INNER JOIN user_types ut on ut.id = u.fk_user_type_id AND ut.type = 'driver'"+
            " LEFT JOIN asset_driver_mappings adm on adm.fk_driver_id = u.id " +
            " WHERE u.active AND u.id = "+ driver.id +" AND u.fk_c_id = "+ fk_c_id;
      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (data) {
        if(data && data.id){
          if(data.connected && map_State ){
            cb({
              err: '',
              msg: 'Driver already assigned',
              code: 400
            })
          }else{
            cb(null, data.id || driver.id)
          }
        }else{
          cb({
            err: null,
            msg: 'Invalid driver id',
            code: 400
          })
        }
      }).catch(function (err) {
        cb({
          err: err,
          msg: 'Internal server error',
          code: 500
        })
      })
    },
    asset: function (cb) {
      let query = "SELECT a.id FROM assets a WHERE a.active AND a.id = "+ asset.id;
      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (data) {
        if(data && data.id){
          cb(null, data.id || asset.id)
        }else{
          cb({
            err: null,
            msg: 'Invalid asset id',
            code: 400
          })
        }
      }).catch(function (err) {
        cb({
          err: err,
          msg: 'Internal server error',
          code: 500
        })
      })
    },
    mapping: ['driver','asset', function (results, cb) {
      let new_connection = {
        fk_driver_id: driver.id,
        fk_asset_id: asset.id,
        connected: map_State
      };

      if(map_State){
        new_connection.tis = moment().tz(utils.timezone.default, false).unix();
        models.asset_driver_mapping.findOrCreate({
          where:{
            fk_asset_id: new_connection.fk_asset_id,
            fk_driver_id: new_connection.fk_driver_id,
            connected: true
          },
          defaults: new_connection
        }).spread(function (connection, created) {
          if(!created){
            cb({
              code: 409,
              msg: 'Mapping already exits',
              err: ''
            })
          }else{
            cb(null, 'driver mapping success')
          }
        }).catch(function (err) {
          cb({
            code: 500,
            err: err,
            msg: 'Internal server error'
          })
        })
      }else{
        new_connection.unassigned_tis = moment().tz(utils.timezone.default, false).unix();
        nuRedis.getAssetDetails(asset.id, function (err, value) {
          new_connection.lat = value ? value.lat : null;
          new_connection.lon = value ? value.lon : null;
          new_connection.lname = value ? value.lname : null;

          models.asset_driver_mapping.update(new_connection, {
            where: {
              fk_asset_id: new_connection.fk_asset_id,
              fk_driver_id: new_connection.fk_driver_id,
              connected: true
            },
            defaults: new_connection
          }).then(function (updated) {
            if (updated === 1) {
              cb(null, 'driver unmapping success')
            } else {
              cb({
                code: 409,
                msg: 'Mapping already unmapping',
                err: ''
              })
            }
          }).catch(function (err) {
            cb({
              code: 500,
              err: err,
              msg: 'Internal server error'
            })
          })

        })
                
      }
    }]
  }, function (err, results) {
    if(err){
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      if(err.code === 500){
        err.err = null
      }
      res.status(err.code).send(err.msg + " : "+ err.err)
    }else{
      res.json(results.mapping)
    }
  })
};

exports.getAllPackages = function (req, res) {
  const fk_u_id = req.headers.fk_u_id;
  const page = req.query.page || 0;
  const limit = req.query.limit || 10;
  const offset = (page * limit);
  const status = req.query.status;
  let q = req.query.q;
  let searchCondition = "";

  if(CONSIGNMENT_STATUSES.indexOf(status) === -1){
    return res.status(400).send("Enter valid consignment status")
  }

  if(q && q.length > 2){
    q =q.toLowerCase();
    searchCondition = " AND ( lower(con.consignee->>'name') like '%"+ q +"%' OR lower(con.consignee->>'lname') like '%"+ q +"%' OR" +
            " lower(con.consignor->>'name') like '%"+ q +"%' OR lower(con.consignor->>'lname') like '%"+ q +"%' )"
  }

  let query = "SELECT con.id, con.status, con.invoice_no, con.consignor, con.consignee, con.material_type, "+
        "   con.product_code, con.total_packages, con.weight, cs.tis, cs.note, cs.reason, "+
        "   count(*) OVER()::int as total_count" +
        " FROM consignments con " +
        " LEFT JOIN consignment_statuses cs on cs.fk_consignment_id = con.id AND cs.is_current" +
        " INNER JOIN trip_plannings tp on tp.id = con.fk_trip_id AND tp.active" +
        " INNER JOIN driver_details dd on ((tp.oth_details->>'drivers')::json->0->>'id')::integer = dd.fk_u_id"+
        " INNER JOIN users u on u.id = " + fk_u_id + " and dd.fk_u_id = u.id"+
        " INNER JOIN user_types ut on ut.id = u.fk_user_type_id and ut.type = 'driver'"+
        " WHERE con.active AND con.status = '"+ status +"' "+
        searchCondition + " ORDER BY con.id desc"+
        " LIMIT " + limit + " OFFSET " + offset;
        
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    let total_rows = 0;
        
    data.forEach(function (i) {
      total_rows = i.total_count;
      i.consignor = _.omit(i.consignor, ['poi_id','loading','unloading','seq_no','active','type']);
      i.consignee = _.omit(i.consignee, ['poi_id','loading','unloading','seq_no','active','type']);
      i.consignee.contact_no = processConsigneeContactNo(i.consignee.contact_no);
      i.total_count = undefined
    });

    res.json({
      count: total_rows,
      data: data
    })
  }).catch(function(err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send()
  })
};

exports.getPackage = function(req, res) {
  const fk_u_id = req.headers.fk_u_id;
  const id = req.query.id;

  let query = "SELECT con.id, con.status, con.invoice_no, con.consignor, con.consignee, con.material_type, "+
        "   con.product_code, con.total_packages, con.weight, cs.tis, cs.note, cs.reason, cs.files_meta" +
        " FROM consignments con " +
        " LEFT JOIN consignment_statuses cs on cs.fk_consignment_id = con.id AND cs.is_current" +
        " INNER JOIN trip_plannings tp on tp.id = con.fk_trip_id AND tp.active" +
        " INNER JOIN driver_details dd on ((tp.oth_details->>'drivers')::json->0->>'id')::integer = dd.fk_u_id"+
        " INNER JOIN users u on u.id = " + fk_u_id + " and dd.fk_u_id = u.id"+
        " INNER JOIN user_types ut on ut.id = u.fk_user_type_id and ut.type = 'driver'"+
        " WHERE con.active AND con.id = "+ id +
        " LIMIT 1";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (data) {
    if(data){
      data.consignor = _.omit(data.consignor, ['poi_id','loading','unloading','seq_no','active','type']);
      data.consignee = _.omit(data.consignee, ['poi_id','loading','unloading','seq_no','active','type']);
      data.consignee.contact_no = processConsigneeContactNo(data.consignee.contact_no);
      res.json(data)
    }else{
      res.status(404).send("Not found")
    }
  }).catch(function(err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send()
  })
};

exports.updatePackageStatus = function (req, res) {

  const fk_u_id = req.headers.fk_u_id;
  let record = req.body;

  record.fk_by_u_id = fk_u_id;
  record.fk_consignment_id = record.id;

  record.id = undefined;

  async.auto({
    consignment: function (cb) {
      models.consignment.update({status: record.status},{
        where: {
          id: record.fk_consignment_id,
          active: true
        }
      }).then(function () {
        cb()
      }).catch(function (err) {
        cb({
          code: 500,
          msg: 'Internal server error',
          err: err
        })
      })
    },
    status: function (cb) {
      models.consignment_status.update({is_current: false},{
        where: {
          fk_consignment_id: record.fk_consignment_id,
          is_current: true,
          active: true
        }
      }).then(function () {
        models.consignment_status.create(record).then(function () {
          cb(null, "sucessfully updated")
        }).catch(function (err) {
          cb({
            code: 500,
            msg: 'Internal server error',
            err: err
          })
        })
      }).catch(function (err) {
        cb({
          code: 500,
          msg: 'Internal server error',
          err: err
        })
      })
    }
  }, function (err, results) {
    if(err){
      res.status(err.code || 500).send(err.msg || 'Internal server error')
    }else{
      res.json(results.status)
    }
  })
};

exports.getMap = function(req, res) {

  // todo : confirm which map? single consignment or all consignments(trip)

  const id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  if(!id){
    res.status(400).send('Invalid trip id');
  }else{
    let query = "SELECT tp.id, tp.fk_asset_id, tp.status, tp.waypoints, tp.trip_log, tp.geo_trip_route FROM trip_plannings tp "+
            " INNER JOIN assets a on a.id = tp.fk_asset_id" +
            " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected"+
            " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected"+
            " LEFT JOIN companies c on c.id = a.fk_c_id" +
            " WHERE g.active AND g.fk_c_id = "+ fk_c_id +" AND tp.id = "+ id +" AND " + groupCondition +" LIMIT 1";

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (trip) {
        if(!trip){
          res.status(404).send('Not found')
        }else{
          res.json({
            estPath: trip.geo_trip_route.paths ? trip.geo_trip_route.paths[0].points : "",
          })
        }
      }).catch(function (err) {
        LOGGER.error(APP_LOGGER.formatMessage(req, err));
        res.status(500).send('Internal server error')
      })
  }
};

exports.getDriverAssetActivity = function (req, res) {

  const user_id = req.query.id;
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const todayStartTime = moment().tz(utils.timezone.default, false).startOf('day').unix();
  const todayEndTime = moment().tz(utils.timezone.default, false).endOf('day').unix();
  let start_time = req.query.start_time || todayStartTime;
  start_time = moment.unix(start_time).tz(utils.timezone.default, false);
  let end_time = req.query.end_time || todayEndTime;
  end_time = moment.unix(end_time).tz(utils.timezone.default, false);
  const startOfDay = start_time.clone().startOf('day').unix();
  const endOfDay = end_time.clone().endOf('day').unix();
  const searchCondition = req.query.q ? " AND to_json(tp.trip_log->'trip')::text ilike '%"+ req.query.q +"%'" : "";
  const output_format = req.query.format;

  if (!user_id) {
    return res.status(400).send('Invalid driver ID');
  }

  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  
  async.auto({
    validateAssetGroup: validateAssetGroup.bind(null, groupIds, user_id),
    activity: ['validateAssetGroup', function (result, cb) {
      let driver_assets = utils.convertGroupIdsToTuple(result.validateAssetGroup.driver_assets);
      let query = "SELECT a.lic_plate_no, c.cname as operator, atp.type as asset_type, tp.trip_log->'active' as active, tp.tid as id, tp.trip_log->'distance' as distance," +
                " tp.trip_log->'violations' as violations, tp.trip_log->'trip' as activity," +
                " json_agg(g.gid) as gids, tp.trip_log as log," +
                " sum((tp.trip_log->>'distance')::float) over() as total_distance,\n" +
                " sum((tp.trip_log->>'time_taken')::int) over() as total_duration," +
                " count(*) OVER() AS total_count FROM groups g " +
                " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected " +
                " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
                " LEFT JOIN companies c on c.id = a.fk_c_id " +
                " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id and atp.type = 'Truck'" +
                " LEFT JOIN auto_trips tp on tp.fk_asset_id = a.id " +
                " WHERE g.active AND tp.custom = FALSE AND g.id in " + groupIds + " AND a.id IN " + driver_assets +
                " AND ( to_json(tp.trip_log->'departure')::TEXT::INT BETWEEN " + startOfDay + " AND " + endOfDay +
                " OR to_json(tp.trip_log->'lkLocation'->'tis')::TEXT::INT BETWEEN " + startOfDay + " AND " + endOfDay + " )" +
                searchCondition +
                " GROUP BY a.id, tp.id, c.cname, atp.type ORDER BY tp.tid DESC";

      if (output_format !== 'xls') {
        query += " LIMIT " + limit + " OFFSET " + offset;
      }

      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
        .then(function (data) {
          if (output_format === 'xls') {
            try {

              let lic_no = data.length > 0 ? data[0].lic_plate_no : 0;
              let file_name = "DRIVER-ACTIVITY-" + lic_no + ".xls";
              let file_path = file_base_path + file_name;

              convertAssetActivityToXLS(data, file_name, function (err) {
                if (err) {
                  res.status(err.code).send(err.msg)
                } else {
                  res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
                  res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
                  res.setHeader("Content-Type", "application/vnd.ms-excel");
                  res.setHeader("Filename", file_name);

                  res.download(file_path, file_name, function (err) {
                    if (!err) {
                      fs.unlink(file_path)
                    }
                  })
                }
              })
            } catch (e) {
              LOGGER.warn(APP_LOGGER.formatMessage(req, e))
            }
          } else {
            async.auto({
              pois: function (cb) {
                let poi_ids = [];
                data.forEach(function (i) {
                  poi_ids = _.concat(poi_ids, _.map(i.activity, 'Id'))
                });
                poi_ids = _.uniq(_.compact(poi_ids));
                if(_.size(poi_ids) > 0){
                  let query = `SELECT p.id, pt.type, CASE WHEN p.priv_typ THEN 'company'
                                    ELSE 'general' END category
                                    FROM pois p 
                                    JOIN poi_types pt on pt.id = p.typ
                                    WHERE p.id in (`+ poi_ids +`)`;
                  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                    .then(function (data) {
                      cb(null, data)
                    }).catch(function () {
                      cb()
                    })
                }else{
                  cb()
                }
              },
              data: ['pois', function (results, cb) {
                let totalAssets = 0;
                let totalDistance = 0;
                data.forEach(function (item) {
                  totalAssets = Number(item.total_count);
                  totalDistance = item.total_distance;
                  item.distance = Number((item.distance / 1000).toFixed(1));
                  item.violations = item.violations ? item.violations.length : 0;
                  item.avgSpd = item.log.avg_speed || null;
                  let s = item.activity[0];
                  item.start = {
                    tis: s.StartTis,
                    lname: s.LName
                  };
                  if (!item.active) {
                    // completed trip
                    let e = item.activity[item.activity.length - 1];
                    item.end = {
                      tis: e.StartTis,
                      lname: e.LName
                    };
                    item.duration = Math.round(moment.duration(item.log.time_taken, 'seconds').asMinutes());
                    item.stops = item.log.stops
                  } else {
                    // ongoing trip
                    item.end = {
                      tis: null,
                      lname: null
                    };
                    item.duration = Math.round(moment.duration((item.log.arrival - item.log.departure), 'seconds').asMinutes());
                    item.stops = (item.log.trip.length - 1) || 0
                  }

                  // detail log
                  let detailLog = [];
                  let activityLength = item.active ? _.size(item.activity) : _.size(item.activity) - 1;
                  let stopsCount = 1;
                  _.forEach(item.activity, function (i, index) {

                    let typeEvent = "planned";
                    let labelText = "";

                    if (index === 0) {
                      labelText = "A"
                    } else if (!item.active && index === activityLength) {
                      labelText = "B"
                    } else {
                      typeEvent = "unplanned";
                      labelText = stopsCount.toString();
                      stopsCount++
                    }

                    detailLog.push({
                      type: typeEvent,
                      lname: i.LName,
                      arrivalTis: i.StartTis,
                      departureTis: i.EndTis,
                      dwellDuration: Math.round(moment.duration(i.DwellTime, 'seconds').asMinutes()),
                      label: labelText,
                      lat: i.Lat,
                      lon: i.Lon,
                      avgSpd: 0,
                      distance: 0,
                      poi: _.find(results.pois, {id: i.Id}) || null
                    });

                    if (index !== activityLength) {
                      detailLog.push({
                        type: "intransit",
                        lname: "",
                        arrivalTis: (i.EndTis + 1),
                        departureTis: item.activity[index + 1] ? (item.activity[index + 1].StartTis - 1) : null,
                        dwellDuration: 0,
                        label: "",
                        lat: null,
                        lon: null,
                        avgSpd: i.AvgSpd,
                        distance: Number((i.Distance / 1000).toFixed(1)),
                        poi: null
                      })
                    }
                  });

                  item.log = detailLog;

                  item.total_count = undefined;
                  item.total_distance = undefined;
                  item.total_duration = undefined;
                  item.active = undefined;
                  item.activity = undefined
                });
                return cb(null, {
                  summary: {
                    total_distance: Number((totalDistance / 1000).toFixed(1)),
                    total_duration: Math.round(moment.duration(totalDistance, 'seconds').asMinutes()),
                    time_period: end_time.diff(start_time, 'minutes')
                  },
                  total_count: totalAssets,
                  data: data
                });
              }]}, function (err, results) {
              cb(null, results.data)
            })
          }
        }).catch(function (err) {
          return res.status(500).send(err);
        })
    }]
  }, function (err, result) {
    if (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      if(err.code === 500){
        err.error = null
      }
      return res.status(err.code).send(err)
    }
    res.status(200).send(result.activity)
  })

    
};

function validateAssetGroup(groupIDs, fk_u_id, callback) {

  let query = `SELECT array_agg(adm.fk_asset_id) driver_assets FROM 
    asset_group_mappings agm
    INNER JOIN asset_driver_mappings adm on adm.fk_asset_id = agm.fk_asset_id
    WHERE agm.fk_group_id in ` + groupIDs + ' and adm.fk_driver_id = ' + fk_u_id;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (result) {
    if (result && result.driver_assets) {
      callback(null, result)
    } else {
      callback({
        code: 403,
        msg: 'No asset found for this driver'
      })
    }
  }).catch(function (err) {
    console.log(err);
    callback({
      error: err,
      message: 'Internal server error',
      code: 500
    })
  })
}

function convertAssetActivityToXLS(data, filename, callback) {
  let date_time_format = 'DD-MM-YY, HH:mm:ss';
  let processed_data = [];
  let lic_no = data.length > 0 ? data[0].lic_plate_no : null;
  data = _.sortBy(data, 'id');
  data.forEach(function (i) {
    let logs = i.activity || [];
    if(logs.length > 1){
      logs.pop()
    }

    logs.forEach(function (a) {
      processed_data.push(a)
    })
  });

  let final_data = [];
  let count = 0;
  let data_length = processed_data.length - 1;
  processed_data.forEach(function (i) {
    // stopped event
    final_data.push({
      start_tis: i.StartTis,
      end_tis: i.EndTis,
      start_lname: i.LName,
      end_lname: i.LName,
      distance: null,
      duration: utils.FormatMinutesDHM(i.DwellTime),
      avg_spd: null,
      event_state: 'stop'
    });

    // Intransit event
    let next_item = processed_data[count + 1];

    if(data_length !== count && next_item){

      final_data.push({
        start_tis: i.EndTis,
        end_tis: next_item.StartTis,
        start_lname: i.LName,
        end_lname: next_item.LName,
        distance: i.Distance ? Number((i.Distance / 1000).toFixed(1)) : 0,
        duration: utils.FormatMinutesDHM(Math.abs(i.EndTis - next_item.StartTis)),
        avg_spd: i.AvgSpd ? i.AvgSpd + ' km/h' : '0 km/h',
        event_state: 'intransit'
      })
    }

    count++
  });

  // SORT data with latest time first
  final_data = _.orderBy(final_data, ['start_tis'],['desc']);

  try {
    let workbook = new Excel.Workbook();
    let ws = workbook.addWorksheet('Asset list');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = [
      {width: 15},
      {width: 15},
      {width: 40},
      {width: 15},
      {width: 40},
      {width: 15},
      {width: 15},
      {width: 15},
      {width: 15},
    ];

    ws.addRow(['Vehicle','Start time', 'Start location', 'End time', 'End location', 'Distance(km)', 'Duration', 'Avg speed(km/h)', 'Event state' ]);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    final_data.forEach(function (i) {

      ws.addRow([
        lic_no,
        moment.unix(i.start_tis).tz(utils.timezone.default, false).format(date_time_format),
        i.start_lname,
        moment.unix(i.end_tis).tz(utils.timezone.default, false).format(date_time_format),
        i.end_lname, // end lname
        i.distance || '-', // distance
        i.duration,
        i.avg_spd  || '-', // avg speed
        i.event_state // event state
      ]);

      let currentRowIndex = ws.rowCount;
      for (let j = 65; j <= 79; j++) {
        let currentCol = String.fromCharCode(j);
        if (['A', 'C', 'E', 'I'].indexOf(currentCol) === -1) {
          ws.getRow(currentRowIndex).getCell(currentCol).alignment = {horizontal: 'right'};
        }
      }
    });

    ws.addRow();
    ws.addRow(['','','','','Total', _.sumBy(final_data, 'distance')]);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, final_data)
      }).catch(function (err) {
        callback({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      });
  }catch(e){
    LOGGER.warn(APP_LOGGER.formatMessage(null, e));
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

exports.getDriverStats = function (req, res) {
  const driver_id = req.query.id;
  const todayStartTime = moment().tz(utils.timezone.default, false).startOf('day').unix();
  const last_7_days =  moment().tz(utils.timezone.default, false).subtract(8, 'days').startOf('day').unix();
  const now = moment().tz(utils.timezone.default, false).unix();
  let processed_data = {
    speed_data : {},
    distance_data : {},
    time_data : {}
  };

  if(!driver_id){
    return res.status(400).send('Enter driver ID')
  }

  let query = `SELECT adm.fk_asset_id, adm.tis, adm.unassigned_tis 
    FROM asset_driver_mappings adm 
    WHERE adm.fk_driver_id = $driver_id and tis >= $last_7_days`;

  models.sequelize.query(query, { bind: {driver_id: driver_id, last_7_days: last_7_days}, type: models.sequelize.QueryTypes.SELECT }).then(function (data) {
    let output = [];
    console.time("dynamo");
    async.forEachOf(data, function (driver_asset, i, callback) {
      nuDynamoDb.getAssetLocationData({
        fk_asset_id: driver_asset.fk_asset_id,
        start_tis: driver_asset.tis,
        end_tis: driver_asset.unassigned_tis ? driver_asset.unassigned_tis : now
      }, function (err, data) {
        callback(null, output.push({
          "tot_osf" : _.filter(data, function(s) { return s.osf && s.tis >= todayStartTime; }).length,
          "tot_hbk" : _.filter(data, function(s) { return s.hbk && s.tis >= todayStartTime; }).length,
          "time_data" : getFleetwideTime(data),
          "speed_data" : getAverageSpeed(data),
          "distance_data" : getDistance(data),
          "fk_asset_id": driver_asset.fk_asset_id,
          "assigned_tis": moment.unix(driver_asset.tis).tz(utils.timezone.default, false).format("hh:mm a"),
          "assigned_day": moment.unix(driver_asset.tis).tz(utils.timezone.default, false).format("D MMM YYYY"),
          "unassigned_tis": driver_asset.unassigned_tis,
          "unassigned_day": driver_asset.unassigned_tis ? moment.unix(driver_asset.unassigned_tis).format("D MMM YYYY") : null
        }))
      })
    }, function () {
      console.timeEnd("dynamo");
      output = _.groupBy(_.orderBy(output, 'assigned_day'), 'assigned_day');
      let day_wise_spd_data = [];
      let day_wise_time_data = [];
      let day_wise_dist_data = [];
      _.forOwn(output, function (value, key) {
        let day_wise_spd_avg = 0;
        let day_wise_spd_total = 0;

        let day_wise_time_avg = 0;
        let day_wise_time_total = 0;

        let day_wise_dist_avg = 0;
        let day_wise_dist_total = 0;

        _.each(value, function (v) {
          day_wise_spd_total += v.speed_data;
          day_wise_time_total += v.time_data;
          day_wise_dist_total += v.distance_data
        });
        day_wise_spd_avg = day_wise_spd_total / value.length;
        day_wise_time_avg = day_wise_time_total / value.length;
        day_wise_dist_avg = day_wise_dist_total / value.length;
                   
        day_wise_spd_data.push({
          day: key,
          speed: Number(day_wise_spd_avg.toFixed(2))
        });
        day_wise_time_data.push({
          day: key,
          time: Number(day_wise_time_avg.toFixed(2))
        });
        day_wise_dist_data.push({
          day: key,
          distance: Number(day_wise_dist_avg.toFixed(2))
        })

      });
      processed_data.speed_data.total = Number(_(day_wise_spd_data).map('speed').sum().toFixed(2));
      processed_data.speed_data.avg = Number((processed_data.speed_data.total / day_wise_spd_data.length).toFixed(2));
      processed_data.speed_data.data = day_wise_spd_data;

      processed_data.time_data.total = Number(_(day_wise_time_data).map('time').sum().toFixed(2));
      processed_data.time_data.avg = Number((processed_data.time_data.total / day_wise_time_data.length).toFixed(2));
      processed_data.time_data.data = day_wise_time_data;

      processed_data.distance_data.total = Number(_(day_wise_dist_data).map('distance').sum().toFixed(2));
      processed_data.distance_data.avg = Number((processed_data.distance_data.total / day_wise_dist_data.length).toFixed(2));
      processed_data.distance_data.data = day_wise_dist_data;

      processed_data.tot_osf = _(output).map('tot_osf').sum();
      processed_data.tot_hbk = _(output).map('tot_hbk').sum();
      res.json(processed_data)
    });
  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send()
  })
};

// return in minutes
function getFleetwideTime (locations) {
  let fleetwideStartTime = 0;
  let fleetwideEndTime = 0;
  let fleetwide = [];
  let fleetwideMin = 0;

  let flag = true;
  _(locations).forEach(function (location) {
    if (location.ign === 'B') {
      if (flag) {
        fleetwideStartTime = location.tis;
      }
      fleetwideEndTime = location.tis;

      flag = false;
    } else {
      if (!flag) {
        fleetwide.push(Math.abs(fleetwideStartTime - location.tis) / 60);
      }
      flag = true;
    }
  });
  if (!flag) fleetwide.push(Math.abs(fleetwideStartTime - fleetwideEndTime) / 60);
  fleetwideMin = _.sum(fleetwide);
  return fleetwideMin;
}

//retutn in kms
function getDistance(locations) {
  let dist = 0;
  let distSeg = [];

  let flag = true;
  _.each(locations, function (location) {
    if (location.ign === 'B' && location.lat && location.lon) {
      distSeg.push({
        latitude: location.lat,
        longitude: location.lon
      });
      flag = false;
    } else {
      if (!flag && location.lat && location.lon) {
        distSeg.push({
          latitude: location.lat,
          longitude: location.lon
        });
        let distance = geolib.getPathLength(distSeg);
        distSeg = [];
        dist = dist + distance / 1000;
      }
      flag = true;
    }
  });
  if (!flag) {
    let distance = (geolib.getPathLength(distSeg));
    dist = dist + distance / 1000;
    distSeg = [];
  }

  return dist
}

function getAverageSpeed (locations) {
  let data = _.filter(locations, function(i) { return i.spd > 0 && i.ign === 'B'; });
  return _.meanBy(data, 'spd') || 0
}

exports.getDriverCompletedTrips = function (req, res) {
  const driver_id = req.query.id;
  const last_7_days =  moment().tz(utils.timezone.default, false).subtract(8, 'days').startOf('day').unix();
  let processed_data = {
    completed_trips : {},
    stops : {}
  };

  if(!driver_id){
    return res.status(400).send('Enter driver ID')
  }

  let query = `SELECT json_agg(x.trips) trips, x.day
    FROM
    (
    SELECT json_build_object('id', tp.id, 'stops', json_array_length(COALESCE(tp.trip_log->>'trip', '[]')::json)) trips, to_char(to_timestamp(tp.end_tis), 'DD Mon YYYY') AS day
    
    FROM asset_driver_mappings adm
    LEFT join trip_plannings tp on tp.fk_asset_id = adm.fk_asset_id
    WHERE adm.fk_driver_id = $driver_id and adm.tis >= $last_7_days and tp.status = 'completed' and
    case
        when adm.connected = false then
        (tp.start_tis >= adm.tis and tp.end_tis <= adm.unassigned_tis) OR
        (tp.start_tis <= adm.tis and tp.end_tis <= adm.unassigned_tis)
        when adm.connected = true then
         (adm.tis BETWEEN tp.start_tis and tp.end_tis) OR
         (adm.tis <= tp.start_tis)
    end
    GROUP BY tp.end_tis, tp.id
    ) x
    GROUP BY x.day
    ORDER BY x.day`;

  models.sequelize.query(query, { bind: {driver_id: driver_id, last_7_days: last_7_days}, type: models.sequelize.QueryTypes.SELECT }).then(function (data) {
    let completed_trips_data = [];
    let stops_data = [];
    _.each(data, function (v) {

      completed_trips_data.push({
        day: v.day,
        trips: v.trips.length
      });
      stops_data.push({
        day: v.day,
        stops:  _(v.trips).map('stops').sum()
      })
    });

    processed_data.completed_trips.total = Number(_(completed_trips_data).map('trips').sum().toFixed(2));
    processed_data.completed_trips.avg = Number((processed_data.completed_trips.total / completed_trips_data.length).toFixed(2));
    processed_data.completed_trips.data = completed_trips_data;

    processed_data.stops.total = Number(_(stops_data).map('stops').sum().toFixed(2));
    processed_data.stops.avg = Number((processed_data.stops.total / stops_data.length).toFixed(2));
    processed_data.stops.data = stops_data;
    res.json(processed_data)

  }).catch(function (err) {
    console.log(err);
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send()
  })
};

exports.getNotifications = function (req, res) {

  const driver_id = req.query.id;

  if(!driver_id) {
    res.status(400).send('Invalid driver id')
  }

  if(driver_id) {
    let query = `SELECT adm.fk_asset_id, adm.tis, adm.unassigned_tis, n.msg, n.fk_notif_id as type, n.level as lvl, n.tis, params as rparams, is_good, anomaly,
        count(*) OVER()::INT AS total_count
         FROM 
        asset_driver_mappings adm 
        INNER JOIN notifications n on n.fk_asset_id = adm.fk_asset_id and 
        CASE 
            WHEN adm.unassigned_tis is null THEN 
            n.tis >= adm.tis
            WHEN adm.unassigned_tis is not null THEN 
            n.tis between adm.tis and adm.unassigned_tis
        END	
        
        WHERE adm.fk_driver_id = $driver_id
        ORDER BY n.tis desc
        LIMIT 25`;

    models.sequelize.query(query, { bind: { driver_id: driver_id}, type: models.sequelize.QueryTypes.SELECT }).then(function (notifications) {
      let total_rows = notifications.length > 0 ? notifications[0].total_count : 0;
            
      res.json({
        count: total_rows,
        data: notifications
      })
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send()
    })
  }
   
};

exports.getDriverLeaderboard = function (req, res) {

  const fk_c_id = req.headers.fk_c_id;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  let sort = req.query.sort;
  const start_time = req.query.start_tis || moment().tz(utils.timezone.default, false).startOf('year').unix();
  const end_time = req.query.end_tis || moment().tz(utils.timezone.default, false).unix();
  let sort_condition = "";
  let sort_order = "desc";
  const output = [];
  const fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';
  const status = 'all';
  const output_format = req.query.format || 'list';

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'distance',
      client: 'distance'
    }, {
      db: 'harsh_braking',
      client: 'hbk'
    }, {
      db: 'harsh_acceleration',
      client: 'harsh_acceleration'
    }, {
      db: 'overspeeding',
      client: 'overspeeding'
    }, {
      db: 'night_driving',
      client: 'night_driving'
    }, {
      db: 'total',
      client: 'total_score'
    }, {
      db: 'total_driving',
      client: 'hours_driven'
    }]
  )

  let request_options = {
    uri: config.server_address.driver_report_engine() + '/v1/driverscores',
    method: 'POST',
    headers: {
      fk_c_id: req.headers.fk_c_id,
    },
    json: {
      stis: start_time,
      etis: end_time
    }
  };
    
  request(request_options, function (error, response, body) {
    if(error) {
      return res.status(500).json({
        msg: "Service api error",
        service_api_error: error
      })
    }

    if(response.statusCode !== 200) {
      return res.status(response.statusCode).json({ 
        code: response.statusCode,
        msg: "Service api error" 
      })
    }

    if(body && body.length === 0) {
      return res.json({
        total_count: 0,
        data: []
      })
    }

    if (searchKeyword.length > 0) {
      body = _.filter(body, function (o) {
        if(o.firstname && o.lastname) {
          if (o.firstname.indexOf(searchKeyword) !== -1 || o.lastname.indexOf(searchKeyword) !== -1) {
            return o;
          }
        }
      });

      if(body.length === 0) {
        return res.json({
          total_count: 0,
          data: []
        })
      }
    }

    let total_count = body.length;


    //sorting and pagination using lodash
    let apiDataSet = [];

    if(output_format === 'list'){
      apiDataSet = _(body)
        .orderBy(function(e) {
          if (sort_condition ===  "total") {
            return e.total
          }
          return e.values[sort_condition]
        }, [sort_order])
        .drop((page - 1) * limit) // page in drop function starts from 0
        .take(limit) // limit 2
        .value();
    }else{
      apiDataSet = _(body)
        .orderBy(function(e) {
          if (sort_condition ===  "total") {
            return e.total
          }
          return e.values[sort_condition]
        }, [sort_order])
        .value();
    }

    apiDataSet.forEach(function (i) {
      let obj = {};
      obj.dist_score = i.distance ? Number(i.distance.toFixed(2)) : null;
      obj.distance = i.values.distance ? Number(i.values.distance.toFixed(2)) : 0;
            
      obj.hbk_score = i.harsh_braking ? Number(i.harsh_braking.toFixed(2)) : null;
      obj.hbk = i.values.harsh_braking ? Number(i.values.harsh_braking.toFixed(2)) : 0;

      obj.harsh_acceleration_score = i.harsh_acceleration ? Number(i.harsh_acceleration.toFixed(2)) : null;
      obj.harsh_acceleration = i.values.harsh_acceleration ? Number(i.values.harsh_acceleration.toFixed(2)) : 0;

      obj.overspeeding_score = i.overspeeding ? Number(i.overspeeding.toFixed(2)) : null;
      obj.overspeeding =  i.values.overspeeding ? Number(i.values.overspeeding.toFixed(2)) : 0;

      obj.night_driving_score = i.night_driving ? Number(i.night_driving.toFixed(2)) : null;
      obj.night_driving =  i.values.night_driving ? Number(i.values.night_driving.toFixed(2)) / 60 : 0;

      obj.hours_driven =  i.values.total_driving ? Number(i.values.total_driving.toFixed(2)) / 60 : 0;

      obj.total_score = i.total ? Number(i.total.toFixed(2)) : null;

      obj.id = i.fk_user_id;
      obj.firstname = i.firstname;
      obj.lastname = i.lastname;
      obj.location = i.location;
      obj.emp_type = i.emp_type;

      output.push(obj)

    });

    if (output_format === 'xls') {
      try {
        let file_name = "DRIVER-LEADERBOARD-" + fk_c_id + "-" + moment().unix() + ".xls";
        let file_path = file_base_path + file_name;

        tableConfigsHelper.getTableConfig('driver-leaderboard-list', fk_u_id, status, default_columns, function (err, table_config) {
          xlsDownload.convertTableV1ToXLS(status, output, file_base_path, file_name, table_config, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })

        })
      }catch (e){
        console.log("e: ", e)
      }
    } else {
      res.json({
        total_count: total_count,
        data: output
      })
    }
  });
};