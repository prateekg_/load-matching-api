/**
 * This controller is used to get all assets realtime data required for dashboard
 */
const utils = require('../../../helpers/util');
const async = require('async');
const models = require('../../../db/models/index');
const _ = require('lodash');
const moment = require('moment');
const assetService = require('../../../services/assetsService');

function getAssetsPastAnalytics(fk_c_id, groupIds, start_tis, end_tis, select_fields){
  let query_fields = select_fields? select_fields : ' * ';
  return `SELECT ${query_fields}
          FROM groups g  
          LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected 
          INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active  
          INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id
          LEFT JOIN asset_stats ast on ast.fk_asset_id = a.id 
          WHERE g.active AND  g.id in ${groupIds} AND ast.tis > ${start_tis} AND ast.tis <= ${end_tis} AND g.fk_c_id = ${fk_c_id}`;
}

/**
 * Api to get realtime stat from redis for all asset type
 * @param {*} req 
 * @param {*} res 
 */
exports.getAssetsStat = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const emailId = req.headers.email;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  assetService.getAllCurrentAssetStats(fk_c_id, groupIds, emailId, function(err, result){
        
    if(err){
      res.status(err.code).send(err.message)
    }
    else res.json(result)
  });
};

/**
 * This controller function gets asset stats for a given period
 * Fetches data from asset_stats table to find past stat and current todays data from redis
 * @param {Object} req.query - start_tis: Fetch asset stat from start_tis
 *                           - end_tis: End date to fetch asset stat
 * @param req
 * @param {*} res
 */
exports.getPastAssetStats = function(req,res){
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis;
  const current_tis = moment().startOf('day').unix();
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const emailId = req.headers.email;

  const requiredKeys = ['start_tis','end_tis'];
  const dataValid = utils.validateMinPayload(requiredKeys, req.query);

  if(!dataValid)
    return res.status(422).send("Mandatory fields: "+requiredKeys);

  let select_fields = "sum(ast.tot_osf) overspeeding, sum(ast.tot_hbk) as harsh_braking ";
  let query = getAssetsPastAnalytics(fk_c_id, groupIds, start_tis, end_tis, select_fields);
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function(data){
    if(end_tis > current_tis){

      //If end date is today, add todays current stat in the array
      assetService.getAllCurrentAssetStats(fk_c_id, groupIds, emailId, function(err, result){
        
        if(err){
          res.status(err.code).send(err.message)
        }
        else{
          data[0].overspeeding = parseInt(data[0].overspeeding) + parseInt(result.totSpdVio);
          data[0].harsh_braking = parseInt(data[0].harsh_braking) + parseInt(result.totHbkVio);
                    
          res.json(data)
        }
      });
    }
    else {
      data[0].overspeeding = parseInt(data[0].overspeeding);
      data[0].harsh_braking = parseInt(data[0].harsh_braking);
      res.json(data)
    }
  }).catch(function(err){
    console.log(err);
    res.status(500).send("Internal server errror")
  })
};

exports.getViolations = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const groupCondition = " g.id in "+ groupIds;
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis || moment().tz(utils.timezone.default, false).endOf('day').unix();

  const requiredKeys = ['start_tis'];
  const dataValid = utils.validateMinPayload(requiredKeys, req.query);
  if(!dataValid){
    return res.status(400).send("Mandatory fields : "+requiredKeys)
  }

  let query = "WITH my_assets as ( SELECT DISTINCT a.id from groups g "+
        " INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected"+
        " INNER JOIN assets a on a.id = agm.fk_asset_id and a.active"+
        " WHERE a.active and g.fk_c_id = "+ fk_c_id +" and "+ groupCondition +
        " )" ;
  query = query + 
    `
    select SUM(tot_osf) + SUM(tot_hbk) count, start_tis
    from assets_reports_data
    where fk_asset_id in (select id from my_assets) and start_tis >= $start_tis and end_tis <= $end_tis
    group by 2
    order by start_tis desc
    `;
  models.sequelize.query(query, {
    bind:{
      start_tis: start_tis,
      end_tis: end_tis
    },type: models.sequelize.QueryTypes.SELECT}).then(function(data){

    _.forEach(data, function(j) {
      j.count = parseInt(j.count);
      j.day = moment.unix(j.start_tis).tz(utils.timezone.default, false).format("ddd");
      j.time = moment.unix(j.start_tis).tz(utils.timezone.default, false).format("h a")
    
    });
    res.send(data)
  }).catch(function (err) {
    res.status(500).send(err)
  })
};

function getAssetStatsForTimeFrame(start_tis, end_tis, query, callback) {
  let q = query +
        `SELECT COALESCE(AVG(CASE WHEN avg_spd > 0 THEN avg_spd END)::INT,0) avg_spd , SUM(tot_dist) tot_dist, tis
            FROM asset_stats
            WHERE fk_asset_id in (select id from my_assets) and tis >= $start_tis and e_tis <= $end_tis
            GROUP BY tis
            ORDER BY tis ASC
            `;
  models.sequelize.query(q, {
    bind: {
      start_tis: start_tis,
      end_tis: end_tis
    }, type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    callback(null, data)
  }).catch(function (err) {
    callback(err)
  })
}

function getAssetStatsForToday(start_tis, end_tis, query, callback) {

  if (end_tis < moment().tz(utils.timezone.default, false).startOf('day').unix()) {
    return callback(null, null)
  }
  let q = query +
        `SELECT COALESCE(SUM(tot_dist),0) tot_dist, COALESCE(AVG(CASE WHEN avg_spd > 0 THEN avg_spd END)::INT,0) avg_spd
            FROM assets_reports_data
            WHERE start_tis >= $start_tis and end_tis <= $end_tis
            AND fk_asset_id IN (select id from my_assets) 
            `;
  models.sequelize.query(q, {
    bind: {
      start_tis: moment().tz(utils.timezone.default, false).startOf('day').unix(),
      end_tis: moment().tz(utils.timezone.default, false).unix()
    }, type: models.sequelize.QueryTypes.SELECT
  }).spread(function (data) {
    data.tis = moment().tz(utils.timezone.default, false).startOf('day').unix();
    callback(null, data)

  }).catch(function (err) {
    callback(err)
  })

}

function addNullDatesToAvgSpeedDistance(params, data) {
  let currentDate = moment.unix(params.last_7_days_tis), end_date = moment.unix(params.end_tis);
  data = data ? data : [];
  while (currentDate.isBefore(end_date)) {
    let dayStart = currentDate.clone();
    let dayTis = dayStart.startOf('day').unix();
    if(!_.find(data, { tis: dayTis})){
      let x = {
        day_format: dayStart.format('D MMM YYYY'),
        tis: dayTis
      };
      x[params.value_key] = 0;
      data.push(x)
    }
    currentDate.add(1, 'day');
  }
  return data
}

exports.getAvgSpeedDistance = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let groupCondition = " g.id in " + groupIds;
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis || moment().tz(utils.timezone.default, false).endOf('day').unix();
  const gid = req.query.gid;
  const speed_data = [];
  const distance_data = [];
  let output = {};
  const fk_asset_id = parseInt(req.query.fk_asset_id) || null;
  const last7DaysTis = moment.unix(end_tis).subtract(7, 'days').startOf('day').unix();
  const requiredKeys = ['start_tis', 'end_tis'];
  const dataValid = utils.validateMinPayload(requiredKeys, req.query);
  if (!dataValid) {
    return res.status(400).send("Mandatory fields : " + requiredKeys)
  }
  if (_.isString(gid)) {
    groupCondition = "g.gid = '" + gid + "'"
  }
  const assetType = req.query.type;
  const assetCondition = utils.getAssetConditions(fk_asset_id,assetType);

  let query = "WITH my_assets as ( SELECT DISTINCT a.id from groups g " +
        " INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected" +
        " INNER JOIN assets a on a.id = agm.fk_asset_id and a.active" +
        " WHERE a.active and g.fk_c_id = " + fk_c_id + " and " + groupCondition + assetCondition +
        " )";

  let task = {
    asset_stats: function (callback) {
      getAssetStatsForTimeFrame(start_tis, end_tis, query, callback)
    }
  };
  if ((moment.unix(end_tis)).tz(utils.timezone.default, false).startOf('day').isSame((moment.unix(moment().unix())).tz(utils.timezone.default, false).startOf('day'), 'day')) {
    task.current_day_stats = function (callback) {
      getAssetStatsForToday(start_tis, end_tis, query, callback)
    };
    console.log("same day");
  } else {
    console.log("not same day");
  }
  async.parallel(
    task
    , function (err, data) {
      if (err) {
        console.log(err);
        return res.status(500).send()
      }
      if (data.current_day_stats !== null) {
        data.asset_stats.push(data.current_day_stats)
      }
      try {
        _.forEach(data.asset_stats, function (j) {
          if (j) {
            speed_data.push({
              avg_speed: parseInt(j.avg_spd),
              tis: j.tis,
              day_format: moment.unix(j.tis).tz(utils.timezone.default, false).format("D MMM YYYY")
            });
            distance_data.push({
              tot_dist: parseInt(j.tot_dist) * 1000, // convert to metres
              tis: j.tis,
              day_format: moment.unix(j.tis).tz(utils.timezone.default, false).format("D MMM YYYY")
            });
          }
        });
        if (data.asset_stats.length > 0) {
          output = {
            speed_data: _.sortBy(addNullDatesToAvgSpeedDistance({value_key: 'avg_speed', last_7_days_tis: last7DaysTis, end_tis: end_tis},speed_data), ['tis']),
            distance_data: _.sortBy(addNullDatesToAvgSpeedDistance({value_key: 'tot_dist', last_7_days_tis: last7DaysTis, end_tis: end_tis},distance_data), ['tis'])
          }
        }
      } catch (e) {
        console.log(e)
      }
      res.send(output)
    })
};

exports.getDaywiseAssetStats = function (req, res) {
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const fk_c_id = req.headers.fk_c_id;
  const start_tis = req.query.start_tis;
  const emailId = req.headers.email;
  const current_tis = moment().unix();
  let output = [];
  const select_fields = "ast.tis, COALESCE(CAST(AVG(ast.avg_spd) as int),0) as avg_spd,  COALESCE(CAST(sum(ast.tot_dist) as int),0) as total_distance ";
  const query = getAssetsPastAnalytics(fk_c_id, groupIds, start_tis, current_tis, select_fields,"") +
      `group by ast.tis
                  order by ast.tis asc`;
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    output = data;
    assetService.getAllCurrentAssetStats(fk_c_id, groupIds, emailId," ",null, function (err, result) {
      if (err) {
        res.status(err.code).send(err.message)
      }
      else {
        output.push({
          tis: current_tis,
          avg_spd: result.avgSpd,
          total_distance: Math.round(result.totDist)
        });
        res.json(output)
      }
    })
  }).catch(function (err) {
    res.status(500).send(err)
  })
};


exports.getAssetUtilisation = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let groupCondition = " g.id in " + groupIds;
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis || moment().tz(utils.timezone.default, false).endOf('day').unix();
  const gid = req.query.gid;
  const requiredKeys = ['start_tis', 'end_tis'];
  const dataValid = utils.validateMinPayload(requiredKeys, req.query);
  if (!dataValid) {
    return res.status(400).send("Mandatory fields : " + requiredKeys)
  }

  if (_.isString(gid)) {
    groupCondition = "g.gid = '" + gid + "'"
  }

  const assetType = req.query.type;
  const assetCondition = utils.getAssetConditions(null, assetType);
  async.parallel({
    total_assets: function (cb) {
      let query = `SELECT count(DISTINCT a.id) total_asset_count from groups g 
          INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
          INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
          WHERE a.active and g.fk_c_id = ` + fk_c_id + ` and  ` + groupCondition + assetCondition;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (data) {
        cb(null, data.total_asset_count)
      }).catch(function (err) {
        res.status(500).send(err.message)
      })

    },
    asset_stats: function (cb) {
      let query = `select ard.fk_asset_id, to_char(to_timestamp(ard.start_tis) at time zone ${utils.timezone.default}, 'YYYY-MM-DD') AS days 
          from groups g  
          INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
          INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
          left join assets_reports_data ard on ard.fk_asset_id = a.id
          WHERE 
          ard.start_tis between $start_tis and $end_tis
          and g.fk_c_id = ` + fk_c_id + ` and  ` + groupCondition + assetCondition +
              ` group by ard.fk_asset_id, to_char(to_timestamp(ard.start_tis) at time zone ${utils.timezone.default}, 'YYYY-MM-DD')
          having SUM(ard.tot_dist) >= 50
          order by to_char(to_timestamp(ard.start_tis) at time zone ${utils.timezone.default}, 'YYYY-MM-DD') asc`;

      models.sequelize.query(query, {
        bind: {
          start_tis: start_tis,
          end_tis: end_tis
        }, type: models.sequelize.QueryTypes.SELECT
      }).then(function (data) {
        cb(null, data)
      }).catch(function (err) {
        res.status(500).send(err.message)
      })
    },
    trips: function (cb) {
      let query = `select tp.fk_asset_id::INT,  to_char(to_timestamp(tp.start_tis) at time zone ${utils.timezone.default}, 'YYYY-MM-DD') AS days 
          from groups g  
          INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
          INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
          left join trip_plannings tp on tp.fk_asset_id = a.id and tp.status != 'scheduled'
          WHERE 
          tp.start_tis between $start_tis and $end_tis
          and g.fk_c_id = ` + fk_c_id + ` and  ` + groupCondition + assetCondition +
              ` group by tp.fk_asset_id, to_char(to_timestamp(tp.start_tis) at time zone ${utils.timezone.default}, 'YYYY-MM-DD')
          order by to_char(to_timestamp(tp.start_tis) at time zone ${utils.timezone.default}, 'YYYY-MM-DD') asc`;

      models.sequelize.query(query, {
        bind: {
          start_tis: start_tis,
          end_tis: end_tis
        }, type: models.sequelize.QueryTypes.SELECT
      }).then(function (data) {
        cb(null, data)
      }).catch(function (err) {
        res.status(500).send(err.message)
      })
    }
  }, function (err, result) {
    let util_data = [];
    let total_assets = Number(result.total_assets);
    result.asset_stats = _.concat(result.asset_stats, result.trips);
    result.asset_stats = _.uniqWith(result.asset_stats, _.isEqual);
    result.asset_stats = _.countBy(result.asset_stats, 'days');

    _.forOwn(result.asset_stats, function (value, key) {
      let obj = {};
      obj.utilisation = Number(((value / total_assets) * 100).toFixed(2));
      obj.tis = moment(key).tz(utils.timezone.default, false).unix();
      obj.day_format= moment(key).tz(utils.timezone.default, false).format("D MMM YYYY");
      util_data.push(obj)
    });

    res.json({
      utilisation: util_data
    })
  })
};


exports.getAssetDistanceLeaderboard = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const groupCondition = " g.id in " + groupIds;
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis || moment().tz(utils.timezone.default, false).unix();
 
  const requiredKeys = ['start_tis'];
  const dataValid = utils.validateMinPayload(requiredKeys, req.query);
  if (!dataValid) {
    return res.status(400).send("Mandatory fields : " + requiredKeys)
  }

  let query =
      `
          SELECT  ROUND(SUM(ard.tot_dist)) distance, a.lic_plate_no, a.id, atp.type, 'km' as unit
          FROM groups g  
          INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id and agm.connected 
          INNER JOIN assets a on a.id = agm.fk_asset_id and a.active
          INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id
          INNER JOIN assets_reports_data ard on ard.fk_asset_id = a.id
          WHERE a.active and g.fk_c_id = ` + fk_c_id + ` and  ` + groupCondition + ` and (ard.start_tis between $start_tis and $end_tis)
          GROUP BY a.lic_plate_no, a.id, atp.type
          HAVING ROUND(SUM(ard.tot_dist)) > 0
          ORDER BY distance DESC
          LIMIT 10
          `;

  models.sequelize.query(query, {
    bind: {
      start_tis: start_tis,
      end_tis: end_tis
    }, type: models.sequelize.QueryTypes.SELECT
  }).then(function (data) {
    res.send(data)
  }).catch(function (err) {
    res.status(500).send(err.message)
  })
};
