const async = require('async');
const models = require('../../../db/models/index');
const nuRedis = require('../../../helpers/nuRedis');

/**
 * Api to show current activity of consignment trackers on the dashboard monitor 
 * @param {*} req 
 * @param {*} res 
 */
exports.getConsignmentsOnMap = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  let query = `SELECT DISTINCT ON (cmvw.fk_entity_id) tp.status as trip_status, e.e_id, u.name, cmvw.fk_consignment_id
                FROM consignment_mapping_vw cmvw
                INNER JOIN entities e on cmvw.fk_entity_id = e.id
                LEFT JOIN consignments c on cmvw.fk_consignment_id = c.id
                LEFT JOIN trip_plannings tp on cmvw.fk_consignment_id = tp.fk_asset_id 
                LEFT JOIN asset_types at on tp.fk_asset_type_id = at.id and at.type = 'Consignment'
                LEFT JOIN users u on cmvw.fk_u_id = u.id
                inner join (
                    select fk_entity_id, max(tis) as max_tis 
                    from consignment_mapping_vw 
                    where fk_c_id = `+ fk_c_id +`
                    group by fk_entity_id
                ) latest_record on (latest_record.fk_entity_id = cmvw.fk_entity_id and latest_record.max_tis = cmvw.tis) OR cmvw.tis is null
                WHERE cmvw.fk_c_id =`+ fk_c_id;   
                
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {

    async.each(data, function(value, callback){
            
      nuRedis.getEntityDetails(value.e_id, function (err, result) {
        if(result){
          value.lat = result.lat;
          value.lon = result.lon;
          value.tis = result.tis;
          value.lname = result.lname;
          value.status = result.status;
          callback()
        }else{
          callback()
        }
      })

    }, function(){
      res.json(data)
    })
  }).catch(function(err){
    console.log(err);
    res.status(500).send("Internal server error")
  });
};