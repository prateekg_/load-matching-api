/**
 * Created by Haston Silva on 28/06/17.
 * Place Dashboard
 */

const utils = require('../../../helpers/util');
const async = require('async');
const models = require('../../../db/models/index');
const nuRedis = require('../../../helpers/nuRedis');
const _ = require('lodash');
const geolib = require('geolib');
const LOGGER = require('../../../helpers/logger');

function convertLocArrayToTuple(locData) {
  let assetLocData = "";
  let count = 0;
  locData.forEach(function (value) {
    if(count !== 0 && count !== locData.length){ assetLocData += "," }
    let lname = value.lname.replace(/'/g, "''");
    assetLocData += "('" + value.aid + "','"+ lname +"','"+ value.status +"',"+ value.lat + ","+ value.lon +",'"+ value.ign +"',"+ value.spd +","+ value.osf +","+ value.tis +"," + value.heading +" )";
    count++
  });
  return assetLocData
}

exports.getAPlaceRealtime = function (req, res) {

  const fk_poi_id = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  if(!fk_poi_id) {
    return res.status(400).send('Invalid place ID');
  }else{

    async.parallel({
      poi: function (cb) {
        let query = "SELECT name from pois where id = "+ fk_poi_id +" LIMIT 1";

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (poi) {
          if(poi){
            cb(null, poi)
          }else{
            cb({
              code: 400,
              msg: "Invalid POI ID"
            })
          }
        }).catch(function (err) {
          cb({
            code: 404,
            msg: err
          })
        })

      },
      assets: function (cb) {
        let query = "SELECT distinct a.id, atm.id as tmp, case when json_array_length(atm.trips) > 0 then true " +
                    " else false end as ontrip, case when aps.poi_stat >= 0 then true else false end as dwelling" +
                    " FROM groups g "+
                    " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
                    " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
                    " LEFT JOIN asset_trip_maps  atm on atm.fk_asset_id = a.id" +
                    " LEFT JOIN assets_pois_stats aps on aps.fk_asset_id = a.id and aps.exit_tis is null and aps.fk_poi_id = "+ fk_poi_id +
                    " WHERE g.active AND "+ groupCondition + " AND a.id is not null" +
                    " GROUP BY a.id, atm.id, aps.id";

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (assets) {
          cb(null, assets)
        }).catch(function (err) {
          cb({
            code: 404,
            msg: err
          })
        })
      },
      in_out: function (cb) {
        let query = "SELECT * FROM (  "+
                        " SELECT a.id, case when json_array_length(tp.waypoints) > 0 then tp.waypoints->0->>'fk_poi_id' end as fk_poi_id_origin,"+
                        " case when json_array_length(tp.waypoints) > 0 then tp.waypoints->(json_array_length(tp.waypoints)-1)->>'fk_poi_id' end as fk_poi_id_destination"+
                        " FROM groups g  LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected  " +
                        " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
                        " LEFT JOIN asset_trip_maps as atm on atm.fk_asset_id = a.id" +
                        " LEFT JOIN json_array_elements_text(atm.trips) x ON TRUE  " +
                        " LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id AND tp.id = to_json(x::json->'id')::text::int " +
                        " WHERE g.active AND " + groupCondition + " AND tp.status = 'inprogress' "+
                    " ) d " +
                    " WHERE (d.fk_poi_id_origin::int = "+ fk_poi_id +" or d.fk_poi_id_destination::int = "+ fk_poi_id +")" +
                    " GROUP BY d.id, d.fk_poi_id_origin,d.fk_poi_id_destination";

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
          cb(null, data)
        }).catch(function (err) {
          console.error(err);
          cb({
            code: 404,
            msg: err
          })
        })
      }
    }, function (err, result) {

      if(err){
        LOGGER.error(err);
        res.status(err.code || 500).send(err.msg || 'Internal server error')
      }

      let total_assets = _.size(result.assets) || 0;
      let total_unassigned = _.size(_.filter(result.assets, {'ontrip': false}));
      res.json({
        name: result.poi.name || '',
        totalAssets: total_assets,
        totalUnassigned: total_unassigned,
        totalInbound: _.size(_.filter(result.in_out, {'fk_poi_id_destination': fk_poi_id})),
        totalOutbound: _.size(_.filter(result.in_out, {'fk_poi_id_origin': fk_poi_id})),
        totalDwelling: _.size(_.filter(result.assets, {'dwelling': true})),
        fleetAssigned: Math.round((total_assets - total_unassigned)/ total_assets * 100) || 0
      })
    })
  }
};

exports.getAPlaceMap = function (req, res) {

  /*
     outbound: place code i.e ship to code from pois company details
     inbound: place code i.e consigner code from pois company details
     */

  const fk_poi_id = req.query.id;
  const searchKeyword = req.query.q || '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const status = req.query.status;

  let statusCondition;

  if(!fk_poi_id) {
    return res.status(400).send('Invalid place ID');
  }

  switch(status){
  case 'inbound':
    statusCondition = "  p_destination.id = " + fk_poi_id;
    break;
  case 'outbound':
    statusCondition = "  p_origin.id = " + fk_poi_id;
    break;
  case 'dwelling':
    statusCondition = "";
    break;
  default:
    return res.status(400).send('Invalid status');
  }

  async.auto({
    poi: function (cb) {
      let query = ' SELECT p.fk_c_id, p.id, p.geo_loc, p.priv_typ, pt.type, p.name, p.addr' +
                ' FROM pois P' +
                ' LEFT JOIN poi_types pt ON pt.id = P.typ' +
                ' INNER JOIN poi_group_mappings pgm on pgm.fk_poi_id = p.id and pgm.connected' +
                ' INNER JOIN groups g on g.id = pgm.fk_group_id AND g.active AND ' + groupCondition +
                ' WHERE P.active is TRUE AND P.id = ' + fk_poi_id + '' +
                ' GROUP BY p.id, pt.id' +
                ' order by fk_c_id desc limit 1';

      models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT})
        .spread(function (poi_data) {
          try {


            if (poi_data) {
              let poi = {};
              let coordinates = {lat: null, lon: null};
              let data = [];
              let geo_obj = [];
              if (poi_data.geo_loc.type === "Polygon") {
                poi_data.geo_loc.coordinates.forEach(function (points) {
                  points.forEach(function (point) {
                    let loc_obj = {};
                    loc_obj.lon = point[0];
                    loc_obj.lat = point[1];
                    geo_obj.push(loc_obj);
                    data.push({
                      latitude: point[1],
                      longitude: point[0]
                    })
                  });
                });

                let geo_data = geolib.getCenter(data);
                coordinates.lat = Number(geo_data.latitude) || null;
                coordinates.lon = Number(geo_data.longitude) || null;

              } else {
                let loc_obj = {};
                loc_obj.lon = poi_data.geo_loc.coordinates[0];
                loc_obj.lat = poi_data.geo_loc.coordinates[1];
                geo_obj.push(loc_obj);

                coordinates.lat = poi_data.geo_loc.coordinates[1];
                coordinates.lon = poi_data.geo_loc.coordinates[0];
              }
              poi.id = poi_data.id;
              poi.type = poi_data.type;
              poi.geofence = geo_obj;
              poi.coordinates = coordinates;
              poi.category = poi_data.fk_c_id ? 'company' : 'general';

              cb(null, poi)
            } else {
              cb({
                code: 404,
                msg: 'Not Found'
              })
            }
          }catch (e){
            console.log("ERROR ", e)
          }
        }).catch(function (err) {
          cb(err)
        })
    },
    location_data: function (cb) {
      let query = "SELECT a.asset_id FROM groups g "+
                " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
                " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
                " WHERE g.active AND "+ groupCondition +
                " GROUP BY asset_id";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (assets) {
        if(assets.length > 0){
          let locData = [];
          async.forEachOf(assets, function (asset, i, callback) {
            nuRedis.getAssetDetails(asset.asset_id, function (err, value) {
              if(value){
                locData.push(value)
              }
              callback()
            })
          }, function () {
            cb(null, convertLocArrayToTuple(locData))
          });
        }else{
          cb({
            code: 404,
            msg: 'Not Found'
          })
        }
      }).catch(function (err) {
        cb(err)
      })
    },
    assets: ['location_data',function (results, cb) {
      let loc_data = results.location_data;
      if(loc_data.length === 0) {
        loc_data = '(null, null,null,null,null,null,0,false,null,null)'
      }

      let query = "SELECT json_build_object('name',p_origin.name,'code',p_origin.company_fields->'consignerCode') as origin," +
                " json_build_object('name',p_destination.name,'code',p_destination.company_fields->'shipToCode') as destination," +
                " d.*, count(*) OVER()::INT AS total_count FROM (  "+

                " SELECT  case when json_array_length(tp.waypoints) > 0 then tp.waypoints->0->>'fk_poi_id' end as fk_poi_id_origin,"+
                " case when json_array_length(tp.waypoints) > 0 then tp.waypoints->(json_array_length(tp.waypoints)-1)->>'fk_poi_id' end as fk_poi_id_destination,"+
                " json_build_object('id', a.id,'asset_id',a.asset_id, 'name',a.name,'licNo', a.lic_plate_no ,'type',atp.type, 'operator', c.cname, 'lname', loc.lname,'lat', loc.lat, 'lon', loc.lon, 'ign', " +
                " COALESCE(loc.ign, 'A'),'spd', COALESCE(loc.spd,0), 'osf', COALESCE(loc.osf, false), 'status', COALESCE(loc.status, 'stationary'), 'tis', loc.tis," +
                " 'heading',  COALESCE(loc.heading::INT,0)) as asset , json_build_object('id',tp.id, 'invoiceNo', iea.invoice_no,'end_tis',tp.end_tis," +
                " 'eta',tp.trip_log->'ETA','delayed_by', tp.trip_log->'delayed_by', 'delayed', tp.trip_log->'delayed') as trip"+
                " FROM groups g  LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected  " +
                " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
                " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id" +
                " LEFT JOIN companies c on c.id = a.fk_c_id LEFT JOIN asset_trip_maps as atm on atm.fk_asset_id = a.id" +
                " LEFT JOIN json_array_elements_text(atm.trips) x ON TRUE  " +
                " LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id AND tp.id = to_json(x::json->'id')::text::int " +
                " LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id " +
                " LEFT OUTER JOIN (   SELECT * FROM (VALUES " + loc_data +") AS t " +
                "(aid, lname,status,lat,lon,ign,spd,osf,tis,heading) ) as loc on a.asset_id = loc.aid "+

                " WHERE g.active AND " + groupCondition + " AND tp.status = 'inprogress' "+
                " AND (lower(loc.lname) ilike '%" + searchKeyword + "%' or lower(a.lic_plate_no) ilike '%" + searchKeyword + "%'" +
                " or iea.*::TEXT ilike '%" + searchKeyword + "%'  or lower(a.name) ilike '%" + searchKeyword + "%' or " +
                " lower(c.cname) ilike '%" + searchKeyword + "%' or lower(c.transporter_code) ilike '%" + searchKeyword + "%' )"+
                " group by iea.invoice_no, a.id, c.id, loc.lname, loc.lat, loc.lon, loc.ign, loc.spd, loc.osf, loc.status, loc.tis, loc.heading, atm.id, tp.id, atp.id"+
                " ) d " +
                " left join pois p_origin on p_origin.id = d.fk_poi_id_origin::int "+
                " left join pois p_destination on p_destination.id = d.fk_poi_id_destination::int" +

                " WHERE " + statusCondition ;

      if(status === 'dwelling'){
        query = "SELECT json_build_object('name',p_origin.name,'code',p_origin.company_fields->'consignerCode') as origin," +
                    " json_build_object('name',p_destination.name,'code',p_destination.company_fields->'shipToCode') as destination," +
                    " d.*, count(*) OVER()::INT AS total_count FROM (  "+

                    " SELECT  aps.entry_tis, aps.latest_tis,case when json_array_length(tp.waypoints) > 0 then tp.waypoints->0->>'fk_poi_id' end as fk_poi_id_origin,"+
                    " case when json_array_length(tp.waypoints) > 0 then tp.waypoints->(json_array_length(tp.waypoints)-1)->>'fk_poi_id' end as fk_poi_id_destination,"+
                    " json_build_object('id', a.id,'asset_id',a.asset_id, 'name',a.name, 'licNo', a.lic_plate_no ,'type',atp.type, 'operator', c.cname, 'lname', loc.lname,'lat', loc.lat, 'lon', loc.lon, 'ign', " +
                    " COALESCE(loc.ign, 'A'),'spd', COALESCE(loc.spd,0), 'osf', COALESCE(loc.osf, false), 'status', COALESCE(loc.status, 'stationary'), 'tis', loc.tis," +
                    " 'heading',  COALESCE(loc.heading::INT,0)) as asset , json_build_object('id',tp.id, 'invoiceNo', iea.invoice_no,'end_tis',tp.end_tis," +
                    " 'eta',tp.trip_log->'ETA','delayed_by', tp.trip_log->'delayed_by', 'delayed', tp.trip_log->'delayed') as trip"+
                    " FROM groups g  LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected  " +
                    " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active" +
                    " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id" +
                    " INNER JOIN assets_pois_stats aps on aps.fk_asset_id = a.id AND aps.fk_poi_id = "+ fk_poi_id +" AND exit_tis is NULL"+

                    " LEFT JOIN companies c on c.id = a.fk_c_id LEFT JOIN asset_trip_maps as atm on atm.fk_asset_id = a.id" +
                    " LEFT JOIN json_array_elements_text(atm.trips) x ON TRUE  " +
                    " LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id AND tp.id = to_json(x::json->'id')::text::int " +
                    " LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id " +
                    " LEFT OUTER JOIN (   SELECT * FROM (VALUES " + loc_data +") AS t " +
                    "(aid, lname,status,lat,lon,ign,spd,osf,tis,heading) ) as loc on a.asset_id = loc.aid "+

                    " WHERE g.active AND " + groupCondition +
                    " AND (lower(loc.lname) ilike '%" + searchKeyword + "%' or lower(a.lic_plate_no) ilike '%" + searchKeyword + "%'" +
                    " or iea.*::TEXT ilike '%" + searchKeyword + "%'  or lower(a.name) ilike '%" + searchKeyword + "%' or " +
                    " lower(c.cname) ilike '%" + searchKeyword + "%' or lower(c.transporter_code) ilike '%" + searchKeyword + "%' )"+
                    " group by iea.invoice_no, a.id, c.id, loc.lname, loc.lat, loc.lon, loc.ign, loc.spd, loc.osf, loc.status, loc.tis, loc.heading, atm.id, tp.id, aps.id, atp.id"+
                    " ) d " +
                    " left join pois p_origin on p_origin.id = d.fk_poi_id_origin::int "+
                    " left join pois p_destination on p_destination.id = d.fk_poi_id_destination::int";
      }

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {

        let assets = [];
        data.forEach(function (i) {
          assets.push(i.asset)
        });

        cb(null, _.uniqBy(assets,'id'));
      }).catch(function (err) {
        cb(err)
      })
    }]},function (err, results) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }else{
        LOGGER.error(err);
        res.status(500).send()
      }
    }else{
      results.location_data = undefined;
      res.json(results)
    }
  })
};

exports.getAPlaceList = function (req, res) {

  /*
     outbound: place code i.e ship to code from pois company details
     inbound: place code i.e consigner code from pois company details
     */

  const fk_poi_id = req.query.id;
  const searchKeyword = req.query.q || '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const status = req.query.status;

  let statusCondition;

  if(!fk_poi_id) {
    return res.status(400).send('Invalid place ID');
  }

  switch(status){
  case 'inbound':
    statusCondition = "  p_destination.id = " + fk_poi_id;
    break;
  case 'outbound':
    statusCondition = "  p_origin.id = " + fk_poi_id;
    break;
  case 'dwelling':
    statusCondition = "";
    break;
  default:
    return res.status(400).send('Invalid status');
  }

  async.waterfall([
    function (cb) {
      let query = "SELECT a.asset_id FROM groups g "+
                " LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected "+
                " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
                " WHERE g.active AND "+ groupCondition +
                " GROUP BY asset_id";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (assets) {
        if(assets.length > 0){
          let locData = [];
          async.forEachOf(assets, function (asset, i, callback) {
            nuRedis.getAssetDetails(asset.asset_id, function (err, value) {
              if(value){
                locData.push(value)
              }
              callback()
            })
          }, function () {
            cb(null, convertLocArrayToTuple(locData))
          });
        }else{
          cb({
            code: 404,
            msg: 'Not Found'
          })
        }
      }).catch(function (err) {
        cb(err)
      })
    },function (loc_data, cb) {

      if(loc_data.length === 0) {
        loc_data = '(null, null,null,null,null,null,0,false,null,null)'
      }

      let query = "SELECT json_build_object('name',p_origin.name,'code',p_origin.company_fields->'consignerCode') as origin," +
                " json_build_object('name',p_destination.name,'code',p_destination.company_fields->'shipToCode') as destination," +
                " d.*, count(*) OVER()::INT AS total_count FROM (  "+

            " SELECT tp.end_tis as sort_key, case when json_array_length(tp.waypoints) > 0 then tp.waypoints->0->>'fk_poi_id' end as fk_poi_id_origin,"+
            " case when json_array_length(tp.waypoints) > 0 then tp.waypoints->(json_array_length(tp.waypoints)-1)->>'fk_poi_id' end as fk_poi_id_destination,"+
            " json_build_object('id', a.id, 'licNo', a.lic_plate_no ,'type',atp.type, 'operator', c.cname, 'lname', loc.lname,'lat', loc.lat, 'lon', loc.lon, 'ign', " +
            " COALESCE(loc.ign, 'A'),'spd', COALESCE(loc.spd,0), 'osf', COALESCE(loc.osf, false), 'status', COALESCE(loc.status, 'stationary'), 'tis', loc.tis," +
            " 'heading',  COALESCE(loc.heading::INT,0)) as asset , json_build_object('id',tp.id, 'invoiceNo', iea.invoice_no,'end_tis',tp.end_tis," +
            " 'eta',tp.trip_log->'ETA','delayed_by', tp.trip_log->'delayed_by', 'delayed', tp.trip_log->'delayed') as trip"+
            " FROM groups g  LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected  " +
            " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active "+
            " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id" +
            " LEFT JOIN companies c on c.id = a.fk_c_id LEFT JOIN asset_trip_maps as atm on atm.fk_asset_id = a.id" +
            " LEFT JOIN json_array_elements_text(atm.trips) x ON TRUE  " +
            " LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id AND tp.id = to_json(x::json->'id')::text::int " +
            " LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id " +
            " LEFT OUTER JOIN (   SELECT * FROM (VALUES " + loc_data +") AS t " +
                "(aid, lname,status,lat,lon,ign,spd,osf,tis,heading) ) as loc on a.asset_id = loc.aid "+

            " WHERE g.active AND " + groupCondition + " AND tp.status = 'inprogress' "+
            " AND (lower(loc.lname) ilike '%" + searchKeyword + "%' or lower(a.lic_plate_no) ilike '%" + searchKeyword + "%'" +
                " or iea.*::TEXT ilike '%" + searchKeyword + "%'  or lower(a.name) ilike '%" + searchKeyword + "%' or " +
            " lower(c.cname) ilike '%" + searchKeyword + "%' or lower(c.transporter_code) ilike '%" + searchKeyword + "%' )"+
            " group by iea.invoice_no, a.id, c.id, loc.lname, loc.lat, loc.lon, loc.ign, loc.spd, loc.osf, loc.status, loc.tis, loc.heading, atm.id, tp.id, atp.id"+
            " ) d " +
                " left join pois p_origin on p_origin.id = d.fk_poi_id_origin::int "+
            " left join pois p_destination on p_destination.id = d.fk_poi_id_destination::int" +

                " WHERE " + statusCondition +
                " ORDER BY sort_key LIMIT " + limit + " OFFSET " + offset;


      if(status === 'dwelling'){
        query = "SELECT json_build_object('name',p_origin.name,'code',p_origin.company_fields->'consignerCode') as origin," +
                    " json_build_object('name',p_destination.name,'code',p_destination.company_fields->'shipToCode') as destination," +
                    " d.*, count(*) OVER()::INT AS total_count FROM (  "+

                    " SELECT tp.end_tis as sort_key, aps.entry_tis, aps.latest_tis,case when json_array_length(tp.waypoints) > 0 then tp.waypoints->0->>'fk_poi_id' end as fk_poi_id_origin,"+
                    " case when json_array_length(tp.waypoints) > 0 then tp.waypoints->(json_array_length(tp.waypoints)-1)->>'fk_poi_id' end as fk_poi_id_destination,"+
                    " json_build_object('id', a.id, 'licNo', a.lic_plate_no , 'type',atp.type, 'operator', c.cname, 'lname', loc.lname,'lat', loc.lat, 'lon', loc.lon, 'ign', " +
                    " COALESCE(loc.ign, 'A'),'spd', COALESCE(loc.spd,0), 'osf', COALESCE(loc.osf, false), 'status', COALESCE(loc.status, 'stationary'), 'tis', loc.tis," +
                    " 'heading',  COALESCE(loc.heading::INT,0)) as asset , json_build_object('id',tp.id, 'invoiceNo', iea.invoice_no,'end_tis',tp.end_tis," +
                    " 'eta',tp.trip_log->'ETA','delayed_by', tp.trip_log->'delayed_by', 'delayed', tp.trip_log->'delayed') as trip"+
                    " FROM groups g  LEFT JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected  " +
                    " LEFT JOIN assets a ON a.id = agm.fk_asset_id and a.active" +
                    " INNER JOIN asset_types atp on atp.id = a.fk_ast_type_id" +
                    " INNER JOIN assets_pois_stats aps on aps.fk_asset_id = a.id AND aps.fk_poi_id = "+ fk_poi_id +" AND exit_tis is NULL"+

                    " LEFT JOIN companies c on c.id = a.fk_c_id LEFT JOIN asset_trip_maps as atm on atm.fk_asset_id = a.id" +
                    " LEFT JOIN json_array_elements_text(atm.trips) x ON TRUE  " +
                    " LEFT JOIN trip_plannings tp on tp.fk_asset_id = a.id AND tp.id = to_json(x::json->'id')::text::int " +
                    " LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id " +
                    " LEFT OUTER JOIN (   SELECT * FROM (VALUES " + loc_data +") AS t " +
                    "(aid, lname,status,lat,lon,ign,spd,osf,tis,heading) ) as loc on a.asset_id = loc.aid "+

                    " WHERE g.active AND " + groupCondition + " AND tp.status = 'inprogress' "+
                    " AND (lower(loc.lname) ilike '%" + searchKeyword + "%' or lower(a.lic_plate_no) ilike '%" + searchKeyword + "%'" +
                    " or iea.*::TEXT ilike '%" + searchKeyword + "%'  or lower(a.name) ilike '%" + searchKeyword + "%' or " +
                    " lower(c.cname) ilike '%" + searchKeyword + "%' or lower(c.transporter_code) ilike '%" + searchKeyword + "%' )"+
                    " group by iea.invoice_no, a.id, c.id, loc.lname, loc.lat, loc.lon, loc.ign, loc.spd, loc.osf, loc.status, loc.tis, loc.heading, atm.id, tp.id, aps.id, atp.id"+
                    " ) d " +
                    " left join pois p_origin on p_origin.id = d.fk_poi_id_origin::int "+
                    " left join pois p_destination on p_destination.id = d.fk_poi_id_destination::int" +

                    " ORDER BY sort_key LIMIT " + limit + " OFFSET " + offset;
      }


      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {

        let totalRecords = 0;
        data.forEach(function (i) {
          totalRecords = Number(i.total_count);

          let trip = i.trip;
          i.trip.eta = {
            tis: trip.end_tis + (trip.delayed_by || 0),
            duration: Math.round(trip.delayed_by / 60) || 0,
            delayed: trip.delayed || false
          };

          i.trip.loaded = i.trip.invoiceNo ? 'Loaded' : 'Unloaded';

          i.entryTis = i.entry_tis || null;
          i.dwellDuration = i.entry_tis && i.latest_tis ? Math.round((i.latest_tis - i.entry_tis) / 60) : null;

          i.trip.end_tis = undefined;
          i.trip.delayed_by = undefined;
          i.trip.delayed = undefined;
          i.total_count = undefined;
          i.fk_poi_id_origin = undefined;
          i.fk_poi_id_destination = undefined;
          i.entry_tis = undefined;
          i.latest_tis = undefined;
          i.sort_key = undefined
        });

        cb(null,{
          total_count: totalRecords,
          data: data
        });
      }).catch(function (err) {
        cb(err)
      })
    }],function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }else{
        LOGGER.error(err);
        res.status(500).send()
      }
    }else{
      res.json(result)
    }
  })
};