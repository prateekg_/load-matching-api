const models = require('../../db/models');
const async = require('async');
const _ = require('lodash');
const moment = require('moment');

exports.getAllUser = function (req, res) {

  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  
  async.auto({
    users: function (callback) {
      let query =
              `SELECT u.id, name, firstname, lastname, email, COALESCE(lm.last_msg_tis, 0) as last_msg_tis, u.last_online_tis
              FROM users u
              LEFT JOIN (
                  SELECT fk_reciever_id, max(tis) as last_msg_tis FROM chats GROUP BY fk_reciever_id
              ) lm on lm.fk_reciever_id = u.id
              WHERE u.fk_c_id = ${fk_c_id} AND u.active AND u.is_internal IS FALSE AND u.id != ${fk_u_id}
              GROUP BY u.id, lm.last_msg_tis;`;
  
      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .then(function (result) {
          callback(null, result)
        }).catch(function (err) {
          callback(err)
        })
    }
  }, function (err, result) {
    if(err){
      return res.status(500).send()
    }
  
    result.users.forEach(function (i) {
      let is_online = false;
      if(i.last_online_tis && (moment().unix() - i.last_online_tis) < 70){
        is_online = true;
      }
      i.is_online = is_online;
    });
    res.json({users: _.orderBy(result.users,['is_online','last_msg_tis'],['desc','desc'])})
  })
};