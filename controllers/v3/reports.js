const moment = require('moment-timezone');
const async = require('async');
const _ = require('lodash');
const json2csv = require('json2csv');
const models = require('../../db/models/index');
const APP_LOGGER = require('../../helpers/logger');
const LOGGER = APP_LOGGER.logger;
const utils = require('../../helpers/util');
const fs = require('fs');
const xlsDownload = require('../../helpers/downloadHelper')
const file_base_path = __dirname + '/../../temp_reports/';

exports.getAll = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const start_tis = req.query.start_tis || moment().subtract(1, 'days').startOf('day').unix();
  const end_tis = req.query.end_tis || moment().subtract(1, 'days').endOf('day').unix();
  const type = req.query.type;

  if(!type){
    return res.status(400).send('Enter report type ID')
  }

  let query = "SELECT * FROM reports_data rd" +
        " WHERE rd.fk_c_id = " + fk_c_id + " AND rd.fk_scheduled_report_type_id = " + type + " AND ( rd.start_tis >=" + start_tis + " OR rd.end_tis <= " + end_tis + ")";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    res.json(data)
  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send({
      error: null,
      code: 500,
      message: 'Internal server error'
    })
  })
};

exports.download = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const start_tis = req.query.start_tis || moment().subtract(1, 'days').startOf('day').unix();
  const end_tis = req.query.end_tis || moment().subtract(1, 'days').endOf('day').unix();
  const type = req.query.type;

  if(!type){
    return res.status(400).send('Enter report type ID')
  }

  let query = "SELECT data, type FROM reports_data rd" +
        " WHERE rd.fk_c_id = " + fk_c_id + " AND rd.fk_scheduled_report_type_id = " + type + " AND ( rd.start_tis >=" + start_tis + " OR rd.end_tis <= " + end_tis + ")";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    let finalData = [];
    async.eachSeries(data, function (item, cb) {
      finalData = _.concat(finalData, item.data);
      cb()
    }, function () {

      let fileContent = json2csv({ data: finalData, fields: _.keys(data[0].data[0]) });
      res.attachment('xtz.csv');
      res.status(200).send(fileContent);
    })


  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send({
      error: null,
      code: 500,
      message: 'Internal server error'
    })
  })
};

exports.getTypes = function (req, res) {

  const id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const page = req.query.page || 0;
  const limit = req.query.limit || 4;
  const offset = page * limit;
  const q = req.query.q || '';
  const category = req.query.category;

  if(id){
    let query = `SELECT rt.name as type, rt.summary, rt.template, rt.id as type_id, rt.config FROM report_types rt WHERE rt.id = ${id}`;
    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (data) {
      if(!data){
        res.status(404).send('Not found')
      }else{
        let template = data.template || [];
        data.template = template.map(function (i) {
          return _.pick(i, ['key','name','format'])
        });
        res.json(data)
      }
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send({
        error: null,
        code: 500,
        message: 'Internal server error'
      })
    })
  }else{

    if(['general','custom'].indexOf(category) < 0){
      return res.status(400).send("Category missing")
    }

    let category_condition = category === 'custom' ? " AND rt.fk_c_id = " + fk_c_id : " AND rt.fk_c_id IS NULL";
    let count = 0;
    let query = 
    `SELECT 
      rt.id as type_id, rt.name as type, rt.summary, rt.template, count(*) over () as count 
    FROM report_types rt 
    INNER JOIN features f ON f.id = rt.fk_feature_id 
    INNER JOIN company_plans_vw cfv ON f.name = ANY(cfv.features) AND cfv.fk_c_id = ${fk_c_id} AND cfv.platform = 'app'
    WHERE f.platform = 'app'
    AND lower(rt.name) LIKE '%${q.toLowerCase()}%' ${category_condition}
    ORDER BY rt.name 
    LIMIT ${limit} OFFSET ${offset};`
    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
      data.forEach(function (item) {
        count = parseInt(item.count);
        delete item.count;
        let template = item.template || [];
        item.template = template.map(function (i) {
          return _.pick(i, ['key','name','format'])
        })
      });
      res.json({
        count: count,
        data: data
      })
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send({
        error: null,
        code: 500,
        message: 'Internal server error'
      })
    })
  }
};

exports.getAssetActivity = function (req, res){
  const fk_c_id = req.headers.fk_c_id;
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? ` g.gid = '${gid}'` : ` g.id in ${groupIds}`;
  const output_type = req.query.output_type || 'json'; // json/excel
  let limit_condition = "";
  const start_tis = req.query.start_tis
  const end_tis = req.query.end_tis

  if(output_type === 'json'){
    limit_condition = `LIMIT ${limit} OFFSET ${offset}`
  }

  if(!start_tis){
    return res.status(400).send("Enter Start time")
  }

  if(!end_tis){
    return res.status(400).send("Enter End time")
  }

  let query = `
  WITH my_vehicles as (
    SELECT DISTINCT a.id 
    FROM GROUPS g
    INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected
    INNER JOIN assets a ON a.id = agm.fk_asset_id AND a.active 
    INNER JOIN (
      SELECT DISTINCT a.id 
      FROM GROUPS g 
      INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected 
      INNER JOIN assets a ON a.id = agm.fk_asset_id AND a.active 
      WHERE g.fk_c_id = ${fk_c_id} AND g.is_admin AND agm.connected AND a.active
    ) ca ON ca.id = a.id 
    WHERE a.active AND g.active AND g.fk_c_id = ${fk_c_id} AND ${groupCondition}
  )
  
  SELECT
    COUNT(*) OVER()::INT AS total_count,
    a.lic_plate_no, sum(ard.tot_dist) tot_dist, sum(ard.tot_eng) tot_eng, sum(ard.tot_idle) tot_idle, sum(ard.tot_stat) tot_stat,
    avg(ard.avg_spd) avg_spd, sum(ard.tot_osf) tot_osf, json_agg(ard.max_spd) filter (where ard.max_spd > 0) max_spd,
    json_agg(json_build_object('shift_start_tis', ard.shift_start_tis, 'shift_start_location', ard.shift_start_lname)) filter (where ard.shift_start_tis is not null) as shift_start,
    json_agg(json_build_object('shift_end_tis', ard.shift_end_tis, 'shift_end_location', ard.shift_end_lname)) filter (where ard.shift_end_tis is not null) as shift_end
  FROM assets a 
  LEFT JOIN assets_reports_data ard ON ard.fk_asset_id = a.id AND ard.start_tis BETWEEN ${start_tis} AND ${end_tis}
  WHERE a.id IN (SELECT id FROM my_vehicles)
  GROUP BY a.id 
  ${limit_condition};
  `

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (vehicles) {
    let total_count = 0;
    vehicles.forEach(function(vehicle){
      total_count = vehicle.total_count;

      vehicle.tot_dist = vehicle.tot_dist ? vehicle.tot_dist.toFixed(1) + " Km" : "0 km"

      vehicle.tot_eng = Math.round(vehicle.tot_eng)
      vehicle.tot_idle = Math.round(vehicle.tot_idle)
      vehicle.tot_stat = Math.round(vehicle.tot_stat)
      vehicle.avg_spd = Math.round(vehicle.avg_spd) + " Km/h"
      vehicle.duration = vehicle.tot_eng + vehicle.tot_idle + vehicle.tot_stat

      vehicle.tot_eng = utils.durationConverter(vehicle.tot_eng)
      vehicle.tot_idle = utils.durationConverter(vehicle.tot_idle)
      vehicle.tot_stat = utils.durationConverter(vehicle.tot_stat)
      vehicle.duration = utils.durationConverter(vehicle.duration)

      vehicle.max_spd = vehicle.max_spd ? _.max(vehicle.max_spd) + " Km/h" : "0 Km/h"

      if(vehicle.shift_start){
        let shift_start = _.minBy(vehicle.shift_start,'shift_start_tis')
        vehicle.shift_start_date = moment.unix(shift_start.shift_start_tis).format('MMM DD YYYY')
        vehicle.shift_start_tis = moment.unix(shift_start.shift_start_tis).format('hh:mm A')
        vehicle.shift_start_lname = shift_start.shift_start_location
      }else{
        vehicle.shift_start_date = null
        vehicle.shift_start_tis = null
        vehicle.shift_start_lname = null
      }
      delete vehicle.shift_start

      if(vehicle.shift_end){
        let shift_end = _.maxBy(vehicle.shift_end,'shift_end_tis')
        vehicle.shift_end_date = moment.unix(shift_end.shift_end_tis).format('MMM DD YYYY')
        vehicle.shift_end_tis = moment.unix(shift_end.shift_end_tis).format('hh:mm A')
        vehicle.shift_end_lname = shift_end.shift_end_location
      }else{
        vehicle.shift_end_date = null
        vehicle.shift_end_tis = null
        vehicle.shift_end_lname = null
      }
      delete vehicle.shift_end
        
      delete vehicle.total_count
    })

    if(output_type === 'excel'){
      const file_name = `ALL-ASSETS-REPORT-${fk_c_id}-${moment().unix()}.xls`;
      const file_path = file_base_path + file_name;
      xlsDownload.convertAssetActivityReportToXLS(vehicles, start_tis, end_tis, file_base_path, file_name, function (err) {
        if (err) {
          res.status(err.code).send(err.msg)
        } else {
          res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
          res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
          res.setHeader("Content-Type", "application/vnd.ms-excel");
          res.setHeader("Filename", file_name);

          res.download(file_path, file_name, function (err) {
            if (!err) {
              fs.unlink(file_path)
            }
          })
        }
      })
    }else{
      return res.json({
        total_count: total_count,
        data: vehicles
      })
    }
  }).catch(function(err){
    console.error(err);
    return res.status(500).send()
  })
}