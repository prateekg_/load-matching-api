const models = require('../../db/models/index');
const utils = require('../../helpers/util');
const _ = require('lodash');

exports.getNotifications = function (req, res) {

  let fk_c_id = req.headers.fk_c_id;
  let isPaginated = false;
  let limit = req.query.limit;
  let start_tis = req.query.start_tis;
  let end_tis = req.query.end_tis;

  let fk_vehicle_id = req.query.vehicle_id;
  let vehicleType = req.query.vehicle_type;

  let fk_poi_id = req.query.poi_id;
  let poi_category = req.query.poi_category;

  let fk_trip_id = req.query.trip_id;
  let level = req.query.level;
  let type_id = req.query.type_id;

  let count_query = '';
  let page = parseInt(req.query.page) || 0;
  let offset = 0;
  let q = req.query.q;
  let search_condition = '';
  let level_condition = '';
  let trip_filter = '';
  let vehicle_filter = '';
  let vehicle_type_filter = '';
  let poi_filter = '';
  let date_range_filter = '';
  let poi_category_filter = '';
  let notification_type_filter = '';

  switch(poi_category){
  case 'general':
    poi_category_filter = ` AND p.priv_typ IS FALSE`;
    break;
  case 'company':
    poi_category_filter =  ` AND p.priv_typ`;
    break;
  default:
    poi_category_filter = ''
  }

  if(type_id){
    let ids = [];
    if(typeof type_id === 'string' && Number(type_id)){
      ids.push(type_id);
      notification_type_filter = ` AND sn.fk_smart_notification_type IN (${ids})`
    }else if(typeof type_id === 'object'){
      type_id.forEach(function(i){
        if(Number(i)){
          ids.push(i)
        }
      });
      notification_type_filter = ` AND sn.fk_smart_notification_type IN (${ids})`
    }
  }

  if(fk_trip_id && Number(fk_trip_id)){
    trip_filter = ` AND sn.fk_trip_id = ${fk_trip_id}`
  }

  if(fk_vehicle_id && Number(fk_vehicle_id)){
    vehicle_filter = ` AND sn.fk_asset_id = ${fk_vehicle_id}`
  }

  if(fk_poi_id && Number(fk_poi_id)){
    poi_filter = ` AND sn.fk_poi_id = ${fk_poi_id}`
  }

  if(vehicleType){
    vehicle_type_filter =  ` AND lower(atp.type) = '${vehicleType}'`
  }

  if(q){
    q = q.toLowerCase();
    search_condition = 
    ` AND ( lower(sn.message) like '%${q}%'
        OR lower(a.lic_plate_no) like '%${q}%'
        OR lower(p.name) like '%${q}%'
      )
    `
  }

  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  let groupCondition = gid ? ` g.gid = '${gid}'` : ` g.id in ${groupIds}`;
  let limitCondition = '';

  if(start_tis > 0 && end_tis > 0){
    isPaginated = true;
    date_range_filter = ` AND sn.start_tis BETWEEN ${start_tis} AND ${end_tis}`
  }else{
    limitCondition = `LIMIT ${limit || 10}`
  }

  if(isPaginated){
    limit = limit || 10;
    offset = limit * page;

    count_query = ', count(*) over()::INT as total_count';
    limitCondition = ` LIMIT ${limit || 10} OFFSET ${offset}`
  }

  if(level && ['medium','low','high'].indexOf(level) > -1){
    level_condition = ` AND sn.level = '${level}'`
  }

  let query =
  ` SELECT sn.id, sn.start_tis, sn.end_tis, sn.fk_smart_notification_type as type, sn.message as msg, sn.params as rparams, sn.level as lvl ${count_query}
    FROM smart_notifications sn
    INNER JOIN groups g on g.id = sn.fk_group_id AND g.active
    LEFT JOIN assets a on a.id = sn.fk_asset_id
    LEFT JOIN asset_types atp on atp.id = a.fk_ast_type_id ${vehicle_type_filter}
    LEFT JOIN pois p on p.id = sn.fk_poi_id 
    WHERE ${groupCondition} AND g.fk_c_id = ${fk_c_id} ${search_condition + level_condition + trip_filter + vehicle_filter + poi_filter +
    date_range_filter + poi_category_filter + notification_type_filter}
    ORDER BY sn.id DESC
    ${limitCondition}`;

  models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
    .then(function (data) {
      let total_count = 0;
      data.forEach(function(i){
        total_count = i.total_count;
        delete i.total_count
      });
      res.json({
        total_count: isPaginated ? total_count : _.size(data),
        data: data
      })
    }).catch(function (err) {
      console.error(err);
      res.status(500).send('Internal error');
    });
};
