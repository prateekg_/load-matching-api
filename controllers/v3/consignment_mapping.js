/**
 * Apis for consignment mapping App
 */

const async = require('async');
const models = require('../../db/models');
const _ = require('lodash');
const moment = require('moment');
const request = require('request');
const fs = require('fs');
const env       = process.env.NODE_ENV || 'development';
const config    = require('../../config/index')[env];
const utils = require('../../helpers/util');
const nuRedis = require('../../helpers/nuRedis');

let default_source_trip_radius = (env === 'development') ? 50 : 500;
let default_dest_trip_radius = (env === 'development') ? 10 : 100;
let default_dest_max_opentime = 2592000; // 7days

//Include services
const consignmentService = require('../../services/consignment_service');
const tripPlanningService = require('../../services/trip_planning_service');
const cmaService = require('../../services/cma_service');
const DOWNLOAD_HELPER = require('../../helpers/downloadHelper');
const file_base_path = __dirname + '/../../temp_reports/';
const TABLE_CONFIG = require('../../helpers/table_config');

function getConsignmentAssetType(type, callback){
  let query = `select id from asset_types where type='`+type+`'`;
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function(data){
    callback(data.id)
  })
}


function unmapConsignmentEntity(e_id, fk_c_id, fk_u_id, consignment_id, callback){
  let query = `select cmv.* , tp.status
        from company_device_inventory_vw cmv
        LEFT JOIN consignments c on cmv.fk_consignment_id = c.id 
        LEFT JOIN trip_plannings tp on c.fk_trip_id = tp.id 
        LEFT JOIN entities e on cmv.fk_entity_id = e.id 
        WHERE e.e_id = '`+ e_id + `' and cmv.fk_c_id =`+ fk_c_id + ` and
            case when cmv.fk_consignment_id is not null 
                    then cmv.connected = true 
                else (cmv.connected = false and cmv.fk_consignment_id is null )
            end
        ORDER BY cmv.tis desc limit 1 `;
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function(data){
        
    if(data && data.status === 'completed'){
      let options = {
        uri: config.server_address.wad_server() + '/ad/consignment/mapunmap',
        method: 'POST',
        json: {
          consignment_id: consignment_id,
          entity_id: e_id,
          fk_u_id: fk_u_id,
          force_unmap: true
        },
        headers: {
          'User-Agent': 'request',
          'Authorization': config.token.wad_consign
        }
      };
      request(options, function (error, response, body) {
        if(error){
          callback({
            code: 500,
            msg: error
          })
        }
        else if(body &&  body.status === "Error"){
          callback({
            code: 400,
            msg: body.msg
          })
        }
        else{
          callback(null, body)
        }
      });        
    }else{
      callback({
        code: 403,
        msg: "Trip is not completed"
      })
    }
  }).catch(function(err){
    console.log(err);
    callback({
      code: 500,
      msg: "Internal Server Error"
    })
  })
}
 
/** 
 * Api to map nubot to consignment.
 * It creates consignment and trip and calls WAD api to map consignment to an entity
 * @param {*} req 
 * @param {*} res 
 */
exports.createConsignmentTrip = function(req,res){
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let waypoints = [];
  const host = req.get('Host');

  if(!fk_c_id && !groupIds){
    res.status(400).send("Company id and group ids missing")
  }
  else{

    let newConsignmentRecord = req.body;
        
    let e_id = req.body.e_id;
    let requiredKeys = ['consignor', 'consignee', 'e_id'];
    let dataValid = utils.validateMinPayload(requiredKeys, newConsignmentRecord);
    if(!dataValid)
      return res.status(400).send("Mandatory fields : "+ requiredKeys);

    let requiredKeys2 = ['poi_id'];
    let dataValid2 = utils.validateMinPayload(requiredKeys2, newConsignmentRecord.consignor);
    if(!dataValid2)
      return res.status(400).send("Mandatory fields in source and destination: "+ requiredKeys2);

    newConsignmentRecord.ext_consignment_no = newConsignmentRecord.consignment_no;
    newConsignmentRecord.consignor.seq_no = 1;
    newConsignmentRecord.consignee.seq_no = 2;

    let trip = {
      trip_type: "consignment_trip",
      fk_c_id: fk_c_id,
      startTis: moment().unix()
    };
        
        
    async.auto({
      validateEntityMapping: function(cb){
        let query = `select cmv.* , tp.status
                            from company_device_inventory_vw cmv
                            LEFT JOIN consignments c on cmv.fk_consignment_id = c.id 
                            LEFT JOIN trip_plannings tp on c.fk_trip_id = tp.id and tp.fk_c_id = `+ fk_c_id + `
                            LEFT JOIN entities e on cmv.fk_entity_id = e.id 
                            WHERE e.e_id = '`+ e_id + `' and cmv.fk_c_id =`+ fk_c_id + ` 
                            order by tis desc 
                            limit 1 `;
        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function(data){
                    
          if(data){
            if(data.connected){
              cb({code: 403,
                msg: "This tracker is currently in use"
              })
            }
            else{
              cb(null, data)
            }
          }else{
            let options = {
              uri: config.server_address.wad_server() + '/ad/registerentity',
              method: 'POST',
              json: {
                c_id: fk_c_id,
                e_id: e_id
              },
              headers: {
                'User-Agent': 'request',
                'Authorization': config.token.wad_consign
              }
            };
            request(options, function (error, response, body) {
              if(error){
                cb({
                  code: 500,
                  msg: error
                })
              }
              else if(body &&  body.status === "Error"){
                cb({
                  code: 400,
                  msg: body.msg
                })
              }
              else{
                if(body.nubot_id_valid) {
                  if(body.mapping_exists) {
                    cb({code: 403,
                      msg: "Tracker mapped to another company"
                    })
                  } else {
                    if(body.mapped_successfully) {
                      cb(null, {
                        fk_c_id: fk_c_id,
                        fk_u_id: fk_u_id,
                        fk_consignment_id: null,
                        fk_entity_id: body.fk_entity_id,
                        tis: null,
                        connected: false,
                        status: "scheduled"
                      })
                    } else {
                      cb({code: 403,
                        msg: "Invalid tracker ID"
                      })
                    }
                  }
                } else {
                  cb({code: 403,
                    msg: "Invalid tracker ID"
                  })
                } 
              }
            }); 
          }
        }).catch(function(err){
          console.log(err);
          res.status(500).send("Internal Server Error")
        })
      },
      getRoute: ['validateEntityMapping', function(result, callback){

        if(
          !newConsignmentRecord.consignor.lat ||
                    !newConsignmentRecord.consignor.lon ||
                    !newConsignmentRecord.consignee.lat ||
                    !newConsignmentRecord.consignee.lon
        ){
          console.log("Missing source or destination lat, lon to compute geoRoute");
          return callback()
        }

        let source_lat_lng = newConsignmentRecord.consignor.lat + ", " + newConsignmentRecord.consignor.lon;
        let dest_lat_lng = newConsignmentRecord.consignee.lat + ", " + newConsignmentRecord.consignee.lon;
        let options = {
          uri: config.server_address.consign_trip_planning() + '/v2/consign/geopath?elevation=false&locale=en-US&point='+source_lat_lng+'&point='+dest_lat_lng+'&type=json&vehicle=truck&weighting=fastest',
          method: 'GET'
        };
        request(options, function (error, response, body) {
          console.log(error);
          if(body.status === "Error"){
            res.status(400).send(body.msg)
          }
          else{
            let output = JSON.parse(body);
            let est_time = Math.round(moment.duration(output.paths[0].time).asMinutes());
            trip.endTis = moment().add(est_time, 'minutes').unix();
            newConsignmentRecord.est_distance = Math.round(output.paths[0].distance) || 0;
            newConsignmentRecord.est_time = est_time; //Converting milliseconds to minutes
            newConsignmentRecord.geo_trip_route = output;
            callback(null,output)
          }
        })
      }],
      createConsignment: ['getRoute', function (result, cb) {
        newConsignmentRecord.status = "unassigned";
        newConsignmentRecord.fk_c_id = fk_c_id;
        models.consignment.create(newConsignmentRecord).then(function (record) {
                    
          trip.geoRoute = result.getRoute;
          trip.consignment_id = record.id;
          waypoints.push({
            "poiId": newConsignmentRecord.consignor.poi_id,
            "lname": newConsignmentRecord.consignor.lname,
            "lat": newConsignmentRecord.consignor.lat,
            "lon": newConsignmentRecord.consignor.lon,
            "tis": newConsignmentRecord.consignor.tis,
            "stop_dur": 0,
            "loading": false,
            "unloading": false,
            "markerLabel": 0,
            "active": true,
            "type": newConsignmentRecord.consignor.type,
            "dispatchAction": "pickup",
            "fk_consignmentIds": [record.id],
            "radius": default_source_trip_radius //Default radius
          });
            
          waypoints.push({
            "poiId": newConsignmentRecord.consignee.poi_id,
            "lname": newConsignmentRecord.consignee.lname,
            "lat": newConsignmentRecord.consignee.lat,
            "lon": newConsignmentRecord.consignee.lon,
            "tis": newConsignmentRecord.consignee.tis,
            "stop_dur": 0,
            "loading": false,
            "unloading": false,
            "markerLabel": 1,
            "active": true,
            "type": newConsignmentRecord.consignee.type,
            "dispatchAction": "dropoff",
            "fk_consignmentIds": [record.id],
            "radius": default_dest_trip_radius, //Default radius
            "max_opentime": default_dest_max_opentime
          });
          cb(null, record)
        }).catch(function (err) {
          console.log(err);
          cb({
            code: 500,
            msg: 'Internal server error: '
          })
        })
      }],
      getAssetType: function(cb){
        getConsignmentAssetType("Consignment", function(type){
          cb(null, type)
        })
      },
      createTrip: ['createConsignment', 'getAssetType', function(results, cb){
        trip.waypoints = waypoints;
                
        trip.fk_asset_type_id = results.getAssetType;
                
        tripPlanningService.addTrip(req.headers, trip, host, function(err, result){
          if(err) {
            console.log(err);
            cb(err)
          }
          else cb(null, result)
        })
      }],
      updateConsignment: ['createTrip', function(result, cb){
        models.consignment.update({
          status: "assigned",
          fk_trip_id: result.createTrip.id
        }, {
          where: {
            id : result.createConsignment.id,
            fk_c_id: fk_c_id,
            active: true
          }
        }).then(function (updated) {
          if(updated)
            cb(null, result.createConsignment.id);
          else cb({
            code: 422,
            msg: 'Unable to update consignment with trip id'
          })
        }).catch(function (err) {
          console.log("ERROR : ", err);
          cb({
            code: 500,
            msg: 'Internal server error'+ err
          })
        })
      }],
      mapConsignment: ['updateConsignment', function (result, cb) {
        let options = {
          uri: config.server_address.wad_server() + '/ad/consignment/mapunmap',
          method: 'POST',
          json: {
            consignment_id: result.updateConsignment,
            entity_id: newConsignmentRecord.e_id,
            fk_u_id: fk_u_id
          },
          headers: {
            'User-Agent': 'request',
            'Authorization': config.token.wad_consign
          }
        };
        request(options, function (error, response, body) {
          if(response.statusCode === 200){
            cb(null, "Mapped")
          }
          else if(response.statusCode === 400){
            cb({code:400, msg:body, err: error})
          }
          else{
            cb({code:500, msg:"WAD mapunmap API error", err: error})
          }
        });
      }],
      mapToGroup: ['createConsignment', function (result, cb) {
        consignmentService.mapToGroups(result.createConsignment, cb)
      }]
    }, function (err, results) {
      if(err){
        console.log("Consignmnet trip create ERROR : ", err);
        res.status(err.code).send(err.msg)
      }else{
        res.json({
          msg: results.mapConsignment
        })
      }
    })
  }
};

/**
 * This api gets entity details
 * Used to read mapped consignment details
 * @param {*} req 
 * @param {*} res 
 */
exports.getConsignmentEntity = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const e_id = req.query.e_id;
  let requiredKeys = ['e_id'];
  let dataValid = utils.validateMinPayload(requiredKeys, req.query);
  if(!dataValid){
    return res.status(422).send("Mandatory fields: "+requiredKeys)
  }

  let query = `SELECT e.e_id,et.max_battery_voltage,et.min_battery_voltage, cmv.fk_consignment_id, c.ext_consignment_no, c.consignor->>'lname' as source_lname, 
                    c.consignee->>'lname' as destination_lname, c.consignor->>'addr' as source, 
                    c.consignee->>'addr' as destination, cmv.connected as mapped, tp.status, u.name
                FROM entities e
                INNER JOIN entity_types et on et.id = e.fk_e_type_id
                LEFT JOIN company_device_inventory_vw cmv on cmv.fk_entity_id = e.id and cmv.fk_c_id = `+ fk_c_id +`
                LEFT JOIN consignments c on cmv.fk_consignment_id = c.id 
                LEFT JOIN trip_plannings tp on c.id = tp.fk_asset_id
                LEFT JOIN asset_types at on tp.fk_asset_type_id = at.id and at.type='Consignment'
                LEFT JOIN users u on cmv.fk_u_id = u.id                
                WHERE e.e_id = '`+ e_id + `'
                ORDER by cmv.tis desc LIMIT 1`;
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function(data){
    if(data){
      let now = moment.tz("Asia/Kolkata").unix();
      nuRedis.getEntityDetails(data.e_id, function (err, nubot) {
        data.nubot_tis = nubot? nubot.tis : null;
        data.lname = nubot? nubot.lname : null;
        data.nubot_status = now - data.nubot_tis < 3600 ? "good" : "no_data";
        data.battery_percentage = nubot ? utils.calculateBatteryPercentage(data , nubot) : null;
        delete data.max_battery_voltage;
        delete data.min_battery_voltage;
        res.json(data)
      })
                
    }else{
      res.status(403).send("Invalid tracker Id")
    }
  }).catch(function(err){
    console.log(err);
    res.status(500).send("Internal Server Error")
  })
};

/**
 * This api calls WAD api to unmap entity. It acknowledges that entity received back to source and have been unmapped
 * company_entity_mapping table will tell who aknowledged the entity
 * @param {*} req 
 * @param {*} res 
 */
exports.unmapEntity = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  let data = req.body;
    
  let e_id = data.e_id;
  let consignment_id = data.consignment_id;
  let force_trip_complete = data.force_trip_complete;
  let trip_complete_reason = data.trip_complete_reason;

  let requiredKeys = ['e_id','consignment_id'];
  let dataValid = utils.validateMinPayload(requiredKeys, req.body);
  if(!dataValid){
    return res.status(422).send("Mandatory fields: "+requiredKeys)
  }

  if(force_trip_complete){
    if(!trip_complete_reason){
      return res.status(422).send("Mandatory fields: trip_complete_reason")
    }

    async.parallel([
      function(callback){
        cmaService.overrideConsignmentTripStatus(fk_c_id, fk_u_id, e_id, consignment_id, trip_complete_reason, function(err){
          if(err){
            res.status(err.code).send(err.msg)
          }else{
            unmapConsignmentEntity(e_id, fk_c_id, fk_u_id, consignment_id, function(err, result){
              if(err){
                callback({
                  code: err.code,
                  msg: err.msg
                })
              }else{
                callback(null, result)
              }
            })
          }
        })
      },
      function(callback){
        cmaService.overrideConsignmentStatus(fk_c_id, consignment_id, fk_u_id, trip_complete_reason, function(err, result){
          if(err){
            callback({
              code: err.code,
              msg: err.msg
            })
          }else{
            callback(null, result)
          }
        });
      }
    ], function(err){
      if(err){
        res.status(err.code).send(err.msg)
      }else{
        res.json("Consignment unmapped")
      }
    })
        
  }
  else{
    unmapConsignmentEntity(e_id, fk_c_id, fk_u_id, consignment_id, function(err, result){
      if(err){
        res.status(err.code).send(err.msg)
      }else{
        res.json(result)
      }
    })
  }
    
};

/**
 * Api to update external consignment no.
 * @param {*} req 
 * @param {*} res 
 */
exports.updateConsignmentMapping = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  let data = req.body;

  let requiredKeys = ['consignment_id','consignment_no'];
  let dataValid = utils.validateMinPayload(requiredKeys, data);
  if(!dataValid){
    return res.status(422).send("Mandatory fields: "+requiredKeys)
  }

  async.auto({
    eta: function (cb) {
      // compute ETA if lat, lng then update consignment and trip
      if (
        !data.consignor ||
                !data.consignee ||
                !data.consignor.lat ||
                !data.consignor.lon ||
                !data.consignee.lat ||
                !data.consignee.lon
      ) {
        console.log("Missing source or destination lat, lon to compute geoRoute");
        return cb()
      }

      let source_lat_lng = data.consignor.lat + ", " + data.consignor.lon;
      let dest_lat_lng = data.consignee.lat + ", " + data.consignee.lon;
      let options = {
        uri: config.server_address.consign_trip_planning() + '/v2/consign/geopath?elevation=false&locale=en-US&point='+source_lat_lng+'&point='+dest_lat_lng+'&type=json&vehicle=truck&weighting=fastest',
        method: 'GET'
      };
      request(options, function (error, response, body) {
        console.log(error);
        if(response.statusCode !== 200){
          cb()
        }
        else{
          let output = JSON.parse(body);
          let est_time = Math.round(moment.duration(output.paths[0].time).asMinutes());
          let eta = {
            est_distance: Math.round(output.paths[0].distance) || 0,
            est_time: est_time,
            geo_trip_route: output,
            end_tis: moment().add(est_time, 'minutes').unix()
          };
          cb(null, eta)
        }
      })
    },
    consignment: ['eta', function (result, cb) {
      let newConsignment = {
        ext_consignment_no: data.consignment_no,
        company_fields: data.company_fields
      };
      if (result.eta) {
        newConsignment = _.merge(newConsignment, result.eta);

        newConsignment.consignor = data.consignor;
        newConsignment.consignee = data.consignee
      }
      models.consignment.update(newConsignment, {
        where: {
          id : data.consignment_id,
          fk_c_id: fk_c_id,
          active: true
        },
        returning: true
      }).spread(function (updated, record) {
        if(updated){
          cb(null, record)
        }
        else{
          cb({code: 304, msg: "Error updating consignment"})
        }
      }).catch(function(err){
        cb({code: 500, msg: "Internal server error", err: err})
      })
    }],
    trip: ['eta', 'consignment', function (result, cb) {

      if(!data.consignor && !data.consignee){
        return cb()
      }

      let consignment = result.consignment[0];

      models.trip_planning.findOne({
        where: {
          id : consignment.fk_trip_id
        },
        attributes: ['id','waypoints','started','ended','past']
      }).then(function (record) {
        if(record){
          if(record.waypoints.length === 2){
            let wp = [];

            let wp1 = {
              fk_poi_id: consignment.consignor.poi_id,
              lat: consignment.consignor.lat,
              lon: consignment.consignor.lon,
              lname: consignment.consignor.lname
            };
            wp.push(_.merge(record.waypoints[0], wp1));

            let wp2 = {
              fk_poi_id: consignment.consignee.poi_id,
              lat: consignment.consignee.lat,
              lon: consignment.consignee.lon,
              lname: consignment.consignee.lname
            };
            wp.push(_.merge(record.waypoints[1], wp2));

            record.waypoints = wp;
            record.started = false;
            record.ended = false;
            record.past = true;

            let trip_update = {
              waypoints: wp,
              started: false,
              ended: false,
              past: true
            };

            if(result.eta){
              trip_update = _.merge(trip_update, result.eta)
            }

            models.trip_planning.update(
              trip_update, {
                where: {
                  id : consignment.fk_trip_id
                },
                returning: true
              }).spread(function (updated, record) {
              if(updated){
                cb(null, record)
              }
              else{
                cb({code: 304, msg: "Error updating consignment"})
              }
            }).catch(function(err){
              cb({code: 500, msg: "Internal server error", err: err})
            })

          }else{
            cb({code: 400, msg: "Invalid trip source and destination"})
          }
        }
        else{
          cb({code: 304, msg: "Error updating consignment"})
        }
      }).catch(function(err){
        cb({code: 500, msg: "Internal server error", err: err})
      })

    }]
  }, function (err) {
    if(err){
      res.status(err.code).send(err)
    }else{
      res.send("Consignment updated")
    }
  })
};

/**
 * @author Haston Silva
 * @description Function to get all company trackers and current or last consignment
 * @param req
 * @param res
 */
exports.getPortableTrackersConsignments = function (req, res) {
  /**
     * If you make changes to this code make sure you test, CMA app, fleet, consign portable trackers
     * Used by other projects
     * tracker status types:
     *  1. in_use
     *  2. available
     *  3. to_acknowledge
     */

  const TRACKER_STATUS_AVAILABLE = 'available';
  const fk_c_id = req.headers.fk_c_id;
  const limit = Number(req.query.limit) ? req.query.limit : 10;
  const page = Number(req.query.page) ? req.query.page : 0;
  const offset = limit * page;
  const output_format = req.query.format;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const sort = req.query.sort;
  const search_cols = req.query.search;
  const fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';

  const tracker_status = req.query.status || '';
  //list to show mapped consignments which requires update of consignment no
  const incomplete_list = req.query.incomplete ? ` AND (fk_consignment_id IS NOT NULL AND (ext_consignment_no is null OR ext_consignment_no = ''
    OR (consignee->'lat')::text = 'null' OR (consignee->'lon')::text = 'null' 
    OR (consignee->'lat')::text is null OR (consignee->'lat')::text is null)) `: '';
  let default_sort_condition = " ORDER BY e_id";

  if(tracker_status === TRACKER_STATUS_AVAILABLE){
    default_sort_condition = " ORDER BY sort_order_user_name"
  }else{
    default_sort_condition = " ORDER BY sort_order_trip_status"
  }

  let search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'e_id',
      client: 'e_id'
    }, {
      db: 'ext_consignment_no',
      client: 'ext_consignment_no'
    }, {
      db: 'name',
      client: 'name'
    },{
      db: 'lic_plate_no',
      client: 'lic_plate_no'
    }]
  );

  let sort_condition = utils.processTableColumnSort(
    sort
    ,[{
      db: 'e_id',
      client: 'e_id'
    }, {
      db: 'ext_consignment_no',
      client: 'ext_consignment_no'
    }],
    default_sort_condition);

  let query = `SELECT *, 
                COUNT(
                    CASE WHEN ext_consignment_no is null OR ext_consignment_no = '' THEN e_id END
                ) OVER()::INT as null_consignment_no_count,
                COUNT(
                    CASE WHEN (consignee->'lat')::text = 'null' OR (consignee->'lon')::text = 'null'
                      OR (consignee->'lat')::text is null OR (consignee->'lat')::text is null THEN e_id END
                ) OVER()::INT as null_destination_count,
                COUNT(e_id) OVER()::INT as total_count
                FROM (
                    SELECT
                        CASE
                            WHEN u.name IS NULL THEN 2
                            ELSE 1
                        END AS sort_order_user_name,
                        CASE
                            WHEN tp.status = 'inprogress'::text THEN 1
                            WHEN tp.status = 'scheduled'::text THEN 2
                            WHEN tp.status = 'open'::text THEN 3
                            ELSE NULL::integer
                        END AS sort_order_trip_status,
                        CASE
                            WHEN cdi.connected IS FALSE THEN 'available'::text
                            WHEN cdi.connected AND tp.status = 'completed'::text THEN 'to_acknowledge'::text
                            WHEN cdi.connected THEN 'in_use'::text
                            ELSE NULL::text
                        END AS tracker_status,
                        tp.status, cdi.tis, e.e_id,et.max_battery_voltage,et.min_battery_voltage, u.name, cdi.connected, cdi.fk_consignment_id, cdi.mapped_by, cdi.unmapped_by, con.ext_consignment_no,
                        con.consignor->>'lname' as source_lname, con.consignee->>'lname' as destination_lname,
                        con.consignor->>'addr' as source, con.consignee->>'addr' as destination, 
                        con.company_fields->>'lic_plate_no' as lic_plate_no, cdi.fk_c_id,
                        con.consignor, con.consignee, con.company_fields
                    FROM 
                      company_device_inventory_vw cdi
                      INNER JOIN entities e on e.id = cdi.fk_entity_id
                      INNER JOIN entity_types et on et.id = e.fk_e_type_id
                      LEFT JOIN consignments con ON con.id = cdi.fk_consignment_id AND con.fk_c_id = cdi.fk_c_id
                      LEFT JOIN trip_plannings tp ON tp.fk_asset_id = con.id
                      LEFT JOIN users u ON cdi.fk_u_id = u.id AND u.fk_c_id = cdi.fk_c_id
              ) d
              WHERE fk_c_id = :fk_c_id 
              `+ incomplete_list + search_col_condition;

  query += tracker_status ? " and tracker_status = :tracker_status" : "";
  query += searchKeyword ? " AND (lower(e_id) like :searchKeyword OR lower(name) like :searchKeyword OR lower(ext_consignment_no) like :searchKeyword)" : "";
  query += sort_condition;
  query += (output_format && output_format === 'xls' ? "" : " LIMIT :limit OFFSET :offset ");

  models.sequelize.query(query, {
    replacements: {
      fk_c_id: fk_c_id,
      limit: limit,
      offset: offset,
      tracker_status: tracker_status,
      searchKeyword: "%"+ searchKeyword +"%"
    },
    type: models.sequelize.QueryTypes.SELECT}).then(function(data){
    let total_count = 0, null_consignment_no_count = 0, null_destination_count = 0;
    if(data.length > 0){
      total_count = data[0].total_count;
      null_consignment_no_count = data[0].null_consignment_no_count;
      null_destination_count = data[0].null_destination_count
    }
    let now = moment.tz("Asia/Kolkata").unix();
    async.each(data, function(value, callback){
      value.name = value.connected && tracker_status === TRACKER_STATUS_AVAILABLE ? null : value.name;

      value.total_count = undefined;
      value.null_consignment_no_count = undefined;
      value.null_destination_count = undefined;
      value.fk_c_id = undefined;
      value.sort_order_user_name = undefined;
      value.sort_order_trip_status = undefined;
      nuRedis.getEntityDetails(value.e_id, function (err, nubot) {
        value.nubot_tis = nubot && nubot.tis ? nubot.tis : null;
        value.lname = nubot && nubot.lname ? nubot.lname : null;
        value.nubot_status = (now - value.nubot_tis) < 3600 ? "good" : "no_data";
        value.battery_percentage = nubot ? utils.calculateBatteryPercentage(value, nubot) : null;
        delete value.max_battery_voltage;
        delete value.min_battery_voltage;
        callback()
      })
    }, function(){
      if(output_format === 'xls'){
        if (data && data.length > 0) {
          let file_name = "PORTABLE-TRACKERS-LIST-"+ (_.upperCase(tracker_status)) +"-" + fk_c_id + "-" + moment().unix() + ".xls";
          let file_path = file_base_path + file_name;

          TABLE_CONFIG.getTableConfig('portable-tracker-list', fk_u_id, tracker_status, default_columns, function (err, table_config) {
            DOWNLOAD_HELPER.convertTableV1ToXLS(tracker_status, data, file_base_path, file_name, table_config, function (err) {
              if (err) {
                res.status(err.code).send(err.msg)
              } else {
                res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
                res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
                res.setHeader("Content-Type", "application/vnd.ms-excel");
                res.setHeader("Filename", file_name);

                res.download(file_path, file_name, function (err) {
                  if (!err) {
                    fs.unlink(file_path)
                  }
                })
              }
            })
          })
        } else {
          res.json("No data to download")
        }
      }else{
        res.json({
          total_count: total_count,
          null_consignment_no_count: null_consignment_no_count,
          null_destination_count: null_destination_count,
          data: data
        })
      }
    })
  }).catch(function(err){
    console.log(err);
    res.status(500).send("Internal Server Error")
  })
};