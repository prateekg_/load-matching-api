const moment = require('moment');
const utils = require('../../helpers/util');
const models = require('../../db/models');
const _ = require('lodash');
const async = require('async');
const PolylineUtil = require('polyline-encoded');
const Excel = require('exceljs');
const fs = require('fs');
const geolib = require('geolib');
const CHAR_LABEL = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
const file_base_path = __dirname + '/../../temp_reports/';
const consignmentService = require('../../services/consignment_service');

function computeAssetStatus(item) {
  let status = "stationary";
  if(item.ign === 'B' && item.spd > 0 && item.osf){
    status = 'overspeeding'
  } else if(item.ign === 'B' && item.spd > 0){
    status = 'moving'
  }else if(item.ign === 'B' && item.spd < 1){
    status = 'idling'
  }
  return status
}

function convertTripLogToXLS(lic_no, data, filename, callback){
  try {
    const workbook = new Excel.Workbook();
    const ws = workbook.addWorksheet('Trip report');
    ws.font = {name: 'Proxima Nova'};
    ws.columns = [
      {width: 15},
      {width: 15},
      {width: 30},
      {width: 15},
      {width: 30},
      {width: 15},
      {width: 15},
      {width: 15},
      {width: 15},
    ];

    ws.addRow(['Asset','Event start time', 'Start location', 'Event end time', 'End location', 'Distance(km)', 'Duration(min)', 'Avg speed','Event type' ]);
    ws.getRow(ws.rowCount).font = {size: 12, bold: true};

    let count = 0;
    data.forEach(function (i) {
      const date_time_format = 'DD-MM-YY, HH:mm';
      let start_lname = i.lname;
      let end_lname = "";
      let start_tis = i.arrivalTis;
      let end_tis = i.departureTis;
      let event_duration = utils.FormatMinutesDHM(Math.abs(i.departureTis - i.arrivalTis));

      if(i.type === 'intransit'){
        const prev_item = data[count - 1];
        const next_item = data[count + 1];

        if(prev_item){
          start_lname = prev_item.lname;
          start_tis = prev_item.departureTis
        }

        if(next_item){
          end_lname = next_item.lname;
          end_tis = next_item.arrivalTis
        }

        if(prev_item && next_item){
          event_duration = utils.FormatMinutesDHM(Math.abs(prev_item.departureTis - next_item.arrivalTis))
        }
      }else{
        end_lname = i.lname
      }

      const excel_row_item = [
        lic_no,
        moment.unix(start_tis).tz(utils.timezone.default, false).format(date_time_format),
        start_lname,
        moment.unix(end_tis).tz(utils.timezone.default, false).format(date_time_format),
        end_lname,
        i.distance,
        event_duration,
        i.avgSpd + ' km/h',
        i.type
      ];

      ws.addRow(excel_row_item);

      const currentRowIndex = ws.rowCount;
      for (let j = 65; j <= 79; j++) {
        const currentCol = String.fromCharCode(j);
        if (['A', 'C', 'E', 'I'].indexOf(currentCol) === -1) {
          ws.getRow(currentRowIndex).getCell(currentCol).alignment = {horizontal: 'right'};
        }
      }
      count++;
    });
    ws.views = [{state: 'frozen', xSplit: 0, ySplit: 1}];

    // write to excel file
    if(!fs.existsSync(file_base_path)){
      fs.mkdirSync(file_base_path)
    }

    workbook.xlsx.writeFile(file_base_path + filename)
      .then(function () {
        callback(null, data)
      }).catch(function (err) {
        callback({
          code: 500,
          msg: 'Internal error',
          err: err
        })
      });
  }catch(e){
    console.log("XLS ERROR : ", e);
    callback({
      code: 500,
      msg: 'Internal error'
    })
  }
}

// SINGLE TRIP PLAN START

function getSummary(params, callback) {

  const query =
    ` SELECT tp.id, tp.tid as "tripId", tp.status, tp.waypoints, tp.trip_log, tp.end_tis,
        json_build_object('id',a.id,'type',ast.type,'operator', c.cname, 'licNo', a.lic_plate_no) as asset,
        json_build_object('name',c.cname,'code',c.transporter_code) as transporter
      FROM trip_plannings tp
      INNER JOIN trip_user_mapping_externals tump ON tp.id = tump.fk_trip_id
      INNER JOIN user_externals ue ON ue.id = tump.fk_user_external_id AND ue.email = '${params.email}'
      INNER JOIN assets a on a.id = tp.fk_asset_id
      LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id
      LEFT JOIN companies c on c.id = a.fk_c_id
      WHERE tp.id = ${params.id} AND ue.email = '${params.email}' 
      LIMIT 1`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (trip) {
      if(!trip){
        return callback({code: 404, msg: 'Record not found'})
      }else{

        trip.distance = 0;
        trip.duration = 0;
        trip.stops = 0;
        trip.eta = {
          tis: trip.end_tis,
          duration: 0,
          delayed: false
        };

        const tl = trip.trip_log;
        if (tl) {
          trip.stops = tl.stops;
          trip.distance = Number((tl.distance / 1000).toFixed(1));
          trip.duration = Math.round( moment.duration( (tl.lkLocation.tis - tl.departure),'seconds').asMinutes());

          if (trip.status === 'inprogress') {
            trip.eta.tis = trip.end_tis + (tl.delayed_by || 0);
            trip.eta.duration = Math.round(tl.delayed_by / 60 ) || 0;
            trip.eta.delayed = tl.delayed || false
          }
        }

        const origin = trip.waypoints[0];
        const destination = trip.waypoints[trip.waypoints.length - 1];

        trip.originName = origin.lname;
        trip.destinationName = destination.lname;

        trip.source = {
          lname: origin.lname,
          tis: origin.tis
        };

        trip.destination = {
          lname: destination.lname,
          tis: destination.tis
        };

        delete trip.waypoints;
        delete trip.end_tis;
        delete trip.trip_log;

        callback(null, trip)
      }
    }).catch(function (err) {
      callback({code: 500, msg: 'Internal server error', err: err})
    })
}

exports.getLog = function (req, res) {
  let id = req.query.id;
  const email = req.query.email;
  const output_format = req.query.format;

  if(!id){
    res.status(400).send('Invalid trip id')
  }else{

    const query = "SELECT a.lic_plate_no, tp.tid, tp.status, tp.waypoints, tp.trip_log" +
      " FROM trip_plannings tp " +
      " INNER JOIN trip_user_mapping_externals tump ON tp.id = tump.fk_trip_id" +
      " INNER JOIN user_externals ue ON ue.id = tump.fk_user_external_id AND ue.email = '" + email + "'" +
      " INNER JOIN assets a on a.id = tp.fk_asset_id" +
      " LEFT JOIN companies c on c.id = a.fk_c_id" +
      " WHERE tp.id = " + id + " AND ue.email = '" + email + "' LIMIT 1";

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (trip) {
        if(!trip){
          res.status(404).send()
        }else{
          const asset_lic = trip.lic_plate_no;
          if(output_format === 'xls'){

            const file_name = "TRIP-REPORT-" + trip.tid + ".xls";
            const file_path = file_base_path + file_name;
            processWaypoints(trip, function (err, data) {
              convertTripLogToXLS(asset_lic, data, file_name, function (err) {
                if(err){
                  res.status(err.code).send(err.msg)
                }else {
                  res.setHeader("Access-Control-Expose-Headers","Content-Disposition, Filename");
                  res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
                  res.setHeader("Content-Type", "application/vnd.ms-excel");
                  res.setHeader("Filename", file_name);

                  res.download(file_path, file_name, function (err) {
                    if (!err) {
                      fs.unlink(file_path)
                    }
                  })
                }
              })
            })
          }else{
            processWaypoints(trip,function (err, data) {
              res.json(data)
            })
          }
        }
      }).catch(function () {
        res.status(500).send()
      })
  }
};

function getTripMap(params, callback) {

  async.auto({
    trip: function (callback) {
      const query =
        ` SELECT tp.fk_asset_id, tp.status, tp.waypoints, tp.trip_log, tp.geo_trip_route
          FROM trip_plannings tp
          INNER JOIN trip_user_mapping_externals tump ON tp.id = tump.fk_trip_id
          INNER JOIN user_externals ue ON ue.id = tump.fk_user_external_id AND ue.email = '${params.email}'
          INNER JOIN assets a on a.id = tp.fk_asset_id
          LEFT JOIN companies c on c.id = a.fk_c_id
          WHERE tp.id = ${params.id} AND ue.email = '${params.email}' 
          LIMIT 1`;

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (trip) {
          if(!trip){
            callback({
              code: 404,
              msg: 'Not found'
            })
          }else{
            let assetCurrentLocation = null;
            let actStartTis = null;
            let actEndTis = null;

            if (trip.trip_log) {

              const tl = trip.trip_log;
              const lkl = trip.trip_log.lkLocation;
              assetCurrentLocation = {
                id: lkl.fk_asset_id,
                licNo: lkl.lic_plate_no,
                lat: lkl.lat,
                lon: lkl.lon,
                tis: lkl.tis,
                ign: lkl.ign,
                spd: lkl.spd
              };
              actStartTis = tl.departure;
              actEndTis = tl.arrival
            }

            processWaypoints(trip, function(err, data) {
              callback(null, {
                estPath: utils.validateEstimatedPolyline(trip.geo_trip_route),
                waypoints: data,
                fk_asset_id: trip.fk_asset_id,
                assetLocation: assetCurrentLocation,
                actStartTis: actStartTis,
                actEndTis: actEndTis
              })
            })
          }
        }).catch(function (err) {
          callback({
            code: 500,
            msg: 'Internal server error',
            err: err
          })
        })
    },
    actual: ['trip', function (result, callback) {

      const params = result.trip;
      if(!params.actStartTis && !params.actEndTis){
        callback(null, {
          path: null
        })
      }else{
        const query =
          ` SELECT l.tis, l.lat, l.lon, l.spd, l.ign, l.osf, l.lname 
            FROM locations l
            WHERE l.fk_asset_id = ${params.fk_asset_id} AND l.tis >= ${params.actStartTis} AND l.tis <= ${params.actEndTis}
            ORDER BY l.tis ASC`;
        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .then(function (data) {
            if(data.length > 0){

              const latlngs = [];
              data.forEach(function (i) {
                latlngs.push([i.lat, i.lon]);

                i.status = computeAssetStatus(i);
                i.duration = 0;
                i.heading = 0
              });
              callback(null, {
                path: PolylineUtil.encode(latlngs),
                points: data
              })
            }else{
              callback(null, {
                path: null,
                points: []
              })
            }
          }).catch(function () {
            callback(null, {
              path: null,
              points: []
            })
          })
      }
    }]

  }, function (err, result) {
    if(err){
      return callback(err)
    }else{
      return callback(null, {
        estPathPolyline: result.trip.estPath,
        waypoints: result.trip.waypoints,
        assetLocation: result.trip.assetLocation || null,
        actualPath: result.actual.path || null,
        actualPoints: result.actual.points || []
      });
    }
  })
}


function getTripInfo(params, callback) {

  const query =
    ` SELECT a.lic_plate_no, tp.start_tis, tp.tid, tp.trip_log, tp.est_distance, iea.*
      FROM trip_plannings tp
      INNER JOIN trip_user_mapping_externals tump ON tp.id = tump.fk_trip_id
      INNER JOIN user_externals ue ON ue.id = tump.fk_user_external_id AND ue.email = '${params.email}'
      INNER JOIN assets a on a.id = tp.fk_asset_id
      LEFT JOIN companies c on c.id = a.fk_c_id
      LEFT JOIN input_essar_a iea on iea.fk_t_id = tp.id
      WHERE tp.id = ${params.id} AND ue.email = '${params.email}' 
      LIMIT 1`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .spread(function (trip) {
      if(!trip){
        return callback({code: 404, msg: 'Not found'})
      }else{
        try {
          let departure_tis = null;
          if (trip.trip_log) {
            departure_tis = trip.trip_log.departure || null
          }

          let customFields = [
            {key: "Consigner", value: trip.consigner},
            {key: "Consignee", value: trip.consignee},
            {key: "Invoice no.", value: trip.invoice_no},
            {key: "Material code", value: trip.material_code},
            {key: "Material quantity", value: trip.quantity},
            {key: "Shipment no.", value: trip.shipment_no},
            {key: "Ship to party no.", value: trip.ship_to_party},
            {key: "Ship to party mobile no.", value: trip.mobile_no},
            {key: "Route Id", value: trip.route_id},
            {key: "Transporter Code", value: trip.add1},
            {key: "Transporter Name", value: trip.add2},
            {key: "Distance (SAP)", value: trip.distance},
            {key: "Dispatch time", value: utils.DatetimeNoYearFormat(trip.start_tis)}
          ];

          customFields = _.sortBy(customFields, ['key']);

          const additionalFields = [{key: "add3", value: trip.add3},
            {key: "add4", value: trip.add4},
            {key: "add5", value: trip.add5}];

          _.forEach(additionalFields, function (o) {
            if(o.value && o.value.length > 0){
              customFields.push(o)
            }
          });

          let data = {
            tripId: trip.tid,
            departureTis: departure_tis || trip.start_tis,
            licNo: trip.lic_plate_no,
            origin: trip.origin,
            destination: trip.destination,
            estDistance: Number((trip.est_distance /1000).toFixed(1)),
            custom: customFields
          };
        }catch (e){
          console.log(e)
        }
        callback(null, data)
      }
    }).catch(function (err) {
      callback({code: 500, msg: 'Internal server error', err: err})
    })
}


// SINGLE TRIP PLAN END

function processWaypoints(trip, mainCb){

  async.auto({
    waypoints: function (cb) {
      const processedPlannedWaypoints = [];
      let seqWithOne = false;

      async.eachOfSeries(trip.waypoints, function (i, index, callbackWaypoints) {

        if(index === 0){
          if(i.seq_no === 1){ seqWithOne = true }
        }

        let waypoint = {
          poi: {
            id: i.fk_poi_id > 0 ? i.fk_poi_id : null,
            type: i.type || null,
            category: null
          },
          type: "planned",
          arrivalTis: i.tis,
          departureTis: i.tis + (i.stop_dur * 60),
          dwellDuration: i.stop_dur,
          violations: 0,
          crossed: false,
          label: seqWithOne ? CHAR_LABEL[i.seq_no - 1] : CHAR_LABEL[i.seq_no] || '',
          avgSpd: 0,
          distance: 0,
          geoBounds: []

        };

        i.type = "planned";
        waypoint = _.merge(waypoint, i);

        const query = "SELECT p.geo_loc from pois p where p.id = " + i.fk_poi_id;

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .spread(function (poi) {
            if(poi){
              const bounds = [];
              const coordinates = poi.geo_loc.coordinates[0];
              coordinates.forEach(function (p) {
                bounds.push({lon: p[0], lat: p[1]})
              });
              waypoint.geoBounds = bounds
            }
            processedPlannedWaypoints.push(waypoint);
            callbackWaypoints()
          }).catch(function (err) {
            console.log("Error occured ",err);
            processedPlannedWaypoints.push(waypoint);
            callbackWaypoints()
          })
      }, function () {
        cb(null, processedPlannedWaypoints)
      })
    },
    poi_processor: function (cb) {
      if(trip.trip_log && trip.trip_log.trip) {
        const log = trip.trip_log.trip || [];

        async.eachOfSeries(log, function (i, index, callbackWaypoints) {
          if(i.Id && i.Id > 0){
            const query = "SELECT CASE WHEN pt.fk_c_id > 0 THEN 'company' ELSE 'general' END as category" +
              " FROM pois p" +
              " LEFT JOIN poi_types pt on pt.id = p.typ" +
              " WHERE p.id = " + i.Id;
            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .spread(function (poi) {
                log[index].poi_category = poi ? poi.category : null;
                callbackWaypoints()
              }).catch(function (err) {
                console.log("Error occured ",err);
                callbackWaypoints()
              })
          }else{
            callbackWaypoints()
          }
        }, function () {
          cb(null, log)
        })

      }else{
        cb(null, [])
      }
    },
    logProcessor: ['waypoints', 'poi_processor', function (results, cb) {
      const waypoints = results.waypoints;
      const trip_data = results.poi_processor;
      const processedLog = [];
      if(trip_data && trip_data.length > 0){
        const log = trip_data;

        _.forEach(log, function (i, index) {

          const matchedItemIndex = _.findIndex(waypoints, function (o) {
            // Check if poi id is same or lat,lon within 50 meters
            return i.Id === o.fk_poi_id || geolib.isPointInCircle({
              latitude: o.lat,
              longitude: o.lon
            }, {
              latitude: i.Lat,
              longitude: i.Lon
            }, 50)
          });

          let labelText = "";
          let typeEvent = "unplanned";
          if(matchedItemIndex >= 0){
            typeEvent = "planned";
            labelText = waypoints[matchedItemIndex].label;
            waypoints.splice(matchedItemIndex,1)
          }

          processedLog.push({
            fk_poi_id: i.Id,
            poi: {
              id: i.Id > 0 ? i.Id : null,
              type: i.Type || null,
              category: i.poi_category || null
            },
            type:  typeEvent,
            lname: i.LName,
            arrivalTis: i.StartTis,
            departureTis: i.EndTis,
            dwellDuration: Math.round(moment.duration(i.DwellTime, 'seconds').asMinutes()),
            violations: 0,
            crossed: true,
            label: labelText,
            lat: i.Lat,
            lon: i.Lon,
            avgSpd: 0,
            distance: 0
          });

          processedLog.push({
            fk_poi_id: null,
            poi: null,
            type: "intransit",
            lname: "",
            arrivalTis: (i.EndTis + 1),
            departureTis: log[index+1] ? (log[index+1].StartTis - 1) : null,
            dwellDuration: 0,
            violations: 0,
            crossed: true,
            label: "",
            lat: null,
            lon: null,
            avgSpd: i.AvgSpd,
            distance: Number((i.Distance /1000).toFixed(1))
          })
        });

        if(trip.status === 'completed'){
          processedLog.pop()
        }

      }
      cb(null, {waypoints: waypoints, log: _.concat(processedLog, waypoints)})
    }],
    processor: ['waypoints','logProcessor',function (results, cb) {
      if(trip.status === 'scheduled'){
        cb(null, results.waypoints)
      } else {
        cb(null, results.logProcessor.log)
      }
    }]
  }, function (err, results) {
    let unplannedLabelIndex = 1;
    const data = _.forEach(results.processor, function (value) {
      value.fk_poi_id = undefined;
      if (value.type === 'unplanned') {
        value.label = unplannedLabelIndex.toString();
        unplannedLabelIndex++
      }
    });
    mainCb(null,  _.orderBy(data, ['crossed','arrivalTis'],['desc','asc']))
  })
}


exports.getAllSharedConsignments = function(req,res){
  if(!req.query.email){
    res.status(400).send("Bad Request");
  }else{
    async.auto({
      consignments: function(cb){
        consignmentService.getSharedConsignments(req.query, cb)
      },
      counts: function(cb){
        consignmentService.get3plConsignmentStatusCounts(req.query, cb);
      } 
    }, function(err, result){
      if(err){
        res.status(err.code).send(err.message);
      } else{
        let response = result.consignments;
        response  = _.merge(response, result.counts);
        res.send(response);
      }
    })
  }
};

exports.getConsignmentInfo = function (req, res) {
  const data = req.query;
  const keys = ['id', 'email'];
  let daataValid = utils.validateMinPayload(keys, data);
  if (!daataValid) {
    return res.status(400).send('Mandatory Fields: '+keys)
  }

  async.auto({
    info: function(cb){
      consignmentService.get3plInfo(data, cb)
    },
    log: function(cb){
      consignmentService.get3plLog(data, cb)
    },
    map: function(cb){
      consignmentService.get3plMap(data, cb)
    }
  }, function(err, results){
    if(err){
      res.status(err.code).send(err.msg);
    }else{
      let info = results.info;
      info.log = results.log;
      info = _.merge(info, results.map);
      res.send(info);
    }
  })
};

exports.tripInfo = function (req, res) {
  let data = req.query;
  const keys = ['id', 'email'];
  let daataValid = utils.validateMinPayload(keys, data);
  if (!daataValid) {
    return res.status(400).send('Mandatory Fields: '+keys)
  }

  let params = {id: req.query.id, email: req.query.email};

  async.auto({
    summary: function(cb){
      getSummary(params, cb)
    },
    info: function(cb){
      getTripInfo(params, cb)
    },
    map: function(cb){
      getTripMap(params, cb)
    }
  }, function(err, results){
    if(err){
      res.status(err.code).send(err.msg);
    }else{
      let data = results.info;
      data = _.merge(data, results.summary);
      data = _.merge(data, results.map);
      res.json(data)
    }
  })
};