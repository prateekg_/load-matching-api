const async = require('async');
const _ = require('lodash');
const moment = require('moment');
const fs = require('fs');
const models = require('../../db/models');
const utils = require('../../helpers/util');
const file_base_path = __dirname + '/../../temp_reports/';
const APP_LOGGER = require('../../helpers/logger');
const LOGGER = APP_LOGGER.logger;
const tableConfigsHelper = require('../../helpers/table_config');
const xlsDownload = require('../../helpers/downloadHelper');

function validateAsset(assetId, groupIds, callback) {

  let query = "SELECT a.id FROM groups g " +
        " INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected " +
        " INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
        " WHERE g.active AND g.id in " + groupIds + " AND a.id = " + assetId +
        " GROUP BY a.id LIMIT 1";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (asset) {
    if (!asset) {
      return callback({
        message: 'Invalid asset id',
        code: 404
      })
    }
    return callback(null, asset.id)
  }).catch(function (err) {
    callback({
      error: err,
      message: 'Internal server error',
      code: 500
    })
  })
}

function getMaintenanceRecord(maintenanceId, groupIds, callback) {
  let query =
  ` SELECT json_build_object('id', a.id, 'type', at.type, 'lic_plate_no', a.lic_plate_no, 'gids', ARRAY_AGG(g.gid), 'operator', c.cname) as asset,
      mr.service_tis, mr.id, mr.odometer, mr.tasks, mr.parts_cost, mr.labor_cost, mr.total_cost, mr.service_station, mr.lname, mr.scheduled_tis,
      mr.is_scheduled, mr.time_interval_unit, mr.time_interval_value, mr.dist_interval, mr.notes, mr.bill_no, mr.bill_tis,
      json_build_object('name', u.name, 'company_name', c2.cname, 'last_edit_tis', extract(epoch from mr."updatedAt")::INT) as user
    FROM maintenance_records mr
    INNER JOIN assets a ON a.id = mr.fk_asset_id and a.active and mr.active
    LEFT JOIN users u on u.id = mr.last_edited_by_fk_u_id
    LEFT JOIN companies c2 on c2.id = u.fk_c_id
    INNER JOIN companies c on c.id = a.fk_c_id and c.active
    INNER JOIN asset_types at on at.id = a.fk_ast_type_id
    INNER JOIN asset_group_mappings agm ON mr.fk_asset_id = agm.fk_asset_id AND agm.fk_asset_id = a.id and agm.connected
    INNER JOIN groups g on agm.fk_group_id = g.id
    WHERE g.active AND g.id in ${groupIds} AND mr.id = ${maintenanceId}
    GROUP BY a.id, mr.id, at.id, c.id, u.id, c2.id LIMIT 1`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (maintenance) {
    if (!maintenance) {
      return callback({
        message: 'Invalid maintenance record id',
        code: 404
      })
    }
        
    maintenance = maintenance.is_scheduled
      ? _.pick(maintenance, ['id', 'asset', 'user', 'tasks', 'lname', 'service_station', 'scheduled_tis', 'time_interval_unit', 'time_interval_value', 'dist_interval', 'notes', 'bill_no', 'bill_tis'])
      : _.pick(maintenance, ['id', 'asset', 'user', 'tasks', 'lname', 'service_station', 'total_cost', 'parts_cost', 'labor_cost', 'service_tis', 'odometer', 'notes', 'bill_no', 'bill_tis']);
    callback(null, maintenance)
  }).catch(function (err) {
    callback({
      error: err,
      message: 'Internal server error',
      code: 500
    })
  })
}

function addNewMaintenanceTypes(tasks, fk_c_id, callback) {
  let bulkCreateData = [];

  async.each(tasks, function (task, cb) {
    bulkCreateData.push({
      fk_c_id: fk_c_id,
      type: task
    });
    cb()
  }, function () {
    models.maintenance_type.bulkCreate(bulkCreateData).then(function () {
      callback()
    }).catch(function (err) {
      callback({
        error: err,
        message: 'Internal server error',
        code: 500
      })
    })
  })
}

exports.add = function (req, res) {
  let assetId = req.body.asset.id;
  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const maintenance = req.body;
  const lic_plate_no = req.body.asset.lic_plate_no;

  if(maintenance.is_scheduled && maintenance.scheduled_tis < moment().unix()) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Cannot schedule maintenance for the past',
      code: 400
    })
  } else if(maintenance.service_tis > moment().unix()){
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Cannot add service records for the future',
      code: 400
    })
  } else if(maintenance.bill_no && !maintenance.bill_tis) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Both Bill no. and Bill tis should be present or both null',
      code: 400
    })

  } else if(maintenance.bill_tis && !maintenance.bill_no) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Both Bill no. and Bill tis should be present or both null',
      code: 400
    })
  }

  async.auto({
    getAssetID: function(callback) {
      if (!assetId && lic_plate_no) {
        models.sequelize.query(`select id from assets where lic_plate_no = $lic_plate_no`, {
          bind: {"lic_plate_no": lic_plate_no},
          type: models.sequelize.QueryTypes.SELECT
        }).then(result => {
          if (!result || result.length <= 0 || result[0].id <= 0) {
            console.error("Could not get asset_id");
            return callback("Could not get asset_id");
          }
          assetId = result[0].id;
          return callback(null);
        }).catch(err => {
          console.error("Error when getting asset id", err);
          return callback(err);
        });
      }
      else {
        callback(null);
      }
    },
    validateAsset: ['getAssetID', function(result, callback) {
      validateAsset(assetId, groupIds, callback);
    }],
    addTypes: ['validateAsset', function (result, cb) {
      addNewMaintenanceTypes(maintenance.new_tasks, fk_c_id, cb)
    }],
    addRecord: ['validateAsset', function (result, cb) {
      maintenance.fk_asset_id = assetId;
      maintenance.added_by_fk_u_id = fk_u_id;
      maintenance.last_edited_by_fk_u_id = fk_u_id;

      models.maintenance_record.create(maintenance).then(function (record) {
        cb(null, {
          code: 201,
          message: 'Maintenance record successfully created',
          id: record.id
        })
      }).catch(function (err) {
        cb({
          error: err,
          message: 'Internal server error',
          code: 500
        })
      })
    }]
  }, function (err, result) {
    if (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      err.error = err.code === 500 ? null : err.error;
      return res.status(err.code).send(err)
    }
    res.status(result.addRecord.code).send(result.addRecord)
  })
};

exports.delete = function (req, res) {
  const fk_u_id = req.headers.fk_u_id;
  const maintenanceId = req.body.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);

  async.auto({
    validateRecord: getMaintenanceRecord.bind(null, maintenanceId, groupIds),
    deleteRecord: ['validateRecord', function (result, cb) {
      models.maintenance_record.update({
        active: false,
        last_edited_by_fk_u_id: fk_u_id
      }, {
        where: {
          id: maintenanceId,
          active: true
        }
      }).spread(function (updated) {
        if (updated > 0) {
          return cb(null, {
            message: 'Maintenance record successfully deleted'
          })
        }
        return cb({
          message: 'Invalid maintenance record id',
          code: 404
        })
      }).catch(function (err) {
        cb({
          error: err,
          message: 'Internal server error',
          code: 500
        })
      })
    }]
  }, function (err, result) {
    if (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      err.error = err.code === 500 ? null : err.error;
      return res.status(err.code).send(err)
    }
    res.json(result.deleteRecord)
  })
};

exports.update = function (req, res) {
  const assetId = req.body.asset.id;
  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const maintenance = req.body;

  if(maintenance.is_scheduled && maintenance.scheduled_tis < moment().unix()) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Cannot schedule maintenance for the past',
      code: 400
    })
  } else if(maintenance.service_tis > moment().unix()){
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Cannot add service records for the future',
      code: 400
    })
  } else if(maintenance.bill_no && !maintenance.bill_tis) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Both Bill no. and Bill tis should be present or both null',
      code: 400
    })

  } else if(maintenance.bill_tis && !maintenance.bill_no) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Both Bill no. and Bill tis should be present or both null',
      code: 400
    })
  }

  async.auto({
    validateAsset: validateAsset.bind(null, assetId, groupIds),
    validateRecord: getMaintenanceRecord.bind(null, maintenance.id, groupIds),
    addTypes: ['validateAsset', function (result, cb) {
      addNewMaintenanceTypes(maintenance.new_tasks, fk_c_id, cb)
    }],
    updateRecord: ['validateAsset', 'validateRecord', function (result, cb) {
      maintenance.fk_asset_id = assetId;
      maintenance.last_edited_by_fk_u_id = fk_u_id;

      models.maintenance_record.update(maintenance, {
        where: {
          id: maintenance.id,
          active: true
        }
      }).spread(function (updated) {
        if (updated > 0) {
          return cb(null, {
            message: 'Maintenance record successfully updated'
          })
        }

        cb({
          message: 'Invalid maintenance record id',
          code: 404
        })

      }).catch(function (err) {
        cb({
          error: err,
          message: 'Internal server error',
          code: 500
        })
      })

    }]
  }, function (err, result) {
    if (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      err.error = err.code === 500 ? null : err.error;
      return res.status(err.code).send(err)
    }
    res.json(result.updateRecord)
  })
};

exports.getAll = function (req, res) {
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = page * limit;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const search_cols = req.query.search;
  let search_col_condition = "";

  const output_format = req.query.format;
  const fk_c_id = req.headers.fk_c_id;
  const type = req.query.type;

  let filterCondition = "";
  let start_tis = null;
  let end_tis = null;

  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  let columnSelection = "";
  const fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';

  if (req.query.type === 'scheduled') {
    start_tis = req.query.start_tis || moment().unix();
    filterCondition =  ` AND mr.is_scheduled AND ( mr.scheduled_tis >= ${start_tis} ) GROUP BY a.id, mr.id, at.id, c.id, u.id, c2.id ORDER BY mr.scheduled_tis DESC`;
    columnSelection = " mr.scheduled_tis, mr.time_interval_unit, mr.time_interval_value, mr.dist_interval"
  } else {
    start_tis = req.query.start_tis;
    end_tis = req.query.end_tis;

    if (!start_tis || ! end_tis) {
      filterCondition = " AND mr.is_scheduled is false GROUP BY a.id, mr.id, at.id, c.id, u.id, c2.id ORDER BY mr.service_tis desc"
    } else {
      filterCondition = ` AND mr.is_scheduled is false AND (mr.service_tis between ${start_tis} AND ${end_tis}) GROUP BY a.id, mr.id, at.id, c.id, u.id, c2.id ORDER BY mr.service_tis DESC`
    }
    columnSelection = " mr.service_tis, mr.total_cost"
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.lic_plate_no'
    }, {
      db: 'mr.tasks',
      client: 'tasks'
    }, {
      db: 'mr.service_station',
      client: 'service_station'
    }, {
      db: 'mr.lname',
      client: 'lname'
    }]
  );

  let query =
  ` SELECT count(*) OVER() AS total_count, mr.id,
      json_build_object('id', a.id, 'type', at.type, 'lic_plate_no', a.lic_plate_no, 'gids', ARRAY_AGG(g.gid), 'operator', c.cname) as asset,
      mr.tasks, mr.service_station, mr.lname, mr.is_scheduled, ${columnSelection}
    FROM maintenance_records mr
    INNER JOIN assets a ON a.id = mr.fk_asset_id and a.active and mr.active
    LEFT JOIN users u on u.id = mr.last_edited_by_fk_u_id
    LEFT JOIN companies c2 on c2.id = u.fk_c_id
    INNER JOIN companies c on c.id = a.fk_c_id and c.active
    INNER JOIN asset_types at on at.id = a.fk_ast_type_id
    INNER JOIN asset_group_mappings agm ON mr.fk_asset_id = agm.fk_asset_id AND agm.fk_asset_id = a.id and agm.connected
    INNER JOIN groups g on agm.fk_group_id = g.id
    WHERE g.active AND ${groupCondition + search_col_condition}`;

  if(searchKeyword.length > 0){
    query += 
    ` AND (lower(a.lic_plate_no) like '%${searchKeyword}%' OR lower(mr.tasks::TEXT) like '%${searchKeyword}%'
      OR lower(mr.service_station) like '%${searchKeyword}%' OR lower(mr.lname) like '%${searchKeyword}%') `
  }

  query += filterCondition;

  if(output_format === 'list') {
    query += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (maintenance) {
    let count = 0;
    async.each(maintenance, function (item, cb) {
      count = Number(item.total_count);
      delete item.total_count;
      cb()
    }, function () {
      if(output_format === 'xls') {
        const file_name = "MAINTENANCE-LIST-"+ fk_c_id +"-"+ moment().unix() + ".xls";
        const file_path = file_base_path + file_name;

        tableConfigsHelper.getTableConfig('maintenance-list', fk_u_id, type, default_columns, function (err, table_config) {
          xlsDownload.convertTableV1ToXLS(type, maintenance, file_base_path, file_name, table_config, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })
        })
      } else {
        res.json({
          total_count: count,
          data: maintenance
        })
      }
    })
  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send({
      error: null,
      message: 'Internal server error',
      code: 500
    })
  })
};

exports.getDetail = function (req, res) {
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const maintenanceId = req.query.id;
  getMaintenanceRecord(maintenanceId, groupIds, function (err, result) {
    if(err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      err.error = err.code === 500 ? null : err.error;
      return res.status(err.code).send(err)
    }

    res.json(result)
  })
};

exports.getRealtimeAnalytics = function (req, res) {

  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const start_tis = req.query.start_tis || moment().tz(utils.timezone.default, false).startOf('day').unix();
  const end_tis = req.query.end_tis || moment().tz(utils.timezone.default, false).endOf('day').unix();
  const current_tis = moment().unix();
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  let query =
  ` WITH assets AS (
      SELECT a.id FROM groups g
      INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id AND agm.connected
      INNER JOIN assets a ON a.id = agm.fk_asset_id AND a.active
      WHERE ${groupCondition} 
      GROUP BY a.id
    )

    SELECT COALESCE(COUNT (CASE WHEN mr.is_scheduled THEN 1 END),0)::INT as scheduled_count,
    count (CASE WHEN mr.is_scheduled AND mr.scheduled_tis < ${current_tis} THEN 1 END)::INT as overdue_count,
    COALESCE(SUM (CASE WHEN mr.is_scheduled IS FALSE AND service_tis BETWEEN ${start_tis} AND ${end_tis} THEN total_cost END), 0) as total_cost
    FROM maintenance_records mr WHERE fk_asset_id IN (SELECT id FROM assets)`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (services) {
    res.json(services)
  }).catch(function(err){
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send({
      error: null,
      msg: 'Internal server error',
      code: 500
    })
  })
};