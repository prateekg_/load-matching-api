const _ = require('lodash');
const async = require('async');
const moment = require('moment');
const models = require('../../db/models');
const tagsHelper = require('../../helpers/tags');
const defaultObject = require('../../helpers/defaultObject');
const utils = require('../../helpers/util');
const xlsDownload = require('../../helpers/downloadHelper');
const file_base_path = __dirname + '/../../temp_reports/';
const tableConfigsHelper = require('../../helpers/table_config');
const fs = require('fs');
const POI_FEES_COLUMN = process.env.NODE_ENV ? 'fees' : 'cost';


exports.checkRouteNameExists = function (req, res) {

  const route_id = req.query.id || null;
  const name = req.query.name || null;
  if (!name) {
    return res.status(422).send("enter name")
  }
  models.route_template.findAll({
    attributes: ['route_id', 'name'],
    where: {
      name: {
        $ilike: name
      }
    },
    raw: true
  }).then(function (result) {
    result.forEach(function (i, index) {
      if (i.route_id === route_id) {
        result.splice(index,1)
      }
    });

    if (result.length > 0) {
      return res.status(409).send({ exists: true})
    } else {
      res.status(200).send({ exists: false})
    }
  }).catch(function (err) {
    res.status(500).send(err)
  })
};

exports.get = function (req, res) {

  const fk_company_id = req.headers.fk_c_id;
  const routeId = req.query.id || null;

  if(!routeId) {
    return res.status(400).send('Invalid route template ID')
  }

  models.route_template.findAll({
    attributes :[['route_id','id'],'name','waypoints',['geo_route','geoRoute'], 'route_planning', 'labels', 'custom_fields', 'route_planning', ['est_dur', 'estTime'], 'is_advanced'],
    where : {
      route_id : routeId,
      fk_c_id : fk_company_id,
      active: true
    }
  }).spread(function (route) {
    if(route){
      route = route.get({plain: true});
        
      let waypoints = route.waypoints;
      let tolls = route.route_templates_poi_mappings;
      async.auto({
        waypoints: function(cb) {
          async.eachSeries(waypoints, function(data, callback){
            if(data.poiId){
              let query = "SELECT p.geo_loc from pois p where p.id = "+ data.poiId;
        
              models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                .spread(function (poi) {
                  if(poi){
                    let bounds = [];
                    let coordinates = poi.geo_loc.coordinates[0];
                    coordinates.forEach(function (i) {
                      bounds.push({
                        lon: i[0],
                        lat: i[1]
                      })
                    });
                    data.geoBounds = bounds;
                    callback()
                  }
        
        
                }).catch(function (err) {
                  console.log(err);
                  callback(err)
                })
            }else{
              callback()
            }
          }, function(err){
            cb(err)
          })

        },
        tollbooths: function(cb) {
          let tollbooths = [];
          if(tolls) {
            tolls.forEach(function (t) {
              if(t.poi && t.poi.active) {
                let obj = {
                  lat: t.poi.lat,
                  lon: t.poi.lon,
                  id: t.id,
                  name: t.poi.name, 
                  toll_cost: _.head(_.filter(t.poi.fees, function(o) {
                    if(process.env.NODE_ENV) {
                      //keys does not match on dev and prod
                      if(o.typeOfVehicle === route.custom_fields.type_of_vehicle) {
                        return o
                      }
                    }
                    if(o.type_of_vehicle === route.custom_fields.type_of_vehicle) {
                      return o
                    }
                  }).map(function(o){
                    if(process.env.NODE_ENV) {
                      //keys does not match on dev and prod
                      return o.singleJourney
                    }
                    return o.single_journey
                  }))
                };
                tollbooths.push(obj)
              }
                           
            });
            route.tollbooths = tollbooths 
          }
                    
          cb()
        }
      }, function (err){
        console.error(err);
        if(err){
          res.status(500).send('Internal server error' + err)
        }else{
          route.route_templates_poi_mappings = undefined;
          res.status(200).send(route)
        }
      });
            
    }else{
      res.status(404).send()
    }
  }).catch(function (err) {
    res.status(500).send('Internal server error' + err)
  })
};

exports.getAll = function (req, res) {

  const fk_u_id = req.headers.fk_u_id;
  const fk_company_id = req.headers.fk_c_id;
  const searchkey = req.query.q || '';
  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = limit * page;
  const search_cols = req.query.search || null;
  let search_col_condition = "";
  const sort = req.query.sort;
  let sort_condition = ' ORDER BY id DESC ';
  let limit_condition = ` LIMIT ${limit} OFFSET ${offset}`;
  let table_search = '';
  const default_columns = req.query.columns === 'all';
  const output_format = req.query.format || 'list';

  if(output_format === 'xls'){
    limit_condition = ''
  }

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'rp.name',
      client: 'name'
    }, {
      db: 'origin',
      client: 'origin'
    }, {
      db: 'destination',
      client: 'destination'
    }]
  );

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'name',
      client: 'name'
    }, {
      db: '"totalWaypoints"',
      client: 'totalWaypoints'
    }, {
      db: 'COALESCE("estimatedDist",0)',
      client: 'estimatedDist'
    }, {
      db: 'COALESCE("estimatedDur",0)',
      client: 'estimatedDur'
    }, {
      db: 'json_array_length("tollbooths")',
      client: 'tollbooths'
    }],
    sort_condition
  );

  if(searchkey){
    table_search = `AND ( rp.name ilike '%${searchkey}%' or origin ilike '%${searchkey}%' or destination ilike '%${searchkey}%' or t.tag ilike '%${searchkey}%')`
  }

  let query = `
    SELECT * FROM (
        SELECT route_id as id, rp.name, origin, destination, tot_waypoints as "totalWaypoints",est_dist as "estimatedDist",
            est_dur as "estimatedDur", COALESCE(json_agg(t.tag) FILTER (WHERE t.tag IS NOT NULL), '[]') AS tags,
            COALESCE(json_agg(json_build_object('id', p.id, 'name', p.name, 'lat', p.lat, 'lon', p.lon, 'toll_cost', p.${POI_FEES_COLUMN}))
            FILTER (WHERE p.id IS NOT NULL), '[]') as "tollbooths" , rp.labels::jsonb, rp.custom_fields::jsonb, route_planning, count(*) OVER() AS total_count
        FROM route_templates rp 
        LEFT JOIN (
            select rt.id, tg.tag 
            from route_templates rt 
            left join tags tg on tg.t_id = rt.id and tg.type = ${tagsHelper.tagType.route_templates} 
            where rt.id in (select t_id from tags where tag ilike '%${searchkey}%' group by 1)
        ) t on t.id = rp.id
        LEFT JOIN route_templates_poi_mappings rtpm on rtpm.fk_route_template_id = rp.id and rtpm.active
        LEFT JOIN pois p on p.id = rtpm.fk_poi_id and p.active
        where rp.fk_c_id = ${fk_company_id} and rp.active is true 
        ${table_search + search_col_condition}
        GROUP BY route_id, rp.name, origin, destination, tot_waypoints, est_dist, est_dur, rp.labels::jsonb, rp.custom_fields::jsonb, route_planning
    ) d
    ${sort_condition} 
    ${limit_condition}`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function(result){

      let totalRows = 0;

      _.each(result, function (template) {
        template.estimatedDist = Number((template.estimatedDist/1000).toFixed(1));
        totalRows = template.total_count;
        let labels = [];
        _.filter(template.labels, function (o) {
          let tmp = _.keys(_.pickBy(o, _.identity));
          tmp.length > 0 ? labels.push(tmp[0]) : null
        });

        template.labels = labels;

        template.tollbooths.forEach(function(t) {
          t.toll_cost = _.head(_.filter(t, function(o) {
            if(process.env.NODE_ENV) {
              //keys does not match on dev and prod
              if(o.typeOfVehicle === template.custom_fields.type_of_vehicle) {
                return o
              }
            }
            if(o.type_of_vehicle === template.custom_fields.type_of_vehicle) {
              return o
            }
          }).map(function(o){
            if(process.env.NODE_ENV) {
              //keys does not match on dev and prod
              return o.singleJourney
            }
            return o.single_journey
          }))
        });
            
        delete template.total_count
      });

      if (output_format === 'xls') {
        let file_name = ("ROUTE-LIST-" + utils.downloadReportDate()).toString().toUpperCase() + ".xls";
        let file_path = file_base_path + file_name;

        tableConfigsHelper.getTableConfig('routes-list', fk_u_id, 'all', default_columns, function (err, table_config) {

          xlsDownload.convertTableV1ToXLS('all', result, file_base_path, file_name, table_config, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })
        })

      } else {
        res.json({
          count: Number(totalRows),
          data: result
        })
      }

    }).catch(function (err) {
      res.status(500).send('Internal server error ' + err)
    })

};

exports.add = function (req, res) {

  const fk_company_id = req.headers.fk_c_id;
  let itm = req.body;
  let geo_route = {};
  let tolls = [];
  let routeTemplate;


  try{
    let tot_waypoints = itm.waypoints.length;
    let tot_duration = 0;

    if(tot_waypoints < 2){
      return res.status(400).send('too less waypoints')
    }

    if(itm.route_planning) {
      geo_route = defaultObject.geoRoute;
      geo_route.paths[0].distance = itm.distance;
      geo_route.paths[0].time = itm.duration;
      geo_route.paths[0].points = itm.polyline;
      geo_route.paths[0].weight = itm.duration / 1000;
      itm.geoRoute = geo_route
    }

    itm.waypoints.forEach(function(i){
      tot_duration += Number(i.stopDur)
    });

    routeTemplate = {
      route_id: null,
      fk_c_id: fk_company_id,
      name: itm.name,
      origin: itm.waypoints[0].lname,
      destination: itm.waypoints[tot_waypoints-1].lname,
      waypoints: itm.waypoints,
      tot_waypoints:tot_waypoints,
      est_dist: itm.geoRoute.paths[0].distance,
      est_dur: tot_duration + Math.round(moment.duration(itm.geoRoute.paths[0].time).asMinutes()),
      geo_route: itm.geoRoute,
      tags: itm.tags,
      is_advanced: itm.is_advanced || null
    };

    if(itm.route_planning) {
      routeTemplate.route_planning = true;
      routeTemplate.labels = itm.labels;
      routeTemplate.custom_fields = {
        type_of_vehicle : itm.type_of_vehicle,
        avoid_highways : itm.avoid_highways,
        avoid_tollbooths : itm.avoid_tollbooths,
        fastag_savings : itm.fastag_savings,
        shell_savings : itm.shell_savings,
        avg_speed : itm.avg_speed,
        fuel_efficiency : itm.fuel_efficiency
      }
    }
  }catch(e){
    console.error(e)
    return res.status(422).send("invalid parameters")
  }

  models.route_template.findAll({
    attributes: ['name'],
    where: {
      name: {
        $ilike: routeTemplate.name
      }
    },
    raw: true
  }).then(function (result) {
    if(result.length > 0){
      return res.status(409).send("Route template name already exists")
    }else{
      async.retry({times: 5, interval: 200},
        function (cb) {

          routeTemplate.route_id = 'RT' + moment().valueOf();

          models.route_template.findOrCreate({
            where : {
              route_id : routeTemplate.route_id
            }, defaults : routeTemplate})
            .spread(function (route_template, created) {
              if(!created) {
                return cb('route template id exists')
              } else {
                if (itm.route_planning && itm.tollbooths.length > 0) {
                  itm.tollbooths.forEach(function(t) {
                    tolls.push({
                      fk_poi_id: t.id,
                      fk_route_template_id: route_template.get({raw: true}).id,
                      active: true
                    })
                  });
                  models.route_templates_poi_mapping.bulkCreate(tolls).catch(function (err) {
                    cb(err)
                  })
                }
              }
              cb(null, route_template)
            }).catch(function (err) {
              cb(err)
            })
        }, function(err, result) {
          if(err) {
            res.status(500).send('Error in creating route template');
          } else {

            if(routeTemplate.tags){

              tagsHelper.processTags(routeTemplate.tags, result.id, fk_company_id, tagsHelper.tagType.route_templates, function (err) {

                if(err){
                  res.status(500).send('Internal server error' + err)
                }else{
                  res.status(200).send({
                    id: result.get('route_id'),
                    name: result.get('name'),
                    msg: 'route template created successfully '
                  })
                }
              })

            }else{
              res.status(200).send({
                id: result.get('route_id'),
                name: result.get('name'),
                msg: 'route template created successfully '
              })
            }

          }
        });
    }

  }).catch(function (err) {
    res.status(500).send(err)
  })
};

exports.update = function (req, res) {

  const fk_company_id = Number(req.headers.fk_c_id);
  let itm = req.body;
  let rec_id = null;
  let routeTemplate;

  if(!itm.id){
    return res.status(422).send("id missing")
  }

  try{

    let tot_waypoints = itm.waypoints.length;
    let tot_duration = 0;

    if(tot_waypoints < 2){
      return res.status(400).send('too less waypoints')
    }

    if(itm.route_planning) {
      let geo_route = defaultObject.geoRoute;
      geo_route.paths[0].distance = itm.distance;
      geo_route.paths[0].time = itm.duration;
      geo_route.paths[0].points = itm.polyline;
      geo_route.paths[0].weight = itm.duration / 1000;
      itm.geoRoute = geo_route
    }

    itm.waypoints.forEach(function(i){
      tot_duration += i.stopDur
    });

    routeTemplate = {
      route_id: itm.id,
      fk_c_id: fk_company_id,
      name: itm.name,
      origin: itm.waypoints[0].lname,
      destination: itm.waypoints[tot_waypoints-1].lname,
      waypoints: itm.waypoints,
      tot_waypoints:tot_waypoints,
      est_dist: itm.geoRoute.paths[0].distance,
      est_dur: tot_duration + Math.round(moment.duration(itm.geoRoute.paths[0].time, "ms").asMinutes()),
      geo_route: itm.geoRoute,
      tags: itm.tags,
      is_advanced: itm.is_advanced
    };

    if(itm.route_planning) {
      routeTemplate.route_planning = true;
      routeTemplate.labels = itm.labels;
      routeTemplate.custom_fields = {
        type_of_vehicle : itm.type_of_vehicle,
        avoid_highways : itm.avoid_highways,
        avoid_tollbooths : itm.avoid_tollbooths,
        fastag_savings : itm.fastag_savings,
        shell_savings : itm.shell_savings,
        avg_speed : itm.avg_speed,
        fuel_efficiency : itm.fuel_efficiency
      };
      routeTemplate.est_dur = tot_duration + Math.round(moment.duration(itm.geoRoute.paths[0].time, "s").asMinutes())
    }
  }catch(e){
    console.error(e)
    return res.status(422).send("invalid parameters")
  }

  routeTemplate = _.omitBy(routeTemplate, _.isNil);

  models.route_template.findAll({
    attributes: ['id','route_id','name'],
    where: {
      name: {
        $ilike: routeTemplate.name
      }
    },
    raw: true
  }).then(function (result) {
    result.forEach(function (i, index) {
      if(i.route_id === routeTemplate.route_id){
        rec_id = i.id;
        result.splice(index,1)
      }
    });

    if(result.length > 0){
      return res.status(409).send("Route template name already exists")
    }else{
      models.route_template.update(routeTemplate, {
        where : {
          route_id : routeTemplate.route_id,
          fk_c_id: fk_company_id,
          active: true
        }
      }).then(function (updated) {
                
        if(updated === 1){
          let trackPoi = [];
                    
          async.auto({

            updateRoutePoiMappings: function(callback) {
              if (itm.route_planning && itm.tollbooths.length > 0) {
                async.each(itm.tollbooths, function (t, cb) {
                  models.route_templates_poi_mapping.findOne({
                    attributes: ['id', 'active'],
                    where: {
                      fk_route_template_id: rec_id,
                      fk_poi_id: t.id
                    },
                    raw: true
                  }).then(function (item) {
                    trackPoi.push("'" + t.id + "'");
                    if (!item) {
                      //insert new
                      models.route_templates_poi_mapping.create({
                        fk_route_template_id: rec_id,
                        fk_poi_id: t.id,
                        active: true
                      }).then(function () {
                        cb()
                      }).catch(function (err) {
                        cb(err)
                      })
                    } else if (item.active === false) {
                      models.route_templates_poi_mapping.update({ active: true }, {
                        where: {
                          fk_route_template_id: rec_id,
                          fk_poi_id: t.id
                        }
                      }).then(function () {
                        cb()
                      }).catch(function (err) {
                        cb(err)
                      })
                    } else {
                      cb()
                    }
                                        
                  }).catch(function (err) {
                    cb(err);
                  })

                }, function (err){
                  if(err){
                    callback(err)
                  }else{
                    callback()
                  }
                })
              }else{
                callback()
              }
            },
            updateOtherPois:  ['updateRoutePoiMappings', function (result, cb){
              if(!itm.route_planning) {
                return cb()
              }

              let where = "";
              let query = " UPDATE route_templates_poi_mappings SET active = false WHERE ";

              if (trackPoi.length > 0) {
                where = " fk_route_template_id = " + rec_id + " AND fk_poi_id NOT IN (" + trackPoi + ")"
              } else {
                where = " fk_route_template_id = " + rec_id
              }

              query += where;
              models.sequelize.query(query, { type: models.sequelize.QueryTypes.UPDATE }).then(function () {
                cb()
              }).catch(function(err){
                cb(err)
              })
            }],
            tagsInsertion: function(cb){
              if (!routeTemplate.tags)
                return cb();

                                
              tagsHelper.processTags(routeTemplate.tags, rec_id, fk_company_id, tagsHelper.tagType.route_templates, function (err) {
                cb(err)
              })
        
            }
          }, function (err){
            if(err){
              res.status(500).send('Internal server error' + err)
            }else{
              res.status(200).send( {
                id: itm.id,
                name: routeTemplate.name,
                msg: 'route template updated successfully '
              })
            }
          })
        }else{
          return res.status(400).send('Invalid route template ID')
        }

      }).catch(function (err) {
        res.status(500).send('Internal server error' + err)
      })
    }
  }).catch(function (err) {
    res.status(500).send('Internal server error' + err)
  })
};

exports.delete = function (req, res) {

  const fk_company_id = req.headers.fk_c_id;
  const routeId = req.body.id || null;

  if(!routeId) {
    return res.status(400).send('Invalid route template ID')
  }

  models.route_template.update({active: false}, {
    where : {
      route_id : routeId,
      fk_c_id: fk_company_id,
      active: true
    }
  }).then(function (deleted) {

    if(deleted === 1){
      res.status(200).send('route template deleted successfully ')
    }else{
      return res.status(400).send('Invalid route template ID')
    }

  }).catch(function () {
    res.status(500).send('Internal server error')
  })

};
