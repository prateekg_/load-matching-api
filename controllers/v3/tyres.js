const moment = require('moment-timezone');
const async = require('async');
const models = require('../../db/models/index');
const utils = require('../../helpers/util');
const APP_LOGGER = require('../../helpers/logger');
const LOGGER = APP_LOGGER.logger;

exports.getAllTyres = function (req, res) {

  const searchKeyword = req.query.q || '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  let tyre_status = req.query.status;
  const fk_asset_id = req.query.asset_id; // asset primary key
  let asset_condition = "";
  let sub_query = "";

  if(fk_asset_id){
    // get only tyres mounted on assets
    asset_condition = " AND a.id = "+ fk_asset_id +" AND atm.connected";
    tyre_status = "mounted"
  }else if(!tyre_status){
    return res.status(400).send("enter tyre status")
  }

  if(tyre_status === 'mounted'){

    sub_query = "SELECT distinct(t.id) as id, t.tyre_id, t.brand, t.type, t.thread, t.size, t.psi, t.cost as \"costIncurred\", t.model, t.manufacturer,"+
            " json_build_object('percentage', 0,'distance',t.used_km)::jsonb as usage,"+
            " json_build_object('id', a.id,'licNo',a.lic_plate_no,'operator',c.cname,'type', atp.type)::jsonb as asset" +
            " FROM groups g"+
            " INNER JOIN tyre_group_mappings tgm on tgm.fk_group_id = g.id AND tgm.connected"+
            " INNER JOIN tyres t on t.id = tgm.fk_tyre_id AND t.active"+
            " LEFT JOIN asset_tyre_mappings atm on atm.fk_tyre_id = t.id AND atm.connected"+
            " LEFT JOIN assets a on a.id = atm.fk_asset_id"+
            " LEFT JOIN companies c on c.id = a.fk_c_id"+
            " LEFT JOIN asset_types atp on atp.id = a.fk_ast_type_id"+
            " LEFT JOIN asset_group_mappings agm on agm.fk_asset_id = a.id AND agm.fk_group_id = g.id"+
            " WHERE " + groupCondition + " AND atm.connected " + asset_condition +
            " AND ( t.tyre_id ilike '%"+ searchKeyword +"%' OR t.type ilike '%"+ searchKeyword +"%' OR a.lic_plate_no ilike '%"+ searchKeyword +"%')";

  }else if(tyre_status === 'unmounted'){
    sub_query = "SELECT distinct(t.id) as id, t.tyre_id, t.brand, t.type, t.thread, t.size, t.psi, t.cost as \"costIncurred\", t.model, t.manufacturer,"+
            " json_build_object('percentage', 0,'distance',t.used_km)::jsonb as usage, null as asset" +
            " FROM groups g"+
            " INNER JOIN tyre_group_mappings tgm on tgm.fk_group_id = g.id AND tgm.connected"+
            " INNER JOIN tyres t on t.id = tgm.fk_tyre_id AND t.active"+
            " LEFT JOIN (" +
            "   SELECT atm.fk_asset_id, atm.fk_tyre_id, atm.connected" +
            "   FROM asset_tyre_mappings atm" +
            "   INNER JOIN ("+
            "       SELECT max(tis), fk_tyre_id FROM asset_tyre_mappings atm GROUP BY atm.fk_tyre_id" +
            "   ) atm1 on atm1.fk_tyre_id = atm.fk_tyre_id and atm1.max = atm.tis" +
            " ) atm on atm.fk_tyre_id = t.id "+
            " LEFT JOIN assets a on a.id = atm.fk_asset_id"+
            " LEFT JOIN companies c on c.id = a.fk_c_id"+
            " LEFT JOIN asset_types atp on atp.id = a.fk_ast_type_id"+
            " LEFT JOIN asset_group_mappings agm on agm.fk_asset_id = a.id AND agm.fk_group_id = g.id"+
            " WHERE " + groupCondition +" AND COALESCE(atm.connected, false) = false" +
            " AND ( t.tyre_id ilike '%"+ searchKeyword +"%' OR t.type ilike '%"+ searchKeyword +"%' )";
  }else{
    return res.status(400).send("enter valid tyre status")
  }


  let query = "SELECT d.*, count(*) OVER()::INT AS total_count FROM (" +
            sub_query +
        " ) d"+
    " ORDER BY d.tyre_id " +
    " LIMIT " + limit + " OFFSET " + offset;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    let total_rows = 0;

    data.forEach(function (i) {
      total_rows = i.total_count;
      i.total_count = undefined
    });

    res.json({
      total_count: total_rows,
      data: data
    })
  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send()
  })
};

exports.getAllTyresSummary = function (req, res) {

  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  async.auto({
    totalMounted:  function (cb) {

      let query = "SELECT count(d.id)::INT FROM (" +
                    " SELECT distinct(t.id) as id FROM groups g"+
                    " INNER JOIN tyre_group_mappings tgm on tgm.fk_group_id = g.id AND tgm.connected"+
                    " INNER JOIN tyres t on t.id = tgm.fk_tyre_id AND t.active"+
                    " LEFT JOIN asset_tyre_mappings atm on atm.fk_tyre_id = t.id AND atm.connected"+
                    " LEFT JOIN assets a on a.id = atm.fk_asset_id"+
                    " LEFT JOIN asset_group_mappings agm on agm.fk_asset_id = a.id AND agm.fk_group_id = g.id AND agm.connected"+
                    " WHERE " + groupCondition + " AND atm.connected " +
                ") d";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (tyres) {
        cb(null, tyres.count)
      }).catch(function (err) {
        console.log("ERROR : totalMounted - ", err);
        cb(err)
      })
    },
    totalUnmounted: function (cb) {

      let query = "SELECT count(d.id)::INT FROM (" +
                    " SELECT distinct(t.id) as id" +
                    " FROM groups g"+
                    " INNER JOIN tyre_group_mappings tgm on tgm.fk_group_id = g.id AND tgm.connected"+
                    " INNER JOIN tyres t on t.id = tgm.fk_tyre_id AND t.active"+
                    " LEFT JOIN (" +
                    "   SELECT atm.fk_asset_id, atm.fk_tyre_id, atm.connected" +
                    "   FROM asset_tyre_mappings atm" +
                    "   INNER JOIN ("+
                    "       SELECT max(tis), fk_tyre_id FROM asset_tyre_mappings atm GROUP BY atm.fk_tyre_id" +
                    "   ) atm1 on atm1.fk_tyre_id = atm.fk_tyre_id and atm1.max = atm.tis" +
                    " ) atm on atm.fk_tyre_id = t.id "+
                    " WHERE " + groupCondition +" AND COALESCE(atm.connected, false) = false" +
                    " GROUP BY t.id" +
                ") d";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (tyres) {
        cb(null, tyres.count)
      }).catch(function (err) {
        console.log("ERROR : totalUnmounted - ", err);
        cb(err)
      })
    },
    totalTyres: ['totalMounted','totalUnmounted',function (results, cb) {
      cb(null, results.totalMounted + results.totalUnmounted)
    }],
    mounted: ['totalTyres','totalUnmounted', function (results, cb) {
      cb(null, Math.round((results.totalTyres - results.totalUnmounted)/ results.totalTyres * 100) || 0)
    }],
    totalNeedsServicing: function (cb) {

      let query = " SELECT DISTINCT(ts.id) as id, ts.scheduled_tis"+
                " FROM groups g"+
                " INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id"+
                " LEFT JOIN assets a on a.id = agm.fk_asset_id"+
                " INNER JOIN tyre_servicings ts on ts.fk_asset_id = a.id AND ts.active" +
                " WHERE " + groupCondition + " AND ts.active AND ts.is_scheduled";

      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (tyre_services) {

        let today = moment();
        let total_due = 0;

        tyre_services.forEach(function (i) {

          let scheduled = moment.unix(i.scheduled_tis);
          let due_difference = scheduled.diff(today, 'days');

          if(due_difference < 30){
            total_due++
          }
        });

        cb(null, total_due)
      }).catch(function (err) {
        console.log("ERROR : totalNeedsServicing - ", err);
        cb(err)
      })
    },
    totalOverInflated: function (cb) {
      cb(null, 0)
    }
  }, function (err, results) {
    if(err){
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send("Internal error")
    }else{
      res.json(results)
    }
  })
};

exports.getAllTyreServicing = function (req, res) {

  const searchKeyword = req.query.q || '';
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  const tyre_service_status = req.query.status;
  let tyre_service_status_condition = "";
  const fk_asset_id = req.query.asset_id; // asset primary key
  let asset_condition = "";
  let orderCondition = "";

  if(tyre_service_status === 'scheduled'){
    tyre_service_status_condition = " AND ts.is_scheduled";
    orderCondition = "ORDER BY d.\"scheduledTis\" ASC"
  }else if(tyre_service_status === 'completed'){
    tyre_service_status_condition = " AND ts.is_scheduled = false";
    orderCondition = "ORDER BY d.\"completedTis\" DESC"
  }else{
    return res.status(400).send("Enter valid status")
  }

  if(fk_asset_id){
    // get only tyres mounted on assets
    asset_condition = " AND a.id = "+ fk_asset_id;
  }


  let query = "SELECT d.*, count(*) OVER()::INT AS total_count FROM (" +
            " SELECT DISTINCT(ts.id) as id, ts.task,ts.tyres::jsonb,ts.location,ts.scheduled_tis as \"scheduledTis\", ts.completed_tis as \"completedTis\"," +
            " ts.est_cost as \"estimatedCost\",ts.cost, ts.odometer,"+
            " json_build_object('fullname', u.firstname || ' ' || u.lastname, 'firstname', u.firstname,'lastname',u.lastname,'tis', ts.scheduled_tis)::jsonb as \"createdBy\","+
            " json_build_object('id', a.id,'licNo',a.lic_plate_no,'operator',c.cname,'type', atp.type)::jsonb as asset"+
            " FROM groups g"+
            " INNER JOIN asset_group_mappings agm on agm.fk_group_id = g.id"+
            " LEFT JOIN assets a on a.id = agm.fk_asset_id"+
            " LEFT JOIN companies c on c.id = a.fk_c_id"+
            " LEFT JOIN asset_types atp on atp.id = a.fk_ast_type_id"+
            " INNER JOIN tyre_servicings ts on ts.fk_asset_id = a.id AND ts.active" +
            " LEFT JOIN users u on u.id = ts.fk_u_id" +
            " WHERE " + groupCondition + " AND (ts.task ilike '%"+ searchKeyword +"%' OR a.lic_plate_no ilike '%"+ searchKeyword +"%') AND ts.active "+ tyre_service_status_condition + asset_condition +
        " ) d "+ orderCondition +
        " LIMIT " + limit + " OFFSET " + offset;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    let total_rows = 0;

    data.forEach(function (i) {
      total_rows = i.total_count;

      let today = moment();
      let scheduled = moment.unix(i.scheduledTis);
      let due_difference = scheduled.diff(today, 'days');
      let due_message = "scheduled";

      if(due_difference > 0 && due_difference < 30){
        due_message = "due soon"
      }else if(due_difference <= 0){
        due_message = "over due"
      }

      i.due = {
        tis: i.scheduledTis,
        msg: due_message
      };

      i.total_count = undefined
    });

    res.json({
      total_count: total_rows,
      data: data
    })
  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send()
  })
};

exports.mountTyre = function (req, res) {

  const tyre = req.body.tyre || {};
  const asset = req.body.asset || {};
  const timestamp = moment().unix();

  if(tyre && !tyre.id){
    res.status(400).send("invalid tyre id")
  }else if(asset && !asset.id){
    res.status(400).send("invalid asset id")
  }else{

    let new_map = {
      fk_asset_id: asset.id,
      fk_tyre_id: tyre.id,
      tis: timestamp,
      connected: true
    };

    models.asset_tyre_mapping.findOrCreate({
      where:{
        fk_asset_id: new_map.fk_asset_id,
        fk_tyre_id: new_map.fk_tyre_id,
        connected: true
      },
      defaults: new_map
    }).spread(function (mapping, created) {
      if(!created){
        res.status(409).send("Tyre already mounted on asset")
      }else{
        res.status(201).send("Tyre sucessfully mounted on asset")
      }
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send()
    })
  }
};

exports.unmountTyre = function (req, res) {

  const tyre = req.body.tyre || {};
  const asset = req.body.asset || {};
  const timestamp = moment().unix();

  if(tyre && !tyre.id){
    res.status(400).send("invalid tyre id")
  }else if(asset && !asset.id){
    res.status(400).send("invalid asset id")
  }else{

    let new_map = {
      fk_asset_id: asset.id,
      fk_tyre_id: tyre.id,
      tis: timestamp,
      connected: false
    };

    models.asset_tyre_mapping.findOne({
      where:{
        fk_asset_id: new_map.fk_asset_id,
        fk_tyre_id: new_map.fk_tyre_id,
        connected: true
      }
    }).then(function (record) {
      if(!record){
        res.status(400).send("tyre is not currently mounted on the asset")
      }else{
        models.asset_tyre_mapping.update(new_map, {
          where:{
            fk_asset_id: new_map.fk_asset_id,
            fk_tyre_id: new_map.fk_tyre_id,
            connected: true
          }
        }).then(function (created) {
          if(created === 1){
            res.status(201).send("Tyre unmounted from asset")
          }else{
            res.status(500).send("error unmounting tyre from asset")
          }
        }).catch(function (err) {
          LOGGER.error(APP_LOGGER.formatMessage(req, err));
          res.status(500).send()
        })
      }
    }).catch(function (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      res.status(500).send()
    })

  }
};