
const async = require('async');
const models = require('../../db/models');
const nodemailer = require('nodemailer');
const _ = require('lodash');
const PolylineUtil = require('polyline-encoded');
const moment = require('moment');
const AWS = require('aws-sdk');
const fs = require('fs');
const geolib = require('geolib');

const env       = process.env.NODE_ENV || 'development';
const config    = require('../../config/index')[env];
const mailerConfig = config.mailer;
const utils = require('../../helpers/util');
const nuDynamoDb = require('../../helpers/dynamoDb');
const s3_config = require('../../config/aws/s3.json');
AWS.config.update(s3_config);
let s3 = new AWS.S3();
const nuRedis = require('../../helpers/nuRedis');

//Include services 
const consignmentService = require('../../services/consignment_service');
const commonService = require('../../services/common_service');
const xlsDownload = require('../../helpers/downloadHelper');
const file_base_path = __dirname + '/../../temp_reports/';
const tableConfigsHelper = require('../../helpers/table_config');
const CONSIGNMENT_TABLE_NAME = "consignment-list-table";

function processTripSharingWithExternalUsers(consignment,mainCallback){

  if(consignment.shared_with && consignment.shared_with.length > 0){
    let transporter = nodemailer.createTransport(mailerConfig);
    let subject = "Consignment shared access - "+ consignment.fk_consignment_id;

    async.each(consignment.shared_with, function (newExternalUser, callback) {

      async.auto({
        external_user: function (ext_user_cb) {
          models.user_external.findOrCreate({
            where: {
              email: newExternalUser.email
            },
            defaults: newExternalUser
          }).spread(function (user_external) {
            ext_user_cb(null, {id:user_external.id})
          }).catch(function (err) {
            ext_user_cb(err)
          })
        },
        share_trip: ['external_user', function (result, ext_user_cb) {
          let external_user = result.external_user;
          let user_trip_mapping = {
            fk_consignment_id: consignment.fk_consignment_id,
            fk_u_id: consignment.fk_u_id,
            fk_user_external_id: external_user.id,
            fk_c_id: consignment.fk_c_id
          };

          if(newExternalUser.active && newExternalUser.active === false){
            // disable trip sharing
            models.consignment_user_mapping_external.update({
              active: false
            },{
              where: {
                fk_user_external_id: external_user.id,
                fk_consignment_id: user_trip_mapping.fk_consignment_id
              },
              defaults: user_trip_mapping
            }).spread(function () {
              ext_user_cb(null, {created: false})
            }).catch(function () {
              ext_user_cb(null, {created: false})
            })

          }else{
            // "Creating new mapping"

            models.consignment_user_mapping_external.findOrCreate({
              where: {
                fk_user_external_id: external_user.id,
                fk_consignment_id: user_trip_mapping.fk_consignment_id,
                active: true
              },
              defaults: user_trip_mapping
            }).spread(function (mapping, created) {
              ext_user_cb(null, {created: created})
            }).catch(function () {
              ext_user_cb(null, {created: false})
            })
          }
        }],
        send_mail: ['share_trip', function (result, ext_user_cb) {
          if(result.share_trip.created) {
            let consignment_url = config.url.domain +"/tracking";

            let mailOptions = {
              from: 'noreply@numadic.com',
              to: newExternalUser.email,
              bcc: 'noreply@numadic.com',
              subject: subject,
              html: `<html>
                                       <body style="width:80%; color: #444444; font-family: tahoma, sans-serif; margin: 50px;">
                                          <p>&nbsp;</p>
                                          <p>Hello,</p>
                                          <p>${consignment.user_name} at ${consignment.company_name} has shared a consignment tracking link with you.</p>
                                          <p>Please click the link below, or copy and paste it into your browser.</p>
                                          <p><a href="${consignment_url}">${consignment_url}</a></p>
                                          <p><br />This tracking link was generated on Numadic's Fleet & Consignment management platform. Learn more <a href="https://numadic.com/demo">here</a>.<br /><br />
                                             <br />Thank you,</span>
                                          </p>
                                          <div>&nbsp;</div>
                                          <div>Numadic  Team</div>
                                       </body>
                                    </html>`
            };

            transporter.sendMail(mailOptions, function (error) {
              if (error) {
                console.log("ERROR :", error);
              }
              ext_user_cb()
            });
          }else{
            ext_user_cb()
          }
        }]
      }, function () {
        callback()
      })
    }, function () {
      mainCallback()
    })
  }else{
    mainCallback()
  }
}

function processWaypoints(trip, mainCb){

  async.auto({
    actual_log: function (cb) {
      if(trip.trip_log && trip.trip_log.trip){
        cb(null, trip.trip_log.trip)
      }else{
        cb(null, [])
      }
    },
    source: ['actual_log', function (results, cb) {
      let i = trip.consignor;
      let item = {
        seq_no: 1,
        dispatchAction: "pickup",
        type: "planned",
        lname: i.lname,
        arrivalTis: i.est_start_tis,
        departureTis: i.est_end_tis,
        dwellDuration: Math.round(moment.duration(i.stop_dur, 'seconds').asMinutes()),
        violations: 0,
        crossed: false,
        label: 'A',
        lat: i.lat,
        lon: i.lon,
        avgSpd: 0,
        distance: 0
      };

      let matchedIndex = _.findIndex(results.actual_log, function (o) {
        // Check if poi id is same or lat,lon within 50 meters
        return i.Id === o.fk_poi_id || geolib.isPointInCircle({
          latitude: o.lat,
          longitude: o.lon
        },{
          latitude: i.lat,
          longitude: i.lon
        }, 50)
      });

      if(matchedIndex >= 0){
        let matchedItem = results.actual_log[matchedIndex];
        item.arrivalTis = matchedItem.StartTis;
        item.departureTis = matchedItem.EndTis;
        item.dwellDuration = Math.round(moment.duration(matchedItem.DwellTime, 'seconds').asMinutes());
        item.crossed = true
      }
      cb(null, { item: item, matchedIndex: matchedIndex })
    }],
    destination: ['actual_log', function (results, cb) {
      let i = trip.consignee;
      let item = {
        seq_no: 2,
        dispatchAction: "dropoff",
        type: "planned",
        lname: i.lname,
        arrivalTis: i.est_start_tis,
        departureTis: i.est_end_tis,
        dwellDuration: Math.round(moment.duration(i.stop_dur, 'seconds').asMinutes()),
        violations: 0,
        crossed: false,
        label: 'B',
        lat: i.lat,
        lon: i.lon,
        avgSpd: 0,
        distance: 0
      };

      let matchedIndex = _.findLastIndex(results.actual_log, function (o) {
        // Check if poi id is same or lat,lon within 50 meters
        return i.Id === o.fk_poi_id || geolib.isPointInCircle({
          latitude: o.lat,
          longitude: o.lon
        },{
          latitude: i.lat,
          longitude: i.lon
        }, 50)
      });

      if(matchedIndex >= 0){
        let matchedItem = results.actual_log[matchedIndex];
        item.arrivalTis = matchedItem.StartTis;
        item.departureTis = matchedItem.EndTis;
        item.dwellDuration = Math.round(moment.duration(matchedItem.DwellTime, 'seconds').asMinutes());
        item.crossed = true
      }
      cb(null, { item: item, matchedIndex: matchedIndex })
    }],
    intransit: ['actual_log', 'source', 'destination', function (results, cb) {

      let startIndex = results.source.matchedIndex;
      let source = results.source.item;
      let endIndex = results.destination.matchedIndex;
      let destination = results.destination.item;
      let log = results.actual_log;
      let event = {
        type: "intransit",
        lname: '',
        arrivalTis: source.departureTis || null,
        departureTis: destination.arrivalTis || null,
        dwellDuration: Math.round(moment.duration((destination.arrivalTis - source.departureTis), 'seconds').asMinutes()) || 0,
        violations: 0,
        crossed: false,
        label: '',
        lat: null,
        lon: null,
        avgSpd: 0,
        distance: 0,
        est_distance: Number((trip.est_distance /1000).toFixed(1)),
        est_time: trip.est_time
      };

      let segment = log.splice(startIndex, endIndex);

      let distance = Number((_.sumBy(segment, 'Distance') /1000).toFixed(1));
      event.crossed = distance > 0;
      event.distance = distance;
      event.avgSpd = _.meanBy(segment, 'AvgSpd');

      cb(null, event)
    }]
  }, function (err, results) {
    let log = [];
    log.push(results.source.item);
    log.push(results.intransit);
    log.push(results.destination.item);

    mainCb(null, log)
  })
}

function processDriverObjectToStringArray(drivers) {
  return _.compact(_.map(drivers, function (o) {
    let name = "";
    if(o.firstName){
      name += o.firstName
    }
    if(o.lastName){
      name += " " + o.lastName
    }
    return name ? name.trim() : null
  }))
}


exports.getSingleConsignment = function (req, res) {

  const id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? ` g.gid = '${gid}' ` : ` g.id in ${groupIds} `;

  if (!id) {
    return res.status(400).send('Invalid consignment ID')
  }

  let query = `SELECT c.id, invoice_no, consignor, consignee, material_type, product_code, total_packages, weight, 
                    weight_unit, c.shared_with, fk_trip_id, c.status, ext_consignment_no as consignment_no, c.est_distance, c.est_time, 
                    c.geo_trip_route as "geoRoute", hsn_code, goods_value, packaging_type,
                    cd.user_registry, tp.trip_log->'arrival' status_tis,
                    json_agg(json_build_object('id', sg.id,'name', sg.name, 'is_admin', sg.is_admin)) as group_access
                FROM consignments c
                INNER JOIN company_details cd on c.fk_c_id = cd.fk_c_id
                LEFT JOIN trip_plannings tp on tp.id = c.fk_trip_id
                INNER JOIN consignment_group_mappings cgm ON cgm.fk_consignment_id = c.id and cgm.connected
                INNER JOIN groups g ON cgm.fk_group_id = g.id and cgm.connected
                LEFT JOIN (
                	SELECT cgm.fk_consignment_id, g.id, g.name, g.is_admin 
                	FROM consignment_group_mappings cgm
                	INNER JOIN groups g on g.id = cgm.fk_group_id
					WHERE g.fk_c_id = $fk_c_id AND cgm.fk_consignment_id = $consignment_id AND cgm.connected        
                ) sg on sg.fk_consignment_id = c.id
                WHERE g.active AND g.fk_c_id = c.fk_c_id AND c.fk_c_id = $fk_c_id and c.id =  $consignment_id AND ${groupCondition} 
                GROUP BY c.id, cd.id, tp.id
                LIMIT 1`;

  models.sequelize.query(query, {
    bind: {
      fk_c_id:        fk_c_id,
      consignment_id: id,

    }, type: models.sequelize.QueryTypes.SELECT})
    .spread(function (record) {
      if(!record){
        res.status(404).send("Record not found")
      }else{
        res.json(record)
      }
    }).catch(function () {
      res.status(500).send("Internal server error")
    })
};


//TODO: Update date filters on proper status' dates.
/**
 * Shows consignment list based on filters( assigned, unassigned, picked, delivered and incomplete)
 * Updated to show consignments created from dashboard (Consignment table also has enteries created from CMA app which creates trip based on consignment source and destination)
 * @param {*} req 
 * @param {*} res 
 * @param {String} res_type
 */
exports.getAllConsignments = function (req, res, res_type) {
  res_type = res_type ? res_type : 'list';
  const fk_c_id = req.headers.fk_c_id;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;
  const searchKeyword = req.query.q ? req.query.q.toLowerCase() : '';
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const consignmentStatus = req.query.status;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const start_tis = req.query.start_tis ? req.query.start_tis : null;
  const end_tis = req.query.end_tis ? req.query.end_tis : null;
  const search_cols = req.query.search || null;
  let search_col_condition = "";
  const sort = req.query.sort;
  let sort_condition = ' ORDER BY con."updatedAt" DESC ';
  let consignmentStatusCondition = "";
  const range_cols = req.query.range_filter;
  let range_filter_condition = "";
  const fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';
  const output_format = req.query.format || 'list';

  if(!consignmentStatus && res_type === 'list'){
    return res.status(400).send("Enter status")
  }

  if(!consignmentStatus && res_type === 'map_location'){
    consignmentStatusCondition = " AND ( con.status = 'assigned' OR con.status = 'picked')";
  }else{
    consignmentStatusCondition = " AND con.status = '"+ consignmentStatus +"'";
  }

  let tableSearchCondition = searchKeyword ? " AND (lower(invoice_no) like '%"+ searchKeyword +"%' OR" +
        " lower(material_type) like '%"+ searchKeyword +"%' OR lower(product_code) like '%"+ searchKeyword +"%' OR" +
        " lower(consignor->>'lname') like '%"+ searchKeyword +"%' OR lower(consignee->>'lname') like '%"+ searchKeyword +"%' OR" +
        " lower(consignor->>'name') like '%"+ searchKeyword +"%' OR lower(consignee->>'name') like '%"+ searchKeyword +"%' OR" +
        " CAST(ext_consignment_no AS TEXT) like '%"+ searchKeyword +"%' OR lower(shipment_no) like '%"+ searchKeyword +"%')" : "";

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'invoice_no',
      client: 'invoice_no'
    }, {
      db: 'material_type',
      client: 'material_type'
    }, {
      db: 'product_code',
      client: 'product_code'
    },{
      db: "consignor->>'lname'",
      client: 'consignor.lname'
    },{
      db: "consignee->>'lname'",
      client: 'consignee.lname'
    },{
      db: "consignor->>'name'",
      client: 'consignor.name'
    },{
      db: "consignee->>'name'",
      client: 'consignee.name'
    },{
      db: 'a.lic_plate_no',
      client: 'asset.licNo'
    },{
      db: 'tp.status',
      client: 'trip.status'
    },{
      db: 'tp.id',
      client: 'trip.id'
    },{
      db: 'asset.operator',
      client: 'c.cname'
    },{
      db: 'ext_consignment_no',
      client: 'consignment_no'
    }]
  );

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'total_packages',
      client: 'total_packages'
    }, {
      db: 'weight',
      client: 'weight'
    }],
    sort_condition
  );

  range_filter_condition = utils.processTableDateRangeFilter(
    range_cols,
    [{
      db: 'extract(epoch from con."createdAt")::INT',
      client: 'created_tis'
    }],
    800 // Approx 2 years data
  );
  range_filter_condition = range_filter_condition.result;

  let timeFrameCondition = (start_tis && end_tis) ? ' and extract(epoch from con."updatedAt")::int between '+ start_tis +' and ' + end_tis : '';

  let query = `SELECT con.id, invoice_no, ext_consignment_no as consignment_no, consignor, consignee, material_type, 
                product_code, total_packages, con.status,
                weight, weight_unit,
                json_build_object('id',a.id,'type',ast.type,'operator', json_build_object('name', c.cname), 'licNo', a.lic_plate_no) as asset,
                json_build_object('id',tp.id,'status',tp.status, 'shipment_no',tp.shipment_no) as trip, 
                    to_json(tp.oth_details->'drivers')::json as drivers, extract(epoch from con."createdAt")::INT created_tis, con."createdAt", tp.trip_log->'lkLocation' lkl,  
                tp.trip_log->'arrival' status_tis,
                count(con.id) OVER()::INT AS total_count
                FROM consignments con
                LEFT JOIN trip_plannings tp on tp.id = con.fk_trip_id
                LEFT JOIN assets a on a.id = tp.fk_asset_id
                LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id
                INNER JOIN consignment_group_mappings cgm ON cgm.fk_consignment_id = con.id and cgm.connected
                INNER JOIN groups g ON cgm.fk_group_id = g.id and cgm.connected
                LEFT JOIN companies c on c.id = a.fk_c_id
                WHERE con.fk_c_id = ${fk_c_id} 
                AND con.active AND g.active AND g.fk_c_id = con.fk_c_id AND ${groupCondition}
                ${consignmentStatusCondition} ${search_col_condition} ${range_filter_condition}
                ${tableSearchCondition} ${timeFrameCondition}
                GROUP BY con.id, a.id, ast.id, c.id, tp.id
                ${sort_condition} `;

  res_type = output_format;
  if(res_type === 'list'){
    query +=" LIMIT "+ limit +" OFFSET "+ offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
    .then(function (consignments) {
      let total_count = 0;

      consignments.forEach(function (consignment) {
        total_count = consignment.total_count;
        consignment.drivers = processDriverObjectToStringArray(consignment.drivers);
        let asset_location = _.merge({tis: null, lname: ''}, _.pick(consignment.lkl,['tis','lname','lat','lon']));
        asset_location.tis = asset_location.tis === 0 ? null : asset_location.tis;
        asset_location.lat = asset_location.lat === 0 ? null : asset_location.lat;
        asset_location.lon = asset_location.lon === 0 ? null : asset_location.lon;
        asset_location.status = utils.processAssetStatus(consignment.lkl);
        consignment.asset = _.merge(consignment.asset, asset_location);

        consignment.total_count = undefined;
        consignment.lkl = undefined
      });

      if (output_format === 'xls') {
        let file_name = ("CONSIGNMENT-LIST-" + _.kebabCase(consignmentStatus.toUpperCase()) + "-" + utils.downloadReportDate()).toString().toUpperCase() + ".xls";
        let file_path = file_base_path + file_name;

        tableConfigsHelper.getTableConfig(CONSIGNMENT_TABLE_NAME, fk_u_id, consignmentStatus, default_columns, function (err, table_config) {

          xlsDownload.convertConsignmentListToXLS(consignmentStatus, consignments, file_base_path, file_name, table_config, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })
        })

      } else {
        res.json({
          count: total_count,
          data: consignments
        })
      }
    }).catch(function (err) {
      console.log(err);
      res.status(500).send("Internal server error")
    })
};

exports.createConsignment = function (req, res) {

  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  let newConsignmentRecord = req.body;

  let requiredKeys = ['geoRoute'];
  let dataValid = utils.validateMinPayload(requiredKeys, newConsignmentRecord);
  if(!dataValid){
    return res.status(400).send("Mandatory fields : "+requiredKeys)
  }

  newConsignmentRecord.fk_c_id = fk_c_id;
  newConsignmentRecord.status = "unassigned";
  newConsignmentRecord.est_distance = Math.round(newConsignmentRecord.geoRoute.paths[0].distance) || 0;
  newConsignmentRecord.est_time = Math.round(moment.duration(newConsignmentRecord.geoRoute.paths[0].time).asMinutes()); //Converting milliseconds to minutes
  newConsignmentRecord.geo_trip_route = newConsignmentRecord.geoRoute;

  let enterprise_company = [];
  enterprise_company.push(newConsignmentRecord.consignee);
  enterprise_company.push(newConsignmentRecord.consignor);
    
  async.auto({
    invoiceNo: function(cb){
      consignmentService.validateUniqueInvoiceNo(newConsignmentRecord.invoice_no, fk_c_id, null, function(err, result){
        if(result){
          if(result.length === 0) cb(null, result);
          else{
            cb({
              code: 400, 
              msg: "Invoice no. already exists"
            })
          }
        }
        else cb(null)
      })
    },
    consignment: ['invoiceNo', function (result, cb) {
      models.consignment.create(newConsignmentRecord).then(function (record) {
        cb(null, record)
      }).catch(function (err) {
        cb({
          code: 500,
          msg: 'Internal server error',
          err: err
        })
      })
    }],
    shareWithExternalUsers: ['consignment', 'creatorInfo', function (result, cb) {
      let record = result.consignment;
      let user = result.creatorInfo;
      processTripSharingWithExternalUsers({
        fk_consignment_id: record.id,
        fk_u_id: req.headers.fk_u_id,
        fk_c_id: fk_c_id,
        shared_with: newConsignmentRecord.shared_with || [],
        user_name: user.name,
        company_name: user.cname
      }, function(){
        cb(null, record)
      })
    }],
    addEnterpriseCompany: ['consignment', function (result, cb) {
      async.each(enterprise_company, function(eCompany, callback){ 
        async.waterfall([
          function(wCallback){
            if(eCompany.gst_no){
              let query = `select * from enterprise_companies where name ='`+eCompany.name + `' and gst_no = '`+ eCompany.gst_no +`'`;
              models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
                .then(function (record) {
                  if(record[0]){ 
                    return callback()
                  }
                  else{
                    wCallback(null)
                  }
                })
            }
            else wCallback(null)
          },
          function(wCallback){
            models.enterprise_company.create({
              name: eCompany.name,
              address: eCompany.lname,
              gst_no: eCompany.gst_no
            }).then(function () {
              wCallback(null)
            }).catch(function (err) {
              wCallback({
                code: 500,
                msg: 'Internal server error: ',
                err: err
              })
            })
          }
        ], function(err){
          if(err) callback(err);
          else callback()
        })
                
      }, function(err){
        if(err) cb(err);
        else cb(null)
      })
    }],
    mapToGroup: ['consignment', function (result, cb) {
      if(newConsignmentRecord.group_access && newConsignmentRecord.group_access.length > 0){
        consignmentService.mapConsignmentToGroups(result.consignment, newConsignmentRecord.group_access, cb)
      }else{
        consignmentService.mapToGroups(result.consignment, cb)
      }
    }],
    creatorInfo: function (cb) {
      let query = `SELECT u.name, c.cname FROM users u 
                        LEFT JOIN companies c on c.id = u.fk_c_id
                        WHERE u.id = ${fk_u_id};`;
      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (record) {
          cb(null, record)
        }).catch(function () {
          cb()
        })
    }
  }, function (err, results) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.json({
        msg: "Consignment created",
        data:{id: results.consignment.id}
      })
    }
  })
};

exports.updateConsignment = function (req, res) {
    
  const fk_u_id = req.headers.fk_u_id;
  const fk_c_id = req.headers.fk_c_id;
  let updateConsignmentRecord = req.body;

  let requiredKeys = ['geoRoute'];
  let dataValid = utils.validateMinPayload(requiredKeys, updateConsignmentRecord);
  if(!dataValid){
    return res.status(400).send("Mandatory fields : "+requiredKeys)
  }
  updateConsignmentRecord = _.omit(updateConsignmentRecord,['status','fk_trip_id']);
  updateConsignmentRecord.est_distance = Math.round(updateConsignmentRecord.geoRoute.paths[0].distance) || 0;
  updateConsignmentRecord.est_time = Math.round(moment.duration(updateConsignmentRecord.geoRoute.paths[0].time).asMinutes()); //Converting milliseconds to minutes
  updateConsignmentRecord.geo_trip_route = updateConsignmentRecord.geoRoute;
    
  async.auto({
    consignment: function (cb) {
      models.consignment.update(updateConsignmentRecord, {
        where: {
          id : updateConsignmentRecord.id,
          fk_c_id: fk_c_id,
          active: true
        }
      }).then(function () {
        cb(null, "Consignment updated")
      }).catch(function (err) {
        console.log("ERROR : ", err);
        cb({
          code: 500,
          msg: 'Internal server error'+ err
        })
      })
    },
    shareWithExternalUsers: ['consignment', 'creatorInfo', function (result, cb) {
      let user = result.creatorInfo;
      processTripSharingWithExternalUsers({
        fk_consignment_id: updateConsignmentRecord.id,
        fk_u_id: req.headers.fk_u_id,
        fk_c_id: fk_c_id,
        shared_with: updateConsignmentRecord.shared_with || [],
        user_name: user.name,
        company_name: user.cname
      }, cb)
    }],
    creatorInfo: function (cb) {
      let query = `SELECT u.name, c.cname FROM users u 
                        LEFT JOIN companies c on c.id = u.fk_c_id
                        WHERE u.id = ${fk_u_id};`;
      models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
        .spread(function (record) {
          cb(null, record)
        }).catch(function () {
          cb()
        })
    },
    mapToGroup: ['consignment', function (result, cb) {
      if(updateConsignmentRecord.group_access && updateConsignmentRecord.group_access.length > 0){
        consignmentService.mapConsignmentToGroups(updateConsignmentRecord, updateConsignmentRecord.group_access, cb)
      }else{
        cb()
      }
    }],
  }, function (err, results) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.json(results.consignment)
    }
  })
};

exports.deleteConsignment = function (req, res) {
  const fk_c_id = req.headers.fk_c_id;
  const consignmentId = req.body.id || null;

  if(!consignmentId) {
    return res.status(400).send('Invalid consignment ID')
  }

  models.consignment.update({active: false}, {
    where : {
      id : consignmentId,
      fk_c_id: fk_c_id,
      status: 'unassigned',
      active: true
    }
  }).then(function (deleted) {

    if(deleted === 1){
      res.status(200).send('Consignment deleted successfully ')
    }else{
      return res.status(400).send('No record found with this consignment ID')
    }

  }).catch(function () {
    res.status(500).send('Internal server error')
  })
};

exports.getInfo = function (req, res) {
  const id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? ` g.gid = '${gid}' ` : ` g.id in ${groupIds} `;

  if (!id) {
    return res.status(400).send('Invalid consignment ID')
  }

  async.auto({
    consignment: function (cb) {
      let query = `SELECT con.*, tp.trip_log->'arrival' status_tis, a.id as fk_asset_id, tp.waypoints, tp.trip_log,
                    json_build_object('id',tp.id, 'status',tp.status) as trip,
                    json_build_object('id', a.id, 'licNo', a.lic_plate_no,'type', ast.type) as asset,
                    json_agg(json_build_object('id', sg.id,'name', sg.name, 'is_admin', sg.is_admin, 'total_users', sg.total_users)) as group_access,
                    tp.oth_details->>'drivers' as drivers,
                    con."updatedAt", con."createdAt", e.e_id,
                    tp.trip_log->'arrival' status_tis,
                    extract(epoch from con."updatedAt")::INT updated_tis,
                    extract(epoch from con."createdAt")::INT created_tis
                  FROM consignments con
                  INNER JOIN consignment_group_mappings cgm ON cgm.fk_consignment_id = con.id and cgm.connected
                  INNER JOIN groups g ON cgm.fk_group_id = g.id and cgm.connected
                  LEFT JOIN trip_plannings tp on tp.id = con.fk_trip_id
                  LEFT JOIN (
                    SELECT cgm.fk_consignment_id, g.id, g.name, g.is_admin, count(ugm.id) as total_users 
                    FROM consignment_group_mappings cgm
                    INNER JOIN groups g on g.id = cgm.fk_group_id
                    LEFT JOIN user_group_mappings ugm on ugm.fk_group_id = g.id AND ugm.connected
                    WHERE g.fk_c_id = $fk_c_id AND cgm.fk_consignment_id = $consignment_id AND cgm.connected
                    GROUP BY cgm.fk_consignment_id, g.id        
                  ) sg on sg.fk_consignment_id = con.id
                  LEFT JOIN assets a on a.id = tp.fk_asset_id
                  LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id
                  LEFT JOIN company_device_inventory_vw cdi on con.id = cdi.fk_consignment_id and cdi.connected
                  LEFT JOIN entities e on cdi.fk_entity_id = e.id
                  WHERE g.active and g.fk_c_id = con.fk_c_id and con.active and con.fk_c_id = $fk_c_id and con.id = $consignment_id and` + groupCondition + ` GROUP BY con.id, tp.id, a.id, ast.type, e.e_id LIMIT 1 `;

      models.sequelize.query(query, {
        bind: {
          fk_c_id: fk_c_id,
          consignment_id: id,
        }, type: models.sequelize.QueryTypes.SELECT})
        .spread(function (record) {
          if(!record){
            cb({
              code: 404,
              msg: "Record not found"
            })
          }else{
            record = _.omit(record, ['fk_c_id', 'fk_trip_id', 'active']);

            if(record.consignor && !record.consignor.act_start_tis){
              record.consignor.act_start_tis = null;
              record.consignor.act_end_tis = null
            }

            if(record.consignee && !record.consignee.act_start_tis){
              record.consignee.act_start_tis = null;
              record.consignee.act_end_tis = null
            }

            record.drivers = utils.cleanDriverObject(record.drivers);
            nuRedis.getEntityDetails(record.e_id, function(err, result){ 
              record.entity_tis = result? result.tis : null;
              delete record.e_id;
              cb(null, record)
            })
          }
        }).catch(function (err) {
          cb({
            code: 500,
            msg: "Internal server error",
            err: err
          })
        })
    },
    waypoints : ['consignment', function (result, cb) {
      processWaypoints(result.consignment, function (err, data) {
        cb(null, data)
      })
    }],
    actualPath: ['consignment', function (result, cb) {
      let consignment = result.consignment;

      if(consignment.fk_asset_id && consignment.consignor && consignment.consignor.act_start_tis &&
                consignment.consignee && consignment.consignee.act_start_tis){

        let params = {
          fk_asset_id: consignment.fk_asset_id,
          actStartTis: consignment.consignor.act_start_tis,
          actEndTis: consignment.consignee.act_start_tis,
        };

        nuDynamoDb.getAssetLocationFromAPI(params, function (err, data) {
          if(data.length > 0){

            let latlngs = [];
            data.forEach(function (i) {
              latlngs.push([i.lat, i.lon])
            });
            cb(null, PolylineUtil.encode(latlngs))
          }else{
            cb(null, "")
          }
        })
      }else{
        cb(null, "")
      }
    }]
  }, function (err, results) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      let consign = results.consignment;
      delete consign.trip_log;
      delete consign.waypoints;
      delete consign.fk_asset_id;
      delete consign.est_distance;
      delete consign.est_time;

      _.merge(results,consign);

      delete results.consignment;
      res.json(results)
    }
  })
};

exports.getMapAutoTripTrail = function (req, res) {
  let id = req.query.id;
  let fk_c_id = req.headers.fk_c_id;
  if (!id) {
    return res.status(400).send('Invalid consignment trail ID')
  }
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let gid = req.query.gid;
  let groupCondition = gid ? ` g.gid = '${gid}' ` : ` g.id in ${groupIds} `;

  async.auto({
    auto_trip: function (cb) {
      let query = `SELECT atp.trip_log, json_build_object('licNo', null,
                'id', null, 'type', null) as asset, json_build_object('id', con.id) as consignment
                FROM consignments con
                INNER JOIN consignment_group_mappings cgm ON cgm.fk_consignment_id = con.id and cgm.connected
                INNER JOIN groups g ON cgm.fk_group_id = g.id and cgm.connected
                INNER JOIN auto_trips atp ON atp.fk_asset_id = con.id
                WHERE atp.tid = $id AND con.fk_c_id = $fk_c_id AND ${groupCondition}
                LIMIT 1`;
      models.sequelize.query(query, {bind:{id: id, fk_c_id: fk_c_id}, type: models.sequelize.QueryTypes.SELECT})
        .spread(function (data) {
          if(data){
            let asset = data.asset;
            let trip_status = "completed";
            let tl = data.trip_log;
            let d = {
              fk_asset_id: tl.fk_asset_id,
              start_tis: tl.departure,
              end_tis: tl.arrival
            };
            let lkl = tl.lkLocation;
            let status = lkl.ign === 'B' ? "moving" : "stationary";
            let last_known_location = {
              ign: lkl.ign,
              lat: lkl.lat,
              lname: lkl.lname,
              lon: lkl.lon,
              osf: lkl.osf,
              spd: lkl.spd,
              status: status,
              tis: lkl.tis
            };
            asset = _.merge(asset, last_known_location);

            let waypoints = [];
            if(tl.active){
              trip_status = "intransit";
              last_known_location.duration = 0;
              waypoints.push(last_known_location)
            }

            tl.trip.length && tl.trip.forEach(function (i) {
              waypoints.push({
                ign: 'A',
                lat: i.Lat,
                lname: i.LName,
                lon: i.Lon,
                osf: false,
                spd: 0,
                status: "stationary",
                tis: i.StartTis,
                duration: Math.round(i.DwellTime / 60)
              })
            });

            tl.violations && tl.violations.forEach(function (i) {
              waypoints.push({
                ign: i.ign,
                lat: i.lat,
                lname: i.lname,
                lon: i.lon,
                osf: i.type === 0 ,
                spd: i.spd,
                status: i.type === 0 ? "overspeeding" : "hardbraking",
                tis: i.tis,
                duration: 0
              })
            });
            cb(null,{
              d: d,
              consignment: data.consignment,
              asset: asset,
              waypoints: _.sortBy(waypoints, 'tis'),
              status: trip_status
            })
          }else{
            cb({
              code: 404,
              msg: 'Not Found'
            })
          }
        }).catch(function (err) {
          cb({
            code: 500,
            msg: 'Internal server error',
            err: err
          })
        })
    },
    points: ['auto_trip', function (result, cb) {
      let trip = result.auto_trip.d;
      let consignment = result.auto_trip.consignment;
      nuDynamoDb.getAssetLocationFromAPI({
        asset_type: 'Consignment',
        fk_asset_id: consignment.id,
        start_tis: trip.start_tis,
        end_tis: trip.end_tis,
        trip_log: {
          departure: trip.start_tis,
          arrival: trip.end_tis
        }
      }, function (err, data) {
        cb(null, data || [])
      })
    }]
  }, function (err, result) {
    if(err){
      if(err.code){
        res.status(err.code).send(err.msg)
      }else{
        res.status(500).send()
      }
    }else{
      let points = result.points;
      result.auto_trip.waypoints.forEach(function (i) {
        points.push(i)
      });
      points = _.orderBy(points, ['tis'],['asc']);
      res.json({
        status: result.auto_trip.status,
        consignment: result.auto_trip.consignment,
        asset: result.auto_trip.asset,
        waypoints: result.auto_trip.waypoints,
        points: points
      })
    }
  })
};

exports.getLogAutoTrip = function (req, res) {
  let id = req.query.id;
  let fk_c_id = req.headers.fk_c_id;
  if (!id) {
    return res.status(400).send('Invalid consignment ID')
  }
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let gid = req.query.gid;
  let groupCondition = gid ? ` g.gid = '${gid}' ` : ` g.id in ${groupIds} `;

  let query = `SELECT NULL lic_plate_no, atp.trip_log->'active' as active, atp.tid as id, atp.trip_log->'distance' as distance, atp.trip_log->'violations' as violations, atp.trip_log->'trip' as activity,
    atp.trip_log as log, count(*) OVER() AS total_count 
    FROM consignments con
    INNER JOIN consignment_group_mappings cgm ON cgm.fk_consignment_id = con.id and cgm.connected
    INNER JOIN groups g ON cgm.fk_group_id = g.id and cgm.connected
    INNER JOIN auto_trips atp ON atp.fk_asset_id = con.id
    WHERE con.id = $id AND con.fk_c_id = $fk_c_id AND ${groupCondition}
    ORDER BY atp.tid DESC`;

  models.sequelize.query(query, {bind:{id: id, fk_c_id: fk_c_id}, type: models.sequelize.QueryTypes.SELECT})
    .then(function (data) {
      async.auto({
        pois: function (cb) {
          let poi_ids = [];
          data.forEach(function (i) {
            poi_ids = _.concat(poi_ids, _.map(i.activity, 'Id'))
          });
          poi_ids = _.uniq(_.compact(poi_ids));
          if (_.size(poi_ids) > 0) {
            let query = `SELECT p.id, pt.type, CASE WHEN p.priv_typ THEN 'company'
                                    ELSE 'general' END category
                                    FROM pois p 
                                    JOIN poi_types pt on pt.id = p.typ
                                    WHERE p.id in (` + poi_ids + `)`;
            models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
              .then(function (data) {
                cb(null, data)
              }).catch(function () {
                cb()
              })
          } else {
            cb()
          }
        },
        data: ['pois', function (results, cb) {
          let totalAssets = 0;
          data.forEach(function (item) {
            totalAssets = Number(item.total_count);
            item.distance = Number((item.distance / 1000).toFixed(1));
            item.violations = item.violations ? item.violations.length : 0;
            item.avgSpd = item.log.avg_speed || null;
            let s = item.activity[0];
            item.start = {
              tis: s.StartTis,
              lname: s.LName
            };
            if (!item.active) {
              // completed trip
              let e = item.activity[item.activity.length - 1];
              item.end = {
                tis: e.StartTis,
                lname: e.LName
              };
              item.duration = Math.round(moment.duration(item.log.time_taken, 'seconds').asMinutes());
              item.stops = item.log.stops
            } else {
              // ongoing trip
              item.end = {
                tis: null,
                lname: null
              };
              item.duration = Math.round(moment.duration((item.log.arrival - item.log.departure), 'seconds').asMinutes());
              item.stops = (item.log.trip.length - 1) || 0
            }

            // detail log
            let detailLog = [];
            let activityLength = item.active ? _.size(item.activity) : _.size(item.activity) - 1;
            let stopsCount = 1;
            _.forEach(item.activity, function (i, index) {

              let typeEvent = "planned";
              let labelText = "";

              if (index === 0) {
                labelText = "A"
              } else if (!item.active && index === activityLength) {
                labelText = "B"
              } else {
                typeEvent = "unplanned";
                labelText = stopsCount.toString();
                stopsCount++
              }

              detailLog.push({
                type: typeEvent,
                lname: i.LName,
                arrivalTis: i.StartTis,
                departureTis: i.EndTis,
                dwellDuration: Math.round(moment.duration(i.DwellTime, 'seconds').asMinutes()),
                label: labelText,
                lat: i.Lat,
                lon: i.Lon,
                avgSpd: 0,
                distance: 0,
                poi: _.find(results.pois, {id: i.Id}) || null
              });

              if (index !== activityLength) {
                detailLog.push({
                  type: "intransit",
                  lname: "",
                  arrivalTis: (i.EndTis + 1),
                  departureTis: item.activity[index + 1] ? (item.activity[index + 1].StartTis - 1) : null,
                  dwellDuration: 0,
                  label: "",
                  lat: null,
                  lon: null,
                  avgSpd: i.AvgSpd,
                  distance: Number((i.Distance / 1000).toFixed(1)),
                  poi: null
                })
              }
            });

            item.log = detailLog;

            item.total_count = undefined;
            item.active = undefined;
            item.activity = undefined
          });
          cb(null, {
            total_count: totalAssets,
            data: data
          });
        }]
      }, function (err, results) {
        res.json(results.data)
      })
    }).catch(function () {
      return res.status(500).send();
    })
};

exports.getEpodInfo = function (req, res) {
  const id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? ` g.gid = '${gid}' ` : ` g.id in ${groupIds} `;

  if (!id) {
    return res.status(400).send('Invalid consignment ID')
  }

  async.auto({
    record: function (cb) {
      let query = `SELECT tc.*, tp.waypoints, cs.tis, cs.note, cs.reason, cs.files_meta, tp.trip_log, json_build_object('id', u.id, 'firstname', u.firstname,'lastname',u.lastname,'contact', cont_no) as driver
                FROM consignments tc
                INNER JOIN consignment_group_mappings cgm ON cgm.fk_consignment_id = tc.id and cgm.connected
                INNER JOIN groups g ON cgm.fk_group_id = g.id and cgm.connected
                LEFT JOIN consignment_statuses cs on cs.fk_consignment_id = tc.id AND cs.is_current
                LEFT JOIN trip_plannings tp on tp.id = tc.fk_trip_id
                LEFT JOIN assets a on a.id = tp.fk_asset_id
                LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id
                LEFT JOIN asset_driver_mappings adm on a.id = adm.fk_asset_id and adm.connected
                LEFT JOIN users u on adm.fk_driver_id = u.id
                LEFT JOIN user_types ut on ut.id = u.fk_user_type_id and ut.type = 'driver'
                WHERE g.active AND g.fk_c_id = tc.fk_c_id and tc.active and tc.fk_c_id = $fk_c_id and
                tc.id = $consignment_id AND ${groupCondition} LIMIT 1`;

      models.sequelize.query(query, {bind: {
        fk_c_id: fk_c_id,
        consignment_id: id,

      }, type: models.sequelize.QueryTypes.SELECT})
        .spread(function (record) {
          if(!record){
            cb({
              code: 404,
              msg: "Record not found"
            })
          }else{
            cb(null, record)
          }
        }).catch(function (err) {
          cb({
            code: 500,
            msg: "Internal server error",
            err: err
          })
        })
    },
    files: ['record', function (results, cb) {
      let record = results.record;
      let file_data = [];

      async.each(record.files_meta, function (file, cb_file) {
        let params = {
          Bucket: file.bucket,
          Key: file.key
        };
        async.auto({
          temporary_url: function (cb_s3) {
            s3.getSignedUrl('getObject', params, function (err, url) {
              if(err){
                cb_s3(err)
              }else{
                cb_s3(null, url)
              }
            })
          },
          base64: function (cb_s3) {
            s3.getObject(params, function (err, data) {
              if(err){
                cb_s3(err)
              }else{
                cb_s3(null, data.Body.toString('base64'))
              }
            })
          },
          mime_type: function (cb_s3) {
            cb_s3(null, file.mimetype || 'image/png')
          }
        }, function (err, data) {
          data.filename = file.originalname;
          file_data.push(data);
          cb_file()
        })
      }, function () {
        cb(null, file_data)
      })
    }],
    actual: ['record', function (results, cb) {
      let record = results.record;
      record.distance = 0;
      record.duration = 0;
      record.consignor = _.omit(record.consignor, ['poi_id','fk_poi_id','loading','unloading','active','type','stop_dur','seq_no']);
      record.consignee = _.omit(record.consignee, ['poi_id','fk_poi_id','loading','unloading','active','type','stop_dur','seq_no']);
      record = _.omit(record, ['fk_c_id','active','createdAt','updatedAt', 'fk_trip_id','shared_with','files_meta']);

      processWaypoints(record, function (err, data) {
        record = _.omit(record, ['waypoints','trip_log']);
        let transit_event = _.find(data, {type: 'intransit'});

        if(transit_event){
          record.distance = transit_event.distance;
          record.duration =  transit_event.dwellDuration
        }
        cb(null, record)
      });
    }]
  }, function (err, results) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      let record = results.actual;
      record.files = results.files;
      res.json(record)
    }
  })
};

exports.getNewConsignmentNumber = function(req, res){
  let fk_c_id = req.headers.fk_c_id;
  if(!fk_c_id ){
    res.status(400).send("Company id ")
  }
  else{
    consignmentService.getConsignmentNumber(fk_c_id, function(err, result){
      if(err) res.status(err.code).send(err.msg);
      else res.send(result)
    })
  }
};

exports.getRealtimeStats = function (req, res) {
  let fk_c_id = req.headers.fk_c_id;
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);

  if(!fk_c_id && !groupIds){
    return res.status(400).send("Company id and group ids missing")
  }
  else{
    if(req.query.start_tis && req.query.end_tis && req.query.type === 'dispatched') {
      exports.getDispatchedConsignmentsStat(req, res)
    }
    else if(req.query.start_tis && req.query.end_tis && req.query.type === "incomplete"){
      exports.getIncompleteConsignmentRealtime(req, res)
    }
    else if(req.query.start_tis && req.query.end_tis && req.query.type === "delivered"){
      exports.getCompletedConsignmentRealtime(req, res)
    }
    else if(req.query.start_tis && req.query.end_tis){
      consignmentService.getConsignmentStatusCounts(req.query, req.headers, groupIds, function(err, result){
        if(err) res.status(err.code).send(err.msg);
        else res.send(result)
      })
    }
    else{
      consignmentService.getCurrentConsignmentStatusCounts(req, fk_c_id, groupIds, function(err, result){
        if(err) res.status(err.code).send(err.msg);
        else res.send(result)
      })
    }
  }
};

exports.getMaterialRealtimeStats = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const start_tis = req.query.start_tis || moment().startOf('day').unix();
  const end_tis = req.query.end_tis || moment().endOf('day').unix();

  if(!fk_c_id && !groupIds){
    return res.status(400).send("Company id and group ids missing")
  }
  else{
    let query = `select mtype.material_type, count::int
                    from (
                        select material_type, count(*) 
                        from consignments con
                        where con.fk_c_id = `+ fk_c_id + `
                            and extract(epoch from con."createdAt") between `+start_tis + ` and `+ end_tis +`
                        group by material_type
                    ) mtype
                    where mtype.material_type is not null
                    order by count desc`;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .then(function (data) {
        res.json({
          data: data
        })
        
      }).catch(function (err) {
        console.log("Error in consignment validation ",err);
        res.status(500).send("Internal Server Error")
      })
  }
};

exports.getIncompleteConsignmentRealtime = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis;

  if(!fk_c_id && !groupIds){
    return res.status(400).send("Company id and group ids missing")
  }
  else{
    let query = `SELECT * FROM(
                select cs.reason, count(*)::int
                from consignments con
                inner join consignment_statuses cs on con.id = cs.fk_consignment_id
                where fk_c_id = `+ fk_c_id + ` and cs.tis between `+start_tis + ` and `+ end_tis +` 
                    and cs.status = 'incomplete'
                group by cs.reason
            ) incomp_reasons
            where reason is not null
            order by count desc`;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .then(function (data) {
        res.json({
          data: data
        })
        
      }).catch(function (err) {
        console.log("Error in consignment validation ",err);
        res.status(500).send("Internal Server Error")
      })
  }
};

/**
 * This is a dashboard api used to get dispatched consignment count for a given time range.
 * @param {*} req 
 * @param {*} res 
 */
exports.getDispatchedConsignmentsStat = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis;

  let requiredKeys = ['start_tis', 'end_tis'];
  let dataValid = utils.validateMinPayload(requiredKeys, req.query);
  if(!dataValid){
    return res.status(400).send("Mandatory fields : "+requiredKeys)
  }

  let query = `select to_char(to_timestamp(tis), 'D Mon YYYY') AS days, count(*)::int 
                from consignments con 
                inner join consignment_statuses cs on con.id = cs.fk_consignment_id  
                where con.active  AND cs.status = 'picked'  
                    and tis between `+ start_tis + ` and `+ end_tis +` and con.fk_c_id = `+ fk_c_id + `
                group by days
                order by days asc`;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (data) {
    let output = data;
    let total_count = _.sumBy(data, function(o) { return o.count; });
    let past_days = _.remove(data, function(n) {
      return n.days === moment().format('YYYY-MM-DD');
    });

    console.log(past_days);
    res.json({
      total_count: total_count,
      data: output,
      avg: total_count/data.length
    })
  }).catch(function (err) {
    console.log(err);
    res.status(500).send("Internal Server Error")
  })
};

exports.getCompletedConsignmentRealtime = function(req, res){
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const start_tis = req.query.start_tis;
  const end_tis = req.query.end_tis;

  if(!fk_c_id && !groupIds){
    return res.status(400).send("Company id and group ids missing")
  }
  else{
    let query = `
        select to_char(to_timestamp(tis), 'D Mon YYYY') AS days, count(*)::int,
                            case when (con.consignee->>'tis')::int > cs.tis
                                    then true
                                else false
                            end as delayed
                        from consignments con
                        inner join consignment_statuses cs on con.id = cs.fk_consignment_id
                        where fk_c_id = `+ fk_c_id +` and cs.tis between `+ start_tis + ` and `+ end_tis +` 
                            and cs.status = 'delivered'
                        group by days, delayed
                    order by days asc`;

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .then(function (data) {
        res.json({
          data: data
        })
        
      }).catch(function (err) {
        console.log("Error in consignment validation ",err);
        res.status(500).send("Internal Server Error")
      })
  }
};

exports.getConsignmentTableConfig = function (req, res) {
// API to get user column preferences

  commonService.getUserTableConfig({
    table_name: CONSIGNMENT_TABLE_NAME,
    fk_c_id: req.headers.fk_c_id,
    fk_u_id: req.headers.fk_u_id
  }, function (err, result) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.json(result)
    }
  })
};

exports.updateConsignmentTableConfig = function (req, res) {

  commonService.updateUserTableConfig({
    table_name: CONSIGNMENT_TABLE_NAME,
    fk_u_id: req.headers.fk_u_id,
    data: req.body
  }, function (err, result) {
    if(err){
      res.status(err.code).send(err.msg)
    }else{
      res.json(result)
    }
  })
};

exports.getConsignmentNotifications = function (req, res) {

  const fk_consignment_id = req.query.id;
  const fk_c_id = req.headers.fk_c_id;
  let single_consignment_condition = "";
  let limit_condition = ` LIMIT ${Number(req.query.limit) || 25}`;
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let gid = req.query.gid;
  let groupCondition = gid ? ` g.gid = '${gid}' ` : ` g.id in ${groupIds} `;
  if(fk_consignment_id){
    single_consignment_condition = " AND con.id = $fk_consignment_id";
    limit_condition = "";
  }

  const query = `SELECT n.msg, n.fk_notif_id as type, n.level as lvl, n.tis, params as rparams
         FROM notifications n
         INNER JOIN consignments con on con.id = n.fk_asset_id AND con.fk_c_id = $fk_c_id ${single_consignment_condition}
         INNER JOIN consignment_group_mappings cgm ON cgm.fk_consignment_id = con.id and cgm.connected
         INNER JOIN groups g ON cgm.fk_group_id = g.id and cgm.connected
         WHERE fk_notif_id NOT IN (5,8) AND n.level = 'high' AND con.fk_c_id = $fk_c_id ${single_consignment_condition}
         AND ${groupCondition}
         ORDER BY n.tis DESC ${limit_condition}`;

  models.sequelize.query(query, { bind:{ fk_consignment_id, fk_c_id},type: models.sequelize.QueryTypes.SELECT})
    .then(function (notifications) {
      res.json({
        count: notifications.length,
        data: notifications
      })
    }).catch(function () {
      res.status(500).send()
    });

};

exports.getStatusCount = function (req, res) {

  let fk_c_id = req.headers.fk_c_id;
  let start_tis = req.query.start_tis;
  let end_tis = req.query.end_tis;
  let timeRangeCondition = "";
  let groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let gid = req.query.gid;
  let groupCondition = gid ? ` g.gid = '${gid}' ` : ` g.id in ${groupIds} `;

  let query = `select cstatus as status, COALESCE(delayed, false) delayed, count(*)::INT from (               
        select 
            DISTINCT con.id,
            case when cs.status is null
                    then con.status
                else cs.status
            end as cstatus,
            case when cs.status = 'picked' and cs.tis > (con.consignor->>'tis')::int
                    then true
                when cs.status = 'picked' and cs.tis <= (con.consignor->>'tis')::int
                    then false
            end as delayed
        from consignments con
        left join (
            select max(coalesce(tis, EXTRACT(EPOCH FROM cons."createdAt")::INT)),cons.id fk_consignment_id from consignments cons
            left join consignment_statuses cs on cs.fk_consignment_id = cons.id
            where cons.fk_c_id = $fk_c_id and cons.active
            group by cons.id
        ) cstat on cstat.fk_consignment_id = con.id
        left join consignment_statuses cs on cs.fk_consignment_id = cstat.fk_consignment_id and cs.tis = cstat.max
        INNER JOIN consignment_group_mappings cgm ON cgm.fk_consignment_id = con.id and cgm.connected
        INNER JOIN groups g ON cgm.fk_group_id = g.id and cgm.connected
        where con.fk_c_id = $fk_c_id and con.active and con.status != 'delivered' ${timeRangeCondition} AND ${groupCondition}
        ) cdata
        group by cdata.cstatus, COALESCE(delayed, false); `;

  if(start_tis && end_tis){
    timeRangeCondition = ` and ((cstat.max between $start_tis and $end_tis ) or
        ((extract(epoch from con."createdAt"))::int >= $start_tis  and
        (extract(epoch from con."createdAt"))::int < $end_tis ))`;
  }

  models.sequelize.query(query, {bind: {fk_c_id: fk_c_id, start_tis: start_tis, end_tis: end_tis}, type: models.sequelize.QueryTypes.SELECT})
    .then(function (record) {
      res.json(record)
    }).catch(function (err) {
      console.log("Error in consignment status count ",err);
      res.status(500).send("Internal Server Error")
    })
};