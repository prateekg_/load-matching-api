const locationService = require('../../services/location_service');

exports.searchPlace = function (req, res) {
  const companyId = req.headers.fk_c_id;
  const lat = req.query.lat;
  const lon = req.query.lon;
  if(lat && lon) {
    locationService.reverseGeocoding(companyId, req.query, function(err, result){
      if(err) return res.status(err.code).send(err.msg);
      else res.json(result)
    })
  }
  else{
    locationService.geocoding(companyId, req.query, function(err, result){
      if(err) return res.status(err.code).send(err.msg);
      else res.json(result)
    })
  }
};