const async = require('async');
const moment = require('moment');
const fs = require('fs');
const models = require('../../db/models/index');
const utils = require('../../helpers/util');
const file_base_path = __dirname + '/../../temp_reports/';
const APP_LOGGER = require('../../helpers/logger');
const LOGGER = APP_LOGGER.logger;
const auth = require(process.cwd() + '/authorization');
const tableConfigsHelper = require('../../helpers/table_config');
const xlsDownload = require('../../helpers/downloadHelper');

function validateAsset(assetId, groupIds, callback) {

  let query = "SELECT a.id FROM groups g " +
        " INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected " +
        " INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
        " WHERE g.active AND g.id in " + groupIds + " AND a.id = " + assetId +
        " GROUP BY a.id LIMIT 1";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (asset) {
    if (!asset) {
      return callback({
        message: 'Invalid asset id',
        code: 404
      })
    }
    return callback(null, asset.id)
  }).catch(function (err) {
    callback({
      error: err,
      message: 'Internal server error',
      code: 500
    })
  })
}

function getFuelRecord(fuelId, groupIds, callback) {
  let query = "SELECT afr.id, json_build_object('name', u.name, 'company_name', c2.cname, 'last_edit_tis', extract(epoch from afr.\"updatedAt\")::INT) as user," +
        " json_build_object('id', a.id, 'type', at.type, 'lic_plate_no', a.lic_plate_no, 'gids', ARRAY_AGG(g.gid), 'operator', c.cname) as asset," +
        " afr.total_cost, afr.volume, afr.price,afr.fill_tis,afr.brand,afr.type,afr.agency,afr.lname, afr.odometer " +
        " FROM asset_fuel_records afr " +
        " INNER JOIN assets a ON a.id = afr.fk_asset_id and a.active and afr.active" +
        " INNER JOIN users u on u.id = afr.last_edited_by_fk_u_id" +
        " INNER JOIN companies c2 on c2.id = u.fk_c_id" +
        " INNER JOIN companies c on c.id = a.fk_c_id and c.active" +
        " INNER JOIN asset_types at on at.id = a.fk_ast_type_id" +
        " INNER JOIN asset_group_mappings agm ON afr.fk_asset_id = agm.fk_asset_id AND agm.fk_asset_id = a.id and agm.connected" +
        " INNER JOIN groups g on agm.fk_group_id = g.id" +
        " WHERE g.active AND g.id in " + groupIds + " AND afr.id = " + fuelId +
        " GROUP BY a.id, afr.id, at.id, c.id, c2.id, u.id LIMIT 1";

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (fuel) {
    if (!fuel) {
      return callback({
        message: 'Invalid fuel record id',
        code: 404
      })
    }

    callback(null, fuel)
  }).catch(function (err) {
    callback({
      error: err,
      message: 'Internal server error',
      code: 500
    })
  })
}

exports.add = function (req, res) {
  const assetId = req.body.asset.id;
  const fk_u_id = req.headers.fk_u_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let fuel = req.body;

  if (fuel.fill_tis > moment().unix()) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Cannot add fuel records for the future',
      code: 400
    })
  }

  async.auto({
    validateAsset: validateAsset.bind(null, assetId, groupIds),
    addRecord: ['validateAsset', function (result, cb) {
      fuel.fk_asset_id = assetId;
      fuel.added_by_fk_u_id = fk_u_id;
      fuel.last_edited_by_fk_u_id = fk_u_id;

      models.asset_fuel_record.create(fuel).then(function () {
        cb(null, {
          code: 201,
          message: 'Fuel record successfully created'
        })
      }).catch(function (err) {
        cb({
          error: err,
          message: 'Internal server error',
          code: 500
        })
      })

    }]
  }, function (err, result) {
    if (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      if (err.code === 500) {
        err.error = null
      }
      return res.status(err.code).send(err)
    }
    res.status(result.addRecord.code).send(result.addRecord)
  })
};

exports.delete = function (req, res) {
  const fk_u_id = req.headers.fk_u_id;
  const fuelId = req.body.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);

  async.auto({
    validateFuelRecord: getFuelRecord.bind(null, fuelId, groupIds),
    deleteRecord: ['validateFuelRecord', function (result, cb) {
      models.asset_fuel_record.update({
        active: false,
        last_edited_by_fk_u_id: fk_u_id
      }, {
        where: {
          id: fuelId,
          active: true
        }
      }).spread(function (updated) {
        if (updated > 0) {
          return cb(null, {
            message: 'Fuel record successfully deleted'
          })
        }
        return cb({
          message: 'Invalid fuel record id',
          code: 404
        })
      }).catch(function (err) {
        cb({
          error: err,
          message: 'Internal server error',
          code: 500
        })
      })
    }]
  }, function (err, result) {
    if (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      if (err.code === 500) {
        err.error = null
      }
      return res.status(err.code).send(err)
    }
    res.json(result.deleteRecord)
  })
};

exports.update = function (req, res) {
  const assetId = req.body.asset.id;
  const fk_u_id = req.headers.fk_u_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const fuel = req.body;

  if (fuel.fill_tis > moment().unix()) {
    return res.status(400).send({
      error: 'Bad Request',
      message: 'Cannot add fuel records for the future',
      code: 400
    })
  }

  async.auto({
    validateAsset: validateAsset.bind(null, assetId, groupIds),
    validateFuelRecord: getFuelRecord.bind(null, fuel.id, groupIds),
    updateRecord: ['validateAsset', 'validateFuelRecord', function (result, cb) {
      fuel.fk_asset_id = assetId;
      fuel.last_edited_by_fk_u_id = fk_u_id;

      models.asset_fuel_record.update(fuel, {
        where: {
          id: fuel.id,
          active: true
        }
      }).spread(function (updated) {
        if (updated > 0) {
          return cb(null, {
            message: 'Fuel record successfully updated'
          })
        }

        cb({
          message: 'Invalid fuel record id',
          code: 404
        })

      }).catch(function (err) {
        cb({
          error: err,
          message: 'Internal server error',
          code: 500
        })
      })

    }]
  }, function (err, result) {
    if (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      if (err.code === 500) {
        err.error = null
      }
      return res.status(err.code).send(err)
    }
    res.json(result.updateRecord)
  })
};

exports.getDetail = function (req, res) {
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const fuelId = req.query.id;
  getFuelRecord(fuelId, groupIds, function (err, result) {
    if (err) {
      LOGGER.error(APP_LOGGER.formatMessage(req, err));
      if (err.code === 500) {
        err.error = null
      }
      return res.status(err.code).send(err)
    }

    res.json(result)
  })
};

exports.getTripFuelSummary = function (req, res) {
  const userFeatureList = JSON.parse(req.headers['feature-list']);
  const id = parseInt(req.query.id);
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;

  if(!auth.FEATURE_LIST.hasFuelSensor(userFeatureList)){
    return res.status(404).send("No access to fuel sensor")
  }else if(isNaN(id)){
    res.status(400).send('Invalid trip id')
  }else{

    let query = "SELECT tp.trip_log->'distance' as distance, tpf.graph_data graph, tpf.consumed, tpf.mileage, tpf.total_refuels, tpf.total_drops" +
            " FROM trip_plannings tp " +
            " INNER JOIN trip_fuels tpf on tpf.fk_trip_id = tp.id"+
            ` INNER JOIN asset_types at on tp.fk_asset_type_id = at.id
              LEFT JOIN(
                SELECT a.id, a.fk_ast_type_id, a.lic_plate_no, ast.type, tp.fk_c_id, c.cname, c.transporter_code 
                from assets a  
                INNER JOIN trip_plannings tp on a.id = tp.fk_asset_id
                LEFT JOIN asset_types ast on ast.id = a.fk_ast_type_id 
                INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected 
                INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected 
                LEFT JOIN companies c on c.id = a.fk_c_id 
                WHERE a.fk_c_id = `+ fk_c_id +` and tp.id = `+ id +` AND  `+ groupCondition +` AND g.active
              ) a on at.id = a.fk_ast_type_id`+
            " WHERE tp.id = "+ id +" LIMIT 1";

    models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
      .spread(function (trip) {
        if(!trip){
          res.json({})
        }else{
          trip.distance = Number(((trip.distance || 0) / 1000).toFixed(1));
          trip.consumed = Number(((trip.consumed || 0)).toFixed(2));
          trip.mileage = Number(((trip.mileage || 0)).toFixed(2));
          trip.graph.forEach(function (i) {
            i.value = Number(i.value.toFixed(2))
          });
          res.json(trip)
        }
      }).catch(function (err) {
        res.status(500).send(err)
      })
  }
};

exports.getTripFuelInstances = function (req, res) {
  const userFeatureList = JSON.parse(req.headers['feature-list']);
  const id = parseInt(req.query.id);
  const fk_c_id = req.headers.fk_c_id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '"+ gid +"'" : " g.id in "+ groupIds;
  const limit = req.query.limit && req.query.limit >= 0 ? req.query.limit : 10;
  const page = req.query.page && req.query.page >= 0 ? req.query.page : 0;
  const offset = limit * page;

  if(!auth.FEATURE_LIST.hasFuelSensor(userFeatureList)){
    return res.status(404).send("No access to fuel sensor")
  }else if(isNaN(id)){
    res.status(400).send('Invalid trip id')
  }else{

    async.auto({
      trip: function (callback) {
        let query = "SELECT tp.fk_asset_id, tp.status, tp.waypoints, tp.trip_log, tp.geo_trip_route FROM trip_plannings tp "+
                    " INNER JOIN assets a on a.id = tp.fk_asset_id" +
                    " INNER JOIN asset_group_mappings agm ON agm.fk_asset_id = a.id and agm.connected"+
                    " INNER JOIN groups g ON agm.fk_group_id = g.id and agm.connected"+
                    " LEFT JOIN companies c on c.id = a.fk_c_id" +
                    " WHERE g.active AND g.fk_c_id = "+ fk_c_id +" AND tp.id = "+ id +" AND " + groupCondition +" LIMIT 1";

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .spread(function (trip) {
            if(!trip){
              callback({
                code: 404,
                msg: 'Not found'
              })
            }else{
              let actStartTis = null;
              let actEndTis = null;

              if (trip.trip_log) {
                let tl = trip.trip_log;
                actStartTis = tl.departure;
                actEndTis = tl.arrival
              }
              callback(null, {
                fk_asset_id: trip.fk_asset_id,
                actStartTis: actStartTis,
                actEndTis: actEndTis
              })
            }
          }).catch(function (err) {
            callback({
              code: 500,
              msg: 'Internal server error 1',
              err: err
            })
          })
      },
      fuel: ['trip', function (result, callback) {
        let params = result.trip;
        if(!params.actStartTis && !params.actEndTis){
          callback(null, {
            count: 0,
            data: []
          })
        }else{
          let query = "SELECT tf.id, is_refuel, tis, lat, lon, lname, volume_before, volume_after," +
                        " count(tf.id) OVER()::INT AS total_count" +
                        " FROM fuel_sensor_instances tf " +
                        " WHERE tf.active AND tf.fk_asset_id = "+ params.fk_asset_id +" " +
                        " AND tf.tis BETWEEN "+ params.actStartTis +" AND " + params.actEndTis +
                        " ORDER BY tf.tis" +
                        " LIMIT "+ limit +" OFFSET "+ offset;

          models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
            .then(function (fuel_instances) {
              let total_count = 0;
              fuel_instances.forEach(function (i) {
                total_count = i.total_count;

                i.volume_before = Number(((i.volume_before || 0)).toFixed(2));
                i.volume_after = Number(((i.volume_after || 0)).toFixed(2));

                i.difference = Number((i.volume_after - i.volume_before).toFixed(2));

                i.total_count = undefined
              });
              res.json({
                count : total_count,
                data : fuel_instances
              });
            }).catch(function (err) {
              callback({
                code: 500,
                msg: 'Internal server error 1',
                err: err
              })
            })
        }
      }]
    }, function (err, result) {
      if(err){
        if(err.code === 404) res.json([]);
        else res.status(err.code).send(err.msg)
      }else{
        res.json(result.fuel);
      }
    })
  }
};

exports.getAll = function (req, res) {

  const limit = req.query.limit || 10;
  const page = req.query.page || 0;
  const offset = page * limit;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const searchKey = req.query.q ? req.query.q.toLowerCase() : '';
  const gid = req.query.gid;
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  const search_cols = req.query.search;
  let search_col_condition = "";
  const sort = req.query.sort;
  let sort_condition = " ORDER BY afr.fill_tis DESC ";
  const start_tis = req.query.start_tis || moment().tz(utils.timezone.default, false).subtract(12, 'months').unix();
  const end_tis = req.query.end_tis || moment().tz(utils.timezone.default, false).unix();
  const output_format = req.query.format;
  const fk_c_id = req.headers.fk_c_id;
  const fk_u_id = req.headers.fk_u_id;
  const default_columns = req.query.columns === 'all';
  const fuelStatus = 'all';

  let searchCondition = searchKey && searchKey.length > 0 ? " AND ( lower(a.lic_plate_no) like '%" + searchKey + "%' OR lower(c.cname) like '%" + searchKey + "%'" +
        " OR lower(afr.total_cost::TEXT) like '%" + searchKey + "%' OR lower(afr.volume::TEXT) like '%" + searchKey + "%' OR lower(afr.price::TEXT) like '%" + searchKey + "%'" +
        " OR lower(afr.type) like '%" + searchKey + "%' OR lower(afr.agency) like '%" + searchKey + "%' OR lower(afr.lname) like '%" + searchKey + "%')" : "";

  search_col_condition = utils.processTableColumnSearch(
    search_cols,
    [{
      db: 'a.lic_plate_no',
      client: 'asset.lic_plate_no'
    }, {
      db: 'c.cname',
      client: 'asset.operator'
    }, {
      db: 'afr.agency',
      client: 'agency'
    }, {
      db: 'afr.lname',
      client: 'lname'
    }, {
      db: 'afr.type',
      client: 'type'
    }]);

  sort_condition = utils.processTableColumnSort(
    sort,
    [{
      db: 'afr.total_cost',
      client: 'total_cost'
    }, {
      db: 'afr.volume',
      client: 'volume'
    }, {
      db: 'afr.price',
      client: 'price'
    }, {
      db: 'afr.fill_tis',
      client: 'fill_tis'
    }],
    sort_condition
  );

  let query = "SELECT count(*) OVER() AS total_count, afr.id, " +
        " json_build_object('name', u.name, 'company_name', c2.cname, 'last_edit_tis', extract(epoch from afr.\"updatedAt\")::INT) as user," +
        " json_build_object('id', a.id, 'type', at.type, 'lic_plate_no', a.lic_plate_no, 'gids', ARRAY_AGG(g.gid), 'operator', json_build_object('name',c.cname)) as asset," +
        " afr.total_cost, afr.volume, afr.price,afr.fill_tis,afr.brand,afr.type,afr.agency,afr.lname, afr.odometer " +
        " FROM asset_fuel_records afr " +
        " INNER JOIN assets a ON a.id = afr.fk_asset_id and a.active and afr.active" +
        " INNER JOIN users u on u.id = afr.last_edited_by_fk_u_id" +
        " INNER JOIN companies c2 on c2.id = u.fk_c_id" +
        " INNER JOIN companies c on c.id = a.fk_c_id and c.active" +
        " INNER JOIN asset_types at on at.id = a.fk_ast_type_id" +
        " INNER JOIN asset_group_mappings agm ON afr.fk_asset_id = agm.fk_asset_id AND agm.fk_asset_id = a.id and agm.connected" +
        " INNER JOIN groups g on agm.fk_group_id = g.id" +
        " WHERE g.active AND " + groupCondition + searchCondition + search_col_condition +
        " AND afr.fill_tis BETWEEN " + start_tis + " AND " + end_tis +
        " GROUP BY a.id, afr.id, at.id, c.id, c2.id, u.id" +
        sort_condition;

  if (output_format !== 'xls') {
    query += " LIMIT " + limit + " OFFSET " + offset;
  }

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).then(function (fuel) {
    let count = 0;
    async.each(fuel, function (item, cb) {
      count = Number(item.total_count);
      delete item.total_count;
      cb()
    }, function () {
      if (output_format === 'xls') {
        const file_name = "FUEL-LIST-" + fk_c_id + "-" + moment().unix() + ".xls";
        const file_path = file_base_path + file_name;

        tableConfigsHelper.getTableConfig('fuel-list', fk_u_id, fuelStatus, default_columns, function (err, table_config) {
          xlsDownload.convertTableV1ToXLS(fuelStatus, fuel, file_base_path, file_name, table_config, function (err) {
            if (err) {
              res.status(err.code).send(err.msg)
            } else {
              res.setHeader("Access-Control-Expose-Headers", "Content-Disposition, Filename");
              res.setHeader("Content-Disposition", "attachment; filename=" + file_name);
              res.setHeader("Content-Type", "application/vnd.ms-excel");
              res.setHeader("Filename", file_name);

              res.download(file_path, file_name, function (err) {
                if (!err) {
                  fs.unlink(file_path)
                }
              })
            }
          })

        })
      } else {
        res.json({
          total_count: count,
          data: fuel
        })
      }

    })

  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send({
      error: null,
      message: 'Internal server error' + err,
      code: 500
    })
  })

};

exports.getRealtimeAnalytics = function (req, res) {
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const gid = req.query.gid;
  const start_tis = req.query.start_tis || moment().tz(utils.timezone.default, false).startOf('day').unix();
  const end_tis = req.query.end_tis || moment().tz(utils.timezone.default, false).endOf('day').unix();
  const groupCondition = gid ? " g.gid = '" + gid + "'" : " g.id in " + groupIds;
  let query = " select coalesce(sum(total_cost), 0) total_cost, coalesce(count(brand)::int, 0) total_stops, coalesce(sum(volume), 0) volume" +
        " from asset_fuel_records where fk_asset_id in " +
        " ( select a.id from groups g " +
        " INNER JOIN asset_group_mappings agm ON agm.fk_group_id = g.id and agm.connected " +
        " INNER JOIN assets a ON a.id = agm.fk_asset_id and a.active " +
        " WHERE " + groupCondition +
        " group by a.id)" +
        " AND fill_tis between " + start_tis + " AND " + end_tis;

  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT}).spread(function (fuel) {
    res.json(fuel);
  }).catch(function (err) {
    LOGGER.error(APP_LOGGER.formatMessage(req, err));
    res.status(500).send({
      error: null,
      message: 'Internal server error',
      code: 500
    })
  })
};

exports.getFuelInstance = function (req, res, apiType) {
  const userFeatureList = JSON.parse(req.headers['feature-list']);
  const assetId = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 0;
  const offset = limit * page;
  let start_tis = parseInt(req.query.start_tis) || null ;
  let end_tis = parseInt(req.query.end_tis) || null;
  if (start_tis ===null){
    start_tis = moment.unix(moment().unix()).tz(utils.timezone.default, false).startOf('day').unix();
  }
  if (end_tis === null) {
    end_tis = moment.unix(start_tis).tz(utils.timezone.default, false).endOf('day').unix();
  }


  if (!auth.FEATURE_LIST.hasFuelSensor(userFeatureList)) {
    return res.status(404).send("No access to fuel sensor")
  } else if (isNaN(assetId)) {
    res.status(400).send('Invalid asset id')
  } else {
    async.auto({
      validateAsset: validateAsset.bind(null, assetId, groupIds),
      data: ['validateAsset', function (result, cb) {
        let query = 'SELECT nsi.id, units_aft as volume_after, units_bef as volume_before, lat, lon, tis, lname, increasing as is_refuel, count(nsi.id) OVER()::INT AS total_count FROM nubot_sensor_instances nsi' +
                    ' INNER JOIN sensors s ON nsi.fk_sensor_id = s.id' +
                    ' INNER JOIN nubot_sensor_types nst ON nst.id = s.fk_sensor_type' +
                    ' WHERE nsi.fk_asset_id = ' + assetId +
                    ((start_tis && end_tis) ? ' AND nsi.tis BETWEEN ' + start_tis + ' AND ' + end_tis : '') +
                    " AND nst.sensor_type = 'Fuel Sensor' " +
                    ' ORDER BY tis DESC ';

        if (apiType === 'list') {
          query += " LIMIT " + limit + " OFFSET " + offset
        }

        models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
          .then(function (result) {
            let total_count = 0;
            result.forEach(function (i) {
              total_count = i.total_count;

              i.volume_before = Number(Number((i.volume_before) || 0).toFixed(2));
              i.volume_after = Number(Number((i.volume_after) || 0).toFixed(2));

              i.difference = Number((i.volume_after - i.volume_before).toFixed(2));
              i.total_count = undefined
            });

            if (apiType === 'list') {
              res.json({
                count: total_count,
                data: result
              });
            } else {
              res.json({
                count: total_count,
                fuel: result
              });
            }

          }).catch(function (err) {
            console.log(err);
            cb({
              error: err,
              message: 'Internal server error',
              code: 500
            })
          })

      }]
    }, function (err, result) {
      if (err) {
        if (err.code === 500) {
          err.error = null
        }
        return res.status(err.code).send(err)
      }
      result.validateAsset = undefined;
      res.status(200).send(result)
    })
  }
};

exports.getFuelSummary = function (req, res) {
  const userFeatureList = JSON.parse(req.headers['feature-list']);
  const assetId = req.query.id;
  const groupIds = utils.convertGroupIdsToTuple(req.headers.group_ids);
  let start_tis = req.query.start_tis;
  let end_tis = moment.unix(start_tis).tz(utils.timezone.default, false).endOf('day').unix();

  if (!auth.FEATURE_LIST.hasFuelSensor(userFeatureList)) {
    return res.status(404).send("No access to fuel sensor")
  } else if (isNaN(assetId)) {
    res.status(400).send('Invalid asset id')
  } else {
    async.auto({
      validateAsset: validateAsset.bind(null, assetId, groupIds),
      counts: ['validateAsset', function (result, cb) {
        let query = 'SELECT SUM(CAST(increasing as Integer)) refuel, SUM(1 - CAST(increasing AS Integer)) fuel_drop FROM nubot_sensor_instances nsi' +
                    ' INNER JOIN sensors s ON nsi.fk_sensor_id = s.id' +
                    ' INNER JOIN nubot_sensor_types nst ON nst.id = s.fk_sensor_type' +
                    ' WHERE nsi.fk_asset_id = ' + assetId +
                    " AND nst.sensor_type = 'Fuel Sensor' " +
                    ((start_tis && end_tis) ? ' AND nsi.tis BETWEEN ' + start_tis + ' AND ' + end_tis : '');

        models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
          .spread(function (count) {
            cb(null, count)

          }).catch(function (err) {
            console.log(err);
            cb({
              error: err,
              message: 'Internal server error',
              code: 500
            })
          })

      }],
      distance:  function (cb) {
        let query = 'SELECT tot_dist as distance FROM assets_reports_data ' +
                    ' WHERE fk_asset_id = ' + assetId +
                    ((start_tis && end_tis) ? ' AND start_tis >= ' + start_tis +  ' AND end_tis <= ' + end_tis : ' ORDER BY id LIMIT 1');

        models.sequelize.query(query, { type: models.sequelize.QueryTypes.SELECT })
          .spread(function (result) {
            cb(null, result)

          }).catch(function (err) {
            console.log(err);
            cb({
              error: err,
              message: 'Internal server error',
              code: 500
            })
          })

      }
    }, function (err, result) {
      if (err) {
        if (err.code === 500) {
          err.error = null
        }
        return res.status(err.code).send(err)
      }
      result.validateAsset = undefined;
      let output = {
        consumed:0,
        distance: result.distance ? result.distance.distance || 0 : 0,
        mileage: 0,
        total_drops: result.counts ? Number(result.counts.fuel_drop) || 0 : 0,
        total_refuels: result.counts ? Number(result.counts.refuel) || 0 : 0,
        has_fuel_sensor: result.counts ? result.counts.connected : false
      };
      res.status(200).send(output)
    })
  }
};