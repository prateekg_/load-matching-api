/**
 * Created by numadic on 13/06/17.
 * @author: Haston Silva
 */

exports.FEATURE_LIST = {
  hasTrips : function (featureList) {
    return featureList.indexOf('TripsManual') >= 0
  },
  hasFuel: function (featureList) {
    return featureList.indexOf('RecordsFuel') >= 0
  },
  hasMaintenance: function (featureList) {
    return featureList.indexOf('Maintenance') >=0
  },
  hasCompanyPlaces: function (featureList) {
    return featureList.indexOf('PlacesCompany') >= 0
  },
  hasGeneralPlaces: function (featureList) {
    return featureList.indexOf('PlacesGeneral') >= 0
  },
  hasDriver: function (featureList) {
    return featureList.indexOf('RecordsDriverManagement') >= 0
  },
  hasDoorSensor: function (featureList) {
    return featureList.indexOf('SensorDoor') >= 0
  },
  hasFuelSensor: function (featureList) {
    return featureList.indexOf('SensorFuel') >= 0
  },
  hasAxleLoadSensor: function (featureList) {
    return featureList.indexOf('SensorAxleload') >= 0
  },
  hasImmobilizerSensor: function (featureList) {
    return featureList.indexOf('Immobiliser') >= 0
  },
  hasTemperatureSensor: function (featureList) {
    return featureList.indexOf('ColdChain') >= 0
  },
  hasConsignment : function (featureList) {
    return featureList.indexOf('Consignment') >= 0
  }
};