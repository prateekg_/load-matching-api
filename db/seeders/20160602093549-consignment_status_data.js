'use strict';

module.exports = {
  up: function (queryInterface) {
    let d = new Date();

    return queryInterface.bulkInsert('consignment_status_types',
      [
        { status: 'IN TRANSIT', createdAt: d, updatedAt: d },
        { status: 'AWAITING POD', createdAt: d, updatedAt: d },
        { status: 'DELIVERED', createdAt: d, updatedAt: d },
        { status: 'AWAITING PICKUP', createdAt: d, updatedAt: d },
        { status: 'DETAINED', createdAt: d, updatedAt: d },
        { status: 'CUSTOMER PICKUP', createdAt: d, updatedAt: d },
        { status: 'POD RECD', createdAt: d, updatedAt: d },
        { status: 'NOT SHIPPED', createdAt: d, updatedAt: d }
      ], {})
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
