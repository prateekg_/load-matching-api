'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trip_failure_condition', {
    condition: DataTypes.TEXT,
    description: DataTypes.TEXT
  });
};