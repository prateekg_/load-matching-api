'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('poi_detention_mapping', {
    fk_primary_poi_id: DataTypes.INTEGER,
    fk_detention_poi_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER,
    detention_config: DataTypes.JSON,
    mapped: DataTypes.FLOAT
  });
};