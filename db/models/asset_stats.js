'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('asset_stats', {
    fk_asset_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER,
    tot_dist: DataTypes.DOUBLE,
    tot_idle: DataTypes.DOUBLE,
    tot_eng: DataTypes.DOUBLE,
    tot_stat: DataTypes.DOUBLE,
    date: DataTypes.TEXT,
    tis: DataTypes.INTEGER,
    avg_spd: DataTypes.INTEGER,
    tot_vio: DataTypes.INTEGER,
    e_tis: DataTypes.INTEGER,
    tot_osf: DataTypes.INTEGER,
    tot_hbk: DataTypes.INTEGER,
    points_distance: DataTypes.DOUBLE,
    distances: DataTypes.JSON
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};