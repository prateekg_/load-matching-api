'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('smart_zone_type', {
    name: DataTypes.TEXT,
    desc: DataTypes.TEXT
  });
};