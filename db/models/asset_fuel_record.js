'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('asset_fuel_record', {
    fk_asset_id: DataTypes.INTEGER,
    fill_tis: DataTypes.INTEGER,
    odometer: DataTypes.INTEGER,
    type: DataTypes.TEXT,
    brand: DataTypes.TEXT,
    volume: DataTypes.DOUBLE,
    price: DataTypes.DOUBLE,
    total_cost: DataTypes.DOUBLE,
    agency: DataTypes.TEXT,
    lname: DataTypes.TEXT,
    added_by_fk_u_id: DataTypes.INTEGER,
    last_edited_by_fk_u_id: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN
  });
};