'use strict';
module.exports = function(sequelize, DataTypes) {
  let company_type = sequelize.define('company_type', {
    type: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        company_type.hasMany(models.company, {
          foreignKey: 'fk_c_type_id',
          constraints: false
        }); 
      }
    }
  });
  return company_type;
};