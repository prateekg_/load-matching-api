'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tag_type', {
    type: DataTypes.STRING
  });
};