'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('smart_notifications', {
    start_tis: DataTypes.INTEGER,
    end_tis: DataTypes.INTEGER,
    fk_smart_notification_type: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    asset_type: DataTypes.STRING,
    message: DataTypes.STRING,
    fk_group_id: DataTypes.INTEGER,
    fk_poi_id: DataTypes.INTEGER,
    params: DataTypes.JSON,
    level: DataTypes.STRING
  });
};