'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('asset_tyre_mapping', {
    fk_asset_id: DataTypes.INTEGER,
    fk_tyre_id: DataTypes.INTEGER,
    tis: DataTypes.BIGINT,
    connected: {
      type :DataTypes.BOOLEAN,
      defaultValue : true
    }
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};