'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trip_plan_template', {
    fk_c_id: DataTypes.INTEGER,
    custom_fields: DataTypes.JSON,
    active: DataTypes.BOOLEAN
  });
};