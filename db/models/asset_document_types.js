'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('asset_document_types', {
    code: DataTypes.TEXT,
    name: DataTypes.TEXT
  });
};