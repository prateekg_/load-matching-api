'use strict';
module.exports = function (sequelize, DataTypes) {
  let admin_api = sequelize.define('admin_api', {
    feature_name: DataTypes.STRING,
    path: DataTypes.STRING,
    active: DataTypes.BOOLEAN,
    fk_url_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function (models) {
        // associations can be defined here
        admin_api.belongsTo(models.admin_url, {
          foreignKey: 'fk_url_id',
          constraints: false
        })
      }
    }
  });
  return admin_api
};
