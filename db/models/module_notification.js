'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('module_notification', {
    fk_module_id: DataTypes.INTEGER,
    msg: DataTypes.TEXT,
    tis: DataTypes.INTEGER
  });
};