'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('location_personnel', {
    fk_asset_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER,
    tis: DataTypes.INTEGER,
    lat: DataTypes.FLOAT,
    lon: DataTypes.FLOAT,
    spd: DataTypes.INTEGER,
    net_stat: DataTypes.INTEGER,
    bt_stat: DataTypes.INTEGER,
    accuracy: DataTypes.FLOAT,
    aid: DataTypes.TEXT,
    lname: DataTypes.TEXT
  });
};