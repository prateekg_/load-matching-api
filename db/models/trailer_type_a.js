'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trailer_type_a', {
    aid: DataTypes.TEXT,
    temp: DataTypes.DOUBLE,
    battery: DataTypes.DOUBLE,
    timestamp: DataTypes.INTEGER,
    fk_con_aid: DataTypes.TEXT,
    zones : DataTypes.JSON
  });
};