'use strict';
module.exports = function(sequelize, DataTypes) {
  let user_type = sequelize.define('user_type', {
    type: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        user_type.hasOne(models.user, {
          foreignKey: 'fk_user_type_id'
        })
      }
    }
  });
  return user_type;
};