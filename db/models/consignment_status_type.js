'use strict';
module.exports = function (sequelize, DataTypes) {
  let consignment_status_type = sequelize.define('consignment_status_type', {
    status: DataTypes.STRING
  }, {
    classMethods: {
      associate: function (models) {
        // associations can be defined here
        // consignment_status_type.belongsTo(models.consignment_history, {foreignKey: 'fk_c_s_type'});
        consignment_status_type.hasMany(models.consignment, {foreignKey: 'fk_c_s_type'});
      }
    }
  });
  return consignment_status_type;
};