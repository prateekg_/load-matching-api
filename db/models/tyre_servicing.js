'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tyre_servicing', {
    fk_c_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    scheduled_tis: DataTypes.INTEGER,
    task: DataTypes.TEXT,
    tyres: DataTypes.JSON,
    location: DataTypes.TEXT,
    est_cost: DataTypes.INTEGER,
    cost: DataTypes.INTEGER,
    fk_u_id: DataTypes.INTEGER,
    odometer: DataTypes.INTEGER,
    note: DataTypes.TEXT,
    is_scheduled: {
      type :DataTypes.BOOLEAN,
      defaultValue : true
    },
    completed_tis: DataTypes.BIGINT,
    active: {
      type :DataTypes.BOOLEAN,
      defaultValue : true
    }
  });
};