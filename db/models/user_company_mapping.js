'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_company_mapping', {
    fk_u_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER,
    connected: DataTypes.BOOLEAN
  });
};