'use strict';
module.exports = function(sequelize, DataTypes) {
  let entity_type = sequelize.define('entity_type', {
    type: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        entity_type.hasMany(models.entity,{
          foreignKey: 'id',
          constraints: false
        });
      }
    }
  });
  return entity_type;
};