'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('report_type', {
    name: DataTypes.TEXT,
    frequency: DataTypes.ARRAY(DataTypes.STRING),
    template: DataTypes.JSON,
    summary: DataTypes.TEXT,
    fk_c_id: DataTypes.INTEGER,
    fk_feature_id: DataTypes.INTEGER,
    config: DataTypes.JSON
  });
};