"use strict";
module.exports = function(sequelize, DataTypes) {
  let poi = sequelize.define(
    "poi",
    {
      fk_c_id: DataTypes.INTEGER,
      geo_loc: DataTypes.GEOGRAPHY,
      priv_typ: DataTypes.BOOLEAN,
      name: DataTypes.STRING,
      typ: DataTypes.INTEGER,
      details: DataTypes.TEXT,
      addr: DataTypes.TEXT,
      start_tim: DataTypes.TEXT,
      end_tim: DataTypes.TEXT,
      entry_fee: DataTypes.FLOAT,
      dwell_tim_limit: DataTypes.TEXT,
      capacity: DataTypes.INTEGER,
      active: DataTypes.BOOLEAN,
      sad: DataTypes.BOOLEAN,
      open_on: DataTypes.JSON,
      is24hours: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      verified: DataTypes.BOOLEAN,
      company_fields: DataTypes.JSON,
      lat: DataTypes.FLOAT,
      lon: DataTypes.FLOAT,
      fk_state_id: DataTypes.INTEGER,
      state: DataTypes.TEXT,
      fees: DataTypes.JSON,
      external_id: DataTypes.TEXT,
      nhtis_id: DataTypes.INTEGER,
      company_lat: DataTypes.FLOAT,
      company_lon: DataTypes.FLOAT,
      plaza_code: DataTypes.STRING
    },

    {
      classMethods: {
        associate: function(models) {
          // associations can be defined here
          poi.hasMany(models.route_templates_poi_mapping, {
            foreignKey: "fk_poi_id",
            constraints: false
          });
        }
      }
    }
  );
  return poi;
};
