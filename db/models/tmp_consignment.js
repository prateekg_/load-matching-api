'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('consignment', {
    fk_c_id: DataTypes.INTEGER,
    fk_trip_id: DataTypes.INTEGER,
    status: DataTypes.TEXT,
    invoice_no: DataTypes.TEXT,
    consignor: DataTypes.JSON,
    consignee: DataTypes.JSON,
    material_type: DataTypes.TEXT,
    product_code: DataTypes.TEXT,
    total_packages: DataTypes.INTEGER,
    weight: DataTypes.FLOAT,
    weight_unit: DataTypes.ENUM('MT','KG'),
    shared_with: DataTypes.JSON,
    active: DataTypes.BOOLEAN,
    consignment_no: DataTypes.INTEGER
  });
};