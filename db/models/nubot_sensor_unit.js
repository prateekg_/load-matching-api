'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nubot_sensor_unit', {
    unit: DataTypes.TEXT,
    description: DataTypes.TEXT
  });
};