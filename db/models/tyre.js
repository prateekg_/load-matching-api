'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tyre', {
    fk_c_id: DataTypes.INTEGER,
    tyre_id: DataTypes.TEXT,
    manufacturer: DataTypes.TEXT,
    model: DataTypes.TEXT,
    brand: DataTypes.TEXT,
    type: DataTypes.TEXT,
    thread: DataTypes.TEXT,
    size: DataTypes.TEXT,
    psi: DataTypes.INTEGER,
    cost: DataTypes.INTEGER,
    is_new: {
      type :DataTypes.BOOLEAN,
      defaultValue : true
    },
    used_km: DataTypes.INTEGER,
    active: {
      type :DataTypes.BOOLEAN,
      defaultValue : true
    }
  });
};