'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_settings_report', {
    fk_report_schedule_id: DataTypes.BIGINT,
    fk_group_id: DataTypes.BIGINT,
    fk_user_id: DataTypes.BIGINT,
    subscribed: DataTypes.BOOLEAN,
    active: DataTypes.BOOLEAN,
    tis: DataTypes.BIGINT
  });
};