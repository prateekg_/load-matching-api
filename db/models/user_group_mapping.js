'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_group_mapping', {
    fk_group_id: DataTypes.INTEGER,
    fk_u_id: DataTypes.INTEGER,
    connected: DataTypes.BOOLEAN,
    tis: DataTypes.INTEGER
  });
};