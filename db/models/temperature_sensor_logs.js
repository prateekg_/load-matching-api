'use strict';
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('temperature_sensor_log', {
    fk_u_id: DataTypes.BIGINT,
    fk_asset_id: DataTypes.BIGINT,
    new_temperature_values: DataTypes.JSON,
    old_temperature_values: DataTypes.JSON,
    tis: DataTypes.INTEGER,
    note: DataTypes.TEXT
  })
};
