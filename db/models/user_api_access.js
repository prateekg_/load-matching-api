'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_api_access', {
    fk_user_id: DataTypes.INTEGER,
    fk_module_feature_id: DataTypes.INTEGER,
    type: DataTypes.STRING,
    checked: DataTypes.BOOLEAN
  });
};