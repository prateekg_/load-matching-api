'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trip_user_mapping_external', {
    active: DataTypes.BOOLEAN,
    fk_trip_id: DataTypes.INTEGER,
    fk_u_id: DataTypes.INTEGER,
    fk_user_external_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER
  });
};