'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mobile_build', {
    url: DataTypes.TEXT,
    version: DataTypes.FLOAT,
    type: DataTypes.INTEGER,
    force: DataTypes.BOOLEAN
  });
};