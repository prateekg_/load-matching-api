'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('country', {
    name: DataTypes.TEXT,
    sortname: DataTypes.TEXT,
    code: DataTypes.TEXT
  });
};