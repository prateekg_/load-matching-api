'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('group_access', {
    fk_g_id: DataTypes.INTEGER,
    settings: DataTypes.JSON
  });
};