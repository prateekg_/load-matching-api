'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trip_package_status', {
    fk_trip_id: DataTypes.INTEGER,
    fk_by_u_id: DataTypes.INTEGER,
    tis: DataTypes.BIGINT,
    status: DataTypes.TEXT,
    note: DataTypes.TEXT,
    reasons: DataTypes.TEXT,
    files_meta: DataTypes.JSON,
    active: DataTypes.BOOLEAN
  });
};