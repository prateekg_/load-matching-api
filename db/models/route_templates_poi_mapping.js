'use strict';
module.exports = function(sequelize, DataTypes) {
  let route_templates_poi_mapping = sequelize.define('route_templates_poi_mapping', {
    fk_poi_id: DataTypes.INTEGER,
    fk_route_template_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        route_templates_poi_mapping.belongsTo(models.route_template, {
          foreignKey: 'fk_route_template_id',
          constraints: false
        });
        route_templates_poi_mapping.belongsTo(models.poi, {
          foreignKey: 'fk_poi_id',
          constraints: false
        });
      }
    }
  });
  return route_templates_poi_mapping;
};