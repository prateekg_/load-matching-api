'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('poi_group_mapping', {
    fk_group_id: DataTypes.INTEGER,
    fk_poi_id: DataTypes.INTEGER,
    connected: DataTypes.BOOLEAN,
    tis: DataTypes.INTEGER
  });
};