'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('asset_sharing', {
    fk_asset_id: DataTypes.INTEGER,
    fk_shared_c_id: DataTypes.INTEGER,
    shared: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  });
};