'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('company_template', {
    fk_c_id: DataTypes.INTEGER,
    trip_summary: DataTypes.JSON
  });
};