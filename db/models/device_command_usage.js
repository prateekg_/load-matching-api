'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('device_command_usage', {
    fk_user_id: DataTypes.INTEGER,
    fk_user_type_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    fk_entity_id: DataTypes.INTEGER,
    fk_device_command_id: DataTypes.INTEGER,
    fk_sensor_id: DataTypes.INTEGER,
    status: DataTypes.TEXT,
    user_notes: DataTypes.TEXT,
    command: DataTypes.TEXT
  });
};