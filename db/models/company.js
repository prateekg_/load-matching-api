'use strict';
module.exports = function(sequelize, DataTypes) {
  let company = sequelize.define('company', {
    cid: DataTypes.STRING,
    email: DataTypes.STRING,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue : true
    },
    cname: DataTypes.TEXT,
    country: DataTypes.STRING,
    street: DataTypes.TEXT,
    cont_no: DataTypes.TEXT,
    c_token: DataTypes.TEXT,
    c_secret: DataTypes.TEXT,
    ow_name: DataTypes.TEXT,
    contact_type: DataTypes.TEXT,
    state: DataTypes.TEXT,
    city: DataTypes.TEXT,
    zip: DataTypes.TEXT,
    fk_c_type_id: DataTypes.INTEGER,
    feature_list: DataTypes.ARRAY(DataTypes.STRING),
    transporter_code: DataTypes.STRING,
    details: DataTypes.JSON
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        company.belongsTo(models.company_type,{
          foreignKey: 'fk_c_type_id',
          constraints: false
        });
        company.hasMany(models.user, {
          foreignKey: 'fk_c_id',
          constraints: false
        }); 
        company.hasMany(models.asset, {
          foreignKey: 'fk_c_id',
          constraints: false
        }); 
        company.hasOne(models.company_details,{
          foreignKey: 'fk_c_id',
          constraints: false
        }); 
      }
    }
  });
  return company;
};
