'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('health_module', {
    name: DataTypes.TEXT
  });
};