'use strict';
module.exports = function(sequelize, DataTypes) {
  let route_template = sequelize.define('route_template', {
    route_id: DataTypes.STRING,
    fk_c_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    origin: DataTypes.STRING,
    destination: DataTypes.STRING,
    tot_waypoints: DataTypes.INTEGER,
    est_dist: DataTypes.FLOAT,
    est_dur: DataTypes.FLOAT,
    waypoints: DataTypes.JSON,
    geo_route: DataTypes.JSON,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    is_advanced: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here

        route_template.hasMany(models.tag,{
          foreignKey: 't_id',
          constraints: false
        });

        route_template.hasMany(models.route_templates_poi_mapping, {
          foreignKey: 'fk_route_template_id',
          constraints: false
        });
      }
    }
  });
  return route_template;
};