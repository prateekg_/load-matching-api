'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trip_fuel', {
    fk_trip_id: DataTypes.BIGINT,
    consumed: DataTypes.FLOAT,
    mileage: DataTypes.FLOAT,
    total_refuels: DataTypes.INTEGER,
    total_drops: DataTypes.INTEGER,
    graph_data: DataTypes.JSON,
    trip_log_fuel: DataTypes.JSON,
    active: DataTypes.BOOLEAN
  });
};