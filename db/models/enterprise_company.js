'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('enterprise_company', {
    name: DataTypes.STRING,
    address: DataTypes.TEXT,
    status: DataTypes.TEXT,
    gst_no: DataTypes.TEXT,
    contact_details: DataTypes.JSON
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};