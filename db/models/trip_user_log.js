'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trip_user_log', {
    fk_trip_id: DataTypes.INTEGER,
    fk_u_id: DataTypes.INTEGER,
    new_status: DataTypes.TEXT,
    old_status: DataTypes.TEXT,
    note: DataTypes.TEXT,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    platform: DataTypes.TEXT
  });
};