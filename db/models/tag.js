'use strict';
module.exports = function(sequelize, DataTypes) {
  let tag = sequelize.define('tag', {
    t_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER,
    type: DataTypes.INTEGER,
    tag: DataTypes.STRING,
    hits: DataTypes.INTEGER,
    fk_tag_type_id : DataTypes.INTEGER,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        tag.belongsTo(models.route_template,{
          foreignKey: 't_id',
          constraints: false
        });

        tag.belongsTo(models.trip_planning,{
          foreignKey: 't_id',
          constraints: false
        });
      }
    }
  });
  return tag;
};