'use strict';
module.exports = function(sequelize, DataTypes) {
  let asset_mapping = sequelize.define('asset_mapping', {
    fk_asset_id: DataTypes.INTEGER,
    fk_con_asset_id: DataTypes.INTEGER,
    connected: {
      type :DataTypes.BOOLEAN,
      defaultValue : true
    }
  },  {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        asset_mapping.belongsTo(models.asset,{
          foreignKey: 'fk_asset_id',
          constraints: false
        });
        asset_mapping.belongsTo(models.asset,{
          foreignKey: 'fk_con_asset_id',
          constraints: false
        });
      }
    }
  });
  return asset_mapping;
};
