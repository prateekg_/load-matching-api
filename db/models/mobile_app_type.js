'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mobile_app_type', {
    type: DataTypes.TEXT
  });
};