'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_external', {
    email: DataTypes.TEXT,
    details: DataTypes.JSON,
    active: DataTypes.BOOLEAN
  });
};