'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('group_permission', {
    fk_group_id: DataTypes.INTEGER,
    fk_feature_id: DataTypes.INTEGER,
    fk_access_type_id: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN
  });
};