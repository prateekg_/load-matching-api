'use strict';
module.exports = (sequelize, DataTypes) => {
  const user_kyc = sequelize.define('user_kyc', {
    mobile_no: DataTypes.STRING,
    user_details: DataTypes.JSON,
    address_proof: DataTypes.JSON,
    id_proof: DataTypes.JSON,
    selfie_proof: DataTypes.JSON,
    verified: DataTypes.BOOLEAN
  }, {});
  user_kyc.associate = function(models) {
    // associations can be defined here
  };
  return user_kyc;
};