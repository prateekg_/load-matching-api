//Model to handle activites associated with the admin table
'use strict';
module.exports = function(sequelize, DataTypes) {
  let admin = sequelize.define('admin', {
    name: DataTypes.STRING,
    p_word: DataTypes.TEXT,
    cont_no: DataTypes.TEXT,
    email: DataTypes.TEXT,
    reset_password_token: DataTypes.TEXT,
    reset_password_expires: DataTypes.DATE,
    fk_admin_type : DataTypes.INTEGER,
    active: {
      type : DataTypes.BOOLEAN,
      defaultValue : true
    }
  }, {
    classMethods: {
      associate: function(models) {
        // Associations can be defined here
        //Defining association with the authorization type(One-To-One Association)
        admin.belongsTo(models.admin_type, {
          foreignKey : 'fk_admin_type',
          constraints: true
        });
      }
    }
  });
  return admin;
};