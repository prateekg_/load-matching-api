'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tyre_group_mapping', {
    fk_group_id: DataTypes.INTEGER,
    fk_tyre_id: DataTypes.INTEGER,
    tis: DataTypes.BIGINT,
    connected: {
      type :DataTypes.BOOLEAN,
      defaultValue : true
    }
  });
};