'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trip_log', {
    fk_trip__id: DataTypes.INTEGER,
    cost: DataTypes.NUMERIC(10,2),
    violations: DataTypes.JSON,
    distance: DataTypes.FLOAT,
    stops: DataTypes.INTEGER,
    time_taken: DataTypes.INTEGER,
    departure: DataTypes.INTEGER,
    arrival: DataTypes.INTEGER,
    location: DataTypes.JSON,
    stationary_time: DataTypes.INTEGER,
    trip: DataTypes.JSON,
    avg_speed: DataTypes.INTEGER,
    eta: DataTypes.INTEGER,
    delayed_by: DataTypes.INTEGER,
    delayed: DataTypes.BOOLEAN,
    progress: DataTypes.INTEGER

  });
};