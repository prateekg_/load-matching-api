'use strict';
module.exports = function(sequelize, DataTypes) {
  let url = sequelize.define('url', {
    name: DataTypes.STRING,
    host: DataTypes.TEXT,
    public_host: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        url.hasMany(models.api,{
          foreignKey: 'fk_url_id', 
          constraints: false
        });
      }
    }
  });
  return url;
};