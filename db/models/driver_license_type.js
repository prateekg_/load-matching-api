'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('driver_license_type', {
    type: DataTypes.TEXT,
    active: DataTypes.BOOLEAN,
    description: DataTypes.TEXT
  });
};