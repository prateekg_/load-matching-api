'use strict';
module.exports = function(sequelize, DataTypes) {
  let trip_planning = sequelize.define('trip_planning', {
    fk_c_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    start_tis: DataTypes.INTEGER,
    end_tis: DataTypes.INTEGER,
    status: DataTypes.TEXT,
    tid: DataTypes.TEXT,
    is_deviating:{
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    geo_trip_route: DataTypes.JSON,
    oth_details: DataTypes.JSON,
    started: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    ended: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    route_box: DataTypes.GEOMETRY,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    est_distance: DataTypes.INTEGER,
    est_time: DataTypes.INTEGER,
    est_matrix: DataTypes.JSON,
    way_points: DataTypes.INTEGER,
    waypoints : DataTypes.JSON,
    new : true,
    ext_tid: DataTypes.TEXT,
    custom_fields: DataTypes.JSON,
    fk_route_template_id: DataTypes.INTEGER,
    shared_with: DataTypes.JSON,
    past: DataTypes.BOOLEAN,
    shipment_no: DataTypes.INTEGER,
    fk_asset_type_id: DataTypes.INTEGER,
    round_trip: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here

        trip_planning.hasOne(models.trip_detail,{
          foreignKey: 'fk_trip_plan_id',
          constraints: false
        });

        trip_planning.hasMany(models.trip_log,{
          foreignKey: 'fk_trip_plan_id',
          constraints: false
        });


        trip_planning.belongsTo(models.asset,{
          foreignKey: 'fk_asset_id',
          constraints: false
        });

        trip_planning.hasMany(models.tag,{
          foreignKey: 't_id',
          constraints: false
        });
      }
    }
  });
  return trip_planning;
};