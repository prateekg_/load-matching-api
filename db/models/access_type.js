'use strict';
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('access_type', {
    type: DataTypes.TEXT
  })
};
