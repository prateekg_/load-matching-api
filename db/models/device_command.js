'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('device_command', {
    name: DataTypes.TEXT,
    description: DataTypes.TEXT,
    transmission_method: DataTypes.TEXT,
    notes: DataTypes.TEXT
  });
};