'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('machine_user_api_mapping', {
    fk_machine_user_id: DataTypes.INTEGER,
    fk_api_id: DataTypes.INTEGER,
    connected: DataTypes.BOOLEAN
  });
};