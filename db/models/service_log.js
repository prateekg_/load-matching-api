'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('service_log', {
    fk_asset_id: DataTypes.INTEGER,
    fk_u_id: DataTypes.INTEGER,
    fk_entity_id: DataTypes.INTEGER,
    notes: DataTypes.STRING,
    tis : DataTypes.INTEGER
  });
};