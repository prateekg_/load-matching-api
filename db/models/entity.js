'use strict';
module.exports = function(sequelize, DataTypes) {
  let entity = sequelize.define('entity', {
    e_id: DataTypes.TEXT,
    cur_fmw_ver: DataTypes.TEXT,
    mac_adr: DataTypes.TEXT,
    imei_no: DataTypes.TEXT,
    active: {
      type : DataTypes.BOOLEAN,
      defaultValue : true
    },
    lat_fmw_ver: DataTypes.TEXT,
    fk_e_type_id: DataTypes.INTEGER,
    sim_no : DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        entity.hasMany(models.entity_mapping,{
          foreignKey: 'fk_entity_id',
          constraints: false
        }); 
        entity.belongsTo(models.entity_type,{
          foreignKey: 'fk_e_type_id',
          constraints: false
        });
      }
    }
  });
  return entity;
};