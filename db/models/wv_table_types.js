'use strict';
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('wv_table_types', {
    fk_c_id: DataTypes.BIGINT,
    platform: DataTypes.STRING,
    table_name: DataTypes.STRING,
    table_description: DataTypes.TEXT,
    default_config: DataTypes.JSON
  });
};
