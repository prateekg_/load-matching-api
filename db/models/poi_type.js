'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('poi_type', {
    type: DataTypes.STRING,
    fk_c_id : DataTypes.INTEGER,
    company_fields : DataTypes.JSON
  });
};