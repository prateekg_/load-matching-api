'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('otp_service', {
    feature: DataTypes.TEXT,
    active: DataTypes.BOOLEAN,
    person_type: DataTypes.ENUM('client', 'wad')
  });
};