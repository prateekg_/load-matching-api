'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('email_request', {
    fk_u_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER,
    n_email: DataTypes.TEXT,
    o_email: DataTypes.TEXT,
    approved: DataTypes.BOOLEAN
  });
};