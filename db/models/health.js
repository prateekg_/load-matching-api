'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('health', {
    fk_module_id: DataTypes.INTEGER,
    tis: DataTypes.INTEGER,
    mem_usage: DataTypes.JSON,
    active: DataTypes.BOOLEAN,
    uptime: DataTypes.INTEGER,
    downtime: DataTypes.INTEGER
  });
};