'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('asset_driver_mapping', {
    fk_asset_id: DataTypes.INTEGER,
    fk_driver_id: DataTypes.INTEGER,
    tis: DataTypes.BIGINT,
    connected: DataTypes.BOOLEAN,
    unassigned_tis: DataTypes.BIGINT,
    lat: DataTypes.FLOAT,
    lon: DataTypes.FLOAT,
    lname: DataTypes.TEXT
  });
};