'use strict';
module.exports = function (sequelize, DataTypes) {
  let admin_url = sequelize.define('admin_url', {
    name: DataTypes.STRING,
    host: DataTypes.STRING
  }, {
    classMethods: {
      associate: function (models) {
        // associations can be defined here
        admin_url.hasMany(models.admin_api, {
          foreignKey: 'fk_url_id',
          constraints: false
        })
      }
    }
  });
  return admin_url
};
