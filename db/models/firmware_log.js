'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('firmware_log', {
    fk_entity_id: DataTypes.INTEGER,
    status: DataTypes.INTEGER,
    batch_id: DataTypes.TEXT
  });
};