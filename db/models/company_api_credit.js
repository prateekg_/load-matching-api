'use strict';
module.exports = (sequelize, DataTypes) => {
  const company_api_credit = sequelize.define('company_api_credit', {
    fk_c_id: DataTypes.INTEGER,
    credit: DataTypes.INTEGER
  }, {});
  company_api_credit.associate = function(models) {
    // associations can be defined here
  };
  return company_api_credit;
};