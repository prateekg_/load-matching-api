'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('reports_log', {
    fk_sr_id: DataTypes.INTEGER,
    sent_at_tis: DataTypes.INTEGER
  });
};