'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('notification_setting', {
    fk_c_id: DataTypes.INTEGER,
    settings: DataTypes.JSON,
    fk_g_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};