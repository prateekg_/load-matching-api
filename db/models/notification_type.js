'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('notification_type', {
    type: DataTypes.STRING
  });
};