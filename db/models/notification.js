'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('notification', {
    cid: DataTypes.STRING,
    msg: DataTypes.TEXT,
    level: DataTypes.STRING,
    fk_notif_id: DataTypes.INTEGER,
    tis: DataTypes.INTEGER,
    fk_entity_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER,
    fk_poi_id: DataTypes.INTEGER,
    fk_u_id: DataTypes.INTEGER,
    is_good: DataTypes.BOOLEAN,
    anomaly: DataTypes.TEXT
  });
};