'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nubot_sensor_data', {
    fk_asset_id: DataTypes.INTEGER,
    fk_entity_id: DataTypes.INTEGER,
    fk_nubot_sensor_type: DataTypes.INTEGER,
    data: DataTypes.JSON,
    tis: DataTypes.INTEGER
  });
};