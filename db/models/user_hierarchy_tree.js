'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_hierarchy_tree', {
    fk_c_id: DataTypes.INTEGER,
    tree: DataTypes.JSON
  });
};