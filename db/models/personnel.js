'use strict';
module.exports = function(sequelize, DataTypes) {
  let personnel = sequelize.define('personnel', {
    prsnl_id: DataTypes.TEXT,
    fk_c_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    p_word: DataTypes.TEXT,
    tel_no: DataTypes.TEXT,
    email: DataTypes.TEXT,
    active: {
      type : DataTypes.BOOLEAN,
      defaultValue : true
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        personnel.hasMany(models.personnel_mapping,{
          foreignKey: 'fk_prsnl_id',
          constraints: false
        });

        personnel.belongsTo(models.company,{
          foreignKey: 'fk_c_id',
          constraints: false
        });
      }
    }
  });
  return personnel;
};