'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('maintenance_record', {
    fk_asset_id: DataTypes.INTEGER,
    service_tis:DataTypes.INTEGER,
    odometer: DataTypes.INTEGER,
    tasks: DataTypes.ARRAY(DataTypes.TEXT),
    parts_cost: DataTypes.DOUBLE,
    labor_cost: DataTypes.DOUBLE,
    total_cost: DataTypes.DOUBLE,
    service_station: DataTypes.TEXT,
    lname:DataTypes.TEXT,
    time_interval_unit: DataTypes.TEXT,
    time_interval_value: DataTypes.INTEGER,
    dist_interval: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN,
    scheduled_tis: DataTypes.INTEGER,
    is_scheduled:DataTypes.BOOLEAN,
    estimated_cost: DataTypes.DOUBLE,
    added_by_fk_u_id: DataTypes.INTEGER,
    last_edited_by_fk_u_id: DataTypes.INTEGER,
    notes: DataTypes.TEXT,
    bill_no: DataTypes.TEXT,
    bill_tis: DataTypes.INTEGER
  });
};