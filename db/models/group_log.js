'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('group_log', {
    fk_group_id: DataTypes.INTEGER,
    fk_prev_group_id: DataTypes.INTEGER,
    fk_by_u_id: DataTypes.INTEGER,
    fk_by_a_id: DataTypes.INTEGER,
    fk_asset_id : DataTypes.INTEGER,
    fk_poi_id : DataTypes.INTEGER,
    fk_u_id : DataTypes.INTEGER,
    payload : DataTypes.JSON,
    tis: DataTypes.INTEGER,
    msg : DataTypes.TEXT
  });
};