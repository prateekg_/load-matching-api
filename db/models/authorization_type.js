'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('authorization_type', {
    type: DataTypes.STRING,
    description: DataTypes.TEXT
  });
};