'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('city', {
    name: DataTypes.TEXT,
    state_id: DataTypes.INTEGER
  });
};