'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nubot_sanity_check', {
    fk_entity_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    a: DataTypes.INTEGER,
    b: DataTypes.INTEGER,
    date: DataTypes.TEXT,
    lagging_by: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};