'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('maintenance_type', {
    type: DataTypes.STRING,
    fk_c_id: DataTypes.INTEGER
  });
};