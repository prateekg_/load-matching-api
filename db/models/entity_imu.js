'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('entity_imu', {
    ax: DataTypes.DOUBLE,
    ay: DataTypes.DOUBLE,
    az: DataTypes.DOUBLE,
    gx: DataTypes.DOUBLE,
    gy: DataTypes.DOUBLE,
    gz: DataTypes.DOUBLE,
    fk_entity_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    tis: DataTypes.INTEGER,
    created_at: DataTypes.INTEGER
  });
};