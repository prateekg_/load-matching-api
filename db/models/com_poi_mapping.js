'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('com_poi_mapping', {
    fk_c_id: DataTypes.INTEGER,
    fk_poi_id: DataTypes.INTEGER,
    connected: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};