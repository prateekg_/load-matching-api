'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('entity_health', {
    fk_loc_id: DataTypes.INTEGER,
    tis: DataTypes.INTEGER,
    b1: DataTypes.FLOAT,
    b2: DataTypes.FLOAT,
    b3: DataTypes.FLOAT,
    type: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    fk_entity_id: DataTypes.INTEGER,
    value: DataTypes.FLOAT
  });
};