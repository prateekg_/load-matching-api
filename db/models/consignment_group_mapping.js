'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('consignment_group_mapping', {
    fk_group_id: DataTypes.INTEGER,
    fk_consignment_id: DataTypes.INTEGER,
    connected: DataTypes.BOOLEAN,
    tis: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};