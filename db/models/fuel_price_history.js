'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('fuel_price_history', {
    fk_fuel_price_id: DataTypes.INTEGER,
    price: DataTypes.DECIMAL
  });
};