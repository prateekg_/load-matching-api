'use strict';
module.exports = function(sequelize, DataTypes) {
  let features = sequelize.define('features', {
    name: DataTypes.STRING,
    default: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {         
        features.hasMany(models.module_features,{
          foreignKey: 'fk_feature_id',
          constraints: false
        });   
      }
    }
  });
  return features;
};