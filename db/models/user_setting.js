'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_setting', {
    fk_u_id: DataTypes.INTEGER,
    settings: DataTypes.JSON,
    group_settings: DataTypes.JSON
  }, {
    timestamps: false
  });
};