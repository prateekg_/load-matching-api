'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('emergency_contact', {
    fk_u_id: DataTypes.INTEGER,
    firstname: DataTypes.TEXT,
    lastname: DataTypes.TEXT,
    phone: DataTypes.TEXT,
    relation: DataTypes.TEXT
  });
};