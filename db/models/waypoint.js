'use strict';
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('waypoint', {
    fk_trip_plan_id: DataTypes.INTEGER,
    fk_poi_id: DataTypes.INTEGER,
    lname: DataTypes.TEXT,
    lat: DataTypes.FLOAT,
    lon: DataTypes.FLOAT,
    tis: DataTypes.INTEGER,
    stop_dur: DataTypes.INTEGER,
    seq_no: DataTypes.INTEGER,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    loading: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    unloading: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    oth_details: DataTypes.JSON
  });
};
