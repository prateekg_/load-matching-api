'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nubot_sensor_instance', {
    fk_asset_id: DataTypes.INTEGER,
    units_aft: DataTypes.DECIMAL,
    units_bef: DataTypes.DECIMAL,
    lat: DataTypes.DOUBLE,
    lon: DataTypes.DOUBLE,
    tis: DataTypes.BIGINT,
    lname: DataTypes.TEXT,
    increasing: DataTypes.BOOLEAN,
    fk_sensor_id: DataTypes.INTEGER,
    fk_sensor_unit_id: DataTypes.INTEGER
  });
};