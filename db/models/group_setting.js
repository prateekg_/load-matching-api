'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('group_setting', {
    fk_g_id: DataTypes.INTEGER,
    settings: DataTypes.JSON
  });
};