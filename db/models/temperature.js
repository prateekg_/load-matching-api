'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('temperature', {
    fk_loc_id: DataTypes.BIGINT,
    z1: DataTypes.DOUBLE,
    z2: DataTypes.DOUBLE,
    z3: DataTypes.DOUBLE,
    z4: DataTypes.DOUBLE,
    z5: DataTypes.DOUBLE,
    z6: DataTypes.DOUBLE,
    z7: DataTypes.DOUBLE,
    z8: DataTypes.DOUBLE,
    eid: DataTypes.TEXT,
    aid: DataTypes.TEXT,
    cid: DataTypes.TEXT,
    tid: DataTypes.TEXT,
    tis: DataTypes.INTEGER
  });
};