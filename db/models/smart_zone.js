'use strict';
module.exports = function(sequelize, DataTypes) {
  let smart_zone = sequelize.define('smart_zone', {
    start_tis: DataTypes.INTEGER,
    end_tis: DataTypes.INTEGER,
    geo_loc: DataTypes.GEOGRAPHY,
    signal_strength: DataTypes.INTEGER,
    fk_smart_zone_type: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        smart_zone.belongsTo(models.smart_zone_type, {
          foreignKey: 'fk_smart_zone_type',
          constraints: false
        });
      }
    }
  });
  return smart_zone;
};