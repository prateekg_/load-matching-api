'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('group', {
    fk_c_id: DataTypes.INTEGER,
    name: DataTypes.TEXT,
    gid: DataTypes.TEXT,
    active: DataTypes.BOOLEAN,
    is_admin: DataTypes.BOOLEAN,
    is_int: DataTypes.BOOLEAN,
    description: DataTypes.TEXT,
    details: DataTypes.JSON
  });
};