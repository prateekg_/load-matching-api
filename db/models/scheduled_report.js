'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('scheduled_report', {
    fk_c_id: DataTypes.INTEGER,
    fk_report_type_id: DataTypes.INTEGER,
    name: DataTypes.TEXT,
    summary: DataTypes.TEXT,
    frequency: DataTypes.TEXT,
    groups: DataTypes.JSON,
    active: DataTypes.BOOLEAN,
    start_tis: DataTypes.INTEGER,
    end_tis: DataTypes.INTEGER,
    trigger_time: DataTypes.STRING,
    template: DataTypes.JSON,
    trigger_on: DataTypes.STRING
  });
};