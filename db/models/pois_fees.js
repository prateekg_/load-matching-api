'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pois_fees', {
    fk_poi_id: DataTypes.INTEGER,
    fees: DataTypes.JSON,
    active: DataTypes.BOOLEAN
  });
};