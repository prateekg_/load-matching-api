'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('location', {
    eid: DataTypes.TEXT,
    cid: DataTypes.TEXT,
    aid: DataTypes.TEXT,
    tis: DataTypes.INTEGER,
    lat: DataTypes.FLOAT,
    lon: DataTypes.FLOAT,
    spd: DataTypes.FLOAT,
    ign: DataTypes.CHAR,
    osf: DataTypes.BOOLEAN,
    hbk: DataTypes.BOOLEAN,
    sig: DataTypes.INTEGER,
    fk_entity_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER,
    fuel: DataTypes.INTEGER,
    harsh_acceleration: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here    	 
      }
    }
  });
};