'use strict';
module.exports = (sequelize, DataTypes) => {
    const vehicle_logs = sequelize.define('vehicle_logs', {
        custtripid: DataTypes.STRING,
        customerid: DataTypes.STRING,
        hextagid: DataTypes.STRING,
        transactiondate: DataTypes.DATE,
        vehiclenumber: DataTypes.STRING,
        plazacode: DataTypes.STRING,
        vehicleclass: DataTypes.STRING,
        location: DataTypes.STRING

    }, {});
    vehicle_logs.associate = function(models) {
        // associations can be defined here
    };
    return vehicle_logs;
};
