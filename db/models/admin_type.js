'use strict';
module.exports = function (sequelize, DataTypes) {
  let admin_type = sequelize.define('admin_type', {
    type: DataTypes.STRING,
    access_list: DataTypes.JSON
  }, {
    classMethods: {
      associate: function (models) {
        // Associations can be defined here
        // Defining the association with 'Admin' model here
        admin_type.hasMany(models.admin, {
          foreignKey: 'fk_admin_type',
          constraints: false
        })
      }
    }
  });
  return admin_type
};
