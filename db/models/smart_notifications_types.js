'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('smart_notifications_types', {
    type: DataTypes.STRING
  });
};