'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('module_features', {
    fk_module_id: DataTypes.INTEGER,
    fk_feature_id: DataTypes.INTEGER
  });
};