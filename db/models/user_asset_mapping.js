'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_asset_mapping', {
    fk_u_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    connected: {
      type :DataTypes.BOOLEAN,
      defaultValue : true
    }
  });
};