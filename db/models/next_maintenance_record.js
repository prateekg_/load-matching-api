'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('next_maintenance_record', {
    fk_main_id: DataTypes.INTEGER,
    fk_main_type_id: DataTypes.INTEGER,
    distance: DataTypes.BIGINT,
    month: DataTypes.INTEGER,
    due_tis: DataTypes.INTEGER,
    notify_on_date: DataTypes.BOOLEAN,
    notify_one_week: DataTypes.BOOLEAN,
    notify_one_month: DataTypes.BOOLEAN
  });
};