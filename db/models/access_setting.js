'use strict';
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('access_setting', {
    fk_group_id: DataTypes.INTEGER,
    settings: DataTypes.JSON
  })
};
