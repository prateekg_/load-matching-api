'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('company_details', {
    fk_c_id: DataTypes.INTEGER,
    acc_no: DataTypes.TEXT,
    pan_no: DataTypes.TEXT,
    tan_no: DataTypes.TEXT,
    w_ph: DataTypes.TEXT,
    m_ph: DataTypes.TEXT,
    h_ph: DataTypes.TEXT,
    o_ph: DataTypes.TEXT,
    w_web: DataTypes.TEXT,
    p_web: DataTypes.TEXT,
    o_web: DataTypes.TEXT,
    linkedin: DataTypes.TEXT,
    twitter: DataTypes.TEXT,
    googleplus: DataTypes.TEXT,
    facebook: DataTypes.TEXT,
  });
};