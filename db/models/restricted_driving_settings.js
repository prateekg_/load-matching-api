'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('restricted_driving_settings', {
    fk_group_id: DataTypes.INTEGER,
    start_time: DataTypes.TEXT,
    end_time: DataTypes.TEXT,
    active: DataTypes.BOOLEAN
  });
};