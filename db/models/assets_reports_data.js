'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('assets_reports_data', {
    fk_asset_id: DataTypes.INTEGER,
    shift_start_tis: DataTypes.INTEGER,
    shift_end_tis: DataTypes.INTEGER,
    shift_start_lname: DataTypes.TEXT,
    shift_end_lname: DataTypes.TEXT,
    tot_eng: DataTypes.DOUBLE,
    tot_dist: DataTypes.DOUBLE,
    tot_idle: DataTypes.DOUBLE,
    tot_stat: DataTypes.DOUBLE,
    avg_spd: DataTypes.INTEGER,
    max_spd: DataTypes.INTEGER,
    max_spd_tis: DataTypes.INTEGER,
    max_spd_lname: DataTypes.TEXT,
    tot_osf: DataTypes.INTEGER,
    tot_hbk: DataTypes.INTEGER,
    max_stop_lname: DataTypes.TEXT,
    max_stop_duration: DataTypes.DOUBLE,
    start_tis: DataTypes.INTEGER,
    end_tis: DataTypes.INTEGER,
    distances: DataTypes.JSON
  });
};