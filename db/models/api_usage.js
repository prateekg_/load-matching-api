'use strict';
module.exports = function (sequelize, DataTypes) {
  let api_usage = sequelize.define('api_usage', {
    fk_c_id: DataTypes.INTEGER,
    fk_u_id: DataTypes.INTEGER,
    fk_api_id: DataTypes.INTEGER,
    ip: DataTypes.TEXT,
    details: DataTypes.JSON
  }, {
    classMethods: {

      associate: function (models) {
        // associations can be defined here
        api_usage.belongsTo(models.user, {
          foreignKey: 'fk_u_id',
          constraints: false
        });
        api_usage.belongsTo(models.api, {
          foreignKey: 'fk_api_id',
          constraints: false
        })
      }
    }
  });
  return api_usage
};
