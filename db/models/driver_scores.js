'use strict';
module.exports = function (sequelize, DataTypes) {
  let driver_scores = sequelize.define('driver_scores', {
    fk_user_id: DataTypes.INTEGER,
    stis: DataTypes.INTEGER,
    etis: DataTypes.INTEGER,
    distance: DataTypes.DOUBLE,
    speed_violations: DataTypes.DOUBLE,
    harsh_braking: DataTypes.DOUBLE,
    harsh_accelerations: DataTypes.DOUBLE,
    night_driving: DataTypes.DOUBLE,
    values: DataTypes.JSON,
    total_score: DataTypes.DOUBLE
  }, {
    classMethods: {
      associate: function (models) {
        // associations can be defined here
        driver_scores.belongsTo(models.user, {
          foreignKey: 'fk_user_id',
          constraints: true
        });
      }
    }
  });
  return driver_scores;
};