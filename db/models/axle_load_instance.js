'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('axle_load_instance', {
    fk_asset_id: DataTypes.BIGINT,
    value_before: DataTypes.FLOAT,
    value_after: DataTypes.FLOAT,
    tis: DataTypes.BIGINT,
    lat: DataTypes.FLOAT,
    lon: DataTypes.FLOAT,
    lname: DataTypes.TEXT,
    loaded: DataTypes.BOOLEAN,
    active: DataTypes.BOOLEAN
  });
};