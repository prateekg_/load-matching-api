'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mobile_update_logs', {
    type: DataTypes.STRING,
    imei: DataTypes.BIGINT,
    fk_u_id: DataTypes.INTEGER,
    cur_version: DataTypes.FLOAT,
    oth_details: DataTypes.JSON
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};