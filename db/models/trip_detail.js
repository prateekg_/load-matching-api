'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trip_detail', {
    fk_trip_plan_id: DataTypes.INTEGER,
    avg_spd: DataTypes.INTEGER,
    act_distance: DataTypes.INTEGER,
    act_start_tis: DataTypes.INTEGER,
    act_end_tis: DataTypes.INTEGER,
    act_matrix: DataTypes.JSON
  });
};