'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('asset_poi', {
    fk_asset_id: DataTypes.INTEGER,
    fk_poi_id: DataTypes.INTEGER,
    timestamp: DataTypes.INTEGER,
    lat: DataTypes.FLOAT,
    lon: DataTypes.FLOAT,
    poi_stat: DataTypes.INTEGER
  });
};