'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_roles', {
    fk_c_id: DataTypes.BIGINT,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    config: DataTypes.JSON
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};