'use strict';
module.exports = function (sequelize, DataTypes) {
  let user = sequelize.define('user', {
    fk_c_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    p_word: DataTypes.TEXT,
    cont_no: DataTypes.TEXT,
    email: DataTypes.TEXT,
    reset_password_token: DataTypes.TEXT,
    reset_password_expires: DataTypes.DATE,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    is_internal: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    fk_auth_type: DataTypes.INTEGER,
    rmq_p_word: DataTypes.TEXT,
    firstname: DataTypes.TEXT,
    lastname: DataTypes.TEXT,
    fk_user_type_id: DataTypes.INTEGER,
    location: DataTypes.TEXT,
    dob: DataTypes.DATE,
    aadhar_no: DataTypes.TEXT,
    is_online: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    last_online_tis: DataTypes.INTEGER,
    sec_cont_no: DataTypes.TEXT
  });

  user.associate = function (models) {
    // associations can be defined here
    user.hasMany(models.user_asset_mapping,{
      foreignKey: 'fk_u_id',
      constraints: true
    });

    user.belongsTo(models.company,{
      foreignKey: 'fk_c_id',
      constraints: false
    });

    user.hasOne(models.driver_detail, {
      foreignKey: 'fk_u_id',
      sourceKey: 'id'
    });

    user.hasOne(models.emergency_contact, {
      foreignKey: 'fk_u_id'
    });

    user.belongsTo(models.user_type,{
      foreignKey: 'fk_user_type_id',
      targetKey: 'id'
    })

  };
  return user
};
