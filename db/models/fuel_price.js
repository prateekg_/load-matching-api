'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('fuel_price', {
    ext_location_id: DataTypes.TEXT,
    lat: DataTypes.DECIMAL,
    lon: DataTypes.DECIMAL,
    lname: DataTypes.TEXT,
    active: DataTypes.BOOLEAN,
    price: DataTypes.DECIMAL,
    fuel_type: DataTypes.TEXT
  });
};