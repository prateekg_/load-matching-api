'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('consignment_mapping', {
    fk_asset_id: DataTypes.INTEGER,
    fk_consignment_id: DataTypes.INTEGER,
    connected: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};