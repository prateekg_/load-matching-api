'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('segment', {
    fk_asset_id: DataTypes.INTEGER,
    start_time: DataTypes.INTEGER,
    end_time: DataTypes.INTEGER,
    start_loc: DataTypes.TEXT,
    end_loc: DataTypes.TEXT,
    avg_spd: DataTypes.INTEGER,
    idle_time: DataTypes.INTEGER,
    duration: DataTypes.INTEGER,
    tot_dist: DataTypes.FLOAT
  });
};
