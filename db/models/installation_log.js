'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('installation_log', {
    fk_u_id: DataTypes.INTEGER,
    fk_entity_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    tis: DataTypes.INTEGER,
    time_taken: DataTypes.INTEGER
  }, {
    updatedAt: false,
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};