'use strict';
module.exports = function (sequelize, DataTypes) {
  let consignment_history = sequelize.define('consignment_history', {
    fk_con_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    fk_c_s_type: DataTypes.INTEGER,
    tis: DataTypes.INTEGER,
    loc: DataTypes.TEXT,
    details: DataTypes.JSON,
    lat: DataTypes.FLOAT,
    lon: DataTypes.FLOAT
  }, {
    classMethods: {
      associate: function (models) {
        // associations can be defined here
        consignment_history.belongsTo(models.consignment, {foreignKey: 'id'});
        // consignment_history.hasMany(models.consignment_status_type, {foreignKey: 'id'})
      }
    }
  });
  return consignment_history;
};