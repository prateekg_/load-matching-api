'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nubot_sensor_type', {
    sensor_type: DataTypes.TEXT,
    description: DataTypes.STRING
  });
};