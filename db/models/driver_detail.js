'use strict';
module.exports = function(sequelize, DataTypes) {
  let driver_detail = sequelize.define('driver_detail', {
    driver_id: DataTypes.TEXT,
    lic_no: DataTypes.TEXT,
    lic_issue_tis: DataTypes.BIGINT,
    lic_expiry_tis: DataTypes.BIGINT,
    fitness_certificate_issue_tis: DataTypes.BIGINT,
    fitness_certificate_expiry_tis: DataTypes.BIGINT,
    hazardous_certificate_no: DataTypes.TEXT,
    hazardous_certificate_issue_tis: DataTypes.BIGINT,
    hazardous_certificate_expiry_tis: DataTypes.BIGINT,
    custom_fields: DataTypes.JSON,
    fk_u_id: DataTypes.INTEGER,
    lic_meta_files: DataTypes.JSON,
    hazardous_certificate_meta_files: DataTypes.JSON,
    emp_type: DataTypes.TEXT,
    salary: DataTypes.INTEGER,
    emp_tis: DataTypes.INTEGER,
    hours_of_service: DataTypes.JSON,
    driver_type: DataTypes.TEXT,
    fk_driver_license_type: DataTypes.INTEGER,
    rate_per_km: DataTypes.INTEGER

  });

  driver_detail.associate = function (models) {
    // associations can be defined here
    driver_detail.belongsTo(models.user,{
      foreignKey: 'fk_u_id',
      targetkey: 'id'
    });

    driver_detail.belongsTo(models.driver_license_type,{
      foreignKey: 'fk_driver_license_type',
      constraints: true
    });
  };

  return driver_detail;
};