'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('assets_pois_stats', {
    fk_asset_id: DataTypes.BIGINT,
    fk_poi_id: DataTypes.INTEGER,
    entry_tis: DataTypes.INTEGER,
    exit_tis: DataTypes.INTEGER,
    latest_tis: DataTypes.INTEGER,
    poi_stat: DataTypes.INTEGER,
    enum_type: {
      type: DataTypes.ENUM,
      values: ['Vehicle', 'Consignment']
    }
  });
};