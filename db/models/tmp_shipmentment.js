'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tmp_shipmentment', {
    fk_c_id: DataTypes.INTEGER,
    fk_trip_id: DataTypes.INTEGER,
    fk_consignement_ids: DataTypes.ARRAY(DataTypes.INTEGER),
    active: DataTypes.BOOLEAN
  });
};