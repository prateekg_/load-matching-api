'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nu_smart_zone', {
    active: DataTypes.BOOLEAN,
    start_tis: DataTypes.INTEGER,
    end_tis: DataTypes.INTEGER,
    file_meta: DataTypes.JSON,
    level: DataTypes.INTEGER,
    fk_smart_zone_type_id: DataTypes.INTEGER,
    tis: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};