'use strict';
module.exports = function(sequelize, DataTypes) {
  let api = sequelize.define('api', {
    name: DataTypes.STRING,
    path: DataTypes.TEXT,
    limit: DataTypes.INTEGER,
    active: {
      type :DataTypes.BOOLEAN,
      defaultValue : true
    },
    auth: DataTypes.ARRAY(DataTypes.INTEGER),   
    fk_url_id: DataTypes.INTEGER,
    feature_name: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        api.belongsTo(models.url, {
          foreignKey: 'fk_url_id', 
          constraints: false
        });
      }
    }
  });
  return api;
};
