'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('consignment_status', {
    fk_consignment_id: DataTypes.INTEGER,
    fk_by_u_id: DataTypes.INTEGER,
    tis: DataTypes.INTEGER,
    status: DataTypes.TEXT,
    note: DataTypes.TEXT,
    reason: DataTypes.TEXT,
    is_current: {
      type: DataTypes.BOOLEAN,
      defaultValue : true
    },
    files_meta: DataTypes.JSON,
    signature: DataTypes.TEXT,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue : true
    }
  });
};