'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('fuel_sensor_instance', {
    fk_asset_id: DataTypes.BIGINT,
    volume_after: DataTypes.FLOAT,
    volume_before: DataTypes.FLOAT,
    tis: DataTypes.BIGINT,
    lat: DataTypes.FLOAT,
    lon: DataTypes.FLOAT,
    lname: DataTypes.TEXT,
    is_refuel: DataTypes.BOOLEAN,
    active: DataTypes.BOOLEAN
  });
};