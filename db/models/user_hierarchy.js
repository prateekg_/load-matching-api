'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_hierarchy', {
    fk_parent_user_id: DataTypes.INTEGER,
    fk_user_id: DataTypes.INTEGER,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  });
};