'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('client_service', {
    fk_c_id: DataTypes.INTEGER,
    description: DataTypes.TEXT,
    name: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function() {
        // associations can be defined here
      }
    }
  });
};