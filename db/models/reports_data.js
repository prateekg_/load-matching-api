'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('reports_data', {
    fk_scheduled_report_type_id: DataTypes.INTEGER,
    data: DataTypes.JSON,
    fk_c_id: DataTypes.INTEGER,
    fk_group_id: DataTypes.INTEGER,
    type: DataTypes.STRING,
    start_tis: DataTypes.INTEGER,
    end_tis: DataTypes.INTEGER
  });
};