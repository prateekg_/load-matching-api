'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('restricted_driving_stats', {
    fk_group_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    start_tis: DataTypes.BIGINT,
    end_tis: DataTypes.BIGINT,
    distance: DataTypes.DOUBLE,
    duration: DataTypes.DOUBLE,
    date: DataTypes.TEXT
  });
};