'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('asset_documents', {
    fk_c_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    fk_u_id: DataTypes.INTEGER,
    fk_document_type_id: DataTypes.INTEGER,
    type_code: DataTypes.TEXT,
    document_no: DataTypes.TEXT,
    issue_tis: DataTypes.INTEGER,
    expiry_tis: DataTypes.INTEGER,
    files_meta: DataTypes.JSON,
    current_record: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  });
};