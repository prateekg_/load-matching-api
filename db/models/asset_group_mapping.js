'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('asset_group_mapping', {
    fk_group_id: DataTypes.INTEGER,
    fk_asset_id: DataTypes.INTEGER,
    connected: DataTypes.BOOLEAN,
    tis: DataTypes.INTEGER
  });
};