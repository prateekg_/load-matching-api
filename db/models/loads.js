'use strict';
module.exports = (sequelize, DataTypes) => {
    const loads = sequelize.define('loads', {
        loadid: DataTypes.STRING,
        pickup_address: DataTypes.STRING,
        pickup_country: DataTypes.STRING,
        pickup_state: DataTypes.STRING,
        pickup_city: DataTypes.STRING,
        pickup_lat: DataTypes.FLOAT,
        pickup_long: DataTypes.FLOAT,
        pickup_date: DataTypes.DATE,
        drop_address: DataTypes.STRING,
        drop_country: DataTypes.STRING,
        drop_state: DataTypes.STRING,
        drop_city: DataTypes.STRING,
        drop_lat: DataTypes.FLOAT,
        drop_long: DataTypes.FLOAT,
        drop_date: DataTypes.DATE,
        load_material_type: DataTypes.STRING,
        load_vehicle_type: DataTypes.STRING,
        price_amount: DataTypes.FLOAT,
        currency: DataTypes.STRING,
        load_weight: DataTypes.FLOAT,
        weight_unit: DataTypes.STRING,
        consignor_first_name: DataTypes.STRING,
        consignor_last_name: DataTypes.STRING,
        email: DataTypes.STRING,
        contact_number: DataTypes.STRING,
        load_valid_from: DataTypes.DATE,
        load_valid_to: DataTypes.DATE
    }, {});
    loads.associate = function(models) {
        // associations can be defined here
    };
    return loads;
};
