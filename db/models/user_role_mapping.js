'use strict';
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user_role_mapping', {
    fk_user_id: DataTypes.INTEGER,
    fk_user_role_id: DataTypes.INTEGER,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  })
};
