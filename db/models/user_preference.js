'use strict';
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user_preference', {
    fk_u_id: DataTypes.INTEGER,
    walkthrough_needed: DataTypes.BOOLEAN,
    preferences: DataTypes.JSON
  }, {
    classMethods: {
      associate: function () {
        // associations can be defined here
      }
    }
  });
};
