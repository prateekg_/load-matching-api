'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('template', {
    fk_c_id: DataTypes.INTEGER,
    name: DataTypes.TEXT,
    geo_trip_route: DataTypes.JSON
  });
};