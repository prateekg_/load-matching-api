'use strict';
module.exports = function(sequelize, DataTypes) {
  let personnel_mapping = sequelize.define('personnel_mapping', {
    fk_asset_id: DataTypes.INTEGER,
    fk_prsnl_id: DataTypes.INTEGER,
    connected: {
      type : DataTypes.BOOLEAN,
      defaultValue : true
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        personnel_mapping.belongsTo(models.asset, {
          foreignKey: 'fk_asset_id',
          constraints: false
        });
        personnel_mapping.belongsTo(models.personnel, {
          foreignKey: 'fk_prsnl_id',
          constraints: false
        });
      }
    }
  });
  return personnel_mapping;
};