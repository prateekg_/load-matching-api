'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('machine_user', {
    fk_c_id: DataTypes.INTEGER,
    token: DataTypes.TEXT,
    is_revoked: DataTypes.BOOLEAN,
    fk_u_id: DataTypes.INTEGER
  });
};