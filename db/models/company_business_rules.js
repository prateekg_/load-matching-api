'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('company_business_rules', {
    name: DataTypes.STRING,
    rule: DataTypes.STRING,
    values: DataTypes.JSON,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    fk_c_id: DataTypes.INTEGER
  });
};