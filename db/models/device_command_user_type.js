'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('device_command_user_type', {
    name: DataTypes.TEXT,
    description: DataTypes.TEXT,
    notes: DataTypes.TEXT
  });
};