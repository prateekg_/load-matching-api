'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('consignment_user_mapping_external', {
    fk_consignment_id: DataTypes.INTEGER,
    fk_u_id: DataTypes.INTEGER,
    fk_user_external_id: DataTypes.INTEGER,
    fk_c_id: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN
  });
};