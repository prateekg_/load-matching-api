'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('poi_contact', {
    cont_name: DataTypes.STRING,
    email: DataTypes.TEXT,
    tel: DataTypes.TEXT,
    fk_poi_id: DataTypes.INTEGER,
  });
};