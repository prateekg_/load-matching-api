'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_otp', {
    fk_u_id: DataTypes.INTEGER,
    secret: DataTypes.TEXT,
    creation_tis: DataTypes.INTEGER,
    counter: DataTypes.INTEGER,
    fk_person_id: DataTypes.TEXT
  });
};