'use strict';
module.exports = function(sequelize, DataTypes) {
  let entity_mapping = sequelize.define('entity_mapping', {
    fk_asset_id: DataTypes.BIGINT,
    fk_entity_id: DataTypes.INTEGER,
    connected: {
      type : DataTypes.BOOLEAN,
      defaultValue : true
    },
    enum_type: {
      type: DataTypes.ENUM,
      values: ['Vehicle', 'Consignment']
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        entity_mapping.belongsTo(models.asset,{
          foreignKey: 'fk_asset_id',
          constraints: true
        });
        entity_mapping.belongsTo(models.entity,{
          foreignKey: 'fk_entity_id',
          constraints: true
        });
      }
    }
  });
  return entity_mapping;
};