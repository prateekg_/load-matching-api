'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('wv_user_config', {
    fk_user_id: DataTypes.BIGINT,
    fk_g_id: DataTypes.BIGINT,
    fk_poi_id: DataTypes.BIGINT,
    fk_wv_table_type_id: DataTypes.BIGINT,
    config: DataTypes.JSON,
    active: {
      type : DataTypes.BOOLEAN,
      defaultValue : true
    },
  });
};