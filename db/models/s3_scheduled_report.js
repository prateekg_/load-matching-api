'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('s3_scheduled_report', {
    fk_c_id: DataTypes.INTEGER,
    tis: DataTypes.INTEGER,
    fk_scheduler_id: DataTypes.INTEGER,
    url: DataTypes.TEXT
  });
};