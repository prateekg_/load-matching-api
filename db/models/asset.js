'use strict';
module.exports = function (sequelize, DataTypes) {
  let asset = sequelize.define('asset', {
    fk_c_id: DataTypes.INTEGER,
    asset_id: DataTypes.TEXT,
    fk_ast_type_id: DataTypes.INTEGER,
    lic_plate_no: DataTypes.TEXT,
    mfg: DataTypes.TEXT,
    model: DataTypes.TEXT,
    yr_of_mfg: DataTypes.TEXT,
    veh_id_no: DataTypes.TEXT,
    oth_details: DataTypes.JSON,
    name: DataTypes.STRING,
    asset_id_ext: DataTypes.TEXT,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    isbuiltintrailer: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    spd_lmt: DataTypes.INTEGER,
    load_lmt: DataTypes.FLOAT,
    no_temp_zone: DataTypes.INTEGER,
    is_refrig: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    zone_lmts: DataTypes.JSON,
    eng_no: DataTypes.TEXT,
    has_eng: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    width: DataTypes.FLOAT,
    height: DataTypes.FLOAT,
    length: DataTypes.FLOAT,
    p_name: DataTypes.TEXT,
    tot_tyres: DataTypes.INTEGER,

    rc_issue_date: DataTypes.INTEGER,
    rc_expiry_date: DataTypes.INTEGER,
    class: DataTypes.STRING,
    body_type: DataTypes.STRING,
    body_color: DataTypes.STRING,
    seating_capacity: DataTypes.INTEGER,
    wheelbase: DataTypes.INTEGER,
    eng_capacity: DataTypes.INTEGER,
    eng_power: DataTypes.INTEGER,
    eng_rpm: DataTypes.INTEGER,
    eng_tot_cylinder: DataTypes.INTEGER,
    axle_tot_no: DataTypes.INTEGER,
    axle_powered_no: DataTypes.INTEGER,
    axle_front_wt: DataTypes.INTEGER,
    axle_rear_wt: DataTypes.INTEGER,
    axle_other_wt: DataTypes.ARRAY(DataTypes.INTEGER),
    gross_wt_certified: DataTypes.INTEGER,
    gross_wt_registered: DataTypes.INTEGER,
    fuel_type: DataTypes.STRING,
    brake_type: DataTypes.STRING,
    owner_f_name: DataTypes.STRING,
    owner_l_name: DataTypes.STRING,
    owner_company: DataTypes.STRING,
    in_charge_as_owner: DataTypes.BOOLEAN,
    incharge_f_name: DataTypes.STRING,
    incharge_l_name: DataTypes.STRING,
    has_abs: DataTypes.BOOLEAN,
    odometer: DataTypes.INTEGER

  }, {
    classMethods: {
      associate: function (models) {
        // associations can be defined here
        asset.hasMany(models.asset_mapping, {
          foreignKey: 'fk_asset_id',
          constraints: false
        });
        asset.hasMany(models.asset_mapping, {
          foreignKey: 'fk_con_asset_id',
          constraints: false
        });
        asset.hasMany(models.entity_mapping, {
          foreignKey: 'fk_asset_id',
          constraints: false
        });
        asset.hasMany(models.personnel_mapping, {
          foreignKey: 'fk_asset_id',
          constraints: false
        });
        asset.belongsTo(models.company, {
          foreignKey: 'fk_c_id',
          constraints: false
        });
        asset.belongsTo(models.asset_type, {
          foreignKey: 'fk_ast_type_id',
          constraints: false
        });
      }
    }
  });
  return asset;
};
