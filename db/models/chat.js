'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('chat', {
    fk_sender_id: DataTypes.INTEGER,
    fk_reciever_id: DataTypes.INTEGER,
    tis: DataTypes.INTEGER,
    msg: DataTypes.TEXT
  });
};