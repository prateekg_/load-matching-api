'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.removeColumn('trailer_type_as', 'spd'),
      queryInterface.removeColumn('trailer_type_as', 'lat'),
      queryInterface.removeColumn('trailer_type_as', 'lon'),
      queryInterface.addColumn('trailer_type_as', 'zones', Sequelize.JSON)
    ]
  },

  down: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('trailer_type_as', 'spd', Sequelize.FLOAT),
      queryInterface.addColumn('trailer_type_as', 'lat', Sequelize.FLOAT),
      queryInterface.addColumn('trailer_type_as', 'lon', Sequelize.FLOAT),
      queryInterface.removeColumn('trailer_type_as', 'zones')
    ]
  }
};
