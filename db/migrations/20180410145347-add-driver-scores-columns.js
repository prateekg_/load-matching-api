'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return  Promise.all([
      queryInterface.removeColumn('driver_scores', 'completed_trips'),
      queryInterface.addColumn('driver_scores', 'harsh_accelerations', {
        type: Sequelize.DOUBLE,
        defaultValue: null
      }),
      queryInterface.addColumn('driver_scores', 'night_driving', {
        type: Sequelize.DOUBLE,
        defaultValue: null
      })
    ])
  }
};
