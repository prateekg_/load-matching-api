'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('report_types', 'fk_feature_id', Sequelize.INTEGER);    
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('report_types', 'fk_feature_id');
  }
};
