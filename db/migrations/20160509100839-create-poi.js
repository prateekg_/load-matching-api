'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('pois', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      geo_loc: {
        type: Sequelize.GEOGRAPHY
      },
      priv_typ: {
        type: Sequelize.BOOLEAN
      },
      name: {
        type: Sequelize.STRING
      },
      typ: {
        type: Sequelize.STRING
      },
      details: {
        type: Sequelize.TEXT
      },
      addr: {
        type: Sequelize.TEXT
      },
      start_tim: {
        type: Sequelize.TEXT
      },
      end_tim: {
        type: Sequelize.TEXT
      },
      entry_fee: {
        type: Sequelize.FLOAT
      },
      dwell_tim_limit: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('pois')
  }
};
