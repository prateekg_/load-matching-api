'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.removeColumn('consignment_histories', 'tis'),
      queryInterface.addColumn('consignment_histories', 'tis', Sequelize.INTEGER)
    ]
  },

  down: function () {
    /*
         Add reverting commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.dropTable('users');
         */
  }
};
