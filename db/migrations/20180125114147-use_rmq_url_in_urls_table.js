'use strict';

module.exports = {
  up: function (queryInterface) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.sequelize.query("UPDATE public.urls SET host='ec2-13-228-52-219.ap-southeast-1.compute.amazonaws.com', \"updatedAt\"='now()' WHERE name = 'RMQINT'");
  },

  down: function (queryInterface) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.sequelize.query("UPDATE public.urls SET host='172.31.16.154', \"updatedAt\"='now()' WHERE name = 'RMQINT'");
  }
};
