'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('asset_fuel_records', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      total_cost: {
        type: Sequelize.DOUBLE
      },
      volume: {
        type: Sequelize.DOUBLE
      },
      price: {
        type: Sequelize.DOUBLE
      },
      fill_tis: {
        type: Sequelize.INTEGER
      },
      odometer: {
        type: Sequelize.INTEGER
      },
      brand: {
        type: Sequelize.TEXT
      },
      type: {
        type: Sequelize.TEXT
      },
      agency: {
        type: Sequelize.TEXT
      },
      lname: {
        type: Sequelize.TEXT
      },
      added_by_fk_u_id: {
        type: Sequelize.INTEGER
      },
      last_edited_by_fk_u_id: {
        type: Sequelize.INTEGER
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('asset_fuel_records')
  }
};
