'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('pois', 'fk_state_id', Sequelize.INTEGER),
      queryInterface.addColumn('pois', 'state', Sequelize.TEXT)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('pois', 'fk_state_id'),
      queryInterface.removeColumn('pois', 'state')
    ]
  }
};
