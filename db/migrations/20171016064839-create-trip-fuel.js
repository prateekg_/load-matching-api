'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('trip_fuels', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_trip_id: {
        type: Sequelize.BIGINT
      },
      consumed: {
        type: Sequelize.FLOAT
      },
      mileage: {
        type: Sequelize.FLOAT
      },
      total_refuels: {
        type: Sequelize.INTEGER
      },
      total_drops: {
        type: Sequelize.INTEGER
      },
      graph_data: {
        type: Sequelize.JSON
      },
      trip_log_fuel: {
        type: Sequelize.JSON
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('trip_fuels');
  }
};