'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('locations', 'hbk', { type: Sequelize.BOOLEAN, defaultValue: false }),
      queryInterface.addColumn('locations', 'sig', { type: Sequelize.INTEGER })
    ]
  },

  down: function (queryInterface) {
    return [

      queryInterface.removeColumn('locations', 'hbk'),
      queryInterface.removeColumn('locations', 'sig')
    ]
  }
};
