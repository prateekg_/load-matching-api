'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('states', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.TEXT
      },
      country_id: {
        type: Sequelize.INTEGER
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('states')
  }
};
