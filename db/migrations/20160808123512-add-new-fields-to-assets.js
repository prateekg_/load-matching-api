'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [

      queryInterface.removeColumn('assets', 'dimen'),
      queryInterface.addColumn('assets', 'width', { type: Sequelize.FLOAT }),
      queryInterface.addColumn('assets', 'height', { type: Sequelize.FLOAT }),
      queryInterface.addColumn('assets', 'length', { type: Sequelize.FLOAT }),
      queryInterface.addColumn('assets', 'p_name', { type: Sequelize.TEXT }),
      queryInterface.addColumn('assets', 'tot_tyres', { type: Sequelize.INTEGER }),
      queryInterface.sequelize.query('ALTER TABLE assets RENAME COLUMN has_trailer TO isBuiltInTrailer'),
      queryInterface.sequelize.query('ALTER TABLE assets ALTER COLUMN load_lmt TYPE DOUBLE PRECISION USING (load_lmt::DOUBLE PRECISION)')

    ]
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
