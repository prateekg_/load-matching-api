'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('asset_stats', 'points_distance', Sequelize.DOUBLE);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('asset_stats', 'points_distance');
  }
};
