'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return  Promise.all([
      queryInterface.addColumn('asset_driver_mappings', 'lat', {
        type: Sequelize.FLOAT,
        defaultValue: null
      }),
      queryInterface.addColumn('asset_driver_mappings', 'lon', {
        type: Sequelize.FLOAT,
        defaultValue: null
      }),
      queryInterface.addColumn('asset_driver_mappings', 'lname', {
        type: Sequelize.TEXT,
        defaultValue: null
      })
    ])
  },

  down: function (queryInterface) {
    return  Promise.all([
      queryInterface.removeColumn('asset_driver_mappings', 'lat'),
      queryInterface.removeColumn('asset_driver_mappings', 'lon'),
      queryInterface.removeColumn('asset_driver_mappings', 'lname')
    ])
  }
};
