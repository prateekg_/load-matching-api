'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('asset_documents', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      fk_u_id: {
        type: Sequelize.INTEGER
      },
      fk_document_type_id: {
        type: Sequelize.INTEGER
      },
      type_code: {
        type: Sequelize.TEXT
      },
      document_no: {
        type: Sequelize.TEXT
      },
      issue_tis: {
        type: Sequelize.INTEGER
      },
      expiry_tis: {
        type: Sequelize.INTEGER
      },
      files_meta: {
        type: Sequelize.JSON
      },
      current_record: {
        type: Sequelize.BOOLEAN,
        default: true
      },
      active: {
        type: Sequelize.BOOLEAN,
        default: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('asset_documents')
  }
};
