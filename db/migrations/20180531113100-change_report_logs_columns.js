'use strict';

module.exports = {
  up: function (queryInterface) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */

    return queryInterface.sequelize.query(`

      BEGIN;

      ALTER TABLE reports_logs ADD sent_at timestamptz;

      update reports_logs set sent_at = to_timestamp(sent_at_tis);

      alter table reports_logs alter sent_at set not null;

      alter table reports_logs drop column sent_at_tis;
   
      ALTER table reports_logs alter "createdAt" set default now();

      ALTER table reports_logs alter "updatedAt" set default now();

      COMMIT;

    `);
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
