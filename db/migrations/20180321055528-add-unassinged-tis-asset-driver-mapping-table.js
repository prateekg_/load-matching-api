'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('asset_driver_mappings', 'unassigned_tis', {
      type: Sequelize.BIGINT,
      defaultValue: null
    });
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('asset_driver_mappings', 'unassigned_tis');
  }
};
