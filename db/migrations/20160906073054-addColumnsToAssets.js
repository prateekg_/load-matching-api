'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('assets', 'rc_issue_date', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'rc_expiry_date', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'class', { type: Sequelize.STRING }),
      queryInterface.addColumn('assets', 'body_type', { type: Sequelize.STRING }),
      queryInterface.addColumn('assets', 'body_color', { type: Sequelize.STRING }),
      queryInterface.addColumn('assets', 'seating_capacity', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'wheelbase', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'eng_capacity', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'eng_power', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'eng_rpm', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'eng_tot_cylinder', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'axle_tot_no', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'axle_front_wt', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'axle_rear_wt', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'axle_other_wt', { type: Sequelize.ARRAY(Sequelize.INTEGER) }),
      queryInterface.addColumn('assets', 'gross_wt_certified', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'gross_wt_registered', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'fuel_type', { type: Sequelize.STRING }),
      queryInterface.addColumn('assets', 'brake_type', { type: Sequelize.STRING }),
      queryInterface.addColumn('assets', 'owner_f_name', { type: Sequelize.STRING }),
      queryInterface.addColumn('assets', 'owner_l_name', { type: Sequelize.STRING }),
      queryInterface.addColumn('assets', 'owner_company', { type: Sequelize.STRING }),
      queryInterface.addColumn('assets', 'in_charge_as_owner', { type: Sequelize.BOOLEAN, defaultValue: false }),
      queryInterface.addColumn('assets', 'incharge_f_name', { type: Sequelize.STRING }),
      queryInterface.addColumn('assets', 'incharge_l_name', { type: Sequelize.STRING }),
      queryInterface.addColumn('assets', 'has_abs', { type: Sequelize.BOOLEAN, defaultValue: false })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('assets', 'rc_issue_date'),
      queryInterface.removeColumn('assets', 'rc_expiry_date'),
      queryInterface.removeColumn('assets', 'class'),
      queryInterface.removeColumn('assets', 'body_type'),
      queryInterface.removeColumn('assets', 'body_color'),
      queryInterface.removeColumn('assets', 'seating_capacity'),
      queryInterface.removeColumn('assets', 'wheelbase'),
      queryInterface.removeColumn('assets', 'eng_capacity'),
      queryInterface.removeColumn('assets', 'eng_power'),
      queryInterface.removeColumn('assets', 'eng_rpm'),
      queryInterface.removeColumn('assets', 'eng_tot_cylinder'),
      queryInterface.removeColumn('assets', 'axle_tot_no'),
      queryInterface.removeColumn('assets', 'axle_front_wt'),
      queryInterface.removeColumn('assets', 'axle_rear_wt'),
      queryInterface.removeColumn('assets', 'gross_wt_certified'),
      queryInterface.removeColumn('assets', 'gross_wt_registered'),
      queryInterface.removeColumn('assets', 'fuel_type'),
      queryInterface.removeColumn('assets', 'brake_type'),
      queryInterface.removeColumn('assets', 'owner_f_name'),
      queryInterface.removeColumn('assets', 'owner_l_name'),
      queryInterface.removeColumn('assets', 'owner_company'),
      queryInterface.removeColumn('assets', 'incharge_f_name'),
      queryInterface.removeColumn('assets', 'incharge_l_name'),
      queryInterface.removeColumn('assets', 'has_abs'),
      queryInterface.removeColumn('assets', 'in_charge_as_owner'),
      queryInterface.removeColumn('assets', 'axle_other_wt')

    ]
  }
};
