'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [

      queryInterface.addColumn('company_details', 'w_ph', { type: Sequelize.TEXT }),
      queryInterface.addColumn('company_details', 'm_ph', { type: Sequelize.TEXT }),
      queryInterface.addColumn('company_details', 'h_ph', { type: Sequelize.TEXT }),
      queryInterface.addColumn('company_details', 'o_ph', { type: Sequelize.TEXT }),
      queryInterface.addColumn('company_details', 'w_web', { type: Sequelize.TEXT }),
      queryInterface.addColumn('company_details', 'p_web', { type: Sequelize.TEXT }),
      queryInterface.addColumn('company_details', 'o_web', { type: Sequelize.TEXT }),
      queryInterface.addColumn('company_details', 'linkedin', { type: Sequelize.TEXT }),
      queryInterface.addColumn('company_details', 'twitter', { type: Sequelize.TEXT }),
      queryInterface.addColumn('company_details', 'googleplus', { type: Sequelize.TEXT }),
      queryInterface.addColumn('company_details', 'facebook', { type: Sequelize.TEXT })

    ]
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
