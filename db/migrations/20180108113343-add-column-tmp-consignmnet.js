'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('tmp_consignments', 'weight_unit',  { 
        type: Sequelize.ENUM('Mt', 'Kg'),
        defaultValue: 'Mt'
      })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('tmp_consignments', 'weight_unit')
    ]
  }
};
