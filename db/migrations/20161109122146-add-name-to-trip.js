'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('trip_plannings', 'name', { type: Sequelize.TEXT })
    ]
  },

  down: function (queryInterface, Sequelize) {
    return [
      queryInterface.removeColumn('trip_plannings', 'name', { type: Sequelize.TEXT })
    ]
  }
};
