'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return  Promise.all([
      queryInterface.addColumn('report_types', 'config', {
        type: Sequelize.JSON,
        defaultValue: null
      })
    ])
  }
};
