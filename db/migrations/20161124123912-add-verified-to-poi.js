'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('pois', 'verified', { type: Sequelize.BOOLEAN, defaultValue: false })
    ]
  },

  down: function () {
    return []
  }
};
