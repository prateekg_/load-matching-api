'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('tyres', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      tyre_id: {
        type: Sequelize.TEXT
      },
      manufacturer: {
        type: Sequelize.TEXT
      },
      model: {
        type: Sequelize.TEXT
      },
      brand: {
        type: Sequelize.TEXT
      },
      type: {
        type: Sequelize.TEXT
      },
      thread: {
        type: Sequelize.TEXT
      },
      size: {
        type: Sequelize.TEXT
      },
      psi: {
        type: Sequelize.INTEGER
      },
      cost: {
        type: Sequelize.INTEGER
      },
      is_new: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      used_km: {
        type: Sequelize.INTEGER
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('tyres')
  }
};
