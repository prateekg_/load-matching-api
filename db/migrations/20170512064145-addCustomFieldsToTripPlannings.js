'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('trip_plannings', 'custom_fields', Sequelize.JSON)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('trip_plannings', 'custom_fields')
    ]
  }
};
