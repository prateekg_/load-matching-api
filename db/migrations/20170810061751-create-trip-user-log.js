'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('trip_user_logs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_trip_id: {
        type: Sequelize.INTEGER
      },
      fk_u_id: {
        type: Sequelize.INTEGER
      },
      new_status: {
        type: Sequelize.TEXT
      },
      old_status: {
        type: Sequelize.TEXT
      },
      note: {
        type: Sequelize.TEXT
      },
      active: {
        type: Sequelize.BOOLEAN,
        default: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('trip_user_logs');
  }
};