'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('maintenance_records', 'active', { type: Sequelize.BOOLEAN, defaultValue: true })
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('maintenance_records', 'active')
  }
};
