'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('group_accesses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_g_id: {
        type: Sequelize.INTEGER
      },
      settings: {
        type: Sequelize.JSON
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('group_accesses')
  }
};
