'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('pois', 'state_code', {
      type: Sequelize.TEXT,
      defaultValue: null
    });
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('pois', 'state_code');
  }
};
