'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('tmp_consignments', 'consignment_no', Sequelize.INTEGER);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('tmp_consignments', 'consignment_no');
  }
};
