'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('nubot_sanity_checks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_entity_id: {
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      a: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      b: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      lagging_by: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      date: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('nubot_santity_checks')
  }
};
