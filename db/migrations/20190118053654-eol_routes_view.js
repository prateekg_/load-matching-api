'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.sequelize.query(`CREATE VIEW eol_routes_view AS
     SELECT iea.route_id,
    iea.consigner,
    iea.consignee,
    iea.origin,
    iea.destination,
    iea.ship_to_party,
    COALESCE(iea.distance::double precision, 0::double precision) AS sap_distance,
    COALESCE(tp.est_distance, 0) AS est_distance,
    COALESCE(tp.est_time, 0) AS est_time,
    iea.fk_t_id AS fk_trip_id
   FROM input_essar_a iea
     JOIN ( SELECT input_essar_a.route_id,
            max(input_essar_a.id) AS id
           FROM input_essar_a
          GROUP BY input_essar_a.route_id) recent ON iea.route_id = recent.route_id AND iea.id = recent.id
     LEFT JOIN trip_plannings tp ON tp.id = iea.fk_t_id;`)
  },

  down: function (queryInterface) {
    return queryInterface.sequelize.query("DROP VIEW eol_routes_view");
  }
};
