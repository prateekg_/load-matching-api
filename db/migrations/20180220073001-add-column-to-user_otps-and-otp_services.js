'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('user_otps', 'fk_person_id', {
        type: Sequelize.TEXT
      }),
      queryInterface.addColumn('otp_services', 'person_type', {
        type: Sequelize.ENUM(['client','wad'])  
      })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('user_otps', 'fk_person_id'),
      queryInterface.removeColumn('otp_services', 'person_type')
    ]
  }
};
