'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('companies', 'transporter_code', { type: Sequelize.STRING })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('companies', 'transporter_code')
    ]
  }
};
