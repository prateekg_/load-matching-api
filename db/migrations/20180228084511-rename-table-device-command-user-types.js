'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.renameTable('device_command_user_types', 'platform_user_types');
  },

  down: function (queryInterface) {
    return queryInterface.renameTable('platform_user_types', 'device_command_user_types');
  }
};
