'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('trip_user_logs', 'platform', Sequelize.TEXT);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('trip_user_logs', 'platform');
  }
};
