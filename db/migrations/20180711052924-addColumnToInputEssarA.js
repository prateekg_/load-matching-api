'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.createTable('users', { id: Sequelize.INTEGER });
        */
    return queryInterface.addColumn('input_essar_a', 'report_generated', Sequelize.BOOLEAN);

  },

  down: function (queryInterface) {
    /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.
        
          Example:
          return queryInterface.dropTable('users');
        */
    return queryInterface.removeColumn('input_essar_a', 'report_generated');

  }
};
