'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('locations', 'lname', Sequelize.TEXT)
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('locations', 'lname')
  }
};
