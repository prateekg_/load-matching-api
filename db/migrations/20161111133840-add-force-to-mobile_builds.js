'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('mobile_builds', 'force', { type: Sequelize.BOOLEAN })
    ]
  },

  down: function (queryInterface, Sequelize) {
    return [
      queryInterface.removeColumn('mobile_builds', 'force', { type: Sequelize.BOOLEAN })
    ]
  }
};
