'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('trip_plannings', 'past', {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    });
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('trip_plannings', 'past');
  }
};
