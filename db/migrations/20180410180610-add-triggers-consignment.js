'use strict';

module.exports = {
  up: function (queryInterface) {
    /*
      Trigger to update consignment status if consignment trip is started and completed
      update consignment status to following:
      consignment status = picked (trip = inprogress)
      consignment status = delevered (trip = completed)
    */

    return [
      queryInterface.sequelize.query(`CREATE OR REPLACE FUNCTION consignment_status_update()
                                                RETURNS trigger AS $fn_consignment_status_update$
                                            BEGIN
                                                IF (NEW.status = 'inprogress') THEN
                                                    UPDATE consignments SET status = 'picked' WHERE id = NEW.fk_asset_id;
                                                    RETURN NEW;
                                                ELSIF (NEW.status = 'completed') THEN
                                                    UPDATE consignments SET status = 'delivered' WHERE id = NEW.fk_asset_id;
                                                    RETURN NEW;
                                                END IF;
                                                RETURN NEW;
                                            END;
                                            $fn_consignment_status_update$
                                            LANGUAGE plpgsql; `),

      queryInterface.sequelize.query(`CREATE TRIGGER trip_status_trigger AFTER UPDATE OF STATUS ON trip_plannings FOR EACH ROW WHEN (NEW.status = 'inprogress' OR NEW.status = 'completed') EXECUTE PROCEDURE consignment_status_update();`)
    ]
  },
  down: function (queryInterface) {
    return [
      queryInterface.sequelize.query(`DROP FUNCTION consignment_status_update()`),
      queryInterface.sequelize.query(`DROP TRIGGER trip_status ON trip_plannings;`)
    ]
    
  }
};
