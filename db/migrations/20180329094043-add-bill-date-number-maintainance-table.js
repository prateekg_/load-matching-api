'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return  Promise.all([
      queryInterface.addColumn('maintenance_records', 'bill_no', {
        type: Sequelize.TEXT,
        defaultValue: null
      }),
      queryInterface.addColumn('maintenance_records', 'bill_tis', {
        type: Sequelize.INTEGER,
        defaultValue: null
      })
    ])
  },

  down: function (queryInterface) {
    return  Promise.all([
      queryInterface.removeColumn('maintenance_records', 'bill_no'),
      queryInterface.removeColumn('maintenance_records', 'bill_tis')
    ])
  }
};
