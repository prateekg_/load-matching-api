'use strict';

module.exports = {
  up: function (queryInterface) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */

    return queryInterface.sequelize.query(`INSERT INTO "public"."poi_types"("type", "createdAt", "updatedAt", "fk_c_id", "company_fields") 
                                        VALUES('DetentionPOI', 'now()', 'now()', NULL, NULL) `)
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */

    
  }
};
