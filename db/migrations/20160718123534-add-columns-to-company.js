'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [

      queryInterface.addColumn('companies', 'ow_name', { type: Sequelize.TEXT }),
      queryInterface.addColumn('companies', 'contact_type', { type: Sequelize.TEXT }),
      queryInterface.addColumn('companies', 'state', { type: Sequelize.TEXT }),
      queryInterface.addColumn('companies', 'city', { type: Sequelize.TEXT }),
      queryInterface.addColumn('companies', 'zip', { type: Sequelize.TEXT }),
      queryInterface.sequelize.query('ALTER TABLE companies RENAME COLUMN reg_addr TO street')

    ]
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
