'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('smart_zones', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      start_tis: {
        type: Sequelize.INTEGER
      },
      end_tis: {
        type: Sequelize.INTEGER
      },
      geo_loc: {
        type: Sequelize.GEOGRAPHY
      },
      signal_strength: {
        type: Sequelize.INTEGER
      },
      fk_smart_zone_type: {
        type: Sequelize.INTEGER,
        references: {
          model: 'smart_zone_types',
          key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(function() {
      return queryInterface.addIndex('smart_zones', ['geo_loc'])
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('smart_zones');
  }
};