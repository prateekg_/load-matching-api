'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('users', 'firstname', { type: Sequelize.TEXT }),
      queryInterface.addColumn('users', 'lastname', { type: Sequelize.TEXT }),
      queryInterface.addColumn('users', 'fk_user_type_id', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('users', 'location', { type: Sequelize.TEXT }),
      queryInterface.addColumn('users', 'dob', { type: Sequelize.DATE }),
      queryInterface.addColumn('users', 'aadhar_no', { type: Sequelize.TEXT })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.addColumn('users', 'firstname'),
      queryInterface.addColumn('users', 'lastname'),
      queryInterface.addColumn('users', 'fk_user_type_id'),
      queryInterface.addColumn('users', 'location'),
      queryInterface.addColumn('users', 'dob'),
      queryInterface.addColumn('users', 'aadhar_no')
    ]
  }
};
