'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('trip_plannings', 'fk_route_template_id', Sequelize.INTEGER),
      queryInterface.addColumn('trip_plannings', 'shared_with', Sequelize.JSON)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('trip_plannings', 'fk_route_template_id'),
      queryInterface.removeColumn('trip_plannings', 'shared_with')
    ]
  }
};
