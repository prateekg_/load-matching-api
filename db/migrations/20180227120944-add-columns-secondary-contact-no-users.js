'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('users', 'sec_cont_no', {
      type: Sequelize.TEXT,
      defaultValue: null
    });
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('users', 'sec_cont_no');
  }
};
