'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('countries', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.TEXT
      },
      sortname: {
        type: Sequelize.TEXT
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('countries')
  }
};
