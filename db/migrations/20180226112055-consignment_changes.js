'use strict';

module.exports = {
  up: function (queryInterface) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */

    // Adds a row to asset_types table for Consignment.
    // Adds a new column to entity_mappings called enum_type (not null, no default). It can take 2 values: 'Vehicle' and 'Consignment'
    // Adds a new column fk_asset_type_id (not null, no default) to trip_plannings referencing asset_types.id.
    // Adds a unique index on asset_types.type.

    // UPDATE trip_plannings set fk_asset_type_id = ast.id from asset_types ast where ast.type = 'Truck';
    return queryInterface.sequelize.query(`
      BEGIN;

      INSERT INTO "public"."asset_types"("type", "createdAt", "updatedAt") VALUES('Consignment', 'now()', 'now()') RETURNING "id", "type", "createdAt", "updatedAt";

      CREATE TYPE "public"."enum_entity_mappings_enum_type" AS ENUM('Vehicle', 'Consignment');

      ALTER TABLE entity_mappings add column enum_type enum_entity_mappings_enum_type DEFAULT 'Vehicle';

      ALTER TABLE "entity_mappings" ALTER COLUMN "enum_type" SET NOT NULL;

      ALTER TABLE "public"."trip_plannings" ADD COLUMN "fk_asset_type_id" integer DEFAULT 1, ADD FOREIGN KEY ("fk_asset_type_id") REFERENCES "public"."asset_types"("id");

      update trip_plannings set fk_asset_type_id = a.fk_ast_type_id from assets a where a.id = fk_asset_id;

      ALTER TABLE trip_plannings ALTER COLUMN fk_asset_type_id SET NOT NULL;

      CREATE UNIQUE INDEX "asset_types_type_idx" ON "public"."asset_types"("type");

      CREATE UNIQUE INDEX "entity_mappings_enum_type_fk_asset_id_idx" ON "public"."entity_mappings"("enum_type","fk_asset_id") WHERE connected;

      CREATE UNIQUE INDEX "entity_mappings_fk_entity_id_idx" ON "public"."entity_mappings"("fk_entity_id") WHERE connected;

      ALTER TABLE "public"."tmp_consignments" RENAME TO "consignments";

      ALTER TABLE "public"."consignments" ALTER COLUMN "id" TYPE bigint;

      ALTER TABLE "public"."entity_mappings" ALTER COLUMN "fk_asset_id" TYPE bigint;

      ALTER TABLE "public"."consignments" ALTER COLUMN "consignment_no" TYPE limited_text;

      ALTER TABLE "public"."consignments" RENAME COLUMN "consignment_no" TO "ext_consignment_no";

      ALTER TABLE "public"."consignments" ADD COLUMN "ext_shipment_no" limited_text;

      ALTER TABLE "public"."consignments" ADD COLUMN "company_fields" json;

      ALTER TABLE "public"."trip_plannings" ALTER COLUMN "fk_asset_id" TYPE bigint;

      ALTER SEQUENCE tmp_consignments_id_seq RESTART WITH 1000000000;

      COMMIT;
    `)
  },

  down: function (queryInterface) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */

    // ALTER TABLE "public"."consignments" ALTER COLUMN "consignment_no" TYPE bigint using consignment_no::bigint;
    return queryInterface.sequelize.query(`
      BEGIN;

      ALTER TABLE "public"."entity_mappings" DROP COLUMN IF EXISTS "enum_type";
      DROP TYPE IF EXISTS enum_entity_mappings_enum_type;
      ALTER TABLE "public"."trip_plannings" DROP COLUMN IF EXISTS "fk_asset_type_id";
      DELETE FROM asset_types WHERE type = 'Consignment';
      DROP INDEX IF EXISTS "public"."asset_types_type_idx";
      DROP INDEX IF EXISTS "public"."entity_mappings_enum_type_fk_asset_id_idx";
      DROP INDEX IF EXISTS "public"."entity_mappings_fk_entity_id_idx";
      ALTER TABLE "public"."consignments" RENAME COLUMN "ext_consignment_no" TO "consignment_no";
      ALTER TABLE "public"."consignments" DROP COLUMN "ext_shipment_no";
      ALTER TABLE "public"."consignments" DROP COLUMN "company_fields";
      ALTER TABLE "public"."consignments" RENAME TO "tmp_consignments";

      COMMIT;
    `)
  }
};
