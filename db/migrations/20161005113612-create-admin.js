'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('admins', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      p_word: {
        type: Sequelize.TEXT
      },
      cont_no: {
        type: Sequelize.TEXT
      },
      email: {
        type: Sequelize.TEXT
      },
      reset_password_token: {
        type: Sequelize.TEXT
      },
      reset_password_expires: {
        type: Sequelize.DATE
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      fk_admin_type: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('admins')
  }
};
