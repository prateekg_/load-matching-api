'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('trip_plannings', 'fk_trip_failure_condition_id', Sequelize.INTEGER);    
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('trip_plannings', 'fk_trip_failure_condition_id');
  }
};
