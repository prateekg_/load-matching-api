'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return  Promise.all([
      queryInterface.addColumn('company_details', 'internal_registry', {
        type: Sequelize.JSON,
        defaultValue: null
      }),
      queryInterface.addColumn('company_details', 'user_registry', {
        type: Sequelize.JSON,
        defaultValue: null
      })
    ])
  },

  down: function (queryInterface) {
    return  Promise.all([
      queryInterface.removeColumn('company_details', 'internal_registry'),
      queryInterface.removeColumn('company_details', 'user_registry')
    ])
  }
};
