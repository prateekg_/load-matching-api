'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('asset_stats', 'e_tis', { type: Sequelize.INTEGER })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('asset_stats', 'e_tis')
    ]
  }
};
