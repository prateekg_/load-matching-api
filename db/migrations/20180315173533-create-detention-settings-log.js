'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('detention_settings_logs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      fk_u_id: {
        type: Sequelize.BIGINT
      },
      fk_poi_id: {
        type: Sequelize.BIGINT
      },
      new_settings: {
        type: Sequelize.JSON
      },
      old_settings: {
        type: Sequelize.JSON
      },
      tis: {
        type: Sequelize.BIGINT
      },
      note: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('detention_settings_logs');
  }
};