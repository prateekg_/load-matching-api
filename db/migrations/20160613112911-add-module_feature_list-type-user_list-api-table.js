'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('apis', 'module_feature_list', Sequelize.ARRAY(Sequelize.INTEGER)),
      queryInterface.addColumn('apis', 'type', Sequelize.STRING),
      queryInterface.addColumn('apis', 'user_list', Sequelize.ARRAY(Sequelize.INTEGER))
    ]
  },

  down: function () {

  }
};
