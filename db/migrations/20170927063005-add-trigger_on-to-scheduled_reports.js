'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('scheduled_reports', 'trigger_on', Sequelize.STRING);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('scheduled_reports', 'trigger_on');
  }
};
