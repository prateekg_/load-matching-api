'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('locations', 'fk_entity_id', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('locations', 'fk_asset_id', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('locations', 'fk_c_id', { type: Sequelize.INTEGER })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('locations', 'fk_entity_id'),
      queryInterface.removeColumn('locations', 'fk_asset_id'),
      queryInterface.removeColumn('locations', 'fk_c_id')
    ]
  }
};
