'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('current_locations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_entity_id: {
        type: Sequelize.INTEGER
      },
      entity_mapped_to: {
        type: Sequelize.ENUM(['Vehicle','Consignment'])
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      fk_consignment_id: {
        type: Sequelize.INTEGER
      }, fk_entity_type_id: {
        type: Sequelize.INTEGER
      },
      lat:{
        type: Sequelize.FLOAT
      },
      lon: {
        type: Sequelize.FLOAT
      },
      no_gps:{
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      lic_plate_no:{
        type: Sequelize.STRING
      },
      spd: {
        type: Sequelize.INTEGER
      },
      tis: {
        type: Sequelize.INTEGER
      },
      osf	:{
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      ign	:{
        type: Sequelize.STRING
      },
      status	:{
        type: Sequelize.STRING
      },
      lname	:{
        type: Sequelize.STRING
      },
      heading	: {
        type: Sequelize.INTEGER
      },
      fk_c_id	: {
        type: Sequelize.INTEGER
      },
      latest_data_packet_tis	: {
        type: Sequelize.INTEGER
      },
      latest_sms_packet_tis	: {
        type: Sequelize.INTEGER
      },

      ignition_on	:{
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },

      vehicle_battery_voltage	: {
        type: Sequelize.FLOAT
      },
      device_battery_voltage	: {
        type: Sequelize.FLOAT
      },
      ignition_voltage	: {
        type: Sequelize.FLOAT
      },
      stationary_since	: {
        type: Sequelize.INTEGER
      },
      idling_since	: {
        type: Sequelize.INTEGER
      },
      moving_since	: {
        type: Sequelize.INTEGER
      },
      door_status	: {
        type: Sequelize.STRING
      },
      door_sensor	: {
        type: Sequelize.JSON
      },
      load_sensor	: {
        type: Sequelize.JSON
      },
      fuel_sensor	: {
        type: Sequelize.JSON
      },
      temperature_sensors	: {
        type: Sequelize.JSON
      },
      immobilizer_sensor	: {
        type: Sequelize.JSON
      },
      rfid_sensor	: {
        type: Sequelize.JSON
      },
      health_status	: {
        type: Sequelize.STRING
      },
      health_status_code	: {
        type: Sequelize.STRING
      },
      health_message	: {
        type: Sequelize.TEXT
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('current_locations');
  }
};