'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('companies', 'feature_list', { type: Sequelize.ARRAY(Sequelize.STRING) })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('companies', 'feature_list')
    ]
  }
};
