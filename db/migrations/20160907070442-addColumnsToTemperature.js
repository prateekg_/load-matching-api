'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('temperatures', 'eid', { type: Sequelize.TEXT }),
      queryInterface.addColumn('temperatures', 'aid', { type: Sequelize.TEXT }),
      queryInterface.addColumn('temperatures', 'cid', { type: Sequelize.TEXT }),
      queryInterface.addColumn('temperatures', 'tid', { type: Sequelize.TEXT }),
      queryInterface.addColumn('temperatures', 'tis', { type: Sequelize.INTEGER }),
      queryInterface.removeColumn('temperatures', 'createdAt'),
      queryInterface.removeColumn('temperatures', 'updatedAt')
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('temperatures', 'eid'),
      queryInterface.removeColumn('temperatures', 'aid'),
      queryInterface.removeColumn('temperatures', 'cid'),
      queryInterface.removeColumn('temperatures', 'tid'),
      queryInterface.removeColumn('temperatures', 'tis')
    ]
  }
};
