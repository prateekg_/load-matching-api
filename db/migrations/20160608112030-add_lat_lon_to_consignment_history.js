'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('consignment_histories', 'lat', Sequelize.FLOAT),
      queryInterface.addColumn('consignment_histories', 'lon', Sequelize.FLOAT)
    ]
  },

  down: function () {
    /*
         Add reverting commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.dropTable('users');
         */
  }
};
