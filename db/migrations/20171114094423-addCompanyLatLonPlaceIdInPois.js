'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('pois', 'company_lat', Sequelize.FLOAT),
      queryInterface.addColumn('pois', 'company_lon', Sequelize.FLOAT)
    ];
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('pois', 'company_lat'),
      queryInterface.removeColumn('pois', 'company_lon'),
    ];
  }
};
