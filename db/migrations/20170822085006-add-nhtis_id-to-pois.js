'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('pois', 'nhtis_id', { type: Sequelize.INTEGER})
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('pois', 'nhtis_id')
  }
};
