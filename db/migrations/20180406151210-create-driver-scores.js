'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('driver_scores', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_user_id: {
        type: Sequelize.INTEGER
      },
      stis: {
        type: Sequelize.INTEGER
      },
      etis: {
        type: Sequelize.INTEGER
      },
      distance: {
        type: Sequelize.DOUBLE
      },
      speed_violations: {
        type: Sequelize.DOUBLE
      },
      harsh_braking: {
        type: Sequelize.DOUBLE
      },
      completed_trips: {
        type: Sequelize.DOUBLE
      },
      values: {
        type: Sequelize.JSON
      },
      total_score: {
        type: Sequelize.DOUBLE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('driver_scores');
  }
};