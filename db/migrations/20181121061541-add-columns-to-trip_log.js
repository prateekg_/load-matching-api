'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('trip_logs', 'violations', Sequelize.JSON),
      queryInterface.addColumn('trip_logs', 'distance', Sequelize.FLOAT),
      queryInterface.addColumn('trip_logs', 'stops', Sequelize.INTEGER),
      queryInterface.addColumn('trip_logs', 'time_taken', Sequelize.INTEGER),
      queryInterface.addColumn('trip_logs', 'departure', Sequelize.INTEGER),
      queryInterface.addColumn('trip_logs', 'arrival', Sequelize.INTEGER),
      queryInterface.addColumn('trip_logs', 'location', Sequelize.JSON),
      queryInterface.addColumn('trip_logs', 'stationary_time', Sequelize.INTEGER),
      queryInterface.addColumn('trip_logs', 'trip', Sequelize.JSON),
      queryInterface.addColumn('trip_logs', 'avg_speed', Sequelize.INTEGER),
      queryInterface.addColumn('trip_logs', 'eta', Sequelize.INTEGER),
      queryInterface.addColumn('trip_logs', 'delayed_by', Sequelize.INTEGER),
      queryInterface.addColumn('trip_logs', 'delayed', Sequelize.BOOLEAN),
      queryInterface.addColumn('trip_logs', 'progress', Sequelize.INTEGER)
    ];
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('trip_logs', 'violations'),
      queryInterface.removeColumn('trip_logs', 'distance'),
      queryInterface.removeColumn('trip_logs', 'stops'),
      queryInterface.removeColumn('trip_logs', 'time_taken'),
      queryInterface.removeColumn('trip_logs', 'departure'),
      queryInterface.removeColumn('trip_logs', 'arrival'),
      queryInterface.removeColumn('trip_logs', 'location'),
      queryInterface.removeColumn('trip_logs', 'stationary_time'),
      queryInterface.removeColumn('trip_logs', 'trip'),
      queryInterface.removeColumn('trip_logs', 'avg_speed'),
      queryInterface.removeColumn('trip_logs', 'eta'),
      queryInterface.removeColumn('trip_logs', 'delayed_by'),
      queryInterface.removeColumn('trip_logs', 'delayed'),
      queryInterface.removeColumn('trip_logs', 'progress')
    ];
  }
};
