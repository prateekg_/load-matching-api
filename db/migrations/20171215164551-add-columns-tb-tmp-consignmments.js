'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('tmp_consignments', 'est_distance', Sequelize.INTEGER),
      queryInterface.addColumn('tmp_consignments', 'est_time', Sequelize.INTEGER)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('tmp_consignments', 'est_distance'),
      queryInterface.removeColumn('tmp_consignments', 'est_time')
    ]
  }
};
