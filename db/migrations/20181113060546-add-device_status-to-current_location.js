'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('current_locations','device_status', Sequelize.JSON);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('current_locations', 'device_status');
  }
};
