'use strict';

'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return  Promise.all([
      queryInterface.addColumn('driver_details', 'lic_meta_files', {
        type: Sequelize.JSON,
        defaultValue: null
      }),
      queryInterface.addColumn('driver_details', 'hazardous_certificate_meta_files', {
        type: Sequelize.JSON,
        defaultValue: null
      }),
      queryInterface.addColumn('driver_details', 'emp_type', {
        type: Sequelize.TEXT,
        defaultValue: null
      }),
      queryInterface.addColumn('driver_details', 'emp_tis', {
        type: Sequelize.INTEGER,
        defaultValue: null
      }),
      queryInterface.addColumn('driver_details', 'salary', {
        type: Sequelize.INTEGER,
        defaultValue: null
      }),
      queryInterface.addColumn('driver_details', 'hours_of_service', {
        type: Sequelize.JSON,
        defaultValue: null
      })
    ])
  },

  down: function (queryInterface) {
    return  Promise.all([
      queryInterface.removeColumn('driver_details', 'lic_meta_files'),
      queryInterface.removeColumn('driver_details', 'hazardous_certificate_meta_files'),
      queryInterface.removeColumn('driver_details', 'emp_type'),
      queryInterface.removeColumn('driver_details', 'emp_tis'),
      queryInterface.removeColumn('driver_details', 'salary'),
      queryInterface.removeColumn('driver_details', 'hours_of_service')
    ])
  }
};
