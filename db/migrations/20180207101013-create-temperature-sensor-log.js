'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('temperature_sensor_logs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      fk_u_id: {
        type: Sequelize.BIGINT
      },
      fk_asset_id: {
        type: Sequelize.BIGINT
      },
      new_temperature_values: {
        type: Sequelize.JSON
      },
      old_temperature_values: {
        type: Sequelize.JSON
      },
      tis: {
        type: Sequelize.BIGINT
      },
      note: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('temperature_sensor_logs');
  }
};