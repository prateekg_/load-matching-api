'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('assets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      asset_id: {
        type: Sequelize.TEXT
      },
      fk_ast_type_id: {
        type: Sequelize.INTEGER
      },
      lic_plate_no: {
        type: Sequelize.TEXT
      },
      mfg: {
        type: Sequelize.TEXT
      },
      model: {
        type: Sequelize.TEXT
      },
      yr_of_mfg: {
        type: Sequelize.TEXT
      },
      veh_id_no: {
        type: Sequelize.TEXT
      },
      oth_details: {
        type: Sequelize.JSON
      },
      name: {
        type: Sequelize.STRING
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('assets')
  }
};
