'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('loads', {
      id: {
        type: Sequelize.STRING
      },
      pickup_address: {
        type: Sequelize.STRING
      },
      pickup_country: {
        type: Sequelize.STRING
      },
      pickup_state: {
        type: Sequelize.STRING
      },
      pickup_city: {
        type: Sequelize.STRING
      },
      pickup_lat: {
        type: Sequelize.FLOAT
      },
      pickup_long: {
        type: Sequelize.FLOAT
      },
      pickup_date: {
        type: Sequelize.DATE
      },
      drop_address: {
        type: Sequelize.STRING
      },
      drop_country: {
        type: Sequelize.STRING
      },
      drop_state: {
        type: Sequelize.STRING
      },
      drop_city: {
        type: Sequelize.STRING
      },
      drop_lat: {
        type: Sequelize.FLOAT
      },
      drop_long: {
        type: Sequelize.FLOAT
      },
      drop_date: {
        type: Sequelize.DATE
      },
      load_material_type: {
        type: Sequelize.STRING
      },
      load_vehicle_type: {
        type: Sequelize.STRING
      },
      price_amount: {
        type: Sequelize.FLOAT
      },
      currency: {
        type: Sequelize.STRING
      },
      load_weight: {
        type: Sequelize.FLOAT
      },
      weight_unit: {
        type: Sequelize.STRING
      },
      consignor_first_name: {
        type: Sequelize.STRING
      },
      consignor_last_name: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      contact_number: {
        type: Sequelize.STRING
      },
      load_valid_from: {
        type: Sequelize.DATE
      },
      load_valid_to: {
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('loads');
  }
};
