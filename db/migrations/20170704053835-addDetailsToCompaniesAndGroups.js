'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('companies', 'details', Sequelize.JSON),
      queryInterface.addColumn('groups', 'details', Sequelize.JSON)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('companies', 'details'),
      queryInterface.removeColumn('groups', 'details')
    ]
  }
};
