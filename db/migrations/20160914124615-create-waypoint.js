'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('waypoints', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_trip_plan_id: {
        type: Sequelize.INTEGER
      },
      fk_poi_id: {
        type: Sequelize.INTEGER
      },
      lname: {
        type: Sequelize.TEXT
      },
      lat: {
        type: Sequelize.FLOAT
      },
      lon: {
        type: Sequelize.FLOAT
      },
      tis: {
        type: Sequelize.INTEGER
      },
      stop_dur: {
        type: Sequelize.INTEGER
      },
      seq_no: {
        type: Sequelize.INTEGER
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      oth_details: {
        type: Sequelize.JSON
      },
      loading: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      unloading: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('waypoints')
  }
};
