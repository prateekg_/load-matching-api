'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('trip_logs', {
      fk_trip_id: {
        type: Sequelize.INTEGER
      },
      cost: {
        type: Sequelize.NUMERIC(10, 2)
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('trip_logs')
  }
};
