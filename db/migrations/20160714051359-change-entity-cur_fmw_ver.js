'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.sequelize.query('ALTER TABLE entities RENAME COLUMN cur_frm_ver TO cur_fmw_ver')
  },
  down: function () {

  }
};
