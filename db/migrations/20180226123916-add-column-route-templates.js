'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('route_templates', 'route_planning', {
      type: Sequelize.BOOLEAN
    });
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('route_templates', 'route_planning');
  }
};
