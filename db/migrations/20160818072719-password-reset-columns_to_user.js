'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('users', 'reset_password_token', { type: Sequelize.TEXT }),
      queryInterface.addColumn('users', 'reset_password_expires', { type: Sequelize.DATE })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('users', 'reset_password_token'),
      queryInterface.removeColumn('users', 'reset_password_expires')
    ]
  }
};
