'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('user_settings', 'group_settings', { type: Sequelize.JSON })
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('user_settings', 'group_settings')
  }
};
