'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('assets', 'axle_powered_no', { type: Sequelize.INTEGER })
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('assets', 'axle_powered_no')
  }
};
