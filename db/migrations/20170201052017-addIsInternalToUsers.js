'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('users', 'is_internal', { type: Sequelize.BOOLEAN, defaultValue: false })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('users', 'is_internal')
    ]
  }
};
