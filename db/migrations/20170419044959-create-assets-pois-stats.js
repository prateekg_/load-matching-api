'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('assets_pois_stats', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      fk_poi_id: {
        type: Sequelize.INTEGER
      },
      entry_tis: {
        type: Sequelize.INTEGER
      },
      exit_tis: {
        type: Sequelize.INTEGER
      },
      latest_tis: {
        type: Sequelize.INTEGER
      },
      poi_stat: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('assets_pois_stats')
  }
};
