'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('route_templates', 'is_advanced', Sequelize.BOOLEAN)
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('route_templates', 'is_advanced')
  }
};
