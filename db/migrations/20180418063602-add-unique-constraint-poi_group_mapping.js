'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.sequelize.query(`ALTER TABLE poi_group_mappings ADD CONSTRAINT unique_poi_group_row UNIQUE (fk_poi_id, fk_group_id, connected);`)
  },

  down: function (queryInterface) {
    return queryInterface.sequelize.query(`ALTER TABLE poi_group_mappings DROP CONSTRAINT unique_poi_group_row;`)
  }
};
