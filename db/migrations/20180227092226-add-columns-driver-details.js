'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return  Promise.all([
      queryInterface.addColumn('driver_details', 'driver_type', {
        type: Sequelize.TEXT,
        defaultValue: null
      }),
      queryInterface.addColumn('driver_details', 'fk_driver_license_type', {
        type: Sequelize.INTEGER,
        defaultValue: null
      }),
      queryInterface.addColumn('driver_details', 'rate_per_km', {
        type: Sequelize.INTEGER,
        defaultValue: null
      })
    ])
  },

  down: function (queryInterface) {
    return  Promise.all([
      queryInterface.removeColumn('driver_details', 'driver_type'),
      queryInterface.removeColumn('driver_details', 'fk_driver_license_type'),
      queryInterface.removeColumn('driver_details', 'rate_per_km')
    ])
  }
};
