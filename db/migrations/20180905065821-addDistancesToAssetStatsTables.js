'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return  Promise.all([
      queryInterface.addColumn('asset_stats', 'distances', {
        type: Sequelize.JSON
      }),
      queryInterface.addColumn('assets_reports_data', 'distances', {
        type: Sequelize.JSON
      })
    ])
  },

  down: function (queryInterface) {
    return  Promise.all([
      queryInterface.removeColumn('asset_stats', 'distances'),
      queryInterface.removeColumn('assets_reports_data', 'distances')
    ])
  }
};