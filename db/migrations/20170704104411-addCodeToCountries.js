'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('countries', 'code', Sequelize.TEXT)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('countries', 'code')
    ]
  }
};
