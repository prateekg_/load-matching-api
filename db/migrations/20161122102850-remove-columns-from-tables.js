'use strict';

module.exports = {
  up: function (queryInterface) {
    return [
      queryInterface.removeColumn('users', 'u_name'),
      queryInterface.removeColumn('locations', 'updatedAt'),
      queryInterface.removeColumn('asset_pois', 'updatedAt'),
      queryInterface.removeColumn('entity_healths', 'updatedAt')
    ]
  },

  down: function () {
    return []
  }
};
