'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('notifications', 'fk_entity_id', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('notifications', 'fk_asset_id', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('notifications', 'fk_c_id', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('notifications', 'fk_poi_id', { type: Sequelize.INTEGER })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('notifications', 'fk_entity_id'),
      queryInterface.removeColumn('notifications', 'fk_asset_id'),
      queryInterface.removeColumn('notifications', 'fk_c_id'),
      queryInterface.removeColumn('notifications', 'fk_poi_id')
    ]
  }
};
