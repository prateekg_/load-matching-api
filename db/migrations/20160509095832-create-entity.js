'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('entities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      e_id: {
        type: Sequelize.TEXT
      },
      cur_frm_ver: {
        type: Sequelize.TEXT
      },
      mac_adr: {
        type: Sequelize.TEXT
      },
      imei_no: {
        type: Sequelize.TEXT
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      lat_fmw_ver: {
        type: Sequelize.TEXT
      },
      fk_e_type_id: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('entities')
  }
};
