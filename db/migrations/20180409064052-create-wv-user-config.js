'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('wv_user_configs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      fk_user_id: {
        type: Sequelize.BIGINT
      },
      fk_g_id: {
        type: Sequelize.BIGINT
      },
      fk_poi_id: {
        type: Sequelize.BIGINT
      },
      fk_wv_table_type_id: {
        type: Sequelize.BIGINT
      },
      config: {
        type: Sequelize.JSON
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('wv_user_configs');
  }
};