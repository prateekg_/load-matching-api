'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('tmp_consignments', 'geo_trip_route', Sequelize.JSON)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('tmp_consignments', 'geo_trip_route')
    ]
  }
};
