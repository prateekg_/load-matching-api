'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('poi_types', 'fk_c_id', { type: Sequelize.INTEGER })
    ]
  },

  down: function () {
    return []
  }
};
