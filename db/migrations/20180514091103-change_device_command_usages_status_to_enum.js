'use strict';

module.exports = {
  up: function (queryInterface) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */

    return queryInterface.sequelize.query(`
   BEGIN;

   CREATE TYPE command_status AS ENUM ('pending', 'sent', 'received_success', 'received_failure', 'cancelled');

   ALTER TABLE device_command_usages add column enum_status command_status;

   UPDATE device_command_usages SET enum_status = status::command_status WHERE status IS NOT NULL AND status != '';

   ALTER TABLE device_command_usages DROP COLUMN status;

   ALTER TABLE device_command_usages RENAME COLUMN enum_status TO status;

   ALTER TABLE device_command_usages ADD COLUMN response text;

   COMMIT;
   `);

  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
