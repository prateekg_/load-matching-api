'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('trailer_type_as', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      aid: {
        type: Sequelize.TEXT
      },
      temp: {
        type: Sequelize.DOUBLE
      },
      battery: {
        type: Sequelize.DOUBLE
      },
      timestamp: {
        type: Sequelize.INTEGER
      },
      fk_con_aid: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('trailer_type_as')
  }
};
