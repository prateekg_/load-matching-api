'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.renameColumn('entities', 'mac_adr', 'device_serial_id');
  },

  down: function (queryInterface) {
    return queryInterface.renameColumn('entities', 'device_serial_id', 'mac_adr');
  }
};