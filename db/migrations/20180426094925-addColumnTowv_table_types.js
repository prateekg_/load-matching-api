'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return  Promise.all([
      queryInterface.addColumn('wv_table_types', 'default_config', {
        type: Sequelize.JSON
      })
    ])
  },

  down: function (queryInterface) {
    return  Promise.all([
      queryInterface.removeColumn('wv_table_types', 'default_config')
    ])
  }
};
