'use strict';
module.exports = {
  up: function (queryInterface) {
    return [
      queryInterface.sequelize.query('ALTER TABLE consignments RENAME COLUMN fk_shm_inv_id TO shm_inv_no'),
      queryInterface.sequelize.query('ALTER TABLE consignments RENAME COLUMN fk_flop_inv_id TO flop_inv_no')
    ]
  },
  down: function () {

  }
};
