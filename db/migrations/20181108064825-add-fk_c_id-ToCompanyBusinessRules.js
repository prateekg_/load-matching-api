'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('company_business_rules','fk_c_id',Sequelize.INTEGER);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('company_business_rules', 'fk_c_id');
  }
};
