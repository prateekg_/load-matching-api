'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('axle_load_instances', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      fk_asset_id: {
        type: Sequelize.BIGINT
      },
      value_before: {
        type: Sequelize.FLOAT
      },
      value_after: {
        type: Sequelize.FLOAT
      },
      tis: {
        type: Sequelize.BIGINT
      },
      lat: {
        type: Sequelize.FLOAT
      },
      lon: {
        type: Sequelize.FLOAT
      },
      lname: {
        type: Sequelize.TEXT
      },
      loaded: {
        type: Sequelize.BOOLEAN
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('axle_load_instances');
  }
};