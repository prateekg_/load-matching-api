'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.createTable('users', { id: Sequelize.INTEGER });
        */
    return [queryInterface.addColumn('entity_types', 'max_battery_voltage', Sequelize.FLOAT), queryInterface.addColumn('entity_types', 'min_battery_voltage', Sequelize.FLOAT)];

  },

  down: function (queryInterface, Sequelize) {
    /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.dropTable('users');
        */
    return [queryInterface.removeColumn('entity_types', 'max_battery_voltage', Sequelize.FLOAT), queryInterface.removeColumn('entity_types', 'min_battery_voltage', Sequelize.FLOAT)];

  }
};
