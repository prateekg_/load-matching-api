'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('cities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.TEXT
      },
      state_id: {
        type: Sequelize.INTEGER
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('cities')
  }
};
