'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('asset_stats', 'tot_osf', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('asset_stats', 'tot_hbk', { type: Sequelize.INTEGER })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('asset_stats', 'tot_osf'),
      queryInterface.removeColumn('asset_stats', 'tot_hbk')
    ]
  }
};
