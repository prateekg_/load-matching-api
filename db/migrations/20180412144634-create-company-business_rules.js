'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return [
      queryInterface.createTable('company_business_rules', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.BIGINT
        },
        name: {
          type: Sequelize.STRING
        },      
        rule: {
          type: Sequelize.STRING
        },
        values: {
          type: Sequelize.JSON
        },
        active: {
          type: Sequelize.BOOLEAN,
          defaultValue: true
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      })
    ]
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('company_business_rules');
  }
};