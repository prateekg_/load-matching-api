'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('entities', 'sim_no', Sequelize.TEXT)
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('entities', 'sim_no')
  }
};
