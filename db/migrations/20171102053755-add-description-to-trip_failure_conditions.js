'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('trip_failure_conditions', 'description', Sequelize.TEXT);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('trip_failure_conditions', 'description');
  }
};
