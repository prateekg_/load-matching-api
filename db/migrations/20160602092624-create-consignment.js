'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('consignments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_shm_c_id: {
        type: Sequelize.INTEGER
      },
      fk_flop_c_id: {
        type: Sequelize.INTEGER
      },
      fk_shm_inv_id: {
        type: Sequelize.TEXT
      },
      fk_flop_inv_id: {
        type: Sequelize.TEXT
      },
      inv_date: {
        type: Sequelize.DATE
      },
      inv_no: {
        type: Sequelize.TEXT
      },
      dest: {
        type: Sequelize.TEXT
      },
      cur_loc: {
        type: Sequelize.TEXT
      },
      fk_c_s_type: {
        type: Sequelize.INTEGER
      },
      shipper: {
        type: Sequelize.TEXT
      },
      transporter: {
        type: Sequelize.TEXT
      },
      act_ship_date: {
        type: Sequelize.DATE
      },
      docket_no: {
        type: Sequelize.TEXT
      },
      exp_delivery_date: {
        type: Sequelize.DATE
      },
      delivered_date: {
        type: Sequelize.DATE
      },
      ship_terms: {
        type: Sequelize.TEXT
      },
      material_desc: {
        type: Sequelize.TEXT
      },
      quantity: {
        type: Sequelize.INTEGER
      },
      dimensions: {
        type: Sequelize.TEXT
      },
      weight: {
        type: Sequelize.TEXT
      },
      delivered: {
        type: Sequelize.BOOLEAN
      },
      details: {
        type: Sequelize.JSON
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('consignments')
  }
};
