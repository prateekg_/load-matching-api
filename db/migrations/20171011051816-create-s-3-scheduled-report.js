'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('s3_scheduled_reports', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      tis: {
        type: Sequelize.INTEGER
      },
      fk_scheduler_id: {
        type: Sequelize.INTEGER
      },
      url: {
        type: Sequelize.TEXT
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('s3_scheduled_reports');
  }
};