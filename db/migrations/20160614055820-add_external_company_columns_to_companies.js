'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('companies', 'c_id_ext', Sequelize.TEXT),
      queryInterface.addColumn('companies', 'c_token', Sequelize.TEXT),
      queryInterface.addColumn('companies', 'c_secret', Sequelize.TEXT)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('companies', 'c_id_ext'),
      queryInterface.removeColumn('companies', 'c_token'),
      queryInterface.removeColumn('companies', 'c_secret')
    ]
  }
};
