'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('pois', 'lat', { type: Sequelize.FLOAT }),
      queryInterface.addColumn('pois', 'lon', { type: Sequelize.FLOAT })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('pois', 'lat'),
      queryInterface.removeColumn('pois', 'lon')
    ]
  }
};
