'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('urls', 'public_host', Sequelize.TEXT);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('urls', 'public_host');
  }
};
