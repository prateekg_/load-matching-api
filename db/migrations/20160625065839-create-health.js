'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('health', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_module_id: {
        type: Sequelize.INTEGER
      },
      tis: {
        type: Sequelize.INTEGER
      },
      mem_usage: {
        type: Sequelize.JSON
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      uptime: {
        type: Sequelize.INTEGER
      },
      downtime: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('health')
  }
};
