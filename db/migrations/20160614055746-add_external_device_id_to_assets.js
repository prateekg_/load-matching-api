'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('assets', 'asset_id_ext', Sequelize.TEXT)
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('assets', 'asset_id_ext')
  }
};
