'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('tyre_group_mappings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_group_id: {
        type: Sequelize.INTEGER
      },
      fk_tyre_id: {
        type: Sequelize.INTEGER
      },
      tis: {
        type: Sequelize.BIGINT
      },
      connected: {
        type: Sequelize.BOOLEAN,
        default: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('tyre_group_mappings')
  }
};
