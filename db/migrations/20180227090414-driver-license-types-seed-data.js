'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.bulkInsert('driver_license_types', [{
      type: "HTV",
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      type: "LMV",
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      type: "LMV-INT",
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      type: "LMV-TR",
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      type: "HMV",
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      type: "HPMV",
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      type: "TRAILER",
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }])
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
