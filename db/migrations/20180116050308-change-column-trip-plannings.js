'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return [
      queryInterface.changeColumn('trip_plannings', 'shipment_no', {
        type : Sequelize.TEXT,
        allowNull : true
      })
    ];
  },
  down: (queryInterface, Sequelize) => {
    queryInterface.changeColumn('trip_plannings', 'shipment_no', {
      type : Sequelize.INTEGER,
      allowNull : true
    })
  }
};