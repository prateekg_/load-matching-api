'use strict';

module.exports = {
  up: function (queryInterface) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return [
      queryInterface.sequelize.query(`
      CREATE OR REPLACE FUNCTION get_sa() RETURNS SETOF pg_stat_activity AS
      $$ SELECT * FROM pg_catalog.pg_stat_activity; $$
      LANGUAGE sql
      VOLATILE
      SECURITY DEFINER
      `),
      queryInterface.sequelize.query(`CREATE VIEW pg_stat_activity_allusers AS SELECT * FROM get_sa()`),
      queryInterface.sequelize.query(`create role view_stats`),
      queryInterface.sequelize.query(`GRANT SELECT ON pg_stat_activity_allusers TO view_stats`)
    ]
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
