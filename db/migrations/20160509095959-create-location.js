'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('locations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      eid: {
        type: Sequelize.TEXT
      },
      cid: {
        type: Sequelize.TEXT
      },
      aid: {
        type: Sequelize.TEXT
      },
      tis: {
        type: Sequelize.INTEGER
      },
      lat: {
        type: Sequelize.FLOAT
      },
      lon: {
        type: Sequelize.FLOAT
      },
      spd: {
        type: Sequelize.FLOAT
      },
      ign: {
        type: Sequelize.CHAR
      },
      osf: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('locations')
  }
};
