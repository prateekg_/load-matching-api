'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('poi_detention_mappings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_primary_poi_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'pois'
          },
          key: 'id'
        }
      },
      fk_detention_poi_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'pois'
          },
          key: 'id'
        }
      },
      fk_c_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'companies'
          },
          key: 'id'
        }
      },
      detention_config: {
        type: Sequelize.JSON
      },
      mapped: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('poi_detention_mappings')
  }
};
