'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('scheduled_reports', 'frequency', { type: Sequelize.TEXT })
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('scheduled_reports', 'frequency')
  }
};
