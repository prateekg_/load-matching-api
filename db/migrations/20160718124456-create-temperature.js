'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('temperatures', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_loc_id: {
        type: Sequelize.BIGINT
      },
      z1: {
        type: Sequelize.DOUBLE
      },
      z2: {
        type: Sequelize.DOUBLE
      },
      z3: {
        type: Sequelize.DOUBLE
      },
      z4: {
        type: Sequelize.DOUBLE
      },
      z5: {
        type: Sequelize.DOUBLE
      },
      z6: {
        type: Sequelize.DOUBLE
      },
      z7: {
        type: Sequelize.DOUBLE
      },
      z8: {
        type: Sequelize.DOUBLE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('temperatures')
  }
};
