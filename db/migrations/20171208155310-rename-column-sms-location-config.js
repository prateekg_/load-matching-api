'use strict';
module.exports = {
  up: function(queryInterface) {
    return [
      queryInterface.renameColumn({
        tableName: 'sms_location_configs'
      },'createdAt','created_at'),
      queryInterface.renameColumn({
        tableName: 'sms_location_configs'
      },'updatedAt','updated_at')
    ];
  },
  down: function(queryInterface) {
    return [
      queryInterface.renameColumn({
        tableName: 'sms_location_configs',
        schema: 'accounting'
      },'createdAt','created_at'),
      queryInterface.renameColumn({
        tableName: 'sms_location_configs'
      },'updatedAt','updated_at')
    ];
  }
};