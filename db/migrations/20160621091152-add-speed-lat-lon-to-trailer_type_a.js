'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('trailer_type_as', 'spd', Sequelize.FLOAT),
      queryInterface.addColumn('trailer_type_as', 'lat', Sequelize.FLOAT),
      queryInterface.addColumn('trailer_type_as', 'lon', Sequelize.FLOAT)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('trailer_type_as', 'spd'),
      queryInterface.removeColumn('trailer_type_as', 'lat'),
      queryInterface.removeColumn('trailer_type_as', 'lon')
    ]
  }
};
