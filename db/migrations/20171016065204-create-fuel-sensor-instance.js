'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('fuel_sensor_instances', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.BIGINT
      },
      volume_after: {
        type: Sequelize.FLOAT
      },
      volume_before: {
        type: Sequelize.FLOAT
      },
      tis: {
        type: Sequelize.BIGINT
      },
      lat: {
        type: Sequelize.FLOAT
      },
      lon: {
        type: Sequelize.FLOAT
      },
      lname: {
        type: Sequelize.TEXT
      },
      is_refuel: {
        type: Sequelize.BOOLEAN
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('fuel_sensor_instances');
  }
};