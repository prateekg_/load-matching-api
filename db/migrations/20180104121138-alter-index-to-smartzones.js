'use strict';

module.exports = {
  up: function (queryInterface) {
    return [
      queryInterface.sequelize.query('DROP INDEX IF EXISTS smart_zones_geo_loc'),
      queryInterface.sequelize.query('CREATE INDEX smart_zones_geo_loc ON smart_zones USING GIST (geo_loc)')
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.sequelize.query('DROP INDEX IF EXISTS smart_zones_geo_loc')
    ]
  }
};