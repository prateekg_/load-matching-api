'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('assets', 'odometer', { type: Sequelize.INTEGER })
    ]
  },

  down: function () {
    // return queryInterface.removeColumn('assets', 'odometer')
  }
};
