'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [

      queryInterface.addColumn('assets', 'has_trailer', { type: Sequelize.BOOLEAN, defaultValue: false }),
      queryInterface.addColumn('assets', 'spd_lmt', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'load_lmt', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'no_temp_zone', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('assets', 'is_refrig', { type: Sequelize.BOOLEAN, defaultValue: false }),
      queryInterface.addColumn('assets', 'zone_lmts', { type: Sequelize.JSON }),
      queryInterface.addColumn('assets', 'dimen', { type: Sequelize.TEXT }),
      queryInterface.addColumn('assets', 'eng_no', { type: Sequelize.TEXT }),
      queryInterface.addColumn('assets', 'has_eng', { type: Sequelize.BOOLEAN, defaultValue: true })

    ]
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
