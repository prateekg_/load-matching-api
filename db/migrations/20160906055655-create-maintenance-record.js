'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('maintenance_records', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      service_tis: {
        type: Sequelize.INTEGER
      },
      odometer: {
        type: Sequelize.INTEGER
      },
      tasks: {
        type: Sequelize.ARRAY(Sequelize.TEXT)
      },
      parts_cost: {
        type: Sequelize.DOUBLE
      },
      labor_cost: {
        type: Sequelize.DOUBLE
      },
      total_cost: {
        type: Sequelize.DOUBLE
      },
      service_station: {
        type: Sequelize.TEXT
      },
      lname: {
        type: Sequelize.TEXT
      },
      time_interval_unit: {
        type: Sequelize.TEXT
      },
      time_interval_value: {
        type: Sequelize.INTEGER
      },
      dist_interval: {
        type: Sequelize.INTEGER
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      scheduled_tis: {
        type: Sequelize.INTEGER
      },
      is_scheduled: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      estimated_cost: {
        type: Sequelize.DOUBLE
      },
      added_by_fk_u_id: {
        type: Sequelize.INTEGER
      },
      last_edited_by_fk_u_id: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('maintenance_records')
  }
};
