'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('scheduled_reports', 'start_tis', Sequelize.INTEGER),
      queryInterface.addColumn('scheduled_reports', 'end_tis', Sequelize.INTEGER),
      queryInterface.addColumn('scheduled_reports', 'trigger_time', Sequelize.STRING)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('scheduled_reports', 'start_tis'),
      queryInterface.removeColumn('scheduled_reports', 'end_tis'),
      queryInterface.removeColumn('scheduled_reports', 'trigger_time')
    ]
  }
};
