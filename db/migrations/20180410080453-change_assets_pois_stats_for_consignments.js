'use strict';

module.exports = {
  up: function (queryInterface) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    queryInterface.sequelize.query(`
   BEGIN;

   ALTER TABLE assets_pois_stats add column enum_type enum_entity_mappings_enum_type;

   UPDATE assets_pois_stats SET enum_type = 'Vehicle' WHERE fk_asset_id IS NOT NULL;
   UPDATE assets_pois_stats SET enum_type = 'Consignment' WHERE fk_consignment_id IS NOT NULL;

   UPDATE assets_pois_stats SET fk_asset_id = fk_consignment_id WHERE fk_consignment_id IS NOT NULL;

   ALTER TABLE assets_pois_stats DROP COLUMN fk_consignment_id;

   ALTER TABLE assets_pois_stats ALTER COLUMN fk_asset_id TYPE bigint;

   COMMIT;
   `);
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
