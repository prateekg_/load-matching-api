'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('next_maintenance_records', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_main_id: {
        type: Sequelize.INTEGER
      },
      fk_main_type_id: {
        type: Sequelize.INTEGER
      },
      distance: {
        type: Sequelize.BIGINT
      },
      month: {
        type: Sequelize.INTEGER
      },
      due_tis: {
        type: Sequelize.INTEGER
      },
      notify_on_date: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      notify_one_week: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      notify_one_month: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('next_maintenance_records')
  }
};
