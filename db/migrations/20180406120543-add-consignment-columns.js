'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return  Promise.all([
      queryInterface.addColumn('consignments', 'packaging_type', {
        type: Sequelize.TEXT,
        defaultValue: null
      }),
      queryInterface.addColumn('consignments', 'hsn_code', {
        type: Sequelize.STRING,
        defaultValue: null
      }),
      queryInterface.addColumn('consignments', 'goods_value', {
        type: Sequelize.DECIMAL,
        defaultValue: null
      })
    ])
  },

  down: function (queryInterface) {
    return  Promise.all([
      queryInterface.removeColumn('consignments', 'packaging_type'),
      queryInterface.removeColumn('consignments', 'hsn_code'),
      queryInterface.removeColumn('consignments', 'goods_value')
    ])
  }
};
