'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('trip_plannings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      start_tis: {
        type: Sequelize.INTEGER
      },
      end_tis: {
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.TEXT
      },
      tid: {
        type: Sequelize.TEXT
      },
      is_deviating: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      geo_trip_route: {
        type: Sequelize.JSON
      },
      oth_details: {
        type: Sequelize.JSON
      },
      started: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      ended: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      route_box: {
        type: Sequelize.GEOMETRY
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      est_distance: {
        type: Sequelize.INTEGER
      },
      est_time: {
        type: Sequelize.INTEGER
      },
      est_matrix: {
        type: Sequelize.JSON
      },
      way_points: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('trip_plannings')
  }
};
