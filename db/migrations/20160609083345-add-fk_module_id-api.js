'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'apis',
      'fk_module_id',
      Sequelize.INTEGER
    )
  },

  down: function () {
    /*
         Add reverting commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.dropTable('users');
         */
  }
};
