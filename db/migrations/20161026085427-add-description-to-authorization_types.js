'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('authorization_types', 'description', { type: Sequelize.TEXT })
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('authorization_types', 'description')
  }
};
