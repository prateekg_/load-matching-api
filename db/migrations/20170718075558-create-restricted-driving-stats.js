'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('restricted_driving_stats', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_group_id: {
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      start_tis: {
        type: Sequelize.BIGINT
      },
      end_tis: {
        type: Sequelize.BIGINT
      },
      distance: {
        type: Sequelize.DOUBLE
      },
      duration: {
        type: Sequelize.DOUBLE
      },
      date: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('restricted_driving_stats')
  }
};
