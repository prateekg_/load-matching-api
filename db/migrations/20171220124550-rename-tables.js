'use strict';

module.exports = {
  up: function (queryInterface) {
    return [
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS access_types RENAME TO access_types_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS api_usages RENAME TO api_usages_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS asset_mappings RENAME TO asset_mappings_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS asset_sharings RENAME TO asset_sharings_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS authorization_types RENAME TO authorization_types_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS batches RENAME TO batches_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS consignment_histories RENAME TO consignment_histories_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS consignment_mappings RENAME TO consignment_mappings_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS consignment_status_types RENAME TO consignment_status_types_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS consignments RENAME TO consignments_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS entity_healths RENAME TO entity_healths_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS entity_imus RENAME TO entity_imus_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS group_accesses RENAME TO group_accesses_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS group_permissions RENAME TO group_permissions_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS health RENAME TO health_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS health_modules RENAME TO health_modules_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS nubot_sanity_checks RENAME TO nubot_sanity_checks_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS pois_old RENAME TO pois_old_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS temperatures RENAME TO temperatures_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS trip_logs RENAME TO trip_logs_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS trip_map_matchings RENAME TO trip_map_matchings_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS trip_plannings_new RENAME TO trip_plannings_new_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS trip_violations RENAME TO trip_violations_deprecate'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS z_admins_obsolete RENAME TO z_admins_obsolete_deprecate')
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS access_types_deprecate RENAME TO access_types'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS api_usages_deprecate RENAME TO api_usages'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS asset_mappings_deprecate RENAME TO asset_mappings'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS asset_sharings_deprecate RENAME TO asset_sharings'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS authorization_types_deprecate RENAME TO authorization_types'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS batches_deprecate RENAME TO batches'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS consignment_histories_deprecate RENAME TO consignment_histories'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS consignment_mappings_deprecate RENAME TO consignment_mappings'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS consignment_status_types_deprecate RENAME TO consignment_status_types'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS consignments_deprecate RENAME TO consignments'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS entity_healths_deprecate RENAME TO entity_healths'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS entity_imus_deprecate RENAME TO entity_imus'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS group_accesses_deprecate RENAME TO group_accesses'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS group_permissions_deprecate RENAME TO group_permissions'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS health_deprecate RENAME TO health'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS health_modules_deprecate RENAME TO health_modules'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS nubot_sanity_checks_deprecate RENAME TO nubot_sanity_checks'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS pois_old_deprecate RENAME TO pois_old'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS temperatures_deprecate RENAME TO temperatures'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS trip_logs_deprecate RENAME TO trip_logs'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS trip_map_matchings_deprecate RENAME TO trip_map_matchings'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS trip_plannings_new_deprecate RENAME TO trip_plannings_new'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS trip_violations_deprecate RENAME TO trip_violations'),
      queryInterface.sequelize.query('ALTER TABLE IF EXISTS z_admins_obsolete_deprecate RENAME TO z_admins_obsolete')
    ]
  }
};
