'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('pois', 'capacity', { type: Sequelize.INTEGER, defaultValue: 0 }),
      queryInterface.addColumn('pois', 'active', { type: Sequelize.BOOLEAN, defaultValue: true }),
      queryInterface.addColumn('pois', 'sad', { type: Sequelize.BOOLEAN, defaultValue: true }),
      queryInterface.addColumn('pois', 'open_on', { type: Sequelize.JSON, defaultValue: { 'sun': false, 'mon': false, 'tue': false, 'wed': false, 'thu': false, 'fri': false, 'sat': false } })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('pois', 'capacity'),
      queryInterface.removeColumn('pois', 'active'),
      queryInterface.removeColumn('pois', 'sad'),
      queryInterface.removeColumn('pois', 'open_on')
    ]
  }
};
