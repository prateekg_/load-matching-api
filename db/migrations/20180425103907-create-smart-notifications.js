'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('smart_notifications', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      start_tis: {
        type: Sequelize.INTEGER
      },
      end_tis: {
        type: Sequelize.INTEGER
      },
      fk_smart_notification_type: {
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      asset_type: {
        type: Sequelize.STRING
      },
      message: {
        type: Sequelize.STRING
      },
      fk_group_id: {
        type: Sequelize.INTEGER
      },
      fk_poi_id: {
        type: Sequelize.INTEGER
      },
      params: {
        type: Sequelize.JSON
      },
      level: {
        type: Sequelize.STRING
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('smart_notifications');
  }
};