'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('route_templates', 'labels', {
      type: Sequelize.JSON,
      defaultValue: null
    });
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('route_templates', 'labels');
  }
};
