'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */

    let set_fk_sensor_id_not_null = "ALTER TABLE public.nubot_sensor_data ADD CHECK (fk_sensor_id is not null or \"createdAt\" < '21 nov 2017');";

    return queryInterface.changeColumn('nubot_sensor_data', 'tis', {
      type: Sequelize.INTEGER,
      allowNull: false
    }
    ).
      then(queryInterface.sequelize.query(set_fk_sensor_id_not_null)).
      then(queryInterface.sequelize.query("CREATE UNIQUE INDEX ON public.nubot_sensor_data(fk_sensor_id,tis) where \"createdAt\" > '21 nov 2017 12:05 ist'"))
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
