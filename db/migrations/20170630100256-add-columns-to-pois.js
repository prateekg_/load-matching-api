'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('pois', 'fees', { type: Sequelize.JSON }),
      queryInterface.addColumn('pois', 'external_id', { type: Sequelize.TEXT })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('pois', 'fees'),
      queryInterface.removeColumn('pois', 'external_id')
    ]
  }
};
