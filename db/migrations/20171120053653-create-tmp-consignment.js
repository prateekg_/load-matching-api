'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('tmp_consignments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      fk_trip_id: {
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.TEXT
      },
      invoice_no: {
        type: Sequelize.TEXT
      },
      consignor: {
        type: Sequelize.JSON
      },
      consignee: {
        type: Sequelize.JSON
      },
      material_type: {
        type: Sequelize.TEXT
      },
      product_code: {
        type: Sequelize.TEXT
      },
      total_packages: {
        type: Sequelize.INTEGER
      },
      weight: {
        type: Sequelize.FLOAT
      },
      shared_with: {
        type: Sequelize.JSON
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('tmp_consignments');
  }
};