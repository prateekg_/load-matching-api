'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('notifications', 'fk_u_id', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('apis', 'fk_feature_id', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('features', 'default', { type: Sequelize.BOOLEAN, defaultValue: false }),
      queryInterface.addColumn('notification_settings', 'fk_g_id', { type: Sequelize.INTEGER }),
      queryInterface.removeColumn('apis', 'auth')
    ]
  },

  down: function (queryInterface, Sequelize) {
    return [
      queryInterface.removeColumn('notifications', 'fk_u_id'),
      queryInterface.removeColumn('apis', 'fk_feature_id'),
      queryInterface.removeColumn('features', 'default'),
      queryInterface.removeColumn('notification_settings', 'fk_g_id'),
      queryInterface.addColumn('apis', 'auth', { type: Sequelize.ARRAY(Sequelize.INTEGER) })
    ]
  }
};
