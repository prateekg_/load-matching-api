'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('service_logs', 'tis', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('installation_logs', 'createdAt', { allowNull: false, type: Sequelize.DATE }),
      queryInterface.addColumn('tags', 'fk_tag_type_id', { type: Sequelize.INTEGER }),
      queryInterface.addColumn('poi_types', 'company_fields', { type: Sequelize.ARRAY(Sequelize.STRING) })
    ]
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
