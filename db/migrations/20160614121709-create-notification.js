'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('notifications', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      cid: {
        type: Sequelize.STRING
      },
      msg: {
        type: Sequelize.TEXT
      },
      level: {
        type: Sequelize.STRING
      },
      fk_notif_id: {
        type: Sequelize.INTEGER
      },
      tis: {
        type: Sequelize.INTEGER
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('notifications')
  }
};
