'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('tyre_servicings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      scheduled_tis: {
        type: Sequelize.INTEGER
      },
      task: {
        type: Sequelize.TEXT
      },
      tyres: {
        type: Sequelize.JSON
      },
      location: {
        type: Sequelize.TEXT
      },
      est_cost: {
        type: Sequelize.INTEGER
      },
      cost: {
        type: Sequelize.INTEGER
      },
      fk_u_id: {
        type: Sequelize.INTEGER
      },
      odometer: {
        type: Sequelize.INTEGER
      },
      note: {
        type: Sequelize.TEXT
      },
      is_scheduled: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      completed_tis: {
        type: Sequelize.BIGINT
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('tyre_servicings')
  }
};
