'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.removeColumn('poi_types', 'company_fields'),
      queryInterface.addColumn('poi_types', 'company_fields', { type: Sequelize.JSON })
    ]
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
