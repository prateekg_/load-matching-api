'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('tmp_shipmentments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      fk_trip_id: {
        type: Sequelize.INTEGER
      },
      fk_consignement_ids: {
        type: Sequelize.ARRAY(Sequelize.INTEGER)
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('tmp_shipmentments');
  }
};