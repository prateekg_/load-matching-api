'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user_kycs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      mobile_no: {
        type: Sequelize.STRING
      },
      user_details: {
        type: Sequelize.JSON
      },
      address_proof: {
        type: Sequelize.JSON
      },
      id_proof: {
        type: Sequelize.JSON
      },
      selfie_proof: {
        type: Sequelize.JSON
      },
      verified: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user_kycs');
  }
};