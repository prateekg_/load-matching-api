'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('notifications', 'is_good', Sequelize.BOOLEAN)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('notifications', 'is_good')
    ]
  }
};
