'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('nu_smart_zones', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      start_tis: {
        type: Sequelize.INTEGER
      },
      end_tis: {
        type: Sequelize.INTEGER
      },
      file_meta: {
        type: Sequelize.JSON
      },
      level: {
        type: Sequelize.INTEGER
      },
      fk_smart_zone_type_id: {
        type: Sequelize.INTEGER
      },
      tis: {
        type: Sequelize.INTEGER
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('nu_smart_zones');
  }
};