'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('trip_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_trip_plan_id: {
        type: Sequelize.INTEGER
      },
      avg_spd: {
        type: Sequelize.INTEGER
      },
      act_distance: {
        type: Sequelize.INTEGER
      },
      act_start_tis: {
        type: Sequelize.INTEGER
      },
      act_end_tis: {
        type: Sequelize.INTEGER
      },
      act_matrix: {
        type: Sequelize.JSON
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('trip_details')
  }
};
