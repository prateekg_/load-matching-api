'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('report_types', 'template', Sequelize.JSON),
      queryInterface.addColumn('report_types', 'summary', Sequelize.TEXT),
      queryInterface.addColumn('report_types', 'fk_c_id', Sequelize.INTEGER)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('scheduled_reports', 'template'),
      queryInterface.removeColumn('scheduled_reports', 'summary'),
      queryInterface.removeColumn('scheduled_reports', 'fk_c_id')
    ]
  }
};
