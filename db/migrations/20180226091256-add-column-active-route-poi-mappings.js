'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('route_templates_poi_mappings', 'active', {
      type: Sequelize.BOOLEAN
    });
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('route_templates_poi_mappings', 'active');
  }
};
