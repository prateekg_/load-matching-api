'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('groups', 'description', { type: Sequelize.TEXT })
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('groups', 'description')
  }
};
