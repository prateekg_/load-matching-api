'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('maintenance_records', 'notes', Sequelize.TEXT);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('maintenance_records', 'notes')
  }
};