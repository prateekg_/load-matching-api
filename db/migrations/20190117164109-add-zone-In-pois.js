'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('pois', 'zone', Sequelize.STRING);

  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('pois', 'zone');

  }
};