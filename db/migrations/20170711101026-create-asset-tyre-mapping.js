'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('asset_tyre_mappings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      fk_tyre_id: {
        type: Sequelize.INTEGER
      },
      tis: {
        type: Sequelize.BIGINT
      },
      connected: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('asset_tyre_mappings')
  }
};
