'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('users', 'rmq_p_word', { type: Sequelize.TEXT })
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('users', 'rmq_p_word')
  }
};
