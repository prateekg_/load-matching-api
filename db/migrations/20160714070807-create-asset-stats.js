'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('asset_stats', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      tot_dist: {
        type: Sequelize.DOUBLE
      },
      tot_idle: {
        type: Sequelize.DOUBLE
      },
      tot_eng: {
        type: Sequelize.DOUBLE
      },
      tot_stat: {
        type: Sequelize.DOUBLE
      },
      date: {
        type: Sequelize.TEXT
      },
      tis: {
        type: Sequelize.INTEGER
      },
      avg_spd: {
        type: Sequelize.INTEGER
      },
      tot_vio: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('asset_stats')
  }
};
