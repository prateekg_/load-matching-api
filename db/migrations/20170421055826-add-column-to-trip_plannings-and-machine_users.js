'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('trip_plannings', 'ext_tid', Sequelize.TEXT),
      queryInterface.addColumn('machine_users', 'fk_u_id', Sequelize.INTEGER)
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('trip_plannings', 'ext_tid'),
      queryInterface.removeColumn('machine_users', 'fk_u_id')
    ]
  }
};
