'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('email_alert_stats', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      lic_plate_no: {
        type: Sequelize.TEXT
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      start_tis: {
        type: Sequelize.INTEGER
      },
      end_tis: {
        type: Sequelize.INTEGER
      },
      email_sent_at: {
        type: Sequelize.INTEGER
      },
      email_type: {
        type: Sequelize.TEXT
      },
      load_status: {
        type: Sequelize.TEXT
      },
      poi_name: {
        type: Sequelize.TEXT
      },
      fk_poi_id: {
        type: Sequelize.INTEGER
      }
      ,
      violations: {
        type: Sequelize.JSON
      },
      violation_count: {
        type: Sequelize.INTEGER
      },
      dwell_time: {
        type: Sequelize.INTEGER
      },
      depot_name: {
        type: Sequelize.TEXT
      },
      fk_group_id: {
        type: Sequelize.INTEGER
      },
      user_email_ids: {
        type: Sequelize.JSON
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('email_alert_stats');
  }
};