'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('user_otps', 'fk_otp_service_id', Sequelize.INTEGER);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('user_otps', 'fk_otp_service_id');
  }
};
