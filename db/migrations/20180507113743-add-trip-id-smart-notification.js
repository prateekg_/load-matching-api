'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.sequelize.query(`ALTER TABLE smart_notifications ADD fk_trip_id int4 NULL ;`)
  },

  down: function (queryInterface) {
    return queryInterface.sequelize.query(`ALTER TABLE smart_notifications DROP COLUMN fk_trip_id;`)
  }
};
