'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('nu_smart_zone_types', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.TEXT
      },
      tis: {
        type: Sequelize.INTEGER
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('nu_smart_zone_types');
  }
};