'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.sequelize.query(`ALTER TABLE asset_group_mappings ADD CONSTRAINT unique_asset_group_row UNIQUE (fk_asset_id, fk_group_id, connected);`)
  },

  down: function (queryInterface) {
    return queryInterface.sequelize.query(`ALTER TABLE asset_group_mappings DROP CONSTRAINT unique_asset_group_row;`)
  }
};
