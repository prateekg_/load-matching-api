'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('users', 'last_online_tis', {type: Sequelize.INTEGER})
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('users', 'last_online_tis')
  }
};
