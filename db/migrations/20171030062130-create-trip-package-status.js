'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('trip_package_statuses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_trip_id: {
        type: Sequelize.INTEGER
      },
      fk_by_u_id: {
        type: Sequelize.INTEGER
      },
      tis: {
        type: Sequelize.BIGINT
      },
      status: {
        type: Sequelize.TEXT
      },
      note: {
        type: Sequelize.TEXT
      },
      reasons: {
        type: Sequelize.TEXT
      },
      files_meta: {
        type: Sequelize.JSON
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('trip_package_statuses');
  }
};