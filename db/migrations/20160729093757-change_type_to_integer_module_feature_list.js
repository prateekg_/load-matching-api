'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.removeColumn('apis', 'module_feature_list'),
      queryInterface.addColumn('apis', 'module_feature_list', {
        type: Sequelize.INTEGER,
        defaultValue: -1
      })
    ]
  },

  down: function () {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
