'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('entity_imus', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ax: {
        type: Sequelize.DOUBLE
      },
      ay: {
        type: Sequelize.DOUBLE
      },
      az: {
        type: Sequelize.DOUBLE
      },
      gx: {
        type: Sequelize.DOUBLE
      },
      gy: {
        type: Sequelize.DOUBLE
      },
      gz: {
        type: Sequelize.DOUBLE
      },
      fk_entity_id: {
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      tis: {
        type: Sequelize.INTEGER
      },
      created_at: {
        type: Sequelize.INTEGER
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('entity_imus')
  }
};
