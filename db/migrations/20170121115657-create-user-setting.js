'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('user_settings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_u_id: {
        type: Sequelize.INTEGER
      },
      settings: {
        type: Sequelize.JSON
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('user_settings')
  }
};
