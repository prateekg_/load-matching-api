'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.sequelize.query('ALTER TABLE locations ALTER COLUMN id TYPE BIGINT USING (id::BIGINT)')
  },

  down: function () {

  }
};
