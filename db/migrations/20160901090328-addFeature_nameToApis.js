'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [

      queryInterface.removeColumn('apis', 'fk_module_id'),
      queryInterface.removeColumn('apis', 'type'),
      queryInterface.removeColumn('apis', 'user_list'),
      queryInterface.removeColumn('apis', 'module_feature_list'),
      queryInterface.addColumn('apis', 'feature_name', { type: Sequelize.TEXT })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('apis', 'feature_name')
    ]
  }
};
