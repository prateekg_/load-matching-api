'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('machine_users', 'is_revoked', { type: Sequelize.BOOLEAN, defaultValue: false })
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('machine_users', 'is_revoked')
  }
};
