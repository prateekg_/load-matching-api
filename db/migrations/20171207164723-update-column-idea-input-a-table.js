'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return [
      queryInterface.changeColumn('input_idea_a', 'msisdn', {
        type : Sequelize.BIGINT,
        allowNull : true
      })
    ];
  },
  down: () => {
   
  }
};