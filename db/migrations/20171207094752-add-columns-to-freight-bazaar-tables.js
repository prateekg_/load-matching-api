'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('freight_bazaar_bulk_loads', 'from_lat', { type: Sequelize.NUMERIC}),
      queryInterface.addColumn('freight_bazaar_bulk_loads', 'from_lon', { type: Sequelize.NUMERIC}),
      queryInterface.addColumn('freight_bazaar_bulk_loads', 'to_lat', { type: Sequelize.NUMERIC}),
      queryInterface.addColumn('freight_bazaar_bulk_loads', 'to_lon', { type: Sequelize.NUMERIC}),

      queryInterface.addColumn('freight_bazaar_spot_loads', 'from_lat', { type: Sequelize.NUMERIC}),
      queryInterface.addColumn('freight_bazaar_spot_loads', 'from_lon', { type: Sequelize.NUMERIC}),
      queryInterface.addColumn('freight_bazaar_spot_loads', 'to_lat', { type: Sequelize.NUMERIC}),
      queryInterface.addColumn('freight_bazaar_spot_loads', 'to_lon', { type: Sequelize.NUMERIC})
    ];
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('freight_bazaar_bulk_loads', 'from_lat'),
      queryInterface.removeColumn('freight_bazaar_bulk_loads', 'from_lon'),
      queryInterface.removeColumn('freight_bazaar_bulk_loads', 'to_lat'),
      queryInterface.removeColumn('freight_bazaar_bulk_loads', 'to_lon'),
        
      queryInterface.removeColumn('freight_bazaar_spot_loads', 'from_lat'),
      queryInterface.removeColumn('freight_bazaar_spot_loads', 'from_lon'),
      queryInterface.removeColumn('freight_bazaar_spot_loads', 'to_lat'),
      queryInterface.removeColumn('freight_bazaar_spot_loads', 'to_lon')
    ];
  }
};
