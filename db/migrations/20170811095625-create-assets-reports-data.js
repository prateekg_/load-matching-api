'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('assets_reports_data', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      shift_start_tis: {
        type: Sequelize.INTEGER
      },
      shift_end_tis: {
        type: Sequelize.INTEGER
      },
      shift_start_lname: {
        type: Sequelize.TEXT
      },
      shift_end_lname: {
        type: Sequelize.TEXT
      },
      tot_eng: {
        type: Sequelize.DOUBLE
      },
      tot_dist: {
        type: Sequelize.DOUBLE
      },
      tot_idle: {
        type: Sequelize.DOUBLE
      },
      tot_stat: {
        type: Sequelize.DOUBLE
      },
      avg_spd: {
        type: Sequelize.INTEGER
      },
      max_spd: {
        type: Sequelize.INTEGER
      },
      max_spd_tis: {
        type: Sequelize.INTEGER
      },
      max_spd_lname: {
        type: Sequelize.TEXT
      },
      tot_osf: {
        type: Sequelize.INTEGER
      },
      tot_hbk: {
        type: Sequelize.INTEGER
      },
      max_stop_lname: {
        type: Sequelize.TEXT
      },
      max_stop_duration: {
        type: Sequelize.DOUBLE
      },
      start_tis: {
        type: Sequelize.INTEGER
      },
      end_tis: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('assets_reports_data');
  }
};