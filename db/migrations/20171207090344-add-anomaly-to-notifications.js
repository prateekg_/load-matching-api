'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('notifications', 'anomaly', Sequelize.TEXT);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('notifications', 'anomaly');
  }
};
