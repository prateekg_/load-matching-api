'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('locations', 'fuel', { type: Sequelize.INTEGER })
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('locations', 'fuel')
  }
};
