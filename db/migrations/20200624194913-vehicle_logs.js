'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vehicle_logs', {
      custtripid: {
        type: Sequelize.STRING
      },
      customerid: {
        type: Sequelize.STRING
      },
      hextagid: {
        type: Sequelize.STRING
      },
      transactiondate: {
        type: Sequelize.DATE
      },
      vehiclenumber: {
        type: Sequelize.STRING
      },
      plazacode: {
        type: Sequelize.STRING
      },
      vehicleclass: {
        type: Sequelize.STRING
      },
      location: {
        type: Sequelize.STRING
      }
    });
  }, 
      
  down: (queryInterface) => {
    return queryInterface.dropTable('loads');
  }
};
