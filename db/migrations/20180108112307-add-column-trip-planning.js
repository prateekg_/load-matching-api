'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('trip_plannings', 'shipment_no',  { 
        type: Sequelize.INTEGER
      })
    ]
  },

  down: function (queryInterface) {
    return [
      queryInterface.removeColumn('trip_plannings', 'shipment_no')
    ]
  }
};
