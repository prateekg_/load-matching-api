'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('driver_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      driver_id: {
        type: Sequelize.TEXT
      },
      lic_no: {
        type: Sequelize.TEXT
      },
      lic_issue_tis: {
        type: Sequelize.BIGINT
      },
      lic_expiry_tis: {
        type: Sequelize.BIGINT
      },
      fitness_certificate_issue_tis: {
        type: Sequelize.BIGINT
      },
      fitness_certificate_expiry_tis: {
        type: Sequelize.BIGINT
      },
      hazardous_certificate_no: {
        type: Sequelize.TEXT
      },
      hazardous_certificate_issue_tis: {
        type: Sequelize.BIGINT
      },
      hazardous_certificate_expiry_tis: {
        type: Sequelize.BIGINT
      },
      custom_fields: {
        type: Sequelize.JSON
      },
      fk_u_id: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('driver_details')
  }
};
