'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('poi_group_mappings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_group_id: {
        type: Sequelize.INTEGER
      },
      fk_poi_id: {
        type: Sequelize.INTEGER
      },
      connected: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      tis: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('poi_group_mappings')
  }
};
