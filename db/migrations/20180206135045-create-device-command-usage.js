'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('device_command_usages', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_user_id: {
        type: Sequelize.INTEGER
      },
      fk_user_type_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'device_command_user_types'
          },
          key: 'id'
        }
      },
      fk_asset_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'assets'
          },
          key: 'id'
        }
      },
      fk_entity_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'entities'
          },
          key: 'id'
        }
      },
      fk_device_command_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'device_commands'
          },
          key: 'id'
        }
      },
      fk_sensor_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'sensors'
          },
          key: 'id'
        }
      },
      status: {
        type: Sequelize.TEXT
      },
      user_notes: {
        type: Sequelize.TEXT
      },
      command: {
        type: Sequelize.TEXT
      },
      transmission_method: {
        allowNull: true,
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      sent_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      received_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('device_command_usages');
  }
};