'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('location_personnels', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      fk_c_id: {
        type: Sequelize.INTEGER
      },
      tis: {
        type: Sequelize.INTEGER
      },
      lat: {
        type: Sequelize.FLOAT
      },
      lon: {
        type: Sequelize.FLOAT
      },
      spd: {
        type: Sequelize.INTEGER
      },
      net_stat: {
        type: Sequelize.INTEGER
      },
      bt_stat: {
        type: Sequelize.INTEGER
      },
      accuracy: {
        type: Sequelize.FLOAT
      },
      aid: {
        type: Sequelize.TEXT
      },
      lname: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface) {
    return queryInterface.dropTable('location_personnels')
  }
};
