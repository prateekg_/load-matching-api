'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('nubot_sensor_instances', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fk_asset_id: {
        type: Sequelize.INTEGER
      },
      units_aft: {
        type: Sequelize.DECIMAL
      },
      units_bef: {
        type: Sequelize.DECIMAL
      },
      lat: {
        type: Sequelize.DOUBLE
      },
      lon: {
        type: Sequelize.DOUBLE
      },
      tis: {
        type: Sequelize.BIGINT
      },
      lname: {
        type: Sequelize.TEXT
      },
      increasing: {
        type: Sequelize.BOOLEAN
      },
      fk_sensor_id: {
        type: Sequelize.INTEGER
      },
      fk_sensor_unit_id: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('nubot_sensor_instances');
  }
};