const env = process.env.NODE_ENV || 'development';
const host = env === 'local' ? "localhost:3003" : 'apid.numadic.com'
const _ = require('lodash')

module.exports = {
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "Numadic API v3",
    "description": "Version 3 API documentation"
  },
  "host": host,
  "basePath": "/v3",
  "tags": [
    {
      "name": "Vehicles",
      "description": "API for vehicles in the system"
    }
  ],
  "schemes": [
    "http",
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "securityDefinitions": {
    "Bearer": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    }
  },
  "paths": _.merge(
    require('./paths/vehicles'),
    require('./paths/notifications'),
    require('./paths/common'),
    require('./paths/pois')
  ),
  "definitions": _.merge(
    require('./definitions/default')
  ),
  "components": _.merge(
    require('./definitions/params')
  ),
};