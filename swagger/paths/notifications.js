module.exports = {
  "/notifications" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Notifications"
      ],
      "summary":"Get notifications",
      "description": "Lists all notifications with filters",
      "parameters": [
        {"$ref": '#/components/params/page'},
        {"$ref": '#/components/params/limit'},
        {
          "name": "start_tis",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Epoch start time"
        },
        {
          "name": "end_tis",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Epoch end time"
        },
        {
          "name": "vehicle_id",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "List vehicle notifications"
        },
        {"$ref": '#/components/params/vehicle_type'},
        {
          "name": "poi_id",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "List POI notifications"
        },
        {
          "name": "poi_category",
          "in": "query",
          "required": false,
          "type": "string",
          "description": "Filter notifications with POI category",
          "enum": ["company","general"] 
        },
        {
          "name": "trip_id",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "List trip notifications"
        },
        {
          "name": "level",
          "in": "query",
          "required": false,
          "type": "string",
          "description": "Filter notifications with level",
          "enum": ["low","medium", "high"] 
        },
        {
          "name": "type_id",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Filter by notification type ID"
        },
        {"$ref": '#/components/params/q'},
        {"$ref": '#/components/params/gid'}
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "List of notifications"
        }
      }
    }
  }
}