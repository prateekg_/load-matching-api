module.exports = {
  "/pois" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      "summary": "Lists all Pois",
      "description": 
      `Column search available keys: [addr, name]
      Column sort available keys:   [addr, name, tot_assets]
      `,
      "parameters": [
        {
          "name": "category",
          "in": "query",
          "required": true,
          "type": "string",
          "description": "Filter table with category",
          "enum": ["company","general"] 
        },
        {"$ref": '#/components/params/q'},
        {"$ref": '#/components/params/page'},
        {"$ref": '#/components/params/limit'},
        {"$ref": '#/components/params/gid'},
        {"$ref": '#/components/params/column_search'},
        {"$ref": '#/components/params/column_sort'},
        {"$ref": '#/components/params/output_format'},
        {"$ref": '#/components/params/download_columns'},
        {
          "name": "type",
          "in": "query",
          "required": false,
          "type": "string",
          "description": "Poi type : Use /v3/types API to get poi types"
        },
        {
          "name": "ne[]",
          "in": "query",
          "required": false,
          "type": "array",
          "items":{
            "type":"double"
          },
          "collectionFormat":"multi",
          "description": "North and East coordinates to find pois with map bounds",
          "x-example": ["24.18471236115215","75.57411975743071"]
        },
        {
          "name": "sw[]",
          "in": "query",
          "required": false,
          "type": "array",
          "items":{
            "type":"double"
          },
          "collectionFormat":"multi",
          "description": "South and West coordinates to find pois with map bounds",
          "x-example": ["24.182928638715538","75.57166017176405"] 
        }
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "List of Pois",
          "schema": {
          }
        }
      }
    }
  },
  "/pois?id=" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      "summary": "Get POI details",
      "description": "View detail information of a place",
      "parameters": [
        {
          "name": "id",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "POI ID"
        }
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "POI details",
          "schema": {
          }
        }
      }
    }
  },
  "/pois/activity" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      "summary":"View POI activity",
      "description": 
      `Poi activity with respect to vehicles inside, entry and exit

      Column search available keys: [lic_plate_no]
      Column sort available keys:   [entry_tis, exit_tis, dwell_time]
      `,
      "parameters": [
        {
          "name": "id",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "POI ID" 
        },
        {
          "name": "status",
          "in": "query",
          "required": false,
          "type": "string",
          "description": "Poi status",
          "enum": ['entered', 'dwelling', 'exited']
        },
        {
          "name": "start_time",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Epoch start time"
        },
        {
          "name": "end_time",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Epoch end time"
        },
        {"$ref": '#/components/params/q'},
        {"$ref": '#/components/params/page'},
        {"$ref": '#/components/params/limit'},
        {"$ref": '#/components/params/gid'},
        {"$ref": '#/components/params/column_search'},
        {"$ref": '#/components/params/column_sort'},
        {"$ref": '#/components/params/output_format'},
        {"$ref": '#/components/params/download_columns'}
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "Poi activity",
          "schema": {
          }
        }
      }
    }
  },
  "/pois/types/count" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      "summary": "POI types with POI count",
      "description": "List of POI types with POI count",
      "parameters": [],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "POI types count",
          "schema": {
          }
        }
      }
    }
  },
  "/pois/stats" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      "summary":"POI statistics",
      "description": "Total entered, exited and dwelling, average dwell time, frequent places, interations, realtim, entered trends, exited trends, average dwell time trends",
      "parameters": [
        {
          "name": "category",
          "in": "query",
          "required": false,
          "type": "string",
          "description": "Filter table with category",
          "enum": ["company","general"] 
        },
        {
          "name": "id",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "POI ID"
        },
        {"$ref": '#/components/params/gid'},
        {
          "name": "start_time",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Epoch start time"
        },
        {
          "name": "end_time",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Epoch end time"
        }
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "POI types count",
          "schema": {
          }
        }
      }
    }
  },
  "/pois/dashboard/summary" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      "summary": "POI dashboard summary",
      "description": "POI dashboard summary of all vehicle interactions",
      "parameters": [
        {
          "name": "id",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "POI ID"
        },
        {"$ref": '#/components/params/gid'}
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "POI dashboard summary",
          "schema": {
          }
        }
      }
    }
  },
  "/pois/dashboard/map" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      "summary": "POI dashboard map",
      "description": "POI dashboard map",
      "parameters": [
        {
          "name": "id",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "POI ID"
        },
        {
          "name": "status",
          "in": "query",
          "required": true,
          "type": "string",
          "description": "Filter table with status",
          "enum": ["inbound","outbound","dwelling"] 
        },
        {"$ref": '#/components/params/q'},
        {"$ref": '#/components/params/gid'}
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "POI dashboard map",
          "schema": {
          }
        }
      }
    }
  },
  "/pois/dashboard/activity" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      "summary": "POI dashboard activity",
      "description": "POI dashboard activity",
      "parameters": [
        {
          "name": "id",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "POI ID"
        },
        {
          "name": "status",
          "in": "query",
          "required": true,
          "type": "string",
          "description": "Filter table with status",
          "enum": ["inbound","outbound","dwelling"] 
        },
        {"$ref": '#/components/params/q'},
        {"$ref": '#/components/params/gid'},
        {"$ref": '#/components/params/page'},
        {"$ref": '#/components/params/limit'}
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "POI dashboard activity",
          "schema": {
          }
        }
      }
    }
  },
  "/pois/assets" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      summary: "List all vehicles interacting with the POI",
      "description": 
      `Vehicles inside, incoming, outgoing and nearby

      Column search available keys: [lic_plate_no, transporter.name, trips.id, lname, source.lname, destination.lname]
      Column sort available keys:   [entry_tis, dwell_minutes, tis, exit_tis, spd]
      Range filter available keys:  [entry_tis, exit_tis]
      `,
      "parameters": [
        {
          "name": "id",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "POI ID"
        },
        {
          "name": "status",
          "in": "query",
          "required": true,
          "type": "string",
          "description": "Filter data with status",
          "enum":["all","incoming","inside","outgoing","nearby"]
        },
        {"$ref": '#/components/params/page'},
        {"$ref": '#/components/params/limit'},
        {"$ref": '#/components/params/gid'},
        {"$ref": '#/components/params/column_search'},
        {"$ref": '#/components/params/column_sort'},
        {"$ref": '#/components/params/output_format'},
        {"$ref": '#/components/params/download_columns'},
        {"$ref": '#/components/params/column_range_filter'},
        {
          "name": "nearby_radius",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Filter vehicles within range in km"
        },
        {"$ref": '#/components/params/device_status'}
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "Lists all vehicles interacting with a POI",
          "schema": {
          }
        }
      }
    }
  },
  "/pois/detention" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      summary: "List all vehicles interaction with detention POI",
      "description": `Vehicles interaction with detention POI`,
      "parameters": [
        {
          "name": "primary_poi",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "Primary POI ID"
        },
        {
          "name": "detention_poi",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "Detention POI ID"
        },
        {
          "name": "start_tis",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "Epoch start time"
        },
        {
          "name": "end_tis",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "Epoch end time"
        },
        {"$ref": '#/components/params/page'},
        {"$ref": '#/components/params/limit'},
        {"$ref": '#/components/params/output_format'},
      ],
      "responses": {
        "200": {
          "description": "List all vehicles interaction with detention POI",
          "schema": {
          }
        }
      }
    }
  },
  "/pois/detention/log" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Pois"
      ],
      "summary": "View detention setting logs",
      "description": "View detailed log of how many time the detention POI has been updated",
      "parameters": [
        {
          "name": "detention_poi",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "Detention POI ID"
        },
        {"$ref": '#/components/params/q'},
        {"$ref": '#/components/params/page'},
        {"$ref": '#/components/params/limit'},
      ],
      "responses": {
        "200": {
          "description": "View detention setting logs",
          "schema": {
          }
        }
      }
    }
  }
}