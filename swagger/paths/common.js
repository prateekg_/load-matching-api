module.exports = {
  "/status" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": false
        }
      ],
      "tags": [
        "Common"
      ],
      "summary": "Platform status",
      "description": "Platform status",
      "responses": {
        "200": {
          "description": "Platform status"
        }
      }
    }
  },
  "/search" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Common"
      ],
      "summary": "Platform wide search",
      "description": "Search vehicles, trips, places, drivers ...",
      "parameters": [
        {"$ref": '#/components/params/q'},
        {
          "name": "type",
          "in": "query",
          "required": false,
          "type": "string",
          "description": "Filter with type",
          "enum": ["vehicles","tyres"] 
        },
        {"$ref": '#/components/params/gid'},
        {"$ref": '#/components/params/page'},
        {"$ref": '#/components/params/limit'}
      ],
      "responses": {
        "200": {
          "description": "Matched item list"
        }
      }
    }
  },
  "/deadzones" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Common"
      ],
      "summary": "Deadzone data",
      "description": "Deadzone map data generated by numadic based on vehicle movement",
      "responses": {
        "200": {
          "description": "Deadzone map data"
        }
      }
    }
  },
  "/types" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Common"
      ],
      "summary":"List types",
      "description": "Get types of vehicles, pois etc",
      "parameters": [
        {
          "name": "category",
          "in": "query",
          "required": false,
          "type": "string",
          "description": "Filter with category for POI",
          "enum": ["company","general"] 
        },
        {
          "name": "type",
          "in": "query",
          "required": false,
          "type": "string",
          "description": "Filter with type",
          "enum": ["poi","notification","user","licence","smart_notification","maintenance"] 
        }
      ],
      "responses": {
        "200": {
          "description": "Type list"
        }
      }
    }
  },
  "/loccode" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Common"
      ],
      "summary": "Get location information",
      "description": "Location lat, lon, address",
      "parameters": [
        {"$ref": '#/components/params/q'},
        {
          "name": "lat",
          "in": "query",
          "required": false,
          "type": "double",
          "description": "Filter with Lat"
        },
        {
          "name": "lon",
          "in": "query",
          "required": false,
          "type": "double",
          "description": "Filter with Lon"
        }
      ],
      "responses": {
        "200": {
          "description": "Location data"
        }
      }
    }
  },
  "/geopath" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Common"
      ],
      "summary": "Get trip path information",
      "description": "Geo path",
      "parameters": [
        {
          "name": "point[]",
          "in": "query",
          "required": true,
          "type": "array",
          "items":{
            "type":"string"
          },
          "collectionFormat":"multi",
          "description": "Lat, lon array" 
        }
      ],
      "responses": {
        "200": {
          "description": "Georoute data"
        }
      }
    }
  },
  "/wv-error" : {
    "post": {
      "tags": [
        "Common"
      ],
      "consumes": ["application/json"],
      "summary": "Webview error reporting",
      "description": "Used by frontend to send error for debugging",
      "parameters": [{
        "name":"body",
        "in":"body"
      }],
      "responses": {
        "405": {
          "description": "Invalid input",
          "content": {}
        }
      },
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ]
    }
  }
}