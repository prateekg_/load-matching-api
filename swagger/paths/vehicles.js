module.exports = {
  "/vehicles" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Vehicles"
      ],
      "summary": "List all vehicles",
      "description": 
      `Poi activity with respect to vehicles inside, entry and exit

      Column search available keys: [lic_plate_no, operator.name, lname]
      Column sort available keys:   [lic_plate_no, operator.name, lname, spd, tis]
      `,
      "parameters": [
        {
          "name": "status",
          "in": "query",
          "required": false,
          "type": "string",
          "description": "Filter table with vehicle status",
          "enum":["moving","idling","stationary","unknown","disconnected"]
        },
        {"$ref": '#/components/params/q'},
        {"$ref": '#/components/params/page'},
        {"$ref": '#/components/params/limit'},
        {"$ref": '#/components/params/gid'},
        {"$ref": '#/components/params/column_search'},
        {"$ref": '#/components/params/column_sort'},
        {"$ref": '#/components/params/output_format'},
        {"$ref": '#/components/params/download_columns'},
        {"$ref": '#/components/params/vehicle_type'},
        {"$ref": '#/components/params/device_status'}
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "List of vehicles",
          "schema": {
          }
        }
      }
    }
  },
  "/vehicles?id=" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Vehicles"
      ],
      "summary": "Get vehicle details",
      "description": "Vehicle details",
      "parameters": [
        {
          "name": "id",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "Vehicle ID"
        }
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "Vehicle details",
          "schema": {
          }
        }
      }
    }
  },
  "/vehicles/stats" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Vehicles"
      ],
      "summary":"Get vehicle stats",
      "description": "Use vehicle ID to get stats for single vehicles",
      "parameters": [
        {
          "name": "id",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Vehicle ID"
        },
        {
          "name": "type",
          "in": "query",
          "required": true,
          "type": "string",
          "description": "Stats type",
          "enum" : ["distance", "speed", "utilisation", "violation","violation_by_time"]
        },
        {"$ref": '#/components/params/vehicle_type'},
        {"$ref": '#/components/params/gid'},
        {
          "name": "start_tis",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Epoch start time"
        },
        {
          "name": "end_tis",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Epoch end time"
        }
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "Vehicles stats"
        }
      }
    }
  },
  "/vehicles/class" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Vehicles"
      ],
      "summary": "List vehicle class",
      "description": "Lists all vehicles class",
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "Vehicles class list"
        }
      }
    }
  },
  "/vehicles/activity" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Vehicles"
      ],
      "summary":"Vehicle movement data",
      "description": "Vehicle activity",
      "parameters": [
        {
          "name": "id",
          "in": "query",
          "required": true,
          "type": "integer",
          "description": "Vehicle ID"
        },
        {
          "name": "type",
          "in": "query",
          "required": false,
          "type": "string",
          "description": "Activity type (Default vehicle activity)",
          "enum" : ["analytics", "gps"]
        },
        {"$ref": '#/components/params/page'},
        {"$ref": '#/components/params/limit'},
        {
          "name": "start_tis",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Epoch start time"
        },
        {
          "name": "end_tis",
          "in": "query",
          "required": false,
          "type": "integer",
          "description": "Epoch end time"
        },
        {"$ref": '#/components/params/q'},
        {"$ref": '#/components/params/output_format'}
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "Vehicles activity"
        }
      }
    }
  },
  "/vehicles/activity/trail" : {
    "get": {
      "security": [
        {
          "Bearer": [],
          "required": true
        }
      ],
      "tags": [
        "Vehicles"
      ],
      "summary": "Vehicle detail movement data",
      "description": "Vehicle activity trail",
      "parameters": [
        {
          "name": "id",
          "in": "query",
          "required": true,
          "type": "string",
          "description": "Trail ID"
        }
      ],
      "produces": [
        "application/json"
      ],
      "responses": {
        "200": {
          "description": "Vehicles activity trail"
        }
      }
    }
  }
}