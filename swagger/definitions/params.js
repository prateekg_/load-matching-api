module.exports = {
  "params":{
    "page": {
      "name": "page",
      "in": "query",
      "required": false,
      "type": "integer",
      "description": "Page number"
    },
    "limit":{
      "name": "limit",
      "in": "query",
      "required": false,
      "type": "integer",
      "description": "Page limit",
      "x-example": 10
    },
    "q": {
      "name": "q",
      "in": "query",
      "required": false,
      "type": "string",
      "description": "Search with keyword"
    },
    "gid":{
      "name": "gid",
      "in": "query",
      "required": false,
      "type": "string",
      "description": "Filter data with group ID (String)"
    },
    "device_status":{
      "name": "device_status[]",
      "in": "query",
      "required": false,
      "type": "array",
      "items":{
        "type":"array",
        "items":{
          "type":"string",
          "enum":['battery_disconnected','good','ignition_anomaly','no_signal','scheduled','service_pending_client','wiring_issue']
        }
      },
      "collectionFormat":"multi",
      "description": "Filter by nubot status"
    },
    "download_columns": {
      "name": "columns",
      "in": "query",
      "required": false,
      "type": "string",
      "description": "Download user selected columns or all columns, no option will download user selected columns",
      "enum": ["all"] 
    },
    "output_format": {
      "name": "format",
      "in": "query",
      "required": false,
      "type": "string",
      "description": "Download as xls file",
      "enum" : ["xls"]
    },
    "column_search": {
      "name": "search",
      "in": "query",
      "required": false,
      "type": "array",
      "items":{
        "type":"string"
      },
      "collectionFormat":"multi",
      "description": "Search table column format: <key_name>|<search_keyword> eg: name|test"
    },
    "column_sort": {
      "name": "sort",
      "in": "query",
      "required": false,
      "type": "array",
      "items":{
        "type":"string"
      },
      "collectionFormat":"multi",
      "description": "Sort table column format: <key_name>|asc eg: tis|desc"
    },
    "column_range_filter":{
      "name": "range_filter",
      "in": "query",
      "required": false,
      "type": "array",
      "items":{
        "type":"string"
      },
      "collectionFormat":"multi",
      "description": "Filter by date range format: <key_name>|<start_tis>-<end_tis> eg: entry_tis|1556649000-1556735399"
    },
    "vehicle_type": {
      "name": "vehicle_type",
      "in": "query",
      "required": false,
      "type": "string",
      "description": "Filter table with vehicle type",
      "enum": ["Truck","Van"] 
    }
  }
}