
# Numadic V3 API server
___
### Getting Started
___
##### Prerequisites
- [Nodejs platform v.9.11.2](https://www.nodejs.org/)
- [Nodemon](https://nodemon.io/)

##### Local development

```
$ npm install
$ NODE_ENV=local nodemon metrics_api.js
```

### Authors
___
- Haston Silva


### API Documentation
___
- Local URL
http://localhost:3003/v3/api/docs

- Development URL
https://apid.numadic.com/v3/api/docs

- Production URL
https://api.numadic.com/v3/api/docs
